﻿Shader "Moonshine/UVResetter"
{
    Properties
    {
        //[PerRendererData]
        _MainTex("_MainTex", 2D) = "white" {}
        //[PerRendererData]
        _Color ("_Color",Color) = (1,1,1,1)
        _ColorPow("_ColorPow" , Float) = 1
        [Header(View 1)]
        [Space(20)]
        [Toggle] _View1_Enable("__Enable", Float) = 0
        [Space(20)]
        [Toggle] _View1_Mirror("__Mirror", Float) = 0
        [Space(10)]
        _View1_Source("__Source",Vector) = (0,0,0.5,0.5)
        _View1_Output("__Output",Vector) = (0.5,0.5,0.1,0.1)
    }
    SubShader
    {
        Tags
        {
            "Queue" = "Transparent"
            "PreviewType" = "Plane"
        }

        LOD 100

        Pass
        {
            Blend SrcAlpha OneMinusSrcAlpha
            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            #include "UnityCG.cginc"
            #include "AVProVideo.cginc"

            struct appdata_t
            {
                float4 vertex   : POSITION;
                float4 color    : COLOR;
                float2 texcoord : TEXCOORD0;
            };

            struct v2f
            {
                float4 vertex   : SV_POSITION;
                fixed4 color : COLOR;
                half4 uv  : TEXCOORD0;
                float4 worldPosition : TEXCOORD1;
            };

            sampler2D _MainTex;
            float4 _MainTex_ST;
            fixed4 _Color;
            float _ColorPow;

            float _View1_Enable;
            float _View1_Mirror;
            vector _View1_Source;
            vector _View1_Output;


            v2f vert (appdata_t v)
            {
                v2f o;
                o.vertex = UnityObjectToClipPos(v.vertex);
                o.uv.xy = TRANSFORM_TEX(v.texcoord, _MainTex);
                o.color = v.color;
                return o;
            }

            float2 BlockUvGet(float2 uv, float4 source, float4 output) {
                return source.xy + (uv - output.xy) * source.zw / output.zw;
            }

            float BlockUvCheck(float2 uv,float4 source,float4 output,float enable) {
                fixed b_x = step(output.x , uv.x);
                fixed s_x = step(uv.x , output.x + output.z);
                fixed b_y = step(output.y, uv.y);
                fixed s_y = step(uv.y, output.y + output.w);

                return step(4.5, b_x + s_x + b_y + s_y + enable);
            }

            fixed4 frag(v2f i) : SV_Target
            {
                float2 uv = float2(abs(i.uv.x -_View1_Mirror),i.uv.y);
                float block_check_1 = BlockUvCheck(uv, _View1_Source, _View1_Output, _View1_Enable);
                float2 block_uv_1 = BlockUvGet(uv, _View1_Source, _View1_Output);
                uv = lerp(uv, block_uv_1, block_check_1);

                //return SampleRGBA(_MainTex, uv) * i.color;
                //return tex2D(_MainTex, uv) *i.color;
                float4 col = tex2D(_MainTex, uv) * _Color;
                return pow(col,_ColorPow);
            }
            ENDCG
        }
    }
}
