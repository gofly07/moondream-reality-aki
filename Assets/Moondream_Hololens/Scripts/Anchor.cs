﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Moonshine.Moondream.SpatialAnchors
{
    [System.Serializable]
    public class Anchor
    {
        public int Number;
        [NonSerialized] public Transform Transform;
        public string AnchorKey;
    }

}