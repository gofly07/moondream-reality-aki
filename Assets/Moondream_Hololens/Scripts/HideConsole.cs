﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonshine.Moondream.SpatialAnchors {
    public class HideConsole : MonoBehaviour {
        // Start is called before the first frame update
        void Start() {
            Debug.developerConsoleVisible = false;
        }
    }
}