﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;
using UnityEngine;
using Microsoft.Azure.SpatialAnchors;
using Microsoft.Azure.SpatialAnchors.Unity;
using UnityEngine.UI;
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
#elif UNITY_WSA
using UnityEngine.XR.WSA;
using UnityEngine.XR.WSA.Input;
#endif
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Moonshine.Moondream.SpatialAnchors
{
    public class AnchorFinderManager : MonoBehaviour
    {
        public class AnchorLocatInfo
        {
            public GameObject Obj;
            public bool IsLocated = true;
            public Vector3 Offset;
        }
        [Header("Select Config")]
        [SerializeField] private SpatialAnchorConfig m_Config;
        [Header("Put Your Anchor Object Here")]
        [SerializeField] public Transform m_Anchor;
        private Vector3[] m_AnchorsLocalPosition = new Vector3[9];
        private List<string> m_AnchorKeys;
        private int m_AnchorStorageIndex = 0;
        [SerializeField] private List<AnchorKeysStorage> m_AnchorStorages;
        [SerializeField] private GameObject m_AnchorPrefab;
        [SerializeField] private bool m_AutoStart = false;

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
        [SerializeField] private ARAnchorManager m_ARAnchorManager;
        private bool m_ARSessionStateIsTracking = false;
#endif
        private SpatialAnchorManager m_SpatialAnchorManager;
        private AnchorLocateCriteria m_Criteria = null;
        private Dictionary<string, AnchorLocatInfo> m_AnchorDic = new Dictionary<string, AnchorLocatInfo>();
        private CloudSpatialAnchorWatcher m_CurrentWathcer;

        [SerializeField]
        private Net.NetTcpBase m_Net = null;
        [SerializeField]
        private Net.NetTcpCommandContentNameMobiReceiver m_NetContent = null;

        [System.Serializable]
        public class TextEvent : UnityEvent<string> { }

        /// <summary>
        /// Anchor Information For Debug 
        /// </summary>
        [Space(40)]
        [Header("Anchor Events")]
        public TextEvent OnLogInvoke;
        /// <summary>
        /// Invoke when spatial anchor start session async
        /// </summary>
        public UnityEvent OnStartSession;
        /// <summary>
        /// Invoke when first anchor found
        /// </summary>
        public UnityEvent OnFirstAnchorFound;
        /// <summary>
        /// Invoke when detected anchor are deleted
        /// </summary>
        public UnityEvent OnAnchorsClear;

        /// <summary>
        /// Invoke when all anchor lost 
        /// </summary>
        [Header("Tracking Events")]
        public UnityEvent OnTrackingLost;
        /// <summary>
        /// Invoke when any anchor are found after all anchor lost
        /// </summary>
        public UnityEvent OnTrackingBack;


        // Start is called before the first frame update
        IEnumerator Start()
        {
            m_SpatialAnchorManager = gameObject.AddComponent<SpatialAnchorManager>();
            m_SpatialAnchorManager.AuthenticationMode = AuthenticationMode.ApiKey;
            m_SpatialAnchorManager.LogLevel = SessionLogLevel.All;
            m_SpatialAnchorManager.AnchorLocated += CloudManager_AnchorLocated;
            SetAnchorsLocalPosition();
            SetConfig();

#if UNITY_EDITOR
            if (Application.isPlaying)
            {
                SetText("Can't Start Session In Editor.");
                m_SpatialAnchorManager.enabled = false;
                yield break;
            }
#endif

            SetText("Checking Session Availablity.");
            var _NowTime = 0.0f;
            var _Reset = true;
            while (IsSlamActive() == false) {
                _NowTime += Time.deltaTime;
                if (_NowTime > 10)
                {
                    if (_Reset)
                    {
                        _Reset = false;
                        var _Cmd = new Net.CommandTest()
                        {
                            Key = MoondreamGlobal.COMMAND_KEY.JustTest,
                            Msg = $"Unable initialize back to lobby SessionStatus:{GetSlamState()}"
                        };
                        m_Net.Send(_Cmd);
                        m_NetContent.LaunchMoondream();
                    }
                }
                yield return null;
            }
            SetText($"Slam State : {GetSlamState()}");

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS

#elif UNITY_WSA
            WorldManager.OnPositionalLocatorStateChanged += OnWorldAnchorPositionalLocatorStateChanged;
#endif

            m_Criteria = new AnchorLocateCriteria();

            if (m_AutoStart)
                StartSession(m_AnchorStorageIndex, true);

            yield break;
        }
        private bool IsSlamActive()
        {
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
            return ARSession.state == ARSessionState.SessionTracking;
#elif UNITY_WSA
            return WorldManager.state == PositionalLocatorState.Active;
#endif
            SetText("Slam Default False");
            return false;
        }

        private string GetSlamState()
        {
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
            return ARSession.state.ToString();
#elif UNITY_WSA
            return WorldManager.state.ToString();
#endif
            return null;
        }


        private void Update()
        {
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
            ARCheckSession();
#endif
        }

        private void SetAnchorsLocalPosition()
        {
            m_AnchorsLocalPosition[0] = Vector3.zero;
            m_AnchorsLocalPosition[1] = new Vector3(2.5f, 1.5f, 2.5f);
            m_AnchorsLocalPosition[2] = new Vector3(2.5f, 1.5f, 0);
            m_AnchorsLocalPosition[3] = new Vector3(2.5f, 1.5f, -2.5f);
            m_AnchorsLocalPosition[4] = new Vector3(0, 1.5f, 2.5f);
            m_AnchorsLocalPosition[5] = new Vector3(0, 1.5f, -2.5f);
            m_AnchorsLocalPosition[6] = new Vector3(-2.5f, 1.5f, 2.5f);
            m_AnchorsLocalPosition[7] = new Vector3(-2.5f, 1.5f, 0);
            m_AnchorsLocalPosition[8] = new Vector3(-2.5f, 1.5f, -2.5f);
        }

        private void SetAnchorKeys(int iIndex = 0)
        {
            m_AnchorKeys = new List<string>();
            foreach (var item in m_AnchorStorages[iIndex].AnchorKeys)
            {
                m_AnchorKeys.Add(item);
                SetText($"Anchor read: {item}");
            }
        }

        private void SetConfig()
        {
            if (m_Config == null)
                return;
            m_SpatialAnchorManager.SpatialAnchorsAccountId = m_Config.SpatialAnchorsAccountId;
            m_SpatialAnchorManager.SpatialAnchorsAccountKey = m_Config.SpatialAnchorsAccountKey;
            m_SpatialAnchorManager.SpatialAnchorsAccountDomain = m_Config.SpatialAnchorsAccountDomain;

            SetText($"AccountId: {m_SpatialAnchorManager.SpatialAnchorsAccountId}");
            SetText($"AccountKey: {m_SpatialAnchorManager.SpatialAnchorsAccountKey}");
            SetText($"AccountDomain: {m_SpatialAnchorManager.SpatialAnchorsAccountDomain}");
        }

        public void SetMainAnchorLoacte()
        {
            var _Poss = new List<Vector3>();
            var _Rots = new List<Quaternion>();
            foreach (var _Anc in m_AnchorDic)
            {
                var _Info = _Anc.Value;
                if (_Info.IsLocated)
                {
                    _Poss.Add(PosDest(_Info.Obj, m_AnchorsLocalPosition[0] - _Info.Offset));
                    _Rots.Add(RotDest(_Info.Obj, Quaternion.identity));
                }
            }
            var _Pos = Average(_Poss);
            var _Rot = Average(_Rots);
            var _Count = _Poss.Count;
            if (_Count > 0)
            {
                m_Anchor.position = _Pos;
                m_Anchor.rotation = _Rot;
            }
        }

        private Vector3 PosDest(GameObject iObj, Vector3 iDirPos)
        {
            return iObj.transform.TransformDirection(iDirPos) + iObj.transform.position;
        }

        private Quaternion RotDest(GameObject iObj, Quaternion iDirRot)
        {
            return iDirRot * iObj.transform.rotation;
        }

        private Vector3 Average(List<Vector3> iPoss)
        {
            var _PAverage = Vector3.zero;
            var _Count = iPoss.Count;
            var _Weight = 1.0f / _Count;
            iPoss.ForEach((_Pos) =>
            {
                _PAverage += Vector3.Lerp(Vector3.zero, _Pos, _Weight);
            });
            return _PAverage;
        }

        private Quaternion Average(List<Quaternion> iRots)
        {
            var _QAverage = Quaternion.identity;
            var _Count = iRots.Count;
            var _Weight = 1.0f / _Count;
            iRots.ForEach((_Rot) =>
            {
                _QAverage *= Quaternion.Slerp(Quaternion.identity, _Rot, _Weight);
            });
            return _QAverage;
        }

        private int IndexCheck(string iKey)
        {
            var _Index = -1;
            for (int i = 0; i < m_AnchorKeys.Count; i++)
            {
                if (m_AnchorKeys[i] == iKey)
                    _Index = i;
            }
            return _Index;
        }

        private void CloudManager_AnchorLocated(object sender, AnchorLocatedEventArgs args)
        {
            if (args.Status == LocateAnchorStatus.Located)
            {
                UnityDispatcher.InvokeOnAppThread(() =>
                {
                    var _Index = IndexCheck(args.Identifier);
                    SetText($"Find Anchor: {args.Identifier}, Anchor Index: {_Index}");
                    var _CloudAnchor = args.Anchor;
                    var _AnchorPose = Pose.identity;
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
                    _AnchorPose = _CloudAnchor.GetPose();
#elif UNITY_WSA

#endif
                    var _Obj = Instantiate(m_AnchorPrefab, _AnchorPose.position, _AnchorPose.rotation);
                    var _NativeAnchor = _Obj.AddComponent<CloudNativeAnchor>();
                    _NativeAnchor.CloudToNative(_CloudAnchor);

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS

#elif UNITY_WSA
                    _NativeAnchor.NativeAnchor.OnTrackingChanged += OnWorldAnchorTrackingChanged;
#endif

                    AnchorDicAdd(args.Anchor.Identifier, _Index, _Obj);

                    if (m_AnchorDic.Count == 1)
                    {
                        OnFirstAnchorFound.Invoke();
                    }

                    SetMainAnchorLoacte();
                });

            }
        }

        private void AnchorDicAdd(string iKey, int iIndex, GameObject iAnchor)
        {
            if (m_AnchorDic.ContainsKey(iKey))
            {
                var _Anchor = m_AnchorDic[iKey];
                _Anchor.Obj = iAnchor;
                _Anchor.IsLocated = true;
            }
            else
            {
                var _LcaInfo = new AnchorLocatInfo()
                {
                    Obj = iAnchor,
                    IsLocated = true,
                    Offset = m_AnchorsLocalPosition[iIndex]
                };
                m_AnchorDic.Add(iKey, _LcaInfo);
            }
        }

        private bool m_IsLocat = false;

        private Coroutine m_ResetCountDown = null;

        private IEnumerator IEResetCountDown()
        {
            yield return new WaitForSecondsRealtime(10);
            ResetAnchorKeyAndRebootSession(m_AnchorStorageIndex);
        }

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS

        private void OnARAhcorTrackingChanged(ARAnchorsChangedEventArgs arg)
        {
            var _IsLocat = false;
            foreach (var _Anc in m_AnchorDic)
            {
                var _Info = _Anc.Value;
                var _Anchor = arg.updated.Find((_Update) => _Update.gameObject == _Info.Obj);

                //check key null
                if (_Anchor == null)
                {
                    continue;
                }

                var _IsTracking = _Anchor.trackingState == TrackingState.Tracking;

                if (_IsTracking)
                {
                    _IsLocat = true;
                }

                if (_Info.IsLocated != _IsTracking)
                {
                    _Info.IsLocated = _IsTracking;
                    SetText($"Tracking Changed: {_Anc.Key}, Index: {IndexCheck(_Anc.Key)}, State: {_Anchor.trackingState}");
                }
            }

            if (m_IsLocat != _IsLocat)
            {
                m_IsLocat = _IsLocat;
                if (m_IsLocat)
                {
                    SetText("AnchorChanged : OnTrackingBack");
                    OnTrackingBack.Invoke();
                }
                else
                {
                    SetText("AnchorChanged: OnTrackingLost");
                    OnTrackingLost.Invoke();
                }
                SetMainAnchorLoacte();
            }
        }

        private void ARCheckSession()
        {
            var _IsTracking = ARSession.state == ARSessionState.SessionTracking;
            if (m_ARSessionStateIsTracking != _IsTracking)
            {
                m_ARSessionStateIsTracking = _IsTracking;
                if (m_ARSessionStateIsTracking)
                {
                    SetText("StateChanged : ARSession Back");
                    if (m_ResetCountDown != null)
                    {
                        StopCoroutine(m_ResetCountDown);
                        m_ResetCountDown = null;
                    }
                }
                else
                {
                    SetText("StateChanged : ARSession Lost");
                    m_ResetCountDown = StartCoroutine(IEResetCountDown());
                }
            }
        }

#elif UNITY_WSA

        private void OnWorldAnchorTrackingChanged(WorldAnchor worldAnchor, bool located)
        {
            var _IsLocat = false;
            foreach (var _Anc in m_AnchorDic)
            {
                var _Info = _Anc.Value;

                if (_Info.Obj.GetComponent<CloudNativeAnchor>().NativeAnchor == worldAnchor)
                {
                    _Info.IsLocated = located;
                    SetText($"Tracking Changed: {_Anc.Key}, Located: {located}");
                }

                if (_Info.IsLocated)
                    _IsLocat = true;

            }

            if (m_IsLocat != _IsLocat)
            {
                m_IsLocat = _IsLocat;
                if (m_IsLocat)
                {
                    SetText("AnchorChanged : OnTrackingBack");
                    OnTrackingBack.Invoke();
                }
                else
                {
                    SetText("AnchorChanged: OnTrackingLost");
                    OnTrackingLost.Invoke();
                }
            }

            SetMainAnchorLoacte();
        }

        private void OnWorldAnchorPositionalLocatorStateChanged(PositionalLocatorState oldState, PositionalLocatorState newState)
        {
            SetText($"StateChanged : WorldAnchorSession Sate: {newState}");
            if (newState == PositionalLocatorState.Active)
            {
                SetText("StateChanged : WorldAnchorSssion Back");
                if (m_ResetCountDown != null)
                {
                    StopCoroutine(m_ResetCountDown);
                    m_ResetCountDown = null;
                }
            }
            else
            {
                SetText("StateChanged : WorldAnchorSssion Lost");
                m_ResetCountDown = StartCoroutine(IEResetCountDown());
            }
        }

#endif

        public async void StartSession(int iIndex, bool iCreatSession)
        {
            if (m_SpatialAnchorManager.IsSessionStarted)
                return;

            if (iCreatSession)
            {
                SetText("Creating Session.");
                await m_SpatialAnchorManager.CreateSessionAsync();
                SetText("Starting Session.");
            }
            await m_SpatialAnchorManager.StartSessionAsync();
            SetText("Started Session.");
            OnStartSession.Invoke();

            SetAnchorKeys(iIndex);
            m_Criteria.Identifiers = new string[0];
            m_Criteria.Identifiers = m_AnchorKeys.ToArray();

            m_SpatialAnchorManager.Error += OnAnchorError;

            m_CurrentWathcer = m_SpatialAnchorManager.Session.CreateWatcher(m_Criteria);
            SetText("Watecher started...");

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
            m_ARAnchorManager.anchorsChanged += OnARAhcorTrackingChanged;
#endif
        }

        public async void StopSession()
        {
            m_CurrentWathcer?.Stop();
            m_SpatialAnchorManager.Error -= OnAnchorError;

            m_SpatialAnchorManager.StopSession();
            await m_SpatialAnchorManager.ResetSessionAsync();
            SetText("Stopped Session");
#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS
            m_ARAnchorManager.anchorsChanged -= OnARAhcorTrackingChanged;
#endif
        }

        private void SetText(string _msg)
        {
            OnLogInvoke.Invoke(_msg);
            Debug.Log($"[AnchorFinder] {_msg}");
        }

        /// <summary>
        /// return index
        /// </summary>
        /// <returns></returns>
        public int AddKeyStorage(AnchorKeysStorage _storage)
        {
            m_AnchorStorages.Add(_storage);
            return m_AnchorStorages.Count - 1;
        }

        private void ClearAnchors()
        {

#if (UNITY_WSA && UNITY_2020_1_OR_NEWER) || UNITY_ANDROID || UNITY_IOS

            var _Anchors = FindObjectsOfType<ARAnchor>();
            var _AnchorCount = _Anchors.Length;
            SetText("ClearAllAnchors : " + _AnchorCount);
            for (int i = _AnchorCount - 1; i <= 0; i--)
            {
                if (_Anchors[i] != null)
                    Destroy(_Anchors[i].gameObject);
            }

#elif UNITY_WSA

            foreach (var _item in m_AnchorDic)
            {
                var _NativeAnchor = _item.Value.Obj.GetComponent<CloudNativeAnchor>();
                if (_NativeAnchor != null)
                {
                    _NativeAnchor.NativeAnchor.OnTrackingChanged -= OnWorldAnchorTrackingChanged;
                    Destroy(_NativeAnchor.gameObject);
                }
            }

#endif
            m_AnchorDic.Clear();
            OnAnchorsClear.Invoke();
        }

        /// <summary>
        /// Index from list m_AnchorStorage
        /// </summary>
        /// <param name="iAnchorStorageIndex"></param>
        public void ResetAnchorKeyAndRebootSession(int iAnchorStorageIndex)
        {
            m_AnchorStorageIndex = iAnchorStorageIndex;
            OnTrackingLost.Invoke();
            ClearAnchors();

            if (m_Criteria == null)
                return;

            StopSession();
            StartSession(m_AnchorStorageIndex, false);
        }
        private void OnAnchorError(object iSender, SessionErrorEventArgs iArgs)
        {
            SetText($"OnAnchorError : Type {iArgs.ErrorCode} , Msg {iArgs.ErrorMessage}");
        }
    }
}