﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EyeMovement_URP : MonoBehaviour
{

    [SerializeField] Material targetMaterial = null;

    Vector2 offset = new Vector2();

    // Start is called before the first frame update
    //void Start()
    //{
        
    //}

    // Update is called once per frame
    void Update()
    {
        offset.x = transform.rotation.eulerAngles.x;
        offset.y = transform.rotation.eulerAngles.y;

        targetMaterial.SetTextureOffset("_BaseMap", offset);
    }
}
