using RenderHeads.Media.AVProVideo;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class VideoPlayer : MonoBehaviour
{
    [SerializeField]
    private MediaPlayer m_GroundPlayer = null;
    [SerializeField]
    private MediaPlayer m_WallPlayer = null;
    [SerializeField]
    private Renderer m_GroundRender = null;
    [SerializeField]
    private Renderer[] m_WallRenders = null;

    [SerializeField]
    private bool m_AutoPlay = false;
    [SerializeField]
    private bool m_Loop = false;

    [SerializeField, Range(0, 0.2f)]
    private float m_LoopEndTimeCheck = 0.1f;

    private IEnumerator Start()
    {
        yield return null;
        while (!(m_WallPlayer.Control.CanPlay() && m_GroundPlayer.Control.CanPlay()))
        {
            yield return null;
        }
        m_WallPlayer.Control.SetLooping(false);
        m_WallPlayer.Pause();
        m_WallPlayer.Stop();

        m_GroundPlayer.Control.SetLooping(false);
        m_GroundPlayer.Pause();
        m_GroundPlayer.Stop();

        if (m_AutoPlay)
        {
            m_WallPlayer.Play();
            m_GroundPlayer.Play();
        }
    }
    private void Update()
    {

        //Set Texture
        var _TexGround = m_GroundPlayer.TextureProducer.GetTexture();
        m_GroundRender.material.mainTexture = _TexGround;

        var _TexWall = m_WallPlayer.TextureProducer.GetTexture();
        var _Count = m_WallRenders.Length;
        for (int i = 0; i < _Count; i++)
        {
            m_WallRenders[i].material.mainTexture = _TexWall;
        }

        if (m_Loop)
        {
            if (GetTime() + m_LoopEndTimeCheck > GertDuration())
            {
                m_WallPlayer.Stop();
                m_GroundPlayer.Stop();
                m_WallPlayer.Rewind(false);
                m_GroundPlayer.Rewind(false);
                m_WallPlayer.Play();
                m_GroundPlayer.Play();
            }
        }
    }
    public float GertDuration()
    {
        return m_GroundPlayer.Info.GetDurationMs() * 0.001f;
    }
    public float GetTime()
    {
        return m_GroundPlayer.Control.GetCurrentTimeMs() * 0.001f;
    }
    public void SetTime(float iTime)
    {
        iTime *= 1000;
        m_WallPlayer.Control.Seek(iTime);
        m_GroundPlayer.Control.Seek(iTime);
    }

    public void Play()
    {
        m_WallPlayer.Play();
        m_GroundPlayer.Play();
    }
    public void Stop()
    {
        m_WallPlayer.Pause();
        m_WallPlayer.Stop();
        m_GroundPlayer.Pause();
        m_GroundPlayer.Stop();
    }
    public void Rewind()
    {
        m_WallPlayer.Rewind(true);
        m_GroundPlayer.Rewind(true);
    }
}
