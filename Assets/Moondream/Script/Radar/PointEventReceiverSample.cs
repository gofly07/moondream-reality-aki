﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Moonshine.Moondream.Net.NetTcpCommandRadar;

namespace Moonshine.Moondream.Pointevent {
    public class PointEventReceiverSample : MonoBehaviour {
        [SerializeField]
        private Vector2 PosScale = new Vector2(10, 5);
        public void OnReceivePoint(MrPoint iPoint) {
            if (iPoint.Event == EVENT.Update) {
                var _X = Mathf.Lerp(-PosScale.x, PosScale.x, iPoint.Pos.x);
                var _Y = Mathf.Lerp(-PosScale.y, PosScale.y, iPoint.Pos.y);
                transform.position = new Vector3(_X, _Y, 0);
            }
        }
    }
}