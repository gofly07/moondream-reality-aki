﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using UnityEngine;
using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpClient : NetTcpBase {

#pragma warning disable 0649
        [System.Serializable]
        private class ServerConfig {
            public string Ip;
            public int Port;
        }
#pragma warning restore 0649

        #region private members 	
        private TcpClient m_Client;
        private Thread m_ReceiveThread;
        #endregion
        [SerializeField]
        public string m_ServerIp = "127.0.0.1";

        public bool m_StartConnect = true;
        public bool m_AutoReconnect = true;
        [SerializeField]
        public MACHINE_TYPE m_MachineType = MACHINE_TYPE.None;
        public CONNECT_STATE State { get; private set; }
        public bool IsConnected { get { return State == CONNECT_STATE.Connected; } }

        private bool m_OnConnectStart = false;
        private bool m_OnConnectEnd = false;

        protected override void OnNetStart() {
            base.OnNetStart();
#if !UNITY_EDITOR && UNITY_STANDALONE_WIN
            var _FolderPath = Application.dataPath + "/Configs~";
            var _FilePath = _FolderPath + "/" + gameObject.name + "_ServerConfig.json";
            var _ServerConfig = new ServerConfig();
            _ServerConfig.Ip = m_ServerIp;
            _ServerConfig.Port = m_Port;

            if (!Directory.Exists(_FolderPath)) {
                Directory.CreateDirectory(_FolderPath);
            }
            if (!File.Exists(_FilePath)) {
                var _T = JsonUtility.ToJson(_ServerConfig, true);
                Debug.Log("[NetTcpClient]SaveText path :" + _FilePath);
                File.WriteAllText(_FilePath, _T);
            }
            Debug.Log("[NetTcpClient]Load path :" + _FilePath);
            var _Text = File.ReadAllText(_FilePath);
            _ServerConfig = JsonUtility.FromJson<ServerConfig>(_Text);
            m_ServerIp = _ServerConfig.Ip;
            m_Port = _ServerConfig.Port;
#endif

            State = CONNECT_STATE.DisConnect;
            if (m_StartConnect) {
                StartConnect();
            }
        }
        protected override void OnNetUpdate() {
            base.OnNetUpdate();
            if (m_OnConnectStart) {
                m_OnConnectStart = false;
                var _Cmd = new CommandMachineType() {
                    Key = COMMAND_KEY.MachineType,
                    Type = m_MachineType
                };
                Send(_Cmd);
                if (m_OnClientConnectStart != null) {
                    m_OnClientConnectStart.Invoke(m_Client);
                }
            }
            if (m_OnConnectEnd) {
                m_OnConnectEnd = false;
                if (m_OnClientConnectEnd != null) {
                    m_OnClientConnectEnd.Invoke(m_Client);
                }
            }
            TestSend();
        }
        protected override void OnNetDestroy() {
            base.OnNetDestroy();
            StopConnect();
        }


        private IEnumerator IEConnectChecker() {
            CloseConnection();
            StartConnection();

            var _ReconnectTimeStep = 5.0f;
            var _ReconnectTime = 0.0f;
            while (m_AutoReconnect) {
                _ReconnectTime += _ReconnectTimeStep;
                if (_ReconnectTime > 30) {
                    _ReconnectTime = 30;
                }
                yield return new WaitForSeconds(_ReconnectTime);
                if (m_Client == null || m_Client.Connected == false) {
                    Debug.Log("[NetTcpClient]IEAutoConnect : Try to reconnect");
                    CloseConnection();
                    StartConnection();
                }
                else {
                    _ReconnectTime = 0.0f;
                }
            }
        }
        private void StartConnection() {
            try {
                State = CONNECT_STATE.Connecting;
                m_ReceiveThread = new Thread(new ThreadStart(ListenForData));
                m_ReceiveThread.IsBackground = true;
                m_ReceiveThread.Start();
                Debug.Log("[NetTcpClient]StartConnection Ip : " + m_ServerIp + ", Port : " + m_Port);
            }
            catch (Exception iE) {
                State = CONNECT_STATE.DisConnect;
                Debug.Log("[NetTcpClient]StartConnection Exception : " + iE);
            }
        }
        private void CloseConnection() {
            if (m_ReceiveThread != null) {
                m_ReceiveThread.Abort();
            }
            if (m_Client != null && m_Client.Connected) {
                m_Client.Close();
            }
            m_ReceiveThread = null;
            m_Client = null;
            State = CONNECT_STATE.DisConnect;
            Debug.Log("[NetTcpClient]CloseConnection");
        }
        private void ListenForData() {
            try {
                m_Client = new TcpClient(m_ServerIp, m_Port);
                State = CONNECT_STATE.Connected;

                m_OnConnectStart = true;

                var _Bytes = new Byte[1024];
                while (true) {
                    if (!m_Client.Connected) {
                        State = CONNECT_STATE.DisConnect;
                        break;
                    }
                    using (NetworkStream _Stream = m_Client.GetStream()) {
                        int _Length;
                        while ((_Length = _Stream.Read(_Bytes, 0, _Bytes.Length)) != 0) {
                            var _Data = new byte[_Length];
                            Array.Copy(_Bytes, 0, _Data, 0, _Length);
                            string _Msg = Encoding.ASCII.GetString(_Data);
                            OnReceive(m_Client, _Msg);
                        }
                    }
                }
            }
            catch (ThreadAbortException) {
                State = CONNECT_STATE.DisConnect;
                m_OnConnectEnd = true;

                Debug.Log("[NetTcpClient]ListenForData ThreadEnd");
            }
            catch (SocketException iSe) {
                State = CONNECT_STATE.DisConnect;
                m_OnConnectEnd = true;

                Debug.LogError("[NetTcpClient]ListenForData SocketException : " + iSe);
            }
            catch (Exception iE) {
                State = CONNECT_STATE.DisConnect;
                m_OnConnectEnd = true;

                Debug.LogError("[NetTcpClient]ListenForData Exception : " + iE);
            }

        }
        public override void Send(Command iCmd) {
            base.Send(iCmd);
            if (m_Client == null) {
                return;
            }

            var _Msg = iCmd.ToMsg();
            try {
                NetworkStream _Stream = m_Client.GetStream();
                if (_Stream.CanWrite) {
                    byte[] _Data = Encoding.ASCII.GetBytes(_Msg);
                    _Stream.Write(_Data, 0, _Data.Length);
                }
            }
            catch (SocketException iSE) {
                Debug.LogError("[NetTcpClient]SendMessage SocketException : " + iSE);
            }
        }
        public void StartConnect() {
            StopAllCoroutines();
            StartCoroutine(IEConnectChecker());
        }
        public void StopConnect() {
            StopAllCoroutines();
            CloseConnection();
        }
        private void TestSend() {
            if (Input.GetKeyDown(KeyCode.C)) {
                var _Cmd = new CommandTest() {
                    Key = COMMAND_KEY.JustTest,
                    Msg = "OLAOLAOLAOLAOLAOLA"
                };
                Send(_Cmd);
            }
        }
    }
}
