﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpClientUI : MonoBehaviour {
        [SerializeField]
        private NetTcpClient m_Client = null;
        [Header("Connect")]
        [SerializeField]
        private Text m_ConnectState = null;
        [SerializeField]
        private InputField m_Input_Ip = null;
        [SerializeField]
        private InputField m_Input_Port = null;
        [SerializeField]
        private Toggle m_Input_AutoReonnect = null;
        [SerializeField]
        private Dropdown m_Input_MachineType = null;
        [SerializeField]
        private Button m_Btn_StartConnect = null;
        [SerializeField]
        private Button m_Btn_StopConnect = null;

        [Header("Send")]
        [SerializeField]
        private Dropdown m_Input_CommandKey = null;
        [SerializeField]
        private InputField m_Input_Data = null;
        [SerializeField]
        private Button m_Btn_SendData = null;
        [Header("Info")]
        [SerializeField]
        private Text m_LogInfo = null;

        private Queue<string> m_LogData = new Queue<string>();
        private void Awake() {
            foreach (var _Type in Enum.GetValues(typeof(COMMAND_KEY))) {
                var _Key = (COMMAND_KEY)_Type;
                m_Client.AddReceive(_Key, (_Val) => OnReceive(_Key, _Val));
            }

            var _MachineOptions = new List<Dropdown.OptionData>();
            var _MachineTypes = System.Enum.GetNames(typeof(MACHINE_TYPE));
            foreach (var _Type in _MachineTypes) {
                var _Option = new Dropdown.OptionData();
                _Option.text = _Type;
                _MachineOptions.Add(_Option);
            }
            m_Input_MachineType.ClearOptions();
            m_Input_MachineType.AddOptions(_MachineOptions);


            var _CmdOptions = new List<Dropdown.OptionData>();
            var _CmdTypes = System.Enum.GetNames(typeof(COMMAND_KEY));
            foreach (var _Type in _CmdTypes) {
                var _Option = new Dropdown.OptionData();
                _Option.text = _Type;
                _CmdOptions.Add(_Option);
            }
            m_Input_CommandKey.ClearOptions();
            m_Input_CommandKey.AddOptions(_CmdOptions);


            m_Input_MachineType.value = 2;
            m_Btn_StartConnect.onClick.AddListener(() => StartConnect());
            m_Btn_StopConnect.onClick.AddListener(() => StopConnect());
            m_Btn_SendData.onClick.AddListener(() => SendText());

        }
        private void Update() {
            LogText();
        }
        private void StartConnect() {
            m_Client.m_ServerIp = m_Input_Ip.text;
            m_Client.m_Port = int.Parse(m_Input_Port.text);
            m_Client.m_AutoReconnect = m_Input_AutoReonnect.isOn;
            m_Client.m_MachineType = (MACHINE_TYPE)m_Input_MachineType.value;
            m_Client.StartConnect();
        }
        private void StopConnect() {
            m_Client.StopConnect();
        }
        private void SendText() {
            var _Key = (COMMAND_KEY)Enum.Parse(typeof(COMMAND_KEY), m_Input_CommandKey.options[m_Input_CommandKey.value].text);
            var _Cmd = new CommandTest() {
                Key = _Key,
                Msg = m_Input_Data.text
            };
            m_Client.Send(_Cmd);
        }
        private void OnReceive(COMMAND_KEY iKey, string iData) {
            m_LogData.Enqueue(iKey.ToString() + " , " + iData);
        }
        private void LogText() {
            m_ConnectState.text = m_Client.State.ToString();
            while (m_LogData.Count > 30) {
                m_LogData.Dequeue();
            }
            var _LogText = "";
            foreach (var _Data in m_LogData) {
                _LogText += _Data + "\n";
            }
            m_LogInfo.text = _LogText;
        }
    }
}

