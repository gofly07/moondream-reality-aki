﻿using UnityEngine;
using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    [System.Serializable]
    public class NetTcpCommandRadar {
        [System.Serializable]
        public class MrPointEvent: UnityEvent<MrPoint>{}
        public enum EVENT {
            Add,
            Remove,
            Update,
        }
        [System.Serializable]
        public class MrPoint {
            public PROJECTION_TYPE Proj;
            public EVENT Event;
            public int Id;
            public float Rad;
            public Vector2 Pos;
        }
        [System.Serializable]
        public class MrPointInt {
            public PROJECTION_TYPE Proj;
            public EVENT Event;
            public int Id;
            public int Rad;
            public Vector2Int Pos;

            public MrPoint Get() {
                var _Point = new MrPoint() {
                    Proj = this.Proj,
                    Event = this.Event,
                    Id = this.Id,
                    Rad = ((float)this.Rad) / FloatMuti,
                    Pos = ((Vector2)this.Pos) / FloatMuti
                };
                return _Point;
            }
            public void Set(MrPoint iPoint) {
                Proj = iPoint.Proj;
                Event = iPoint.Event;
                Id = iPoint.Id;
                Rad = (int)(iPoint.Rad * FloatMuti);
                Pos = new Vector2Int((int)(iPoint.Pos.x * FloatMuti), (int)(iPoint.Pos.y * FloatMuti));
            }
            private int FloatMuti { get { return 10000; } }
        }
        [System.Serializable]
        public class ManagerInfo {
            public float MergeDistance;
            public float MergeSpreadBuffer;
            public float IgnoreRadMin;
            public float IgnoreRadMax;
        }
        [System.Serializable]
        public class ManagerInfoInt {
            public int MergeDistance;
            public int MergeSpreadBuffer;
            public int IgnoreRadMin;
            public int IgnoreRadMax;
            public ManagerInfo Get() {
                var _Info = new ManagerInfo() {
                    MergeDistance = ((float)this.MergeDistance) / FloatMuti,
                    MergeSpreadBuffer = ((float)this.MergeSpreadBuffer) / FloatMuti,
                    IgnoreRadMin = ((float)this.IgnoreRadMin) / FloatMuti,
                    IgnoreRadMax = ((float)this.IgnoreRadMax) / FloatMuti,
                };
                return _Info;
            }
            public void Set(ManagerInfo iInfo) {
                MergeDistance = (int)(iInfo.MergeDistance * FloatMuti);
                MergeSpreadBuffer = (int)(iInfo.MergeSpreadBuffer * FloatMuti);
                IgnoreRadMin = (int)(iInfo.IgnoreRadMin * FloatMuti);
                IgnoreRadMax = (int)(iInfo.IgnoreRadMax * FloatMuti);
            }
            private int FloatMuti { get { return 10000; } }
        }

        [System.Serializable]
        public class CommandMrPointInt : Command {
            public MrPointInt[] MrPoints;
        }
        [System.Serializable]
        public class CommandManagerInfoInt : Command {
            public ManagerInfoInt Info;
        }
    }

}
