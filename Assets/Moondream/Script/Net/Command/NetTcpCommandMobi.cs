﻿using UnityEngine;

namespace Moonshine.Moondream.Net {
    [System.Serializable]
    public class NetTcpCommandMobi {
        [System.Serializable]
        public class Locat {
            public bool IsLocat = false;
            public Vector3 Pos;
            public Vector3 Rot;
        }
        [System.Serializable]
        public class LocatInt {
            public LocatInt() { }
            public LocatInt(Locat iLo) {
                Set(iLo);
            }
            public bool IsLocat = false;
            public Vector3Int Pos;
            public Vector3Int Rot;
            private int FloatMuti { get { return 1000; } }
            public void Set(Locat iLo) {
                IsLocat = iLo.IsLocat;
                Pos = new Vector3Int((int)(iLo.Pos.x * FloatMuti), (int)(iLo.Pos.y * FloatMuti), (int)(iLo.Pos.z * FloatMuti));
                Rot = new Vector3Int((int)(iLo.Rot.x * FloatMuti), (int)(iLo.Rot.y * FloatMuti), (int)(iLo.Rot.z * FloatMuti));
            }
            public Locat Get() {
                var _Lo = new Locat() {
                    IsLocat = IsLocat,
                    Pos = ((Vector3)Pos) / FloatMuti,
                    Rot = ((Vector3)Rot) / FloatMuti
                };
                return _Lo;
            }
        }
        [System.Serializable]
        public class LocatData {
            public string Content;
            public LocatInt Location;
        }
        [System.Serializable]
        public class LocatInfo {
            public string Ip;
            public int LastUpdateTime;
            public LocatData Data;
        }

        [System.Serializable]
        public class CommandLocatInfo: Command {
            public LocatInfo[] Infos;
        }
        [System.Serializable]
        public class CommandLocatData : Command {
            public LocatData Data;
        }
    }
}
