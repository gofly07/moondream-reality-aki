﻿using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    [System.Serializable]
    public class Command {
        public COMMAND_KEY Key;
        public static string EndChar { get { return "@@@"; } }
        public string ToMsg() {
            return JsonUtility.ToJson(this) + EndChar;
        }
        public static T ToObj<T>(string iMsg) {
            try {
                var _EndLen = EndChar.Length;
                var _Msg = iMsg.Remove(iMsg.Length - _EndLen, _EndLen);
                return JsonUtility.FromJson<T>(_Msg);
            }
            catch {
                Debug.LogWarning("[NetTcpGlobal.Command]ToObj : Json ParseError : " + iMsg);
            }
            return default(T);
        }
    }

    [System.Serializable]
    public class CommandMachineType : Command {
        public MACHINE_TYPE Type;
    }

    [System.Serializable]
    public class CommandTest : Command {
        public string Msg;
    }
}