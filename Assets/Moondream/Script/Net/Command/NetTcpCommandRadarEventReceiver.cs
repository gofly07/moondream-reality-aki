﻿using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;
using static Moonshine.Moondream.Net.NetTcpCommandRadar;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandRadarEventReceiver : NetTcpCommandReceiver {
        [System.Serializable]
        public class PointEvent : UnityEvent<MrPoint> { }

        public PointEvent m_OnReceive = new PointEvent();
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.RadarEvent; } }
        protected override void OnReceive(string iData) {
            base.OnReceive(iData);
            var _Obj = Command.ToObj<CommandMrPointInt>(iData);
            if (_Obj != null) {
                for (int i = 0; i < _Obj.MrPoints.Length; i++) {
                    m_OnReceive.Invoke(_Obj.MrPoints[i].Get());
                }
            }
        }
    }
}