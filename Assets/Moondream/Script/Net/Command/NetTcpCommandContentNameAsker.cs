﻿using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandContentNameAsker : NetTcpCommandStartSender {
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.ContentName; } }
    }
}
