﻿using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandContentStatePriorAsker : NetTcpCommandStartSender {
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.ContentStatePrior; } }
    }

}
