﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandContentStateMinorSender : MonoBehaviour {
        [SerializeField]
        protected NetTcpBase m_Net = null;
        protected COMMAND_KEY m_CommandKey { get { return COMMAND_KEY.ContentStateMinor; } }
        public void Send() {
            var _Cmd = new NetTcpCommandContentState() {
                Key = m_CommandKey,
                State = CONTENT_GAME_STATE.EndLoop
            };
            m_Net.Send(_Cmd);
        }
    }
}