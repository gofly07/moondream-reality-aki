﻿using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    [System.Serializable]
    public class NetTcpCommandContentName : Command {
        public string Environment = ContentDefaultName;
        public string MoboleDevice = ContentDefaultName;
    }
}
