using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientReceiver : MonoBehaviour
{

    public List<GameObject> actors = new List<GameObject>();

    private void Start()
    {
        foreach (var i in actors)
            i.SetActive(false);

       // InvokeRepeating("FunctionReceive",0,38);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
            FunctionReceive();

        if (count)
        {
            timer += Time.deltaTime;
            if (timer >= 36)
            {
                count = false;
                foreach (var i in actors)
                    i.SetActive(false);
            }
        }
    }

    private float timer = 0;

    private bool count = false;

    public void FunctionReceive()
    {
        if (count)
            return;

        Debug.Log("<color=yellow>Received</color>");

        timer = 0;
        count = true;
        foreach (var i in actors)
            i.SetActive(true);
    }

}
