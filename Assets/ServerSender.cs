using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using System.IO;
public class ServerSender : MonoBehaviour
{
    [SerializeField] private VideoPlayer videoPlayer;
    [SerializeField] private Button btPlay;
    [SerializeField] private Button btStop;
    [SerializeField] private Text txtTime;
    [SerializeField] private Button btMR;

    private bool sw = true;
    private float time = 0;

    private void Start()
    {
        //Read the text from directly from the test.txt file
        StreamReader reader = new StreamReader(Application.streamingAssetsPath+ "/duration.txt");
        time = float.Parse(reader.ReadToEnd());
        reader.Close();

        sw = true;
        btPlay.onClick.AddListener(delegate { FunctionPlay(); });
        btStop.onClick.AddListener(delegate { FunctionStop(); });
        btMR.onClick.AddListener(delegate { FunctionMR(); });
    }

    public void FunctionPlay()
    {
        sw = true;
        videoPlayer.SetTime(0);
        videoPlayer.Play();
    }

    public void FunctionStop()
    {
        sw = true;
        videoPlayer.SetTime(0);
        videoPlayer.Stop();
    }

    private void Update()
    {
        txtTime.text = Math.Round(videoPlayer.GetTime(), 2).ToString() + "/" + Math.Round(videoPlayer.GertDuration(), 2).ToString();
        if (Math.Round(videoPlayer.GetTime(), 2) > time && sw)
        {
            FunctionMR();
            sw = false;
        }
    }

    public void FunctionMR()
    {
        Debug.Log("<color=lime>MR Play</color>");
        FMNetworkManager.instance.SendToOthers("Play");
    }
}
