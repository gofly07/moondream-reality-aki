﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonshine.Moondream.SpatialAnchors {
    public static class JsonConvert {
        public static string ToJson<T>(T iObj) {
            return JsonUtility.ToJson(iObj);
        }

        public static T ToObj<T>(string iJson) {
            return JsonUtility.FromJson<T>(iJson);
        }
        public static string ToArrayJson<T>(T[] iObj) {
            var _W = new Wrapper<T>() {
                Items = iObj
            };
            var _Json = JsonUtility.ToJson(_W, true);
            var _StartIndex = 14;//{/n \"Item\" :
            var _Length = _Json.Length;
            _Json =_Json.Substring(_StartIndex, _Length - _StartIndex -1);
            return _Json;
        }
        public static T[] ToArrayObj<T>(string iJson) {
            var _Json = "{ \"Items\": " + iJson + "}";
            var _W = JsonUtility.FromJson<Wrapper<T>>(_Json);
            return _W.Items;
        }

        [Serializable]
        private class Wrapper<T> {
            public T[] Items;
        }
    }
}
