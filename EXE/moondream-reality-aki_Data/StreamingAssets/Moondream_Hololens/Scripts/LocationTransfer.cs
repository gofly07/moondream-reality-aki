﻿using Moonshine.Moondream.Net;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonshine.Moondream.SpatialAnchors {
    public class LocationTransfer : MonoBehaviour {
        [SerializeField]
        AnchorFinderManager m_AnchorManager = null;
        [SerializeField]
        private Camera m_Camera = null;
        [SerializeField]
        private NetTcpCommandMobiLocatDataSender m_LocatSender = null;
        [SerializeField]
        private bool m_IsLocat = true;
        public bool IsLocat { get { return m_IsLocat; } }
        void Start() {
            m_IsLocat = false;
            m_AnchorManager.OnFirstAnchorFound.AddListener(() => m_IsLocat = true);
            m_AnchorManager.OnTrackingBack.AddListener(() => m_IsLocat = true);
            m_AnchorManager.OnTrackingLost.AddListener(() => m_IsLocat = false);
            StartCoroutine(IESend());
        }
        private IEnumerator IESend() {
            yield return null;
            while (true) {
                yield return new WaitForSeconds(0.2f);
                var _Center = m_AnchorManager.m_Anchor;
                var _Target = m_Camera.transform;
                var _Pos = _Center.InverseTransformDirection(_Target.position - _Center.position);
                var _Rot = (Quaternion.Inverse(_Center.rotation) * _Target.rotation).eulerAngles;

                m_LocatSender.m_Locat.IsLocat = m_IsLocat;
                m_LocatSender.m_Locat.Pos = _Pos;
                m_LocatSender.m_Locat.Rot = _Rot;
            }
        }
    }
}