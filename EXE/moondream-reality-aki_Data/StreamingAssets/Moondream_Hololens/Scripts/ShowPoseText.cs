﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Moonshine.Moondream.SpatialAnchors {
    public class ShowPoseText : MonoBehaviour {
        [SerializeField] 
        private TextMesh m_Text;
        private Transform m_Camera = null;

        private void Start() {
            m_Camera = Camera.main.transform;
        }
        void Update() {
            m_Text.transform.forward = (m_Text.transform.position - m_Camera.position).normalized;
            m_Text.text = transform.position.ToString() + "\r\n" + transform.rotation.eulerAngles.ToString();
        }
    }
}