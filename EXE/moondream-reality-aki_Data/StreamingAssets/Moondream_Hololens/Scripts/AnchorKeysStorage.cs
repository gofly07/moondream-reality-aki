﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonshine.Moondream.SpatialAnchors;
using Moonshine.Moondream.Utility;
#if UNITY_EDITOR
using UnityEditor;
using System.IO;
#endif
namespace Moonshine.Moondream.SpatialAnchors {
    [CreateAssetMenu(fileName = "AnchorKeysStorage", menuName = "Create AnchorKeysStorage")]
    public class AnchorKeysStorage : ScriptableObject {
        public List<string> AnchorKeys;
    }

#if UNITY_EDITOR
    [CustomEditor(typeof(AnchorKeysStorage))]
    public class AnchorKeysStorageInspector : Editor {
        private AnchorKeysStorage _storage;
        private string _filePath;
        private void OnEnable() {
            _storage = (AnchorKeysStorage)target;
            _filePath = EditorPrefs.GetString("LastAnchorKeyPath");
        }
        public override void OnInspectorGUI() {
            base.OnInspectorGUI();

            _filePath = EditorGUILayout.TextField(_filePath);
            if (GUILayout.Button("Select File")) {
                string _tempPath = EditorUtility.OpenFilePanel("選擇Anchor檔", _filePath, "txt");
                if (_tempPath != "")
                    _filePath = _tempPath;
                if (_filePath != "") {
                    EditorPrefs.SetString("LastAnchorKeyPath", _filePath);
                }
            }
            if (GUILayout.Button("Read File")) {
                if (_filePath == "")
                    return;
                if (!File.Exists(_filePath))
                    return;
                string _content = File.ReadAllText(_filePath);
                var _anchors = JsonConvert.ToArrayObj<Anchor>(_content);
                _storage.AnchorKeys = new List<string>();
                foreach (var item in _anchors) {
                    _storage.AnchorKeys.Add(item.AnchorKey);
                }
                EditorUtility.SetDirty(target);
            }
        }
    }
#endif
}