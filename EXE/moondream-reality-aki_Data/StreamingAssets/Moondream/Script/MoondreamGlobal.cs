﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonshine.Moondream {
    public class MoondreamGlobal {
        #region Machine
        [System.Serializable]
        public enum MACHINE_TYPE {
            None,
            SystemTool,
            SystemOperator,
            ContentServer,
            Projector,
            Hololens,
            IPad
        }
        [System.Serializable]
        public enum PROJECTION_TYPE {
            Wall_Front,
            Wall_Back,
            Wall_Left,
            Wall_Right,
            Ground,
        }
        #endregion

        #region Net
        [System.Serializable]
        public enum COMMAND_KEY {
            None,
            JustTest,
            MachineType,

            ContentName,
            ContentStatePrior,
            ContentStateMinor,

            OeratorBtnData,
            OeratorMsg,

            RadarManagerInfo,
            RadarEvent,

            MobiLocatData,
            MobiLocatInfo,


            Max = 9527,
        }
        [System.Serializable]
        public enum CONNECT_STATE {
            DisConnect,
            Connecting,
            Connected
        }
        #endregion

        #region Content
        public static string ContentDefaultName { get { return "moondream"; } }
        public static string ContentDefaultPath { get { return ""; } }

        [System.Serializable]
        public enum CONTENT_MACHINE_STATE {
            None,
            Environment,
            EnvironmentAndMobileDevice,
        }
        [System.Serializable]
        public enum CONTENT_GAME_TYPE {
            Loop,
            Show
        }
        [System.Serializable]
        public enum CONTENT_GAME_STATE {
            IdleLoop,
            Play,
            EndLoop
        }
        #endregion

    }

}
