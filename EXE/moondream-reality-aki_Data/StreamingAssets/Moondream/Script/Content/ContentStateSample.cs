﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Moonshine.Moondream.Net;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Utility {
    public class ContentStateSample : MonoBehaviour {
        [SerializeField]
        private NetTcpCommandContentStateMinorSender m_StateEndSender = null;
        [SerializeField]
        private GameObject m_IdleLoop = null;
        [SerializeField]
        private GameObject m_Play = null;
        [SerializeField]
        private GameObject m_EndLoop = null;
        private void Start() {
            SwitchOn(m_IdleLoop);
        }
        public void OnChangeState(CONTENT_GAME_STATE iState) {
            switch (iState) {
                case CONTENT_GAME_STATE.IdleLoop:
                    Debug.Log("[ContentStateSample]OnChangeState : IdleLoop");
                    StopAllCoroutines();
                    SwitchOn(m_IdleLoop);
                    break;
                case CONTENT_GAME_STATE.Play:
                    Debug.Log("[ContentStateSample]OnChangeState : Play");
                    StopAllCoroutines();
                    StartCoroutine(IEPlay());
                    break;
                case CONTENT_GAME_STATE.EndLoop:
                    Debug.Log("[ContentStateSample]OnChangeState : End");
                    StopAllCoroutines();
                    SwitchOn(m_EndLoop);
                    break;
            }
        }
        private void SwitchOn(GameObject iObj) {
            m_IdleLoop.SetActive(false);
            m_Play.SetActive(false);
            m_EndLoop.SetActive(false);

            iObj.SetActive(true);
        }       
        private IEnumerator IEPlay() {
            SwitchOn(m_Play);
            yield return new WaitForSeconds(5);
            SwitchOn(m_EndLoop);
            m_StateEndSender.Send();
            Debug.Log("[ContentStateSample]Play To End");
        }
    }

}
