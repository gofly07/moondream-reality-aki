﻿using System.Collections;
using System.Collections.Generic;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Text;
using UnityEngine;
using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpBase : MonoBehaviour {

        private object m_SynkObj = new object();

        public UnityAction<TcpClient> m_OnClientConnectStart;
        public UnityAction<TcpClient> m_OnClientConnectEnd;

        private List<TcpClient> m_ReceiveClients = new List<TcpClient>();
        private Dictionary<TcpClient, StringBuilder> m_ReceiveMsg = new Dictionary<TcpClient, StringBuilder>();

        private Dictionary<COMMAND_KEY, UnityAction<string>> m_OnReceiveCmdKey = new Dictionary<COMMAND_KEY, UnityAction<string>>();
        private Dictionary<COMMAND_KEY, UnityAction<TcpClient, string>> m_OnReceiveCmdKeyClient = new Dictionary<COMMAND_KEY, UnityAction<TcpClient, string>>();

        [SerializeField]
        protected bool m_LogCommand = true;
        private string m_ObjName;

        [SerializeField]
        public int m_Port = 9527;
        [SerializeField]
        public string m_DeviceSelfIp = "127.0.0.1";

        public virtual void Send(Command iCmd) {
            if (m_LogCommand) {
                Debug.Log("[NetTcp] " + m_ObjName + " Send : " + iCmd.ToMsg());
            }
        }
        protected void OnReceive(TcpClient iClient, string _Message) {
            if (m_LogCommand) {
                Debug.Log("[NetTcp] " + m_ObjName + " OnReceive: " + _Message);
            }

            lock (m_SynkObj) {
                if (m_ReceiveMsg.ContainsKey(iClient) == false) {
                    var _Sb = new StringBuilder(65535);
                    m_ReceiveMsg.Add(iClient, _Sb);
                    m_ReceiveClients.Add(iClient);
                }
                m_ReceiveMsg[iClient].Append(_Message);
            }

        }
        protected void OnRemove(TcpClient iClient) {
            lock (m_SynkObj) {
                m_ReceiveClients.Remove(iClient);
                m_ReceiveMsg.Remove(iClient);
            }
        }
        private void Start() {
            m_ObjName = name;
            OnNetStart();
        }
        private void Update() {
            OnNetUpdate();
        }
        private void OnDestroy() {
            OnNetDestroy();
        }
        protected virtual void OnNetStart() {
            if (string.IsNullOrEmpty(m_DeviceSelfIp) || string.Equals("127.0.0.1", m_DeviceSelfIp)) {
                m_DeviceSelfIp = GetSelfIP(ADDRESSFAM.IPv4);
            }
            Debug.Log("[NetTcp]DeviceSelfIp : " + m_DeviceSelfIp + ", Port : " + m_Port);
        }
        protected virtual void OnNetUpdate() {
            TcpClientReceiveProcess();
        }
        protected virtual void OnNetDestroy() {
            m_OnReceiveCmdKey.Clear();
            m_OnReceiveCmdKeyClient.Clear();

            lock (m_SynkObj) {
                m_ReceiveMsg.Clear();
                m_ReceiveClients.Clear();
            }
        }

        private void TcpClientReceiveProcess() {
            lock (m_SynkObj) {

                for (int i = 0; i < m_ReceiveClients.Count; i++) {
                    var _Client = m_ReceiveClients[i];
                    var _StringBuilder = m_ReceiveMsg[_Client];
                    var _EndCharCount = Command.EndChar.Length;
                    var _ReceiveMsg = _StringBuilder.ToString();
                    var _Index = _ReceiveMsg.LastIndexOf(Command.EndChar);
                    if (_Index < _EndCharCount) {
                        continue;
                    }
                    _StringBuilder.Remove(0, _Index + _EndCharCount);
                    var _Msgs = _ReceiveMsg.Substring(0, _Index + _EndCharCount).Split(
                            new string[] { Command.EndChar },
                            System.StringSplitOptions.RemoveEmptyEntries
                        );
                    for (int j = 0; j < _Msgs.Length; j++) {
                        ParseJsonCommand(_Client, _Msgs[j] + Command.EndChar);
                    }
                }

            }

        }
        private void ParseJsonCommand(TcpClient iClient, string iMsg) {
            var _Command = Command.ToObj<Command>(iMsg);
            if (_Command != null) {
                var _CommandKey = _Command.Key;
                if (m_OnReceiveCmdKey.ContainsKey(_CommandKey)) {
                    m_OnReceiveCmdKey[_CommandKey].Invoke(iMsg);
                }
                if (m_OnReceiveCmdKeyClient.ContainsKey(_CommandKey)) {
                    m_OnReceiveCmdKeyClient[_CommandKey].Invoke(iClient, iMsg);
                }
            }
            else {
                Debug.LogError("[NetTcp]Cmd Json Parse Error : " + iMsg);
            }
        }
        public void AddReceive(COMMAND_KEY _Command, UnityAction<string> _Receive) {
            if (m_OnReceiveCmdKey.ContainsKey(_Command)) {
                m_OnReceiveCmdKey[_Command] += _Receive;
            }
            else {
                m_OnReceiveCmdKey.Add(_Command, _Receive);
            }
        }
        public void AddReceive(COMMAND_KEY _Command, UnityAction<TcpClient, string> _Receive) {
            if (m_OnReceiveCmdKeyClient.ContainsKey(_Command)) {
                m_OnReceiveCmdKeyClient[_Command] += _Receive;
            }
            else {
                m_OnReceiveCmdKeyClient.Add(_Command, _Receive);
            }
        }
        public void RemoveReceive(COMMAND_KEY _Command, UnityAction<string> _Receive) {
            if (m_OnReceiveCmdKey.ContainsKey(_Command)) {
                m_OnReceiveCmdKey[_Command] -= _Receive;
                if (m_OnReceiveCmdKey[_Command] == null) {
                    m_OnReceiveCmdKey.Remove(_Command);
                }
            }
        }
        public void RemoveReceive(COMMAND_KEY _Command, UnityAction<TcpClient, string> _Receive) {
            if (m_OnReceiveCmdKeyClient.ContainsKey(_Command)) {
                m_OnReceiveCmdKeyClient[_Command] -= _Receive;
                if (m_OnReceiveCmdKeyClient[_Command] == null) {
                    m_OnReceiveCmdKeyClient.Remove(_Command);
                }
            }
        }
        #region IpManager
        public enum ADDRESSFAM {
            IPv4,
            IPv6
        }
        public static string GetSelfIP(ADDRESSFAM iAddfam) {
            //Return null if ADDRESSFAM is Ipv6 but Os does not support it
            if (iAddfam == ADDRESSFAM.IPv6 && !Socket.OSSupportsIPv6) {
                return null;
            }

            string output = "";

            foreach (NetworkInterface item in NetworkInterface.GetAllNetworkInterfaces()) {
#if UNITY_EDITOR_WIN || UNITY_STANDALONE_WIN
                NetworkInterfaceType _type1 = NetworkInterfaceType.Wireless80211;
                NetworkInterfaceType _type2 = NetworkInterfaceType.Ethernet;

                if ((item.NetworkInterfaceType == _type1 || item.NetworkInterfaceType == _type2) && item.OperationalStatus == OperationalStatus.Up)
#endif
            {
                    foreach (UnicastIPAddressInformation ip in item.GetIPProperties().UnicastAddresses) {
                        //IPv4
                        if (iAddfam == ADDRESSFAM.IPv4) {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetwork) {
                                output = ip.Address.ToString();
                            }
                        }

                        //IPv6
                        else if (iAddfam == ADDRESSFAM.IPv6) {
                            if (ip.Address.AddressFamily == AddressFamily.InterNetworkV6) {
                                output = ip.Address.ToString();
                            }
                        }
                    }
                }
            }
            return output;
        }
        #endregion
    }
}
