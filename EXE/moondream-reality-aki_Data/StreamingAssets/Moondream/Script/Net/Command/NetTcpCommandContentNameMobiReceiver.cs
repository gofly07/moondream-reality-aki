﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net
{
    public class NetTcpCommandContentNameMobiReceiver : NetTcpCommandReceiver
    {
        [SerializeField]
        private string m_ContentName = "custom";
        public string ContentName { get { return m_ContentName; } }
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.ContentName; } }
        protected override void OnReceive(string iData)
        {
            base.OnReceive(iData);
            var _Content = Command.ToObj<NetTcpCommandContentName>(iData);
            if (_Content != null)
            {
                var _ContentName = _Content.MoboleDevice;
                if (!string.Equals(m_ContentName, _ContentName))
                {
                    Debug.Log("[NetTcpCommandContentNameMobiReceiver]OnReceive Launch : " + _ContentName);
                    Launch(_ContentName);
                    StopAllCoroutines();
                    StartCoroutine(Quit());
                }
            }
        }
        public void LaunchMoondream()
        {
            Launch("moondream");
            StopAllCoroutines();
            StartCoroutine(Quit());
        }
        private void Launch(string iUri)
        {
            iUri += "://";
#if WINDOWS_UWP
            UnityEngine.WSA.Application.InvokeOnUIThread(async () =>
            {
                bool result = await global::Windows.System.Launcher.LaunchUriAsync(new System.Uri(iUri));
                if (!result)
                {
                    Debug.LogError("[NetTcpCommandContentNameMobiReceiver]Launching URI failed.");
                }
            }, false);

            Debug.Log("[NetTcpCommandContentNameMobiReceiver]Launch : " + iUri);
#endif
        }
        private IEnumerator Quit()
        {
            //for hololens restart
#if WINDOWS_UWP
            Debug.Log("[NetTcpCommandContentNameMobiReceiver]Wait For Qiut");
            yield return new WaitForSeconds(10);
            Debug.Log("[NetTcpCommandContentNameMobiReceiver]Qiut");
            Application.Quit();
#endif
            yield break;
        }
    }
}
