﻿using UnityEngine;
using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandContentStatePriorReceiver : NetTcpCommandReceiver {
        [System.Serializable]
        public class ContentStateEvent : UnityEvent<CONTENT_GAME_STATE> { }

        public ContentStateEvent m_OnReceive = new ContentStateEvent();
        public CONTENT_GAME_STATE GameState { get; private set; }
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.ContentStatePrior; } }
        protected override void OnReceive(string iData) {
            var _Obj =Command.ToObj< NetTcpCommandContentState>(iData);
            if (_Obj != null) {
                m_OnReceive.Invoke(_Obj.State);
                GameState = _Obj.State;
            }
        }
    }
}