﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandReceiver : MonoBehaviour {
        [SerializeField]
        protected NetTcpBase m_Net = null;
        protected virtual COMMAND_KEY CommandKey { get { return COMMAND_KEY.None; } }
        protected virtual void OnStart() { }
        protected virtual void OnReceive(string iData) { }
        private void Start() {
            m_Net.AddReceive(CommandKey, OnReceive);
            OnStart();
        }
        private void OnDestroy() {
            m_Net.RemoveReceive(CommandKey, OnReceive);
        }
    }
}
