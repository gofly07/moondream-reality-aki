﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandLoopSender : MonoBehaviour {
        [SerializeField]
        protected NetTcpBase m_Net = null;
        protected virtual float WaitTime { get { return 3; } }
        protected virtual COMMAND_KEY CommandKey { get { return COMMAND_KEY.None; } }
        private void Start() {
            StartCoroutine(IEAutoSend());
        }
        private IEnumerator IEAutoSend() {
            while (true) {
                yield return new WaitForSeconds(WaitTime);
                Send();
            }
        }
        protected virtual Command GetCmd() {
            return null;
        }
        public void Send() {
            m_Net.Send(GetCmd());
        }
    }

}
