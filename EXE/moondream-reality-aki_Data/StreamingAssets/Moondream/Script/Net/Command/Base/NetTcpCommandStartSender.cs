﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandStartSender : MonoBehaviour {
        [SerializeField]
        protected NetTcpBase m_Net = null;

        protected virtual COMMAND_KEY CommandKey { get { return COMMAND_KEY.None; } }
        public void Send() {
            var _Cmd = new Command() {
                Key = CommandKey
            };
            m_Net.Send(_Cmd);
        }
        private void Start() {
            m_Net.m_OnClientConnectStart += (_Client) => Send();
        }
        private void OnDestroy() {
            if (m_Net!= null) {
                m_Net.m_OnClientConnectStart -= (_Client) => Send();
            }
        }
    }
}