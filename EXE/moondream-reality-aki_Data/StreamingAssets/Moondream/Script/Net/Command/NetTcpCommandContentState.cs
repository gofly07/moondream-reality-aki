﻿using Moonshine.Moondream.Net;
using static Moonshine.Moondream.MoondreamGlobal;

[System.Serializable]
public class NetTcpCommandContentState : Command {
    public CONTENT_GAME_STATE State;
}
