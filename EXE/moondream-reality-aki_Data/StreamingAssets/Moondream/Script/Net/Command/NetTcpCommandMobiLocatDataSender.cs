﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using static Moonshine.Moondream.MoondreamGlobal;
using static Moonshine.Moondream.Net.NetTcpCommandMobi;

namespace Moonshine.Moondream.Net {
    public class NetTcpCommandMobiLocatDataSender : NetTcpCommandLoopSender {
        protected override float WaitTime { get { return 1; } }
        protected override COMMAND_KEY CommandKey { get { return COMMAND_KEY.MobiLocatData; } }
        [SerializeField]
        protected NetTcpCommandContentNameMobiReceiver m_ContentReceiver = null;
        public Locat m_Locat = new Locat() {
            IsLocat = false,
            Pos = Vector3.zero,
            Rot = Vector3.zero
        };
        protected override Command GetCmd() {
            var _Locat = new LocatInt(m_Locat);
            var _Data = new LocatData() {
                Content = m_ContentReceiver.ContentName,
                Location = _Locat
            };
            var _Cmd = new CommandLocatData() {
                Key = CommandKey,
                Data = _Data
            };
            return _Cmd;
        }
    }

}
