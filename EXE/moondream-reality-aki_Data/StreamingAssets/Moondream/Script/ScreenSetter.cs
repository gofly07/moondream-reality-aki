﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Moonshine.Moondream.Utility {
    public class ScreenSetter : MonoBehaviour {
        [SerializeField]
        private Vector2Int m_Res = new Vector2Int(1280, 720);
        [SerializeField]
        private bool m_FullScreen = false;
        void Start() {
            Screen.SetResolution(m_Res.x, m_Res.y, m_FullScreen);
        }
    }
}

