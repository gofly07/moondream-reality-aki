﻿using System.IO;
using UnityEngine;
using Moonshine.Moondream.Net;
using static Moonshine.Moondream.Net.NetTcpCommandRadar;
using static Moonshine.Moondream.MoondreamGlobal;
using System.Collections.Generic;
using System.Collections;

namespace Moonshine.Moondream.Pointevent {
    public class PointEventManagerNetTcpReceiver : MonoBehaviour {
        [System.Serializable]
        public class MrPointOption {
            public MrPoint Point;
            public float LastUpdateTime;
        }

        [System.Serializable]
        public class RadarPointEventToEventType {
            public MrPointEvent Add = new MrPointEvent();
            public MrPointEvent Remove = new MrPointEvent();
            public MrPointEvent Update = new MrPointEvent();
            public void Invoke(MrPoint iPoint) {
                switch (iPoint.Event) {
                    case EVENT.Add:
                        Add.Invoke(iPoint);
                        break;
                    case EVENT.Remove:
                        Remove.Invoke(iPoint);
                        break;
                    case EVENT.Update:
                        Update.Invoke(iPoint);
                        break;
                }
            }
        }

        [System.Serializable]
        public class RadarPointEventToProjection {
            public MrPointEvent Wall_Front = new MrPointEvent();
            public MrPointEvent Wall_Back = new MrPointEvent();
            public MrPointEvent Wall_Left = new MrPointEvent();
            public MrPointEvent Wall_Right = new MrPointEvent();
            public MrPointEvent Ground = new MrPointEvent();
            public void Invoke(MrPoint iPoint) {
                switch (iPoint.Proj) {
                    case PROJECTION_TYPE.Wall_Front:
                        Wall_Front.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Back:
                        Wall_Back.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Left:
                        Wall_Left.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Right:
                        Wall_Right.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Ground:
                        Ground.Invoke(iPoint);
                        break;
                }
            }
        }

        [System.Serializable]
        public class RadarPointEventAllApart {
            public RadarPointEventToEventType Wall_Front = new RadarPointEventToEventType();
            public RadarPointEventToEventType Wall_Back = new RadarPointEventToEventType();
            public RadarPointEventToEventType Wall_Left = new RadarPointEventToEventType();
            public RadarPointEventToEventType Wall_Right = new RadarPointEventToEventType();
            public RadarPointEventToEventType Ground = new RadarPointEventToEventType();
            public void Invoke(MrPoint iPoint) {
                switch (iPoint.Proj) {
                    case PROJECTION_TYPE.Wall_Front:
                        Wall_Front.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Back:
                        Wall_Back.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Left:
                        Wall_Left.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Wall_Right:
                        Wall_Right.Invoke(iPoint);
                        break;
                    case PROJECTION_TYPE.Ground:
                        Ground.Invoke(iPoint);
                        break;
                }
            }
        }
        [SerializeField]
        private NetTcpBase m_Net = null;
        [SerializeField]
        private NetTcpCommandRadarEventReceiver m_PointReceiver = null;


        [Header("現場雷達點位計算參數，若非必要請勿更動")]
        [SerializeField, Range(0, 1000)]
        private float m_MergeDistance = 20;
        [SerializeField, Range(1, 2)]
        private float m_MergeSpreadBuffer = 1.25f;
        [SerializeField, Range(0, 100)]
        private float m_IgnoreRadMin = 0;
        [SerializeField, Range(1, 5000)]
        private float m_IgnoreRadMax = 1000;

        private Dictionary<int, MrPointOption> m_PointOptions = new Dictionary<int, MrPointOption>();

        [SerializeField, Header("Event_All")]
        public MrPointEvent Event_All = new MrPointEvent();

        public RadarPointEventToEventType Event_Type = new RadarPointEventToEventType();
        public RadarPointEventToProjection Event_Projection = new RadarPointEventToProjection();
        public RadarPointEventAllApart Event_AllApart = new RadarPointEventAllApart();

        private void Awake() {
            m_PointReceiver.m_OnReceive.AddListener(OnPointEvent);
            m_Net.m_OnClientConnectStart += (_Client) => SendManagerInfo();
            StartCoroutine(CheckRemove());
        }
        private void SendManagerInfo() {
            var _ManagerInfo = new ManagerInfo() {
                MergeDistance = m_MergeDistance,
                MergeSpreadBuffer = m_MergeSpreadBuffer,
                IgnoreRadMin = m_IgnoreRadMin,
                IgnoreRadMax = m_IgnoreRadMax,
            };

#if !UNITY_EDITOR && UNITY_STANDALONE_WIN
            var _FolderPath = Application.dataPath + "/Configs~";
            var _FilePath = _FolderPath + "/" + "PointEventManagerInfoConfig.json";

            if (!Directory.Exists(_FolderPath)) {
                Directory.CreateDirectory(_FolderPath);
            }
            if (!File.Exists(_FilePath)) {
                var _T = JsonUtility.ToJson(_ManagerInfo, true);
                Debug.Log("[PointEventManagerNetTcpReceiver]SaveText path :" + _FilePath);
                File.WriteAllText(_FilePath, _T);
            }
            Debug.Log("[PointEventManagerNetTcpReceiver]Load path :" + _FilePath);
            var _Text = File.ReadAllText(_FilePath);
            _ManagerInfo = JsonUtility.FromJson<ManagerInfo>(_Text);
#endif

            var _ManagerIntInfo = new ManagerInfoInt();
            _ManagerIntInfo.Set(_ManagerInfo);
            var _Cmd = new CommandManagerInfoInt() {
                Key = COMMAND_KEY.RadarManagerInfo,
                Info = _ManagerIntInfo
            };
            m_Net.Send(_Cmd);
        }
        private void EventInvoke(MrPoint iPoint) {
            Event_All.Invoke(iPoint);
            Event_Type.Invoke(iPoint);
            Event_Projection.Invoke(iPoint);
            Event_AllApart.Invoke(iPoint);
        }
        private void OnPointEvent(MrPoint iPoint) {
            var _Contain = false;
            var _Time = Time.time;
            switch (iPoint.Event) {
                case EVENT.Add:
                    if (!m_PointOptions.ContainsKey(iPoint.Id)) {
                        var _Option = new MrPointOption() {
                            Point = iPoint,
                            LastUpdateTime = _Time
                        };
                        m_PointOptions.Add(iPoint.Id, _Option);
                        _Contain = true;
                    }
                    break;
                case EVENT.Update:
                    if (m_PointOptions.ContainsKey(iPoint.Id)) {
                        m_PointOptions[iPoint.Id].Point = iPoint;
                        m_PointOptions[iPoint.Id].LastUpdateTime = _Time;
                    }
                    else {
                        iPoint.Event = EVENT.Add;
                        var _Option = new MrPointOption() {
                            Point = iPoint,
                            LastUpdateTime = _Time
                        };
                        m_PointOptions.Add(iPoint.Id, _Option);
                    }
                    _Contain = true;
                    break;
                case EVENT.Remove:
                    if (m_PointOptions.ContainsKey(iPoint.Id)) {
                        m_PointOptions.Remove(iPoint.Id);
                        _Contain = true;
                    }
                    break;
            }
            if (_Contain) {
                EventInvoke(iPoint);
            }
        }
        
        private IEnumerator CheckRemove() {
            while (true) {
                yield return new WaitForSeconds(1);
                var _RemovePoints = new List<int>();
                foreach (var _Option in m_PointOptions) {
                    if (_Option.Value.LastUpdateTime + 1 < Time.time) {
                        _RemovePoints.Add(_Option.Key);
                    }
                }
                _RemovePoints.ForEach((_R) => {
                    var _Point = m_PointOptions[_R].Point;
                    EventInvoke(_Point);
                    m_PointOptions.Remove(_R);
                });
            }
        }
    }
}