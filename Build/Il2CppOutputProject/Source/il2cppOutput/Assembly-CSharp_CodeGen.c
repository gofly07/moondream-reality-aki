﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void ClientReceiver::Start()
extern void ClientReceiver_Start_mEC9E753BB87B3FDD745D526D922050911AB6C3C0 (void);
// 0x00000002 System.Void ClientReceiver::Update()
extern void ClientReceiver_Update_m17173BA23A7665ACA1B16B90A02D07E6DDE2BE04 (void);
// 0x00000003 System.Void ClientReceiver::FunctionReceive()
extern void ClientReceiver_FunctionReceive_m80D4988487FA6B5B1FBE3969C26D324B2884241E (void);
// 0x00000004 System.Void ClientReceiver::.ctor()
extern void ClientReceiver__ctor_m02241980F08639822CE42315EEBDCFFD24AA9D4F (void);
// 0x00000005 System.Void UnityEventFloat::.ctor()
extern void UnityEventFloat__ctor_mC7D6C9C18641A39D7C257210EF668C341794E36C (void);
// 0x00000006 System.Void UnityEventInt::.ctor()
extern void UnityEventInt__ctor_m84D0CABE2FCC7052FF8D67EB3767A224D23FE27D (void);
// 0x00000007 System.Void UnityEventBool::.ctor()
extern void UnityEventBool__ctor_m9B937ABAF103E75E79DC2104A7B7EEFB6E444AF5 (void);
// 0x00000008 System.Void UnityEventString::.ctor()
extern void UnityEventString__ctor_mCE66A4594C42C4A82746C833FBE4145F91484656 (void);
// 0x00000009 System.Void UnityEventByteArray::.ctor()
extern void UnityEventByteArray__ctor_m23B301FA7AABC26A59C88D8615503D77986024E2 (void);
// 0x0000000A System.Void UnityEventFloatArray::.ctor()
extern void UnityEventFloatArray__ctor_mA9B88E8929B24D14E96409EB0799FAD4F9766AE4 (void);
// 0x0000000B System.Void UnityEventTexture::.ctor()
extern void UnityEventTexture__ctor_m9028716C8991DE8500EFE3D238EB6FB8CF563851 (void);
// 0x0000000C System.Void UnityEventTexture2D::.ctor()
extern void UnityEventTexture2D__ctor_m0A227E24E5CC48D25E1AA3BBE53E748422DBA7F4 (void);
// 0x0000000D System.Void UnityEventRenderTexture::.ctor()
extern void UnityEventRenderTexture__ctor_m5420132F74600A6756EEC00B5A2B25503C3669A8 (void);
// 0x0000000E System.Void UnityEventWebcamTexture::.ctor()
extern void UnityEventWebcamTexture__ctor_m92A1016EF2046F602286072F75493C61845517F4 (void);
// 0x0000000F System.Void UnityEventClass::.ctor()
extern void UnityEventClass__ctor_m637B4A5B38E8D5CEA66D6685C2AF4B53FC7555D3 (void);
// 0x00000010 System.Int32 Loom::get_maxThreads()
extern void Loom_get_maxThreads_mBFDF05F02373CF221873B7795BB41E30A0EF52B8 (void);
// 0x00000011 Loom Loom::get_Current()
extern void Loom_get_Current_mD40AEFF1D0447BC2176470BC2AA958D16FBF56CD (void);
// 0x00000012 System.Void Loom::Awake()
extern void Loom_Awake_m987F8BB9813558AE2440315CCA4487F28B5B851E (void);
// 0x00000013 System.Void Loom::Initialize()
extern void Loom_Initialize_m7AD8AE8E54D9B61CB2CC6A5E6CD5B3F81994CC6E (void);
// 0x00000014 System.Void Loom::QueueOnMainThread(System.Action)
extern void Loom_QueueOnMainThread_m1D1A0F1B02B10089C3F728F99A03014556ECC8F1 (void);
// 0x00000015 System.Void Loom::QueueOnMainThread(System.Action,System.Single)
extern void Loom_QueueOnMainThread_m01E9E67E96D2FBF8F5DB23F7D2A7AEEBCBAEB754 (void);
// 0x00000016 System.Threading.Thread Loom::RunAsync(System.Action)
extern void Loom_RunAsync_m60361CAE4DAF2E2B34F1281871CA7ADF7F2D4DEA (void);
// 0x00000017 System.Void Loom::RunAction(System.Object)
extern void Loom_RunAction_mDA59C16D2E0D8E794B21FFAFA0B7ADA89B4360F2 (void);
// 0x00000018 System.Void Loom::OnDisable()
extern void Loom_OnDisable_mCB21F8F1E3CE52AEA8B0030C8DC629CB94F75C24 (void);
// 0x00000019 System.Void Loom::Start()
extern void Loom_Start_m63DBD9D9C1128FABC8C1B93FE1A193CE870AE374 (void);
// 0x0000001A System.Void Loom::Update()
extern void Loom_Update_mC403CC9D6F66697121E191A1EDBBF7AF17B205D8 (void);
// 0x0000001B System.Void Loom::.ctor()
extern void Loom__ctor_m26E7344A62332D33A6E3091DD5374695D30A0A4F (void);
// 0x0000001C System.Void Loom::.cctor()
extern void Loom__cctor_m6161BC2506526A5D25E5692BF1FF063C2F4E5663 (void);
// 0x0000001D System.Void Loom/<>c::.cctor()
extern void U3CU3Ec__cctor_mCAC5F226E1B6A1A5B723BB1E586D142068A79DEF (void);
// 0x0000001E System.Void Loom/<>c::.ctor()
extern void U3CU3Ec__ctor_m8CBFEE95D33DE68C847BCE1FD7103DA5ADE9AFCF (void);
// 0x0000001F System.Boolean Loom/<>c::<Update>b__22_0(Loom/DelayedQueueItem)
extern void U3CU3Ec_U3CUpdateU3Eb__22_0_m9FC295F33486DA897E20972DBF8D38327830452D (void);
// 0x00000020 System.Boolean EnergySavingManager::getGyroChanged()
extern void EnergySavingManager_getGyroChanged_mAE35A89DD1FC6F0D2973104A72BF661C100E19EC (void);
// 0x00000021 System.Boolean EnergySavingManager::getAnyKeyChanged()
extern void EnergySavingManager_getAnyKeyChanged_m40B5F5B75E89726BA5FF199BF7C5B1215572E729 (void);
// 0x00000022 System.Boolean EnergySavingManager::getTouchChanged()
extern void EnergySavingManager_getTouchChanged_mA6997D0226800538704518F9FA378AC74135F5C4 (void);
// 0x00000023 System.Void EnergySavingManager::Awake()
extern void EnergySavingManager_Awake_m9B379DF18D472454BE85BF71BAD42445146983F2 (void);
// 0x00000024 System.Void EnergySavingManager::Start()
extern void EnergySavingManager_Start_m4CF5B507EDD85D8AA6918F6C9B4DF3001A96C9B2 (void);
// 0x00000025 System.Void EnergySavingManager::Update()
extern void EnergySavingManager_Update_m7E8A32874D4315C245F9A77124C9F5229BA43143 (void);
// 0x00000026 System.Void EnergySavingManager::Action_ReloadScene(System.Int32,System.Int32)
extern void EnergySavingManager_Action_ReloadScene_m779C8EA9C51C5A183C93DC47DD18893EBE6D175D (void);
// 0x00000027 System.Collections.IEnumerator EnergySavingManager::DelayReloadScene(System.Int32,System.Int32)
extern void EnergySavingManager_DelayReloadScene_mF7A2AB7B939480498089DD8EC05FB35D902FBCF4 (void);
// 0x00000028 System.Void EnergySavingManager::Action_QuitApp(System.Int32,System.Int32)
extern void EnergySavingManager_Action_QuitApp_mEC4869447A6CA727C79C68F2247922D4FCD59817 (void);
// 0x00000029 System.Collections.IEnumerator EnergySavingManager::DelayQuitApp(System.Int32,System.Int32)
extern void EnergySavingManager_DelayQuitApp_m40B6738D77295EFBB81AEFA977CB3E759DD6F58A (void);
// 0x0000002A System.Void EnergySavingManager::.ctor()
extern void EnergySavingManager__ctor_m6812540BA1398FCFDDA4772AF5AE5B87C56D871E (void);
// 0x0000002B System.Void EnergySavingManager/<DelayReloadScene>d__40::.ctor(System.Int32)
extern void U3CDelayReloadSceneU3Ed__40__ctor_m9C2D4FCABFC9EADD8728467A959758E86FABE86D (void);
// 0x0000002C System.Void EnergySavingManager/<DelayReloadScene>d__40::System.IDisposable.Dispose()
extern void U3CDelayReloadSceneU3Ed__40_System_IDisposable_Dispose_mEBF06A2B6A445F58ED8A59ACA890C2B6B0A85000 (void);
// 0x0000002D System.Boolean EnergySavingManager/<DelayReloadScene>d__40::MoveNext()
extern void U3CDelayReloadSceneU3Ed__40_MoveNext_mD769F9DC7CDCF6049647F0B90EF14BE686F19DF7 (void);
// 0x0000002E System.Object EnergySavingManager/<DelayReloadScene>d__40::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7CB2CB7AAC68E2F8FF7BBB0A8B866B8DF0CFF12E (void);
// 0x0000002F System.Void EnergySavingManager/<DelayReloadScene>d__40::System.Collections.IEnumerator.Reset()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_Reset_mD3D4AFD5BA4923D70EEAC2CDC7BA1A8617565EF8 (void);
// 0x00000030 System.Object EnergySavingManager/<DelayReloadScene>d__40::System.Collections.IEnumerator.get_Current()
extern void U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_get_Current_mCCE823FFE54CAAB72EA600771F6EFCAB4F67FB4D (void);
// 0x00000031 System.Void EnergySavingManager/<DelayQuitApp>d__42::.ctor(System.Int32)
extern void U3CDelayQuitAppU3Ed__42__ctor_mD835D2843CD39786B0E8B4493969D7303407FFA2 (void);
// 0x00000032 System.Void EnergySavingManager/<DelayQuitApp>d__42::System.IDisposable.Dispose()
extern void U3CDelayQuitAppU3Ed__42_System_IDisposable_Dispose_mADC91381BCDE6FCB627908B1C77D31057D306399 (void);
// 0x00000033 System.Boolean EnergySavingManager/<DelayQuitApp>d__42::MoveNext()
extern void U3CDelayQuitAppU3Ed__42_MoveNext_m88363142B70765D3FC9EDCB98B21AE791613DB67 (void);
// 0x00000034 System.Object EnergySavingManager/<DelayQuitApp>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27A3D3C634F6D04EA49224B5121E8F04BF434E76 (void);
// 0x00000035 System.Void EnergySavingManager/<DelayQuitApp>d__42::System.Collections.IEnumerator.Reset()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_Reset_m12DC3D27867D00F5F5711FA68E13A315EFB2663A (void);
// 0x00000036 System.Object EnergySavingManager/<DelayQuitApp>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_get_Current_m2012CD18A7958FE1CE6CD116718B220A86F78B30 (void);
// 0x00000037 System.Void FPSManager::Update()
extern void FPSManager_Update_mC5FB1A7327C9855566378BC360F75CFE7C51E48C (void);
// 0x00000038 System.Void FPSManager::Action_SetFPS(System.Int32)
extern void FPSManager_Action_SetFPS_mB79FBF7A0CF4A52225CE7867E06F6F5AF8B2745B (void);
// 0x00000039 System.Void FPSManager::.ctor()
extern void FPSManager__ctor_mA7EA885C97F3C25E21C0587DE0782EAAFF465040 (void);
// 0x0000003A System.Void DemoObjectAnimation::Start()
extern void DemoObjectAnimation_Start_mC5A5FA2787F1BF6B7331CBF86377461668846B11 (void);
// 0x0000003B System.Void DemoObjectAnimation::Update()
extern void DemoObjectAnimation_Update_m146AAA28DF5D61F0A8158B4DD806819CA76D4752 (void);
// 0x0000003C System.Void DemoObjectAnimation::.ctor()
extern void DemoObjectAnimation__ctor_m16E3C4AC0AFEF8E404D01021F8DAAFEDC90B410C (void);
// 0x0000003D System.Void DemoScreenshot::Reset()
extern void DemoScreenshot_Reset_mEA1D117811D21202332F24B9E8C1D4B4FC947CA6 (void);
// 0x0000003E System.Void DemoScreenshot::Update()
extern void DemoScreenshot_Update_m704E9858DBF8B0887D644DBB47E9D4BA021296CF (void);
// 0x0000003F System.Void DemoScreenshot::SaveScreenshot()
extern void DemoScreenshot_SaveScreenshot_m67711F8567152BE16D935D52655F27E3CE0C02BD (void);
// 0x00000040 System.Collections.IEnumerator DemoScreenshot::SaveJPG(System.String)
extern void DemoScreenshot_SaveJPG_mA77A0626915ADB9F075EEF09C1CB6A04B7B30109 (void);
// 0x00000041 System.Collections.IEnumerator DemoScreenshot::SaveScreenshotPano()
extern void DemoScreenshot_SaveScreenshotPano_m166A4B075B7D0A12D14C157449298CAF29D04406 (void);
// 0x00000042 System.Void DemoScreenshot::.ctor()
extern void DemoScreenshot__ctor_mDFE42454CAF301642D867A5A417B3E11E4D63AD1 (void);
// 0x00000043 System.Void DemoScreenshot/<SaveJPG>d__10::.ctor(System.Int32)
extern void U3CSaveJPGU3Ed__10__ctor_m386C97FF710A502A5DD109699097996FAC80E844 (void);
// 0x00000044 System.Void DemoScreenshot/<SaveJPG>d__10::System.IDisposable.Dispose()
extern void U3CSaveJPGU3Ed__10_System_IDisposable_Dispose_mE48D4EF3C29A7661DCB7054FDEB399F421509A10 (void);
// 0x00000045 System.Boolean DemoScreenshot/<SaveJPG>d__10::MoveNext()
extern void U3CSaveJPGU3Ed__10_MoveNext_mB4D7B683EA6797A048372892231910BA13E85A12 (void);
// 0x00000046 System.Object DemoScreenshot/<SaveJPG>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveJPGU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m350A78EEC0F2BC37D985636B9E53D9EE08D2C089 (void);
// 0x00000047 System.Void DemoScreenshot/<SaveJPG>d__10::System.Collections.IEnumerator.Reset()
extern void U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_Reset_m47650C7D79E391BAC8F9A6FDC69A040F94B02D72 (void);
// 0x00000048 System.Object DemoScreenshot/<SaveJPG>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_get_Current_m0C0561FABE8DADE8914B7CFE2E6ACB668C1C0509 (void);
// 0x00000049 System.Void DemoScreenshot/<SaveScreenshotPano>d__11::.ctor(System.Int32)
extern void U3CSaveScreenshotPanoU3Ed__11__ctor_mC5B18D77BB15BA8E62E9F4D69621F487CB7DF6DC (void);
// 0x0000004A System.Void DemoScreenshot/<SaveScreenshotPano>d__11::System.IDisposable.Dispose()
extern void U3CSaveScreenshotPanoU3Ed__11_System_IDisposable_Dispose_mF64E38ADD60416E5AB6D008F50FD32EF32BB5F9C (void);
// 0x0000004B System.Boolean DemoScreenshot/<SaveScreenshotPano>d__11::MoveNext()
extern void U3CSaveScreenshotPanoU3Ed__11_MoveNext_mCCB0E917EFC9C49FA290AAFE5B754A82B1042F52 (void);
// 0x0000004C System.Object DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0551D82882EFA01F6AB668CF7D8FB3B3F870169C (void);
// 0x0000004D System.Void DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.IEnumerator.Reset()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_Reset_m55FFC76C7427098F5A8E637A5535D8FE5F7EC0D7 (void);
// 0x0000004E System.Object DemoScreenshot/<SaveScreenshotPano>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_get_Current_m0B1B6BC1A4115B6442C4A49280D06D42024640C2 (void);
// 0x0000004F System.Void FillCameraBackground::Start()
extern void FillCameraBackground_Start_m59ED5CFBA36D1D32540B49C1F06A25B4B0270974 (void);
// 0x00000050 System.Void FillCameraBackground::LateUpdate()
extern void FillCameraBackground_LateUpdate_mC787831631F44F8650BADB42ED16740B0312EE67 (void);
// 0x00000051 System.Void FillCameraBackground::.ctor()
extern void FillCameraBackground__ctor_m4BF8EEC0947E4143F4993BB4F6D8FF3AB95EB2D2 (void);
// 0x00000052 System.Void GyroController::Awake()
extern void GyroController_Awake_mC2902E5004D2352B3071F88CC17C7C62EE884D33 (void);
// 0x00000053 System.Void GyroController::Start()
extern void GyroController_Start_mEC40C772A93C44744C7021CDB4A1F8940AE5B8EC (void);
// 0x00000054 System.Boolean GyroController::EnableGyro()
extern void GyroController_EnableGyro_m597850219FA05002EC2469F9D3757C422899B3C4 (void);
// 0x00000055 System.Void GyroController::Update()
extern void GyroController_Update_m4E220AE541FA69B8F1E08A97889EA40B81EA3C8F (void);
// 0x00000056 System.Void GyroController::AimingOffset(System.Single)
extern void GyroController_AimingOffset_mE10E89A8AA15DA14BA06A831A27CB7E74C01DF16 (void);
// 0x00000057 System.Void GyroController::MouseDragRot()
extern void GyroController_MouseDragRot_mC16070F127CE717C9144A0C68BA9B44FFEF74DDD (void);
// 0x00000058 System.Void GyroController::TouchDragRot()
extern void GyroController_TouchDragRot_mAA75FF07714BC9415249E984D0D768B09282D945 (void);
// 0x00000059 System.Void GyroController::.ctor()
extern void GyroController__ctor_m35DB854DB03DA01F19148B9B3573023B58F6BEF8 (void);
// 0x0000005A System.Void _UnityEventFloat::.ctor()
extern void _UnityEventFloat__ctor_mF404A530353279730A7A450770D9512CAE7E8EAE (void);
// 0x0000005B System.Void PointerEvents::Start()
extern void PointerEvents_Start_m0619591BD50210C2173550EA58603B5CFDEE27DE (void);
// 0x0000005C System.Void PointerEvents::Update()
extern void PointerEvents_Update_m13290D9940D8B331488477FEBB2220CDBFE4701B (void);
// 0x0000005D System.Void PointerEvents::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerDown_mED61F3B782D38C8728A651B7C1D838939E9F2BE0 (void);
// 0x0000005E System.Void PointerEvents::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerUp_mC3E8505699F6E3CB01EBEB6E1072215C5C58E7EF (void);
// 0x0000005F System.Void PointerEvents::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerEnter_mE5EA77DD8C92B353B8A2CF7E6EB9655BA4D5FDF5 (void);
// 0x00000060 System.Void PointerEvents::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void PointerEvents_OnPointerExit_mE6616049BED7EBAF817BB3489E18E609BB453247 (void);
// 0x00000061 System.Void PointerEvents::.ctor()
extern void PointerEvents__ctor_m0C46DC0ED7304CE9F4984DD8A89F8654F84B04A9 (void);
// 0x00000062 System.Void TouchVisualization::Start()
extern void TouchVisualization_Start_mE9826E2391900A7DB4CB8E064725A9A2AC956D33 (void);
// 0x00000063 System.Void TouchVisualization::Update()
extern void TouchVisualization_Update_m985B411E26E043B601FAEA94DBF6F6DF8EAAA2A1 (void);
// 0x00000064 System.Void TouchVisualization::.ctor()
extern void TouchVisualization__ctor_mD5E8D494581ED40336DA8B692BDD8004F54DACB3 (void);
// 0x00000065 System.Void OffScreenTrigger::Start()
extern void OffScreenTrigger_Start_m1DA8D8FD100CFB63BCBDCC783D429BB0037B81BE (void);
// 0x00000066 System.Void OffScreenTrigger::Update()
extern void OffScreenTrigger_Update_m7F8B9926160286D53BB1A44AB8EDC2631D004FE3 (void);
// 0x00000067 System.Void OffScreenTrigger::Action_offscreen()
extern void OffScreenTrigger_Action_offscreen_mECD98558A5777297AFD5E58878CA6CA1B4F1FD81 (void);
// 0x00000068 System.Void OffScreenTrigger::.ctor()
extern void OffScreenTrigger__ctor_m72FCBCD47F07ACC83413D9DED90008AE45AC74D5 (void);
// 0x00000069 System.Void WorldToScreenSpace::Start()
extern void WorldToScreenSpace_Start_m64F64FC3E4C8FA8D69ED364C4E5BD25C468633C3 (void);
// 0x0000006A System.Void WorldToScreenSpace::Update()
extern void WorldToScreenSpace_Update_m2FE5ADA33F0AF77704C773EA5A0562F3D7816385 (void);
// 0x0000006B System.Void WorldToScreenSpace::InvokeOnScreen()
extern void WorldToScreenSpace_InvokeOnScreen_m6279AEBFA4AE793391F0E170A3FAD8A91A4A2CE9 (void);
// 0x0000006C System.Void WorldToScreenSpace::InvokeOffScreen()
extern void WorldToScreenSpace_InvokeOffScreen_mDFCAD4E5FFAD7C4866232FF89AD24C24B52ED7D9 (void);
// 0x0000006D System.Void WorldToScreenSpace::OnDisable()
extern void WorldToScreenSpace_OnDisable_m4EC05D8B91C08C09F3A889802970F7EF469AB74F (void);
// 0x0000006E System.Void WorldToScreenSpace::OnEnable()
extern void WorldToScreenSpace_OnEnable_m115A27B81FA39428D8C0AC43C4E76491A822088A (void);
// 0x0000006F System.Void WorldToScreenSpace::DebugTest(System.String)
extern void WorldToScreenSpace_DebugTest_m88873998A1257B961986D72F0B85727D442F99C2 (void);
// 0x00000070 System.Void WorldToScreenSpace::.ctor()
extern void WorldToScreenSpace__ctor_m5781646FA5241C4CF867EB77A4DC89CD9D613E59 (void);
// 0x00000071 System.Void ZoomManager::Start()
extern void ZoomManager_Start_m77C510FE9406539A9299F49C21AC7D16EBC746A0 (void);
// 0x00000072 System.Void ZoomManager::Update()
extern void ZoomManager_Update_m1250B595F983EC9A4E8E644CB36474C578EFAAF2 (void);
// 0x00000073 System.Void ZoomManager::GestureZoom()
extern void ZoomManager_GestureZoom_m6169EE03098A43B55A0BA1A525CB084637B44042 (void);
// 0x00000074 System.Void ZoomManager::ZoomIn()
extern void ZoomManager_ZoomIn_m15B67012D606434D0FE3F64250C29DA22B98909A (void);
// 0x00000075 System.Void ZoomManager::ZoomOut()
extern void ZoomManager_ZoomOut_mD1E2A13519B774C144CDE623EBDE5AEB1D90D356 (void);
// 0x00000076 System.Void ZoomManager::.ctor()
extern void ZoomManager__ctor_m2CC2CF7A4F8DED39D25F2D27826C34B9DF0D1E42 (void);
// 0x00000077 System.Void SceneManager_helper::Action_LoadSceneName(System.String)
extern void SceneManager_helper_Action_LoadSceneName_m7D4039AA3780B88CE04BCE3B6A5CA60C002BE78C (void);
// 0x00000078 System.Void SceneManager_helper::Action_Quit()
extern void SceneManager_helper_Action_Quit_mEE2D2B3DAED5D8D8D9F57F25CBDF7B8F0D8DC320 (void);
// 0x00000079 System.Void SceneManager_helper::.ctor()
extern void SceneManager_helper__ctor_m8FE1179083EDEA3CCBD8B8963DE371871FBDA356 (void);
// 0x0000007A System.Void TargetProjectionMatrix::Action_UpdateFOV(System.Single)
extern void TargetProjectionMatrix_Action_UpdateFOV_mE341D8AF055048386C552360EFF1AA3E2E3EADC5 (void);
// 0x0000007B System.Void TargetProjectionMatrix::Action_SetAllowUpdate(System.Boolean)
extern void TargetProjectionMatrix_Action_SetAllowUpdate_m5DF69304BCB6ACC412411FA08052F9087B803DC7 (void);
// 0x0000007C System.Void TargetProjectionMatrix::Action_SetForceDisableUpdate(System.Boolean)
extern void TargetProjectionMatrix_Action_SetForceDisableUpdate_mCDC2DDBEF44F38712DC40F73C4694BB0DBC811A5 (void);
// 0x0000007D System.Collections.IEnumerator TargetProjectionMatrix::UpdateProjectionMatrixLoop()
extern void TargetProjectionMatrix_UpdateProjectionMatrixLoop_mA4F3092CBAA52589E199C271C5C5F4310AA2A280 (void);
// 0x0000007E System.Void TargetProjectionMatrix::Start()
extern void TargetProjectionMatrix_Start_m151B070FE2720E5F112472BCC5B76038473036E8 (void);
// 0x0000007F System.Void TargetProjectionMatrix::.ctor()
extern void TargetProjectionMatrix__ctor_m645FBE1E100CD4D2C40320542AF123C03CDA0819 (void);
// 0x00000080 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::.ctor(System.Int32)
extern void U3CUpdateProjectionMatrixLoopU3Ed__10__ctor_mF1F092D3580744393E65F26470ABFB5FA624205E (void);
// 0x00000081 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.IDisposable.Dispose()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_IDisposable_Dispose_m69E3ED3E3CC52EFF42639FC9CC20547412CEB760 (void);
// 0x00000082 System.Boolean TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::MoveNext()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_MoveNext_m6427B67D9CC0687F909615E34FEE9CEAE09C0E92 (void);
// 0x00000083 System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FFF9756FD760615484DAD33BED17A5F64410B8A (void);
// 0x00000084 System.Void TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.Reset()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m48BDCC8BCBDF2E199C2F0747709847FC77A9368D (void);
// 0x00000085 System.Object TargetProjectionMatrix/<UpdateProjectionMatrixLoop>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_get_Current_mA53BBB6DCE402D2D9D24D7D9E09381B8D90E11CC (void);
// 0x00000086 System.Void WebcamManager::Action_useFrontCam(System.Boolean)
extern void WebcamManager_Action_useFrontCam_m8ED258689D0EC42710B946D7E49F39AF32068513 (void);
// 0x00000087 System.Collections.IEnumerator WebcamManager::RestartWebcam()
extern void WebcamManager_RestartWebcam_mAAEB6C09BE69612582B7F0B161B28D01AD8B54A6 (void);
// 0x00000088 UnityEngine.WebCamTexture WebcamManager::get_WebCamTexture()
extern void WebcamManager_get_WebCamTexture_m35BDDA7BBF791071FA71CD6C867BC8A40773885F (void);
// 0x00000089 System.Boolean WebcamManager::get_IsFlipped()
extern void WebcamManager_get_IsFlipped_m08320BB96217FE938F08C77B79B7C864B93CE2D2 (void);
// 0x0000008A System.Void WebcamManager::Start()
extern void WebcamManager_Start_mBDAA4410F75B7A6E5B3F4CE6DD63AE0BDEBBE47A (void);
// 0x0000008B System.Void WebcamManager::Update()
extern void WebcamManager_Update_m6E5EB41C4A34E8C2A2ACE54173EE7904EA1D3A77 (void);
// 0x0000008C System.Void WebcamManager::OnApplicationQuit()
extern void WebcamManager_OnApplicationQuit_m74199660E98163FB7E6D51199FC80C4541F0C113 (void);
// 0x0000008D System.Void WebcamManager::CalculateBackgroundQuad()
extern void WebcamManager_CalculateBackgroundQuad_mDDDB2D51DFBEE57B38A98B651BE962B91B1A9FE4 (void);
// 0x0000008E System.Void WebcamManager::Action_StopWebcam()
extern void WebcamManager_Action_StopWebcam_mC18EABF0211B71C2A435D164C1138313B599E09A (void);
// 0x0000008F System.Void WebcamManager::Action_StartWebcam()
extern void WebcamManager_Action_StartWebcam_m7E278C81D0ACB8C6A1B56EA3328DF828BC19FB65 (void);
// 0x00000090 System.Void WebcamManager::OnEnable()
extern void WebcamManager_OnEnable_mC2C792D9FDCC2E607A96F8823BB0B5161E671E2D (void);
// 0x00000091 System.Void WebcamManager::OnDisable()
extern void WebcamManager_OnDisable_mC7DDD33854300614CE34B2F00C4B1A5F08C861DF (void);
// 0x00000092 System.Collections.IEnumerator WebcamManager::initAndWaitForWebCamTexture()
extern void WebcamManager_initAndWaitForWebCamTexture_m2038FC96D0B93758A70FFA9E3599BBC792665A3C (void);
// 0x00000093 System.Void WebcamManager::.ctor()
extern void WebcamManager__ctor_m3B1211ACBDEBC4181E1591761814C889BE33063B (void);
// 0x00000094 System.Void WebcamManager/<RestartWebcam>d__8::.ctor(System.Int32)
extern void U3CRestartWebcamU3Ed__8__ctor_m558DDD3135B8F717F7DA1D08FABCB9A938DEB726 (void);
// 0x00000095 System.Void WebcamManager/<RestartWebcam>d__8::System.IDisposable.Dispose()
extern void U3CRestartWebcamU3Ed__8_System_IDisposable_Dispose_mB65F777C48E205E4B8051407831AD5C9A6174F58 (void);
// 0x00000096 System.Boolean WebcamManager/<RestartWebcam>d__8::MoveNext()
extern void U3CRestartWebcamU3Ed__8_MoveNext_mBADBC7BAB84F045A65DFD2FBBAF2FF000F6C8972 (void);
// 0x00000097 System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRestartWebcamU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF3A3164CF7A7D035E413E7C50F9E56F07F7E530 (void);
// 0x00000098 System.Void WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_m2684DEA1E75FDB1D383B0CA963ACAE2FE8C4258E (void);
// 0x00000099 System.Object WebcamManager/<RestartWebcam>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_get_Current_m90716B3793FFE4699BB46A762BD02777BC8B78F3 (void);
// 0x0000009A System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::.ctor(System.Int32)
extern void U3CinitAndWaitForWebCamTextureU3Ed__34__ctor_m13012C61A04A8289FFB98070BCCB13E25ACAEDE7 (void);
// 0x0000009B System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.IDisposable.Dispose()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_IDisposable_Dispose_m1D25632C5F2F60D269705EE1F5D7E6706D4042D8 (void);
// 0x0000009C System.Boolean WebcamManager/<initAndWaitForWebCamTexture>d__34::MoveNext()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_MoveNext_mDF413DF7226F4F21703A20F310075B3DD94B5356 (void);
// 0x0000009D System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50FBA164EB4EC570F2970A9E97BF1D255E4B5707 (void);
// 0x0000009E System.Void WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.Reset()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m821CC481B994FF1BFA627B2CF48DE3C19B90B938 (void);
// 0x0000009F System.Object WebcamManager/<initAndWaitForWebCamTexture>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m9E35B5BF182FF5519B47E825AA7CF8B3E2A0083C (void);
// 0x000000A0 System.Void AudioDecoder::Start()
extern void AudioDecoder_Start_m5475B508B4DFE94F8D70274CE38E8103BDCE79B7 (void);
// 0x000000A1 System.Void AudioDecoder::Action_ProcessData(System.Byte[])
extern void AudioDecoder_Action_ProcessData_m62376715692CE83F5AE3F0913E2E8302E046AB05 (void);
// 0x000000A2 System.Single AudioDecoder::get_Volume()
extern void AudioDecoder_get_Volume_mF02AD931449AB29B2AC9CE4BD52692A9DAE6F9CD (void);
// 0x000000A3 System.Void AudioDecoder::set_Volume(System.Single)
extern void AudioDecoder_set_Volume_m644B238CB46A45955C70C8844B5628E7B78C97C0 (void);
// 0x000000A4 System.Collections.IEnumerator AudioDecoder::ProcessAudioData(System.Byte[])
extern void AudioDecoder_ProcessAudioData_mB2F38413A2FF8AE50E38F965B40CFC61A8FAC5FB (void);
// 0x000000A5 System.Void AudioDecoder::CreateClip()
extern void AudioDecoder_CreateClip_m64D78B4D39B05BF08776572FBF1AA4F9B529620B (void);
// 0x000000A6 System.Void AudioDecoder::OnAudioRead(System.Single[])
extern void AudioDecoder_OnAudioRead_m568DEB832519F54C8D6BB5976F6909C2B46154D0 (void);
// 0x000000A7 System.Void AudioDecoder::OnAudioSetPosition(System.Int32)
extern void AudioDecoder_OnAudioSetPosition_mC171D96CEFE23F3AAA05093CD126AD64E492BC0A (void);
// 0x000000A8 System.Single[] AudioDecoder::ToFloatArray(System.Byte[])
extern void AudioDecoder_ToFloatArray_m7365E20988C94B62B0880CFF96CB01F862F66869 (void);
// 0x000000A9 System.Void AudioDecoder::.ctor()
extern void AudioDecoder__ctor_m7428FA819C5CC591B785F7B21008B2D7305C40F4 (void);
// 0x000000AA System.Void AudioDecoder/<ProcessAudioData>d__19::.ctor(System.Int32)
extern void U3CProcessAudioDataU3Ed__19__ctor_m4B88CC0E0F821B45617660761DC9B8DFB2F499D0 (void);
// 0x000000AB System.Void AudioDecoder/<ProcessAudioData>d__19::System.IDisposable.Dispose()
extern void U3CProcessAudioDataU3Ed__19_System_IDisposable_Dispose_m930215C882E907A199C8FA9B963030F088AA0528 (void);
// 0x000000AC System.Boolean AudioDecoder/<ProcessAudioData>d__19::MoveNext()
extern void U3CProcessAudioDataU3Ed__19_MoveNext_mF66D5B9590FFF8E2B543FE8D1E2667670D6E6D62 (void);
// 0x000000AD System.Object AudioDecoder/<ProcessAudioData>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44CB672888E763F716EEE1C09BC3AFB640583584 (void);
// 0x000000AE System.Void AudioDecoder/<ProcessAudioData>d__19::System.Collections.IEnumerator.Reset()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_Reset_m1D9CAA1A5872B486EB412357823782A971EA7FB2 (void);
// 0x000000AF System.Object AudioDecoder/<ProcessAudioData>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_get_Current_m4E20AB0886EAFEFB5C1514ECE053B692DAE069B8 (void);
// 0x000000B0 System.Void AudioEncoder::Start()
extern void AudioEncoder_Start_mDCB81765A426669CB1ED0771CF9294CDB113C212 (void);
// 0x000000B1 System.Void AudioEncoder::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioEncoder_OnAudioFilterRead_mC29FB707D0864B9850543CA7A6B5EAFBD9627EBD (void);
// 0x000000B2 System.Collections.IEnumerator AudioEncoder::SenderCOR()
extern void AudioEncoder_SenderCOR_m76EC65EB4C6150B41F4142EFB891DF9F875428F6 (void);
// 0x000000B3 System.Int16 AudioEncoder::FloatToInt16(System.Single)
extern void AudioEncoder_FloatToInt16_mCDD0D0272A1E75F91AF06B6DF815D3CC95668A93 (void);
// 0x000000B4 System.Void AudioEncoder::EncodeBytes()
extern void AudioEncoder_EncodeBytes_m366517BA3823C4322276A91E4BC4296E026B94C3 (void);
// 0x000000B5 System.Void AudioEncoder::OnEnable()
extern void AudioEncoder_OnEnable_mD707839AEF3DAA3125438BFDBF246F6462650BD1 (void);
// 0x000000B6 System.Void AudioEncoder::OnDisable()
extern void AudioEncoder_OnDisable_m42F8E5421D72F9F90767FB204A2AE400C5B21F85 (void);
// 0x000000B7 System.Void AudioEncoder::.ctor()
extern void AudioEncoder__ctor_mD67A2B52F6528C40A25E16B6DC9A5BC9F281FF23 (void);
// 0x000000B8 System.Void AudioEncoder/<SenderCOR>d__22::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__22__ctor_mA61B9C1473DBEE4FCD50436184FEA2699A8F78FD (void);
// 0x000000B9 System.Void AudioEncoder/<SenderCOR>d__22::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__22_System_IDisposable_Dispose_m6E26F5E9A98A30EE1565B8B3A8A4DE7920784F74 (void);
// 0x000000BA System.Boolean AudioEncoder/<SenderCOR>d__22::MoveNext()
extern void U3CSenderCORU3Ed__22_MoveNext_m13C9958FA4E691BCC15F2E316DC7DB8F4A9DF186 (void);
// 0x000000BB System.Object AudioEncoder/<SenderCOR>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC49BDBFB780B49D30F8E4F047363C9C950016565 (void);
// 0x000000BC System.Void AudioEncoder/<SenderCOR>d__22::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__22_System_Collections_IEnumerator_Reset_mF5740BB0AB5EAD53E7DA519DA7AE7C94D2584E94 (void);
// 0x000000BD System.Object AudioEncoder/<SenderCOR>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__22_System_Collections_IEnumerator_get_Current_mCC0D350AB3C6FE8E1EC9301105A6B01C726CEEBD (void);
// 0x000000BE System.Void FMPCStreamDecoder::Reset()
extern void FMPCStreamDecoder_Reset_mCA58D8086FCF98EC123AE664F92887006F16C581 (void);
// 0x000000BF System.Void FMPCStreamDecoder::Start()
extern void FMPCStreamDecoder_Start_m914A6E5746B23DF3B69C11EAB23D7B7F29D389A8 (void);
// 0x000000C0 System.Void FMPCStreamDecoder::Update()
extern void FMPCStreamDecoder_Update_mAC3F39B67DBF2EA99D28E343163A04C1AEDDE617 (void);
// 0x000000C1 System.Void FMPCStreamDecoder::Action_ProcessPointCloudData(System.Byte[])
extern void FMPCStreamDecoder_Action_ProcessPointCloudData_mF6442A3097E627CAE7DD9C67E195CF0BD6134E20 (void);
// 0x000000C2 System.Collections.IEnumerator FMPCStreamDecoder::ProcessImageData(System.Byte[])
extern void FMPCStreamDecoder_ProcessImageData_mD9FF4EF87D20EA43045108B6F9D4084EFF200471 (void);
// 0x000000C3 System.Void FMPCStreamDecoder::OnDisable()
extern void FMPCStreamDecoder_OnDisable_m5D145A576F28245E5C5E95DA5F1D74AEC0B04A0F (void);
// 0x000000C4 System.Void FMPCStreamDecoder::Action_ProcessImage(UnityEngine.Texture2D)
extern void FMPCStreamDecoder_Action_ProcessImage_mBF87C53684E3039948F95D27667EA4DB3B3CA775 (void);
// 0x000000C5 System.Void FMPCStreamDecoder::.ctor()
extern void FMPCStreamDecoder__ctor_mA97CBAB85033CED698376EAC562249C51BB2FBF9 (void);
// 0x000000C6 System.Void FMPCStreamDecoder/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_mDC5886C435106F338D4C84ADB1DF100277EC94E2 (void);
// 0x000000C7 System.Void FMPCStreamDecoder/<>c__DisplayClass28_1::.ctor()
extern void U3CU3Ec__DisplayClass28_1__ctor_m9D800CD136370CFBF3FE5877597105F3AC9DAAEB (void);
// 0x000000C8 System.Void FMPCStreamDecoder/<>c__DisplayClass28_1::<ProcessImageData>b__0()
extern void U3CU3Ec__DisplayClass28_1_U3CProcessImageDataU3Eb__0_m4FA67DA6789CC38361FAD8A65877AF0741647A34 (void);
// 0x000000C9 System.Void FMPCStreamDecoder/<ProcessImageData>d__28::.ctor(System.Int32)
extern void U3CProcessImageDataU3Ed__28__ctor_mFCF7164CF0FAB59E6AB8917741B3C6F3EEF79D30 (void);
// 0x000000CA System.Void FMPCStreamDecoder/<ProcessImageData>d__28::System.IDisposable.Dispose()
extern void U3CProcessImageDataU3Ed__28_System_IDisposable_Dispose_m2E2E49F74BFE9C572E6BD13984BBC1A3D0AE7153 (void);
// 0x000000CB System.Boolean FMPCStreamDecoder/<ProcessImageData>d__28::MoveNext()
extern void U3CProcessImageDataU3Ed__28_MoveNext_mA27C953D5B7D3BD4BC8F0F9E40019E37E416CC5B (void);
// 0x000000CC System.Object FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessImageDataU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC23C1918B3A8403769110160090139AB506A4F6F (void);
// 0x000000CD System.Void FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.IEnumerator.Reset()
extern void U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_Reset_m0A11C827AE83D9B52521DCBE5B72051B54F5938F (void);
// 0x000000CE System.Object FMPCStreamDecoder/<ProcessImageData>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_get_Current_m7AB274E9CC84E8231455FC8BB41DC8D71F337363 (void);
// 0x000000CF System.Void FMPCStreamEncoder::Reset()
extern void FMPCStreamEncoder_Reset_m97928B002BBDC40E528A6FD43E31945B8F1FB761 (void);
// 0x000000D0 System.Void FMPCStreamEncoder::CheckResolution()
extern void FMPCStreamEncoder_CheckResolution_m2B95F9304C724F86ACED86081A886C1838066B2B (void);
// 0x000000D1 System.Void FMPCStreamEncoder::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void FMPCStreamEncoder_OnRenderImage_m6AD97D04BE044BB8577CE6CFB3C411B65FA694A5 (void);
// 0x000000D2 System.Void FMPCStreamEncoder::LateUpdate()
extern void FMPCStreamEncoder_LateUpdate_m617BC57ED5ECE413638B362B37A8927A2121FE1E (void);
// 0x000000D3 System.Void FMPCStreamEncoder::RequestTextureUpdate()
extern void FMPCStreamEncoder_RequestTextureUpdate_m80C2F23F7FE8E9C6BAAF6F06AA9CACF6908BD2A8 (void);
// 0x000000D4 System.Boolean FMPCStreamEncoder::get_SupportsAsyncGPUReadback()
extern void FMPCStreamEncoder_get_SupportsAsyncGPUReadback_m6D459B7B91CFE756D7D2006C0369B09FD20FBD72 (void);
// 0x000000D5 UnityEngine.Texture FMPCStreamEncoder::get_GetStreamTexture()
extern void FMPCStreamEncoder_get_GetStreamTexture_mD3856A27A2E6054E87F8837CA8E9B969CF33A9D7 (void);
// 0x000000D6 System.Void FMPCStreamEncoder::ProcessCapturedTexture()
extern void FMPCStreamEncoder_ProcessCapturedTexture_m29A4DC70FE8EFA0C5AAE66AB8DFEC044139FE74B (void);
// 0x000000D7 System.Collections.IEnumerator FMPCStreamEncoder::ProcessCapturedTextureCOR()
extern void FMPCStreamEncoder_ProcessCapturedTextureCOR_mEEF58C9ACD8F623090D081ED5544F013411687C5 (void);
// 0x000000D8 System.Collections.IEnumerator FMPCStreamEncoder::ProcessCapturedTextureGPUReadbackCOR()
extern void FMPCStreamEncoder_ProcessCapturedTextureGPUReadbackCOR_mF332AAAC334DD1B48EB5B0942B8DADCE67A99BB8 (void);
// 0x000000D9 System.Collections.IEnumerator FMPCStreamEncoder::EncodeBytes(System.Byte[])
extern void FMPCStreamEncoder_EncodeBytes_m1149580C4C4E67E7A75B774344BDF3A1BDD60B5B (void);
// 0x000000DA System.Void FMPCStreamEncoder::.ctor()
extern void FMPCStreamEncoder__ctor_m216674951E578D21752320559D7A3774665CF77E (void);
// 0x000000DB System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::.ctor(System.Int32)
extern void U3CProcessCapturedTextureCORU3Ed__42__ctor_m9B0D8AC83A7A59E971A4F6E14618F00458E13B95 (void);
// 0x000000DC System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_IDisposable_Dispose_mB0EAD2DA81A514C9EBF867F4504928E367D4760A (void);
// 0x000000DD System.Boolean FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::MoveNext()
extern void U3CProcessCapturedTextureCORU3Ed__42_MoveNext_mD8C2A597937A8EDD0E027243CEC22A2DE2E0D9A4 (void);
// 0x000000DE System.Object FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m432DF574611723EE7D90AFC05BAFAF519025D7E7 (void);
// 0x000000DF System.Void FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_Reset_mA5A95C2FEF36C236B4E1ACC68D96F9D27B07776A (void);
// 0x000000E0 System.Object FMPCStreamEncoder/<ProcessCapturedTextureCOR>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_get_Current_m9CD50AC1A662E9A97D4013A384FB77D17A4E32D1 (void);
// 0x000000E1 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43__ctor_m628D38CA50098205DC7961CC96D0969766998742 (void);
// 0x000000E2 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_IDisposable_Dispose_mD3777C2FF27BF3822C0271654FB810F8A7BD0889 (void);
// 0x000000E3 System.Boolean FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_MoveNext_m5055C3A5AB0EA3B92D315A6773F5EE92BF8572D6 (void);
// 0x000000E4 System.Object FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C9AD5DE44B3F60239BE9AB0421ABC1C94B38413 (void);
// 0x000000E5 System.Void FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_Reset_m58A0CECB176018FDFF73FAA5145ECE3BA60D1139 (void);
// 0x000000E6 System.Object FMPCStreamEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_get_Current_m7CCF691370A65E331963B42A4BAB1B316F409283 (void);
// 0x000000E7 System.Void FMPCStreamEncoder/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m94C496F6DBDAB67C1669AA4E9DEE705B8B340AB8 (void);
// 0x000000E8 System.Void FMPCStreamEncoder/<>c__DisplayClass44_1::.ctor()
extern void U3CU3Ec__DisplayClass44_1__ctor_mE17FD129FC1E71888647150455ADF1B46991D1A9 (void);
// 0x000000E9 System.Void FMPCStreamEncoder/<>c__DisplayClass44_1::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass44_1_U3CEncodeBytesU3Eb__0_m6995E320DB1F67A4E95581C93B91BF0E9070A4D6 (void);
// 0x000000EA System.Void FMPCStreamEncoder/<EncodeBytes>d__44::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__44__ctor_m89AC43A66209F3E6CA9C66A7EED633ECD87574DA (void);
// 0x000000EB System.Void FMPCStreamEncoder/<EncodeBytes>d__44::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__44_System_IDisposable_Dispose_mDA990CE2A65C441D6654CD47F4734A861C985A60 (void);
// 0x000000EC System.Boolean FMPCStreamEncoder/<EncodeBytes>d__44::MoveNext()
extern void U3CEncodeBytesU3Ed__44_MoveNext_mD3BF17BE3F895BD8321A8569E3423E777FF16F03 (void);
// 0x000000ED System.Object FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34D2D1471592200E73B991DE08D37BF78292633B (void);
// 0x000000EE System.Void FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_Reset_m719E2377FE8C0B41D256C40A8746863C79E34255 (void);
// 0x000000EF System.Object FMPCStreamEncoder/<EncodeBytes>d__44::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_get_Current_m637465828A3DC422301B364D6058290D43D59C4B (void);
// 0x000000F0 UnityEngine.Texture GameViewDecoder::get_ReceivedTexture()
extern void GameViewDecoder_get_ReceivedTexture_mC8D5BFB695908343AD33716F621616BF40B88E17 (void);
// 0x000000F1 System.Void GameViewDecoder::Reset()
extern void GameViewDecoder_Reset_m6E4BADFF8BF5AD016B1F46156D0E1079C85BA71D (void);
// 0x000000F2 System.Void GameViewDecoder::Start()
extern void GameViewDecoder_Start_m02A174BEA22D2C4D9782E741F1B0F327533DDDEE (void);
// 0x000000F3 System.Void GameViewDecoder::Action_ProcessImageData(System.Byte[])
extern void GameViewDecoder_Action_ProcessImageData_m3F85268F93E00143F04267321D188A0133E1E933 (void);
// 0x000000F4 System.Collections.IEnumerator GameViewDecoder::ProcessImageData(System.Byte[])
extern void GameViewDecoder_ProcessImageData_m4CC55BFFF33D854FD9594CB3F7A3273B56330194 (void);
// 0x000000F5 System.Void GameViewDecoder::OnDisable()
extern void GameViewDecoder_OnDisable_mD9F68509B11935C9F0E091D77E23723430681DE1 (void);
// 0x000000F6 System.Void GameViewDecoder::Action_ProcessMJPEGData(System.Byte[])
extern void GameViewDecoder_Action_ProcessMJPEGData_m5331F366DDEB05B612EC6362336714B6690B1917 (void);
// 0x000000F7 System.Void GameViewDecoder::parseStreamBuffer(System.Byte[])
extern void GameViewDecoder_parseStreamBuffer_m102267345D55FFB348419018E8413656D486C3B5 (void);
// 0x000000F8 System.Void GameViewDecoder::searchPicture(System.Byte[])
extern void GameViewDecoder_searchPicture_m05F8D56CF9BB26BA5AB7817D96E255D42BA97170 (void);
// 0x000000F9 System.Void GameViewDecoder::parsePicture(System.Byte[])
extern void GameViewDecoder_parsePicture_m4A1036265EE466424C265271C4C76DC31C702C01 (void);
// 0x000000FA System.Void GameViewDecoder::.ctor()
extern void GameViewDecoder__ctor_mE2B4B0BAA070CAB391F76A020683FC4BD0EDB976 (void);
// 0x000000FB System.Void GameViewDecoder/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_m96A07E4DE0753CE6F6691B27DCC5917982594D78 (void);
// 0x000000FC System.Void GameViewDecoder/<>c__DisplayClass26_1::.ctor()
extern void U3CU3Ec__DisplayClass26_1__ctor_m2FBE3B1C27912851E189E05EC005609ADAB7CF4B (void);
// 0x000000FD System.Void GameViewDecoder/<>c__DisplayClass26_1::<ProcessImageData>b__0()
extern void U3CU3Ec__DisplayClass26_1_U3CProcessImageDataU3Eb__0_mD51C3F6EB6C5830012033595D0E4C847A9477DBA (void);
// 0x000000FE System.Void GameViewDecoder/<ProcessImageData>d__26::.ctor(System.Int32)
extern void U3CProcessImageDataU3Ed__26__ctor_mF2D2ACE7EEE752AEAE686BCDCD8EFFA06B08C293 (void);
// 0x000000FF System.Void GameViewDecoder/<ProcessImageData>d__26::System.IDisposable.Dispose()
extern void U3CProcessImageDataU3Ed__26_System_IDisposable_Dispose_m0DE8BEF4B5A38A8C2873A613B488BDC1C8AA4F40 (void);
// 0x00000100 System.Boolean GameViewDecoder/<ProcessImageData>d__26::MoveNext()
extern void U3CProcessImageDataU3Ed__26_MoveNext_m7020AFA3E0D721C509DCD4DA7D0E3BE2BC47DEE9 (void);
// 0x00000101 System.Object GameViewDecoder/<ProcessImageData>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessImageDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m173DA41E891FEF77D2C0CE8B9F2C3D971E7679EE (void);
// 0x00000102 System.Void GameViewDecoder/<ProcessImageData>d__26::System.Collections.IEnumerator.Reset()
extern void U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_Reset_m8EE0C1FB6E65B9956EDB8A8F04B7FAD61D2638CC (void);
// 0x00000103 System.Object GameViewDecoder/<ProcessImageData>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_get_Current_mED336401EEB8B42E18D24CA260705B40C5D91587 (void);
// 0x00000104 System.Boolean GameViewEncoder::get_SupportsAsyncGPUReadback()
extern void GameViewEncoder_get_SupportsAsyncGPUReadback_m307889E9B3EF82B96B63C2CB42805822E9B1537A (void);
// 0x00000105 UnityEngine.Texture GameViewEncoder::get_GetStreamTexture()
extern void GameViewEncoder_get_GetStreamTexture_m3B80F6AC311D8112299F6EE6AAF2C4D4B58D332B (void);
// 0x00000106 System.Single GameViewEncoder::get_brightness()
extern void GameViewEncoder_get_brightness_mB48985FF7799171F3870ECAE8E3675736656285C (void);
// 0x00000107 System.Void GameViewEncoder::CaptureModeUpdate()
extern void GameViewEncoder_CaptureModeUpdate_mE20F5185E59FBEA746200D033129034D63A27D43 (void);
// 0x00000108 System.Void GameViewEncoder::Reset()
extern void GameViewEncoder_Reset_m16BBE1C91978FC808D971510EA4FEFE274383CA5 (void);
// 0x00000109 System.Void GameViewEncoder::Start()
extern void GameViewEncoder_Start_m711CD69DEA6EC5A232DB1DB99A860124A4A90A7B (void);
// 0x0000010A System.Void GameViewEncoder::Update()
extern void GameViewEncoder_Update_m40EEE8439FEEFC1ECB88B4EC58F96AA5577D69C9 (void);
// 0x0000010B System.Void GameViewEncoder::CheckResolution()
extern void GameViewEncoder_CheckResolution_m9E19E28728EE71B829D02D534BC222D95C75D0C2 (void);
// 0x0000010C System.Void GameViewEncoder::ProcessCapturedTexture()
extern void GameViewEncoder_ProcessCapturedTexture_m85D51DDD505253E5959A847473E3D4C0FF42D347 (void);
// 0x0000010D System.Collections.IEnumerator GameViewEncoder::ProcessCapturedTextureCOR()
extern void GameViewEncoder_ProcessCapturedTextureCOR_m4CE12E97C197BC25686A06DE3C6DF495EDA7519A (void);
// 0x0000010E System.Collections.IEnumerator GameViewEncoder::ProcessCapturedTextureGPUReadbackCOR()
extern void GameViewEncoder_ProcessCapturedTextureGPUReadbackCOR_mD67F2991A429A2F3653FEAA41D5F14A1AB9EFF23 (void);
// 0x0000010F System.Void GameViewEncoder::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void GameViewEncoder_OnRenderImage_mCCC857972D92691CB693EE22468BB660B979D12B (void);
// 0x00000110 System.Collections.IEnumerator GameViewEncoder::RenderTextureRefresh()
extern void GameViewEncoder_RenderTextureRefresh_m55372366F394AD43E1D7BD18DC0849751ABECB48 (void);
// 0x00000111 System.Void GameViewEncoder::Action_UpdateTexture()
extern void GameViewEncoder_Action_UpdateTexture_m13F6BB9310FCF2729B107CC4B941BFDADFFAFC9C (void);
// 0x00000112 System.Void GameViewEncoder::RequestTextureUpdate()
extern void GameViewEncoder_RequestTextureUpdate_m984BB1DA3D27181E8C83A9243B9CD527F4133CA0 (void);
// 0x00000113 System.Collections.IEnumerator GameViewEncoder::SenderCOR()
extern void GameViewEncoder_SenderCOR_mFE18AE7EF7B9D623234795730887A0857F426FD7 (void);
// 0x00000114 System.Collections.IEnumerator GameViewEncoder::EncodeBytes(System.Byte[])
extern void GameViewEncoder_EncodeBytes_mDE72CF51618F7FC8253001FC64D0435FA84984DF (void);
// 0x00000115 System.Void GameViewEncoder::OnEnable()
extern void GameViewEncoder_OnEnable_m922B2729372038A53A98AC56EDEFA99A68112B58 (void);
// 0x00000116 System.Void GameViewEncoder::OnDisable()
extern void GameViewEncoder_OnDisable_m4EBC128C316E48C583EDC4567BDA50888DD1F883 (void);
// 0x00000117 System.Void GameViewEncoder::OnApplicationQuit()
extern void GameViewEncoder_OnApplicationQuit_mBF83C40D20DCA2DB4B0D6BD6B61C34BD132B51B6 (void);
// 0x00000118 System.Void GameViewEncoder::OnDestroy()
extern void GameViewEncoder_OnDestroy_mB5A25D2943751726F379A9DBB1D29002D64EBB24 (void);
// 0x00000119 System.Void GameViewEncoder::StopAll()
extern void GameViewEncoder_StopAll_m1DF6708CA5E16BC6C75D685DA9F51EF0BF276626 (void);
// 0x0000011A System.Void GameViewEncoder::StartAll()
extern void GameViewEncoder_StartAll_m6308B2B05D7EF911D97A759E27DF336D154DB9DC (void);
// 0x0000011B System.Void GameViewEncoder::.ctor()
extern void GameViewEncoder__ctor_mC5B6393F4082FDAF2E3D4BAE014C125D5AC7AAB1 (void);
// 0x0000011C System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::.ctor(System.Int32)
extern void U3CProcessCapturedTextureCORU3Ed__69__ctor_m349BA12399B95AA9CF528CE193AE14A6A0DC889C (void);
// 0x0000011D System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_IDisposable_Dispose_mE6885B6EBA1687614681C0E6F70023B4C85E971B (void);
// 0x0000011E System.Boolean GameViewEncoder/<ProcessCapturedTextureCOR>d__69::MoveNext()
extern void U3CProcessCapturedTextureCORU3Ed__69_MoveNext_mE26D9D1B4F6AD3E56CCC47DD1731C88E572E433D (void);
// 0x0000011F System.Object GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2A8853CF78817477FD9F9EDA951A636795BBD44 (void);
// 0x00000120 System.Void GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_Reset_m0BD7910AEFC35C5DA7F6D396E58C89911C8EAD30 (void);
// 0x00000121 System.Object GameViewEncoder/<ProcessCapturedTextureCOR>d__69::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_get_Current_m2F63B58BAF7737184B353CDE5CE59E88C573A4C6 (void);
// 0x00000122 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70__ctor_mE032DCBF94F2AF749A547BA3CDF0848E4D4C1611 (void);
// 0x00000123 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_IDisposable_Dispose_m3A3C3021524EBF569E24E6F5E88CC331A9A5E035 (void);
// 0x00000124 System.Boolean GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_MoveNext_m9086D4FE8C5C349A458E0A51490593710D59735E (void);
// 0x00000125 System.Object GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD04D1FB74E758F4E4366F40E0F5F777D44A83C02 (void);
// 0x00000126 System.Void GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_Reset_m479B2599F6C84598BEF16687ACCC823E5BDC4AD6 (void);
// 0x00000127 System.Object GameViewEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__70::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_get_Current_m7DDE4BEF1104D014C21E07E6B41BF4BB44547B1C (void);
// 0x00000128 System.Void GameViewEncoder/<RenderTextureRefresh>d__72::.ctor(System.Int32)
extern void U3CRenderTextureRefreshU3Ed__72__ctor_m9FBA53AD62A26B656D74D5FF7EBC1ED4D5058C8E (void);
// 0x00000129 System.Void GameViewEncoder/<RenderTextureRefresh>d__72::System.IDisposable.Dispose()
extern void U3CRenderTextureRefreshU3Ed__72_System_IDisposable_Dispose_m96AA81C9A1C1A6D477DC7248915A860A35464D1E (void);
// 0x0000012A System.Boolean GameViewEncoder/<RenderTextureRefresh>d__72::MoveNext()
extern void U3CRenderTextureRefreshU3Ed__72_MoveNext_m529DF1FE044ECA76473D3F37DD799EA9366AF3D3 (void);
// 0x0000012B System.Object GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m199E00D245FE84F06F945B32D651296E6CCF8C83 (void);
// 0x0000012C System.Void GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.IEnumerator.Reset()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_Reset_m157D8745BAEB1A3D271EB347E3B1F9D162643C8E (void);
// 0x0000012D System.Object GameViewEncoder/<RenderTextureRefresh>d__72::System.Collections.IEnumerator.get_Current()
extern void U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_get_Current_m56670B5014CFA817C863BAC21A236A9020F86761 (void);
// 0x0000012E System.Void GameViewEncoder/<SenderCOR>d__75::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__75__ctor_m5248213CF3B7629AE57D204251CB3C32CC909A68 (void);
// 0x0000012F System.Void GameViewEncoder/<SenderCOR>d__75::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__75_System_IDisposable_Dispose_m4D5072E44AFE990223CE8302A75FC993E9E4B454 (void);
// 0x00000130 System.Boolean GameViewEncoder/<SenderCOR>d__75::MoveNext()
extern void U3CSenderCORU3Ed__75_MoveNext_m6B0FE085DB2C4014080CF578F8BF07A7CCB6E71F (void);
// 0x00000131 System.Object GameViewEncoder/<SenderCOR>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D72E6225B84BB0CAB451C5FA237AE7EA2620961 (void);
// 0x00000132 System.Void GameViewEncoder/<SenderCOR>d__75::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__75_System_Collections_IEnumerator_Reset_mAAFB7A18426C402C9C931751E2CA48E719EE207A (void);
// 0x00000133 System.Object GameViewEncoder/<SenderCOR>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__75_System_Collections_IEnumerator_get_Current_m298A61AD42A74332B11A4F3FE6520899D31B2A76 (void);
// 0x00000134 System.Void GameViewEncoder/<>c__DisplayClass76_0::.ctor()
extern void U3CU3Ec__DisplayClass76_0__ctor_mBD2590CA5D804F1FC7FB9917E51AE295542A211B (void);
// 0x00000135 System.Void GameViewEncoder/<>c__DisplayClass76_1::.ctor()
extern void U3CU3Ec__DisplayClass76_1__ctor_mB045CF39D33BA33C20D7C83D50BC2BF58DE823C6 (void);
// 0x00000136 System.Void GameViewEncoder/<>c__DisplayClass76_1::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass76_1_U3CEncodeBytesU3Eb__0_m1FB723FF38B2687580FD67AC5A982433E27E0BBB (void);
// 0x00000137 System.Void GameViewEncoder/<EncodeBytes>d__76::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__76__ctor_m2D4DF2846C6A077D6CE0649718EBDDA075DAF9EE (void);
// 0x00000138 System.Void GameViewEncoder/<EncodeBytes>d__76::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__76_System_IDisposable_Dispose_mE55A15277E473D9F2D4135393D008DBD6A1100BC (void);
// 0x00000139 System.Boolean GameViewEncoder/<EncodeBytes>d__76::MoveNext()
extern void U3CEncodeBytesU3Ed__76_MoveNext_mAE62A232391C9E9DB9471CD19026CBEE54D088EF (void);
// 0x0000013A System.Object GameViewEncoder/<EncodeBytes>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D6C24CC185C0BB17D5D992844654BF01D72FA10 (void);
// 0x0000013B System.Void GameViewEncoder/<EncodeBytes>d__76::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_Reset_m2C431F2696EB5A00A934D87D93F282F96565C2C7 (void);
// 0x0000013C System.Object GameViewEncoder/<EncodeBytes>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_get_Current_m2F175BEE870299917793DBBB52E3FB7E5ADBF973 (void);
// 0x0000013D System.Void MicEncoder::Start()
extern void MicEncoder_Start_m2DC3500C0FB63AB7DB2D74A6969423C4C0BCDD61 (void);
// 0x0000013E System.Collections.IEnumerator MicEncoder::CaptureMic()
extern void MicEncoder_CaptureMic_m20B4454E2E157822E9F9B99DF4EEC735E231F9D8 (void);
// 0x0000013F System.Int16 MicEncoder::FloatToInt16(System.Single)
extern void MicEncoder_FloatToInt16_mE492C780E2F11BA78D7DB9D7B360B9D7C38EE557 (void);
// 0x00000140 System.Void MicEncoder::AddMicData()
extern void MicEncoder_AddMicData_mD38334C2E64D85EF325366ABC27B50EF9F751FAE (void);
// 0x00000141 System.Collections.IEnumerator MicEncoder::SenderCOR()
extern void MicEncoder_SenderCOR_m4D54CDCA339EB0313D89E5FCD3372DFA06B73D3F (void);
// 0x00000142 System.Void MicEncoder::EncodeBytes()
extern void MicEncoder_EncodeBytes_mE627954E5410F9EB32C0A64D396857ADBCF55758 (void);
// 0x00000143 System.Void MicEncoder::OnEnable()
extern void MicEncoder_OnEnable_mE3E85CF5A81A7D3A05A1C00832DA8167874F68A9 (void);
// 0x00000144 System.Void MicEncoder::OnDisable()
extern void MicEncoder_OnDisable_m5704E679F1E757575272B920122D74B1B45EDBD9 (void);
// 0x00000145 System.Void MicEncoder::StartAll()
extern void MicEncoder_StartAll_mF2137D48FEA0E905A99FD4B9DDF78E5BA90BADF9 (void);
// 0x00000146 System.Void MicEncoder::StopAll()
extern void MicEncoder_StopAll_m2CB39B0755D59BDDAB21A72EE740FA9A65348866 (void);
// 0x00000147 System.Void MicEncoder::.ctor()
extern void MicEncoder__ctor_m785BC83D676DADE5B67A41D37A95CE6C8979E5AC (void);
// 0x00000148 System.Void MicEncoder/<CaptureMic>d__25::.ctor(System.Int32)
extern void U3CCaptureMicU3Ed__25__ctor_mCCB0E36591F95A3EB566D6B7572EA298DFE9C12B (void);
// 0x00000149 System.Void MicEncoder/<CaptureMic>d__25::System.IDisposable.Dispose()
extern void U3CCaptureMicU3Ed__25_System_IDisposable_Dispose_mDF2FDE4489AE0061221D6ADDE9B9CE4C44AB7419 (void);
// 0x0000014A System.Boolean MicEncoder/<CaptureMic>d__25::MoveNext()
extern void U3CCaptureMicU3Ed__25_MoveNext_m5D2E82894CC545B60F44215031A877A8DF2C8D36 (void);
// 0x0000014B System.Object MicEncoder/<CaptureMic>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCaptureMicU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FBDAC6120F439A264A6232B62FE956E2B50FC3B (void);
// 0x0000014C System.Void MicEncoder/<CaptureMic>d__25::System.Collections.IEnumerator.Reset()
extern void U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_Reset_mD46140D14D5E336728E2ACBFAD184D06ECD36ADF (void);
// 0x0000014D System.Object MicEncoder/<CaptureMic>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_get_Current_m5C7D3AB53FC19BDC813676D19AAC400CEC7E41DB (void);
// 0x0000014E System.Void MicEncoder/<SenderCOR>d__28::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__28__ctor_m4599307B8957A286498335806B28024B30147C43 (void);
// 0x0000014F System.Void MicEncoder/<SenderCOR>d__28::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__28_System_IDisposable_Dispose_m720B454D1377C86EB4320E4901D75C76002136CC (void);
// 0x00000150 System.Boolean MicEncoder/<SenderCOR>d__28::MoveNext()
extern void U3CSenderCORU3Ed__28_MoveNext_m113B01C2E1795B6B00D604595E41A6D0F31D5F56 (void);
// 0x00000151 System.Object MicEncoder/<SenderCOR>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01CC83D39095EAB79B390A0B03B8CA80514A76B9 (void);
// 0x00000152 System.Void MicEncoder/<SenderCOR>d__28::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__28_System_Collections_IEnumerator_Reset_m48088E046EDEFB8EBFCEFD117D0F8DD554C2E2B6 (void);
// 0x00000153 System.Object MicEncoder/<SenderCOR>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__28_System_Collections_IEnumerator_get_Current_mA2103C688782E250D58380931F78E3C58B0F4991 (void);
// 0x00000154 System.Void TextureEncoder::set_SetStreamWebCamTexture(UnityEngine.WebCamTexture)
extern void TextureEncoder_set_SetStreamWebCamTexture_m6E95DF6D926A8D67CF725A232C5EFE45689E6E50 (void);
// 0x00000155 UnityEngine.Texture TextureEncoder::get_GetStreamTexture()
extern void TextureEncoder_get_GetStreamTexture_mE92CC2619C619E2B9264F2BA53D19169FDBAF991 (void);
// 0x00000156 UnityEngine.Texture TextureEncoder::get_GetPreviewTexture()
extern void TextureEncoder_get_GetPreviewTexture_m353A4445898BE7FBA1C16BBAC2F3EC75A3C3ED03 (void);
// 0x00000157 System.Boolean TextureEncoder::get_SupportsAsyncGPUReadback()
extern void TextureEncoder_get_SupportsAsyncGPUReadback_mA189BBE116743BE5DFE4FF5F31EDF165E2AD4BC3 (void);
// 0x00000158 System.Int32 TextureEncoder::get_StreamWidth()
extern void TextureEncoder_get_StreamWidth_m1F194961386571E5CF4D9936423585438704C788 (void);
// 0x00000159 System.Int32 TextureEncoder::get_StreamHeight()
extern void TextureEncoder_get_StreamHeight_mB8C20A46F386C65FADA651B0099D2BCDA7ECE2D7 (void);
// 0x0000015A System.Void TextureEncoder::Start()
extern void TextureEncoder_Start_m529A619E091D0517661737C0ECD97362F435BABC (void);
// 0x0000015B System.Void TextureEncoder::Action_StreamTexture(System.Byte[],System.Int32,System.Int32)
extern void TextureEncoder_Action_StreamTexture_m7F30752FC32A927EC96CCA3FD0F2884623E97318 (void);
// 0x0000015C System.Void TextureEncoder::Action_StreamTexture(UnityEngine.Texture2D)
extern void TextureEncoder_Action_StreamTexture_m4394C73A567297ED27F23D803C82EB21CB3E4D64 (void);
// 0x0000015D System.Void TextureEncoder::Action_StreamWebcamTexture(UnityEngine.WebCamTexture)
extern void TextureEncoder_Action_StreamWebcamTexture_m68C22C283EE3470DE9C34C35976F19E0831B857F (void);
// 0x0000015E System.Void TextureEncoder::Action_StreamRenderTexture(UnityEngine.RenderTexture)
extern void TextureEncoder_Action_StreamRenderTexture_mEF5834465A7923540928BE5054EB59D8BF47A892 (void);
// 0x0000015F System.Collections.IEnumerator TextureEncoder::ProcessCapturedTextureGPUReadbackCOR(UnityEngine.RenderTexture)
extern void TextureEncoder_ProcessCapturedTextureGPUReadbackCOR_mE98216B9C49ED531A9CE4BBAD86209DA07F2F2EA (void);
// 0x00000160 System.Void TextureEncoder::RequestTextureUpdate()
extern void TextureEncoder_RequestTextureUpdate_m53878A01F689338F0A032E6654995B4FFB6F85A3 (void);
// 0x00000161 System.Collections.IEnumerator TextureEncoder::SenderCOR()
extern void TextureEncoder_SenderCOR_m104C7B8592F46AACB30D679D7BE67D0044C24811 (void);
// 0x00000162 System.Collections.IEnumerator TextureEncoder::EncodeBytes()
extern void TextureEncoder_EncodeBytes_m6C7371FB88101453BB519DB958B8962F394AAF89 (void);
// 0x00000163 System.Void TextureEncoder::OnEnable()
extern void TextureEncoder_OnEnable_mBF71F42C787977589B39646CB5D9ADE299EEDF8E (void);
// 0x00000164 System.Void TextureEncoder::OnDisable()
extern void TextureEncoder_OnDisable_m412273B270C757AF203DE6B5E8070A3256C4D43A (void);
// 0x00000165 System.Void TextureEncoder::OnApplicationQuit()
extern void TextureEncoder_OnApplicationQuit_mCE9D539FA0C437B7A6779D5DB0F29D131BF3A7B8 (void);
// 0x00000166 System.Void TextureEncoder::OnDestroy()
extern void TextureEncoder_OnDestroy_m6FFA6FC72EA038D2C4D3B27394230FC2961941F8 (void);
// 0x00000167 System.Void TextureEncoder::StopAll()
extern void TextureEncoder_StopAll_m25496EA60398851424A398B5639BFDC1A8A395B7 (void);
// 0x00000168 System.Void TextureEncoder::StartAll()
extern void TextureEncoder_StartAll_mCDD5DF1747A784CF03FB25B97FB3D1A0F6726735 (void);
// 0x00000169 System.Void TextureEncoder::.ctor()
extern void TextureEncoder__ctor_m72CD11EEB9878CDEAED87E5A22A8E751DEF6F384 (void);
// 0x0000016A System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::.ctor(System.Int32)
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49__ctor_mC87CAD2F648891E0981CEF30D3E6D27509D81EBC (void);
// 0x0000016B System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.IDisposable.Dispose()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_IDisposable_Dispose_m6A816B7D02CEE27A81BB54A3265BFD1E6794BE95 (void);
// 0x0000016C System.Boolean TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::MoveNext()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_MoveNext_mD2E47E573D8891F9202AD8404A1042ED568F7669 (void);
// 0x0000016D System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A5A209C5405CAA94D3927AD8ADD99716C1823 (void);
// 0x0000016E System.Void TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.Reset()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_mEBE7C0CA8F0CB85DA36512EB77B767899B9B1C46 (void);
// 0x0000016F System.Object TextureEncoder/<ProcessCapturedTextureGPUReadbackCOR>d__49::System.Collections.IEnumerator.get_Current()
extern void U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_get_Current_mBB96842995E784FE9B131940828407C1EFA4817A (void);
// 0x00000170 System.Void TextureEncoder/<SenderCOR>d__51::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__51__ctor_m3A3DE179749EE5B0246188A55FF9DEA4715DC25C (void);
// 0x00000171 System.Void TextureEncoder/<SenderCOR>d__51::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__51_System_IDisposable_Dispose_m4D3672B13294D2040F91E4E6939CCF61A1C8C529 (void);
// 0x00000172 System.Boolean TextureEncoder/<SenderCOR>d__51::MoveNext()
extern void U3CSenderCORU3Ed__51_MoveNext_m6FB439208F02D5A12C87CE9BE76D583AF26B2C12 (void);
// 0x00000173 System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70ACD69140004798E2FCA2F250052903E6ABBD04 (void);
// 0x00000174 System.Void TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m6BEF057DF094C567EFA3665F7913CBFC6C2B0392 (void);
// 0x00000175 System.Object TextureEncoder/<SenderCOR>d__51::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__51_System_Collections_IEnumerator_get_Current_mA10486303E326BDB27D355835C7B3E9CE3D66C80 (void);
// 0x00000176 System.Void TextureEncoder/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m47C8B4213013AFC4EB7A1A48847B3D91F7F0E5A9 (void);
// 0x00000177 System.Void TextureEncoder/<>c__DisplayClass52_0::<EncodeBytes>b__0()
extern void U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_mEF0607CD72BEF643FC20FBACFDB2EED7BFC0CA8D (void);
// 0x00000178 System.Void TextureEncoder/<EncodeBytes>d__52::.ctor(System.Int32)
extern void U3CEncodeBytesU3Ed__52__ctor_m56FCD6473B32D5B1E608B1ABAC380AB684063136 (void);
// 0x00000179 System.Void TextureEncoder/<EncodeBytes>d__52::System.IDisposable.Dispose()
extern void U3CEncodeBytesU3Ed__52_System_IDisposable_Dispose_mBA680916D858702CB22BDD455DE7C17B0C3EE194 (void);
// 0x0000017A System.Boolean TextureEncoder/<EncodeBytes>d__52::MoveNext()
extern void U3CEncodeBytesU3Ed__52_MoveNext_mE18E2596E658D91FA0062B85D3884438B46FE0E8 (void);
// 0x0000017B System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CEncodeBytesU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79E11F5603810579ECB7ECCDFD25AECC0C341E61 (void);
// 0x0000017C System.Void TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.Reset()
extern void U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_mE899EFFCEF5A5600448CB197DBFEFA3056382E77 (void);
// 0x0000017D System.Object TextureEncoder/<EncodeBytes>d__52::System.Collections.IEnumerator.get_Current()
extern void U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_get_Current_mDA8A1B5058EB0B650A7C569384C33B439EDC8E0D (void);
// 0x0000017E System.Void ConnectionDebugText::Start()
extern void ConnectionDebugText_Start_m9F11FE5DF1F0B8F5BE4A631F95319DEF73EC537C (void);
// 0x0000017F System.Void ConnectionDebugText::Update()
extern void ConnectionDebugText_Update_m13B28363A6D1453E2AB7E64DF4CA916D185FCEF1 (void);
// 0x00000180 System.Void ConnectionDebugText::.ctor()
extern void ConnectionDebugText__ctor_mF6E3404B2077E82E9868CABBC03163E64D5DDF4E (void);
// 0x00000181 System.Void NetworkActionClient::Awake()
extern void NetworkActionClient_Awake_m0129ECF53495C70E7D264424D83A9BD7EB46E532 (void);
// 0x00000182 System.Void NetworkActionClient::Action_SetIP(System.String)
extern void NetworkActionClient_Action_SetIP_m1329683FA4CC4B7D5F0DE194694D3102F08446F4 (void);
// 0x00000183 System.Void NetworkActionClient::Action_SetServerListenPort(System.Int32)
extern void NetworkActionClient_Action_SetServerListenPort_mFF3DB0256F47F9221B0F9556A4FDEAB4365E04BB (void);
// 0x00000184 System.Int32 NetworkActionClient::GetCurrentMS()
extern void NetworkActionClient_GetCurrentMS_m7D13F79664DA785207D4DC1B51ABD12C177E2E47 (void);
// 0x00000185 System.Void NetworkActionClient::Action_ReceivedDataDebug(System.String)
extern void NetworkActionClient_Action_ReceivedDataDebug_m6F4D86CC99381EAEE18A5944E5D90947D4069D80 (void);
// 0x00000186 System.Void NetworkActionClient::Action_RpcSend(System.String)
extern void NetworkActionClient_Action_RpcSend_m3A9E72A8E6DE779085C0E85BC451FA3F7B1E85D1 (void);
// 0x00000187 System.Void NetworkActionClient::Action_RpcSend(System.Byte[])
extern void NetworkActionClient_Action_RpcSend_mCE72D264D14CFF800C70475D720281F4AFE75DCA (void);
// 0x00000188 System.Void NetworkActionClient::Start()
extern void NetworkActionClient_Start_m4723B6DDC83B59F11DA4810DF37F274A0FCA012C (void);
// 0x00000189 System.Void NetworkActionClient::Update()
extern void NetworkActionClient_Update_m94D09E65469CBF45EB9123F27A533B6975117508 (void);
// 0x0000018A System.Int32 NetworkActionClient::DataByteArrayToByteLength(System.Byte[])
extern void NetworkActionClient_DataByteArrayToByteLength_m2AB6FABA3C8F0175EE6F1E590AD02D3E943A394F (void);
// 0x0000018B System.Int32 NetworkActionClient::ReadDataByteSize(System.Int32)
extern void NetworkActionClient_ReadDataByteSize_m6FEBE9380FB53EE49AEEE093A1150007837F6C96 (void);
// 0x0000018C System.Void NetworkActionClient::ReadDataByteArray(System.Int32)
extern void NetworkActionClient_ReadDataByteArray_m9FAC77E4788D7E46E9E52E7B2CAA3FAA670E0AC4 (void);
// 0x0000018D System.Void NetworkActionClient::ProcessReceivedData(System.Byte[])
extern void NetworkActionClient_ProcessReceivedData_m5FF1C0DE231F1C60FCA8338927E611C6FC7F7DDA (void);
// 0x0000018E System.Collections.IEnumerator NetworkActionClient::RelaunchApp()
extern void NetworkActionClient_RelaunchApp_m729C27576144BB159677DC800587326119B2F2F6 (void);
// 0x0000018F System.Void NetworkActionClient::Action_Disconnect()
extern void NetworkActionClient_Action_Disconnect_m700CFDB523341F4DD6038140495308145E718488 (void);
// 0x00000190 System.Void NetworkActionClient::Action_ConnectServer()
extern void NetworkActionClient_Action_ConnectServer_m8B8D1FDC28572A441A30228C929B603BF735E76F (void);
// 0x00000191 System.Collections.IEnumerator NetworkActionClient::ConnectServer()
extern void NetworkActionClient_ConnectServer_m06918BC933FBA994E4C6F8046023893A739BD21C (void);
// 0x00000192 System.Collections.IEnumerator NetworkActionClient::MainThreadSender()
extern void NetworkActionClient_MainThreadSender_m0825837539C1C0491FA47C4C4292968988A040C0 (void);
// 0x00000193 System.Void NetworkActionClient::Sender()
extern void NetworkActionClient_Sender_m7483290E0CD5431AC11966DA766F351CF83C1F30 (void);
// 0x00000194 System.Void NetworkActionClient::ClientEndConnect(System.IAsyncResult)
extern void NetworkActionClient_ClientEndConnect_mD2C9C67C21DCB5F118634DD1772EBE65422C92CB (void);
// 0x00000195 System.Void NetworkActionClient::LOGWARNING(System.String)
extern void NetworkActionClient_LOGWARNING_mFD5E2BBDC186CBF8CA0A6D9EF38765020C21F861 (void);
// 0x00000196 System.Void NetworkActionClient::OnApplicationQuit()
extern void NetworkActionClient_OnApplicationQuit_mAECE415B5508C60F634C4A3248060DF4D637F93C (void);
// 0x00000197 System.Void NetworkActionClient::OnDestroy()
extern void NetworkActionClient_OnDestroy_mC7EA6C36E605D5D5FDE16659B75DA2EF787DAB0E (void);
// 0x00000198 System.Void NetworkActionClient::CheckPause()
extern void NetworkActionClient_CheckPause_m59F07538EC48B84D1F779D2564E05F41FE9F3029 (void);
// 0x00000199 System.Void NetworkActionClient::OnApplicationFocus(System.Boolean)
extern void NetworkActionClient_OnApplicationFocus_mAA469CC852708A81D05B4D8517950105F2A4AF11 (void);
// 0x0000019A System.Void NetworkActionClient::OnApplicationPause(System.Boolean)
extern void NetworkActionClient_OnApplicationPause_m4CC365C83844E1741C0D3CCC5470231CC7E8AFAA (void);
// 0x0000019B System.Void NetworkActionClient::OnDisable()
extern void NetworkActionClient_OnDisable_m6AD25F8B6DD0A8CA958AB3F5DBE328FA2280AE87 (void);
// 0x0000019C System.Void NetworkActionClient::OnEnable()
extern void NetworkActionClient_OnEnable_m24443D74C46A76D6FA517E15175C76D02959D9A3 (void);
// 0x0000019D System.Void NetworkActionClient::.ctor()
extern void NetworkActionClient__ctor_mDE9AE6C068C52EA3DB849623995E64756060615C (void);
// 0x0000019E System.Void NetworkActionClient::<ConnectServer>b__45_0()
extern void NetworkActionClient_U3CConnectServerU3Eb__45_0_m7E387AEDD18614BE47F4386C66DA798638FFE6B2 (void);
// 0x0000019F System.Void NetworkActionClient::<ConnectServer>b__45_1()
extern void NetworkActionClient_U3CConnectServerU3Eb__45_1_m7832DA779556CAFD32DC2DF43617C697B5E8F1BF (void);
// 0x000001A0 System.Void NetworkActionClient/<RelaunchApp>d__42::.ctor(System.Int32)
extern void U3CRelaunchAppU3Ed__42__ctor_mC166E2E13D67362ACEEBAB3E211F28ECC2D9E696 (void);
// 0x000001A1 System.Void NetworkActionClient/<RelaunchApp>d__42::System.IDisposable.Dispose()
extern void U3CRelaunchAppU3Ed__42_System_IDisposable_Dispose_m824E8B74094A6B06F29F7B75E90B19AAC4A12CFF (void);
// 0x000001A2 System.Boolean NetworkActionClient/<RelaunchApp>d__42::MoveNext()
extern void U3CRelaunchAppU3Ed__42_MoveNext_m5EA62805E0763A0C1C3DFB17296F1BFF230D82D4 (void);
// 0x000001A3 System.Object NetworkActionClient/<RelaunchApp>d__42::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRelaunchAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02533516AFC7B3B94CB7C92D548F9F8EA5B07083 (void);
// 0x000001A4 System.Void NetworkActionClient/<RelaunchApp>d__42::System.Collections.IEnumerator.Reset()
extern void U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_Reset_m4600363476C054520A4342D63CFBE64ABC1B01AD (void);
// 0x000001A5 System.Object NetworkActionClient/<RelaunchApp>d__42::System.Collections.IEnumerator.get_Current()
extern void U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_get_Current_m4538C6110478E7EE96D7A9E086D4F7656679BDE2 (void);
// 0x000001A6 System.Void NetworkActionClient/<ConnectServer>d__45::.ctor(System.Int32)
extern void U3CConnectServerU3Ed__45__ctor_mEB1C6684AE9BD51E2B784FF20CFF65DA4F15D7B6 (void);
// 0x000001A7 System.Void NetworkActionClient/<ConnectServer>d__45::System.IDisposable.Dispose()
extern void U3CConnectServerU3Ed__45_System_IDisposable_Dispose_m744DD31C7E7D6DE2BC1AD4D20DB497FCE25C381F (void);
// 0x000001A8 System.Boolean NetworkActionClient/<ConnectServer>d__45::MoveNext()
extern void U3CConnectServerU3Ed__45_MoveNext_mDB6263EDC7254F687E5DB70F3EFC6E434C724388 (void);
// 0x000001A9 System.Object NetworkActionClient/<ConnectServer>d__45::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CConnectServerU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m656B315965F4C79B1B8D82515BBFC491AA19A0E0 (void);
// 0x000001AA System.Void NetworkActionClient/<ConnectServer>d__45::System.Collections.IEnumerator.Reset()
extern void U3CConnectServerU3Ed__45_System_Collections_IEnumerator_Reset_m7789E8076A903AF653ECAB8EA22BE776B5B92259 (void);
// 0x000001AB System.Object NetworkActionClient/<ConnectServer>d__45::System.Collections.IEnumerator.get_Current()
extern void U3CConnectServerU3Ed__45_System_Collections_IEnumerator_get_Current_mE81591438BEF6C4A6BC372B5960D504CBE5B3A72 (void);
// 0x000001AC System.Void NetworkActionClient/<MainThreadSender>d__46::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__46__ctor_m72D6EB3EE179291316E5E8C164A557F57AC85F1F (void);
// 0x000001AD System.Void NetworkActionClient/<MainThreadSender>d__46::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__46_System_IDisposable_Dispose_m7451DDFBC5D2BA1B1BCBEC5F7222E00B38F5AFB5 (void);
// 0x000001AE System.Boolean NetworkActionClient/<MainThreadSender>d__46::MoveNext()
extern void U3CMainThreadSenderU3Ed__46_MoveNext_mD080B387ADBF7309316E5060D54E526933401EF5 (void);
// 0x000001AF System.Object NetworkActionClient/<MainThreadSender>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B43672C8A3F269F239E63CBB71D966575691A8 (void);
// 0x000001B0 System.Void NetworkActionClient/<MainThreadSender>d__46::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_Reset_m0F47F7EB31CF4F2B413F260F0CA04402EEE11BE7 (void);
// 0x000001B1 System.Object NetworkActionClient/<MainThreadSender>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_get_Current_m1AAC2AC837669AFE3B0A4D90A0131A72B254FB4F (void);
// 0x000001B2 System.Void NetworkActionDemo::Action_ProcessRpcMessage(System.String)
extern void NetworkActionDemo_Action_ProcessRpcMessage_m3F8F048923276D2122DCCAD79CCEDA00D6DD2F0C (void);
// 0x000001B3 System.Void NetworkActionDemo::Rpc_SetInt(System.Int32)
extern void NetworkActionDemo_Rpc_SetInt_mEBBE08D5E1344C92408FB6FA145E98D42397B61B (void);
// 0x000001B4 System.Void NetworkActionDemo::Rpc_SetFloat(System.Single)
extern void NetworkActionDemo_Rpc_SetFloat_mD86F473D5B9A30F6CD42E7BD987C47333D466BFC (void);
// 0x000001B5 System.Void NetworkActionDemo::Rpc_SetBool(System.Boolean)
extern void NetworkActionDemo_Rpc_SetBool_m896F40BAD9170498E3D68D6990A7ACB1336BD267 (void);
// 0x000001B6 System.Void NetworkActionDemo::Rpc_SetString(System.String)
extern void NetworkActionDemo_Rpc_SetString_mDFE3A5D709D57FE685C243BE04A62003D9D3CA14 (void);
// 0x000001B7 System.Void NetworkActionDemo::Rpc_SetVector3(UnityEngine.Vector3)
extern void NetworkActionDemo_Rpc_SetVector3_m3E86BABD99D07E90B384942005B98522CC7CC527 (void);
// 0x000001B8 System.Void NetworkActionDemo::Rpc_SetVector3Random()
extern void NetworkActionDemo_Rpc_SetVector3Random_mFECF9BB2C316DFB29D0CE5E0B3C532C64C745B7F (void);
// 0x000001B9 System.Void NetworkActionDemo::Cmd_ServerSendByte()
extern void NetworkActionDemo_Cmd_ServerSendByte_mB888EB2604FD758B75043C5F79A3CC367F147C07 (void);
// 0x000001BA System.Void NetworkActionDemo::Action_ProcessByteData(System.Byte[])
extern void NetworkActionDemo_Action_ProcessByteData_m3C3432D783402ACBD6B496CCA97ADEA6300967F6 (void);
// 0x000001BB System.Void NetworkActionDemo::Action_SetColorRed()
extern void NetworkActionDemo_Action_SetColorRed_m82F64D3A67C6FD6DCC0A7D3A7E319CCA75F8248A (void);
// 0x000001BC System.Void NetworkActionDemo::Action_SetColorGreen()
extern void NetworkActionDemo_Action_SetColorGreen_m8C420BD785548EC9EAF09DF64C99565E285F510C (void);
// 0x000001BD System.Void NetworkActionDemo::Action_SetInt(System.Int32)
extern void NetworkActionDemo_Action_SetInt_m2F4D4153F43296823AE9B31CE160CEFC5D337AF2 (void);
// 0x000001BE System.Void NetworkActionDemo::Action_SetFloat(System.Single)
extern void NetworkActionDemo_Action_SetFloat_m5E7947E47E2D3B3EE5DDD4B82D6929F4BDBF7CFC (void);
// 0x000001BF System.Void NetworkActionDemo::Action_SetBool(System.Boolean)
extern void NetworkActionDemo_Action_SetBool_mC80C9D091E26CC8D5C644F2E2723EE41A532FA56 (void);
// 0x000001C0 System.Void NetworkActionDemo::Action_SetString(System.String)
extern void NetworkActionDemo_Action_SetString_m7780089255ED01EC5D4EA2F93C43E5B5D8289C4E (void);
// 0x000001C1 System.Void NetworkActionDemo::Action_SetVector3(UnityEngine.Vector3)
extern void NetworkActionDemo_Action_SetVector3_m0327530139A146D5AA96E1D6506FD4A434A56E4D (void);
// 0x000001C2 System.Void NetworkActionDemo::.ctor()
extern void NetworkActionDemo__ctor_mDC108827B590B45CBA0C048A4BDE53231BF7075A (void);
// 0x000001C3 System.Void NetworkActionServer::Awake()
extern void NetworkActionServer_Awake_mFF5CC68AE73711BE3157B2C39C383B47D7CCB8DB (void);
// 0x000001C4 System.Void NetworkActionServer::Action_AddCmd(System.String)
extern void NetworkActionServer_Action_AddCmd_mF4D43D4B498051C922B889049A3469400E8F3FE2 (void);
// 0x000001C5 System.Void NetworkActionServer::Action_AddCmd(System.Byte[])
extern void NetworkActionServer_Action_AddCmd_m3E6B404027169B68E8483D392D39C7AFE8EE662B (void);
// 0x000001C6 System.Collections.IEnumerator NetworkActionServer::NetworkServerStart()
extern void NetworkActionServer_NetworkServerStart_m2688A65E422E89F6F3216E9FB7FEBA8914DC9EFF (void);
// 0x000001C7 System.Void NetworkActionServer::UdpReceiveCallback(System.IAsyncResult)
extern void NetworkActionServer_UdpReceiveCallback_m9AF08AAAD3055786AD8B497BB876348C7F397603 (void);
// 0x000001C8 System.Void NetworkActionServer::StopServerListener()
extern void NetworkActionServer_StopServerListener_m37502E31CAA345E65E6C2C3E858382F79F3BBBCB (void);
// 0x000001C9 System.Void NetworkActionServer::Start()
extern void NetworkActionServer_Start_m04046C3163907AF3B7D948BB9E8FE9946BE3A275 (void);
// 0x000001CA System.Void NetworkActionServer::Update()
extern void NetworkActionServer_Update_m95B586DE2395189CBBC30E9600E5E01B20904C8A (void);
// 0x000001CB System.Collections.IEnumerator NetworkActionServer::initServer()
extern void NetworkActionServer_initServer_m37F92E6AEFAED7576D00B747B7D3A16336BD7277 (void);
// 0x000001CC System.Void NetworkActionServer::AcceptCallback(System.IAsyncResult)
extern void NetworkActionServer_AcceptCallback_m8704F3571C35364A116135F77AB380261A7569DF (void);
// 0x000001CD System.Collections.IEnumerator NetworkActionServer::senderCOR()
extern void NetworkActionServer_senderCOR_m58BC12CB2B4A1257A332B462963F5651152AACFD (void);
// 0x000001CE System.Void NetworkActionServer::Sender()
extern void NetworkActionServer_Sender_m32027FCFC7F536EB5793330EBBD3F1980A1F8AE3 (void);
// 0x000001CF System.Void NetworkActionServer::byteLengthToDataByteArray(System.Int32,System.Byte[])
extern void NetworkActionServer_byteLengthToDataByteArray_m95DB6C4D211DB8FCC639C7E4EC2D11FA353870B5 (void);
// 0x000001D0 System.Void NetworkActionServer::StreamWrite(System.Net.Sockets.NetworkStream,System.Byte[],System.Byte[])
extern void NetworkActionServer_StreamWrite_m302C090AF1DC2294A58073027D796E8511378391 (void);
// 0x000001D1 System.Void NetworkActionServer::RemoveClientConnection(System.Int32)
extern void NetworkActionServer_RemoveClientConnection_mBCA3F635EC6D407D301D66B1F09445C0B282A58E (void);
// 0x000001D2 System.Void NetworkActionServer::LOG(System.String)
extern void NetworkActionServer_LOG_m4475F2FAD5E9BF5C85EA5B518D6D0F6815E21673 (void);
// 0x000001D3 System.Void NetworkActionServer::StopAll()
extern void NetworkActionServer_StopAll_m5FBD1B20F9C906A250E03879495E4564E5233A0A (void);
// 0x000001D4 System.Void NetworkActionServer::OnApplicationQuit()
extern void NetworkActionServer_OnApplicationQuit_mE85D2AED0E5FBD89C30DF7F96B24763C025FDFE2 (void);
// 0x000001D5 System.Void NetworkActionServer::OnDisable()
extern void NetworkActionServer_OnDisable_m56A4FF4D08752497E79828A2F74C0F2BAE88341E (void);
// 0x000001D6 System.Void NetworkActionServer::OnDestroy()
extern void NetworkActionServer_OnDestroy_m1D4FFF3BBEFFC782C910B257113618D8F196810B (void);
// 0x000001D7 System.Void NetworkActionServer::OnEnable()
extern void NetworkActionServer_OnEnable_m9FAEA6E9C1AEC742BEB1F432B2E50C343AF44838 (void);
// 0x000001D8 System.Void NetworkActionServer::.ctor()
extern void NetworkActionServer__ctor_mD2D39A5FCE26BAAAE14B4E3CA6564DA3D44C1662 (void);
// 0x000001D9 System.Void NetworkActionServer::<NetworkServerStart>b__27_0()
extern void NetworkActionServer_U3CNetworkServerStartU3Eb__27_0_m13882AB3D8A7FD03E6A869746FDADB3353A83F66 (void);
// 0x000001DA System.Void NetworkActionServer::<initServer>b__35_0()
extern void NetworkActionServer_U3CinitServerU3Eb__35_0_mEF779817712A934D8D21A0CC500E2A9927DE71C7 (void);
// 0x000001DB System.Void NetworkActionServer::<initServer>b__35_1()
extern void NetworkActionServer_U3CinitServerU3Eb__35_1_mE7807EA9C8BDC3AF1363B473621B79BD3E44F0E6 (void);
// 0x000001DC System.Void NetworkActionServer/<NetworkServerStart>d__27::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__27__ctor_m2C646D9D17A8BD6E720CDB981A41A0560FAFCBB9 (void);
// 0x000001DD System.Void NetworkActionServer/<NetworkServerStart>d__27::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__27_System_IDisposable_Dispose_m533F0C08C38526569CD1EB13067B801676272478 (void);
// 0x000001DE System.Boolean NetworkActionServer/<NetworkServerStart>d__27::MoveNext()
extern void U3CNetworkServerStartU3Ed__27_MoveNext_m0002F7E9853B359B8CFC47257AD9EF1145296DBE (void);
// 0x000001DF System.Object NetworkActionServer/<NetworkServerStart>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75759CFC10015BECC7F79EB2A53F87802AC7A108 (void);
// 0x000001E0 System.Void NetworkActionServer/<NetworkServerStart>d__27::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_Reset_m96F2BF1B9713688F24E984C192AEA8367D15E200 (void);
// 0x000001E1 System.Object NetworkActionServer/<NetworkServerStart>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_get_Current_m34FEE03B5E5A351BDE9BAC79F515334DFA8C8B3C (void);
// 0x000001E2 System.Void NetworkActionServer/<initServer>d__35::.ctor(System.Int32)
extern void U3CinitServerU3Ed__35__ctor_m35EDCFE7B1FB0BA4D7CE2CEE71918DF3035645A3 (void);
// 0x000001E3 System.Void NetworkActionServer/<initServer>d__35::System.IDisposable.Dispose()
extern void U3CinitServerU3Ed__35_System_IDisposable_Dispose_m1538E68F5057A36C9F641C716590E4225775C31D (void);
// 0x000001E4 System.Boolean NetworkActionServer/<initServer>d__35::MoveNext()
extern void U3CinitServerU3Ed__35_MoveNext_mB2B3704B8B39D8257922A44E4987DF846C584E96 (void);
// 0x000001E5 System.Object NetworkActionServer/<initServer>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CinitServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13187C7D067445A8FD48F5BD8F19F5D741173A7D (void);
// 0x000001E6 System.Void NetworkActionServer/<initServer>d__35::System.Collections.IEnumerator.Reset()
extern void U3CinitServerU3Ed__35_System_Collections_IEnumerator_Reset_m82D955E151C89F40E20B4E37E73A649492DCA561 (void);
// 0x000001E7 System.Object NetworkActionServer/<initServer>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CinitServerU3Ed__35_System_Collections_IEnumerator_get_Current_mDB4BD06A0F8DD85C73AC1854C292074FE10DF82B (void);
// 0x000001E8 System.Void NetworkActionServer/<>c__DisplayClass37_0::.ctor()
extern void U3CU3Ec__DisplayClass37_0__ctor_m226F9B77215E54E404F92C6753B0D482B8EB8A8A (void);
// 0x000001E9 System.Void NetworkActionServer/<>c__DisplayClass37_0::<senderCOR>b__0()
extern void U3CU3Ec__DisplayClass37_0_U3CsenderCORU3Eb__0_m25BD754690CE381A95838D867D206E015D13C1D0 (void);
// 0x000001EA System.Void NetworkActionServer/<senderCOR>d__37::.ctor(System.Int32)
extern void U3CsenderCORU3Ed__37__ctor_mF334575352C3321DE1D342411B3ECB9CBD1EA96E (void);
// 0x000001EB System.Void NetworkActionServer/<senderCOR>d__37::System.IDisposable.Dispose()
extern void U3CsenderCORU3Ed__37_System_IDisposable_Dispose_mF4E8998740261CA0165CF75F1264077EB0A3E847 (void);
// 0x000001EC System.Boolean NetworkActionServer/<senderCOR>d__37::MoveNext()
extern void U3CsenderCORU3Ed__37_MoveNext_mC9FB2083E84C2C4231BA379ABD830AB63EF6FB6E (void);
// 0x000001ED System.Object NetworkActionServer/<senderCOR>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CsenderCORU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF22D6865B20A1B465C339C8E4E2D99F443A51415 (void);
// 0x000001EE System.Void NetworkActionServer/<senderCOR>d__37::System.Collections.IEnumerator.Reset()
extern void U3CsenderCORU3Ed__37_System_Collections_IEnumerator_Reset_mDC28498A4A797A7813650075FD9A3C8CC638303C (void);
// 0x000001EF System.Object NetworkActionServer/<senderCOR>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CsenderCORU3Ed__37_System_Collections_IEnumerator_get_Current_m267C4C53E001D2589C1072BCF06FC874BB7780D3 (void);
// 0x000001F0 System.Void NetworkActions_Debug::Action_TextUpdate(System.String)
extern void NetworkActions_Debug_Action_TextUpdate_m89583A78522D14B92FBE1F3002AB2AF05947249F (void);
// 0x000001F1 System.Void NetworkActions_Debug::Start()
extern void NetworkActions_Debug_Start_m2A553719B77C9C9B2DFA7D86C94BF26C803F23F8 (void);
// 0x000001F2 System.Void NetworkActions_Debug::Update()
extern void NetworkActions_Debug_Update_mE1F9C9C264509B369FC8C88A81F8ACB286CE680F (void);
// 0x000001F3 System.Void NetworkActions_Debug::.ctor()
extern void NetworkActions_Debug__ctor_mA59D1D164AC95AA2ECE2EE69748A4F1BDCDD0578 (void);
// 0x000001F4 System.String NetworkDiscovery::LocalIPAddress()
extern void NetworkDiscovery_LocalIPAddress_m899D97D8486722BDB44FF5DC21938AD414E9FDDA (void);
// 0x000001F5 System.Void NetworkDiscovery::SetStreamingPort(System.Int32)
extern void NetworkDiscovery_SetStreamingPort_m034B46BE8E395BC77691EFF405E2EAA044B128B7 (void);
// 0x000001F6 System.Void NetworkDiscovery::Action_SetIsStreaming(System.Boolean)
extern void NetworkDiscovery_Action_SetIsStreaming_m26F1D10B3721EE621C9D6B26F99EAA69C8D67B67 (void);
// 0x000001F7 System.Void NetworkDiscovery::Start()
extern void NetworkDiscovery_Start_mA4ECDDCD6F9ED39958B4CCF8925D194B42F6AA5D (void);
// 0x000001F8 System.Void NetworkDiscovery::Update()
extern void NetworkDiscovery_Update_m5F90B443785D7576373A6B5A766224D8B2584EA2 (void);
// 0x000001F9 System.Void NetworkDiscovery::Action_StartServer()
extern void NetworkDiscovery_Action_StartServer_m40A95A1B427BCEEDE75993C4C08EB1E02684985C (void);
// 0x000001FA System.Void NetworkDiscovery::Action_StartClient()
extern void NetworkDiscovery_Action_StartClient_m7E87F3ECE1065853ED805D304BB79B4EF9AEBFFC (void);
// 0x000001FB System.Collections.IEnumerator NetworkDiscovery::NetworkServerStart()
extern void NetworkDiscovery_NetworkServerStart_mAA1A2A539471914D5C3C6CEE4BE22303725132E7 (void);
// 0x000001FC System.Void NetworkDiscovery::UdpReceiveCallback(System.IAsyncResult)
extern void NetworkDiscovery_UdpReceiveCallback_mC09D6C0E64AE8BDBD498ECD679C1D6908EA8ADB6 (void);
// 0x000001FD System.Collections.IEnumerator NetworkDiscovery::NetworkClientStart()
extern void NetworkDiscovery_NetworkClientStart_mA6CE469839598282D32F834948855D9382CD1A08 (void);
// 0x000001FE System.Void NetworkDiscovery::DebugLog(System.String)
extern void NetworkDiscovery_DebugLog_m2002FB01A4CFC4093E030EBE8E098C2672415AFB (void);
// 0x000001FF System.Void NetworkDiscovery::OnApplicationQuit()
extern void NetworkDiscovery_OnApplicationQuit_m840B5586B84CBF8894E7AE478226AE2EACA0CD78 (void);
// 0x00000200 System.Void NetworkDiscovery::OnDisable()
extern void NetworkDiscovery_OnDisable_mFFA9C6E77731585FCACE941A17123127DEE0AF8D (void);
// 0x00000201 System.Void NetworkDiscovery::OnDestroy()
extern void NetworkDiscovery_OnDestroy_m41EDDE21ADF9E6A5AC08F82F0A4D5170446E8DF7 (void);
// 0x00000202 System.Void NetworkDiscovery::OnEnable()
extern void NetworkDiscovery_OnEnable_m6F3CD3D70AF7E31F8C92019795E2077EA22A526D (void);
// 0x00000203 System.Void NetworkDiscovery::StartAll()
extern void NetworkDiscovery_StartAll_m8E24754B92576DD7E43AAF2528D41B26DF49B280 (void);
// 0x00000204 System.Void NetworkDiscovery::StopAll()
extern void NetworkDiscovery_StopAll_mC4B8203A989CD055E7FEF559E3FC157C967229DF (void);
// 0x00000205 System.Void NetworkDiscovery::.ctor()
extern void NetworkDiscovery__ctor_m0FC92465AC4A86371F98B088168111ABED326F45 (void);
// 0x00000206 System.Void NetworkDiscovery::<NetworkServerStart>b__25_0()
extern void NetworkDiscovery_U3CNetworkServerStartU3Eb__25_0_m4E0DC9FDE405ED9CF764E15035667AE5D4334BFC (void);
// 0x00000207 System.Void NetworkDiscovery/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m203DA6941EB9343E3BE68B0437421ECD10221663 (void);
// 0x00000208 System.Void NetworkDiscovery/<>c__DisplayClass25_0::<NetworkServerStart>b__1()
extern void U3CU3Ec__DisplayClass25_0_U3CNetworkServerStartU3Eb__1_m453275FBA3251421E5FCBC1DC748BCCFF8D676D7 (void);
// 0x00000209 System.Void NetworkDiscovery/<NetworkServerStart>d__25::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__25__ctor_mFAA855ABF9E5F50C4CF3325F5555839E75776051 (void);
// 0x0000020A System.Void NetworkDiscovery/<NetworkServerStart>d__25::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__25_System_IDisposable_Dispose_m72F3867FCB52544BF1AB5D70DC37D669A688DA6F (void);
// 0x0000020B System.Boolean NetworkDiscovery/<NetworkServerStart>d__25::MoveNext()
extern void U3CNetworkServerStartU3Ed__25_MoveNext_mAA6A0384BEEA5603C22AFA4B9CECA514CCCAA93C (void);
// 0x0000020C System.Object NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C21120D45AFB33F348C5C03EF52D19C77B07659 (void);
// 0x0000020D System.Void NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_Reset_mEC78230C8DBA48F0728BC1A4BA4DDA029FE4B1B9 (void);
// 0x0000020E System.Object NetworkDiscovery/<NetworkServerStart>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_get_Current_m924B735731B289A454C795548B370C172FC61CBF (void);
// 0x0000020F System.Void NetworkDiscovery/<>c__DisplayClass28_0::.ctor()
extern void U3CU3Ec__DisplayClass28_0__ctor_m05A2ADD61078628E2E713C390F4A96EEF1DA981A (void);
// 0x00000210 System.Void NetworkDiscovery/<>c__DisplayClass28_0::<NetworkClientStart>b__0()
extern void U3CU3Ec__DisplayClass28_0_U3CNetworkClientStartU3Eb__0_m22980F80AA34CA5B3B6100EA625DD6411B6F34EF (void);
// 0x00000211 System.Void NetworkDiscovery/<>c__DisplayClass28_1::.ctor()
extern void U3CU3Ec__DisplayClass28_1__ctor_mBD315E3EF46F8A35CD9B5F81C1E32F6CAF833FB4 (void);
// 0x00000212 System.Void NetworkDiscovery/<>c__DisplayClass28_1::<NetworkClientStart>b__1()
extern void U3CU3Ec__DisplayClass28_1_U3CNetworkClientStartU3Eb__1_mCA5B2A89C47BE3102A555E40465E7FB7708277B7 (void);
// 0x00000213 System.Void NetworkDiscovery/<NetworkClientStart>d__28::.ctor(System.Int32)
extern void U3CNetworkClientStartU3Ed__28__ctor_m588398FEA5BB59B827665F538B0C67CC82ED61D1 (void);
// 0x00000214 System.Void NetworkDiscovery/<NetworkClientStart>d__28::System.IDisposable.Dispose()
extern void U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mFB47488CB857B037562EE2879797882A285F6044 (void);
// 0x00000215 System.Boolean NetworkDiscovery/<NetworkClientStart>d__28::MoveNext()
extern void U3CNetworkClientStartU3Ed__28_MoveNext_mE2992BFB46A9D1422694FD490D3472740DAB2356 (void);
// 0x00000216 System.Object NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F8FB0706D98F36F5870A48944ED8AF73ACD41AE (void);
// 0x00000217 System.Void NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_m3E308821787605440D93A440EDD66A5F852C22F2 (void);
// 0x00000218 System.Object NetworkDiscovery/<NetworkClientStart>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_m7EC0795415E3F366BCE7F0B6E920D6299E26BBE0 (void);
// 0x00000219 System.Void FMNetwork_Demo::Action_ProcessStringData(System.String)
extern void FMNetwork_Demo_Action_ProcessStringData_m7A2886FD3653BC709B033256872A04C17CEE2272 (void);
// 0x0000021A System.Void FMNetwork_Demo::Action_ProcessByteData(System.Byte[])
extern void FMNetwork_Demo_Action_ProcessByteData_mC38EB4AFE7746483DA6D88A635DD6D1F45238EF2 (void);
// 0x0000021B System.Void FMNetwork_Demo::Action_ShowRawByteLength(System.Byte[])
extern void FMNetwork_Demo_Action_ShowRawByteLength_m92F49EB9840876C51961CDB7BC026EFFBEE4F314 (void);
// 0x0000021C System.Void FMNetwork_Demo::Start()
extern void FMNetwork_Demo_Start_mD8433EB4B7A4A007090CC49906F40D8D1CCF8A65 (void);
// 0x0000021D System.Void FMNetwork_Demo::Action_SetTargetIP(System.String)
extern void FMNetwork_Demo_Action_SetTargetIP_mED976053F751BA1E6D08C813293D05D2D88F8842 (void);
// 0x0000021E System.Void FMNetwork_Demo::Action_SendRandomLargeFile()
extern void FMNetwork_Demo_Action_SendRandomLargeFile_mC543B8FA119F65E56C07D670BDF7C60D7498787C (void);
// 0x0000021F System.Void FMNetwork_Demo::Action_DemoReceivedLargeFile(System.Byte[])
extern void FMNetwork_Demo_Action_DemoReceivedLargeFile_m8825524BBE69F10E24F95E39F5AF69DBD0D6E588 (void);
// 0x00000220 System.Void FMNetwork_Demo::Update()
extern void FMNetwork_Demo_Update_m2B4BA43EB9BF273EF89EE5B1A6E332C160DFF23B (void);
// 0x00000221 System.Void FMNetwork_Demo::Action_SendByteToAll(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToAll_mE3BE1EAAC7F2B036DC2DF18B198C89B32F3F8BA8 (void);
// 0x00000222 System.Void FMNetwork_Demo::Action_SendByteToServer(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToServer_m4FC261F8F2030734782D4DD9A5540379D09D900E (void);
// 0x00000223 System.Void FMNetwork_Demo::Action_SendByteToOthers(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToOthers_m7F72C78765E3DD638E6B7DD879415179D72C6505 (void);
// 0x00000224 System.Void FMNetwork_Demo::Action_SendByteToTarget(System.Int32)
extern void FMNetwork_Demo_Action_SendByteToTarget_m622243264F3EE20F1089A8C88364300C06D9B2AF (void);
// 0x00000225 System.Void FMNetwork_Demo::Action_SendTextToAll(System.String)
extern void FMNetwork_Demo_Action_SendTextToAll_m006FA91BD4E2DAEF7CBECA9886F0D5490916DF66 (void);
// 0x00000226 System.Void FMNetwork_Demo::Action_SendTextToServer(System.String)
extern void FMNetwork_Demo_Action_SendTextToServer_mD921C4272C60C54BA1F92475AFD5773EEB195D86 (void);
// 0x00000227 System.Void FMNetwork_Demo::Action_SendTextToOthers(System.String)
extern void FMNetwork_Demo_Action_SendTextToOthers_m8DB963E93D1CEF370C6AA5BEBCE981412C02CDC9 (void);
// 0x00000228 System.Void FMNetwork_Demo::Action_SendTextToTarget(System.String)
extern void FMNetwork_Demo_Action_SendTextToTarget_m85CEB4B1C11C4D094062EA8A620064C12EC363A2 (void);
// 0x00000229 System.Void FMNetwork_Demo::Action_SendRandomTextToAll()
extern void FMNetwork_Demo_Action_SendRandomTextToAll_m9A47DFFA21B7B8E5348E1E45393F219D6819D017 (void);
// 0x0000022A System.Void FMNetwork_Demo::Action_SendRandomTextToServer()
extern void FMNetwork_Demo_Action_SendRandomTextToServer_m126799FA3059FE97D9B8F7E721DBB9ED6C459C04 (void);
// 0x0000022B System.Void FMNetwork_Demo::Action_SendRandomTextToOthers()
extern void FMNetwork_Demo_Action_SendRandomTextToOthers_m9128ED8777F95C4E099DE1F5683A3E3D61EAB3FA (void);
// 0x0000022C System.Void FMNetwork_Demo::Action_SendRandomTextToTarget()
extern void FMNetwork_Demo_Action_SendRandomTextToTarget_mEC42914678A9DE97D29FB96A7A5C2C88D8FABE58 (void);
// 0x0000022D System.Void FMNetwork_Demo::.ctor()
extern void FMNetwork_Demo__ctor_mFFE4C215963020C8A1AE5149CEF7A5726685F391 (void);
// 0x0000022E System.Void FMNetwork_DemoAnimation::Start()
extern void FMNetwork_DemoAnimation_Start_m0A02B683C9F5FD5DFBE542BE0F37D587104AEEC5 (void);
// 0x0000022F System.Void FMNetwork_DemoAnimation::Update()
extern void FMNetwork_DemoAnimation_Update_mF484EB21E515F55FF49CAA30FF5BBBD519539139 (void);
// 0x00000230 System.Void FMNetwork_DemoAnimation::.ctor()
extern void FMNetwork_DemoAnimation__ctor_mA67A4914F35832EB07A70C8CB4E7781AF20E77D7 (void);
// 0x00000231 System.Void FMClient::Action_AddPacket(System.Byte[],FMSendType)
extern void FMClient_Action_AddPacket_m6E0E5EEA363C5366B8B6AB731B8A5C47EB9F6D5C (void);
// 0x00000232 System.Void FMClient::Action_AddPacket(System.String,FMSendType)
extern void FMClient_Action_AddPacket_m580FBF73630A5FC570036342E9A5E19EA6E1521B (void);
// 0x00000233 System.Void FMClient::Action_AddPacket(System.Byte[],System.String)
extern void FMClient_Action_AddPacket_m1B65C3B1F1AF6D1A5260193A1E356D799C68E513 (void);
// 0x00000234 System.Void FMClient::Action_AddPacket(System.String,System.String)
extern void FMClient_Action_AddPacket_m71648492CC5BD5B38719AB2EB8DCB0852B06DBAF (void);
// 0x00000235 System.Void FMClient::Start()
extern void FMClient_Start_m7EF75BDA3F900D60B93D4CA48E37373CB2C98D52 (void);
// 0x00000236 System.Void FMClient::Update()
extern void FMClient_Update_mC0B21DB773CB78C46F10612F99FD856895E08D60 (void);
// 0x00000237 System.Void FMClient::Action_StartClient()
extern void FMClient_Action_StartClient_m63159EE5DB88F36D586637A1B5FD77EC0DDFAAD5 (void);
// 0x00000238 System.Collections.IEnumerator FMClient::NetworkClientStart()
extern void FMClient_NetworkClientStart_m5E27078E24274AEC8230AFA4937615DC4601548B (void);
// 0x00000239 System.Collections.IEnumerator FMClient::MainThreadSender()
extern void FMClient_MainThreadSender_m196A72144F5000BDFB23C783D44BC2DC14E812A8 (void);
// 0x0000023A System.Void FMClient::Sender()
extern void FMClient_Sender_m9858E1229F3183FCC17477AB0DA0F77F1970C883 (void);
// 0x0000023B System.Void FMClient::DebugLog(System.String)
extern void FMClient_DebugLog_mFC9A94DBE93B5A642AAFEE1AE775383D3A7153AE (void);
// 0x0000023C System.Void FMClient::OnApplicationQuit()
extern void FMClient_OnApplicationQuit_m8E0563113DC634B4093271417FB17A181C95A261 (void);
// 0x0000023D System.Void FMClient::OnDisable()
extern void FMClient_OnDisable_mB222D1B570190BC929C21218FEAEAFC3AF0D37D3 (void);
// 0x0000023E System.Void FMClient::OnDestroy()
extern void FMClient_OnDestroy_m97FEC1C579488DC892E8001B463607A3019361F6 (void);
// 0x0000023F System.Void FMClient::OnEnable()
extern void FMClient_OnEnable_mBF31D51BB11E190FDC7764AAF1E975C6995249B0 (void);
// 0x00000240 System.Void FMClient::StartAll()
extern void FMClient_StartAll_m5383DFF40567DC16485F046FC4DC389507F2898A (void);
// 0x00000241 System.Void FMClient::StopAll()
extern void FMClient_StopAll_m3AA2D18DD3E271369ED3341110D0CDB4341EE55E (void);
// 0x00000242 System.Void FMClient::.ctor()
extern void FMClient__ctor_mF3630922996062720429DAC8AAB0CF54A9A81F39 (void);
// 0x00000243 System.Void FMClient::<NetworkClientStart>b__28_0()
extern void FMClient_U3CNetworkClientStartU3Eb__28_0_mBBEBC74609803EA5F8DB22F41F196745521CBE5F (void);
// 0x00000244 System.Void FMClient::<NetworkClientStart>b__28_1()
extern void FMClient_U3CNetworkClientStartU3Eb__28_1_m43CFE2754BF5A51CA7F909A39580F2DC8E5ADF0B (void);
// 0x00000245 System.Void FMClient/<NetworkClientStart>d__28::.ctor(System.Int32)
extern void U3CNetworkClientStartU3Ed__28__ctor_mD0C05F1F39BBABC6C0D70C7B30F89B6E3D0EB2B9 (void);
// 0x00000246 System.Void FMClient/<NetworkClientStart>d__28::System.IDisposable.Dispose()
extern void U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mF2BADF251C9477A3B1E70B3DDC1B7CD5358EC8AE (void);
// 0x00000247 System.Boolean FMClient/<NetworkClientStart>d__28::MoveNext()
extern void U3CNetworkClientStartU3Ed__28_MoveNext_m88F6943556E00A3B7D997E8BE0094CD44CE7EAE5 (void);
// 0x00000248 System.Object FMClient/<NetworkClientStart>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D0C5EDA57496BEBA1CFFBAAB14A5F49E651AF5D (void);
// 0x00000249 System.Void FMClient/<NetworkClientStart>d__28::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_m24251982DA4EC8B562502778BA2EF43A417E8D50 (void);
// 0x0000024A System.Object FMClient/<NetworkClientStart>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_mC5E19B6425665B16DDFAC1F5BE654B1C5271CAAF (void);
// 0x0000024B System.Void FMClient/<MainThreadSender>d__30::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__30__ctor_mAC384BA67402126A0885FD1AD8A1954E829FBF68 (void);
// 0x0000024C System.Void FMClient/<MainThreadSender>d__30::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__30_System_IDisposable_Dispose_mAFC05B450E4C58A6E68382D4335EA28161B2E52B (void);
// 0x0000024D System.Boolean FMClient/<MainThreadSender>d__30::MoveNext()
extern void U3CMainThreadSenderU3Ed__30_MoveNext_m8B3F6AEF66AE4002C42370A90CB432B96D221D5E (void);
// 0x0000024E System.Object FMClient/<MainThreadSender>d__30::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C134E2E4D965A7F2156A4C7BF03492BD14BD8DF (void);
// 0x0000024F System.Void FMClient/<MainThreadSender>d__30::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_Reset_mB5A814F21BE08CFA7F8652A198D9B0C871BB1736 (void);
// 0x00000250 System.Object FMClient/<MainThreadSender>d__30::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_get_Current_m8A2558B8300492A3C4BB912E460E30343133F346 (void);
// 0x00000251 System.String FMNetworkManager::LocalIPAddress()
extern void FMNetworkManager_LocalIPAddress_mFF5C5D3101F680EAD193F0B8AB14E72180446D88 (void);
// 0x00000252 System.String FMNetworkManager::get_ReadLocalIPAddress()
extern void FMNetworkManager_get_ReadLocalIPAddress_m1734E265C3775E22F619B798BF4B2D1FE2E44B93 (void);
// 0x00000253 System.Void FMNetworkManager::Action_SendNetworkObjectTransform()
extern void FMNetworkManager_Action_SendNetworkObjectTransform_mB21BA52BB427E2937CB4B0D59F2AE4488058E8B4 (void);
// 0x00000254 System.Byte[] FMNetworkManager::EncodeTransformByte(UnityEngine.GameObject)
extern void FMNetworkManager_EncodeTransformByte_mBD3C68CA6F4AF2CCFD3C61CDA2CBA9F9D30B363A (void);
// 0x00000255 System.Single[] FMNetworkManager::DecodeByteToFloatArray(System.Byte[],System.Int32)
extern void FMNetworkManager_DecodeByteToFloatArray_mDA7BD48F3E977D615F91E2A0E0557AFB779691B3 (void);
// 0x00000256 System.Void FMNetworkManager::Action_SyncNetworkObjectTransform(System.Byte[])
extern void FMNetworkManager_Action_SyncNetworkObjectTransform_mBDD78DE536A6FD33666F850F4E5FEF0EC0071C70 (void);
// 0x00000257 System.Void FMNetworkManager::Action_InitAsServer()
extern void FMNetworkManager_Action_InitAsServer_mD75997CBFB7810E3BA74F3B92AC4F841500E93CD (void);
// 0x00000258 System.Void FMNetworkManager::Action_InitAsClient()
extern void FMNetworkManager_Action_InitAsClient_mCAF26F52C065A121D76F7D38A46189D68812CC26 (void);
// 0x00000259 System.Void FMNetworkManager::Action_InitStereoPi()
extern void FMNetworkManager_Action_InitStereoPi_mC46FAA867B3D47E404AD63B55CB22C4F5CC30FDA (void);
// 0x0000025A System.Void FMNetworkManager::Init()
extern void FMNetworkManager_Init_mDF26E04C4250C31C3CDF3E895D97BCA345543117 (void);
// 0x0000025B System.Void FMNetworkManager::Awake()
extern void FMNetworkManager_Awake_mD0648E4D22E948CFED1E76BF694150FBA6CCB2ED (void);
// 0x0000025C System.Void FMNetworkManager::Start()
extern void FMNetworkManager_Start_m731AE51B5EABFE66D44FB8CEE255AB457FB97262 (void);
// 0x0000025D System.Void FMNetworkManager::Update()
extern void FMNetworkManager_Update_m6F03B1E8BF42A1CE516732FBB8A4D6CEE21CA5F7 (void);
// 0x0000025E System.Void FMNetworkManager::Send(System.Byte[],FMSendType)
extern void FMNetworkManager_Send_mD1CE1D270FB72C5A021C84B173318245264AE344 (void);
// 0x0000025F System.Void FMNetworkManager::Send(System.String,FMSendType)
extern void FMNetworkManager_Send_mA1E79ACB3C1202E3683D2AB618EC6D1DC86D27F3 (void);
// 0x00000260 System.Void FMNetworkManager::SendToAll(System.Byte[])
extern void FMNetworkManager_SendToAll_mAC74B498156B51511CE7B8AD00A306E383001FCE (void);
// 0x00000261 System.Void FMNetworkManager::SendToServer(System.Byte[])
extern void FMNetworkManager_SendToServer_m680FBCA306390F1AC9CCAFA028A82AA5627FBFF6 (void);
// 0x00000262 System.Void FMNetworkManager::SendToOthers(System.Byte[])
extern void FMNetworkManager_SendToOthers_m733696729DB2551537EFD2E773A724EB8EE5CF4E (void);
// 0x00000263 System.Void FMNetworkManager::SendToAll(System.String)
extern void FMNetworkManager_SendToAll_m6ED1B08A63EE6CFA249B6CB0B876F36AD9276CAE (void);
// 0x00000264 System.Void FMNetworkManager::SendToServer(System.String)
extern void FMNetworkManager_SendToServer_mDAD550229F4DF9C8F51FA4D00616BB4E3204AFBB (void);
// 0x00000265 System.Void FMNetworkManager::SendToOthers(System.String)
extern void FMNetworkManager_SendToOthers_m76F6D91817597631893088F5D096124B6BCDF4E7 (void);
// 0x00000266 System.Void FMNetworkManager::SendToTarget(System.Byte[],System.String)
extern void FMNetworkManager_SendToTarget_m8383491A137B24E5CA3221B1E46A9AE0FEC37CA6 (void);
// 0x00000267 System.Void FMNetworkManager::SendToTarget(System.String,System.String)
extern void FMNetworkManager_SendToTarget_mB9CDABD8072BD4F65E18F3F04D50214A9D4F84D1 (void);
// 0x00000268 System.Void FMNetworkManager::Send(System.Byte[],FMSendType,System.String)
extern void FMNetworkManager_Send_m6D07FC063EF1F6AB020FD1A67653A5650F5F62CE (void);
// 0x00000269 System.Void FMNetworkManager::Send(System.String,FMSendType,System.String)
extern void FMNetworkManager_Send_m87394D847F9E011E967D68341A56259D5AFDD18D (void);
// 0x0000026A System.Void FMNetworkManager::Action_ReloadScene()
extern void FMNetworkManager_Action_ReloadScene_m428B4CE135C87C69D4A30986A69E37BADBA8030D (void);
// 0x0000026B System.Void FMNetworkManager::.ctor()
extern void FMNetworkManager__ctor_m255207BB8A01D0DCBEDACEF4FBDF1E91B5FA9050 (void);
// 0x0000026C System.Void FMNetworkManager/FMServerSettings::.ctor()
extern void FMServerSettings__ctor_m64A2752379B3561CF097041AA6099A4F2C558270 (void);
// 0x0000026D System.Void FMNetworkManager/FMClientSettings::.ctor()
extern void FMClientSettings__ctor_mE0AFD7FD7FB9C13E7097068E56458ADD1094B0FC (void);
// 0x0000026E System.Void FMNetworkManager/FMStereoPiSettings::.ctor()
extern void FMStereoPiSettings__ctor_m4D1EFA321536E48A1A6A9AC4C477BDC2E8BB93AA (void);
// 0x0000026F System.Void FMServer::Action_CheckClientStatus(System.String)
extern void FMServer_Action_CheckClientStatus_m697DDE3F22C5F94E96C19FA542AE624D429F5B80 (void);
// 0x00000270 System.Void FMServer::Action_AddPacket(System.Byte[],FMSendType)
extern void FMServer_Action_AddPacket_m8B6FB971E0CF10570AEA2D893F76C8FFBB48CC26 (void);
// 0x00000271 System.Void FMServer::Action_AddPacket(System.String,FMSendType)
extern void FMServer_Action_AddPacket_m07633FBC0C78F4DEB4E5213FBF8A6E7F425AE294 (void);
// 0x00000272 System.Void FMServer::Action_AddPacket(System.Byte[],System.String)
extern void FMServer_Action_AddPacket_mF05DE2811D4EFE814FAF5F09E6E57C330863E5DA (void);
// 0x00000273 System.Void FMServer::Action_AddPacket(System.String,System.String)
extern void FMServer_Action_AddPacket_m9403C788A623023BD3D60777956E616EF07E8A07 (void);
// 0x00000274 System.Void FMServer::Action_AddPacket(FMPacket)
extern void FMServer_Action_AddPacket_mF8F31D6960D0B7DB6C13862B7C9605536EFAB8D9 (void);
// 0x00000275 System.Void FMServer::Action_AddNetworkObjectPacket(System.Byte[],FMSendType)
extern void FMServer_Action_AddNetworkObjectPacket_m2BAFF3FEAE295A507FA72F9B245CA19175CE78FE (void);
// 0x00000276 System.Void FMServer::Start()
extern void FMServer_Start_m91ACB0E99A6AF95E484BA364CF6F88C643206DB9 (void);
// 0x00000277 System.Void FMServer::Update()
extern void FMServer_Update_m81796254B4462FA3D945D9AD86A2868877115C5C (void);
// 0x00000278 System.Void FMServer::Action_StartServer()
extern void FMServer_Action_StartServer_m339E23A122EF8AFCD0470F20BB1D5C593479343F (void);
// 0x00000279 System.Void FMServer::InitializeServerListener()
extern void FMServer_InitializeServerListener_m4B19FAA611DDBB51DB70F52DCBA8AADFD0DC20B8 (void);
// 0x0000027A System.Void FMServer::MulticastChecker()
extern void FMServer_MulticastChecker_m13C1C2475530617B7142AD568298BE2AE966CFB7 (void);
// 0x0000027B System.Collections.IEnumerator FMServer::MulticastCheckerCOR()
extern void FMServer_MulticastCheckerCOR_m3A46927DB28F9946F4C899AF05151ECDFBAC7B8F (void);
// 0x0000027C System.Collections.IEnumerator FMServer::NetworkServerStart()
extern void FMServer_NetworkServerStart_m53F1BB0509D324B11975F677FD07742D0973E5B8 (void);
// 0x0000027D System.Void FMServer::UdpReceiveCallback(System.IAsyncResult)
extern void FMServer_UdpReceiveCallback_mA83C94A8724BAC988799466EA13E1E27565F6F8F (void);
// 0x0000027E System.Collections.IEnumerator FMServer::MainThreadSender()
extern void FMServer_MainThreadSender_m70FD3C1A08C40D256E082E45E453BCC963A27828 (void);
// 0x0000027F System.Void FMServer::Sender()
extern void FMServer_Sender_mCE3BF4BD1427311E5F5D5820AFE31DB239D29531 (void);
// 0x00000280 System.Void FMServer::DebugLog(System.String)
extern void FMServer_DebugLog_m4CABEA630DD0DAB46FF89CC0DF8EAD4236B1E13C (void);
// 0x00000281 System.Void FMServer::OnApplicationQuit()
extern void FMServer_OnApplicationQuit_mC120710981FE52A740E7DE7407F98DEF20D0FED3 (void);
// 0x00000282 System.Void FMServer::OnDisable()
extern void FMServer_OnDisable_mFDA76628E20DB16C1C299CF6B525F3470BF5650E (void);
// 0x00000283 System.Void FMServer::OnDestroy()
extern void FMServer_OnDestroy_m754D033F470CC9DE3B5CC31A5CF4B6841553D230 (void);
// 0x00000284 System.Void FMServer::OnEnable()
extern void FMServer_OnEnable_mEE044CFBE1D9A976AAF900029C34CA170E9A0F53 (void);
// 0x00000285 System.Void FMServer::StartAll()
extern void FMServer_StartAll_m552518A1DCE75B2C60B437502696AF77B7032A3F (void);
// 0x00000286 System.Void FMServer::StopAll()
extern void FMServer_StopAll_m3B4D785EF1D5476CEB50CFF471F63FA3C6ED77F5 (void);
// 0x00000287 System.Void FMServer::.ctor()
extern void FMServer__ctor_m3F38AE880D79741720AA0CDACD5E7C2F1AF6A748 (void);
// 0x00000288 System.Void FMServer::<NetworkServerStart>b__33_0()
extern void FMServer_U3CNetworkServerStartU3Eb__33_0_m019FF8207F1A320EACED7C2B21ABA5011000D0BC (void);
// 0x00000289 System.Void FMServer::<NetworkServerStart>b__33_1()
extern void FMServer_U3CNetworkServerStartU3Eb__33_1_mB8A5642B352FF0140ACCE2724A57FDD2A3F06E5F (void);
// 0x0000028A System.Void FMServer/ConnectedClient::Send(System.Byte[])
extern void ConnectedClient_Send_m5124596DFBAA7F50F7CA750322BDDFEBC0D4A8A0 (void);
// 0x0000028B System.Void FMServer/ConnectedClient::Close()
extern void ConnectedClient_Close_m0F38D5BBADA3E8929A01703747D5F00548BF15FA (void);
// 0x0000028C System.Void FMServer/ConnectedClient::.ctor()
extern void ConnectedClient__ctor_m6BC6FA9C0877507C72C93EC74E03912402AB92BF (void);
// 0x0000028D System.Void FMServer/<MulticastCheckerCOR>d__32::.ctor(System.Int32)
extern void U3CMulticastCheckerCORU3Ed__32__ctor_mEBE29B27242A666E5DC2E35C2205FD9508AD4B56 (void);
// 0x0000028E System.Void FMServer/<MulticastCheckerCOR>d__32::System.IDisposable.Dispose()
extern void U3CMulticastCheckerCORU3Ed__32_System_IDisposable_Dispose_mED7730E279EA6D79015C9B7AA9C9A54FF8B918CE (void);
// 0x0000028F System.Boolean FMServer/<MulticastCheckerCOR>d__32::MoveNext()
extern void U3CMulticastCheckerCORU3Ed__32_MoveNext_m36C8B3214F59CA2317C98A17821A19C43AAEB535 (void);
// 0x00000290 System.Object FMServer/<MulticastCheckerCOR>d__32::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D3E8BF7F56103F88160A2C35C110293D2801A52 (void);
// 0x00000291 System.Void FMServer/<MulticastCheckerCOR>d__32::System.Collections.IEnumerator.Reset()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_Reset_mB2BFAA304CD76CBEBB072F14C2E754797C6FDD5D (void);
// 0x00000292 System.Object FMServer/<MulticastCheckerCOR>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_get_Current_mA23AE4B1D341719C24DB531D037D1434C1196159 (void);
// 0x00000293 System.Void FMServer/<NetworkServerStart>d__33::.ctor(System.Int32)
extern void U3CNetworkServerStartU3Ed__33__ctor_m972BFA66770E884FAFECCAC141E33029EE25D1FF (void);
// 0x00000294 System.Void FMServer/<NetworkServerStart>d__33::System.IDisposable.Dispose()
extern void U3CNetworkServerStartU3Ed__33_System_IDisposable_Dispose_m380952EF87F5E0FF391FA61DF5DC181C681918F3 (void);
// 0x00000295 System.Boolean FMServer/<NetworkServerStart>d__33::MoveNext()
extern void U3CNetworkServerStartU3Ed__33_MoveNext_m9341645BB9BB9D6FD2C78FEE8CB544A3097A736F (void);
// 0x00000296 System.Object FMServer/<NetworkServerStart>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m724209347BFD6AFD23815CD817EA2782FF8994C2 (void);
// 0x00000297 System.Void FMServer/<NetworkServerStart>d__33::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_Reset_mC52F22569BF00478381E1042BFEA8539C62C9AF7 (void);
// 0x00000298 System.Object FMServer/<NetworkServerStart>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_get_Current_m1C8242006350B74D05CCDFB1487A843014D0C282 (void);
// 0x00000299 System.Void FMServer/<MainThreadSender>d__35::.ctor(System.Int32)
extern void U3CMainThreadSenderU3Ed__35__ctor_m9D0709E5C8EE5A30AFBC6500387A9355BA2B6220 (void);
// 0x0000029A System.Void FMServer/<MainThreadSender>d__35::System.IDisposable.Dispose()
extern void U3CMainThreadSenderU3Ed__35_System_IDisposable_Dispose_mDC8120E6FE2B752B263CAA0006654F67108CE68B (void);
// 0x0000029B System.Boolean FMServer/<MainThreadSender>d__35::MoveNext()
extern void U3CMainThreadSenderU3Ed__35_MoveNext_m5B24123688606EDE726867C8378A5776987FE63B (void);
// 0x0000029C System.Object FMServer/<MainThreadSender>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m701C2588F8EF921C4303758C33A5CFBB85268914 (void);
// 0x0000029D System.Void FMServer/<MainThreadSender>d__35::System.Collections.IEnumerator.Reset()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_Reset_m0E92ACA965A1272BB82B14AE0D16119F8FCC51C6 (void);
// 0x0000029E System.Object FMServer/<MainThreadSender>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_get_Current_m7C6243AE661C6462A0EFA63461CE446F2106354B (void);
// 0x0000029F System.Void FMStereoPi::MulticastChecker()
extern void FMStereoPi_MulticastChecker_mE6337322229810E52A878C54106CD0D2DFD582BA (void);
// 0x000002A0 System.Collections.IEnumerator FMStereoPi::NetworkServerStartTCP()
extern void FMStereoPi_NetworkServerStartTCP_m340A32E63D94C88F5D78E8B645153A80A273FC9B (void);
// 0x000002A1 System.Collections.IEnumerator FMStereoPi::TCPReceiverCOR(System.Net.Sockets.TcpClient,System.Net.Sockets.NetworkStream)
extern void FMStereoPi_TCPReceiverCOR_mE3E982BE3AAFCAA6F0247CA91A574BC4016DBF3A (void);
// 0x000002A2 System.Collections.IEnumerator FMStereoPi::NetworkClientStartUDP()
extern void FMStereoPi_NetworkClientStartUDP_m74ED068D5CA02A0B2A4D56C103A3DF94664BD35E (void);
// 0x000002A3 System.Void FMStereoPi::Action_StartClient()
extern void FMStereoPi_Action_StartClient_m7AFFEC5FBB05195B1C061D091EF40E8B6CC3698D (void);
// 0x000002A4 System.Void FMStereoPi::Action_StopClient()
extern void FMStereoPi_Action_StopClient_m42A4914C65C39BF9BDE18B33995E7C78D0C7B866 (void);
// 0x000002A5 System.Void FMStereoPi::StartAll()
extern void FMStereoPi_StartAll_m4DD907458FFACEFCB565F0D7E54AAA84297F1F08 (void);
// 0x000002A6 System.Void FMStereoPi::StopAll()
extern void FMStereoPi_StopAll_mBFA8A3D1577D67DA86416C286C405C45DABDD1F0 (void);
// 0x000002A7 System.Void FMStereoPi::Start()
extern void FMStereoPi_Start_m5C56E43A962EB2AB3142F131873E31B6C67C1252 (void);
// 0x000002A8 System.Void FMStereoPi::Update()
extern void FMStereoPi_Update_m669664D93070B477ECB1248013E98330F84C806C (void);
// 0x000002A9 System.Void FMStereoPi::DebugLog(System.String)
extern void FMStereoPi_DebugLog_mEA2F453091F62DEB2AD6AE5A10033B3D976E1BA1 (void);
// 0x000002AA System.Void FMStereoPi::OnApplicationQuit()
extern void FMStereoPi_OnApplicationQuit_m8D66AF5414B77B0A4BCDD4FD7F90C50199261665 (void);
// 0x000002AB System.Void FMStereoPi::OnDisable()
extern void FMStereoPi_OnDisable_mE178085A7511A88A5427B1403261910E76CD5AE9 (void);
// 0x000002AC System.Void FMStereoPi::OnDestroy()
extern void FMStereoPi_OnDestroy_m05C6536B8E44AFCD384C4E2A27D51A1DA7E793C6 (void);
// 0x000002AD System.Void FMStereoPi::OnEnable()
extern void FMStereoPi_OnEnable_mA412A52C0D3D96BE2849529461109A93A32B861C (void);
// 0x000002AE System.Void FMStereoPi::.ctor()
extern void FMStereoPi__ctor_mC4389F4793CA039689A359AAE34B12E48B16A6CB (void);
// 0x000002AF System.Void FMStereoPi::<NetworkServerStartTCP>b__17_0()
extern void FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_0_mDE846926D20096AC69C860B2CC892E9EB5201017 (void);
// 0x000002B0 System.Void FMStereoPi::<NetworkServerStartTCP>b__17_1()
extern void FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_1_mD393BF6020BF61F547EDF0B19A7A5315D8C81799 (void);
// 0x000002B1 System.Void FMStereoPi::<NetworkClientStartUDP>b__19_0()
extern void FMStereoPi_U3CNetworkClientStartUDPU3Eb__19_0_m31CE99B57F1168691E3675227BCE9D7735B38D8F (void);
// 0x000002B2 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::.ctor(System.Int32)
extern void U3CNetworkServerStartTCPU3Ed__17__ctor_mD2F4B52884AAAC2EAACEB55877EEB9C49CFAB6D3 (void);
// 0x000002B3 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::System.IDisposable.Dispose()
extern void U3CNetworkServerStartTCPU3Ed__17_System_IDisposable_Dispose_m27ECC5885334F25784BD9A2BF7E1F1A8CCDAA3AB (void);
// 0x000002B4 System.Boolean FMStereoPi/<NetworkServerStartTCP>d__17::MoveNext()
extern void U3CNetworkServerStartTCPU3Ed__17_MoveNext_mC7925593F077ECE3B5574BDFAD4DEF45D51FF8D6 (void);
// 0x000002B5 System.Object FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3E5EE41438D96AB95C8ABE8B77BB8A86E0005D1 (void);
// 0x000002B6 System.Void FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.IEnumerator.Reset()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_Reset_m576FFCB7C06814A17D54A5C3F36AE30327C79F3F (void);
// 0x000002B7 System.Object FMStereoPi/<NetworkServerStartTCP>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_get_Current_m3ACAB37C2F1469D1DC9EF6B0A6C483C2CC618FD3 (void);
// 0x000002B8 System.Void FMStereoPi/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m30EACA80AEB3694A078D9ECFCBAD51AB8E8AFD30 (void);
// 0x000002B9 System.Void FMStereoPi/<>c__DisplayClass18_0::<TCPReceiverCOR>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CTCPReceiverCORU3Eb__0_m1562A138DEF249C8159E454A2CA88EFAB97DE843 (void);
// 0x000002BA System.Void FMStereoPi/<TCPReceiverCOR>d__18::.ctor(System.Int32)
extern void U3CTCPReceiverCORU3Ed__18__ctor_mBB60D670782FA0DA8072E1EF545AE0E2CCA8824A (void);
// 0x000002BB System.Void FMStereoPi/<TCPReceiverCOR>d__18::System.IDisposable.Dispose()
extern void U3CTCPReceiverCORU3Ed__18_System_IDisposable_Dispose_m80F58A7EFDFE8F6D6FC425EE4A4B23123FF35939 (void);
// 0x000002BC System.Boolean FMStereoPi/<TCPReceiverCOR>d__18::MoveNext()
extern void U3CTCPReceiverCORU3Ed__18_MoveNext_mFA1B27720A1B2BA4683FD60B6DE24B2A189640B0 (void);
// 0x000002BD System.Object FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m097C55C143D8A44C8E33887270757FB86F5641CE (void);
// 0x000002BE System.Void FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.IEnumerator.Reset()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_Reset_m8C6625E3E247A26AAD989B44FD4F284701951CE7 (void);
// 0x000002BF System.Object FMStereoPi/<TCPReceiverCOR>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_get_Current_m4927905AEEA2663D8194895244E5C56583E9EF90 (void);
// 0x000002C0 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::.ctor(System.Int32)
extern void U3CNetworkClientStartUDPU3Ed__19__ctor_m3AB53F056F41B2ACABAF62903AB2415C6EDE2860 (void);
// 0x000002C1 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::System.IDisposable.Dispose()
extern void U3CNetworkClientStartUDPU3Ed__19_System_IDisposable_Dispose_m545790542E4EC408530C6BCDD0E9439A1A2CEA47 (void);
// 0x000002C2 System.Boolean FMStereoPi/<NetworkClientStartUDP>d__19::MoveNext()
extern void U3CNetworkClientStartUDPU3Ed__19_MoveNext_m41F1F39461142620FCBED9A8A72E0DEDC2193520 (void);
// 0x000002C3 System.Object FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85409D5AA7C4F3CBACA5B77FC376B696C19D5100 (void);
// 0x000002C4 System.Void FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.IEnumerator.Reset()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_Reset_mFD196C3E0E04341F0AB7074F52AE7BFF496EDEDB (void);
// 0x000002C5 System.Object FMStereoPi/<NetworkClientStartUDP>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_get_Current_m649785D485B0CB7DBCBFB913A00029BC129DA1FF (void);
// 0x000002C6 System.Void LargeFileDecoder::Start()
extern void LargeFileDecoder_Start_m5E787EE1377276EC1ECAC717F61F2D41251C61D4 (void);
// 0x000002C7 System.Void LargeFileDecoder::Action_ProcessData(System.Byte[])
extern void LargeFileDecoder_Action_ProcessData_mEB5A97A33C2FC65E98D093904347A4FA54FD2D5E (void);
// 0x000002C8 System.Collections.IEnumerator LargeFileDecoder::ProcessData(System.Byte[])
extern void LargeFileDecoder_ProcessData_mEBF37621E853E17BB32619C02ED43CAB8C802369 (void);
// 0x000002C9 System.Void LargeFileDecoder::OnDisable()
extern void LargeFileDecoder_OnDisable_m2C226054369BBFC62926B83C6EE42A3AA7B586DC (void);
// 0x000002CA System.Void LargeFileDecoder::.ctor()
extern void LargeFileDecoder__ctor_mA7F48FB18934D4C52414C5BF8FB3A2031F214620 (void);
// 0x000002CB System.Void LargeFileDecoder/<ProcessData>d__9::.ctor(System.Int32)
extern void U3CProcessDataU3Ed__9__ctor_m1D8753BB750B2BDBF34D090FC1AF8F54296A37A7 (void);
// 0x000002CC System.Void LargeFileDecoder/<ProcessData>d__9::System.IDisposable.Dispose()
extern void U3CProcessDataU3Ed__9_System_IDisposable_Dispose_mB8EE0592DB225F64A12DA26C9C2A96AC5994BB18 (void);
// 0x000002CD System.Boolean LargeFileDecoder/<ProcessData>d__9::MoveNext()
extern void U3CProcessDataU3Ed__9_MoveNext_m9B87121401483778192C4EF68E21436F4A85ADFB (void);
// 0x000002CE System.Object LargeFileDecoder/<ProcessData>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CProcessDataU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D0BA7875E1CBA0DAF864D3C4F1DCCAAC19AB24D (void);
// 0x000002CF System.Void LargeFileDecoder/<ProcessData>d__9::System.Collections.IEnumerator.Reset()
extern void U3CProcessDataU3Ed__9_System_Collections_IEnumerator_Reset_mE2F20B95EACBEB68F53ACEDDD1F6FECB7DD02488 (void);
// 0x000002D0 System.Object LargeFileDecoder/<ProcessData>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CProcessDataU3Ed__9_System_Collections_IEnumerator_get_Current_m83F846C2A6C24061431D5F6CCF8E1E096430590D (void);
// 0x000002D1 System.Void LargeFileEncoder::Start()
extern void LargeFileEncoder_Start_m4B8724341015C59F5476826EA1266AFB51B0F992 (void);
// 0x000002D2 System.Void LargeFileEncoder::Action_SendLargeByte(System.Byte[])
extern void LargeFileEncoder_Action_SendLargeByte_m388FA72B2A0E4638C55FD7327ED14FF5DCB73D72 (void);
// 0x000002D3 System.Collections.IEnumerator LargeFileEncoder::SenderCOR(System.Byte[])
extern void LargeFileEncoder_SenderCOR_mE44CB8852EFE29B44BE72ED1A0B777B585CE0394 (void);
// 0x000002D4 System.Void LargeFileEncoder::OnDisable()
extern void LargeFileEncoder_OnDisable_mB07BA3442A13E8170209FB15D71DFA60D6B68F57 (void);
// 0x000002D5 System.Void LargeFileEncoder::OnApplicationQuit()
extern void LargeFileEncoder_OnApplicationQuit_m5C59A630FBE659DA934B25ED543915979A810ABA (void);
// 0x000002D6 System.Void LargeFileEncoder::OnDestroy()
extern void LargeFileEncoder_OnDestroy_m5AF3FD1F49F86FEF9E54C96E5FBA8DB51D79D7F1 (void);
// 0x000002D7 System.Void LargeFileEncoder::StopAll()
extern void LargeFileEncoder_StopAll_m21F4C498FEB1A921681BE3FA3D95957BB0F79E24 (void);
// 0x000002D8 System.Void LargeFileEncoder::.ctor()
extern void LargeFileEncoder__ctor_m4E846FFA06F2B85DC7B34704AD0E8D54A10F41C4 (void);
// 0x000002D9 System.Void LargeFileEncoder/<SenderCOR>d__10::.ctor(System.Int32)
extern void U3CSenderCORU3Ed__10__ctor_m979630D72EB333BD3D559E23D89A6C41034D73F3 (void);
// 0x000002DA System.Void LargeFileEncoder/<SenderCOR>d__10::System.IDisposable.Dispose()
extern void U3CSenderCORU3Ed__10_System_IDisposable_Dispose_mDCFF2339BE6134EADF67579FE2B4E5A0995BABF1 (void);
// 0x000002DB System.Boolean LargeFileEncoder/<SenderCOR>d__10::MoveNext()
extern void U3CSenderCORU3Ed__10_MoveNext_mF4F366F5F8BCC982F47507B4E827ABA1A61EC626 (void);
// 0x000002DC System.Object LargeFileEncoder/<SenderCOR>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CSenderCORU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6718863C0F3BF554552C0E21BF45F5018F8D24C (void);
// 0x000002DD System.Void LargeFileEncoder/<SenderCOR>d__10::System.Collections.IEnumerator.Reset()
extern void U3CSenderCORU3Ed__10_System_Collections_IEnumerator_Reset_m73B5BEC5CCEE11A1012F906BAEB1A6913FB4697A (void);
// 0x000002DE System.Object LargeFileEncoder/<SenderCOR>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CSenderCORU3Ed__10_System_Collections_IEnumerator_get_Current_m928A9082517545E01F08ABDE8D4F85B8489208B4 (void);
// 0x000002DF System.Void FMWebSocketNetwork_debug::Action_SendStringAll(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringAll_m710AA9D18C9678FBA3E386242EE961E596B2738C (void);
// 0x000002E0 System.Void FMWebSocketNetwork_debug::Action_SendStringServer(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringServer_m3DB4A603C84EC6ABB5CEB4BA1D12D37065C75055 (void);
// 0x000002E1 System.Void FMWebSocketNetwork_debug::Action_SendStringOthers(System.String)
extern void FMWebSocketNetwork_debug_Action_SendStringOthers_mE93AF6930EA1511FCC58E3781578EB780DB5609E (void);
// 0x000002E2 System.Void FMWebSocketNetwork_debug::Action_SendByteAll()
extern void FMWebSocketNetwork_debug_Action_SendByteAll_m64C0D4E3AA4E5072F2DBC53D27B50CD0638130F5 (void);
// 0x000002E3 System.Void FMWebSocketNetwork_debug::Action_SendByteServer()
extern void FMWebSocketNetwork_debug_Action_SendByteServer_m1B22593913893A54DE6F962F4443E734C455326E (void);
// 0x000002E4 System.Void FMWebSocketNetwork_debug::Action_SendByteOthers()
extern void FMWebSocketNetwork_debug_Action_SendByteOthers_mA3692507372718FE9170E4886799F75F8A5B93AF (void);
// 0x000002E5 System.Void FMWebSocketNetwork_debug::Start()
extern void FMWebSocketNetwork_debug_Start_m3549CDBD49A97D04753A2B15D7BA4114505722E2 (void);
// 0x000002E6 System.Void FMWebSocketNetwork_debug::Update()
extern void FMWebSocketNetwork_debug_Update_mF36F69582E94D602BBCACCA65233E7C2DFFE603C (void);
// 0x000002E7 System.Void FMWebSocketNetwork_debug::Action_OnReceivedData(System.String)
extern void FMWebSocketNetwork_debug_Action_OnReceivedData_mE613FFB040B6376DFFEFB4250DE37B0E4CC7752A (void);
// 0x000002E8 System.Void FMWebSocketNetwork_debug::Action_OnReceivedData(System.Byte[])
extern void FMWebSocketNetwork_debug_Action_OnReceivedData_m617A4C13DCA225022BF3C8AC858904C301DA69BE (void);
// 0x000002E9 System.Void FMWebSocketNetwork_debug::.ctor()
extern void FMWebSocketNetwork_debug__ctor_m6EA7FB1852EB66109002508769CCB878C5518608 (void);
// 0x000002EA System.Void FMSocketIOManager::Action_SetIP(System.String)
extern void FMSocketIOManager_Action_SetIP_m38911A13EEB9F983FDFD2B9F21671633428A8AD0 (void);
// 0x000002EB System.Void FMSocketIOManager::Action_SetPort(System.String)
extern void FMSocketIOManager_Action_SetPort_m7B100CDBA1ABA01B8154F318C49FBEE920E11EF1 (void);
// 0x000002EC System.Void FMSocketIOManager::Action_SetSslEnabled(System.Boolean)
extern void FMSocketIOManager_Action_SetSslEnabled_m7CDDEA8981B461C9137BD0CC12E8020408063501 (void);
// 0x000002ED System.Void FMSocketIOManager::Action_SetPortRequired(System.Boolean)
extern void FMSocketIOManager_Action_SetPortRequired_m8D119A6B549E65FEE8EC8445A1C3B2007662B5DE (void);
// 0x000002EE System.Void FMSocketIOManager::Action_SetSocketIORequired(System.Boolean)
extern void FMSocketIOManager_Action_SetSocketIORequired_m02269CAA46FFF3A7D3124E2C6C97E2DE6AC58CEC (void);
// 0x000002EF System.Void FMSocketIOManager::DebugLog(System.String)
extern void FMSocketIOManager_DebugLog_mF393F44FCAF797283BFDE20104402C1E2F7A2807 (void);
// 0x000002F0 System.Collections.IEnumerator FMSocketIOManager::WaitForSocketIOConnected()
extern void FMSocketIOManager_WaitForSocketIOConnected_mC9DFC3F62F87645F9CF82654280DCBC590D62BB0 (void);
// 0x000002F1 System.Void FMSocketIOManager::OnReceivedData(FMSocketIO.SocketIOEvent)
extern void FMSocketIOManager_OnReceivedData_m9DE3C68690679FCF87DEB0434F21E6CE38D90488 (void);
// 0x000002F2 System.Void FMSocketIOManager::OnReceivedData(FMSocketIOData)
extern void FMSocketIOManager_OnReceivedData_m8D9F078E65AD33C8CE6E542E7DB7A6E15ECC6386 (void);
// 0x000002F3 System.Void FMSocketIOManager::Action_OnReceivedData(System.String)
extern void FMSocketIOManager_Action_OnReceivedData_m85951BD1CAA66149C7EFFE9397BA7C72DECB6C90 (void);
// 0x000002F4 System.Void FMSocketIOManager::Action_OnReceivedData(System.Byte[])
extern void FMSocketIOManager_Action_OnReceivedData_mCB7DE948C9918409D115C764DE79BB2C7D78C866 (void);
// 0x000002F5 System.Void FMSocketIOManager::Send(System.String,FMSocketIOEmitType)
extern void FMSocketIOManager_Send_m20E19ECB679394E9F69B344469D8B684E3729342 (void);
// 0x000002F6 System.Void FMSocketIOManager::Send(System.Byte[],FMSocketIOEmitType)
extern void FMSocketIOManager_Send_m3F7FD03DE01D2982ABDB56E5E5B37E4B163EEEED (void);
// 0x000002F7 System.Void FMSocketIOManager::SendToAll(System.Byte[])
extern void FMSocketIOManager_SendToAll_m4C15155F9CBF231D3C1944A54FE55A7510D14883 (void);
// 0x000002F8 System.Void FMSocketIOManager::SendToServer(System.Byte[])
extern void FMSocketIOManager_SendToServer_m1AB99649FBD70015F4787CDFFA1B71A719AFC4CF (void);
// 0x000002F9 System.Void FMSocketIOManager::SendToOthers(System.Byte[])
extern void FMSocketIOManager_SendToOthers_mD8F13D9F1E857F9FDAC64135CBD3173E72D4C7B6 (void);
// 0x000002FA System.Void FMSocketIOManager::SendToAll(System.String)
extern void FMSocketIOManager_SendToAll_mE136CA2E7413C8FED36BCF1198E7258AF575BDEA (void);
// 0x000002FB System.Void FMSocketIOManager::SendToServer(System.String)
extern void FMSocketIOManager_SendToServer_mED9E12DF8192F5615B5A4EEC901D6F0E733FF40D (void);
// 0x000002FC System.Void FMSocketIOManager::SendToOthers(System.String)
extern void FMSocketIOManager_SendToOthers_mB53EAB2B343442048B4B867DA052C1DFE6851391 (void);
// 0x000002FD System.Void FMSocketIOManager::Awake()
extern void FMSocketIOManager_Awake_m7AD2C757EC3E419F2CB37C288746EA6DDD5BE465 (void);
// 0x000002FE System.Void FMSocketIOManager::Start()
extern void FMSocketIOManager_Start_m058F620AD42078333D93DB3DAC1AFBB4C4363DE2 (void);
// 0x000002FF System.Void FMSocketIOManager::Update()
extern void FMSocketIOManager_Update_m093EE4B34A0F8C8F8B20E7E93D8974676E5B07A8 (void);
// 0x00000300 System.Void FMSocketIOManager::InitAsServer()
extern void FMSocketIOManager_InitAsServer_mBDA50ED9CA25055F865FD0D596B3D53B398C272E (void);
// 0x00000301 System.Void FMSocketIOManager::InitAsClient()
extern void FMSocketIOManager_InitAsClient_m0C21E1BFA6FCFCE9ADE2000568C5EE5EF87985B1 (void);
// 0x00000302 System.Void FMSocketIOManager::Init()
extern void FMSocketIOManager_Init_m10ED4BB67ECA9C071357F9F61EF22DC4B8137DB5 (void);
// 0x00000303 System.Boolean FMSocketIOManager::IsWebSocketConnected()
extern void FMSocketIOManager_IsWebSocketConnected_m4B040DE7E90AFFF58C65B8DA413BC93F97AF10DD (void);
// 0x00000304 System.Void FMSocketIOManager::Connect()
extern void FMSocketIOManager_Connect_mD43366D4F33D6FFA5A52689F657870028FBF5E55 (void);
// 0x00000305 System.Void FMSocketIOManager::Close()
extern void FMSocketIOManager_Close_mFD37852F1355930A749967DCA7FCBDEC30466967 (void);
// 0x00000306 System.Void FMSocketIOManager::Emit(System.String)
extern void FMSocketIOManager_Emit_mF3D80A5715A7B189EEC6265A95D5076A1A2C3ABC (void);
// 0x00000307 System.Void FMSocketIOManager::Emit(System.String,System.Action`1<System.String>)
extern void FMSocketIOManager_Emit_mC6719E1E45F72F0860005BA0D7C28FFACF983D69 (void);
// 0x00000308 System.Void FMSocketIOManager::Emit(System.String,System.String)
extern void FMSocketIOManager_Emit_m7E2F16793986058BA70EBC0FEA342FB4C5521733 (void);
// 0x00000309 System.Void FMSocketIOManager::Emit(System.String,System.String,System.Action`1<System.String>)
extern void FMSocketIOManager_Emit_m63A707CA81569DE6CBDACEFF59F08D61671C4140 (void);
// 0x0000030A System.Void FMSocketIOManager::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void FMSocketIOManager_On_m97401A83C13774A5DB9CBBCA699B1BF57AB1B8B9 (void);
// 0x0000030B System.Void FMSocketIOManager::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void FMSocketIOManager_Off_m327F0CEC43F76C072725B00346B0B23CFB8CF36E (void);
// 0x0000030C System.Void FMSocketIOManager::OnEnable()
extern void FMSocketIOManager_OnEnable_m7E06DEEED0061D3AF7DC5313371CB2320F1D3D6C (void);
// 0x0000030D System.Void FMSocketIOManager::OnDisable()
extern void FMSocketIOManager_OnDisable_m90A181726FA88AD67EC022F5868EF12234BC295A (void);
// 0x0000030E System.Void FMSocketIOManager::OnApplicationQuit()
extern void FMSocketIOManager_OnApplicationQuit_m2FA72D0DB4CBA1C09E3BC49FAF50C7C4E3940DE9 (void);
// 0x0000030F System.Void FMSocketIOManager::.ctor()
extern void FMSocketIOManager__ctor_mBDFA994C593B5F265CBEFA26A647C5EA77830EF8 (void);
// 0x00000310 System.Void FMSocketIOManager::<Update>b__38_0(FMSocketIO.SocketIOEvent)
extern void FMSocketIOManager_U3CUpdateU3Eb__38_0_m6F5519D94A6940CD3CB3C6B7A81DE63C302332D9 (void);
// 0x00000311 System.Void FMSocketIOManager/SocketIOSettings::.ctor()
extern void SocketIOSettings__ctor_mA87553DEE71CC2BD0F04E1AA42604BF8F2D7C83A (void);
// 0x00000312 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::.ctor(System.Int32)
extern void U3CWaitForSocketIOConnectedU3Ed__23__ctor_m3236B60F9A2EB434CF870582C147666F22977B5C (void);
// 0x00000313 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.IDisposable.Dispose()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_IDisposable_Dispose_m49F83C457661CEA76D51CD99003472635CEC0824 (void);
// 0x00000314 System.Boolean FMSocketIOManager/<WaitForSocketIOConnected>d__23::MoveNext()
extern void U3CWaitForSocketIOConnectedU3Ed__23_MoveNext_m7772FFA73D1FA5B6E862FEB6246E6B74A084D7A3 (void);
// 0x00000315 System.Object FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97EF547E3759158A1EC60A04AE1398701DC4365A (void);
// 0x00000316 System.Void FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.IEnumerator.Reset()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_Reset_m6291606BABD352B6800CE7C5637AF5EA2094CECE (void);
// 0x00000317 System.Object FMSocketIOManager/<WaitForSocketIOConnected>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_get_Current_m6ABD17DEC39D75E49B5B43FFAFD37F26229B6491 (void);
// 0x00000318 System.Void EventJson::.ctor()
extern void EventJson__ctor_m578301E6CD602AA80AD29BDF12DF2207B12D06A3 (void);
// 0x00000319 System.Void SocketIOComponentWebGL::Awake()
extern void SocketIOComponentWebGL_Awake_mFFD18ABE4AD2A927E929285A73BF03CA9C0DDAD3 (void);
// 0x0000031A System.Void SocketIOComponentWebGL::DebugLog(System.String)
extern void SocketIOComponentWebGL_DebugLog_m5EB0712BE4C850ECA4A17292AC060CAD4E20083B (void);
// 0x0000031B System.Boolean SocketIOComponentWebGL::IsWebSocketConnected()
extern void SocketIOComponentWebGL_IsWebSocketConnected_m2AA9F0DE669F40D6D62AE78CAA22DC4A48982C26 (void);
// 0x0000031C System.Boolean SocketIOComponentWebGL::IsReady()
extern void SocketIOComponentWebGL_IsReady_m3252F7F80254C13ED6449D0B9EF85E59E49ADDD1 (void);
// 0x0000031D System.Void SocketIOComponentWebGL::Update()
extern void SocketIOComponentWebGL_Update_mC7A9764C2735E42E26DC0C45ACDFB320447ADAF2 (void);
// 0x0000031E System.Void SocketIOComponentWebGL::Init()
extern void SocketIOComponentWebGL_Init_m9BC833009A299D73291C27774367696C18C94009 (void);
// 0x0000031F System.Void SocketIOComponentWebGL::OnConnected(FMSocketIO.SocketIOEvent)
extern void SocketIOComponentWebGL_OnConnected_m9DB7A8704B9112D8A74DEC2D5D619904EFF0509A (void);
// 0x00000320 System.Void SocketIOComponentWebGL::AddSocketIO()
extern void SocketIOComponentWebGL_AddSocketIO_m531A7D0E1FB1EC7CA45EE8941C3E4FB29D10C859 (void);
// 0x00000321 System.Void SocketIOComponentWebGL::AddEventListeners()
extern void SocketIOComponentWebGL_AddEventListeners_m9405B4EBC3F81FC27D37526265091EF2B16EBA53 (void);
// 0x00000322 System.Void SocketIOComponentWebGL::Connect()
extern void SocketIOComponentWebGL_Connect_m27B3028A02989F528EE6D7196B20C52340C00981 (void);
// 0x00000323 System.Void SocketIOComponentWebGL::Close()
extern void SocketIOComponentWebGL_Close_m85C247694C36C9E9BF0B4BF0D5BB021E5C0BD39D (void);
// 0x00000324 System.Void SocketIOComponentWebGL::Emit(System.String)
extern void SocketIOComponentWebGL_Emit_m0253F2AA8EA759A4FE106B67B510A1595240C6B6 (void);
// 0x00000325 System.Void SocketIOComponentWebGL::Emit(System.String,System.String)
extern void SocketIOComponentWebGL_Emit_m9CC2BA989466F8B011B65051CB8C19B4276491C5 (void);
// 0x00000326 System.Void SocketIOComponentWebGL::Emit(System.String,System.Action`1<System.String>)
extern void SocketIOComponentWebGL_Emit_m8367518E54945C5467AEC7EA699063F7E7FE5E0D (void);
// 0x00000327 System.Void SocketIOComponentWebGL::Emit(System.String,System.String,System.Action`1<System.String>)
extern void SocketIOComponentWebGL_Emit_m499EBAFFDD6BDBA3EACAAB2B0FC0EC24B4080097 (void);
// 0x00000328 System.Void SocketIOComponentWebGL::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponentWebGL_On_m50A1393227149A7F64377545FB57A2418C081E45 (void);
// 0x00000329 System.Void SocketIOComponentWebGL::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponentWebGL_Off_m7B84AEE2E20769A96135CE1E29A8EDA51296A433 (void);
// 0x0000032A System.Void SocketIOComponentWebGL::InvokeAck(System.String)
extern void SocketIOComponentWebGL_InvokeAck_m3A07EBA5960382A5358A626D419ADF3DF403BB2F (void);
// 0x0000032B System.Void SocketIOComponentWebGL::OnOpen()
extern void SocketIOComponentWebGL_OnOpen_m4FA0157C82D243643B6A63180E9B6927A2A8F8A6 (void);
// 0x0000032C System.Void SocketIOComponentWebGL::SetSocketID(System.String)
extern void SocketIOComponentWebGL_SetSocketID_m6600E6EA388CD6E21A0F6DB953FF0FF4512D744A (void);
// 0x0000032D System.Void SocketIOComponentWebGL::InvokeEventCallback(System.String)
extern void SocketIOComponentWebGL_InvokeEventCallback_m321CAA1AB04A660A9DC19BD1A8D1D32D8E6254D8 (void);
// 0x0000032E System.Void SocketIOComponentWebGL::RegOnOpen()
extern void SocketIOComponentWebGL_RegOnOpen_m0AB925DAF4BB84EA088752D3D1D429CD8C3370D7 (void);
// 0x0000032F System.Void SocketIOComponentWebGL::RegOnClose()
extern void SocketIOComponentWebGL_RegOnClose_m0C256244BF2BFA1D5A95977B102D5DD73E47ECB9 (void);
// 0x00000330 System.Void SocketIOComponentWebGL::RegOnMessage(System.String)
extern void SocketIOComponentWebGL_RegOnMessage_mA860FE89E7B5236AAE352748447B0F85C5E0DB38 (void);
// 0x00000331 System.Void SocketIOComponentWebGL::RegOnError(System.String)
extern void SocketIOComponentWebGL_RegOnError_m7606640BFC80E31102516BC5D8F6936407A12258 (void);
// 0x00000332 System.Void SocketIOComponentWebGL::RegWebSocketConnected()
extern void SocketIOComponentWebGL_RegWebSocketConnected_m8D21B5FDCC1C61496BA78B9585FE0EF4CD67EC48 (void);
// 0x00000333 System.Void SocketIOComponentWebGL::RegWebSocketDisconnected()
extern void SocketIOComponentWebGL_RegWebSocketDisconnected_m75D6345DF745FA0D435B669E4F9846F98E50048B (void);
// 0x00000334 System.Void SocketIOComponentWebGL::.ctor()
extern void SocketIOComponentWebGL__ctor_mAF6B59F74804B5E81402F2902E7C2B449CBF0EE0 (void);
// 0x00000335 System.Void NetTcpCommandContentState::.ctor()
extern void NetTcpCommandContentState__ctor_mB30B8111821F99DAE59D3A376E49688C2EB9D48D (void);
// 0x00000336 System.Void ServerSender::Start()
extern void ServerSender_Start_m5812E957F255B623FD61783241DC64221F9D1ED0 (void);
// 0x00000337 System.Void ServerSender::FunctionPlay()
extern void ServerSender_FunctionPlay_m4C15A74A0D6AEAC6F60EFB0C05B398F9E5970082 (void);
// 0x00000338 System.Void ServerSender::FunctionStop()
extern void ServerSender_FunctionStop_m60E48C0CE6A8595185204126E19BFA8C8F204EB2 (void);
// 0x00000339 System.Void ServerSender::Update()
extern void ServerSender_Update_m7A51483EA660D0D8C857D5C79DE39111F385749F (void);
// 0x0000033A System.Void ServerSender::FunctionMR()
extern void ServerSender_FunctionMR_m01C42C2239AD64FBB0C7BB18F7E23194C0825D75 (void);
// 0x0000033B System.Void ServerSender::.ctor()
extern void ServerSender__ctor_m778C5D67E33926342310F6D46B2688253CF0248D (void);
// 0x0000033C System.Void ServerSender::<Start>b__7_0()
extern void ServerSender_U3CStartU3Eb__7_0_m3FC2E125AECFFE5902D724285B39EF97437B60DD (void);
// 0x0000033D System.Void ServerSender::<Start>b__7_1()
extern void ServerSender_U3CStartU3Eb__7_1_m8E999A233A9090B377098A86B8752196E4800398 (void);
// 0x0000033E System.Void ServerSender::<Start>b__7_2()
extern void ServerSender_U3CStartU3Eb__7_2_m6202A6242EA9131030805813921FB02B72053024 (void);
// 0x0000033F System.Collections.IEnumerator Randomizer::Start()
extern void Randomizer_Start_mC4A70B1DCBC84FEBEFA238F22002B69C0DE644E2 (void);
// 0x00000340 System.Void Randomizer::Update()
extern void Randomizer_Update_m40CECDF55DE2336D4500335F97D1D2844601B94C (void);
// 0x00000341 System.Void Randomizer::.ctor()
extern void Randomizer__ctor_m78CE5F7DC9A43B823EA742C8A4B246CC84363223 (void);
// 0x00000342 System.Void Randomizer/<Start>d__1::.ctor(System.Int32)
extern void U3CStartU3Ed__1__ctor_m2956A75A24CA7BA18E319063A30F4A334A84A944 (void);
// 0x00000343 System.Void Randomizer/<Start>d__1::System.IDisposable.Dispose()
extern void U3CStartU3Ed__1_System_IDisposable_Dispose_m86EB27051FD8CF0457ED59EA92C8E9C6DC8ECBDD (void);
// 0x00000344 System.Boolean Randomizer/<Start>d__1::MoveNext()
extern void U3CStartU3Ed__1_MoveNext_mF07251FD51141E37FC667D2388865835BF37CCD1 (void);
// 0x00000345 System.Object Randomizer/<Start>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78F8C1E52E2155D00A9EBBCE2B6F9B22F78B7996 (void);
// 0x00000346 System.Void Randomizer/<Start>d__1::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m68F8B70553C3C1230AAB2EB4A437464E6160C054 (void);
// 0x00000347 System.Object Randomizer/<Start>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mB25A3BBFCFC9C4322B76D61E6E07249287267B0F (void);
// 0x00000348 System.Void SourceSelector::Start()
extern void SourceSelector_Start_m8067A37546F88DB7AC2F30A0C443B932C2B1CC0E (void);
// 0x00000349 System.Void SourceSelector::Update()
extern void SourceSelector_Update_m31F2A6A307C7035510CEC2EBEA9D75858BB04563 (void);
// 0x0000034A System.Void SourceSelector::OnChangeValue(System.Int32)
extern void SourceSelector_OnChangeValue_m7FDB0914CDAEB00915FA01539B996593377E1DF6 (void);
// 0x0000034B System.Void SourceSelector::.ctor()
extern void SourceSelector__ctor_mAADF270CF83E1F0BB22002D010121725755E3B28 (void);
// 0x0000034C System.Void Readme::.ctor()
extern void Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C (void);
// 0x0000034D System.Void Readme/Section::.ctor()
extern void Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96 (void);
// 0x0000034E System.Collections.IEnumerator VideoPlayer::Start()
extern void VideoPlayer_Start_m7E53175EE0C7A14AEDC282140539EA796E789B2F (void);
// 0x0000034F System.Void VideoPlayer::Update()
extern void VideoPlayer_Update_m0349DF5F35E9BEC308B4D436BE343155F39691D3 (void);
// 0x00000350 System.Single VideoPlayer::GertDuration()
extern void VideoPlayer_GertDuration_m539E5D915BC544E8C09EAA9354E195376288D2DB (void);
// 0x00000351 System.Single VideoPlayer::GetTime()
extern void VideoPlayer_GetTime_mF0C75D3173DBBDF2F9F0FD7FCC1FE2A288C759DA (void);
// 0x00000352 System.Void VideoPlayer::SetTime(System.Single)
extern void VideoPlayer_SetTime_mBC35E6A475D4D14CC3C3377CA96236ECCEB66949 (void);
// 0x00000353 System.Void VideoPlayer::Play()
extern void VideoPlayer_Play_m26CA5DFB768F5A2B5B5BCBA07072CD76EBA6BC1A (void);
// 0x00000354 System.Void VideoPlayer::Stop()
extern void VideoPlayer_Stop_m91E4E92710505D710713B03DCCBB88C767266A4C (void);
// 0x00000355 System.Void VideoPlayer::Rewind()
extern void VideoPlayer_Rewind_mED6A835D6A134D17F1866E9BDBD6EE42E3EA96B4 (void);
// 0x00000356 System.Void VideoPlayer::.ctor()
extern void VideoPlayer__ctor_m413EFC9FAAA8D61D3E811C27882A7DE2DD785528 (void);
// 0x00000357 System.Void VideoPlayer/<Start>d__7::.ctor(System.Int32)
extern void U3CStartU3Ed__7__ctor_mB34FEAA7BD5BC71B9EB90A424F0B650DE6C6F532 (void);
// 0x00000358 System.Void VideoPlayer/<Start>d__7::System.IDisposable.Dispose()
extern void U3CStartU3Ed__7_System_IDisposable_Dispose_m3C7A251A2596B042F84254FD9D200FA51C46FB1B (void);
// 0x00000359 System.Boolean VideoPlayer/<Start>d__7::MoveNext()
extern void U3CStartU3Ed__7_MoveNext_mC214364B7BB57009C58C36334BB31B4C5A33E0FE (void);
// 0x0000035A System.Object VideoPlayer/<Start>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E765C7F2FAC8D85FB50FC7E413AB9A153119DDB (void);
// 0x0000035B System.Void VideoPlayer/<Start>d__7::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mFB4759DEE674598DD4D804CB3967D01FAD3B0C52 (void);
// 0x0000035C System.Object VideoPlayer/<Start>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m0F2862D46B86E7C2F279201428924BE97768F456 (void);
// 0x0000035D System.Void DynamicBoneDemo1::Update()
extern void DynamicBoneDemo1_Update_m41DAB2BE429EC00C7D3D240549F88ED2570A6D48 (void);
// 0x0000035E System.Void DynamicBoneDemo1::OnGUI()
extern void DynamicBoneDemo1_OnGUI_mFC144CE71DBDFE9D2DE0D34043459D12C654F14F (void);
// 0x0000035F System.Void DynamicBoneDemo1::.ctor()
extern void DynamicBoneDemo1__ctor_mE314C3805FCC23162262352076FD53038CB329B6 (void);
// 0x00000360 System.Void DynamicBone::Start()
extern void DynamicBone_Start_m09C19C501BA86409F55CABF271EE4D02531099AC (void);
// 0x00000361 System.Void DynamicBone::FixedUpdate()
extern void DynamicBone_FixedUpdate_m7BE96A041089E847876DDC49C7EADD2BEECC58A5 (void);
// 0x00000362 System.Void DynamicBone::Update()
extern void DynamicBone_Update_m333CFF83EAA30B78204E71D01360BC83DFEBC354 (void);
// 0x00000363 System.Void DynamicBone::LateUpdate()
extern void DynamicBone_LateUpdate_m3342FA910227203865B0ECE67D97E0B5F4181D2A (void);
// 0x00000364 System.Void DynamicBone::PreUpdate()
extern void DynamicBone_PreUpdate_m3B1C81BDA01FFE96BEA337D1D81F348A473270D4 (void);
// 0x00000365 System.Void DynamicBone::CheckDistance()
extern void DynamicBone_CheckDistance_m8A4721DB0EC629666DC0345231DDD1399D47EB9B (void);
// 0x00000366 System.Void DynamicBone::OnEnable()
extern void DynamicBone_OnEnable_m67C88D6F3E5CD3B2666BBFDBC6E9FEF3067E5AC8 (void);
// 0x00000367 System.Void DynamicBone::OnDisable()
extern void DynamicBone_OnDisable_m7C4AEA3DF4853BE12AE6C44C7DF8B33BEA70BD1D (void);
// 0x00000368 System.Void DynamicBone::OnValidate()
extern void DynamicBone_OnValidate_m35B188AEDAE18394BF72414CBEC2E1A3A41B940F (void);
// 0x00000369 System.Void DynamicBone::OnDrawGizmosSelected()
extern void DynamicBone_OnDrawGizmosSelected_m33D1FDD944F70FE42DB2B223DE4E66B16D8162C9 (void);
// 0x0000036A System.Void DynamicBone::SetWeight(System.Single)
extern void DynamicBone_SetWeight_mDFB5B187C9045D0993D0F7E38193BD1BB208CD3A (void);
// 0x0000036B System.Single DynamicBone::GetWeight()
extern void DynamicBone_GetWeight_m7DBFEB86F41488B34CC4A7DC8BB9164918917E60 (void);
// 0x0000036C System.Void DynamicBone::UpdateDynamicBones(System.Single)
extern void DynamicBone_UpdateDynamicBones_m19D20412B5B593AB8842BEA66A73BC632066C012 (void);
// 0x0000036D System.Void DynamicBone::SetupParticles()
extern void DynamicBone_SetupParticles_m2536944256CB1F5DF4094988E26F56C48698878E (void);
// 0x0000036E System.Void DynamicBone::AppendParticles(UnityEngine.Transform,System.Int32,System.Single)
extern void DynamicBone_AppendParticles_mCAF941296F86AAA1D6DA6164509A7276EBCBC049 (void);
// 0x0000036F System.Void DynamicBone::UpdateParameters()
extern void DynamicBone_UpdateParameters_mD6D2591B0434D8032D72DA0832ED64EF73562B5C (void);
// 0x00000370 System.Void DynamicBone::InitTransforms()
extern void DynamicBone_InitTransforms_mCB14EA48C108D0F027A1B53BC0464984B7E5408D (void);
// 0x00000371 System.Void DynamicBone::ResetParticlesPosition()
extern void DynamicBone_ResetParticlesPosition_mD0010153723916A63B72C76AFFF9F6C018D7186F (void);
// 0x00000372 System.Void DynamicBone::UpdateParticles1(System.Single)
extern void DynamicBone_UpdateParticles1_mFE4535A12897CFFC1D8B7217559917246F37DEC2 (void);
// 0x00000373 System.Void DynamicBone::UpdateParticles2(System.Single)
extern void DynamicBone_UpdateParticles2_m182515EFD4911688E00359EBEC74C87935A4C198 (void);
// 0x00000374 System.Void DynamicBone::SkipUpdateParticles()
extern void DynamicBone_SkipUpdateParticles_m5B1CFA3094CEE47F138C55CE64EC3420DE1B9423 (void);
// 0x00000375 UnityEngine.Vector3 DynamicBone::MirrorVector(UnityEngine.Vector3,UnityEngine.Vector3)
extern void DynamicBone_MirrorVector_m875346828B6E210900E42E98C7D78822A5736F42 (void);
// 0x00000376 System.Void DynamicBone::ApplyParticlesToTransforms()
extern void DynamicBone_ApplyParticlesToTransforms_m0CC5F5474DAE8F3C47FA4513E7FC3228EB0F3CF6 (void);
// 0x00000377 System.Void DynamicBone::.ctor()
extern void DynamicBone__ctor_mAAF795E0041D7D4946C2A2FB67684568139D86D6 (void);
// 0x00000378 System.Void DynamicBone/Particle::.ctor()
extern void Particle__ctor_m3A4D31F4AB1CF6B8EA8E7B145E263072E2911339 (void);
// 0x00000379 System.Void DynamicBoneCollider::OnValidate()
extern void DynamicBoneCollider_OnValidate_m7F5F03E8043DCE6D37B984BF0F3C117541899389 (void);
// 0x0000037A System.Boolean DynamicBoneCollider::Collide(UnityEngine.Vector3&,System.Single)
extern void DynamicBoneCollider_Collide_mABC62EE87827B7A6F93E5B931B6F859015D7860B (void);
// 0x0000037B System.Boolean DynamicBoneCollider::OutsideSphere(UnityEngine.Vector3&,System.Single,UnityEngine.Vector3,System.Single)
extern void DynamicBoneCollider_OutsideSphere_m82BC29B4E79F3F2C1A1DCC502807A0899AC5B196 (void);
// 0x0000037C System.Boolean DynamicBoneCollider::InsideSphere(UnityEngine.Vector3&,System.Single,UnityEngine.Vector3,System.Single)
extern void DynamicBoneCollider_InsideSphere_mF314A452C0EF709506A7FE30A5BEA569BCE2B594 (void);
// 0x0000037D System.Boolean DynamicBoneCollider::OutsideCapsule(UnityEngine.Vector3&,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void DynamicBoneCollider_OutsideCapsule_mBD831688B6A973ADF1A1824F319E36148314777B (void);
// 0x0000037E System.Boolean DynamicBoneCollider::InsideCapsule(UnityEngine.Vector3&,System.Single,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void DynamicBoneCollider_InsideCapsule_mF0AF0601862231180629E77D619A32B3E9F4A1BF (void);
// 0x0000037F System.Void DynamicBoneCollider::OnDrawGizmosSelected()
extern void DynamicBoneCollider_OnDrawGizmosSelected_mF2A6F406B9D84EC5DFEBF4DCC0832F47C8D1E960 (void);
// 0x00000380 System.Void DynamicBoneCollider::.ctor()
extern void DynamicBoneCollider__ctor_m7A732AE491D455DE001411A537A8B182C712BD10 (void);
// 0x00000381 System.Boolean DynamicBoneColliderBase::Collide(UnityEngine.Vector3&,System.Single)
extern void DynamicBoneColliderBase_Collide_mDA4A4598657DD2BA8EE0BFC694041372198F54C9 (void);
// 0x00000382 System.Void DynamicBoneColliderBase::.ctor()
extern void DynamicBoneColliderBase__ctor_mDAC26A2319B6459A424DA163F7ECFCB975C5279C (void);
// 0x00000383 System.Void DynamicBonePlaneCollider::OnValidate()
extern void DynamicBonePlaneCollider_OnValidate_mB48770F578629D7D21F154E82F945D0BB97F259B (void);
// 0x00000384 System.Boolean DynamicBonePlaneCollider::Collide(UnityEngine.Vector3&,System.Single)
extern void DynamicBonePlaneCollider_Collide_m0949ED810AEA976BD66F52FB295B19CF17CE8655 (void);
// 0x00000385 System.Void DynamicBonePlaneCollider::OnDrawGizmosSelected()
extern void DynamicBonePlaneCollider_OnDrawGizmosSelected_m4C4D0053900941332B4710B619686596B9B2E999 (void);
// 0x00000386 System.Void DynamicBonePlaneCollider::.ctor()
extern void DynamicBonePlaneCollider__ctor_mD159B7430A7D692D15A8B53336EABA1FD1416939 (void);
// 0x00000387 System.Void LiveBase::Start()
extern void LiveBase_Start_m23AB136BFB0BEC2DB358C0BEC40E88B149BFC989 (void);
// 0x00000388 System.Void LiveBase::OnDestroy()
extern void LiveBase_OnDestroy_mD09C85171DA9CD80059A495F17A8A7CE144D3089 (void);
// 0x00000389 System.Void LiveBase::BaseUpdate()
extern void LiveBase_BaseUpdate_m82ED0920F76B0BBC0CED6D24BC2B6274156CDA9D (void);
// 0x0000038A System.Void LiveBase::LateUpdate()
extern void LiveBase_LateUpdate_m36CD1A4226C12289B178F2B33F4ECA685863BABF (void);
// 0x0000038B SimpleJSON.JSONNode LiveBase::GetControl(System.String)
extern void LiveBase_GetControl_m56175D218F38E6052A3D552B8C7FF2FB0B3C5DB9 (void);
// 0x0000038C System.Void LiveBase::Connect()
extern void LiveBase_Connect_m5CF0034B7F9E494D55C3D9EE177165F2E2447ABC (void);
// 0x0000038D System.Void LiveBase::Disconnect()
extern void LiveBase_Disconnect_m673DB56DD91C8BD55A14A43DA9EBBEFD3D1DAD23 (void);
// 0x0000038E System.Void LiveBase::OnSettingsChange()
extern void LiveBase_OnSettingsChange_mB9F4A675C741700C1AE3BD3ED8AE4F9C90641529 (void);
// 0x0000038F System.Void LiveBase::OnApplicationQuit()
extern void LiveBase_OnApplicationQuit_mF676E9838419E483F284666CE040B94795BA6578 (void);
// 0x00000390 System.Void LiveBase::OnToggleRecording()
extern void LiveBase_OnToggleRecording_m079449E65CC3851246CBF715E014164A6B877F7B (void);
// 0x00000391 System.Void LiveBase::IntializeRecording()
extern void LiveBase_IntializeRecording_m3970946F203CAA1F2118B951FFC3066146BC35AE (void);
// 0x00000392 System.Void LiveBase::RecordFrame()
extern void LiveBase_RecordFrame_m0CAF21C2D01247947ABAFC18028612A10E10C845 (void);
// 0x00000393 System.Void LiveBase::.ctor()
extern void LiveBase__ctor_mB9F977350761317318D71E7EE9E4F666B903A38E (void);
// 0x00000394 System.Void LiveBase/FTIAnimCurve::.ctor()
extern void FTIAnimCurve__ctor_m9B2DC10FB02ADE0E30B5477470EE36D3F55A5EF2 (void);
// 0x00000395 System.String Utility::CombinePath(System.String[])
extern void Utility_CombinePath_m6C52ED1A44B56B77443C339707667FC49D3999B5 (void);
// 0x00000396 System.Void Utility::.ctor()
extern void Utility__ctor_m50E58B4CD220E2DF1CAF80E8952B04226B78F6F9 (void);
// 0x00000397 System.Void MetaObj::.ctor()
extern void MetaObj__ctor_mFB17ED0F8B7A16F3A3447CD092677E4583F09E52 (void);
// 0x00000398 System.Void Expression::.ctor()
extern void Expression__ctor_mCE3E7BEECC6CE1CA3A12199B922E0A8B69324D95 (void);
// 0x00000399 System.Void Expression::.ctor(System.String,System.String,System.String,System.Boolean)
extern void Expression__ctor_mB702B1853EC9C774CE86E8082E9387525CCD77C7 (void);
// 0x0000039A System.Void Data::.ctor()
extern void Data__ctor_mCD3AD6E8D195C4D9716A6478E83258C452C89C07 (void);
// 0x0000039B System.Void LiveCharacterSetup::add_ReportError(LiveCharacterSetup/ReportErrorHandler)
extern void LiveCharacterSetup_add_ReportError_mC40A1A6C54A3098BD86055536ECB698C213E997D (void);
// 0x0000039C System.Void LiveCharacterSetup::remove_ReportError(LiveCharacterSetup/ReportErrorHandler)
extern void LiveCharacterSetup_remove_ReportError_m78449C1699B336404D0A0097ED74F251989633E2 (void);
// 0x0000039D System.Void LiveCharacterSetup::.ctor()
extern void LiveCharacterSetup__ctor_m1CCF969306C7C302F20233B178FB4030BDB8B22F (void);
// 0x0000039E System.Void LiveCharacterSetup::Init(System.Collections.Generic.List`1<Expression>,System.Collections.Generic.List`1<System.String>)
extern void LiveCharacterSetup_Init_m8274C58A3C0E1F5A4A1FAC079F63233ACF4E76E8 (void);
// 0x0000039F System.Void LiveCharacterSetup::UpdateData(System.Collections.Generic.List`1<Expression>,System.Collections.Generic.List`1<System.String>)
extern void LiveCharacterSetup_UpdateData_m0ABB58DB509327E97D5C69E6D894553D792CA7CF (void);
// 0x000003A0 System.Void LiveCharacterSetup::Cleanup()
extern void LiveCharacterSetup_Cleanup_m6718A5412432B4C65D58A0840EECB5B1D37651AA (void);
// 0x000003A1 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4> LiveCharacterSetup::GetNeutralControlValues()
extern void LiveCharacterSetup_GetNeutralControlValues_m3B89DA1004949F854FA8627BA5133062E234700C (void);
// 0x000003A2 Expression LiveCharacterSetup::FindExpression(System.String)
extern void LiveCharacterSetup_FindExpression_mCC2056319862A29142EE6C4C8CA33EFCFED69A86 (void);
// 0x000003A3 System.Collections.Generic.Dictionary`2<System.String,System.String> LiveCharacterSetup::GetExpressionNameAttrList()
extern void LiveCharacterSetup_GetExpressionNameAttrList_mD827FD81BAE17E5521AF9D1EBB2FAA4E0EA8392F (void);
// 0x000003A4 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4> LiveCharacterSetup::GetControlValues(System.String)
extern void LiveCharacterSetup_GetControlValues_m2925A56F38C9F2494F290F3CF9D78EAE2911D563 (void);
// 0x000003A5 System.Boolean LiveCharacterSetup::SetControlValues(System.String,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4>)
extern void LiveCharacterSetup_SetControlValues_m83033E00F83AD58169973094A7842CA9F9B945E0 (void);
// 0x000003A6 System.Collections.Generic.List`1<System.String> LiveCharacterSetup::GetControlList()
extern void LiveCharacterSetup_GetControlList_m8A31F9595212F3265C557AE53D3A5867D40B1BFB (void);
// 0x000003A7 System.Boolean LiveCharacterSetup::InUse(System.String)
extern void LiveCharacterSetup_InUse_m4D701BCDE7F299C11D16A64148AEE55A311BA97F (void);
// 0x000003A8 System.Void LiveCharacterSetup::SetInUse(System.String,System.Boolean)
extern void LiveCharacterSetup_SetInUse_m2CA403718833EFD44B197F4FAC6F9A7014FDB8AE (void);
// 0x000003A9 System.Void LiveCharacterSetup::AddControls(System.Collections.Generic.List`1<System.String>)
extern void LiveCharacterSetup_AddControls_m27CEE2BCE8CCA61C1950D73D752C482C66EF153D (void);
// 0x000003AA System.Void LiveCharacterSetup::RemoveControls(System.Collections.Generic.List`1<System.String>)
extern void LiveCharacterSetup_RemoveControls_m106CD67F7D9370463F1CABAD5E493D0E52A9E2AA (void);
// 0x000003AB System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4> LiveCharacterSetup::ConstructRigValues(System.Collections.Generic.Dictionary`2<System.String,System.Single>,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4>)
extern void LiveCharacterSetup_ConstructRigValues_mB577F73A32F396BCBAC66D3DB475468C29C47C1F (void);
// 0x000003AC UnityEngine.Vector4 LiveCharacterSetup::QuaternionToVector4(UnityEngine.Quaternion)
extern void LiveCharacterSetup_QuaternionToVector4_m2F4BEA97AD2F21633F7243FCBB1C39017DA72386 (void);
// 0x000003AD UnityEngine.Quaternion LiveCharacterSetup::Vector4ToQuaternion(UnityEngine.Vector4)
extern void LiveCharacterSetup_Vector4ToQuaternion_m7831A5648E7E6354FDE32B52570DFA855059D226 (void);
// 0x000003AE System.Void LiveCharacterSetup::.cctor()
extern void LiveCharacterSetup__cctor_mA56F2AF11059CCCEF39C22ABF0832A1FAF222C47 (void);
// 0x000003AF System.Void LiveCharacterSetup/ReportErrorHandler::.ctor(System.Object,System.IntPtr)
extern void ReportErrorHandler__ctor_mB17B2B1B05596589BC2446B37E17E61376212D8A (void);
// 0x000003B0 System.Void LiveCharacterSetup/ReportErrorHandler::Invoke(System.String,System.String)
extern void ReportErrorHandler_Invoke_mB0AC953F35928F8D5C36C0E6CD7104BA30E2D3BA (void);
// 0x000003B1 System.IAsyncResult LiveCharacterSetup/ReportErrorHandler::BeginInvoke(System.String,System.String,System.AsyncCallback,System.Object)
extern void ReportErrorHandler_BeginInvoke_m75FF2797011A23D4D11FC45EC2749453BDE0654B (void);
// 0x000003B2 System.Void LiveCharacterSetup/ReportErrorHandler::EndInvoke(System.IAsyncResult)
extern void ReportErrorHandler_EndInvoke_m3D3872629757888EEE5A5C957FA96A4031C78667 (void);
// 0x000003B3 System.Void LiveCharacterSetup/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m7C1489C2E19FA5AEF596A41E60E3664EA8974B95 (void);
// 0x000003B4 System.Boolean LiveCharacterSetup/<>c__DisplayClass14_0::<FindExpression>b__0(Expression)
extern void U3CU3Ec__DisplayClass14_0_U3CFindExpressionU3Eb__0_m596DABE95B2295D9A048361DDBB2DA4A065D9F5B (void);
// 0x000003B5 System.Void LiveCharacterSetup/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m8F19CFCDD1020E3CFBAB491D9F5BD564F77344A8 (void);
// 0x000003B6 System.Boolean LiveCharacterSetup/<>c__DisplayClass17_0::<SetControlValues>b__0(System.String)
extern void U3CU3Ec__DisplayClass17_0_U3CSetControlValuesU3Eb__0_m6B69D17E0D34F06B231F4DD081E10B463F012A4C (void);
// 0x000003B7 System.Void LiveCharacterSetupFile::.ctor()
extern void LiveCharacterSetupFile__ctor_m47BA844EE91EF75A3418187249001E2F162082C1 (void);
// 0x000003B8 System.Void LiveClient::Update()
extern void LiveClient_Update_m6D665AEFB5B4B9C36CD0ED23BCE17FED001F6A2F (void);
// 0x000003B9 System.Void LiveClient::.ctor()
extern void LiveClient__ctor_mD4E6377106BD4E517CEC6CBE7922CBAD89D33E4E (void);
// 0x000003BA System.Void LiveClientAdv::Start()
extern void LiveClientAdv_Start_m9697325283FE39ED4B2E5E21A005AA43F76CDFCF (void);
// 0x000003BB System.Void LiveClientAdv::Update()
extern void LiveClientAdv_Update_mA9634B70E4E2B35BAF0B1C2A11E40F0C191A29E4 (void);
// 0x000003BC System.Void LiveClientAdv::.ctor()
extern void LiveClientAdv__ctor_mFB7C0D68B6E278766F095DB32F40D0E094A99970 (void);
// 0x000003BD System.String LiveConnection::get_m_HostIP()
extern void LiveConnection_get_m_HostIP_mFC35E87F09A068BA031B574C53FE8CFB5AA63346 (void);
// 0x000003BE System.Void LiveConnection::set_m_HostIP(System.String)
extern void LiveConnection_set_m_HostIP_mFF2977990A63C47D693437C03DC33EF086D91417 (void);
// 0x000003BF System.Int32 LiveConnection::get_m_HostPort()
extern void LiveConnection_get_m_HostPort_m89B5CD503F28DD27D716BCFA4E3F8CD5D5772058 (void);
// 0x000003C0 System.Void LiveConnection::set_m_HostPort(System.Int32)
extern void LiveConnection_set_m_HostPort_mB7A7040CFA7F670CE8C21344A40ABCB71939CF5D (void);
// 0x000003C1 System.Boolean LiveConnection::get_m_Reconnect()
extern void LiveConnection_get_m_Reconnect_mCB25C0C4516D938D0DDAE29C37E97B497849220A (void);
// 0x000003C2 System.Void LiveConnection::set_m_Reconnect(System.Boolean)
extern void LiveConnection_set_m_Reconnect_m01631CBCA4A96D90E2A8492AEB87CED4AC5D338F (void);
// 0x000003C3 System.Boolean LiveConnection::get_m_DropPackets()
extern void LiveConnection_get_m_DropPackets_mC796B5B6EC11727E8DE164D8AA3C418915D4C38F (void);
// 0x000003C4 System.Void LiveConnection::set_m_DropPackets(System.Boolean)
extern void LiveConnection_set_m_DropPackets_m935E4C562B60393217509B4015B8531196F4872F (void);
// 0x000003C5 System.Boolean LiveConnection::get_m_RecievedNewData()
extern void LiveConnection_get_m_RecievedNewData_m0FF9C54F09BD79822765ECB110649A496A0ED831 (void);
// 0x000003C6 System.Void LiveConnection::set_m_RecievedNewData(System.Boolean)
extern void LiveConnection_set_m_RecievedNewData_m526C95804D4DD1BC5E6391BBA7194915E4F1AE80 (void);
// 0x000003C7 System.Void LiveConnection::.ctor()
extern void LiveConnection__ctor_mB7C9F396DBC4A1F934574CB7FB11014A8B2AEBCE (void);
// 0x000003C8 System.Void LiveConnection::.ctor(System.String,System.Int32)
extern void LiveConnection__ctor_mD6FC8CBEB130B4B13E8B456BDCD917A8468B9CC7 (void);
// 0x000003C9 System.Void LiveConnection::Connect()
extern void LiveConnection_Connect_mB07373B1855AD818DFB3B0AF88929DA337911293 (void);
// 0x000003CA System.Void LiveConnection::Disconnect()
extern void LiveConnection_Disconnect_m9A088A3B00AA8597C6BDB23FE684F3CB2CAB059B (void);
// 0x000003CB System.Boolean LiveConnection::IsConnected()
extern void LiveConnection_IsConnected_m2229C7DD058959C3E841A97A9AFB6AA86D7D78E9 (void);
// 0x000003CC System.Void LiveConnection::BeginConnectCallback(System.IAsyncResult)
extern void LiveConnection_BeginConnectCallback_mDB9493EC2ED3B6DEBF0D0F28378A07B43D8022A6 (void);
// 0x000003CD System.Void LiveConnection::GetNextMessage()
extern void LiveConnection_GetNextMessage_m5F94BE973F766528AFEA8FE1EA71D78BC21F3F17 (void);
// 0x000003CE System.Void LiveConnection::OnNewMessageComplete(System.String)
extern void LiveConnection_OnNewMessageComplete_mB3005CC10DF4E0DC4D29CED92EA2AD761FEBEAD2 (void);
// 0x000003CF System.Void LiveConnection::GetHeader(System.Int32,System.Byte[])
extern void LiveConnection_GetHeader_m53EB8E6FBEA3C809E163C21542C572D7EECA40A4 (void);
// 0x000003D0 System.Void LiveConnection::GetMessage(System.Int32,System.Int32,System.Byte[])
extern void LiveConnection_GetMessage_m9A5301C0AB96CC14D467BB3987B44A107F924611 (void);
// 0x000003D1 SimpleJSON.JSONNode LiveConnection::GetLiveData()
extern void LiveConnection_GetLiveData_m9311FAECC8789A20E6CE86347CBB7B1956153431 (void);
// 0x000003D2 System.Void LiveConnection::PrintMessage(System.String)
extern void LiveConnection_PrintMessage_mAB70C3E38FBA8F44224478E8DC3448458170F844 (void);
// 0x000003D3 System.Void LiveConnection::PrintWarning(System.String)
extern void LiveConnection_PrintWarning_mA829AE99B4F69826D8F95483C0D4DB81F411C693 (void);
// 0x000003D4 System.Void LiveConnection::PrintError(System.String)
extern void LiveConnection_PrintError_m998F3C9CF804EAC2BF8388F0C47AF5E6FFB6C610 (void);
// 0x000003D5 System.Void LiveConnection/<>c__DisplayClass31_0::.ctor()
extern void U3CU3Ec__DisplayClass31_0__ctor_mF7136D163E537DD346D7F47FF8FC861DE7053CEC (void);
// 0x000003D6 System.Void LiveConnection/<>c__DisplayClass31_0::<GetHeader>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass31_0_U3CGetHeaderU3Eb__0_m5A8A7C588566FE8736C31E767D2C67B87B838D4B (void);
// 0x000003D7 System.Void LiveConnection/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m197EA3DE1D40439EED66DC9560ABDED4EF6D9F6F (void);
// 0x000003D8 System.Void LiveConnection/<>c__DisplayClass32_0::<GetMessage>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass32_0_U3CGetMessageU3Eb__0_mC9A5B90470FF17466D0C4DF0118B8B0A78813299 (void);
// 0x000003D9 System.String LiveHelpers::CurrentVersion()
extern void LiveHelpers_CurrentVersion_m2F8481FE4B4B0681A42822A4D3953286B91B8432 (void);
// 0x000003DA System.String LiveHelpers::JoinNameAttr(System.String,System.String)
extern void LiveHelpers_JoinNameAttr_mAD880B7E3AAF6CE3C4A23BCA66910991F40D6D3A (void);
// 0x000003DB System.Void LiveHelpers::SplitNameAttr(System.String,System.String&,System.String&)
extern void LiveHelpers_SplitNameAttr_mF4CE3E6632B3DD26088C67E38856B07D89323F71 (void);
// 0x000003DC System.String LiveHelpers::GetAttrString(System.String)
extern void LiveHelpers_GetAttrString_m5FD5447F370A06EE88697388118126918809001A (void);
// 0x000003DD System.Void LiveHelpers::.ctor()
extern void LiveHelpers__ctor_m1AE366B731B087D84D6C55C44BA90B806A4689AB (void);
// 0x000003DE System.Void LiveHelpers::.cctor()
extern void LiveHelpers__cctor_m333A33AD8C7985D5F3FDE854BC505885D46AB195 (void);
// 0x000003DF UnityEngine.Transform TransformDeepChildExtension::FindDeepChild(UnityEngine.Transform,System.String)
extern void TransformDeepChildExtension_FindDeepChild_m597CDC7F2947821AFD555C89C8747ADF6163C1AC (void);
// 0x000003E0 System.Collections.Generic.List`1<UnityEngine.GameObject> TransformDeepChildExtension::ReturnAllChildren(UnityEngine.Transform,System.Collections.Generic.List`1<UnityEngine.GameObject>)
extern void TransformDeepChildExtension_ReturnAllChildren_mE27C937BA2276371000830342C72221EB0A37366 (void);
// 0x000003E1 System.Collections.Generic.List`1<System.String> LiveUnityInterface::ValidateControls(UnityEngine.GameObject,System.Collections.Generic.List`1<System.String>)
extern void LiveUnityInterface_ValidateControls_m1A9449C1FF52E0A8BFB46A77118977C51E4BC640 (void);
// 0x000003E2 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4> LiveUnityInterface::GetControlValues(UnityEngine.GameObject,System.Collections.Generic.List`1<System.String>)
extern void LiveUnityInterface_GetControlValues_m610888310FA4490A5D6AA4EADE77028CA6633488 (void);
// 0x000003E3 System.Void LiveUnityInterface::ApplyControlValues(UnityEngine.GameObject,System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4>)
extern void LiveUnityInterface_ApplyControlValues_m374587EADF0BB6C4A62DB2C9DFBDF619D390D22C (void);
// 0x000003E4 System.Collections.Generic.List`1<UnityEngine.Object> LiveUnityInterface::GetControls(UnityEngine.GameObject,System.Collections.Generic.List`1<System.String>)
extern void LiveUnityInterface_GetControls_mC3D0EF497C612039E5D09E2210EED02EF14E8A2E (void);
// 0x000003E5 System.Collections.Generic.List`1<System.String> LiveUnityInterface::GetControls(UnityEngine.GameObject)
extern void LiveUnityInterface_GetControls_m0BEA8A3937AFEF1292417F4BB1972728AB7405FE (void);
// 0x000003E6 System.Int32 LiveUnityInterface::GetBlendShapeIndex(UnityEngine.SkinnedMeshRenderer,System.String)
extern void LiveUnityInterface_GetBlendShapeIndex_mF2B6CBEB836EDDD0281BAC10131B5CBFED5E41F6 (void);
// 0x000003E7 System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4> LiveUnityInterface::StringToDictionary(System.String)
extern void LiveUnityInterface_StringToDictionary_m2F8D927317FCE375932D330A05BB356FF2E61FA9 (void);
// 0x000003E8 System.String LiveUnityInterface::DictionaryToString(System.Collections.Generic.Dictionary`2<System.String,UnityEngine.Vector4>)
extern void LiveUnityInterface_DictionaryToString_mCBB7AC09CCBAB2DB4846E141118C688CA5996FCE (void);
// 0x000003E9 System.String LiveUnityInterface::GetRelativeGameObjectPath(UnityEngine.GameObject,UnityEngine.GameObject)
extern void LiveUnityInterface_GetRelativeGameObjectPath_mC58FB43A569928A68672F99C8E305DD32B928FD2 (void);
// 0x000003EA System.Void LiveUnityInterface::.ctor()
extern void LiveUnityInterface__ctor_m8CB4A0C020607500D3EA0BFF68541CAE9B1969B0 (void);
// 0x000003EB System.Void AutoCannon::Awake()
extern void AutoCannon_Awake_mAF615E661A1B704F2CD9ECFED744811BDBA251C0 (void);
// 0x000003EC System.Void AutoCannon::Update()
extern void AutoCannon_Update_m07A39FB9F356B8ED9FF151D6E4EE4CC837B4AE17 (void);
// 0x000003ED System.Collections.IEnumerator AutoCannon::Shoot()
extern void AutoCannon_Shoot_m9CBA0FF8E07B180AF0FA347E50A3418FB102E2E9 (void);
// 0x000003EE System.Void AutoCannon::Launch(UnityEngine.Rigidbody)
extern void AutoCannon_Launch_mCEFA755A2CF58AA7F3D88ABB1562C136A501DF3B (void);
// 0x000003EF System.Void AutoCannon::.ctor()
extern void AutoCannon__ctor_m1C39C78D51A3F0A736F93AAFDC4358DDC8C36AD5 (void);
// 0x000003F0 System.Void AutoCannon/<Shoot>d__11::.ctor(System.Int32)
extern void U3CShootU3Ed__11__ctor_m582297B3D98641892729FEA14AA7B2949620E55A (void);
// 0x000003F1 System.Void AutoCannon/<Shoot>d__11::System.IDisposable.Dispose()
extern void U3CShootU3Ed__11_System_IDisposable_Dispose_mD871AE5420FAD32A2D546881D6CF453683A2135D (void);
// 0x000003F2 System.Boolean AutoCannon/<Shoot>d__11::MoveNext()
extern void U3CShootU3Ed__11_MoveNext_m5F8E4FCE3BB5B992DC893440DAFA14334FBC91B6 (void);
// 0x000003F3 System.Object AutoCannon/<Shoot>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC373A26FC8D14442CC85CD348B347D97B1D7398 (void);
// 0x000003F4 System.Void AutoCannon/<Shoot>d__11::System.Collections.IEnumerator.Reset()
extern void U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m8F3DE312668C06C75A6FF727B8FE6C4DA095BD81 (void);
// 0x000003F5 System.Object AutoCannon/<Shoot>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mF67E4313990ECA2EE9AA34C0613954B506BCCA87 (void);
// 0x000003F6 System.Void BoatController::Update()
extern void BoatController_Update_m5E830C0CA65DD23F0399418B01F06489F1B32C7D (void);
// 0x000003F7 System.Void BoatController::Respawn(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void BoatController_Respawn_mA5F68BDB54C493584178F59EF3118A687EF8D55F (void);
// 0x000003F8 System.Void BoatController::.ctor()
extern void BoatController__ctor_m8D82C71D4F650E7CEC7493F29C7D58820AF60D6F (void);
// 0x000003F9 System.Void BoatFinishLine::OnTriggerEnter(UnityEngine.Collider)
extern void BoatFinishLine_OnTriggerEnter_m4846DB56F482A8FF8CFC9266C5AB2EB78D3CB76C (void);
// 0x000003FA System.Void BoatFinishLine::.ctor()
extern void BoatFinishLine__ctor_m1F1EAC4B284FDD3930751AE98892676F25A0356B (void);
// 0x000003FB System.Void BoatGameController::Die()
extern void BoatGameController_Die_m2CDB5A46E14DF6AC20A1C367E029F179F4D91723 (void);
// 0x000003FC System.Void BoatGameController::Finish()
extern void BoatGameController_Finish_m67A45535E98D76AC1C69A16475E3B25DAF50FBBA (void);
// 0x000003FD System.Void BoatGameController::Update()
extern void BoatGameController_Update_mA75696DF95F96915B4CF99207BDD2E952FDDACD4 (void);
// 0x000003FE System.Void BoatGameController::.ctor()
extern void BoatGameController__ctor_m692B927DCFE12024C6FDF7FDDC9195D55977AACE (void);
// 0x000003FF System.Void CannonBall::Awake()
extern void CannonBall_Awake_m39AAA42F20BD133CB13B7776329AF5C8B52B947E (void);
// 0x00000400 System.Void CannonBall::OnTriggerEnter(UnityEngine.Collider)
extern void CannonBall_OnTriggerEnter_m02677151980279A01283EFE3A91DC1687298C7CD (void);
// 0x00000401 System.Void CannonBall::.ctor()
extern void CannonBall__ctor_mBD9439BE37A985DBE5D9E75A1641D90AFD665D69 (void);
// 0x00000402 System.Void ClothTornEvent::.ctor()
extern void ClothTornEvent__ctor_mAE14AF606DBDB6DB9F4D9770B7749F46012F4A91 (void);
// 0x00000403 System.Void Mine::Awake()
extern void Mine_Awake_m8AB007A5D3E7E879E3BAE0418223ADB88EE5FBC5 (void);
// 0x00000404 System.Void Mine::Update()
extern void Mine_Update_m92E900A05E6231B34CE617DA2BF534EC8E361E7B (void);
// 0x00000405 System.Void Mine::OnCollisionEnter(UnityEngine.Collision)
extern void Mine_OnCollisionEnter_m4B4BCCB775FB535F3B1ABDF0BA1F8EFCDA3AAD9B (void);
// 0x00000406 System.Void Mine::.ctor()
extern void Mine__ctor_m9FAC586E7870688D2482B01ECAE7D3EABFF575B1 (void);
// 0x00000407 System.Void RaycastLasers::Update()
extern void RaycastLasers_Update_m2D7CCDC306D394CC173E3CF32A5F4201F9276A03 (void);
// 0x00000408 System.Void RaycastLasers::.ctor()
extern void RaycastLasers__ctor_m7C45CF0FF758A8A6D8DC2A5BF9DB1FD123EA0494 (void);
// 0x00000409 System.Void Rudder::Awake()
extern void Rudder_Awake_m95A18FFB6B7BE5C1DABE60C2949EED493F67E46B (void);
// 0x0000040A System.Void Rudder::FixedUpdate()
extern void Rudder_FixedUpdate_m26E509B1D680B4DCCB2476CAD04F1AFD99FC861B (void);
// 0x0000040B System.Void Rudder::.ctor()
extern void Rudder__ctor_mCE7BE81EAC70BA50DFA8771B58145DAD162CA6A9 (void);
// 0x0000040C UnityEngine.Mesh SackGenerator::GenerateSheetMesh()
extern void SackGenerator_GenerateSheetMesh_m4D897F59DA927E940897ED401B572CA5507C46F8 (void);
// 0x0000040D System.Collections.IEnumerator SackGenerator::GenerateSack()
extern void SackGenerator_GenerateSack_mE58E47F846441C8D48C977CEFCDD189204E49AE9 (void);
// 0x0000040E System.Void SackGenerator::Update()
extern void SackGenerator_Update_m5174D51EDADFE80FBF6F19D1137DFD5A8A0D0651 (void);
// 0x0000040F System.Void SackGenerator::.ctor()
extern void SackGenerator__ctor_mBCD2C75B309D51AA249B740F0B163D586727626D (void);
// 0x00000410 System.Void SackGenerator/<GenerateSack>d__5::.ctor(System.Int32)
extern void U3CGenerateSackU3Ed__5__ctor_mAF9FC2B43596E379E045C6B942598B6DE8E1625A (void);
// 0x00000411 System.Void SackGenerator/<GenerateSack>d__5::System.IDisposable.Dispose()
extern void U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_m6A56564150536F7EBD1D1556F23310B127D9D69C (void);
// 0x00000412 System.Boolean SackGenerator/<GenerateSack>d__5::MoveNext()
extern void U3CGenerateSackU3Ed__5_MoveNext_mE447A4200E887F4CFF73220FCA2F0148C10C2DB4 (void);
// 0x00000413 System.Object SackGenerator/<GenerateSack>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8520A6EF0BA6F050CF193C5625DB903AAEB49454 (void);
// 0x00000414 System.Void SackGenerator/<GenerateSack>d__5::System.Collections.IEnumerator.Reset()
extern void U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mB9EECC48047030740283F89CFCFBF392A8C7AEFA (void);
// 0x00000415 System.Object SackGenerator/<GenerateSack>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m44D3B2101C02E3BAE05B5824B24B3777715F0DEF (void);
// 0x00000416 System.Void StretchToColors::Start()
extern void StretchToColors_Start_m84A1D0DECFCBD6AE3542F7D81721ED0FA83A0E92 (void);
// 0x00000417 System.Void StretchToColors::OnDestroy()
extern void StretchToColors_OnDestroy_mFCE211E74D86B4B90113B5E1092C45F6F683D52B (void);
// 0x00000418 System.Void StretchToColors::Cloth_OnEndStep(Obi.ObiActor,System.Single)
extern void StretchToColors_Cloth_OnEndStep_mD83FDE0E00587F2C318944D84FFFD65B49725E6F (void);
// 0x00000419 System.Void StretchToColors::.ctor()
extern void StretchToColors__ctor_mE6F73607981E8FAF152C6AB6379BE50046743A6B (void);
// 0x0000041A System.Void WaterTrigger::.ctor()
extern void WaterTrigger__ctor_m39703604DD34C2041537BF8B8E29B0875A24CCAA (void);
// 0x0000041B System.Void ActorCOMTransform::Update()
extern void ActorCOMTransform_Update_m1F823F53C26B50577A9206EE84DF1A0431B521D3 (void);
// 0x0000041C System.Void ActorCOMTransform::.ctor()
extern void ActorCOMTransform__ctor_mC943B9CEBED589C494795B5404E6F9EB27B66355 (void);
// 0x0000041D System.Void ActorSpawner::Update()
extern void ActorSpawner_Update_m5FAF94492AE283E7A239154A04B35E7529DF26BF (void);
// 0x0000041E System.Void ActorSpawner::.ctor()
extern void ActorSpawner__ctor_mE0C9D4C3A5893A45204B40196A3FA8EEC26481FB (void);
// 0x0000041F System.Void AddRandomVelocity::Update()
extern void AddRandomVelocity_Update_mF22E9293D511149200299475467F71FB153422A5 (void);
// 0x00000420 System.Void AddRandomVelocity::.ctor()
extern void AddRandomVelocity__ctor_m22A503480182A167097A851FC1608ADB9EDC6590 (void);
// 0x00000421 System.Void Blinker::Awake()
extern void Blinker_Awake_m9C51885B879A52D7AE55F6B9C19A1FC548CC55F0 (void);
// 0x00000422 System.Void Blinker::Blink()
extern void Blinker_Blink_mE2AF8E7C64ABDBD2923300009611949F995280AF (void);
// 0x00000423 System.Void Blinker::LateUpdate()
extern void Blinker_LateUpdate_mCB43A52D3EC48BAE96EB43B64AEA0CE689FAA702 (void);
// 0x00000424 System.Void Blinker::.ctor()
extern void Blinker__ctor_m0BB5933E13D30BEAC6F1984A7CA20BB975B723FD (void);
// 0x00000425 System.Void ColliderHighlighter::Awake()
extern void ColliderHighlighter_Awake_mD7548963E0E1A25B64C4ACF3ED60E0208AB7DA0F (void);
// 0x00000426 System.Void ColliderHighlighter::OnEnable()
extern void ColliderHighlighter_OnEnable_mD405F376BE984D8C2DC65BA90F123045425889A2 (void);
// 0x00000427 System.Void ColliderHighlighter::OnDisable()
extern void ColliderHighlighter_OnDisable_m8167AC977EA9FB1824D0C7C94BD2FCEA5EB8152F (void);
// 0x00000428 System.Void ColliderHighlighter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ColliderHighlighter_Solver_OnCollision_mD528A4449F2984FDEB04C486B4FF5D11B16D0C34 (void);
// 0x00000429 System.Void ColliderHighlighter::.ctor()
extern void ColliderHighlighter__ctor_m9038E4292512B0C57E52D54BFAF3AD762E231CBC (void);
// 0x0000042A System.Void CollisionEventHandler::Awake()
extern void CollisionEventHandler_Awake_mE879892B39EE071010C56B69B31A5E783DD6612B (void);
// 0x0000042B System.Void CollisionEventHandler::OnEnable()
extern void CollisionEventHandler_OnEnable_mBA3BC1C421014CD940B0904AE15029255526E366 (void);
// 0x0000042C System.Void CollisionEventHandler::OnDisable()
extern void CollisionEventHandler_OnDisable_mF45281EA8987ADB067FDBA463D39366B33A7055F (void);
// 0x0000042D System.Void CollisionEventHandler::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionEventHandler_Solver_OnCollision_mF61D0D8CED0666E40FC559BB115A6AC3B0AA8520 (void);
// 0x0000042E System.Void CollisionEventHandler::OnDrawGizmos()
extern void CollisionEventHandler_OnDrawGizmos_m79FA90D3A4AB307B865242CE6D4EAF3FBFBD63B7 (void);
// 0x0000042F System.Void CollisionEventHandler::.ctor()
extern void CollisionEventHandler__ctor_m65FB1C1378DBE078E9C55CD6A501540A22E46D4E (void);
// 0x00000430 System.Void DebugParticleFrames::Awake()
extern void DebugParticleFrames_Awake_mDEA8621CC4B7B374DF75A1BE149D6205122BFF2D (void);
// 0x00000431 System.Void DebugParticleFrames::OnDrawGizmos()
extern void DebugParticleFrames_OnDrawGizmos_m1FF932B802AE7F050B9BD7B089FB59A242FD906D (void);
// 0x00000432 System.Void DebugParticleFrames::.ctor()
extern void DebugParticleFrames__ctor_m158351C055E1A30DDC179EC0A2E7F16D9EAD2802 (void);
// 0x00000433 System.Void ExtrapolationCamera::Start()
extern void ExtrapolationCamera_Start_m18BE363AB98243B04A0E91142AB5CF016505F791 (void);
// 0x00000434 System.Void ExtrapolationCamera::FixedUpdate()
extern void ExtrapolationCamera_FixedUpdate_m710C67C238F8F586AD72E4F542039752D09C32EB (void);
// 0x00000435 System.Void ExtrapolationCamera::LateUpdate()
extern void ExtrapolationCamera_LateUpdate_m5ECE6CAF70FB6F9773924ADE53A97521C42D48A4 (void);
// 0x00000436 System.Void ExtrapolationCamera::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ExtrapolationCamera_Teleport_m40D5CC8268DE24BF3592A2538AAD9B0E91D01206 (void);
// 0x00000437 System.Void ExtrapolationCamera::.ctor()
extern void ExtrapolationCamera__ctor_m9B6CB33C322252BC5DBA779DC64B5D4A8FDCEF99 (void);
// 0x00000438 System.Single FPSDisplay::get_CurrentFPS()
extern void FPSDisplay_get_CurrentFPS_mBF5EE37E66C4BD0BDE050A10B96908654CB14112 (void);
// 0x00000439 System.Single FPSDisplay::get_FPSMedian()
extern void FPSDisplay_get_FPSMedian_mA32A92AC63660D7A6505380BE7B0C15F2679B981 (void);
// 0x0000043A System.Single FPSDisplay::get_FPSAverage()
extern void FPSDisplay_get_FPSAverage_mBC9E6262603A5135AF4B3CF6E3A60188D951495B (void);
// 0x0000043B System.Void FPSDisplay::Start()
extern void FPSDisplay_Start_m9DE167885FC272684751D270809F5CD7CBE56661 (void);
// 0x0000043C System.Void FPSDisplay::Update()
extern void FPSDisplay_Update_mE45C5E9F8AA0815DBDBC36FFE099E4E174985930 (void);
// 0x0000043D System.Void FPSDisplay::ResetMedianAndAverage()
extern void FPSDisplay_ResetMedianAndAverage_m700CC1AAF6E0654A6F110B25B10EF96790FECA0A (void);
// 0x0000043E System.Void FPSDisplay::.ctor()
extern void FPSDisplay__ctor_mE5C50AF5EDE70F699D1BDC3B56BF31972E2215CE (void);
// 0x0000043F System.Void FirstPersonLauncher::Update()
extern void FirstPersonLauncher_Update_mFDCF1EF9E8CEF2D5D595B02E7E5F96F23C47C2A7 (void);
// 0x00000440 System.Void FirstPersonLauncher::.ctor()
extern void FirstPersonLauncher__ctor_mBD44730194215A6C9068F4C8CBD17A6B36B0053B (void);
// 0x00000441 System.Void ObiActorTeleport::Teleport()
extern void ObiActorTeleport_Teleport_m8ACCEBEFA199CC4C0783DB1A96B79E527E9796D4 (void);
// 0x00000442 System.Void ObiActorTeleport::.ctor()
extern void ObiActorTeleport__ctor_m94A57B1EF294D587000F1A95B4B1202695A60EA2 (void);
// 0x00000443 System.Void ObiParticleCounter::Awake()
extern void ObiParticleCounter_Awake_mB252C00950761A480BAFEAAC8861A5CFF9550951 (void);
// 0x00000444 System.Void ObiParticleCounter::OnEnable()
extern void ObiParticleCounter_OnEnable_mBA0242368FF5BAB41CA489AFBC8E28947DF6F5EC (void);
// 0x00000445 System.Void ObiParticleCounter::OnDisable()
extern void ObiParticleCounter_OnDisable_mFAFED9D4809DD1A2F751B0085E3C658E86AEF62E (void);
// 0x00000446 System.Void ObiParticleCounter::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiParticleCounter_Solver_OnCollision_m508741B60529B3DB6163E0C8B7816F6600692BCF (void);
// 0x00000447 System.Void ObiParticleCounter::.ctor()
extern void ObiParticleCounter__ctor_m084AC398EBDA0A6A47F8E48EFBA58982612BDE80 (void);
// 0x00000448 System.Void ObjectDragger::OnMouseDown()
extern void ObjectDragger_OnMouseDown_mD327370BC525D8D7547255D7FB659E348CD357D3 (void);
// 0x00000449 System.Void ObjectDragger::OnMouseDrag()
extern void ObjectDragger_OnMouseDrag_mACC1C3F00553438D26EC4D58915E082960DE5F85 (void);
// 0x0000044A System.Void ObjectDragger::.ctor()
extern void ObjectDragger__ctor_m0945194968142437BDB43638A1F691E34015FB5E (void);
// 0x0000044B System.Void ObjectLimit::Update()
extern void ObjectLimit_Update_m92A3CA606475C1A1D2AD3C46FD42C5A637E917BB (void);
// 0x0000044C System.Void ObjectLimit::.ctor()
extern void ObjectLimit__ctor_m0E7593045B03E02A861D17028B07076FE90E36A5 (void);
// 0x0000044D System.Void SlowmoToggler::Slowmo(System.Boolean)
extern void SlowmoToggler_Slowmo_m78F297084A3A842FCB021D175755FDD06F328042 (void);
// 0x0000044E System.Void SlowmoToggler::.ctor()
extern void SlowmoToggler__ctor_mCD5D9F92D3C3A1FA3C20BD57E5829E1959FE51B0 (void);
// 0x0000044F System.Void WorldSpaceGravity::Awake()
extern void WorldSpaceGravity_Awake_mE1964D804311A22C20AFFA04E73EE8561865CA77 (void);
// 0x00000450 System.Void WorldSpaceGravity::Update()
extern void WorldSpaceGravity_Update_m5075642D9AE74E08C02C1EF8AF60F4A362E2561D (void);
// 0x00000451 System.Void WorldSpaceGravity::.ctor()
extern void WorldSpaceGravity__ctor_m62438629F8754A79863207A1D7216A45C95F3601 (void);
// 0x00000452 System.Void CustomShadowResolution::Update()
extern void CustomShadowResolution_Update_m4A82927108D89BBAE4A9B678697723AD9B2BDE7A (void);
// 0x00000453 System.Void CustomShadowResolution::.ctor()
extern void CustomShadowResolution__ctor_mEC5E0FA947B95A83CC9906600C052852E1E2EE08 (void);
// 0x00000454 System.Void FrameByFrameRendering_Default::Start()
extern void FrameByFrameRendering_Default_Start_mDB34175A1329CA70EE9BADC4D93D4615684295F3 (void);
// 0x00000455 System.Void FrameByFrameRendering_Default::Update()
extern void FrameByFrameRendering_Default_Update_m3651056826EC081A81578F9F33B0BE2E702C7311 (void);
// 0x00000456 System.Void FrameByFrameRendering_Default::.ctor()
extern void FrameByFrameRendering_Default__ctor_m5C19E2780BE0259895809C153FA7DE9627D8EC47 (void);
// 0x00000457 System.Void FrameByFrameRendering_Manual::Start()
extern void FrameByFrameRendering_Manual_Start_m0FF7E063255BD97266AACC470DDCFDCA6D3864FF (void);
// 0x00000458 System.Void FrameByFrameRendering_Manual::Update()
extern void FrameByFrameRendering_Manual_Update_mD33F8DE37196CF376D4CD1A2FAA7018A4BF4F876 (void);
// 0x00000459 System.Void FrameByFrameRendering_Manual::.ctor()
extern void FrameByFrameRendering_Manual__ctor_mEAEBCA964753257395356113A94E78C82B64EE69 (void);
// 0x0000045A System.Void SmoothObjectNormalHelper::LateUpdate()
extern void SmoothObjectNormalHelper_LateUpdate_m7FCE4EFFAFB658DA2D02DF953013C16869D61283 (void);
// 0x0000045B System.Void SmoothObjectNormalHelper::.ctor()
extern void SmoothObjectNormalHelper__ctor_mB626BE00D5D8A2FE44879DA086EB0F8A0BCB344F (void);
// 0x0000045C System.Void EyeMovement_URP::Update()
extern void EyeMovement_URP_Update_m54F920500EEF67518CD5800F199B8036A8A01FD6 (void);
// 0x0000045D System.Void EyeMovement_URP::.ctor()
extern void EyeMovement_URP__ctor_mAE0D716EC73062762192F9798DA917B0FEE11D0D (void);
// 0x0000045E System.Void Obi.ObiCharacter::Start()
extern void ObiCharacter_Start_mD9672416FAC12184B8BDFCBBD2C3B584D9119967 (void);
// 0x0000045F System.Void Obi.ObiCharacter::Move(UnityEngine.Vector3,System.Boolean,System.Boolean)
extern void ObiCharacter_Move_mDB588583845391E57799EBB3D9759C09B0CAD4D6 (void);
// 0x00000460 System.Void Obi.ObiCharacter::ScaleCapsuleForCrouching(System.Boolean)
extern void ObiCharacter_ScaleCapsuleForCrouching_m26D40D4BAE34DAB3C392B17BA39C8FA96671E0B9 (void);
// 0x00000461 System.Void Obi.ObiCharacter::PreventStandingInLowHeadroom()
extern void ObiCharacter_PreventStandingInLowHeadroom_m4B56915CD2A223C7387E2C98DEA597BE6160DBEA (void);
// 0x00000462 System.Void Obi.ObiCharacter::UpdateAnimator(UnityEngine.Vector3)
extern void ObiCharacter_UpdateAnimator_m31A22B166E3BAABB61E5FBCAEC563403C47E8CEC (void);
// 0x00000463 System.Void Obi.ObiCharacter::HandleAirborneMovement()
extern void ObiCharacter_HandleAirborneMovement_mF9CB87C328B6E70CD75161167C2DBC24066D2F18 (void);
// 0x00000464 System.Void Obi.ObiCharacter::HandleGroundedMovement(System.Boolean,System.Boolean)
extern void ObiCharacter_HandleGroundedMovement_mCE5389E5C95BBEDF93AEE8D8C702DD334632CA0A (void);
// 0x00000465 System.Void Obi.ObiCharacter::ApplyExtraTurnRotation()
extern void ObiCharacter_ApplyExtraTurnRotation_m0158F9ED1DADBB4DB1778E1769404F4E4519EB3F (void);
// 0x00000466 System.Void Obi.ObiCharacter::OnAnimatorMove()
extern void ObiCharacter_OnAnimatorMove_m1E554779AB1DB14D5C35B9776D904A93D398EE38 (void);
// 0x00000467 System.Void Obi.ObiCharacter::CheckGroundStatus()
extern void ObiCharacter_CheckGroundStatus_m9ECAA5E8638F313F5BCC5D7CB682717B0FA3BDA3 (void);
// 0x00000468 System.Void Obi.ObiCharacter::.ctor()
extern void ObiCharacter__ctor_mB01C01015198C990E5D95E958A237B0F937E64D9 (void);
// 0x00000469 System.Void Obi.SampleCharacterController::Start()
extern void SampleCharacterController_Start_m60F6CB41550DAAFE822AC776E9991D2EA49F22AD (void);
// 0x0000046A System.Void Obi.SampleCharacterController::FixedUpdate()
extern void SampleCharacterController_FixedUpdate_mEF05AB0405E5C7DDD2541C9B11018A6F38D0C518 (void);
// 0x0000046B System.Void Obi.SampleCharacterController::.ctor()
extern void SampleCharacterController__ctor_mDBB94C75A2743501D65D57DFEBEAEF85A0B7C38C (void);
// 0x0000046C System.Void Obi.ColorFromPhase::Awake()
extern void ColorFromPhase_Awake_mFDAF56183456E601E4B67ADD52B6857CABA8DFA3 (void);
// 0x0000046D System.Void Obi.ColorFromPhase::LateUpdate()
extern void ColorFromPhase_LateUpdate_m341E0A8F72D57ABB72F0B731ADD6A3B92FEDB85C (void);
// 0x0000046E System.Void Obi.ColorFromPhase::.ctor()
extern void ColorFromPhase__ctor_mE6F652DA0DCB109E6452388955C725262A3E75A6 (void);
// 0x0000046F System.Void Obi.ColorFromVelocity::Awake()
extern void ColorFromVelocity_Awake_mE5B9EAF1C7446ACE20FEAB352DF0BB7570E43CE6 (void);
// 0x00000470 System.Void Obi.ColorFromVelocity::OnEnable()
extern void ColorFromVelocity_OnEnable_mC10C6ED7634FB12AF978C9025E1E8233AF8E6939 (void);
// 0x00000471 System.Void Obi.ColorFromVelocity::LateUpdate()
extern void ColorFromVelocity_LateUpdate_m643273185816893AD0D09C3A0B0DA5BB7491CB9D (void);
// 0x00000472 System.Void Obi.ColorFromVelocity::.ctor()
extern void ColorFromVelocity__ctor_m47F43FDEB8166D362328048C31F2D55685AAB729 (void);
// 0x00000473 System.Void Obi.ColorRandomizer::Start()
extern void ColorRandomizer_Start_m847D4C2A64460979346CA8ADDAFE1C30D29E7B11 (void);
// 0x00000474 System.Void Obi.ColorRandomizer::.ctor()
extern void ColorRandomizer__ctor_m717FD604F1CEF39623E0EB75DFCA9C055D94094D (void);
// 0x00000475 System.Void Obi.LookAroundCamera::Awake()
extern void LookAroundCamera_Awake_m836F7040257EA46F94A52A03E83D2BBE52A2110A (void);
// 0x00000476 System.Void Obi.LookAroundCamera::LookAt(UnityEngine.Vector3,UnityEngine.Vector3)
extern void LookAroundCamera_LookAt_mFB2F30F7FCD622190F52F3CEF7C8034B2C333CED (void);
// 0x00000477 System.Void Obi.LookAroundCamera::UpdateShot()
extern void LookAroundCamera_UpdateShot_m3B8DFE20DCAA2931E7DE8E50B29AA2FE2E6CF48F (void);
// 0x00000478 System.Void Obi.LookAroundCamera::LateUpdate()
extern void LookAroundCamera_LateUpdate_m6EA5FE3CB3DA88F43717405B349639C1980BE1DA (void);
// 0x00000479 System.Void Obi.LookAroundCamera::.ctor()
extern void LookAroundCamera__ctor_m506721CFC9EC4A3765D79AD4811BA19D1F295227 (void);
// 0x0000047A System.Void Obi.LookAroundCamera/CameraShot::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3,System.Single)
extern void CameraShot__ctor_m5D4853E2044D14AAB4D03B53A4C7409EDB5B2006 (void);
// 0x0000047B System.Void Obi.MoveAndRotate::Start()
extern void MoveAndRotate_Start_m663E9DF6947BDDF2A719E6CDC4DC3DF42F6CE653 (void);
// 0x0000047C System.Void Obi.MoveAndRotate::FixedUpdate()
extern void MoveAndRotate_FixedUpdate_m6B299C18D85880FA61BFE34D1D101D836949390C (void);
// 0x0000047D System.Void Obi.MoveAndRotate::.ctor()
extern void MoveAndRotate__ctor_m298F94FA31EFD02624375FF0A44BF09B631608D4 (void);
// 0x0000047E System.Void Obi.MoveAndRotate/Vector3andSpace::.ctor()
extern void Vector3andSpace__ctor_mC900A3B74BFD7DF5F677A5A010EB658632FEF7F2 (void);
// 0x0000047F System.Collections.Generic.IEnumerable`1<System.String> SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_mB2E6CD262A489290E32BDD22E29D1D972891CC98 (void);
// 0x00000480 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865 (void);
// 0x00000481 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96 (void);
// 0x00000482 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E (void);
// 0x00000483 System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26 (void);
// 0x00000484 System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580 (void);
// 0x00000485 System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76 (void);
// 0x00000486 System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C (void);
// 0x00000487 System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E (void);
// 0x00000488 System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0 (void);
// 0x00000489 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD (void);
// 0x0000048A System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461 (void);
// 0x0000048B System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31 (void);
// 0x0000048C System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60 (void);
// 0x0000048D System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1 (void);
// 0x0000048E System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4 (void);
// 0x0000048F SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653 (void);
// 0x00000490 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE (void);
// 0x00000491 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118 (void);
// 0x00000492 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395 (void);
// 0x00000493 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F (void);
// 0x00000494 System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75 (void);
// 0x00000495 System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698 (void);
// 0x00000496 System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x00000497 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x00000498 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8 (void);
// 0x00000499 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098 (void);
// 0x0000049A System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1 (void);
// 0x0000049B System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C (void);
// 0x0000049C System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC (void);
// 0x0000049D System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63 (void);
// 0x0000049E System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947 (void);
// 0x0000049F System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7 (void);
// 0x000004A0 SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A (void);
// 0x000004A1 SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B (void);
// 0x000004A2 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84 (void);
// 0x000004A3 System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D (void);
// 0x000004A4 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4 (void);
// 0x000004A5 System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487 (void);
// 0x000004A6 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837 (void);
// 0x000004A7 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552 (void);
// 0x000004A8 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5 (void);
// 0x000004A9 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18 (void);
// 0x000004AA SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477 (void);
// 0x000004AB System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099 (void);
// 0x000004AC System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E (void);
// 0x000004AD System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5 (void);
// 0x000004AE System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0 (void);
// 0x000004AF System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC (void);
// 0x000004B0 System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6 (void);
// 0x000004B1 System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69 (void);
// 0x000004B2 SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9 (void);
// 0x000004B3 System.Void SimpleJSON.JSONNode::Serialize(System.IO.BinaryWriter)
extern void JSONNode_Serialize_mD8A4B156C9FA37AB8FA4ABA6B22D8F1A9B27CC3B (void);
// 0x000004B4 System.Void SimpleJSON.JSONNode::SaveToStream(System.IO.Stream)
extern void JSONNode_SaveToStream_m46C0008EB9F1F7350E55DD57CEFAEFF36DDCF523 (void);
// 0x000004B5 System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_mF87F91D2D54DA91BCAA6715B42B5D8F4482DF062 (void);
// 0x000004B6 System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m72489371227B0206AC10DDDAC077225AB02D0FAB (void);
// 0x000004B7 System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mE6579E9F545077594603F08862654B3FABF11186 (void);
// 0x000004B8 System.Void SimpleJSON.JSONNode::SaveToFile(System.String)
extern void JSONNode_SaveToFile_m11795B826D93616FA6C477ED66A40CB3D2C1E372 (void);
// 0x000004B9 System.String SimpleJSON.JSONNode::SaveToBase64()
extern void JSONNode_SaveToBase64_mF2EE9BA46DB1AF86A0B455DA13A3550E8B641B16 (void);
// 0x000004BA SimpleJSON.JSONNode SimpleJSON.JSONNode::Deserialize(System.IO.BinaryReader)
extern void JSONNode_Deserialize_m30D4EEBBFA933D823AA50A5DE03E07C570F443A4 (void);
// 0x000004BB SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m576D63C52D5BF2EE31524033FB81BFA831B4EEB0 (void);
// 0x000004BC SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_mA161803714EFBF3EC65934585069E8DB530DAC55 (void);
// 0x000004BD SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_m07944ECC5086AEFB5DB67E5BCA4E3FCE98FCB39C (void);
// 0x000004BE SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromStream(System.IO.Stream)
extern void JSONNode_LoadFromStream_mCF525FAC941AD38BB9BC0A7BE0128C44BE96A783 (void);
// 0x000004BF SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromFile(System.String)
extern void JSONNode_LoadFromFile_mA14F24B61F89F4C5D8465C908AC104BCA6561A31 (void);
// 0x000004C0 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBase64(System.String)
extern void JSONNode_LoadFromBase64_m0B9BCACA8F672CB6EE0BDD06C570CDDD3B928E2F (void);
// 0x000004C1 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE (void);
// 0x000004C2 System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD (void);
// 0x000004C3 System.Void SimpleJSON.JSONNode/<get_Keys>d__1::.ctor(System.Int32)
extern void U3Cget_KeysU3Ed__1__ctor_m31B22F928A08E7B411030A15D7D511E9C6826567 (void);
// 0x000004C4 System.Void SimpleJSON.JSONNode/<get_Keys>d__1::System.IDisposable.Dispose()
extern void U3Cget_KeysU3Ed__1_System_IDisposable_Dispose_mCEE33DEA3B47BE82D24E16E43A3405BB12911FF7 (void);
// 0x000004C5 System.Boolean SimpleJSON.JSONNode/<get_Keys>d__1::MoveNext()
extern void U3Cget_KeysU3Ed__1_MoveNext_m4B6CE8B2D02CF534EAAB72E262AAE8A07B5680F5 (void);
// 0x000004C6 System.String SimpleJSON.JSONNode/<get_Keys>d__1::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_KeysU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m75F3E48104D74C7DEC2FAB44B93E2E7F4A274C73 (void);
// 0x000004C7 System.Void SimpleJSON.JSONNode/<get_Keys>d__1::System.Collections.IEnumerator.Reset()
extern void U3Cget_KeysU3Ed__1_System_Collections_IEnumerator_Reset_mC84561A668E4BB4FAC92044F2DFAACB32C4934D4 (void);
// 0x000004C8 System.Object SimpleJSON.JSONNode/<get_Keys>d__1::System.Collections.IEnumerator.get_Current()
extern void U3Cget_KeysU3Ed__1_System_Collections_IEnumerator_get_Current_m0D5E782AC56841198E470369005371EAA86AB3EC (void);
// 0x000004C9 System.Collections.Generic.IEnumerator`1<System.String> SimpleJSON.JSONNode/<get_Keys>d__1::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_KeysU3Ed__1_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mE403F886111FC9CB8936AC10998F237687EC9547 (void);
// 0x000004CA System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Keys>d__1::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_KeysU3Ed__1_System_Collections_IEnumerable_GetEnumerator_m7F27D903D8E8B11413C5D576B6A5DF90B4F48A29 (void);
// 0x000004CB System.Void SimpleJSON.JSONNode/<get_Children>d__31::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__31__ctor_m8F6FEDFD2CBB567FB3C44794CF5B5FEA0E7D0981 (void);
// 0x000004CC System.Void SimpleJSON.JSONNode/<get_Children>d__31::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__31_System_IDisposable_Dispose_mA50D783DEF319BC009881D9258382CC9D4A3FF68 (void);
// 0x000004CD System.Boolean SimpleJSON.JSONNode/<get_Children>d__31::MoveNext()
extern void U3Cget_ChildrenU3Ed__31_MoveNext_m40E4302F968AB61EB4995AFBEC337FE878B2BCD8 (void);
// 0x000004CE SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__31::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__31_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m621BDAC43DA20BF53C7413FFD49DD655000C13B1 (void);
// 0x000004CF System.Void SimpleJSON.JSONNode/<get_Children>d__31::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerator_Reset_m47AC690AE79658F787D1CB8C08D3D1C0488A39EC (void);
// 0x000004D0 System.Object SimpleJSON.JSONNode/<get_Children>d__31::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerator_get_Current_m631428405E88D0CA4CB1EB5495BEE79598F9146B (void);
// 0x000004D1 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__31::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__31_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m529466030E53F1DCA736E9EAD8ECD0C8EFF091DE (void);
// 0x000004D2 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__31::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m158067C4E4DEE7D06269BFF13156BBC6165A409C (void);
// 0x000004D3 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__33::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__33__ctor_m6556D8B2C4E7ED9752AFFA558BFF563240D0C419 (void);
// 0x000004D4 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__33_System_IDisposable_Dispose_m0C16C4312921C7F1FFA135FABE659152DA55D864 (void);
// 0x000004D5 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__33::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__33_MoveNext_m35A09F1D6B7CF89200082A8B14A3A0EBCC6DBA5E (void);
// 0x000004D6 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__33::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__33_U3CU3Em__Finally1_m391A8C008DB875F4176B93BE66EFC26A4829A02B (void);
// 0x000004D7 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__33::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__33_U3CU3Em__Finally2_m3E1A5C6226630A2C82545B43701EB82DFB03C51D (void);
// 0x000004D8 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__33_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF4C5AFBCF250BF1CA96F70247B53704BD20903D7 (void);
// 0x000004D9 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerator_Reset_mE9263C830CF629B7F8D65FEA16666E756330C49A (void);
// 0x000004DA System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerator_get_Current_mA6B6DDC581FB0CF09EAD8FD26B3B86463CC22286 (void);
// 0x000004DB System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__33_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF3DE030FDACE36ACD6E736CA90D8B3A02671485E (void);
// 0x000004DC System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__33::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m7ABE068F347314D21AAABEC79E46B34E3FE5F56C (void);
// 0x000004DD SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE (void);
// 0x000004DE System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03 (void);
// 0x000004DF SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10 (void);
// 0x000004E0 System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885 (void);
// 0x000004E1 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215 (void);
// 0x000004E2 System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE (void);
// 0x000004E3 System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647 (void);
// 0x000004E4 System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D (void);
// 0x000004E5 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F (void);
// 0x000004E6 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049 (void);
// 0x000004E7 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85 (void);
// 0x000004E8 System.Collections.IEnumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m946A4A17D8F354B270A313F0967F6539662C5B14 (void);
// 0x000004E9 System.Void SimpleJSON.JSONArray::Serialize(System.IO.BinaryWriter)
extern void JSONArray_Serialize_m2D871FA5430E31F65BE9EE079ACB39A6559E753C (void);
// 0x000004EA System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C (void);
// 0x000004EB System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA (void);
// 0x000004EC System.Void SimpleJSON.JSONArray/<get_Children>d__18::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__18__ctor_m8A2BBBE94FF9E527B67162C99C5BE2E41D21B3C8 (void);
// 0x000004ED System.Void SimpleJSON.JSONArray/<get_Children>d__18::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__18_System_IDisposable_Dispose_m131403239E7888DF9608A2086961BA6203163F73 (void);
// 0x000004EE System.Boolean SimpleJSON.JSONArray/<get_Children>d__18::MoveNext()
extern void U3Cget_ChildrenU3Ed__18_MoveNext_m1CD60520FBB35990617570CCD0C6C54A954E2371 (void);
// 0x000004EF System.Void SimpleJSON.JSONArray/<get_Children>d__18::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__18_U3CU3Em__Finally1_m79651C2EEC5E19C6A774899A3FB1AF290FB7D632 (void);
// 0x000004F0 SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__18::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__18_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mE655E5ABB445B1286650C5150968A5DF162108BA (void);
// 0x000004F1 System.Void SimpleJSON.JSONArray/<get_Children>d__18::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerator_Reset_mA0096AB464F0880367669DCB22255BCF8270C2AE (void);
// 0x000004F2 System.Object SimpleJSON.JSONArray/<get_Children>d__18::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerator_get_Current_m352AF0622FC2693D4ADEC8234288CC87DD1431A6 (void);
// 0x000004F3 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__18::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__18_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m975BED2D279FF4DC2672F45E0CAAB074B5B14D24 (void);
// 0x000004F4 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__18::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerable_GetEnumerator_m17D327415306FBB8B51C370A0E513D682A67F7C7 (void);
// 0x000004F5 System.Void SimpleJSON.JSONArray/<GetEnumerator>d__19::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__19__ctor_m3519C4DACB103DC807C9DF71E541DEE82EF9808B (void);
// 0x000004F6 System.Void SimpleJSON.JSONArray/<GetEnumerator>d__19::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__19_System_IDisposable_Dispose_mE824F34CC804B44662C65501124D8A1CAD898858 (void);
// 0x000004F7 System.Boolean SimpleJSON.JSONArray/<GetEnumerator>d__19::MoveNext()
extern void U3CGetEnumeratorU3Ed__19_MoveNext_mA13864872CE8E506B539447FD8FB82E364D11A19 (void);
// 0x000004F8 System.Void SimpleJSON.JSONArray/<GetEnumerator>d__19::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__19_U3CU3Em__Finally1_mE58BA16207C8BF557C667239DF595312E8DEF157 (void);
// 0x000004F9 System.Object SimpleJSON.JSONArray/<GetEnumerator>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9DDA20D156CFC5FC1184371E2AEE55A7171412E (void);
// 0x000004FA System.Void SimpleJSON.JSONArray/<GetEnumerator>d__19::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__19_System_Collections_IEnumerator_Reset_m3C8120B53988728D4E5645CD89BC20BD1B82982A (void);
// 0x000004FB System.Object SimpleJSON.JSONArray/<GetEnumerator>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__19_System_Collections_IEnumerator_get_Current_m7CD471DEA843AEA7E0E892A970BE2143D17E41E3 (void);
// 0x000004FC SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486 (void);
// 0x000004FD System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD (void);
// 0x000004FE System.Collections.Generic.IEnumerable`1<System.String> SimpleJSON.JSONObject::get_Keys()
extern void JSONObject_get_Keys_m3A9DB65AF2CA08FD679CC57F7F0E17C4DA798D7B (void);
// 0x000004FF SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF (void);
// 0x00000500 System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B (void);
// 0x00000501 SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976 (void);
// 0x00000502 System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E (void);
// 0x00000503 System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF (void);
// 0x00000504 System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D (void);
// 0x00000505 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7 (void);
// 0x00000506 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE (void);
// 0x00000507 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15 (void);
// 0x00000508 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6 (void);
// 0x00000509 System.Collections.IEnumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_m5BFA47C417BF72FA5A9DDD18822A3D92FCCE2802 (void);
// 0x0000050A System.Void SimpleJSON.JSONObject::Serialize(System.IO.BinaryWriter)
extern void JSONObject_Serialize_m705D9632D662D77E8A6EAD74EE69FEA48EE0DF2C (void);
// 0x0000050B System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E (void);
// 0x0000050C System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F (void);
// 0x0000050D System.Void SimpleJSON.JSONObject/<get_Keys>d__7::.ctor(System.Int32)
extern void U3Cget_KeysU3Ed__7__ctor_m8256372ABD3AD5AAB0C9DB23F779DBAFB396E877 (void);
// 0x0000050E System.Void SimpleJSON.JSONObject/<get_Keys>d__7::System.IDisposable.Dispose()
extern void U3Cget_KeysU3Ed__7_System_IDisposable_Dispose_m709766446222B475AB0550365840C2DF96568E3A (void);
// 0x0000050F System.Boolean SimpleJSON.JSONObject/<get_Keys>d__7::MoveNext()
extern void U3Cget_KeysU3Ed__7_MoveNext_m1290B50AD99CBEB9E1B3822BC69B2CA4B92FAC3E (void);
// 0x00000510 System.Void SimpleJSON.JSONObject/<get_Keys>d__7::<>m__Finally1()
extern void U3Cget_KeysU3Ed__7_U3CU3Em__Finally1_m1F5A55E18106B1B85F04C8640960A71FB1B663FD (void);
// 0x00000511 System.String SimpleJSON.JSONObject/<get_Keys>d__7::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3Cget_KeysU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m5F91D9FA31882E9EFADE7628A032D1BE9ED01EEC (void);
// 0x00000512 System.Void SimpleJSON.JSONObject/<get_Keys>d__7::System.Collections.IEnumerator.Reset()
extern void U3Cget_KeysU3Ed__7_System_Collections_IEnumerator_Reset_m8CCFF1DB60A4659FD0DFD8053DC5BB1D79236CD2 (void);
// 0x00000513 System.Object SimpleJSON.JSONObject/<get_Keys>d__7::System.Collections.IEnumerator.get_Current()
extern void U3Cget_KeysU3Ed__7_System_Collections_IEnumerator_get_Current_m1E9FF99A66827940C935823D10BD1B228988C81C (void);
// 0x00000514 System.Collections.Generic.IEnumerator`1<System.String> SimpleJSON.JSONObject/<get_Keys>d__7::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3Cget_KeysU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m27F64964C2D5F9B90BDD39A97F4BEE7D873943E0 (void);
// 0x00000515 System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Keys>d__7::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_KeysU3Ed__7_System_Collections_IEnumerable_GetEnumerator_mC743CC524C8244B1252852B1B9DAB61B1D889D82 (void);
// 0x00000516 System.Void SimpleJSON.JSONObject/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_m1A4CD6E6D85675EE935ECF89DD4029272B6E2D4A (void);
// 0x00000517 System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass19_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass19_0_U3CRemoveU3Eb__0_m0031F9D24A86A93EC7282002744BA70B5C0BADD0 (void);
// 0x00000518 System.Void SimpleJSON.JSONObject/<get_Children>d__21::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__21__ctor_mFF58DDBB7D0C1CE865AA301B26B6FDF260988FF7 (void);
// 0x00000519 System.Void SimpleJSON.JSONObject/<get_Children>d__21::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__21_System_IDisposable_Dispose_m74E7A8F92CB73C9F3459155A08FEC13013AA07AA (void);
// 0x0000051A System.Boolean SimpleJSON.JSONObject/<get_Children>d__21::MoveNext()
extern void U3Cget_ChildrenU3Ed__21_MoveNext_mDE2E2C71804DC023D743BF7B3260B5065A2DDC27 (void);
// 0x0000051B System.Void SimpleJSON.JSONObject/<get_Children>d__21::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__21_U3CU3Em__Finally1_mABCC4CA7E7DA6BFBF5B8D25750656D146D76D9AB (void);
// 0x0000051C SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__21::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__21_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mCFF54B790DE5CB1B9D12EA2BD0EA0F3517F85249 (void);
// 0x0000051D System.Void SimpleJSON.JSONObject/<get_Children>d__21::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerator_Reset_mE92B65B3DDA55AB7049F67DAF4D6E8D9BC8F45EA (void);
// 0x0000051E System.Object SimpleJSON.JSONObject/<get_Children>d__21::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerator_get_Current_m2669987DCE20D237DE880133D52DE160363689E1 (void);
// 0x0000051F System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__21::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__21_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF0A115DF1C4FFDF65675931C6E9296F8F173EACD (void);
// 0x00000520 System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__21::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m4BC7E51A8F4519F5A87014F8BF064C2F09B06490 (void);
// 0x00000521 System.Void SimpleJSON.JSONObject/<GetEnumerator>d__22::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__22__ctor_mD19E8726BE2F6BF6FA86D25009FB93C52C1B3E82 (void);
// 0x00000522 System.Void SimpleJSON.JSONObject/<GetEnumerator>d__22::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__22_System_IDisposable_Dispose_m4DE772784AE4D0ACCF401372E2E9E52F021968B0 (void);
// 0x00000523 System.Boolean SimpleJSON.JSONObject/<GetEnumerator>d__22::MoveNext()
extern void U3CGetEnumeratorU3Ed__22_MoveNext_m89DBEB3B00A4969674554AD576B4B90B76CAB6A2 (void);
// 0x00000524 System.Void SimpleJSON.JSONObject/<GetEnumerator>d__22::<>m__Finally1()
extern void U3CGetEnumeratorU3Ed__22_U3CU3Em__Finally1_m34F486AC0FCAFBE94E8916FF21102B50FEDEA8D9 (void);
// 0x00000525 System.Object SimpleJSON.JSONObject/<GetEnumerator>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGetEnumeratorU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEDA3BCA87876262144946F76847AB270591FEB6 (void);
// 0x00000526 System.Void SimpleJSON.JSONObject/<GetEnumerator>d__22::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__22_System_Collections_IEnumerator_Reset_m0E6C15FFF0ED45C2DB4DB3989D9B80D814046549 (void);
// 0x00000527 System.Object SimpleJSON.JSONObject/<GetEnumerator>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__22_System_Collections_IEnumerator_get_Current_m63EA31BA062240254C552EB36D0479C9D13C4529 (void);
// 0x00000528 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185 (void);
// 0x00000529 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E (void);
// 0x0000052A System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A (void);
// 0x0000052B System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF (void);
// 0x0000052C System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA (void);
// 0x0000052D System.Void SimpleJSON.JSONString::Serialize(System.IO.BinaryWriter)
extern void JSONString_Serialize_mB6AF2698D7B61FB566B258B381056A5291804168 (void);
// 0x0000052E System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8 (void);
// 0x0000052F System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09 (void);
// 0x00000530 System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1 (void);
// 0x00000531 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F (void);
// 0x00000532 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912 (void);
// 0x00000533 System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB (void);
// 0x00000534 System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0 (void);
// 0x00000535 System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399 (void);
// 0x00000536 System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68 (void);
// 0x00000537 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8 (void);
// 0x00000538 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F (void);
// 0x00000539 System.Void SimpleJSON.JSONNumber::Serialize(System.IO.BinaryWriter)
extern void JSONNumber_Serialize_m56B7577953CB962BEAA82D7677523CAAEE91A074 (void);
// 0x0000053A System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B (void);
// 0x0000053B System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887 (void);
// 0x0000053C System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931 (void);
// 0x0000053D System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5 (void);
// 0x0000053E SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A (void);
// 0x0000053F System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA (void);
// 0x00000540 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6 (void);
// 0x00000541 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A (void);
// 0x00000542 System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9 (void);
// 0x00000543 System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91 (void);
// 0x00000544 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD (void);
// 0x00000545 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE (void);
// 0x00000546 System.Void SimpleJSON.JSONBool::Serialize(System.IO.BinaryWriter)
extern void JSONBool_Serialize_m649615169DA6E401935D5AFFAB7EEF65A1E1A6C5 (void);
// 0x00000547 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9 (void);
// 0x00000548 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09 (void);
// 0x00000549 System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A (void);
// 0x0000054A SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936 (void);
// 0x0000054B System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA (void);
// 0x0000054C System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60 (void);
// 0x0000054D System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922 (void);
// 0x0000054E System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98 (void);
// 0x0000054F System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000 (void);
// 0x00000550 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600 (void);
// 0x00000551 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B (void);
// 0x00000552 System.Void SimpleJSON.JSONNull::Serialize(System.IO.BinaryWriter)
extern void JSONNull_Serialize_m750D4DFC23CE85F6E833E89B6CE53D6476C9BB48 (void);
// 0x00000553 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E (void);
// 0x00000554 System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F (void);
// 0x00000555 SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D (void);
// 0x00000556 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798 (void);
// 0x00000557 System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188 (void);
// 0x00000558 System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779 (void);
// 0x00000559 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047 (void);
// 0x0000055A System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606 (void);
// 0x0000055B SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C (void);
// 0x0000055C System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC (void);
// 0x0000055D System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670 (void);
// 0x0000055E System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F (void);
// 0x0000055F System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7 (void);
// 0x00000560 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95 (void);
// 0x00000561 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E (void);
// 0x00000562 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43 (void);
// 0x00000563 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A (void);
// 0x00000564 System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857 (void);
// 0x00000565 System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870 (void);
// 0x00000566 System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934 (void);
// 0x00000567 System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465 (void);
// 0x00000568 System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350 (void);
// 0x00000569 System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE (void);
// 0x0000056A System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2 (void);
// 0x0000056B SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D (void);
// 0x0000056C SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2 (void);
// 0x0000056D System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F (void);
// 0x0000056E SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9 (void);
// 0x0000056F System.Void AkilliMum.Standard.Invisible.SimpleRotater::FixedUpdate()
extern void SimpleRotater_FixedUpdate_mBCA4D633FA62C6F78827C34702753DCBB2926936 (void);
// 0x00000570 System.Void AkilliMum.Standard.Invisible.SimpleRotater::.ctor()
extern void SimpleRotater__ctor_m3557D6B06C2004B51FDBF6111124102E10E1AE5E (void);
// 0x00000571 System.Void UnityTemplateProjects.SimpleCameraController::OnEnable()
extern void SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8 (void);
// 0x00000572 UnityEngine.Vector3 UnityTemplateProjects.SimpleCameraController::GetInputTranslationDirection()
extern void SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47 (void);
// 0x00000573 System.Void UnityTemplateProjects.SimpleCameraController::Update()
extern void SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F (void);
// 0x00000574 System.Single UnityTemplateProjects.SimpleCameraController::GetBoostFactor()
extern void SimpleCameraController_GetBoostFactor_m6C4E35CBB2577F112CF6F1FDF139E9187BAE8264 (void);
// 0x00000575 UnityEngine.Vector2 UnityTemplateProjects.SimpleCameraController::GetInputLookRotation()
extern void SimpleCameraController_GetInputLookRotation_mF8174D1397CCC68B1D36541A2AC09F183DAD70DF (void);
// 0x00000576 System.Boolean UnityTemplateProjects.SimpleCameraController::IsBoostPressed()
extern void SimpleCameraController_IsBoostPressed_m11707E0D8E2E4FCC72DBDE58D2C9D213AF34C39B (void);
// 0x00000577 System.Boolean UnityTemplateProjects.SimpleCameraController::IsEscapePressed()
extern void SimpleCameraController_IsEscapePressed_m481C4E60AB6E6D91CF8049996FA0F143EDE04697 (void);
// 0x00000578 System.Boolean UnityTemplateProjects.SimpleCameraController::IsCameraRotationAllowed()
extern void SimpleCameraController_IsCameraRotationAllowed_mA079105CE1BFC131A87F6FA443C583CE16338F86 (void);
// 0x00000579 System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonDown()
extern void SimpleCameraController_IsRightMouseButtonDown_m76909E007FF408C0B24965901DEA129CC4D328CB (void);
// 0x0000057A System.Boolean UnityTemplateProjects.SimpleCameraController::IsRightMouseButtonUp()
extern void SimpleCameraController_IsRightMouseButtonUp_m5F0915186A6101ED2059B9AFE8ED96311608145A (void);
// 0x0000057B System.Void UnityTemplateProjects.SimpleCameraController::.ctor()
extern void SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E (void);
// 0x0000057C System.Void UnityTemplateProjects.SimpleCameraController/CameraState::SetFromTransform(UnityEngine.Transform)
extern void CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E (void);
// 0x0000057D System.Void UnityTemplateProjects.SimpleCameraController/CameraState::Translate(UnityEngine.Vector3)
extern void CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247 (void);
// 0x0000057E System.Void UnityTemplateProjects.SimpleCameraController/CameraState::LerpTowards(UnityTemplateProjects.SimpleCameraController/CameraState,System.Single,System.Single)
extern void CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282 (void);
// 0x0000057F System.Void UnityTemplateProjects.SimpleCameraController/CameraState::UpdateTransform(UnityEngine.Transform)
extern void CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623 (void);
// 0x00000580 System.Void UnityTemplateProjects.SimpleCameraController/CameraState::.ctor()
extern void CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322 (void);
// 0x00000581 System.String Moonshine.Moondream.MoondreamGlobal::get_ContentDefaultName()
extern void MoondreamGlobal_get_ContentDefaultName_m2940A865FC9DA770A6E189520E112898C0B54F9F (void);
// 0x00000582 System.String Moonshine.Moondream.MoondreamGlobal::get_ContentDefaultPath()
extern void MoondreamGlobal_get_ContentDefaultPath_m28B2BA85F67E924DF9EE7EE17F7D6D0D75C69020 (void);
// 0x00000583 System.Void Moonshine.Moondream.MoondreamGlobal::.ctor()
extern void MoondreamGlobal__ctor_m5753C1A2502D3A8A500ADA94C5ADE097429C88BB (void);
// 0x00000584 System.Void Moonshine.Moondream.SpatialAnchors.Anchor::.ctor()
extern void Anchor__ctor_mEDBD78046974D904938525CBCA94A79E40CAC908 (void);
// 0x00000585 System.Collections.IEnumerator Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::Start()
extern void AnchorFinderManager_Start_m8F731531D23301AFAB5BEBF942D2B341F6F41D48 (void);
// 0x00000586 System.Boolean Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::IsSlamActive()
extern void AnchorFinderManager_IsSlamActive_mAD7DF966CDF3EE20BE24A6BC910E8B8A7E403188 (void);
// 0x00000587 System.String Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::GetSlamState()
extern void AnchorFinderManager_GetSlamState_m45AFEFED9E5D3C4F0F2D3F68B5A7A4CCB78A3644 (void);
// 0x00000588 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::Update()
extern void AnchorFinderManager_Update_m3B309B456200B6CAD9EB3E6D61F0A8D2EE73A778 (void);
// 0x00000589 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::SetAnchorsLocalPosition()
extern void AnchorFinderManager_SetAnchorsLocalPosition_mB53B16D7604AAB95D590DA6A57D71C32139018B8 (void);
// 0x0000058A System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::SetAnchorKeys(System.Int32)
extern void AnchorFinderManager_SetAnchorKeys_m362C176A35A39B9FFA8C34D8F0563A66E0737138 (void);
// 0x0000058B System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::SetConfig()
extern void AnchorFinderManager_SetConfig_m17A3D629412AC7431560BC1628B7A6835BD7EE70 (void);
// 0x0000058C System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::SetMainAnchorLoacte()
extern void AnchorFinderManager_SetMainAnchorLoacte_m210EA3F9662FCCC5766BC279A9A8D009F64AE9AA (void);
// 0x0000058D UnityEngine.Vector3 Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::PosDest(UnityEngine.GameObject,UnityEngine.Vector3)
extern void AnchorFinderManager_PosDest_m3F194A7FB9A27A1349DD7681649C22A7DFE1C25F (void);
// 0x0000058E UnityEngine.Quaternion Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::RotDest(UnityEngine.GameObject,UnityEngine.Quaternion)
extern void AnchorFinderManager_RotDest_m8B7C74E8455665394ED8284D224CD1CB770746C4 (void);
// 0x0000058F UnityEngine.Vector3 Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::Average(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void AnchorFinderManager_Average_m5452E04BA4877DE53718FBA920F72E6A4485CAA9 (void);
// 0x00000590 UnityEngine.Quaternion Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::Average(System.Collections.Generic.List`1<UnityEngine.Quaternion>)
extern void AnchorFinderManager_Average_mB7789F4122E0BF21C3CCE5D3DAE2926DEBB67BD8 (void);
// 0x00000591 System.Int32 Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::IndexCheck(System.String)
extern void AnchorFinderManager_IndexCheck_mB191D430575C2CAEB11F22BCFB023ED2178464CB (void);
// 0x00000592 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::CloudManager_AnchorLocated(System.Object,Microsoft.Azure.SpatialAnchors.AnchorLocatedEventArgs)
extern void AnchorFinderManager_CloudManager_AnchorLocated_m458F48B2CD499FF0322EE872FF6A2E33F92E31A7 (void);
// 0x00000593 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::AnchorDicAdd(System.String,System.Int32,UnityEngine.GameObject)
extern void AnchorFinderManager_AnchorDicAdd_m8CB5852A76527B4087BB3667DDCC844DD8FC78F5 (void);
// 0x00000594 System.Collections.IEnumerator Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::IEResetCountDown()
extern void AnchorFinderManager_IEResetCountDown_mF16FD936B5F802C679FAEA5D8465F367B853786D (void);
// 0x00000595 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::OnWorldAnchorTrackingChanged(UnityEngine.XR.WSA.WorldAnchor,System.Boolean)
extern void AnchorFinderManager_OnWorldAnchorTrackingChanged_m2598E5C63C9CC15835B05B6E6565C6B239F66696 (void);
// 0x00000596 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::OnWorldAnchorPositionalLocatorStateChanged(UnityEngine.XR.WSA.PositionalLocatorState,UnityEngine.XR.WSA.PositionalLocatorState)
extern void AnchorFinderManager_OnWorldAnchorPositionalLocatorStateChanged_mF24D09840D99232F348AD086536D06538949898B (void);
// 0x00000597 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::StartSession(System.Int32,System.Boolean)
extern void AnchorFinderManager_StartSession_m11CACBD5FE67BE9D656F8CAFB2D8DFFCCB17B321 (void);
// 0x00000598 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::StopSession()
extern void AnchorFinderManager_StopSession_m851520B6A1C4402692609AD6A438A65917C2AA59 (void);
// 0x00000599 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::SetText(System.String)
extern void AnchorFinderManager_SetText_mE05B15387D3B26ABCC7A4CD8713CABA0E005117F (void);
// 0x0000059A System.Int32 Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::AddKeyStorage(Moonshine.Moondream.SpatialAnchors.AnchorKeysStorage)
extern void AnchorFinderManager_AddKeyStorage_m473D850E93646CA53356B391216F048A2473C18F (void);
// 0x0000059B System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::ClearAnchors()
extern void AnchorFinderManager_ClearAnchors_mB5C71D00B8706289CA97C768CF78AE5808867A56 (void);
// 0x0000059C System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::ResetAnchorKeyAndRebootSession(System.Int32)
extern void AnchorFinderManager_ResetAnchorKeyAndRebootSession_m77205760B1DDBB7EF4DE578C2123F502526AEE03 (void);
// 0x0000059D System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::OnAnchorError(System.Object,Microsoft.Azure.SpatialAnchors.SessionErrorEventArgs)
extern void AnchorFinderManager_OnAnchorError_mECD32768CB3D6DBFF41863A2C3BE58E29700DEB9 (void);
// 0x0000059E System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager::.ctor()
extern void AnchorFinderManager__ctor_m993C8D123894CD50ADCDD80AE24885C1399C4CEE (void);
// 0x0000059F System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/AnchorLocatInfo::.ctor()
extern void AnchorLocatInfo__ctor_m2A7B6A75D5CA881F344CCEA9634994F54E7298A7 (void);
// 0x000005A0 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/TextEvent::.ctor()
extern void TextEvent__ctor_mBF1408D3E57E1D67441750BB4E06923224E8E2A0 (void);
// 0x000005A1 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::.ctor(System.Int32)
extern void U3CStartU3Ed__22__ctor_m3AE7F0E26310E1ABE65F85760E823DA506DE626C (void);
// 0x000005A2 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::System.IDisposable.Dispose()
extern void U3CStartU3Ed__22_System_IDisposable_Dispose_m2F58623F23AA9C86966B1F64FE3502331BABF5CE (void);
// 0x000005A3 System.Boolean Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::MoveNext()
extern void U3CStartU3Ed__22_MoveNext_m56833A3F89A0F071CF678D9CCAD69F1D66AFCDF0 (void);
// 0x000005A4 System.Object Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DE77A7BAF2F35FFEE0EA6C5DB36D4A4914CC21 (void);
// 0x000005A5 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m27E280184A27CC5326AD33B96BB846EA4507E7FC (void);
// 0x000005A6 System.Object Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<Start>d__22::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_m8D61C11DF8CDB41C7343B5DF8B0FF3BF10EAE2D5 (void);
// 0x000005A7 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m16200422F20C49FAE9DB10CE789053116E20CA7F (void);
// 0x000005A8 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass32_0::<Average>b__0(UnityEngine.Vector3)
extern void U3CU3Ec__DisplayClass32_0_U3CAverageU3Eb__0_mE897262A8555E3185AD5B3D0AAED1E05DD6E324E (void);
// 0x000005A9 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_mF8F8F8E5EE89481645AAEDD182B6AFBC2B93B68F (void);
// 0x000005AA System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass33_0::<Average>b__0(UnityEngine.Quaternion)
extern void U3CU3Ec__DisplayClass33_0_U3CAverageU3Eb__0_m0BA3723C10BBF1E356687DCCA3389FBECB18D576 (void);
// 0x000005AB System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass35_0::.ctor()
extern void U3CU3Ec__DisplayClass35_0__ctor_m3A1C2512C541CB79243EDA8E04FE5D04B2599DCD (void);
// 0x000005AC System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<>c__DisplayClass35_0::<CloudManager_AnchorLocated>b__0()
extern void U3CU3Ec__DisplayClass35_0_U3CCloudManager_AnchorLocatedU3Eb__0_mCCA73668A15007B5FB0D7B41800964E4F65151B7 (void);
// 0x000005AD System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::.ctor(System.Int32)
extern void U3CIEResetCountDownU3Ed__39__ctor_m4E8C2F91FB0E57D54693303E0CDC77AB66001EA4 (void);
// 0x000005AE System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::System.IDisposable.Dispose()
extern void U3CIEResetCountDownU3Ed__39_System_IDisposable_Dispose_mBE802B6017D0F5B99C2F7040DDDFFC114222A6AD (void);
// 0x000005AF System.Boolean Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::MoveNext()
extern void U3CIEResetCountDownU3Ed__39_MoveNext_mD0BE91B96EB6B2BF399D549659152912C74310CE (void);
// 0x000005B0 System.Object Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEResetCountDownU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE405334C887C6B6CA00A42F06C6D629D12EFED96 (void);
// 0x000005B1 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::System.Collections.IEnumerator.Reset()
extern void U3CIEResetCountDownU3Ed__39_System_Collections_IEnumerator_Reset_m4806F0F21202BC9A7BCCF5381177B201C33CE6D4 (void);
// 0x000005B2 System.Object Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<IEResetCountDown>d__39::System.Collections.IEnumerator.get_Current()
extern void U3CIEResetCountDownU3Ed__39_System_Collections_IEnumerator_get_Current_m4AFA30E73D2DDFDF3BC367F030A49D62BE2E335E (void);
// 0x000005B3 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<StartSession>d__42::MoveNext()
extern void U3CStartSessionU3Ed__42_MoveNext_m329796E6A3238FB5B238DEA7FAF14414FE812257 (void);
// 0x000005B4 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<StartSession>d__42::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartSessionU3Ed__42_SetStateMachine_mDC00E8F92E6128821F8B1C0B6FBEE6C8B4B88CAC (void);
// 0x000005B5 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<StopSession>d__43::MoveNext()
extern void U3CStopSessionU3Ed__43_MoveNext_mCD54D758D216F67B1069575F29A48D3F96C0AA43 (void);
// 0x000005B6 System.Void Moonshine.Moondream.SpatialAnchors.AnchorFinderManager/<StopSession>d__43::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStopSessionU3Ed__43_SetStateMachine_m88F137D413FEB1B7F9ED0B53F41DFCAD92232FB3 (void);
// 0x000005B7 System.Void Moonshine.Moondream.SpatialAnchors.AnchorKeysStorage::.ctor()
extern void AnchorKeysStorage__ctor_m8E6AC3AA3993257E48B4855D1238CDB0FA52C510 (void);
// 0x000005B8 System.Void Moonshine.Moondream.SpatialAnchors.HideConsole::Start()
extern void HideConsole_Start_m33BC63F5E9929CB5F5781BCD316CFC1D515BE6BF (void);
// 0x000005B9 System.Void Moonshine.Moondream.SpatialAnchors.HideConsole::.ctor()
extern void HideConsole__ctor_mF53E92BC63661611B114E192F0EF7A66D331BC51 (void);
// 0x000005BA System.String Moonshine.Moondream.SpatialAnchors.JsonConvert::ToJson(T)
// 0x000005BB T Moonshine.Moondream.SpatialAnchors.JsonConvert::ToObj(System.String)
// 0x000005BC System.String Moonshine.Moondream.SpatialAnchors.JsonConvert::ToArrayJson(T[])
// 0x000005BD T[] Moonshine.Moondream.SpatialAnchors.JsonConvert::ToArrayObj(System.String)
// 0x000005BE System.Void Moonshine.Moondream.SpatialAnchors.JsonConvert/Wrapper`1::.ctor()
// 0x000005BF System.Boolean Moonshine.Moondream.SpatialAnchors.LocationTransfer::get_IsLocat()
extern void LocationTransfer_get_IsLocat_mC0F66F4188480310D3FDA8679EAA1DFD9B7DFE44 (void);
// 0x000005C0 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer::Start()
extern void LocationTransfer_Start_mE89044231A13DB33B812F641A866682EC3991A83 (void);
// 0x000005C1 System.Collections.IEnumerator Moonshine.Moondream.SpatialAnchors.LocationTransfer::IESend()
extern void LocationTransfer_IESend_mC6E0A3FD1DB20C3D941C6E3D5ADCC072536AB8A2 (void);
// 0x000005C2 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer::.ctor()
extern void LocationTransfer__ctor_m951F2850C584150B05521445DFE2A0B039B01AFB (void);
// 0x000005C3 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer::<Start>b__6_0()
extern void LocationTransfer_U3CStartU3Eb__6_0_mA1BBC945E3A303B41B0E006BB6247F032F757E85 (void);
// 0x000005C4 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer::<Start>b__6_1()
extern void LocationTransfer_U3CStartU3Eb__6_1_mE8B4C1348EB7CE1571081FBA8B3E0CB93C1C5EDB (void);
// 0x000005C5 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer::<Start>b__6_2()
extern void LocationTransfer_U3CStartU3Eb__6_2_m7B24A9A804574139544E545F63E471C74E6CC27B (void);
// 0x000005C6 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::.ctor(System.Int32)
extern void U3CIESendU3Ed__7__ctor_m2B1D9B9B0F210343256EF72153D0A3EC44D0975C (void);
// 0x000005C7 System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::System.IDisposable.Dispose()
extern void U3CIESendU3Ed__7_System_IDisposable_Dispose_m1233795C699E6E5391CC5FF9D9165C52646FC7E0 (void);
// 0x000005C8 System.Boolean Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::MoveNext()
extern void U3CIESendU3Ed__7_MoveNext_mCC7C43C525A4C0CA35580F5469170B44F2507534 (void);
// 0x000005C9 System.Object Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIESendU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B259E96E9F2B54BD386C873E9CB1456E8EE1279 (void);
// 0x000005CA System.Void Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::System.Collections.IEnumerator.Reset()
extern void U3CIESendU3Ed__7_System_Collections_IEnumerator_Reset_m6DF6C0A186AA997896FC0AEF424A1C7A1A0F2028 (void);
// 0x000005CB System.Object Moonshine.Moondream.SpatialAnchors.LocationTransfer/<IESend>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CIESendU3Ed__7_System_Collections_IEnumerator_get_Current_m093B757F42BD4F52C4F3C0E7F97A13D63E616E7B (void);
// 0x000005CC System.Void Moonshine.Moondream.SpatialAnchors.ShowPoseText::Start()
extern void ShowPoseText_Start_m8114BE306F69870C4F7BFE26631374E3B128112B (void);
// 0x000005CD System.Void Moonshine.Moondream.SpatialAnchors.ShowPoseText::Update()
extern void ShowPoseText_Update_m2DC1AC93D81F8663F1EE4E627A26152EF0DFB43B (void);
// 0x000005CE System.Void Moonshine.Moondream.SpatialAnchors.ShowPoseText::.ctor()
extern void ShowPoseText__ctor_m5FACCE7BED4174973FB0E70CAC76F2066FC2595F (void);
// 0x000005CF System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::Awake()
extern void PointEventManagerNetTcpReceiver_Awake_m0C71BA7443FAA801016B078CA8D5F39E8A0B1213 (void);
// 0x000005D0 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::SendManagerInfo()
extern void PointEventManagerNetTcpReceiver_SendManagerInfo_m1B76C2B7CF63495BFEFC199DED915C22AD62D0A4 (void);
// 0x000005D1 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::EventInvoke(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void PointEventManagerNetTcpReceiver_EventInvoke_mA0DC5281A9EBE48E29D5E0598D61F632C4221C32 (void);
// 0x000005D2 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::OnPointEvent(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void PointEventManagerNetTcpReceiver_OnPointEvent_mD5922E4017D46BA88EA0C8A85C779287BFB5C3A9 (void);
// 0x000005D3 System.Collections.IEnumerator Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::CheckRemove()
extern void PointEventManagerNetTcpReceiver_CheckRemove_mFDB40509B2FD477E19383FFCB19B7BD4FCAE4324 (void);
// 0x000005D4 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::.ctor()
extern void PointEventManagerNetTcpReceiver__ctor_m064CE06198B483C78DDD9E7C29BE3D13CD9DB776 (void);
// 0x000005D5 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::<Awake>b__15_0(System.Net.Sockets.TcpClient)
extern void PointEventManagerNetTcpReceiver_U3CAwakeU3Eb__15_0_m39AEA1674AF3107A0C40E296E6FE3006EFFF3862 (void);
// 0x000005D6 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver::<CheckRemove>b__19_0(System.Int32)
extern void PointEventManagerNetTcpReceiver_U3CCheckRemoveU3Eb__19_0_m4772E4408849296B81488FC1F781586C613CF956 (void);
// 0x000005D7 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/MrPointOption::.ctor()
extern void MrPointOption__ctor_mA8E5AB01C01482AB20551D06F6571DB6A72BDC48 (void);
// 0x000005D8 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventToEventType::Invoke(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void RadarPointEventToEventType_Invoke_mB645B98854574DF724D06CF4E145A320A9A00A91 (void);
// 0x000005D9 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventToEventType::.ctor()
extern void RadarPointEventToEventType__ctor_mB27BDE5D7016852C37368B5A8C4A87C9092B76C0 (void);
// 0x000005DA System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventToProjection::Invoke(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void RadarPointEventToProjection_Invoke_m2672339344D9AE6E3531C7F2F73385727CC26179 (void);
// 0x000005DB System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventToProjection::.ctor()
extern void RadarPointEventToProjection__ctor_mF097C78983C314E0069B1AF07211F4CCC402691E (void);
// 0x000005DC System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventAllApart::Invoke(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void RadarPointEventAllApart_Invoke_mF73D5474D59178DEC433F6AB859C8294F7775790 (void);
// 0x000005DD System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/RadarPointEventAllApart::.ctor()
extern void RadarPointEventAllApart__ctor_mADB89D5C7D977A9BDF380F0C2B98476E5BB05DE2 (void);
// 0x000005DE System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::.ctor(System.Int32)
extern void U3CCheckRemoveU3Ed__19__ctor_m650D80292D981B04C59F59A7651DAE4E00C67F20 (void);
// 0x000005DF System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::System.IDisposable.Dispose()
extern void U3CCheckRemoveU3Ed__19_System_IDisposable_Dispose_m6B2120F06500CC5E9F76E530513678A0C2E78C45 (void);
// 0x000005E0 System.Boolean Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::MoveNext()
extern void U3CCheckRemoveU3Ed__19_MoveNext_m83B904F1A0C82C3A7B3808D048A3AE1C12C6DEA4 (void);
// 0x000005E1 System.Object Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckRemoveU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C7B72BF923176448792D2F0D8FAA7E48783B648 (void);
// 0x000005E2 System.Void Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::System.Collections.IEnumerator.Reset()
extern void U3CCheckRemoveU3Ed__19_System_Collections_IEnumerator_Reset_m17A58011263019A2F9AE87E01A0570B0C469861F (void);
// 0x000005E3 System.Object Moonshine.Moondream.Pointevent.PointEventManagerNetTcpReceiver/<CheckRemove>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CCheckRemoveU3Ed__19_System_Collections_IEnumerator_get_Current_mB6F8A4AC9DAFA3078DE4CDFFD837243B7BF91DAD (void);
// 0x000005E4 System.Void Moonshine.Moondream.Pointevent.PointEventReceiverSample::OnReceivePoint(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void PointEventReceiverSample_OnReceivePoint_mA790C6887EC7E585777EC91DE14A059183875CB0 (void);
// 0x000005E5 System.Void Moonshine.Moondream.Pointevent.PointEventReceiverSample::.ctor()
extern void PointEventReceiverSample__ctor_m982181823EBA4062106034B5CBDB57B068C689A6 (void);
// 0x000005E6 System.Single Moonshine.Moondream.Net.NetTcpCommandLoopSender::get_WaitTime()
extern void NetTcpCommandLoopSender_get_WaitTime_mEDF32F282B945D7B0BC4FAE8D26FAC2C6FC35732 (void);
// 0x000005E7 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandLoopSender::get_CommandKey()
extern void NetTcpCommandLoopSender_get_CommandKey_mB66621B49376BC51D63AA7875DF0ED6AE0D54C1F (void);
// 0x000005E8 System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender::Start()
extern void NetTcpCommandLoopSender_Start_m97FD49F84A7BAAAC69411774EFDC70FE5D9B26B9 (void);
// 0x000005E9 System.Collections.IEnumerator Moonshine.Moondream.Net.NetTcpCommandLoopSender::IEAutoSend()
extern void NetTcpCommandLoopSender_IEAutoSend_m2033FDE385F0034049C1A3A4D73A71768F2431A9 (void);
// 0x000005EA Moonshine.Moondream.Net.Command Moonshine.Moondream.Net.NetTcpCommandLoopSender::GetCmd()
extern void NetTcpCommandLoopSender_GetCmd_m9D7950C5E77539ED4EF2E31DE16EC47D0456D977 (void);
// 0x000005EB System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender::Send()
extern void NetTcpCommandLoopSender_Send_m41CF44FDBC157A902C27F603439C98A7AB927BD3 (void);
// 0x000005EC System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender::.ctor()
extern void NetTcpCommandLoopSender__ctor_m80A421AB84842DBEE5C4DB8EF80CFE09B08953A6 (void);
// 0x000005ED System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::.ctor(System.Int32)
extern void U3CIEAutoSendU3Ed__6__ctor_m0069DB3ACF16DCB499EB9888D06844040DF7219B (void);
// 0x000005EE System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::System.IDisposable.Dispose()
extern void U3CIEAutoSendU3Ed__6_System_IDisposable_Dispose_mFA94AAB72D4814AD87EFC49B5E8FFC3D4DCAB3E8 (void);
// 0x000005EF System.Boolean Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::MoveNext()
extern void U3CIEAutoSendU3Ed__6_MoveNext_m348C0C8E5710FD2F9E50A9002D9721986CC345B2 (void);
// 0x000005F0 System.Object Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEAutoSendU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D579B832156C15D9E02B2B42D8BA7A347689DA7 (void);
// 0x000005F1 System.Void Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::System.Collections.IEnumerator.Reset()
extern void U3CIEAutoSendU3Ed__6_System_Collections_IEnumerator_Reset_mDD93896E985965B1E9598A0CCF95FCE69ED47B86 (void);
// 0x000005F2 System.Object Moonshine.Moondream.Net.NetTcpCommandLoopSender/<IEAutoSend>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CIEAutoSendU3Ed__6_System_Collections_IEnumerator_get_Current_mE91AB47AC2218A01D81999D185B170BCEB09ACCF (void);
// 0x000005F3 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandReceiver::get_CommandKey()
extern void NetTcpCommandReceiver_get_CommandKey_m10078225A29BC944370B4E509F94E8E4EF37386C (void);
// 0x000005F4 System.Void Moonshine.Moondream.Net.NetTcpCommandReceiver::OnStart()
extern void NetTcpCommandReceiver_OnStart_mB50F5BC285166596E7C6BE622E77D450A37DCA05 (void);
// 0x000005F5 System.Void Moonshine.Moondream.Net.NetTcpCommandReceiver::OnReceive(System.String)
extern void NetTcpCommandReceiver_OnReceive_m26A04A75D9F2A4B46883003379F395AD67DC5652 (void);
// 0x000005F6 System.Void Moonshine.Moondream.Net.NetTcpCommandReceiver::Start()
extern void NetTcpCommandReceiver_Start_m5504921AE35DD66EFFB306CE89E1046C1A019822 (void);
// 0x000005F7 System.Void Moonshine.Moondream.Net.NetTcpCommandReceiver::OnDestroy()
extern void NetTcpCommandReceiver_OnDestroy_mA92EC2CC4841DFC97C55CDE0D05A113AD354C11E (void);
// 0x000005F8 System.Void Moonshine.Moondream.Net.NetTcpCommandReceiver::.ctor()
extern void NetTcpCommandReceiver__ctor_m87A6D8ACD93482C755B54F5D9F326D78AFB3615A (void);
// 0x000005F9 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandStartSender::get_CommandKey()
extern void NetTcpCommandStartSender_get_CommandKey_m7D8A78CCE981C2968435B97713784ECFEE40204A (void);
// 0x000005FA System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::Send()
extern void NetTcpCommandStartSender_Send_m836435175A3F58CD5FB8EE883880C3B3D9B1A9F1 (void);
// 0x000005FB System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::Start()
extern void NetTcpCommandStartSender_Start_m0BF337B212483553627CF52F5D075CEA616BBCFE (void);
// 0x000005FC System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::OnDestroy()
extern void NetTcpCommandStartSender_OnDestroy_m5A8B5087A03312483E46D9B745C4F222A92419E4 (void);
// 0x000005FD System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::.ctor()
extern void NetTcpCommandStartSender__ctor_m0CEB80360FECF460FAA211633A76ECBB0D9B3251 (void);
// 0x000005FE System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::<Start>b__4_0(System.Net.Sockets.TcpClient)
extern void NetTcpCommandStartSender_U3CStartU3Eb__4_0_m232C7B1A32E284D57EB77F4493BCCBB32037B4FC (void);
// 0x000005FF System.Void Moonshine.Moondream.Net.NetTcpCommandStartSender::<OnDestroy>b__5_0(System.Net.Sockets.TcpClient)
extern void NetTcpCommandStartSender_U3COnDestroyU3Eb__5_0_m9F2E2EFA85C61BBF0045218728ED2340E2C301AE (void);
// 0x00000600 System.String Moonshine.Moondream.Net.Command::get_EndChar()
extern void Command_get_EndChar_m9A09580D18E7CE7317E80760095D25B276746E6A (void);
// 0x00000601 System.String Moonshine.Moondream.Net.Command::ToMsg()
extern void Command_ToMsg_mC96618D476B4F93DF8837490AC5AFF7242657038 (void);
// 0x00000602 T Moonshine.Moondream.Net.Command::ToObj(System.String)
// 0x00000603 System.Void Moonshine.Moondream.Net.Command::.ctor()
extern void Command__ctor_m77EBF5C65FCA259C4346C6388C7934EC7A9945BF (void);
// 0x00000604 System.Void Moonshine.Moondream.Net.CommandMachineType::.ctor()
extern void CommandMachineType__ctor_m63B254FEAB69055EE65B813227FE2C229D8A5A80 (void);
// 0x00000605 System.Void Moonshine.Moondream.Net.CommandTest::.ctor()
extern void CommandTest__ctor_m02FFA4146EB57D497D98AB810D54430E8EE5FD4E (void);
// 0x00000606 System.Void Moonshine.Moondream.Net.NetTcpCommandContentName::.ctor()
extern void NetTcpCommandContentName__ctor_m0284AB94416E8E45505EB9B88864AAD873C3F3DD (void);
// 0x00000607 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandContentNameAsker::get_CommandKey()
extern void NetTcpCommandContentNameAsker_get_CommandKey_m9A7A5E14BBB54AD81AAF5FB618C82CD61BE5CF43 (void);
// 0x00000608 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameAsker::.ctor()
extern void NetTcpCommandContentNameAsker__ctor_mA92279D0054239CCEF6E218CEE9AB6C2773B7178 (void);
// 0x00000609 System.String Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::get_ContentName()
extern void NetTcpCommandContentNameMobiReceiver_get_ContentName_m9F59AD5E9FA554ACA0002460843F8C5E1A6468CB (void);
// 0x0000060A Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::get_CommandKey()
extern void NetTcpCommandContentNameMobiReceiver_get_CommandKey_m04B3974BAABFBD626FFD4FA8925732ED9388011B (void);
// 0x0000060B System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::OnReceive(System.String)
extern void NetTcpCommandContentNameMobiReceiver_OnReceive_m6AA989C775F0D01246C0AA4FA755FF4AD57CF602 (void);
// 0x0000060C System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::LaunchMoondream()
extern void NetTcpCommandContentNameMobiReceiver_LaunchMoondream_mB239F5C9F09189204B07B83168AA84C204D70E8F (void);
// 0x0000060D System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::Launch(System.String)
extern void NetTcpCommandContentNameMobiReceiver_Launch_m6CD6D1F025AD365FBC92E0292E442EA83F60A85E (void);
// 0x0000060E System.Collections.IEnumerator Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::Quit()
extern void NetTcpCommandContentNameMobiReceiver_Quit_m107525E8AE96BC382D7597247CFE835E52616277 (void);
// 0x0000060F System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver::.ctor()
extern void NetTcpCommandContentNameMobiReceiver__ctor_m33FF0FE8094C10FC8B14F52074F3051FE72FF8AF (void);
// 0x00000610 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_m977754DDE02CA9CD4A1570726CFBBA5255778CA4 (void);
// 0x00000611 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<>c__DisplayClass7_0::<Launch>b__0()
extern void U3CU3Ec__DisplayClass7_0_U3CLaunchU3Eb__0_m70A036C4601D79CDE57FCDD3E0E0D0BC7F88D103 (void);
// 0x00000612 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<>c__DisplayClass7_0/<<Launch>b__0>d::MoveNext()
extern void U3CU3CLaunchU3Eb__0U3Ed_MoveNext_m1F3D2B6EE42AA4393605AB5ECD32A58CAB1517DC (void);
// 0x00000613 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<>c__DisplayClass7_0/<<Launch>b__0>d::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CU3CLaunchU3Eb__0U3Ed_SetStateMachine_m257EBACFAB8335CC954A8F191BA4BCBDCF3FB134 (void);
// 0x00000614 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::.ctor(System.Int32)
extern void U3CQuitU3Ed__8__ctor_m83871648FC6BB0B5EB4AEA754CA6FD28795DDE1F (void);
// 0x00000615 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::System.IDisposable.Dispose()
extern void U3CQuitU3Ed__8_System_IDisposable_Dispose_m1A900FB77CB57314AA40A86BC3618A395139DC2B (void);
// 0x00000616 System.Boolean Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::MoveNext()
extern void U3CQuitU3Ed__8_MoveNext_m8EB528A1B0C1086C9674ABDA55479ED46820B297 (void);
// 0x00000617 System.Object Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CQuitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB21B4981BE91F804C92C5280A7ABE6BAED63C482 (void);
// 0x00000618 System.Void Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::System.Collections.IEnumerator.Reset()
extern void U3CQuitU3Ed__8_System_Collections_IEnumerator_Reset_m67AD25F6414A987F61D58A35F22A0141F15A16F8 (void);
// 0x00000619 System.Object Moonshine.Moondream.Net.NetTcpCommandContentNameMobiReceiver/<Quit>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CQuitU3Ed__8_System_Collections_IEnumerator_get_Current_m4E509938DCED0A2CD04F4C470D8B8BF64C95D237 (void);
// 0x0000061A Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandContentStateMinorSender::get_m_CommandKey()
extern void NetTcpCommandContentStateMinorSender_get_m_CommandKey_m97E4E84EF907A0A30D8A6E1F23C9862FA25E419D (void);
// 0x0000061B System.Void Moonshine.Moondream.Net.NetTcpCommandContentStateMinorSender::Send()
extern void NetTcpCommandContentStateMinorSender_Send_m745A0A0ECDE78A199692E53125F9F5D9C3B03506 (void);
// 0x0000061C System.Void Moonshine.Moondream.Net.NetTcpCommandContentStateMinorSender::.ctor()
extern void NetTcpCommandContentStateMinorSender__ctor_m3152C06FB109329C296A976B7E70921B0C4FD596 (void);
// 0x0000061D Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandContentStatePriorAsker::get_CommandKey()
extern void NetTcpCommandContentStatePriorAsker_get_CommandKey_mAB5AF8D5C5BD5DA2347F255D58CE65C11D0514B9 (void);
// 0x0000061E System.Void Moonshine.Moondream.Net.NetTcpCommandContentStatePriorAsker::.ctor()
extern void NetTcpCommandContentStatePriorAsker__ctor_m115BD48B4EAA6139F31E06FFD1C4B5B8BF269AB6 (void);
// 0x0000061F Moonshine.Moondream.MoondreamGlobal/CONTENT_GAME_STATE Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver::get_GameState()
extern void NetTcpCommandContentStatePriorReceiver_get_GameState_mD4CCA951132A8233FB6D9E691C6138D9DB807E99 (void);
// 0x00000620 System.Void Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver::set_GameState(Moonshine.Moondream.MoondreamGlobal/CONTENT_GAME_STATE)
extern void NetTcpCommandContentStatePriorReceiver_set_GameState_m56AD84B94D6530BC30BD30DE4817616A2A4BE74C (void);
// 0x00000621 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver::get_CommandKey()
extern void NetTcpCommandContentStatePriorReceiver_get_CommandKey_m8619B2444FC612906E679015189C0BD87278496E (void);
// 0x00000622 System.Void Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver::OnReceive(System.String)
extern void NetTcpCommandContentStatePriorReceiver_OnReceive_m53AEEEDF7BD2FB89093CA4A2347223A08BC8D7E7 (void);
// 0x00000623 System.Void Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver::.ctor()
extern void NetTcpCommandContentStatePriorReceiver__ctor_m5626A6FE1F04CC61F77D26F9A5ABA37A64121448 (void);
// 0x00000624 System.Void Moonshine.Moondream.Net.NetTcpCommandContentStatePriorReceiver/ContentStateEvent::.ctor()
extern void ContentStateEvent__ctor_m63FF1F75C9A94FF7D5F1C46899FE7DEEA51C7709 (void);
// 0x00000625 System.Void Moonshine.Moondream.Net.NetTcpCommandMobi::.ctor()
extern void NetTcpCommandMobi__ctor_m245DF3C983967472263D5EE6F42C38858C17BBDD (void);
// 0x00000626 System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/Locat::.ctor()
extern void Locat__ctor_m89645C09C50AD912C1DEA2022475A6040BF4F228 (void);
// 0x00000627 System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInt::.ctor()
extern void LocatInt__ctor_m2644F877404B89E29DEA143449CDC3874A3F943F (void);
// 0x00000628 System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInt::.ctor(Moonshine.Moondream.Net.NetTcpCommandMobi/Locat)
extern void LocatInt__ctor_mFAB019D56DE214E4BC415FD81F1DF0CAE780F553 (void);
// 0x00000629 System.Int32 Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInt::get_FloatMuti()
extern void LocatInt_get_FloatMuti_mDF01F5884727F4C627C55B838E9BB270D522B181 (void);
// 0x0000062A System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInt::Set(Moonshine.Moondream.Net.NetTcpCommandMobi/Locat)
extern void LocatInt_Set_mD004B3503BA3A85DC70E15FE1ECD43E3B079DE32 (void);
// 0x0000062B Moonshine.Moondream.Net.NetTcpCommandMobi/Locat Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInt::Get()
extern void LocatInt_Get_m2C2F6A7C7BD04ED97E0569C998FE69427A5064CB (void);
// 0x0000062C System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/LocatData::.ctor()
extern void LocatData__ctor_mFDDFC6C741439B91D83C913E823BA02D4CAAF26C (void);
// 0x0000062D System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/LocatInfo::.ctor()
extern void LocatInfo__ctor_m9434A81E8531F43A1B84C9B6299822D78D469CD3 (void);
// 0x0000062E System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/CommandLocatInfo::.ctor()
extern void CommandLocatInfo__ctor_mA9C2AFD74B9D17E9F935F508A012662C5575E43B (void);
// 0x0000062F System.Void Moonshine.Moondream.Net.NetTcpCommandMobi/CommandLocatData::.ctor()
extern void CommandLocatData__ctor_m25BE57F4A73602D89C0983C3CE3C96992D09F1D3 (void);
// 0x00000630 System.Single Moonshine.Moondream.Net.NetTcpCommandMobiLocatDataSender::get_WaitTime()
extern void NetTcpCommandMobiLocatDataSender_get_WaitTime_mF9206BFB02179FDF329817DDEC532FF728EC0732 (void);
// 0x00000631 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandMobiLocatDataSender::get_CommandKey()
extern void NetTcpCommandMobiLocatDataSender_get_CommandKey_m670A48F6422CC99ADF67C7FCCA766555F1E1D72F (void);
// 0x00000632 Moonshine.Moondream.Net.Command Moonshine.Moondream.Net.NetTcpCommandMobiLocatDataSender::GetCmd()
extern void NetTcpCommandMobiLocatDataSender_GetCmd_mC14E9321E48EB81AC5F0E66744A33A1167C6FFD3 (void);
// 0x00000633 System.Void Moonshine.Moondream.Net.NetTcpCommandMobiLocatDataSender::.ctor()
extern void NetTcpCommandMobiLocatDataSender__ctor_m225F6C1574A2E46B10DF0F150E7005A9CF9453FF (void);
// 0x00000634 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar::.ctor()
extern void NetTcpCommandRadar__ctor_m1E8318A3ED7136AF12DD81026B2AD3B5F2CD64A6 (void);
// 0x00000635 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/MrPointEvent::.ctor()
extern void MrPointEvent__ctor_m17A9534F42E7846AFD0863D02BE95817F0C98284 (void);
// 0x00000636 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint::.ctor()
extern void MrPoint__ctor_m8E801A5F0F9F71C4E0F9EDEA19D1DFC9588A975B (void);
// 0x00000637 Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint Moonshine.Moondream.Net.NetTcpCommandRadar/MrPointInt::Get()
extern void MrPointInt_Get_m5F94AFEC39983D71ECCD73ECB1F80AC01DB79BE7 (void);
// 0x00000638 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/MrPointInt::Set(Moonshine.Moondream.Net.NetTcpCommandRadar/MrPoint)
extern void MrPointInt_Set_m6EF7162785708C3D6185E2635DCCD08ABF453E34 (void);
// 0x00000639 System.Int32 Moonshine.Moondream.Net.NetTcpCommandRadar/MrPointInt::get_FloatMuti()
extern void MrPointInt_get_FloatMuti_m1B7D8CEABF6C332E87758A8736D6CAE876A256DC (void);
// 0x0000063A System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/MrPointInt::.ctor()
extern void MrPointInt__ctor_m727654EE8A68F7E3CA87196C3A1E8F16C1F5C337 (void);
// 0x0000063B System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfo::.ctor()
extern void ManagerInfo__ctor_mA6C27EC67B8E2EAC1D4EF1EDB2E79C232B9D824A (void);
// 0x0000063C Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfo Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfoInt::Get()
extern void ManagerInfoInt_Get_m39809FD72816424D91476E6B916237B6325829DC (void);
// 0x0000063D System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfoInt::Set(Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfo)
extern void ManagerInfoInt_Set_m9155C5B2EB22BEB3C299DE59223F08ACEF36F1B1 (void);
// 0x0000063E System.Int32 Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfoInt::get_FloatMuti()
extern void ManagerInfoInt_get_FloatMuti_m3E12D33EF7D06A4E9B6E08E56FB6DF27695AA66B (void);
// 0x0000063F System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/ManagerInfoInt::.ctor()
extern void ManagerInfoInt__ctor_m5B8160F856BE990E00D3C2A6ED85EE420E731A96 (void);
// 0x00000640 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/CommandMrPointInt::.ctor()
extern void CommandMrPointInt__ctor_mCF364D38914E6DB4E91712B7B38B0924EED79765 (void);
// 0x00000641 System.Void Moonshine.Moondream.Net.NetTcpCommandRadar/CommandManagerInfoInt::.ctor()
extern void CommandManagerInfoInt__ctor_m5BB15E897E8ABD6B22A2E26BF1BCD9398C8C0E7B (void);
// 0x00000642 Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY Moonshine.Moondream.Net.NetTcpCommandRadarEventReceiver::get_CommandKey()
extern void NetTcpCommandRadarEventReceiver_get_CommandKey_m7DFE35B95046F570B7CDB4557B3F5A91D1C3D7F7 (void);
// 0x00000643 System.Void Moonshine.Moondream.Net.NetTcpCommandRadarEventReceiver::OnReceive(System.String)
extern void NetTcpCommandRadarEventReceiver_OnReceive_m2CBAD579181195D5725310AF5BD4668A8A1CE19D (void);
// 0x00000644 System.Void Moonshine.Moondream.Net.NetTcpCommandRadarEventReceiver::.ctor()
extern void NetTcpCommandRadarEventReceiver__ctor_mAA34C55FD68AAEC370A38895CE2EBDB2F78BEB10 (void);
// 0x00000645 System.Void Moonshine.Moondream.Net.NetTcpCommandRadarEventReceiver/PointEvent::.ctor()
extern void PointEvent__ctor_mDED15E845649252D96352EEA178D5844964DC43B (void);
// 0x00000646 System.Void Moonshine.Moondream.Net.NetTcpBase::Send(Moonshine.Moondream.Net.Command)
extern void NetTcpBase_Send_m5229E9056BEF1DDF03A2FC87CAB96030DD6329ED (void);
// 0x00000647 System.Void Moonshine.Moondream.Net.NetTcpBase::OnReceive(System.Net.Sockets.TcpClient,System.String)
extern void NetTcpBase_OnReceive_mCFCC08BA749AC78A2E5AAFF292351F4BBC834B4B (void);
// 0x00000648 System.Void Moonshine.Moondream.Net.NetTcpBase::OnRemove(System.Net.Sockets.TcpClient)
extern void NetTcpBase_OnRemove_mA030BFB6CBEB0BA7550A8E313161AE2FFE04D95D (void);
// 0x00000649 System.Void Moonshine.Moondream.Net.NetTcpBase::Start()
extern void NetTcpBase_Start_mAFECA91A6D7B01F78E9EA64F9974AC182B349C5E (void);
// 0x0000064A System.Void Moonshine.Moondream.Net.NetTcpBase::Update()
extern void NetTcpBase_Update_m24B2A56971CA97BA8583C9585F31C9E39C73C12E (void);
// 0x0000064B System.Void Moonshine.Moondream.Net.NetTcpBase::OnDestroy()
extern void NetTcpBase_OnDestroy_mBE8F642D8B8B4E45AB8A07488215AC1C7EA06A3A (void);
// 0x0000064C System.Void Moonshine.Moondream.Net.NetTcpBase::OnNetStart()
extern void NetTcpBase_OnNetStart_m554A3B7CE09A5BB57768910D16314851BC41C4C0 (void);
// 0x0000064D System.Void Moonshine.Moondream.Net.NetTcpBase::OnNetUpdate()
extern void NetTcpBase_OnNetUpdate_m49B0AB12009A7CD849D9738F7116560F7A2DC0F2 (void);
// 0x0000064E System.Void Moonshine.Moondream.Net.NetTcpBase::OnNetDestroy()
extern void NetTcpBase_OnNetDestroy_m902769AAD4D861ED9F09662D4F8D72AF6578E51A (void);
// 0x0000064F System.Void Moonshine.Moondream.Net.NetTcpBase::TcpClientReceiveProcess()
extern void NetTcpBase_TcpClientReceiveProcess_mECDD2F5EF18750622DACAE164331B9921C7BA355 (void);
// 0x00000650 System.Void Moonshine.Moondream.Net.NetTcpBase::ParseJsonCommand(System.Net.Sockets.TcpClient,System.String)
extern void NetTcpBase_ParseJsonCommand_m89313DB8DAC00805CB8D64447D8A7A058B7FA06C (void);
// 0x00000651 System.Void Moonshine.Moondream.Net.NetTcpBase::AddReceive(Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY,UnityEngine.Events.UnityAction`1<System.String>)
extern void NetTcpBase_AddReceive_mFC7D4AB6F014D39D44BB7CEE16CFF614A32A1A97 (void);
// 0x00000652 System.Void Moonshine.Moondream.Net.NetTcpBase::AddReceive(Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY,UnityEngine.Events.UnityAction`2<System.Net.Sockets.TcpClient,System.String>)
extern void NetTcpBase_AddReceive_mA07BE09459FF87EA10102DF24AA33E8AA748261E (void);
// 0x00000653 System.Void Moonshine.Moondream.Net.NetTcpBase::RemoveReceive(Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY,UnityEngine.Events.UnityAction`1<System.String>)
extern void NetTcpBase_RemoveReceive_m84F153C2388137303D4CDCAE9E6A81FD47375DD2 (void);
// 0x00000654 System.Void Moonshine.Moondream.Net.NetTcpBase::RemoveReceive(Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY,UnityEngine.Events.UnityAction`2<System.Net.Sockets.TcpClient,System.String>)
extern void NetTcpBase_RemoveReceive_m03CFDFF5928FFF834E4251012697EDE7427FEF8C (void);
// 0x00000655 System.String Moonshine.Moondream.Net.NetTcpBase::GetSelfIP(Moonshine.Moondream.Net.NetTcpBase/ADDRESSFAM)
extern void NetTcpBase_GetSelfIP_m8EE4F1D489156A78440FD88382E99EBEDF39A95E (void);
// 0x00000656 System.Void Moonshine.Moondream.Net.NetTcpBase::.ctor()
extern void NetTcpBase__ctor_mBC289CF4CB7D736497D60AC279DB553C95E4DF90 (void);
// 0x00000657 Moonshine.Moondream.MoondreamGlobal/CONNECT_STATE Moonshine.Moondream.Net.NetTcpClient::get_State()
extern void NetTcpClient_get_State_mE2A95D990953181B048F28A8963AFB998B392198 (void);
// 0x00000658 System.Void Moonshine.Moondream.Net.NetTcpClient::set_State(Moonshine.Moondream.MoondreamGlobal/CONNECT_STATE)
extern void NetTcpClient_set_State_m27337EE737FA5F730A0C6FF77E90CF534676B7F9 (void);
// 0x00000659 System.Boolean Moonshine.Moondream.Net.NetTcpClient::get_IsConnected()
extern void NetTcpClient_get_IsConnected_m44DDE5F97F3013463D1A2647F3A284DECC273716 (void);
// 0x0000065A System.Void Moonshine.Moondream.Net.NetTcpClient::OnNetStart()
extern void NetTcpClient_OnNetStart_mB977D1201C8589B6AE2E0FC76A7C40FB5DD5AA28 (void);
// 0x0000065B System.Void Moonshine.Moondream.Net.NetTcpClient::OnNetUpdate()
extern void NetTcpClient_OnNetUpdate_mACD7D4F80DFCC8B6A0A6AD6621EFAE76A1AD8F20 (void);
// 0x0000065C System.Void Moonshine.Moondream.Net.NetTcpClient::OnNetDestroy()
extern void NetTcpClient_OnNetDestroy_m2515A6821A1B5DBF9A19DCFA03973BFF1A020291 (void);
// 0x0000065D System.Collections.IEnumerator Moonshine.Moondream.Net.NetTcpClient::IEConnectChecker()
extern void NetTcpClient_IEConnectChecker_m65AF567FDA8580CB6E511D6973CBB1FDEE354EBA (void);
// 0x0000065E System.Void Moonshine.Moondream.Net.NetTcpClient::StartConnection()
extern void NetTcpClient_StartConnection_m980500371FD3DC817B5AA91C144596ADBFCED67C (void);
// 0x0000065F System.Void Moonshine.Moondream.Net.NetTcpClient::CloseConnection()
extern void NetTcpClient_CloseConnection_mD6074A0496B50EF173D999428277BAB7D741E3E8 (void);
// 0x00000660 System.Void Moonshine.Moondream.Net.NetTcpClient::ListenForData()
extern void NetTcpClient_ListenForData_m855A8EA35E9E327F423820AF6885A5F804C6084C (void);
// 0x00000661 System.Void Moonshine.Moondream.Net.NetTcpClient::Send(Moonshine.Moondream.Net.Command)
extern void NetTcpClient_Send_m6C2A8469FF603DF0855CE236DF7A59DD12640C39 (void);
// 0x00000662 System.Void Moonshine.Moondream.Net.NetTcpClient::StartConnect()
extern void NetTcpClient_StartConnect_mE48822326008B46D7F2D153020C1E2758083189A (void);
// 0x00000663 System.Void Moonshine.Moondream.Net.NetTcpClient::StopConnect()
extern void NetTcpClient_StopConnect_mBB4919C3A3C1C5F3314534C12893193B7D00FB70 (void);
// 0x00000664 System.Void Moonshine.Moondream.Net.NetTcpClient::TestSend()
extern void NetTcpClient_TestSend_m838FC6C444919FDFAFBFADCE59A029A7C82F13E3 (void);
// 0x00000665 System.Void Moonshine.Moondream.Net.NetTcpClient::.ctor()
extern void NetTcpClient__ctor_m17878DD54EF77E6C5D3C733ECA0895AB1EC58579 (void);
// 0x00000666 System.Void Moonshine.Moondream.Net.NetTcpClient/ServerConfig::.ctor()
extern void ServerConfig__ctor_m74167241DE821404E2DFB7B6AF4CCAB417EE14D9 (void);
// 0x00000667 System.Void Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::.ctor(System.Int32)
extern void U3CIEConnectCheckerU3Ed__18__ctor_m7667FA4CAFD90218927D7B52D5C7DCC92D29D42F (void);
// 0x00000668 System.Void Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::System.IDisposable.Dispose()
extern void U3CIEConnectCheckerU3Ed__18_System_IDisposable_Dispose_mA419303A04EC03BA765C4A7660F46207C19C4B87 (void);
// 0x00000669 System.Boolean Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::MoveNext()
extern void U3CIEConnectCheckerU3Ed__18_MoveNext_m17D24B9035EC2C4C5713FF8AA61FB2BCB8479D7E (void);
// 0x0000066A System.Object Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEConnectCheckerU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB75DB48057F0B87063D27C3B63974091B5541E52 (void);
// 0x0000066B System.Void Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::System.Collections.IEnumerator.Reset()
extern void U3CIEConnectCheckerU3Ed__18_System_Collections_IEnumerator_Reset_mDF7EC0D8FDF7631E4EFD68C95E0C6482B86BBFF2 (void);
// 0x0000066C System.Object Moonshine.Moondream.Net.NetTcpClient/<IEConnectChecker>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CIEConnectCheckerU3Ed__18_System_Collections_IEnumerator_get_Current_m2D26B9DDC6D4094049C6CC87CA4989579576863E (void);
// 0x0000066D System.Void Moonshine.Moondream.Net.NetTcpClientUI::Awake()
extern void NetTcpClientUI_Awake_m0539284B8A151778038293AFC8FFD8A3DA3FF2D7 (void);
// 0x0000066E System.Void Moonshine.Moondream.Net.NetTcpClientUI::Update()
extern void NetTcpClientUI_Update_m2B739F1B6947162971EE7350B879BD5E89F4CB87 (void);
// 0x0000066F System.Void Moonshine.Moondream.Net.NetTcpClientUI::StartConnect()
extern void NetTcpClientUI_StartConnect_mDE897BB7596A5ABF82BB0298428833679181AA72 (void);
// 0x00000670 System.Void Moonshine.Moondream.Net.NetTcpClientUI::StopConnect()
extern void NetTcpClientUI_StopConnect_m2087F3E9AE5AA60D769D2EED15318C0649DA491B (void);
// 0x00000671 System.Void Moonshine.Moondream.Net.NetTcpClientUI::SendText()
extern void NetTcpClientUI_SendText_m7C56BDB19D020F07D7C7A925F418BB6749494889 (void);
// 0x00000672 System.Void Moonshine.Moondream.Net.NetTcpClientUI::OnReceive(Moonshine.Moondream.MoondreamGlobal/COMMAND_KEY,System.String)
extern void NetTcpClientUI_OnReceive_m3E70C489683BD75B7D4B8936A79666F3324E1CF6 (void);
// 0x00000673 System.Void Moonshine.Moondream.Net.NetTcpClientUI::LogText()
extern void NetTcpClientUI_LogText_m226C76143CEBBF3225B8DD7D440DE6E1B5CD8D73 (void);
// 0x00000674 System.Void Moonshine.Moondream.Net.NetTcpClientUI::.ctor()
extern void NetTcpClientUI__ctor_mCB8B0382BABA84B5590B0AEF80ED28B7FCDC90A5 (void);
// 0x00000675 System.Void Moonshine.Moondream.Net.NetTcpClientUI::<Awake>b__13_0()
extern void NetTcpClientUI_U3CAwakeU3Eb__13_0_mED77C26440357DDBC30367A58769D7B63B597971 (void);
// 0x00000676 System.Void Moonshine.Moondream.Net.NetTcpClientUI::<Awake>b__13_1()
extern void NetTcpClientUI_U3CAwakeU3Eb__13_1_m51316CC0B16C59046404A2BEB870D2444D87B8C3 (void);
// 0x00000677 System.Void Moonshine.Moondream.Net.NetTcpClientUI::<Awake>b__13_2()
extern void NetTcpClientUI_U3CAwakeU3Eb__13_2_m6926B73F4DD0D775FF0CFB223C7F0A22BE39BCE5 (void);
// 0x00000678 System.Void Moonshine.Moondream.Net.NetTcpClientUI/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m58419B6A7FA249D003044A59D1E507585F5721F6 (void);
// 0x00000679 System.Void Moonshine.Moondream.Net.NetTcpClientUI/<>c__DisplayClass13_0::<Awake>b__3(System.String)
extern void U3CU3Ec__DisplayClass13_0_U3CAwakeU3Eb__3_mD9952291B26422C5D0B84DA7F9D124E72FF58FC6 (void);
// 0x0000067A System.Void Moonshine.Moondream.Utility.ContentStateSample::Start()
extern void ContentStateSample_Start_m322A42195FD7C6192DF1EEC8E7D29E01CD3A9EF8 (void);
// 0x0000067B System.Void Moonshine.Moondream.Utility.ContentStateSample::OnChangeState(Moonshine.Moondream.MoondreamGlobal/CONTENT_GAME_STATE)
extern void ContentStateSample_OnChangeState_m2EA4B5B62C76F3B373D6D3C2640BADAA363AD58F (void);
// 0x0000067C System.Void Moonshine.Moondream.Utility.ContentStateSample::SwitchOn(UnityEngine.GameObject)
extern void ContentStateSample_SwitchOn_m7B78C709579965631DAB4078C3A095AA288F294F (void);
// 0x0000067D System.Collections.IEnumerator Moonshine.Moondream.Utility.ContentStateSample::IEPlay()
extern void ContentStateSample_IEPlay_m1B4A4B53DD450D3B5247595F56E85438275FAC34 (void);
// 0x0000067E System.Void Moonshine.Moondream.Utility.ContentStateSample::.ctor()
extern void ContentStateSample__ctor_m0397B1EB314ABDBE3459644C953CD7F072528C54 (void);
// 0x0000067F System.Void Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::.ctor(System.Int32)
extern void U3CIEPlayU3Ed__7__ctor_mD4955882B86C000ACC0498B6865B3815131879E9 (void);
// 0x00000680 System.Void Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::System.IDisposable.Dispose()
extern void U3CIEPlayU3Ed__7_System_IDisposable_Dispose_m79F6A7CA3E3EDDF66F1AD65AE609B0DA3B6B3B37 (void);
// 0x00000681 System.Boolean Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::MoveNext()
extern void U3CIEPlayU3Ed__7_MoveNext_m6A2083F9971992F9BA2B4E8758BF7D8649B5BC46 (void);
// 0x00000682 System.Object Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CIEPlayU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AEB494184E6F93754B3C92BC69DAA9A9123D5F9 (void);
// 0x00000683 System.Void Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::System.Collections.IEnumerator.Reset()
extern void U3CIEPlayU3Ed__7_System_Collections_IEnumerator_Reset_m1390D4048199AF8E1DE64A53BD215F65F6A68C8D (void);
// 0x00000684 System.Object Moonshine.Moondream.Utility.ContentStateSample/<IEPlay>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CIEPlayU3Ed__7_System_Collections_IEnumerator_get_Current_m63E5BAAC69E6912B5EB54ADE17538BD7F605A0EA (void);
// 0x00000685 System.Void Moonshine.Moondream.Utility.ScreenSetter::Start()
extern void ScreenSetter_Start_m5509B5038C81DECECB1518783F59A63642F612C2 (void);
// 0x00000686 System.Void Moonshine.Moondream.Utility.ScreenSetter::.ctor()
extern void ScreenSetter__ctor_m5C1148FAD264EB456C53E84D3D65EADF78548FC5 (void);
// 0x00000687 System.Void FMSocketIO.Ack::.ctor(System.Int32,System.Action`1<System.String>)
extern void Ack__ctor_m08234500CEC4683E23A3BB6488CF3E80C0DD1AE6 (void);
// 0x00000688 System.Void FMSocketIO.Ack::Invoke(System.String)
extern void Ack_Invoke_m1684EF9DA9D3477DB4291EC7C7B22E64617D7A3E (void);
// 0x00000689 System.String FMSocketIO.Ack::ToString()
extern void Ack_ToString_m18C39847D0BEE63C19DACC22F41472D21744F11D (void);
// 0x0000068A System.String FMSocketIO.SocketIOEvent::get_name()
extern void SocketIOEvent_get_name_m8BF41B50247424907D32828F045A9AEFD4100A7B (void);
// 0x0000068B System.Void FMSocketIO.SocketIOEvent::set_name(System.String)
extern void SocketIOEvent_set_name_mAB1E4E8E4BEE26376B357723E139F1E00C36285E (void);
// 0x0000068C System.String FMSocketIO.SocketIOEvent::get_data()
extern void SocketIOEvent_get_data_mD0BFDDFAF5CED0377B199B49D3E469C8FC9436E3 (void);
// 0x0000068D System.Void FMSocketIO.SocketIOEvent::set_data(System.String)
extern void SocketIOEvent_set_data_m0E8C7171DD9F42F8B3B432BE7C4200B5161392D0 (void);
// 0x0000068E System.Void FMSocketIO.SocketIOEvent::.ctor(System.String)
extern void SocketIOEvent__ctor_mA59F67E832EB9B9A32755553A146B5F09E49FB99 (void);
// 0x0000068F System.Void FMSocketIO.SocketIOEvent::.ctor(System.String,System.String)
extern void SocketIOEvent__ctor_m0F88CC221E37B4C2534F54B6905A36E190D7F3BB (void);
// 0x00000690 FMSocketIO.Packet FMSocketIO.Decoder::Decode(WebSocketSharp.MessageEventArgs)
extern void Decoder_Decode_mE9916415893AC5EAC4A9591E7986D70BE6356323 (void);
// 0x00000691 System.Void FMSocketIO.Decoder::.ctor()
extern void Decoder__ctor_mFBACFE8DBA1DDF616646C8EED85CA2B648895A9E (void);
// 0x00000692 System.String FMSocketIO.Encoder::Encode(FMSocketIO.Packet)
extern void Encoder_Encode_m32540EDF9016B706E02CB246EA2DBA0EE7993F15 (void);
// 0x00000693 System.Void FMSocketIO.Encoder::.ctor()
extern void Encoder__ctor_m40C2B47D58C46A830F9A97E6316C0C5FB8A4563E (void);
// 0x00000694 FMSocketIO.SocketIOEvent FMSocketIO.Parser::Parse(System.String)
extern void Parser_Parse_mCDB90B11F5EE900C8201D843E63E52842D5B28F8 (void);
// 0x00000695 System.String FMSocketIO.Parser::ParseData(System.String)
extern void Parser_ParseData_mCED4BE76844CA9226D7C5BE92E353EC14D3B86D0 (void);
// 0x00000696 System.Void FMSocketIO.Parser::.ctor()
extern void Parser__ctor_m3DD403B121B9CC71093A4D8092BA2F37BAA5DAB1 (void);
// 0x00000697 System.Void FMSocketIO.SocketOpenData::.ctor()
extern void SocketOpenData__ctor_mF5E43B32190B2259A1B8DB582B6246AD216513C5 (void);
// 0x00000698 WebSocketSharp.WebSocket FMSocketIO.SocketIOComponent::get_socket()
extern void SocketIOComponent_get_socket_mA884EBCA27838812B6E294D27FD36E0C3CCFD1A7 (void);
// 0x00000699 System.String FMSocketIO.SocketIOComponent::get_sid()
extern void SocketIOComponent_get_sid_m0C46865BD1B328FED251F23E84F0768B27B470BD (void);
// 0x0000069A System.Void FMSocketIO.SocketIOComponent::set_sid(System.String)
extern void SocketIOComponent_set_sid_mA53C6F8F0D778DE207234664352D150C2BA2ADDC (void);
// 0x0000069B System.Boolean FMSocketIO.SocketIOComponent::get_IsConnected()
extern void SocketIOComponent_get_IsConnected_m9ED854A70EFAE10B4EFC6209EA9A50CFF6332E77 (void);
// 0x0000069C System.Boolean FMSocketIO.SocketIOComponent::IsWebSocketConnected()
extern void SocketIOComponent_IsWebSocketConnected_mA30BCB0FC0190FD722BA9D85232526B35A0D63F7 (void);
// 0x0000069D System.Boolean FMSocketIO.SocketIOComponent::IsWebSocketConnected(WebSocketSharp.WebSocket)
extern void SocketIOComponent_IsWebSocketConnected_m097D3A1E04DACB2220068D4B171B7723ADB9BDFB (void);
// 0x0000069E System.Void FMSocketIO.SocketIOComponent::DebugLog(System.String)
extern void SocketIOComponent_DebugLog_m8DD4451BC850A1382014C701B91A7AF5D6A79FE8 (void);
// 0x0000069F System.Void FMSocketIO.SocketIOComponent::Awake()
extern void SocketIOComponent_Awake_m68239C7A2402548546E6798F030787D946B09F5D (void);
// 0x000006A0 System.Void FMSocketIO.SocketIOComponent::Init()
extern void SocketIOComponent_Init_m6E1B61362320E5FAB20CD66D9061AF28D2AC49F4 (void);
// 0x000006A1 System.Void FMSocketIO.SocketIOComponent::Update()
extern void SocketIOComponent_Update_m1956204DD766153E837C3CAE978C534E0EA0C2E0 (void);
// 0x000006A2 System.Void FMSocketIO.SocketIOComponent::OnDestroy()
extern void SocketIOComponent_OnDestroy_m830362FAD05E1C5367CBAF76DC68296167600F5F (void);
// 0x000006A3 System.Void FMSocketIO.SocketIOComponent::OnApplicationQuit()
extern void SocketIOComponent_OnApplicationQuit_mF7FF8EE476DFF358E629D6FB68CDB9897163B993 (void);
// 0x000006A4 System.Void FMSocketIO.SocketIOComponent::Connect()
extern void SocketIOComponent_Connect_m62FAA7EAE40B164CFE919172454BE0031E778924 (void);
// 0x000006A5 System.Void FMSocketIO.SocketIOComponent::Close()
extern void SocketIOComponent_Close_mF0F34BF8A82E9C34FC5B64253833C8F63D059A71 (void);
// 0x000006A6 System.Void FMSocketIO.SocketIOComponent::On(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponent_On_m59A96433B41B36469DD95C8E34C3902CE7F89CCE (void);
// 0x000006A7 System.Void FMSocketIO.SocketIOComponent::Off(System.String,System.Action`1<FMSocketIO.SocketIOEvent>)
extern void SocketIOComponent_Off_m5C653B03FA1B99F63AE91783395DFBCF44A8C060 (void);
// 0x000006A8 System.Void FMSocketIO.SocketIOComponent::EmitRaw(System.String)
extern void SocketIOComponent_EmitRaw_mC3061234D412577D3A5EBECBF818BFCEBD52993B (void);
// 0x000006A9 System.Void FMSocketIO.SocketIOComponent::Emit(System.String)
extern void SocketIOComponent_Emit_mD6C33729F2C0139BC5D803903476E33B3BA2E69D (void);
// 0x000006AA System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.Action`1<System.String>)
extern void SocketIOComponent_Emit_m8DA5788D2FBBBF6FC99DF0BF58689C7D9F47608A (void);
// 0x000006AB System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.String)
extern void SocketIOComponent_Emit_mAC61D914F84B78301CD978C01DD5A73E12FA298F (void);
// 0x000006AC System.Void FMSocketIO.SocketIOComponent::Emit(System.String,System.String,System.Action`1<System.String>)
extern void SocketIOComponent_Emit_m6C169DB7463C5EA6E415CFFC0D9DB3EB049AE720 (void);
// 0x000006AD System.Void FMSocketIO.SocketIOComponent::RunSocketThread(System.Object)
extern void SocketIOComponent_RunSocketThread_m363B2B632BBF024C54AB8BB194C37573B98FDE46 (void);
// 0x000006AE System.Void FMSocketIO.SocketIOComponent::RunPingThread(System.Object)
extern void SocketIOComponent_RunPingThread_mBC668F3C3CE8368A7F04CF3C8C8DA72829868EDB (void);
// 0x000006AF System.Void FMSocketIO.SocketIOComponent::EmitMessage(System.Int32,System.String)
extern void SocketIOComponent_EmitMessage_m13602AE9F37F31561E9737869DEEA3214CCBC222 (void);
// 0x000006B0 System.Void FMSocketIO.SocketIOComponent::EmitClose()
extern void SocketIOComponent_EmitClose_m53A0841BB638D99F5E95CCB667E9E0E0D0485350 (void);
// 0x000006B1 System.Void FMSocketIO.SocketIOComponent::EmitPacket(FMSocketIO.Packet)
extern void SocketIOComponent_EmitPacket_m87EA1D5B4E9042BB1D7D9BB95B86AE1E9D112968 (void);
// 0x000006B2 System.Void FMSocketIO.SocketIOComponent::OnOpen(System.Object,System.EventArgs)
extern void SocketIOComponent_OnOpen_mBDB7B465DA5508D96CAAD6D7AD0A9F075F6C4167 (void);
// 0x000006B3 System.Void FMSocketIO.SocketIOComponent::OnMessage(System.Object,WebSocketSharp.MessageEventArgs)
extern void SocketIOComponent_OnMessage_m45B84D7E90C2F947A0C7B665BB7E7EE195417CAB (void);
// 0x000006B4 System.Void FMSocketIO.SocketIOComponent::HandleOpen(FMSocketIO.Packet)
extern void SocketIOComponent_HandleOpen_m9CEC78ED9C6141EFCC67A87263B2F2B00E55D0D9 (void);
// 0x000006B5 System.Void FMSocketIO.SocketIOComponent::HandlePing()
extern void SocketIOComponent_HandlePing_m97B4AAA6970A059AFAA628E99E4BDB0DA841134E (void);
// 0x000006B6 System.Void FMSocketIO.SocketIOComponent::HandlePong()
extern void SocketIOComponent_HandlePong_m4C7BC4C912FA04DCDCFD4F21E8AA282C08C0362B (void);
// 0x000006B7 System.Void FMSocketIO.SocketIOComponent::HandleMessage(FMSocketIO.Packet)
extern void SocketIOComponent_HandleMessage_m8C2D8164F0E809D8ECED2CB4C5B67E5B4E2CEF95 (void);
// 0x000006B8 System.Void FMSocketIO.SocketIOComponent::OnError(System.Object,WebSocketSharp.ErrorEventArgs)
extern void SocketIOComponent_OnError_mE37CF1EBD007D80766F71FCFF61159829038D3AB (void);
// 0x000006B9 System.Void FMSocketIO.SocketIOComponent::OnClose(System.Object,WebSocketSharp.CloseEventArgs)
extern void SocketIOComponent_OnClose_m502B56788A12AC02BF7A57DC4BA6DB7189E490CD (void);
// 0x000006BA System.Void FMSocketIO.SocketIOComponent::EmitEvent(System.String)
extern void SocketIOComponent_EmitEvent_mDEF25F0171CBECA7C9E3BF3D9E2EC484ACBB19A3 (void);
// 0x000006BB System.Void FMSocketIO.SocketIOComponent::EmitEvent(FMSocketIO.SocketIOEvent)
extern void SocketIOComponent_EmitEvent_m3CA2EF1EEAA7B71AADBF48C80A116308E047A08D (void);
// 0x000006BC System.Void FMSocketIO.SocketIOComponent::InvokeAck(FMSocketIO.Packet)
extern void SocketIOComponent_InvokeAck_m67F9ECDC987E0AB81C68D08136D8619A967BED43 (void);
// 0x000006BD System.Void FMSocketIO.SocketIOComponent::.ctor()
extern void SocketIOComponent__ctor_mA0E54A729810BC4451F24547B3EEA6F07AF6D3BC (void);
// 0x000006BE System.Void FMSocketIO.SocketIOException::.ctor()
extern void SocketIOException__ctor_mCFF7B8D15E809DB1DA844E1FFEE0676D71F1F7B4 (void);
// 0x000006BF System.Void FMSocketIO.SocketIOException::.ctor(System.String)
extern void SocketIOException__ctor_mC788EB73AD818F9F9BA787DCC771B4D51C7EE27B (void);
// 0x000006C0 System.Void FMSocketIO.SocketIOException::.ctor(System.String,System.Exception)
extern void SocketIOException__ctor_mFC0408FCE9B6F6DF878E7178EFA2A1D62A258624 (void);
// 0x000006C1 System.Void FMSocketIO.Packet::.ctor()
extern void Packet__ctor_mF51931D050DA480074A1DEF47033792270B7A4DC (void);
// 0x000006C2 System.Void FMSocketIO.Packet::.ctor(FMSocketIO.EnginePacketType)
extern void Packet__ctor_m574DBF18FC45DF64A934726B04BD456628659999 (void);
// 0x000006C3 System.Void FMSocketIO.Packet::.ctor(FMSocketIO.EnginePacketType,FMSocketIO.SocketPacketType,System.Int32,System.String,System.Int32,System.String)
extern void Packet__ctor_m40039C66ED685CD2480AD0571FBA9B5C9D1AF139 (void);
// 0x000006C4 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Player()
extern void ApplyToMaterial_get_Player_mB6C8A0EF379B2463DA0545E230B21EAD550B8E57 (void);
// 0x000006C5 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMaterial_set_Player_m01EC27B0C640E4009F2878B73122A01D8ABD9D72 (void);
// 0x000006C6 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.ApplyToMaterial::get_DefaultTexture()
extern void ApplyToMaterial_get_DefaultTexture_m4C03F0037670A9B098D7AB3AD72705C9F96C9ECA (void);
// 0x000006C7 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_DefaultTexture(UnityEngine.Texture2D)
extern void ApplyToMaterial_set_DefaultTexture_m64F519D1141C4C13C498E08B69F1A2786DDA1A96 (void);
// 0x000006C8 UnityEngine.Material RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Material()
extern void ApplyToMaterial_get_Material_m8DF293249B9F2C1A20FEFA2D4E91FBB14E808C49 (void);
// 0x000006C9 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Material(UnityEngine.Material)
extern void ApplyToMaterial_set_Material_m79C35328320609A3F46FAC341952715AF67C5F95 (void);
// 0x000006CA System.String RenderHeads.Media.AVProVideo.ApplyToMaterial::get_TexturePropertyName()
extern void ApplyToMaterial_get_TexturePropertyName_m0EBCDD3F8141A6ACEFE40083833B0BBE6D68F990 (void);
// 0x000006CB System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_TexturePropertyName(System.String)
extern void ApplyToMaterial_set_TexturePropertyName_m68E4267E3E9EF5BD0668646E3B4B2EDDDA964624 (void);
// 0x000006CC UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Offset()
extern void ApplyToMaterial_get_Offset_m2F7740DDBE943479863AB8073927D8DBD0117EBC (void);
// 0x000006CD System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Offset(UnityEngine.Vector2)
extern void ApplyToMaterial_set_Offset_m8FF499A647BEF4A7ECDC2BA3C75C28F577263516 (void);
// 0x000006CE UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMaterial::get_Scale()
extern void ApplyToMaterial_get_Scale_m389E6835F369AF3E1E8594B69C190F34B44BFFD9 (void);
// 0x000006CF System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::set_Scale(UnityEngine.Vector2)
extern void ApplyToMaterial_set_Scale_m9D411AC85C79799D9C0E8C6F61B5C6C6174DEBAB (void);
// 0x000006D0 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::Awake()
extern void ApplyToMaterial_Awake_m7E57B5B508BC0E5CE63E5EAC7371EE0760201858 (void);
// 0x000006D1 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMaterial_ChangeMediaPlayer_m0745378FC9807E03AA1609DE4DCC8D92DA75AD4F (void);
// 0x000006D2 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ForceUpdate()
extern void ApplyToMaterial_ForceUpdate_m3AE2E809819157595FCD1FDC8201B5B8366E996B (void);
// 0x000006D3 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void ApplyToMaterial_OnMediaPlayerEvent_m3A938D7E9BC8DD5434039E4DC5610BFEFB8E9352 (void);
// 0x000006D4 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::LateUpdate()
extern void ApplyToMaterial_LateUpdate_m901EFD1FC7B6BA3DC9963B546008E9AB93AB8FF1 (void);
// 0x000006D5 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::ApplyMapping(UnityEngine.Texture,System.Boolean,System.Int32)
extern void ApplyToMaterial_ApplyMapping_mA9DC832B6875DF510DB1B8B8FFC169402A5F9D2B (void);
// 0x000006D6 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::Start()
extern void ApplyToMaterial_Start_m2BB52F08B0BC2F1AE024F1AEC8C1B1297D18BE52 (void);
// 0x000006D7 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnEnable()
extern void ApplyToMaterial_OnEnable_m846E313CC473E6FCA45D5A54E1BD5B5320E6F79A (void);
// 0x000006D8 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::OnDisable()
extern void ApplyToMaterial_OnDisable_mF74CA0E13A95F4F466326E3D15D51FDB9348B29E (void);
// 0x000006D9 System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::SaveProperties()
extern void ApplyToMaterial_SaveProperties_mFB26241C77CFBBA00F2331E67F8B7C49CEB266EB (void);
// 0x000006DA System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::RestoreProperties()
extern void ApplyToMaterial_RestoreProperties_m7FF3523DC1A19EA6A9D1D8BF16D1D96A13DD07CA (void);
// 0x000006DB System.Void RenderHeads.Media.AVProVideo.ApplyToMaterial::.ctor()
extern void ApplyToMaterial__ctor_m962029797C27CD72C83644B71FF42FAC2640C931 (void);
// 0x000006DC RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.ApplyToMesh::get_Player()
extern void ApplyToMesh_get_Player_mA443704ECC9CAB41E66661C62837724881005F14 (void);
// 0x000006DD System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMesh_set_Player_mFEEB90240904C21B6D4AB9E3A7A38563100CFC33 (void);
// 0x000006DE UnityEngine.Texture2D RenderHeads.Media.AVProVideo.ApplyToMesh::get_DefaultTexture()
extern void ApplyToMesh_get_DefaultTexture_m4FF557DE6887E102DC0D8FFDE62A7BEF89450D59 (void);
// 0x000006DF System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_DefaultTexture(UnityEngine.Texture2D)
extern void ApplyToMesh_set_DefaultTexture_m2B230BE4C850A453459C8E32FE2FDCD5A4E09FC9 (void);
// 0x000006E0 UnityEngine.Renderer RenderHeads.Media.AVProVideo.ApplyToMesh::get_MeshRenderer()
extern void ApplyToMesh_get_MeshRenderer_m00FAA70B2A56BC20EF9E578C47ACAC454C8BB1B0 (void);
// 0x000006E1 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_MeshRenderer(UnityEngine.Renderer)
extern void ApplyToMesh_set_MeshRenderer_m6CA21013EB5DF529CFE2EB772A52B1914EADC2A5 (void);
// 0x000006E2 System.String RenderHeads.Media.AVProVideo.ApplyToMesh::get_TexturePropertyName()
extern void ApplyToMesh_get_TexturePropertyName_m71146112A3E29E7C3D4BED6C1693FEBAB939893C (void);
// 0x000006E3 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_TexturePropertyName(System.String)
extern void ApplyToMesh_set_TexturePropertyName_m2C04ED369F1B3F05F2BA6C147B9ADEE0D51FE37D (void);
// 0x000006E4 UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMesh::get_Offset()
extern void ApplyToMesh_get_Offset_mC45ACBF08437FABCF7CBAAB56690DEA13C9C89BD (void);
// 0x000006E5 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Offset(UnityEngine.Vector2)
extern void ApplyToMesh_set_Offset_mF7CA89E15E34A84976CE82D4271DECD8FF3496E5 (void);
// 0x000006E6 UnityEngine.Vector2 RenderHeads.Media.AVProVideo.ApplyToMesh::get_Scale()
extern void ApplyToMesh_get_Scale_m1A934B3665631E10868E07034CAAFFBFE0106F97 (void);
// 0x000006E7 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::set_Scale(UnityEngine.Vector2)
extern void ApplyToMesh_set_Scale_m2C74ABE8E9B235E05579C2400A2F16F360F37248 (void);
// 0x000006E8 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::Awake()
extern void ApplyToMesh_Awake_mC9F830518F1F041ADF55A41661783FAFD070E634 (void);
// 0x000006E9 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ForceUpdate()
extern void ApplyToMesh_ForceUpdate_mC90728E69359AB7AB69A608DDB421BBE52CA54E2 (void);
// 0x000006EA System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void ApplyToMesh_OnMediaPlayerEvent_m97E527922684F252D4BAD9EA3C1B59E18CAAD8F4 (void);
// 0x000006EB System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void ApplyToMesh_ChangeMediaPlayer_mE3B842C698FF969B7A60AEACD39A0EBF0F38E20B (void);
// 0x000006EC System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::LateUpdate()
extern void ApplyToMesh_LateUpdate_m43ED3BD5D74AE724DC018AE44C62D339CE914D9D (void);
// 0x000006ED System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::ApplyMapping(UnityEngine.Texture,System.Boolean,System.Int32)
extern void ApplyToMesh_ApplyMapping_mC917218EF3DE8D576C73A4273271100FBCAC1811 (void);
// 0x000006EE System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnEnable()
extern void ApplyToMesh_OnEnable_m5302692EC0A37A56FA7200903B043F0149510879 (void);
// 0x000006EF System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnDisable()
extern void ApplyToMesh_OnDisable_mA154C54CD3A11D695E453AC06B2F6EEE4B35B41E (void);
// 0x000006F0 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::OnDestroy()
extern void ApplyToMesh_OnDestroy_mD9B923F54EE32F7D3A8B10EA49358DD85B87F241 (void);
// 0x000006F1 System.Void RenderHeads.Media.AVProVideo.ApplyToMesh::.ctor()
extern void ApplyToMesh__ctor_m9853935678DD2E16331C07E8E4C2BFFACE641E6A (void);
// 0x000006F2 System.Single[] RenderHeads.Media.AVProVideo.AudioChannelMixer::get_Channel()
extern void AudioChannelMixer_get_Channel_m36DE91B7CC4EF0018A665D0F751C42EB8961FB2E (void);
// 0x000006F3 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::set_Channel(System.Single[])
extern void AudioChannelMixer_set_Channel_m17FB468A2757EDCC01E04C0CD5E8110BC95787A5 (void);
// 0x000006F4 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::Reset()
extern void AudioChannelMixer_Reset_m72348CC0D4BA9583B644CD63142A18C976226762 (void);
// 0x000006F5 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::ChangeChannelCount(System.Int32)
extern void AudioChannelMixer_ChangeChannelCount_m1A227F7C5982F0E664D5187F612D0833B5A25B85 (void);
// 0x000006F6 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioChannelMixer_OnAudioFilterRead_m07A362C3079507ED94ECECD378DC4A848068D897 (void);
// 0x000006F7 System.Void RenderHeads.Media.AVProVideo.AudioChannelMixer::.ctor()
extern void AudioChannelMixer__ctor_m15D089E1CFD6D6AFC204941FDF4805F1E3B0E35A (void);
// 0x000006F8 System.Void RenderHeads.Media.AVProVideo.AudioOutput::Awake()
extern void AudioOutput_Awake_mBB71EB6182D93E59CAD7C6DE66925C21A7DD1E4E (void);
// 0x000006F9 System.Void RenderHeads.Media.AVProVideo.AudioOutput::Start()
extern void AudioOutput_Start_m1E8E1637B93BB98B3DB1C47C3E1D4C4252E42934 (void);
// 0x000006FA System.Void RenderHeads.Media.AVProVideo.AudioOutput::OnDestroy()
extern void AudioOutput_OnDestroy_m1732E24E26E46EA8A55A286B422F9A5AA3F5A834 (void);
// 0x000006FB System.Void RenderHeads.Media.AVProVideo.AudioOutput::Update()
extern void AudioOutput_Update_m8DDA88A39B472AC19C3701C6955EA53C382F178E (void);
// 0x000006FC System.Void RenderHeads.Media.AVProVideo.AudioOutput::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void AudioOutput_ChangeMediaPlayer_m5C841D792F1E33838683AF29FEA0CA10224EE255 (void);
// 0x000006FD System.Void RenderHeads.Media.AVProVideo.AudioOutput::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void AudioOutput_OnMediaPlayerEvent_m640548026E67B82F85AE39691D83AAA9CED8E928 (void);
// 0x000006FE System.Void RenderHeads.Media.AVProVideo.AudioOutput::ApplyAudioSettings(RenderHeads.Media.AVProVideo.MediaPlayer,UnityEngine.AudioSource)
extern void AudioOutput_ApplyAudioSettings_m155E212F219AB6E0F6B16B38F1899D7E488CDFF1 (void);
// 0x000006FF System.Void RenderHeads.Media.AVProVideo.AudioOutput::OnAudioFilterRead(System.Single[],System.Int32)
extern void AudioOutput_OnAudioFilterRead_m92C2E01B546CC4C225ABC63F1083FEF45138C4DE (void);
// 0x00000700 System.Void RenderHeads.Media.AVProVideo.AudioOutput::.ctor()
extern void AudioOutput__ctor_mD734F7D2FF41D3BC077C59058EB696F8B9CC525F (void);
// 0x00000701 System.Void RenderHeads.Media.AVProVideo.CubemapCube::set_Player(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void CubemapCube_set_Player_m786DBA437A00F219B8724B4B790127041BFD3F1A (void);
// 0x00000702 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.CubemapCube::get_Player()
extern void CubemapCube_get_Player_m45C5250BADD23763B85A8B24348C659A56B9A070 (void);
// 0x00000703 System.Void RenderHeads.Media.AVProVideo.CubemapCube::Awake()
extern void CubemapCube_Awake_mE13D80223A30E1051C55C73E76CDF29C202E85FE (void);
// 0x00000704 System.Void RenderHeads.Media.AVProVideo.CubemapCube::Start()
extern void CubemapCube_Start_m0C9A1CD846CA7780FDEF2839C1D5D94E533D7B22 (void);
// 0x00000705 System.Void RenderHeads.Media.AVProVideo.CubemapCube::OnDestroy()
extern void CubemapCube_OnDestroy_m529E238B85070B02314BFE3236F72E043528981F (void);
// 0x00000706 System.Void RenderHeads.Media.AVProVideo.CubemapCube::LateUpdate()
extern void CubemapCube_LateUpdate_m85A7FC1AF25749568D38BAA10877993B86C4E15F (void);
// 0x00000707 System.Void RenderHeads.Media.AVProVideo.CubemapCube::BuildMesh()
extern void CubemapCube_BuildMesh_m11D141C43A762E2773E547628158FB6F7B3E0C92 (void);
// 0x00000708 System.Void RenderHeads.Media.AVProVideo.CubemapCube::UpdateMeshUV(System.Int32,System.Int32,System.Boolean)
extern void CubemapCube_UpdateMeshUV_m8B9A0835BB3532A1FB4F5A45300E0DE3C5090281 (void);
// 0x00000709 System.Void RenderHeads.Media.AVProVideo.CubemapCube::.ctor()
extern void CubemapCube__ctor_mA2E10BF9977ADC4D8E9192A46A078F4EBB2FB3F7 (void);
// 0x0000070A System.Boolean RenderHeads.Media.AVProVideo.DebugOverlay::get_DisplayControls()
extern void DebugOverlay_get_DisplayControls_mA25296A3959714C32C8944C41F6FDE3F89696C91 (void);
// 0x0000070B System.Void RenderHeads.Media.AVProVideo.DebugOverlay::set_DisplayControls(System.Boolean)
extern void DebugOverlay_set_DisplayControls_m8574F621F6A16842A6D1532A5E423C1D381A595F (void);
// 0x0000070C RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DebugOverlay::get_CurrentMediaPlayer()
extern void DebugOverlay_get_CurrentMediaPlayer_m68170AB21C0C1F9EF31238EA3CE20A9C3F2A7E6A (void);
// 0x0000070D System.Void RenderHeads.Media.AVProVideo.DebugOverlay::set_CurrentMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void DebugOverlay_set_CurrentMediaPlayer_m807A6CF1DEF8F34106BFEA2E45E2D9753D0A4EA9 (void);
// 0x0000070E System.Void RenderHeads.Media.AVProVideo.DebugOverlay::SetGuiPositionFromVideoIndex(System.Int32)
extern void DebugOverlay_SetGuiPositionFromVideoIndex_mE2C944733349B606C8FD4B0FAFC0EA982BC2DBD9 (void);
// 0x0000070F System.Void RenderHeads.Media.AVProVideo.DebugOverlay::Update()
extern void DebugOverlay_Update_mB215173AEF7E7062D045B338365B90CBC55C1E1B (void);
// 0x00000710 System.Void RenderHeads.Media.AVProVideo.DebugOverlay::OnGUI()
extern void DebugOverlay_OnGUI_mF431BD39EC8BE99F13FF588DCB6110DF56236DEC (void);
// 0x00000711 System.Void RenderHeads.Media.AVProVideo.DebugOverlay::.ctor()
extern void DebugOverlay__ctor_mA38FC6D9F4B3ED5BCD236D24AA1C994E28FFC0A0 (void);
// 0x00000712 System.Void RenderHeads.Media.AVProVideo.DisplayBackground::OnRenderObject()
extern void DisplayBackground_OnRenderObject_m13305EE4E1780947B07A41385E7503A0BDCD86A6 (void);
// 0x00000713 System.Void RenderHeads.Media.AVProVideo.DisplayBackground::.ctor()
extern void DisplayBackground__ctor_mAD78B661699F7B4C90000B3B906772EDA2B9AAA8 (void);
// 0x00000714 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Awake()
extern void DisplayIMGUI_Awake_mA008F02B94AA77426ABA34822495C45A965A3FEC (void);
// 0x00000715 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Start()
extern void DisplayIMGUI_Start_mC915CC4ABC539AB9BD10795DC320CCB20B251293 (void);
// 0x00000716 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::OnDestroy()
extern void DisplayIMGUI_OnDestroy_mD32A66825532BF56B9136001F325D5B3AB4C163D (void);
// 0x00000717 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayIMGUI::GetRequiredShader()
extern void DisplayIMGUI_GetRequiredShader_m85B047763F7B734407E0CEDBEC51CEDABAF50D78 (void);
// 0x00000718 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::Update()
extern void DisplayIMGUI_Update_m4C60931D8BFD20E349E2E601B701D029618F2940 (void);
// 0x00000719 System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::OnGUI()
extern void DisplayIMGUI_OnGUI_mB90CC061E60EFB384DC4DC5E742E6BE85831B795 (void);
// 0x0000071A UnityEngine.Rect RenderHeads.Media.AVProVideo.DisplayIMGUI::GetRect()
extern void DisplayIMGUI_GetRect_m62FA7CE5192D732BF264126F4437A186FDD9B922 (void);
// 0x0000071B System.Void RenderHeads.Media.AVProVideo.DisplayIMGUI::.ctor()
extern void DisplayIMGUI__ctor_m4C1F51E569AAED48140CE7CFF37A7516C06AB1B8 (void);
// 0x0000071C System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::Awake()
extern void DisplayUGUI_Awake_m1100BEFC95A78728DED3C156861B8351C0077552 (void);
// 0x0000071D System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::HasMask(UnityEngine.GameObject)
extern void DisplayUGUI_HasMask_m5DC9A6B4A7CA49383C70AA100F3D4AE9B41CDA03 (void);
// 0x0000071E UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureShader(UnityEngine.Shader,System.String)
extern void DisplayUGUI_EnsureShader_m26A38D72BDBF78F5AEBE8A5DBC65E5EAA60BB758 (void);
// 0x0000071F UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureAlphaPackingShader()
extern void DisplayUGUI_EnsureAlphaPackingShader_mF6561C2FB86D60F3AB5CF2D9698A25DD400A0EB3 (void);
// 0x00000720 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::EnsureStereoPackingShader()
extern void DisplayUGUI_EnsureStereoPackingShader_mBA86459C1A369B131F2069A91B3C528D1A3C7E30 (void);
// 0x00000721 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::Start()
extern void DisplayUGUI_Start_m340D6AC57FCC4A534FC8F7D601EAD815687246A8 (void);
// 0x00000722 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnDestroy()
extern void DisplayUGUI_OnDestroy_m36BDD82F1030860A005BEC3D1983AF2FB2865E9E (void);
// 0x00000723 UnityEngine.Shader RenderHeads.Media.AVProVideo.DisplayUGUI::GetRequiredShader()
extern void DisplayUGUI_GetRequiredShader_mEEE05823A9985B7E30669FEA97A876676EC73E76 (void);
// 0x00000724 UnityEngine.Texture RenderHeads.Media.AVProVideo.DisplayUGUI::get_mainTexture()
extern void DisplayUGUI_get_mainTexture_mD641E773EBB910A6F937B11A68A6BA8D5EE2EEB0 (void);
// 0x00000725 System.Boolean RenderHeads.Media.AVProVideo.DisplayUGUI::HasValidTexture()
extern void DisplayUGUI_HasValidTexture_mC46FA36F2F674D62B0C0FBE269311F3F07E3D626 (void);
// 0x00000726 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::UpdateInternalMaterial()
extern void DisplayUGUI_UpdateInternalMaterial_m4D8CCFF9B776EF4523B908ED69A3A9276FC7337E (void);
// 0x00000727 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::LateUpdate()
extern void DisplayUGUI_LateUpdate_m803830F93553EFECE1BD1027151043FB301B5B9D (void);
// 0x00000728 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.DisplayUGUI::get_CurrentMediaPlayer()
extern void DisplayUGUI_get_CurrentMediaPlayer_m6413594BD172B2BE0F01A23A22ECD6E0A44E914A (void);
// 0x00000729 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::set_CurrentMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void DisplayUGUI_set_CurrentMediaPlayer_m0137026F76CDB76222CB7D154FBE5BFA44F6D415 (void);
// 0x0000072A UnityEngine.Rect RenderHeads.Media.AVProVideo.DisplayUGUI::get_uvRect()
extern void DisplayUGUI_get_uvRect_m5BA7C10E45A554F5134B740A498CF296FA77A48F (void);
// 0x0000072B System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::set_uvRect(UnityEngine.Rect)
extern void DisplayUGUI_set_uvRect_mE6FD948006B313F5E722F0EF75D7EE4E4EE148F4 (void);
// 0x0000072C System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::SetNativeSize()
extern void DisplayUGUI_SetNativeSize_mBFB78E70F72FACE80348730711BC1EBED76D430D (void);
// 0x0000072D System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnPopulateMesh(UnityEngine.UI.VertexHelper)
extern void DisplayUGUI_OnPopulateMesh_mB6B84D334C9C2EB105650CD0DFDE9D57FDDAB757 (void);
// 0x0000072E System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void DisplayUGUI_OnFillVBO_m4B2E50B68D5D2288CFF34E5770AEF75A9EE350E1 (void);
// 0x0000072F System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::_OnFillVBO(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern void DisplayUGUI__OnFillVBO_mE5A8F484001FC74FA85E4269124511E781C3F3CD (void);
// 0x00000730 UnityEngine.Vector4 RenderHeads.Media.AVProVideo.DisplayUGUI::GetDrawingDimensions(UnityEngine.ScaleMode,UnityEngine.Rect&)
extern void DisplayUGUI_GetDrawingDimensions_mBA562A0DD5E74F6B38E54DACF1431589D11494F9 (void);
// 0x00000731 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::.ctor()
extern void DisplayUGUI__ctor_m173552BE5F1B4158F144E3EA4B5401C604E57C0C (void);
// 0x00000732 System.Void RenderHeads.Media.AVProVideo.DisplayUGUI::.cctor()
extern void DisplayUGUI__cctor_mDA8776C0893A01DC3126AF144EFCFE11EE7D8701 (void);
// 0x00000733 RenderHeads.Media.AVProVideo.Resampler RenderHeads.Media.AVProVideo.MediaPlayer::get_FrameResampler()
extern void MediaPlayer_get_FrameResampler_mFD4B144D49EBC8C5E71D2EC06EA5496016CEC775 (void);
// 0x00000734 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_Persistent()
extern void MediaPlayer_get_Persistent_m8B7F0802B6A0FEF69554DBA04714612FABB9AD9C (void);
// 0x00000735 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_Persistent(System.Boolean)
extern void MediaPlayer_set_Persistent_m0DA7A5514D93EFCF8161B0F830CBE54E910EDC43 (void);
// 0x00000736 RenderHeads.Media.AVProVideo.VideoMapping RenderHeads.Media.AVProVideo.MediaPlayer::get_VideoLayoutMapping()
extern void MediaPlayer_get_VideoLayoutMapping_m6DA508DD29B8E3845E79BA5CB3AED45D1B8DB4D4 (void);
// 0x00000737 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_VideoLayoutMapping(RenderHeads.Media.AVProVideo.VideoMapping)
extern void MediaPlayer_set_VideoLayoutMapping_mAA98A49170DDC5F7B1951E7A38B6CE84CBFAD383 (void);
// 0x00000738 RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.MediaPlayer::get_Info()
extern void MediaPlayer_get_Info_m892C1B5EE61D1CA6EAE5D4B7CEE6BCC5095F5E25 (void);
// 0x00000739 RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.MediaPlayer::get_Control()
extern void MediaPlayer_get_Control_m9F501D1DC633B3B0C9A7F19F4DEC9942CB502785 (void);
// 0x0000073A RenderHeads.Media.AVProVideo.IMediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer::get_Player()
extern void MediaPlayer_get_Player_mD38D49159D63F0392915E24501953BA7222A3C94 (void);
// 0x0000073B RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.MediaPlayer::get_TextureProducer()
extern void MediaPlayer_get_TextureProducer_m859AA3620F1A0EF46BB8CB76A4ECC1D952734D30 (void);
// 0x0000073C RenderHeads.Media.AVProVideo.IMediaSubtitles RenderHeads.Media.AVProVideo.MediaPlayer::get_Subtitles()
extern void MediaPlayer_get_Subtitles_mC487084FFA754521D3673E9CC9088CA66BB21EC9 (void);
// 0x0000073D RenderHeads.Media.AVProVideo.MediaPlayerEvent RenderHeads.Media.AVProVideo.MediaPlayer::get_Events()
extern void MediaPlayer_get_Events_m6AA188A1F784751E6E4C011CDF9C435BC22747D1 (void);
// 0x0000073E System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_VideoOpened()
extern void MediaPlayer_get_VideoOpened_m0ECBBEAEF0C2318D2825FBB2954781AF6A1DB554 (void);
// 0x0000073F System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_PauseMediaOnAppPause()
extern void MediaPlayer_get_PauseMediaOnAppPause_mB510C4F2BF54D080F4C16B2C993CC51E21976EF7 (void);
// 0x00000740 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_PauseMediaOnAppPause(System.Boolean)
extern void MediaPlayer_set_PauseMediaOnAppPause_m181588E3BBE8EF18B3D62D429E82E6A13A54638B (void);
// 0x00000741 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_PlayMediaOnAppUnpause()
extern void MediaPlayer_get_PlayMediaOnAppUnpause_m34DC30B0AD6C2FCB474B0AC28C53BF40C744F37D (void);
// 0x00000742 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_PlayMediaOnAppUnpause(System.Boolean)
extern void MediaPlayer_set_PlayMediaOnAppUnpause_m52D2A2587E418FFDF3345AF9B171AAB917A7EC83 (void);
// 0x00000743 RenderHeads.Media.AVProVideo.FileFormat RenderHeads.Media.AVProVideo.MediaPlayer::get_ForceFileFormat()
extern void MediaPlayer_get_ForceFileFormat_mEF3A7984D5B24D7D13975680FBF7445100B5F691 (void);
// 0x00000744 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_ForceFileFormat(RenderHeads.Media.AVProVideo.FileFormat)
extern void MediaPlayer_set_ForceFileFormat_m7574D7273C22D98272945A528BA205113C6CFAC1 (void);
// 0x00000745 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioHeadTransform(UnityEngine.Transform)
extern void MediaPlayer_set_AudioHeadTransform_m3BF627799C6270FCCE5479547086BFCCC4F3865E (void);
// 0x00000746 UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioHeadTransform()
extern void MediaPlayer_get_AudioHeadTransform_m19E267C75F48D9A9CDECC0F7D6607ACACB37C8C5 (void);
// 0x00000747 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusEnabled()
extern void MediaPlayer_get_AudioFocusEnabled_m78DBBD149307F7E97754F7A44F751F14D3170E40 (void);
// 0x00000748 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusEnabled(System.Boolean)
extern void MediaPlayer_set_AudioFocusEnabled_m1072BCF69DE55ECD9E4A0FEE88ED44E4CF47F962 (void);
// 0x00000749 System.Single RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusOffLevelDB()
extern void MediaPlayer_get_AudioFocusOffLevelDB_mC4284F2F6BFB7C1D983D270983AA798D07A7B78D (void);
// 0x0000074A System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusOffLevelDB(System.Single)
extern void MediaPlayer_set_AudioFocusOffLevelDB_mE858792F9C6BC1E75F5C189221D68B3471BD81AB (void);
// 0x0000074B System.Single RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusWidthDegrees()
extern void MediaPlayer_get_AudioFocusWidthDegrees_m52071D006D7F6D06E2C61B0F5508426FA838AB85 (void);
// 0x0000074C System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusWidthDegrees(System.Single)
extern void MediaPlayer_set_AudioFocusWidthDegrees_m9067556A5EB455E5DD5E64810A3C32522EF1A79C (void);
// 0x0000074D UnityEngine.Transform RenderHeads.Media.AVProVideo.MediaPlayer::get_AudioFocusTransform()
extern void MediaPlayer_get_AudioFocusTransform_mA161A65E069EBF3BABB6F553470EB1935F6CF46C (void);
// 0x0000074E System.Void RenderHeads.Media.AVProVideo.MediaPlayer::set_AudioFocusTransform(UnityEngine.Transform)
extern void MediaPlayer_set_AudioFocusTransform_m5B46E3A23179B11CBE23390467EB42E159DDF47A (void);
// 0x0000074F RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindows()
extern void MediaPlayer_get_PlatformOptionsWindows_m32BEE4FD8E72E2FC34401A4AFC60D6B924847E12 (void);
// 0x00000750 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsMacOSX()
extern void MediaPlayer_get_PlatformOptionsMacOSX_m6DFEA8DE608A93BD9481F99D76934A8CDBDCCA5E (void);
// 0x00000751 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsIOS()
extern void MediaPlayer_get_PlatformOptionsIOS_m80010BFF3D40711BEE55AC6318A3C17CCF20D3BF (void);
// 0x00000752 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsTVOS()
extern void MediaPlayer_get_PlatformOptionsTVOS_m0749CF890B381DF64D6791F25948604BBBD530CB (void);
// 0x00000753 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsAndroid()
extern void MediaPlayer_get_PlatformOptionsAndroid_m587557F7E207174FDD4DEBEF10E14DF565DAEF68 (void);
// 0x00000754 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindowsPhone()
extern void MediaPlayer_get_PlatformOptionsWindowsPhone_mEDAE7CA6631BFE9374BF274134DA6C0381CE306C (void);
// 0x00000755 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWindowsUWP()
extern void MediaPlayer_get_PlatformOptionsWindowsUWP_m41EFFEA11D55D7DA8D6A4F1B2C782E4EAB332793 (void);
// 0x00000756 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsWebGL()
extern void MediaPlayer_get_PlatformOptionsWebGL_m8A785B44082027BD6050750E123874834F309348 (void);
// 0x00000757 RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4 RenderHeads.Media.AVProVideo.MediaPlayer::get_PlatformOptionsPS4()
extern void MediaPlayer_get_PlatformOptionsPS4_m272F9F2F70F840B2704477A8F225E85F912F424D (void);
// 0x00000758 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Awake()
extern void MediaPlayer_Awake_m81585DF80FFC96ED2FAF6B5694A285DEE2E25CC5 (void);
// 0x00000759 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Initialise()
extern void MediaPlayer_Initialise_mC02870F200DBB011EF0D69F35B91556886BAFE1C (void);
// 0x0000075A System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Start()
extern void MediaPlayer_Start_mA2B1902059842C2F2A6452D34300B6378F2BDD86 (void);
// 0x0000075B System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromFile(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String,System.Boolean)
extern void MediaPlayer_OpenVideoFromFile_mF8BBE1AF6D372DA420FAEA6544FD99B3F852C7D4 (void);
// 0x0000075C System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromBuffer(System.Byte[],System.Boolean)
extern void MediaPlayer_OpenVideoFromBuffer_mFD6AEA9C0F03D1ACF8CBABBE452B1887D8854E6C (void);
// 0x0000075D System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::StartOpenChunkedVideoFromBuffer(System.UInt64,System.Boolean)
extern void MediaPlayer_StartOpenChunkedVideoFromBuffer_mB26D87D4A0FFC1D39FED64D1A7D743458D311300 (void);
// 0x0000075E System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
extern void MediaPlayer_AddChunkToVideoBuffer_m84548CCDD0FD75D4EC9CBA422F4514D2545AEF10 (void);
// 0x0000075F System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EndOpenChunkedVideoFromBuffer()
extern void MediaPlayer_EndOpenChunkedVideoFromBuffer_m936370BFA408F2F4685C8D7AAC7A651F0FA87C30 (void);
// 0x00000760 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitlesEnabled()
extern void MediaPlayer_get_SubtitlesEnabled_m41791B6BE4871E561B7E1515280EA27D17482FC0 (void);
// 0x00000761 System.String RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitlePath()
extern void MediaPlayer_get_SubtitlePath_m7C69DA4B86C83150E3F298CCA675702B015C5221 (void);
// 0x00000762 RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.MediaPlayer::get_SubtitleLocation()
extern void MediaPlayer_get_SubtitleLocation_mDBD69B1ED28FFC0B7D7E37569F0F59EB643D020D (void);
// 0x00000763 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EnableSubtitles(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String)
extern void MediaPlayer_EnableSubtitles_mF697604562DF14D9337BCB5AA1E1FFBCA6CE84ED (void);
// 0x00000764 System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::LoadSubtitlesCoroutine(System.String,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation,System.String)
extern void MediaPlayer_LoadSubtitlesCoroutine_mC1C588A71F4166374D28AE67922782263215AB90 (void);
// 0x00000765 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::DisableSubtitles()
extern void MediaPlayer_DisableSubtitles_mDD634C56194D865C80B899AE2FDE8306B2B60651 (void);
// 0x00000766 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromBufferInternal(System.Byte[])
extern void MediaPlayer_OpenVideoFromBufferInternal_m8B3AA056B2B742D23080B83DDCCA810AA20AD691 (void);
// 0x00000767 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::StartOpenVideoFromBufferInternal(System.UInt64)
extern void MediaPlayer_StartOpenVideoFromBufferInternal_m859DA6C32CF39B1E44414F81F41AA2357B6946CA (void);
// 0x00000768 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::AddChunkToBufferInternal(System.Byte[],System.UInt64,System.UInt64)
extern void MediaPlayer_AddChunkToBufferInternal_m8B621222B739B3AA135B244C0AD23BECAF61BFA5 (void);
// 0x00000769 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::EndOpenVideoFromBufferInternal()
extern void MediaPlayer_EndOpenVideoFromBufferInternal_mB6E615E12FF5AEEE02E9F52842901B9D2CD47397 (void);
// 0x0000076A System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::OpenVideoFromFile()
extern void MediaPlayer_OpenVideoFromFile_m1E7E6CBB1217E4CC631EF0C40F980184E4E3C3AD (void);
// 0x0000076B System.Void RenderHeads.Media.AVProVideo.MediaPlayer::SetPlaybackOptions()
extern void MediaPlayer_SetPlaybackOptions_mE1766C4B3918B7B3C46EBE05EDE96F3026B096A9 (void);
// 0x0000076C System.Void RenderHeads.Media.AVProVideo.MediaPlayer::CloseVideo()
extern void MediaPlayer_CloseVideo_mFA38C016D94EB68335AF5FD633C4656B2985E0BA (void);
// 0x0000076D System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Play()
extern void MediaPlayer_Play_mDCA8D2FD3713AD945BA974B834C4BC2B3333CA10 (void);
// 0x0000076E System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Pause()
extern void MediaPlayer_Pause_m5DCF2D66E3D766151FFD28C01601ED58EDB6D2BF (void);
// 0x0000076F System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Stop()
extern void MediaPlayer_Stop_m7D021D8158E851FC42FFD244AEC95694D87C6F5F (void);
// 0x00000770 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Rewind(System.Boolean)
extern void MediaPlayer_Rewind_mF70EC8972BD5C7C80E27069ABFA28B2AD4922814 (void);
// 0x00000771 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::Update()
extern void MediaPlayer_Update_mAB86CC034891635C1DF6EDA82D2D4C3C38A1B380 (void);
// 0x00000772 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::LateUpdate()
extern void MediaPlayer_LateUpdate_m981B468BA9E097CAAC93EC48C535DB8984CB1D5B (void);
// 0x00000773 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateResampler()
extern void MediaPlayer_UpdateResampler_m9CCE634CC4598192B07B2CDB66F11DFC4439F38F (void);
// 0x00000774 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnEnable()
extern void MediaPlayer_OnEnable_mB8E7F8702477636CA36075BCBC08E855F5CA1317 (void);
// 0x00000775 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnDisable()
extern void MediaPlayer_OnDisable_mC64A3FCF151089B3F87BD0DC89E4DB5F3F22010B (void);
// 0x00000776 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnDestroy()
extern void MediaPlayer_OnDestroy_m33D0537F397377727E7A2FC4C856FF20C750C7F1 (void);
// 0x00000777 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationQuit()
extern void MediaPlayer_OnApplicationQuit_m6413D5BE3FB31758E3BF472E67D0084EA6203D82 (void);
// 0x00000778 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::StartRenderCoroutine()
extern void MediaPlayer_StartRenderCoroutine_mFB469F063776D12BE46098E7BFD909D75F98A5DE (void);
// 0x00000779 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::StopRenderCoroutine()
extern void MediaPlayer_StopRenderCoroutine_m7175FD751272DFAB1BBB26D8EBB2D48341246FC5 (void);
// 0x0000077A System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::FinalRenderCapture()
extern void MediaPlayer_FinalRenderCapture_m595B57A33925C770854F1072776D29CA813858B5 (void);
// 0x0000077B RenderHeads.Media.AVProVideo.Platform RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatform()
extern void MediaPlayer_GetPlatform_m56A303D68A3D0BBF592A6062850C02F5B392430F (void);
// 0x0000077C RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions RenderHeads.Media.AVProVideo.MediaPlayer::GetCurrentPlatformOptions()
extern void MediaPlayer_GetCurrentPlatformOptions_m625311690AC47FBADEE660C6C264F0FDEDA851A6 (void);
// 0x0000077D System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPath(RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation)
extern void MediaPlayer_GetPath_mF9FC7476D5B2BCBC16F4BDE13F88736B3869E306 (void);
// 0x0000077E System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetFilePath(System.String,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation)
extern void MediaPlayer_GetFilePath_mB6F36FF9EAAB948837D93821086E2FAAE86A4312 (void);
// 0x0000077F System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformVideoApiString()
extern void MediaPlayer_GetPlatformVideoApiString_m5EF740D370B119FFCB15A4587857AA949A65FA45 (void);
// 0x00000780 System.Int64 RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformFileOffset()
extern void MediaPlayer_GetPlatformFileOffset_mF5C5FFDCA07EB8F9654796EC6BBB403DF744CAC5 (void);
// 0x00000781 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformHttpHeaderJson()
extern void MediaPlayer_GetPlatformHttpHeaderJson_mDB1709D38721AA79E78D4DD97DDABC0457A42BB9 (void);
// 0x00000782 System.String RenderHeads.Media.AVProVideo.MediaPlayer::GetPlatformFilePath(RenderHeads.Media.AVProVideo.Platform,System.String&,RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation&)
extern void MediaPlayer_GetPlatformFilePath_m83BDED03DFFCF7589DBD8AB1ED589C3588297D9C (void);
// 0x00000783 RenderHeads.Media.AVProVideo.BaseMediaPlayer RenderHeads.Media.AVProVideo.MediaPlayer::CreatePlatformMediaPlayer()
extern void MediaPlayer_CreatePlatformMediaPlayer_m944F7C6C6CBB983AE00F37BCC01D7406FF5565D0 (void);
// 0x00000784 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::ForceWaitForNewFrame(System.Int32,System.Single)
extern void MediaPlayer_ForceWaitForNewFrame_mAC2324027286B166003C076538393B4BD596176F (void);
// 0x00000785 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateAudioFocus()
extern void MediaPlayer_UpdateAudioFocus_mCB989D0794C9B95C324B56FD022E84EB365D9B03 (void);
// 0x00000786 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateAudioHeadTransform()
extern void MediaPlayer_UpdateAudioHeadTransform_m0BB617E534FE76BFBD82D01ADB7ECAEB9D4C2632 (void);
// 0x00000787 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateErrors()
extern void MediaPlayer_UpdateErrors_mE4B8A878D2788E1E7C9C1ACCFA44D56E34FE5E3E (void);
// 0x00000788 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::UpdateEvents()
extern void MediaPlayer_UpdateEvents_m50151BA7A595CA1F79DE9D3CBA873CD1AA3DCD50 (void);
// 0x00000789 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::IsHandleEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType)
extern void MediaPlayer_IsHandleEvent_m7B9626A1AD683F5B668CCBF54C73B9CD536F77BC (void);
// 0x0000078A System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::FireEventIfPossible(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,System.Boolean)
extern void MediaPlayer_FireEventIfPossible_mAF2739727EC4B7EF7369B6C33BC72C9987FC1C33 (void);
// 0x0000078B System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer::CanFireEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,System.Boolean)
extern void MediaPlayer_CanFireEvent_m4A853CD3B2DFFB1397A41EBD832A0006AD8C2594 (void);
// 0x0000078C System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationFocus(System.Boolean)
extern void MediaPlayer_OnApplicationFocus_mC2EE8C5FA60A9D479B1403A7D1915D3894F1A238 (void);
// 0x0000078D System.Void RenderHeads.Media.AVProVideo.MediaPlayer::OnApplicationPause(System.Boolean)
extern void MediaPlayer_OnApplicationPause_mAF10B4F42272741F5318794AD4042E01692C8DCA (void);
// 0x0000078E UnityEngine.Camera RenderHeads.Media.AVProVideo.MediaPlayer::GetDummyCamera()
extern void MediaPlayer_GetDummyCamera_m34919C27E907E4965F535B45314BEE3E97BBB707 (void);
// 0x0000078F System.Collections.IEnumerator RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrameCoroutine(UnityEngine.Texture2D,RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrameCoroutine_m753D87BA7EEC8FB35B19A3B3BCD0CD8B094D5ADB (void);
// 0x00000790 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrameAsync(UnityEngine.Texture2D,RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrameAsync_mAFC39B40F066960CE802DA0988C830DAEEEC0FE5 (void);
// 0x00000791 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrame(UnityEngine.Texture2D,System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrame_mBF7C7650151D9549A9F680B70EF80205C3C19BBA (void);
// 0x00000792 UnityEngine.Texture RenderHeads.Media.AVProVideo.MediaPlayer::ExtractFrame(System.Single,System.Boolean,System.Int32,System.Int32)
extern void MediaPlayer_ExtractFrame_mF22A5879E2BDCC4509A1D1219D5BAF6EBF80EC74 (void);
// 0x00000793 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::.ctor()
extern void MediaPlayer__ctor_mEA0B8EE8782727537BAA83041F31E886AE7A68F8 (void);
// 0x00000794 System.Void RenderHeads.Media.AVProVideo.MediaPlayer::.cctor()
extern void MediaPlayer__cctor_mD9BDEB0FD8D2CFCD5211CF04AA64CC023BC950A6 (void);
// 0x00000795 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/Setup::.ctor()
extern void Setup__ctor_m2DD91DC7933F9E014D61CDD6E9C3F9E884F9E061 (void);
// 0x00000796 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::IsModified()
extern void PlatformOptions_IsModified_m71C51B232FA4BD4F26E2128AC47A3399BC9A6B33 (void);
// 0x00000797 System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerURL()
extern void PlatformOptions_GetKeyServerURL_m4707689FF84155F90F0BCB978672DF512D5CFC2B (void);
// 0x00000798 System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetKeyServerAuthToken()
extern void PlatformOptions_GetKeyServerAuthToken_m3E67CC2EEC1E45DAD52BAA67AA7B06EA2960191A (void);
// 0x00000799 System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::GetDecryptionKey()
extern void PlatformOptions_GetDecryptionKey_mB1B68C3D50435FB3C52CA657FE0B9DADECE28ABA (void);
// 0x0000079A System.String RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::StringAsJsonString(System.String)
extern void PlatformOptions_StringAsJsonString_mBA55018151E7D4D86B209D3781F51289997AF4F4 (void);
// 0x0000079B System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader> RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::ParseJsonHTTPHeadersIntoHTTPHeaderList(System.String)
extern void PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_mD8B18ABB2BFF9E4F5059E83F0062155CCFC4669E (void);
// 0x0000079C System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions::.ctor()
extern void PlatformOptions__ctor_mF91EEB8BD3ACDD8C1C8D5E3FFE2D64B8320CF778 (void);
// 0x0000079D System.Void RenderHeads.Media.AVProVideo.MediaPlayer/PlatformOptions/HTTPHeader::.ctor(System.String,System.String)
extern void HTTPHeader__ctor_mA1B6C1144262977F342F0E56019D842A4253C638 (void);
// 0x0000079E System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::IsModified()
extern void OptionsWindows_IsModified_mDAEA9CB1817C744E9DCBE74B0BC8BD5BFD14FC53 (void);
// 0x0000079F System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindows::.ctor()
extern void OptionsWindows__ctor_m2E1577E0C22C717A9D6FF6ED394DB042D3293EAE (void);
// 0x000007A0 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetHTTPHeadersAsJSON()
extern void OptionsApple_GetHTTPHeadersAsJSON_m011DE9BDDC01267B30B8B9134F176758BCC8315A (void);
// 0x000007A1 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::IsModified()
extern void OptionsApple_IsModified_m49D70F8F37E58D8DD498E54748CF339BED533973 (void);
// 0x000007A2 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerURL()
extern void OptionsApple_GetKeyServerURL_mE02C721161486FF7610C9D359A71174D797AE100 (void);
// 0x000007A3 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetKeyServerAuthToken()
extern void OptionsApple_GetKeyServerAuthToken_m4DAE19686FD1FA3B33B083AFC4B4042EA3115C05 (void);
// 0x000007A4 System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::GetDecryptionKey()
extern void OptionsApple_GetDecryptionKey_mFE8625B7498515C5694FBC205DAEE4FAA2F1DD8E (void);
// 0x000007A5 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnBeforeSerialize()
extern void OptionsApple_OnBeforeSerialize_m6748B4B5D209AEFB8FC4247FC4AAFBD0C9AE43CF (void);
// 0x000007A6 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::OnAfterDeserialize()
extern void OptionsApple_OnAfterDeserialize_mF63E8E76CDD82A22CA90CD166C916E5FD9D0E001 (void);
// 0x000007A7 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsApple::.ctor()
extern void OptionsApple__ctor_m73BE9C207C01BA66D9D3F5B03C6E8429E8419A4D (void);
// 0x000007A8 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsMacOSX::.ctor()
extern void OptionsMacOSX__ctor_m1014A2B088839E88607252E8C6E7E1B555B9A24E (void);
// 0x000007A9 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::IsModified()
extern void OptionsIOS_IsModified_m92628B29EF881A6881A5BDC3BE0646D80C7E5C81 (void);
// 0x000007AA System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsIOS::.ctor()
extern void OptionsIOS__ctor_m35ED509D6724A6999C186E40CAC9611A9A542297 (void);
// 0x000007AB System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsTVOS::.ctor()
extern void OptionsTVOS__ctor_m25AE150249DC2EB468D63C69494194D53CE3A419 (void);
// 0x000007AC System.String RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::GetHTTPHeadersAsJSON()
extern void OptionsAndroid_GetHTTPHeadersAsJSON_m4F395FEEDA90E4EF61104DD370B3455AA989617F (void);
// 0x000007AD System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::IsModified()
extern void OptionsAndroid_IsModified_mE87FA7C79CDC62FB4B21ABD7B40639AFED2D4B9E (void);
// 0x000007AE System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnBeforeSerialize()
extern void OptionsAndroid_OnBeforeSerialize_mBBAD61956D439415F29E77F553F439A3A2FF3758 (void);
// 0x000007AF System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::OnAfterDeserialize()
extern void OptionsAndroid_OnAfterDeserialize_mF2BE9837CDB7B2954E9B8B38CB4CAEAC6D5F4D78 (void);
// 0x000007B0 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsAndroid::.ctor()
extern void OptionsAndroid__ctor_m928A6791182AF8083CA560AF4EA43580CD7D6921 (void);
// 0x000007B1 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::IsModified()
extern void OptionsWindowsPhone_IsModified_mCF06D8E87EC7E282F72E185201BB6D6398DCD0FB (void);
// 0x000007B2 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsPhone::.ctor()
extern void OptionsWindowsPhone__ctor_m40497DE961E324A4C54E7E2DA2BC7F6D1EEF49E0 (void);
// 0x000007B3 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::IsModified()
extern void OptionsWindowsUWP_IsModified_mD1B24222D21BB71742D025747AC2BCD35D092CCA (void);
// 0x000007B4 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWindowsUWP::.ctor()
extern void OptionsWindowsUWP__ctor_m25F9B0B015AA4552C41CCA9B7FCEA92E12277623 (void);
// 0x000007B5 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::IsModified()
extern void OptionsWebGL_IsModified_mE9612729CCD42B4AB1B3240EF0706A9DC955851B (void);
// 0x000007B6 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsWebGL::.ctor()
extern void OptionsWebGL__ctor_mD167207B291900B8BDEEE7478203783CBF1409A9 (void);
// 0x000007B7 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/OptionsPS4::.ctor()
extern void OptionsPS4__ctor_m5FCC4BF70D296EA41734D313CC8081052158F83F (void);
// 0x000007B8 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::.ctor(System.Object,System.IntPtr)
extern void ProcessExtractedFrame__ctor_mDEA4881A022C1FB4FAA06AE958EB08D21D811109 (void);
// 0x000007B9 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::Invoke(UnityEngine.Texture2D)
extern void ProcessExtractedFrame_Invoke_mE91683088863043AEABFD0ABAE4D1B68EF606D2E (void);
// 0x000007BA System.IAsyncResult RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::BeginInvoke(UnityEngine.Texture2D,System.AsyncCallback,System.Object)
extern void ProcessExtractedFrame_BeginInvoke_mEC4DB2F3EF07BFFC7940E451FA628B87FC4438D7 (void);
// 0x000007BB System.Void RenderHeads.Media.AVProVideo.MediaPlayer/ProcessExtractedFrame::EndInvoke(System.IAsyncResult)
extern void ProcessExtractedFrame_EndInvoke_mD784C44DD4F9C1B5E634A4643127B5B22850C3D4 (void);
// 0x000007BC System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::.ctor(System.Int32)
extern void U3CLoadSubtitlesCoroutineU3Ed__166__ctor_m3BA5CF390664C02F54BAE83EE3AD1894453FCABE (void);
// 0x000007BD System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.IDisposable.Dispose()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7C6143DF57A1AA20F88AFCA1FEA975830612BA18 (void);
// 0x000007BE System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::MoveNext()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_MoveNext_mA91A20EBBEE60FA01B42B5AFD2EB0A643C45DD57 (void);
// 0x000007BF System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m870B6EA79DC2C40F77AB31033DE628C6026EC048 (void);
// 0x000007C0 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.Reset()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_mFBED6405DCDF1AD12602131C68CF0E05115BF583 (void);
// 0x000007C1 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<LoadSubtitlesCoroutine>d__166::System.Collections.IEnumerator.get_Current()
extern void U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m18D39C4AB130195E8C8551EF9B6E42575C587210 (void);
// 0x000007C2 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::.ctor(System.Int32)
extern void U3CFinalRenderCaptureU3Ed__188__ctor_m83AEB76C2A2B5F62EF60A95F1F876F0E30A870DB (void);
// 0x000007C3 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.IDisposable.Dispose()
extern void U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m169F9FB9630C9B699CA8E558353DC0133E1D4846 (void);
// 0x000007C4 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::MoveNext()
extern void U3CFinalRenderCaptureU3Ed__188_MoveNext_m5A26C83C7146D00BAABB7FDD64756432853E6587 (void);
// 0x000007C5 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36D557AD55F693C628CA2C0013C8638AD245E0B0 (void);
// 0x000007C6 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.Reset()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m85561D95C6DA29995D900F1CD05EE916ADBB5BBC (void);
// 0x000007C7 System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<FinalRenderCapture>d__188::System.Collections.IEnumerator.get_Current()
extern void U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m91F1095E060AB713424B739E24D53791659C0C21 (void);
// 0x000007C8 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::.ctor(System.Int32)
extern void U3CExtractFrameCoroutineU3Ed__209__ctor_mA11B8601F5A169D372F306455057C7B2D907D040 (void);
// 0x000007C9 System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.IDisposable.Dispose()
extern void U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m8C7D508575FE7B7BFEF701CC14E48810F84E87A7 (void);
// 0x000007CA System.Boolean RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::MoveNext()
extern void U3CExtractFrameCoroutineU3Ed__209_MoveNext_m77BB9A547A71459D4145517F51544D2BBAE0E2CE (void);
// 0x000007CB System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3C2D5F368AFFE770F3644C92EB05991CB4AAECF (void);
// 0x000007CC System.Void RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.Reset()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m36A003F3565EF812B375B183BDBAFBC032E7A63E (void);
// 0x000007CD System.Object RenderHeads.Media.AVProVideo.MediaPlayer/<ExtractFrameCoroutine>d__209::System.Collections.IEnumerator.get_Current()
extern void U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_m2F5AD6BB6BC4507304D46E6CC65AC53D9A1A2739 (void);
// 0x000007CE System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem> RenderHeads.Media.AVProVideo.MediaPlaylist::get_Items()
extern void MediaPlaylist_get_Items_mD2A5662FB9ACA992687BFD278CCC3046B688C649 (void);
// 0x000007CF System.Boolean RenderHeads.Media.AVProVideo.MediaPlaylist::HasItemAt(System.Int32)
extern void MediaPlaylist_HasItemAt_m7576A76184605ABC57C833A18C9E5FF673F8CF59 (void);
// 0x000007D0 System.Void RenderHeads.Media.AVProVideo.MediaPlaylist::.ctor()
extern void MediaPlaylist__ctor_m9C7ED936C5544B6BC82C2D392D5522C9B272592C (void);
// 0x000007D1 System.Void RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem::.ctor()
extern void MediaItem__ctor_mE27521C47613D523F763838EA55107E722A58CFC (void);
// 0x000007D2 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_CurrentPlayer()
extern void PlaylistMediaPlayer_get_CurrentPlayer_m6A41483F6DB31986090E5110093BF50A6200F159 (void);
// 0x000007D3 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_NextPlayer()
extern void PlaylistMediaPlayer_get_NextPlayer_m14BE7712C77CB964ACD5D3601D2ADF40C392F549 (void);
// 0x000007D4 RenderHeads.Media.AVProVideo.MediaPlaylist RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Playlist()
extern void PlaylistMediaPlayer_get_Playlist_m8718E4C723952CE65990D7075FE3B8DB22FE6E68 (void);
// 0x000007D5 System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_PlaylistIndex()
extern void PlaylistMediaPlayer_get_PlaylistIndex_m9534F26937A29B1D0906DC88987A10096AA94156 (void);
// 0x000007D6 RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_PlaylistItem()
extern void PlaylistMediaPlayer_get_PlaylistItem_mE9C2E47E38EBA2AAE0892AF32D974BB44DDD85F4 (void);
// 0x000007D7 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_LoopMode()
extern void PlaylistMediaPlayer_get_LoopMode_m4F26EDC7E0163F05C7F19328DC5C170E70783503 (void);
// 0x000007D8 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::set_LoopMode(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/PlaylistLoopMode)
extern void PlaylistMediaPlayer_set_LoopMode_m6C617F2E4BAB35FDA9AE24C661AC701F575C6C56 (void);
// 0x000007D9 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_AutoProgress()
extern void PlaylistMediaPlayer_get_AutoProgress_m00E75AC847D3B435AA4F131A83BD97643816D393 (void);
// 0x000007DA System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::set_AutoProgress(System.Boolean)
extern void PlaylistMediaPlayer_set_AutoProgress_m5839E3AF4D61C2FEC681187E1D6E573521A2F310 (void);
// 0x000007DB RenderHeads.Media.AVProVideo.IMediaInfo RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Info()
extern void PlaylistMediaPlayer_get_Info_m8861FFDCABB53D1B116E1166B2F7A66BA615E2DF (void);
// 0x000007DC RenderHeads.Media.AVProVideo.IMediaControl RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_Control()
extern void PlaylistMediaPlayer_get_Control_mB2433EDC44C5FF8919CAE207E807A723C8498872 (void);
// 0x000007DD RenderHeads.Media.AVProVideo.IMediaProducer RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::get_TextureProducer()
extern void PlaylistMediaPlayer_get_TextureProducer_mE7E07EB5BFF6D22C40694BE74158BA038C8AC353 (void);
// 0x000007DE System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SwapPlayers()
extern void PlaylistMediaPlayer_SwapPlayers_m6AA34F0D90C14E76CF2088C3E8161AE4563499D2 (void);
// 0x000007DF UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetCurrentTexture()
extern void PlaylistMediaPlayer_GetCurrentTexture_m00FF396CBAD27D08E1718EE7DF044D5756F7D007 (void);
// 0x000007E0 UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetNextTexture()
extern void PlaylistMediaPlayer_GetNextTexture_m1D779DFC4F971548B55FE030C9E5A25D93E68C49 (void);
// 0x000007E1 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Awake()
extern void PlaylistMediaPlayer_Awake_m6F70FB582484067750B4E136EC28C4C3D100F1CB (void);
// 0x000007E2 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OnDestroy()
extern void PlaylistMediaPlayer_OnDestroy_mE90A892567B8F8FDEED6CDB5D8C61C02FE0D6D67 (void);
// 0x000007E3 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Start()
extern void PlaylistMediaPlayer_Start_m9FAA02764BEDCF0517A4F00F8604191500ADB209 (void);
// 0x000007E4 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void PlaylistMediaPlayer_OnVideoEvent_m993B9C42D3D6997D97CA7DA2720F9474C297C3BC (void);
// 0x000007E5 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::PrevItem()
extern void PlaylistMediaPlayer_PrevItem_mAB331FFAAF8C25DBFD21A80426B228F8C46D3A17 (void);
// 0x000007E6 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::NextItem()
extern void PlaylistMediaPlayer_NextItem_m20F76EEBBF8E14B4AA1DCB425070D6C8D1D171AD (void);
// 0x000007E7 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::CanJumpToItem(System.Int32)
extern void PlaylistMediaPlayer_CanJumpToItem_m9543998971993E852240D1660A3749735105A900 (void);
// 0x000007E8 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::JumpToItem(System.Int32)
extern void PlaylistMediaPlayer_JumpToItem_mD4B299AC55241F56D56B252723D1F969536110F3 (void);
// 0x000007E9 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::OpenVideoFile(RenderHeads.Media.AVProVideo.MediaPlaylist/MediaItem)
extern void PlaylistMediaPlayer_OpenVideoFile_m73586FE7E3BABCCFCB64F747AEC6EDB43979988C (void);
// 0x000007EA System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::IsTransitioning()
extern void PlaylistMediaPlayer_IsTransitioning_m1AE32A0D518D6A243FDCE8D84B2B71F2921AB093 (void);
// 0x000007EB System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SetTransition(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition,System.Single,RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
extern void PlaylistMediaPlayer_SetTransition_mCBF924A30F232BB6AB5162C7156E35818EFAB9AA (void);
// 0x000007EC System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::Update()
extern void PlaylistMediaPlayer_Update_mE39BCA59DB824B314C15EB06169DE4F5C70CEFAF (void);
// 0x000007ED UnityEngine.Texture RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTexture(System.Int32)
extern void PlaylistMediaPlayer_GetTexture_m85B29C83F825CC1248BC4438FA2F64FFEC7A5CF2 (void);
// 0x000007EE System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureCount()
extern void PlaylistMediaPlayer_GetTextureCount_m1BF08422B9047D3B82BB2509934C98698BF2EB71 (void);
// 0x000007EF System.Int32 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureFrameCount()
extern void PlaylistMediaPlayer_GetTextureFrameCount_mE125507DCF3AFCAF443C82815D326C03D8D84432 (void);
// 0x000007F0 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::SupportsTextureFrameCount()
extern void PlaylistMediaPlayer_SupportsTextureFrameCount_m4C7E4D445D5C1DE3ADDE7799FEF2400D1B066F61 (void);
// 0x000007F1 System.Int64 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTextureTimeStamp()
extern void PlaylistMediaPlayer_GetTextureTimeStamp_mE10D738CDFB09D11EEBEC65554EDBBBCD9E537C4 (void);
// 0x000007F2 System.Boolean RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::RequiresVerticalFlip()
extern void PlaylistMediaPlayer_RequiresVerticalFlip_m2670C70EEBBCBBD7CCB1B2C48721BBD22C12CAA4 (void);
// 0x000007F3 UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetYpCbCrTransform()
extern void PlaylistMediaPlayer_GetYpCbCrTransform_m9B3F18D5361A8090FD820ADCECB4C8B6F341D89E (void);
// 0x000007F4 System.String RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::GetTransitionName(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Transition)
extern void PlaylistMediaPlayer_GetTransitionName_m55D1F1A8F22AA0E02ED97C06B3997F716E54C2B5 (void);
// 0x000007F5 System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer::.ctor()
extern void PlaylistMediaPlayer__ctor_mCD990CF574CF2B4664FFDD29F64F4E5DBC9F76A6 (void);
// 0x000007F6 System.Func`2<System.Single,System.Single> RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::GetFunction(RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing/Preset)
extern void Easing_GetFunction_m0A3A998B12B19042FEC59052821830FA5A244FB0 (void);
// 0x000007F7 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseIn(System.Single,System.Single)
extern void Easing_PowerEaseIn_m12E2B5F13FAC43680CDE866416B9F865AB408752 (void);
// 0x000007F8 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseOut(System.Single,System.Single)
extern void Easing_PowerEaseOut_mC47935AAD5B0A356D751ADEE8771F7A3E3B3426E (void);
// 0x000007F9 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::PowerEaseInOut(System.Single,System.Single)
extern void Easing_PowerEaseInOut_mB11B4206841BB35B6C7FB19AC1ADC6D2F32663E2 (void);
// 0x000007FA System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Step(System.Single)
extern void Easing_Step_m8EC854B0DA4F982CE742CD89D61FF5CD8CD3A6B5 (void);
// 0x000007FB System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::Linear(System.Single)
extern void Easing_Linear_m0F7845F63905AF3DEDD3E0874FF4EBD50F5F9C5B (void);
// 0x000007FC System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuad(System.Single)
extern void Easing_InQuad_m0910CE9C6A4DEC3CC2D488716CE1DE81AE0E8D94 (void);
// 0x000007FD System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuad(System.Single)
extern void Easing_OutQuad_m4EFCE47F8757465279E674493A385FE0A85D8FA7 (void);
// 0x000007FE System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuad(System.Single)
extern void Easing_InOutQuad_m84C8A0A99A683B57A40380D847CCDD210BE6A6FB (void);
// 0x000007FF System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InCubic(System.Single)
extern void Easing_InCubic_m3882BD96AEF88F265A14C50B0E0065620FB337C3 (void);
// 0x00000800 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutCubic(System.Single)
extern void Easing_OutCubic_m06A435FE7E0BF4A082D4248E81B619ADEEDAF621 (void);
// 0x00000801 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutCubic(System.Single)
extern void Easing_InOutCubic_mD85410418A70EB023B08AC363617CE29D3C18818 (void);
// 0x00000802 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuart(System.Single)
extern void Easing_InQuart_mCD5C6C2A5EB808E0C2745B2A0109C3651FF7FF07 (void);
// 0x00000803 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuart(System.Single)
extern void Easing_OutQuart_mAA2813D82B15C85F42628F01C29F4EE798811C84 (void);
// 0x00000804 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuart(System.Single)
extern void Easing_InOutQuart_mBF80AD6F229C65D98105C6BE124E06CA0F5DACDF (void);
// 0x00000805 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InQuint(System.Single)
extern void Easing_InQuint_m96144186DB2B204C0BD4C2D3371BE0EE15D302BE (void);
// 0x00000806 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutQuint(System.Single)
extern void Easing_OutQuint_mA61B78C42D6C2A54D6AE6517F8C4E402829B02B2 (void);
// 0x00000807 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutQuint(System.Single)
extern void Easing_InOutQuint_mFA82331F78A14A0147DA221645351A996CA5FFC6 (void);
// 0x00000808 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InExpo(System.Single)
extern void Easing_InExpo_m26C2FEBD5602614B836585ABDE6EBA6AC941FEF1 (void);
// 0x00000809 System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::OutExpo(System.Single)
extern void Easing_OutExpo_m792D836881D4844DAFE638E40B9F49E72F152013 (void);
// 0x0000080A System.Single RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::InOutExpo(System.Single)
extern void Easing_InOutExpo_m3C31A5337EC51EA2B21ED36136F0DA47AA7AD736 (void);
// 0x0000080B System.Void RenderHeads.Media.AVProVideo.PlaylistMediaPlayer/Easing::.ctor()
extern void Easing__ctor_m69F5D01C5C4E7801ECC79823E327068E777E9E35 (void);
// 0x0000080C System.Void RenderHeads.Media.AVProVideo.StreamParserEvent::.ctor()
extern void StreamParserEvent__ctor_m1AB02AA693ABF9BED4483AA27AF35536356E9D99 (void);
// 0x0000080D RenderHeads.Media.AVProVideo.StreamParserEvent RenderHeads.Media.AVProVideo.StreamParser::get_Events()
extern void StreamParser_get_Events_m82D43156A8A018AD68844A707E56914E54D95DC2 (void);
// 0x0000080E System.Void RenderHeads.Media.AVProVideo.StreamParser::LoadFile()
extern void StreamParser_LoadFile_mA4778595E7AE78BB3BEF70F20BCD20CC973D4D8D (void);
// 0x0000080F System.Boolean RenderHeads.Media.AVProVideo.StreamParser::get_Loaded()
extern void StreamParser_get_Loaded_m67AD40A7AD72ACD95A4D6853E57022655CA0FF0C (void);
// 0x00000810 RenderHeads.Media.AVProVideo.Stream RenderHeads.Media.AVProVideo.StreamParser::get_Root()
extern void StreamParser_get_Root_mA022BE599C672DD272842F6D1BDEFA0DB5F3FA50 (void);
// 0x00000811 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.StreamParser::get_SubStreams()
extern void StreamParser_get_SubStreams_m7D4A5FC7B2777866C606E46655213C8E768CDFBC (void);
// 0x00000812 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.StreamParser::get_Chunks()
extern void StreamParser_get_Chunks_mF01316B8D9CB08BC2D7A4C564B173BCAC14AEBDE (void);
// 0x00000813 System.Void RenderHeads.Media.AVProVideo.StreamParser::ParseStream()
extern void StreamParser_ParseStream_m921884BC1A514287242F9CE087ECCDD96B5321ED (void);
// 0x00000814 System.Void RenderHeads.Media.AVProVideo.StreamParser::Start()
extern void StreamParser_Start_m7743BC53818D9DF8E63AEA4B8361C6946E341132 (void);
// 0x00000815 System.Void RenderHeads.Media.AVProVideo.StreamParser::.ctor()
extern void StreamParser__ctor_m6707C579E3361430F9B4580985240B5D7C4113D1 (void);
// 0x00000816 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::Start()
extern void SubtitlesUGUI_Start_m81020E68C72F44C7F365119C104CA5AC6D069C83 (void);
// 0x00000817 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::OnDestroy()
extern void SubtitlesUGUI_OnDestroy_mBB3C4F5C149E76F98FE0ADB2E9076B9CC9316BFA (void);
// 0x00000818 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::ChangeMediaPlayer(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SubtitlesUGUI_ChangeMediaPlayer_mFFF8749831CDB17D89AB552558A74B006C6DC27A (void);
// 0x00000819 System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void SubtitlesUGUI_OnMediaPlayerEvent_m45912EB24F60297AB0D940A3D4C612F0F976C6BD (void);
// 0x0000081A System.Void RenderHeads.Media.AVProVideo.SubtitlesUGUI::.ctor()
extern void SubtitlesUGUI__ctor_m09F63FA6D7B6DBADFEDBC9931A68C94BFFD69034 (void);
// 0x0000081B RenderHeads.Media.AVProVideo.StereoEye RenderHeads.Media.AVProVideo.UpdateStereoMaterial::get_ForceEyeMode()
extern void UpdateStereoMaterial_get_ForceEyeMode_m3313BC2D25C21E7E3FE4C0D7CB9E96BD9D467236 (void);
// 0x0000081C System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::set_ForceEyeMode(RenderHeads.Media.AVProVideo.StereoEye)
extern void UpdateStereoMaterial_set_ForceEyeMode_m6A3722F8E86AD1584278ACAF0B56F97BD028696B (void);
// 0x0000081D System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::Awake()
extern void UpdateStereoMaterial_Awake_m5C7BC1FC7C350AD68BEA4F48E1F856F9555B5A4D (void);
// 0x0000081E System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::SetupMaterial(UnityEngine.Material,UnityEngine.Camera)
extern void UpdateStereoMaterial_SetupMaterial_mE9ABE9B3D62B30F7B85E70D2063D9B7A09D9EA6C (void);
// 0x0000081F System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::LateUpdate()
extern void UpdateStereoMaterial_LateUpdate_mE13E0CA32652B0833042CA16EA17CD26B355C4A6 (void);
// 0x00000820 System.Void RenderHeads.Media.AVProVideo.UpdateStereoMaterial::.ctor()
extern void UpdateStereoMaterial__ctor_m3B5DAF7BD246113BB67579975E2F0FCB257EB5E9 (void);
// 0x00000821 RenderHeads.Media.AVProVideo.AudioOutputManager RenderHeads.Media.AVProVideo.AudioOutputManager::get_Instance()
extern void AudioOutputManager_get_Instance_m21A0B52D43DD53D989089023AAC34C307580BE62 (void);
// 0x00000822 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::.ctor()
extern void AudioOutputManager__ctor_m12F37DB8F5B1DF79399DFDD975EBC86A0A31FBBE (void);
// 0x00000823 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::RequestAudio(RenderHeads.Media.AVProVideo.AudioOutput,RenderHeads.Media.AVProVideo.MediaPlayer,System.Single[],System.Int32,System.Int32,RenderHeads.Media.AVProVideo.AudioOutput/AudioOutputMode)
extern void AudioOutputManager_RequestAudio_m95F63CEA8221E9EA65686941AEE7809DDE860EE2 (void);
// 0x00000824 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::GrabAudio(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single[],System.Int32)
extern void AudioOutputManager_GrabAudio_m546B648A8930F6AC77D910ABE65764BB49E783A4 (void);
// 0x00000825 System.Void RenderHeads.Media.AVProVideo.AudioOutputManager::.cctor()
extern void AudioOutputManager__cctor_m820C810B05A2150FF714519A8710A157037CF6B2 (void);
// 0x00000826 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVersion()
// 0x00000827 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
// 0x00000828 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::OpenVideoFromBuffer(System.Byte[])
extern void BaseMediaPlayer_OpenVideoFromBuffer_mC593EB17CC16F086B7529871FBF82FD5C3DDF01E (void);
// 0x00000829 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::StartOpenVideoFromBuffer(System.UInt64)
extern void BaseMediaPlayer_StartOpenVideoFromBuffer_m1CE803B3BA190E946AC5B6521DF01599889E0495 (void);
// 0x0000082A System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
extern void BaseMediaPlayer_AddChunkToVideoBuffer_m70424D78C585DF8AB4424CAFB3C991F7EC5785B3 (void);
// 0x0000082B System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::EndOpenVideoFromBuffer()
extern void BaseMediaPlayer_EndOpenVideoFromBuffer_m6E9468D3BEBC1563C9D1854974E95E98ABF50AFF (void);
// 0x0000082C System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::CloseVideo()
extern void BaseMediaPlayer_CloseVideo_m8A46A0EFF2937C84C37B84D2CE96B83286BAB255 (void);
// 0x0000082D System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetLooping(System.Boolean)
// 0x0000082E System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsLooping()
// 0x0000082F System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasMetaData()
// 0x00000830 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::CanPlay()
// 0x00000831 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Play()
// 0x00000832 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Pause()
// 0x00000833 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Stop()
// 0x00000834 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Rewind()
extern void BaseMediaPlayer_Rewind_m21BD0FC7F5E14F57AB8E1ADA8B14203CBD4301F2 (void);
// 0x00000835 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Seek(System.Single)
// 0x00000836 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SeekFast(System.Single)
// 0x00000837 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SeekWithTolerance(System.Single,System.Single,System.Single)
extern void BaseMediaPlayer_SeekWithTolerance_m5187D00A5EFCF2C04432484B54D3009F02A98245 (void);
// 0x00000838 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentTimeMs()
// 0x00000839 System.Double RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentDateTimeSecondsSince1970()
extern void BaseMediaPlayer_GetCurrentDateTimeSecondsSince1970_mB71250974B99A7087F39A1A0B5B0903C3E37225E (void);
// 0x0000083A RenderHeads.Media.AVProVideo.TimeRange[] RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSeekableTimeRanges()
extern void BaseMediaPlayer_GetSeekableTimeRanges_m1458191D20E86DCD6BABAA2C3E038695E99857F7 (void);
// 0x0000083B System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetPlaybackRate()
// 0x0000083C System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetPlaybackRate(System.Single)
// 0x0000083D System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetDurationMs()
// 0x0000083E System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoWidth()
// 0x0000083F System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoHeight()
// 0x00000840 UnityEngine.Rect RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCropRect()
extern void BaseMediaPlayer_GetCropRect_m9D43A4E5CC9F5E93EA543095D6D2CF37E10F18E1 (void);
// 0x00000841 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoDisplayRate()
// 0x00000842 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasAudio()
// 0x00000843 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::HasVideo()
// 0x00000844 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsSeeking()
// 0x00000845 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPlaying()
// 0x00000846 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPaused()
// 0x00000847 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsFinished()
// 0x00000848 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsBuffering()
// 0x00000849 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::WaitForNextFrame(UnityEngine.Camera,System.Int32)
extern void BaseMediaPlayer_WaitForNextFrame_mAF4E01AF17AA338A80826E916CBCD72A3012BBBD (void);
// 0x0000084A System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetPlayWithoutBuffering(System.Boolean)
extern void BaseMediaPlayer_SetPlayWithoutBuffering_m84657EABD2A7F681E41DA921053EF9F7E5A32FE0 (void);
// 0x0000084B System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetKeyServerURL(System.String)
extern void BaseMediaPlayer_SetKeyServerURL_m68329F6EB28C2D3D4DDF9FF6C6BE2DD77888085E (void);
// 0x0000084C System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetKeyServerAuthToken(System.String)
extern void BaseMediaPlayer_SetKeyServerAuthToken_m7B67176FF00D0AEF2E75D08FF48912CAE153B200 (void);
// 0x0000084D System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetDecryptionKeyBase64(System.String)
extern void BaseMediaPlayer_SetDecryptionKeyBase64_m2965B2C1792CC50902D0FA8C1258F98059343CE3 (void);
// 0x0000084E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetDecryptionKey(System.Byte[])
extern void BaseMediaPlayer_SetDecryptionKey_mD824E456CB891A3791026B94E040725A77501306 (void);
// 0x0000084F System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureCount()
extern void BaseMediaPlayer_GetTextureCount_m3163275E7B317107EAC6D953E7C1D0382CEF378A (void);
// 0x00000850 UnityEngine.Texture RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTexture(System.Int32)
// 0x00000851 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureFrameCount()
// 0x00000852 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::SupportsTextureFrameCount()
extern void BaseMediaPlayer_SupportsTextureFrameCount_m622928C95F61D05684B3F6B9925ECC930C9F2E8A (void);
// 0x00000853 System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureTimeStamp()
extern void BaseMediaPlayer_GetTextureTimeStamp_m676503683CF7AA0077A0276837801B3FC91BFFC4 (void);
// 0x00000854 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::RequiresVerticalFlip()
// 0x00000855 System.Single[] RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetTextureTransform()
extern void BaseMediaPlayer_GetTextureTransform_m4A6ED13D46A85CA3D313E8FAC84B44C52097ADE7 (void);
// 0x00000856 UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetYpCbCrTransform()
extern void BaseMediaPlayer_GetYpCbCrTransform_mA98D5704137DCC314ABFC4D18B4E7A1AB295220B (void);
// 0x00000857 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::MuteAudio(System.Boolean)
// 0x00000858 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsMuted()
// 0x00000859 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetVolume(System.Single)
// 0x0000085A System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetBalance(System.Single)
extern void BaseMediaPlayer_SetBalance_m4ACBBCC574A73F920ACD4D79BCB579EF416F0508 (void);
// 0x0000085B System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVolume()
// 0x0000085C System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBalance()
extern void BaseMediaPlayer_GetBalance_m68AD9ACF463F71B6F5D6D3A3A70C65B2ECC5F9B3 (void);
// 0x0000085D System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetAudioTrackCount()
// 0x0000085E System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetAudioTrackId(System.Int32)
extern void BaseMediaPlayer_GetAudioTrackId_m91237F46ED4BDB69C8EA0263687C609ED5434640 (void);
// 0x0000085F System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrack()
// 0x00000860 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioTrack(System.Int32)
// 0x00000861 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrackId()
// 0x00000862 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentAudioTrackBitrate()
// 0x00000863 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetNumAudioChannels()
extern void BaseMediaPlayer_GetNumAudioChannels_m038B5490B28622997B55DC335762285E9AB04E9B (void);
// 0x00000864 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioHeadRotation(UnityEngine.Quaternion)
extern void BaseMediaPlayer_SetAudioHeadRotation_mEEEF49611403C804F60732FF49F6486DF369AABD (void);
// 0x00000865 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ResetAudioHeadRotation()
extern void BaseMediaPlayer_ResetAudioHeadRotation_m6FD57161928DD898649A27ED05D18AE6113F8785 (void);
// 0x00000866 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioChannelMode(RenderHeads.Media.AVProVideo.Audio360ChannelMode)
extern void BaseMediaPlayer_SetAudioChannelMode_m8CDC213D8EE7FED6CC9FF39FF37A7800E5334DB9 (void);
// 0x00000867 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusEnabled(System.Boolean)
extern void BaseMediaPlayer_SetAudioFocusEnabled_mDD8B1A0E83E729EFE4206D45ACB66FA4D8F388C9 (void);
// 0x00000868 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusProperties(System.Single,System.Single)
extern void BaseMediaPlayer_SetAudioFocusProperties_m238FB862DFFB14FD1780476AD6AAF2CF6E025B63 (void);
// 0x00000869 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetAudioFocusRotation(UnityEngine.Quaternion)
extern void BaseMediaPlayer_SetAudioFocusRotation_m3CACBE6DA9DBE53A90B29C28709C616BB7C916E5 (void);
// 0x0000086A System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ResetAudioFocus()
extern void BaseMediaPlayer_ResetAudioFocus_mDF5A065D0C38363C05F59245E0A165F77FE4ABB3 (void);
// 0x0000086B System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoTrackCount()
// 0x0000086C System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoTrackId(System.Int32)
extern void BaseMediaPlayer_GetVideoTrackId_mCA57D735551EBEF02F6A603EFEFB02B3FD4CE77A (void);
// 0x0000086D System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrack()
// 0x0000086E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetVideoTrack(System.Int32)
// 0x0000086F System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrackId()
// 0x00000870 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetCurrentVideoTrackBitrate()
// 0x00000871 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetVideoFrameRate()
// 0x00000872 System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetEstimatedTotalBandwidthUsed()
extern void BaseMediaPlayer_GetEstimatedTotalBandwidthUsed_m37FE0EB8CEA51EFBB767DCD8283B2BFDDD5C9EB7 (void);
// 0x00000873 System.Single RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferingProgress()
// 0x00000874 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Update()
// 0x00000875 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Render()
// 0x00000876 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::Dispose()
// 0x00000877 RenderHeads.Media.AVProVideo.ErrorCode RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetLastError()
extern void BaseMediaPlayer_GetLastError_m33EC86CDFC8FA0AE2F0337610C3BF2BCE03F7DD7 (void);
// 0x00000878 System.Int64 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetLastExtendedErrorCode()
extern void BaseMediaPlayer_GetLastExtendedErrorCode_m383E605472A35AE521086941B23708F70F0589FF (void);
// 0x00000879 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetPlayerDescription()
extern void BaseMediaPlayer_GetPlayerDescription_mD051AF98C918B186134053C6152BE8AD0CBA46FE (void);
// 0x0000087A System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::PlayerSupportsLinearColorSpace()
extern void BaseMediaPlayer_PlayerSupportsLinearColorSpace_m6A1E690E4994B174A8F25568F540DA5E81C38331 (void);
// 0x0000087B System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferedTimeRangeCount()
extern void BaseMediaPlayer_GetBufferedTimeRangeCount_m2E26A5993EC0CC528461EFBBCD171276FFEC4651 (void);
// 0x0000087C System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&)
extern void BaseMediaPlayer_GetBufferedTimeRange_mEB2F52809E4685560C07155F0A1F182F5E6982F3 (void);
// 0x0000087D System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::SetTextureProperties(UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.Int32)
extern void BaseMediaPlayer_SetTextureProperties_mB51EE0AAE3C6A751C7C27E5906159AA2775DC569 (void);
// 0x0000087E System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::ApplyTextureProperties(UnityEngine.Texture)
extern void BaseMediaPlayer_ApplyTextureProperties_mA7690675AFD1C19C4364F1B4AF30AAB6FB26805D (void);
// 0x0000087F System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::GrabAudio(System.Single[],System.Int32,System.Int32)
extern void BaseMediaPlayer_GrabAudio_mDCC211A1CC2DF1581D299B889339B49264E9E2BA (void);
// 0x00000880 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsExpectingNewVideoFrame()
extern void BaseMediaPlayer_IsExpectingNewVideoFrame_mF272D2074F469EAEB4356322D6DB1DFAB8D7700D (void);
// 0x00000881 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::IsPlaybackStalled()
extern void BaseMediaPlayer_IsPlaybackStalled_mA4E8DBDFE8CF1448634321782034DEA4FF0E3DEC (void);
// 0x00000882 System.Boolean RenderHeads.Media.AVProVideo.BaseMediaPlayer::LoadSubtitlesSRT(System.String)
extern void BaseMediaPlayer_LoadSubtitlesSRT_mC662A14864D9F44643C4A5A37D53B73901C1859F (void);
// 0x00000883 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::UpdateSubtitles()
extern void BaseMediaPlayer_UpdateSubtitles_m64F8C65ADC9E604171C087D90CC928B01CD37E5B (void);
// 0x00000884 System.Int32 RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSubtitleIndex()
extern void BaseMediaPlayer_GetSubtitleIndex_mE96BE5A9DFF09753063EEDC44C09A0E2E2720C1B (void);
// 0x00000885 System.String RenderHeads.Media.AVProVideo.BaseMediaPlayer::GetSubtitleText()
extern void BaseMediaPlayer_GetSubtitleText_m221600D40D605ACCED95B6857864ACBEB132DA2C (void);
// 0x00000886 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::OnEnable()
extern void BaseMediaPlayer_OnEnable_mC94F1DF3F741AFFC55F3C4A8C5E3B7D9AEF82AE8 (void);
// 0x00000887 System.Void RenderHeads.Media.AVProVideo.BaseMediaPlayer::.ctor()
extern void BaseMediaPlayer__ctor_mDC46CC0B61C09BE8D457D48244A2A3E5E0E3B6A9 (void);
// 0x00000888 System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Width()
extern void HLSStream_get_Width_mE2C9215D2017DB5F1DD95BDE257208CC7A06BBBB (void);
// 0x00000889 System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Height()
extern void HLSStream_get_Height_mED68CB6841284E54938AA5CD620F3579AB63CB3B (void);
// 0x0000088A System.Int32 RenderHeads.Media.AVProVideo.HLSStream::get_Bandwidth()
extern void HLSStream_get_Bandwidth_m7108E1C0D4C8B56BB87551E543E8CEBEF40FED70 (void);
// 0x0000088B System.String RenderHeads.Media.AVProVideo.HLSStream::get_URL()
extern void HLSStream_get_URL_mA87D6580CFCE96996C51F2B8B002453428D6489B (void);
// 0x0000088C System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.HLSStream::GetAllChunks()
extern void HLSStream_GetAllChunks_m07F1CF2A1708C1CF63581D6CBBB52D14CFAF0AD1 (void);
// 0x0000088D System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.HLSStream::GetChunks()
extern void HLSStream_GetChunks_mE5B2DBE348285D9559FD44FA73A4F73F7FE56F4F (void);
// 0x0000088E System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.HLSStream::GetAllStreams()
extern void HLSStream_GetAllStreams_m75B7666CD2212065D0E2ED2B30FD8511567D41F5 (void);
// 0x0000088F System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.HLSStream::GetStreams()
extern void HLSStream_GetStreams_m5CA80839B74ABCCCFD1CB7BD3B46829064289A4C (void);
// 0x00000890 System.Boolean RenderHeads.Media.AVProVideo.HLSStream::ExtractStreamInfo(System.String,System.Int32&,System.Int32&,System.Int32&)
extern void HLSStream_ExtractStreamInfo_m84998280D507E38FDB70FEB34A7C231E89B81734 (void);
// 0x00000891 System.Boolean RenderHeads.Media.AVProVideo.HLSStream::IsChunk(System.String)
extern void HLSStream_IsChunk_mF58D1A3CFD8BF16B35267A4C81792845D8D15C51 (void);
// 0x00000892 System.Void RenderHeads.Media.AVProVideo.HLSStream::ParseFile(System.String[],System.String)
extern void HLSStream_ParseFile_m6E91648162A2F952FBF64FF8146D6E4BF7934345 (void);
// 0x00000893 System.Void RenderHeads.Media.AVProVideo.HLSStream::.ctor(System.String,System.Int32,System.Int32,System.Int32)
extern void HLSStream__ctor_m5DEE09F9AD646EB9BA3056970141EFBC18FD57D7 (void);
// 0x00000894 System.Boolean RenderHeads.Media.AVProVideo.MediaPlayerEvent::HasListeners()
extern void MediaPlayerEvent_HasListeners_m1D4A700D7A2F1A3DAAA1A9D2AF5FB8A2258D0944 (void);
// 0x00000895 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::AddListener(UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>)
extern void MediaPlayerEvent_AddListener_m53C4058E9896017113BC52444B1E2A2694339396 (void);
// 0x00000896 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::RemoveListener(UnityEngine.Events.UnityAction`3<RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode>)
extern void MediaPlayerEvent_RemoveListener_m98B2B41347E72C42DDA1FAF4BC9F2F8F8A3AC4D4 (void);
// 0x00000897 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::RemoveAllListeners()
extern void MediaPlayerEvent_RemoveAllListeners_mC26BC9FFB1C15B8EECF122696EC8C5BB45A34FED (void);
// 0x00000898 System.Void RenderHeads.Media.AVProVideo.MediaPlayerEvent::.ctor()
extern void MediaPlayerEvent__ctor_m00B69F6545EB57638E5D342D6146F3C9ADCCABC4 (void);
// 0x00000899 System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::OnEnable()
// 0x0000089A System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::Update()
// 0x0000089B System.Void RenderHeads.Media.AVProVideo.IMediaPlayer::Render()
// 0x0000089C System.Boolean RenderHeads.Media.AVProVideo.IMediaSubtitles::LoadSubtitlesSRT(System.String)
// 0x0000089D System.Int32 RenderHeads.Media.AVProVideo.IMediaSubtitles::GetSubtitleIndex()
// 0x0000089E System.String RenderHeads.Media.AVProVideo.IMediaSubtitles::GetSubtitleText()
// 0x0000089F System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
// 0x000008A0 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::OpenVideoFromBuffer(System.Byte[])
// 0x000008A1 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::StartOpenVideoFromBuffer(System.UInt64)
// 0x000008A2 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
// 0x000008A3 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::EndOpenVideoFromBuffer()
// 0x000008A4 System.Void RenderHeads.Media.AVProVideo.IMediaControl::CloseVideo()
// 0x000008A5 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetLooping(System.Boolean)
// 0x000008A6 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsLooping()
// 0x000008A7 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::HasMetaData()
// 0x000008A8 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::CanPlay()
// 0x000008A9 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsPlaying()
// 0x000008AA System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsSeeking()
// 0x000008AB System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsPaused()
// 0x000008AC System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsFinished()
// 0x000008AD System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsBuffering()
// 0x000008AE System.Void RenderHeads.Media.AVProVideo.IMediaControl::Play()
// 0x000008AF System.Void RenderHeads.Media.AVProVideo.IMediaControl::Pause()
// 0x000008B0 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Stop()
// 0x000008B1 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Rewind()
// 0x000008B2 System.Void RenderHeads.Media.AVProVideo.IMediaControl::Seek(System.Single)
// 0x000008B3 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SeekFast(System.Single)
// 0x000008B4 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SeekWithTolerance(System.Single,System.Single,System.Single)
// 0x000008B5 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentTimeMs()
// 0x000008B6 System.Double RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentDateTimeSecondsSince1970()
// 0x000008B7 RenderHeads.Media.AVProVideo.TimeRange[] RenderHeads.Media.AVProVideo.IMediaControl::GetSeekableTimeRanges()
// 0x000008B8 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetPlaybackRate()
// 0x000008B9 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetPlaybackRate(System.Single)
// 0x000008BA System.Void RenderHeads.Media.AVProVideo.IMediaControl::MuteAudio(System.Boolean)
// 0x000008BB System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::IsMuted()
// 0x000008BC System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVolume(System.Single)
// 0x000008BD System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetBalance(System.Single)
// 0x000008BE System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetVolume()
// 0x000008BF System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetBalance()
// 0x000008C0 System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentAudioTrack()
// 0x000008C1 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioTrack(System.Int32)
// 0x000008C2 System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetCurrentVideoTrack()
// 0x000008C3 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetVideoTrack(System.Int32)
// 0x000008C4 System.Single RenderHeads.Media.AVProVideo.IMediaControl::GetBufferingProgress()
// 0x000008C5 System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRangeCount()
// 0x000008C6 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&)
// 0x000008C7 RenderHeads.Media.AVProVideo.ErrorCode RenderHeads.Media.AVProVideo.IMediaControl::GetLastError()
// 0x000008C8 System.Int64 RenderHeads.Media.AVProVideo.IMediaControl::GetLastExtendedErrorCode()
// 0x000008C9 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetTextureProperties(UnityEngine.FilterMode,UnityEngine.TextureWrapMode,System.Int32)
// 0x000008CA System.Void RenderHeads.Media.AVProVideo.IMediaControl::GrabAudio(System.Single[],System.Int32,System.Int32)
// 0x000008CB System.Int32 RenderHeads.Media.AVProVideo.IMediaControl::GetNumAudioChannels()
// 0x000008CC System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioHeadRotation(UnityEngine.Quaternion)
// 0x000008CD System.Void RenderHeads.Media.AVProVideo.IMediaControl::ResetAudioHeadRotation()
// 0x000008CE System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioChannelMode(RenderHeads.Media.AVProVideo.Audio360ChannelMode)
// 0x000008CF System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusEnabled(System.Boolean)
// 0x000008D0 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusProperties(System.Single,System.Single)
// 0x000008D1 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetAudioFocusRotation(UnityEngine.Quaternion)
// 0x000008D2 System.Void RenderHeads.Media.AVProVideo.IMediaControl::ResetAudioFocus()
// 0x000008D3 System.Boolean RenderHeads.Media.AVProVideo.IMediaControl::WaitForNextFrame(UnityEngine.Camera,System.Int32)
// 0x000008D4 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetPlayWithoutBuffering(System.Boolean)
// 0x000008D5 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetKeyServerURL(System.String)
// 0x000008D6 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetKeyServerAuthToken(System.String)
// 0x000008D7 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetDecryptionKeyBase64(System.String)
// 0x000008D8 System.Void RenderHeads.Media.AVProVideo.IMediaControl::SetDecryptionKey(System.Byte[])
// 0x000008D9 System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetDurationMs()
// 0x000008DA System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoWidth()
// 0x000008DB System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoHeight()
// 0x000008DC UnityEngine.Rect RenderHeads.Media.AVProVideo.IMediaInfo::GetCropRect()
// 0x000008DD System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoFrameRate()
// 0x000008DE System.Single RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoDisplayRate()
// 0x000008DF System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::HasVideo()
// 0x000008E0 System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::HasAudio()
// 0x000008E1 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetAudioTrackCount()
// 0x000008E2 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetAudioTrackId(System.Int32)
// 0x000008E3 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentAudioTrackId()
// 0x000008E4 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentAudioTrackBitrate()
// 0x000008E5 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoTrackCount()
// 0x000008E6 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetVideoTrackId(System.Int32)
// 0x000008E7 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentVideoTrackId()
// 0x000008E8 System.Int32 RenderHeads.Media.AVProVideo.IMediaInfo::GetCurrentVideoTrackBitrate()
// 0x000008E9 System.String RenderHeads.Media.AVProVideo.IMediaInfo::GetPlayerDescription()
// 0x000008EA System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::PlayerSupportsLinearColorSpace()
// 0x000008EB System.Boolean RenderHeads.Media.AVProVideo.IMediaInfo::IsPlaybackStalled()
// 0x000008EC System.Single[] RenderHeads.Media.AVProVideo.IMediaInfo::GetTextureTransform()
// 0x000008ED System.Int64 RenderHeads.Media.AVProVideo.IMediaInfo::GetEstimatedTotalBandwidthUsed()
// 0x000008EE System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureCount()
// 0x000008EF UnityEngine.Texture RenderHeads.Media.AVProVideo.IMediaProducer::GetTexture(System.Int32)
// 0x000008F0 System.Int32 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureFrameCount()
// 0x000008F1 System.Boolean RenderHeads.Media.AVProVideo.IMediaProducer::SupportsTextureFrameCount()
// 0x000008F2 System.Int64 RenderHeads.Media.AVProVideo.IMediaProducer::GetTextureTimeStamp()
// 0x000008F3 System.Boolean RenderHeads.Media.AVProVideo.IMediaProducer::RequiresVerticalFlip()
// 0x000008F4 UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.IMediaProducer::GetYpCbCrTransform()
// 0x000008F5 System.Boolean RenderHeads.Media.AVProVideo.Subtitle::IsBefore(System.Single)
extern void Subtitle_IsBefore_m3E4D52E009AEA07E6587FC4B18C4293CC35F6F00 (void);
// 0x000008F6 System.Boolean RenderHeads.Media.AVProVideo.Subtitle::IsTime(System.Single)
extern void Subtitle_IsTime_m65474521C2FB8EE455BAEDFF1D385E965C252381 (void);
// 0x000008F7 System.Void RenderHeads.Media.AVProVideo.Subtitle::.ctor()
extern void Subtitle__ctor_m0E660134DF475780C8E2DD7EC7B9D9FEC0E0B156 (void);
// 0x000008F8 System.String RenderHeads.Media.AVProVideo.Helper::GetName(RenderHeads.Media.AVProVideo.Platform)
extern void Helper_GetName_m54E21394A5707C2DF0FC55BFCBA45D967AA7267D (void);
// 0x000008F9 System.String RenderHeads.Media.AVProVideo.Helper::GetErrorMessage(RenderHeads.Media.AVProVideo.ErrorCode)
extern void Helper_GetErrorMessage_m9079D5C4500C27D45D27BB21B13F8BDFD5BF0AAD (void);
// 0x000008FA System.String[] RenderHeads.Media.AVProVideo.Helper::GetPlatformNames()
extern void Helper_GetPlatformNames_m32907ACC4F350516C837341125B1BF45A1474973 (void);
// 0x000008FB System.Void RenderHeads.Media.AVProVideo.Helper::LogInfo(System.String,UnityEngine.Object)
extern void Helper_LogInfo_m57E62F23D3724009DCFA601668DE51D00FA68D0B (void);
// 0x000008FC System.String RenderHeads.Media.AVProVideo.Helper::GetTimeString(System.Single,System.Boolean)
extern void Helper_GetTimeString_mAB04F8B7709C7383F8B83EBB7EEDAC74A8571975 (void);
// 0x000008FD RenderHeads.Media.AVProVideo.Orientation RenderHeads.Media.AVProVideo.Helper::GetOrientation(System.Single[])
extern void Helper_GetOrientation_m7F57989C7488CC06CFE72165F96A80958B56C3E7 (void);
// 0x000008FE UnityEngine.Matrix4x4 RenderHeads.Media.AVProVideo.Helper::GetMatrixForOrientation(RenderHeads.Media.AVProVideo.Orientation)
extern void Helper_GetMatrixForOrientation_m992E825423E87F735A42C75B1105DC06F3A541DA (void);
// 0x000008FF System.Void RenderHeads.Media.AVProVideo.Helper::SetupStereoEyeModeMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.StereoEye)
extern void Helper_SetupStereoEyeModeMaterial_m73164807B8944B82455B1170DE7C49ABECEDECB1 (void);
// 0x00000900 System.Void RenderHeads.Media.AVProVideo.Helper::SetupLayoutMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.VideoMapping)
extern void Helper_SetupLayoutMaterial_mA9E72958D7B85BE9550BE01546B7D4E207F2B8EF (void);
// 0x00000901 System.Void RenderHeads.Media.AVProVideo.Helper::SetupStereoMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.StereoPacking,System.Boolean)
extern void Helper_SetupStereoMaterial_m7D47161AD4D7DE7D58593F18E064F5742235F4E1 (void);
// 0x00000902 System.Void RenderHeads.Media.AVProVideo.Helper::SetupAlphaPackedMaterial(UnityEngine.Material,RenderHeads.Media.AVProVideo.AlphaPacking)
extern void Helper_SetupAlphaPackedMaterial_m2A7D2B8E3A4FBDB682E39F139AF2423E79FE5260 (void);
// 0x00000903 System.Void RenderHeads.Media.AVProVideo.Helper::SetupGammaMaterial(UnityEngine.Material,System.Boolean)
extern void Helper_SetupGammaMaterial_m016674C657C3EB56580F0EE8D37411FB99546012 (void);
// 0x00000904 System.Int32 RenderHeads.Media.AVProVideo.Helper::ConvertTimeSecondsToFrame(System.Single,System.Single)
extern void Helper_ConvertTimeSecondsToFrame_m888568E8ADB79C4FEF9642A8DE6B07B0B64EB8B5 (void);
// 0x00000905 System.Single RenderHeads.Media.AVProVideo.Helper::ConvertFrameToTimeSeconds(System.Int32,System.Single)
extern void Helper_ConvertFrameToTimeSeconds_m56B24BD3E7BC25DBF0B4B032017D0E7D9BD1C0D9 (void);
// 0x00000906 System.Single RenderHeads.Media.AVProVideo.Helper::FindNextKeyFrameTimeSeconds(System.Single,System.Single,System.Int32)
extern void Helper_FindNextKeyFrameTimeSeconds_m8CACA1571C74BE5757F29776BF796D356DD0845A (void);
// 0x00000907 System.DateTime RenderHeads.Media.AVProVideo.Helper::ConvertSecondsSince1970ToDateTime(System.Double)
extern void Helper_ConvertSecondsSince1970ToDateTime_mEBAA30D44CAA0CFA31386B3C1AB5CCF71C913733 (void);
// 0x00000908 System.Void RenderHeads.Media.AVProVideo.Helper::DrawTexture(UnityEngine.Rect,UnityEngine.Texture,UnityEngine.ScaleMode,RenderHeads.Media.AVProVideo.AlphaPacking,UnityEngine.Material)
extern void Helper_DrawTexture_m253452BF0111F499B7CC4614B81B84B67A51CFED (void);
// 0x00000909 UnityEngine.Texture2D RenderHeads.Media.AVProVideo.Helper::GetReadableTexture(UnityEngine.Texture,System.Boolean,RenderHeads.Media.AVProVideo.Orientation,UnityEngine.Texture2D)
extern void Helper_GetReadableTexture_m02D1A423BD93C3502237CF8626BD93498DD16863 (void);
// 0x0000090A System.Int32 RenderHeads.Media.AVProVideo.Helper::ParseTimeToMs(System.String)
extern void Helper_ParseTimeToMs_m74FA06613620F1693563E873BA58E71A695E3F9A (void);
// 0x0000090B System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Subtitle> RenderHeads.Media.AVProVideo.Helper::LoadSubtitlesSRT(System.String)
extern void Helper_LoadSubtitlesSRT_m6B5236684F29839B4000110A1903BA1B633C68DA (void);
// 0x0000090C System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVersion()
extern void NullMediaPlayer_GetVersion_m2D32E17AB7A45268AA1974BBA65F9E13CCB39F10 (void);
// 0x0000090D System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
extern void NullMediaPlayer_OpenVideoFromFile_m67087280552E4185DFD4140156C655BF339D963F (void);
// 0x0000090E System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::CloseVideo()
extern void NullMediaPlayer_CloseVideo_m846FD21829C18686C24A8C35D74BFC5433D8697A (void);
// 0x0000090F System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetLooping(System.Boolean)
extern void NullMediaPlayer_SetLooping_m36E203C918FCDDF95AE29AA557962DD0417AF675 (void);
// 0x00000910 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsLooping()
extern void NullMediaPlayer_IsLooping_mBD79AE53183451C1CBFEEC3251923557A93F6937 (void);
// 0x00000911 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasMetaData()
extern void NullMediaPlayer_HasMetaData_m1C27C69454261BBF3AC9EC3442BF560F899A7083 (void);
// 0x00000912 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::CanPlay()
extern void NullMediaPlayer_CanPlay_m65C35A4AFFB0ED793C89350779AD1D11EB1ACE69 (void);
// 0x00000913 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasAudio()
extern void NullMediaPlayer_HasAudio_mACF01EA2350A8249A4FE958C04070126A7DA488C (void);
// 0x00000914 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::HasVideo()
extern void NullMediaPlayer_HasVideo_m2C8A3CC70091CF1696EE546F8C9851DE07201359 (void);
// 0x00000915 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Play()
extern void NullMediaPlayer_Play_m74B6D3E0E0C629FAFAB8B5147561E6C14CD8E35B (void);
// 0x00000916 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Pause()
extern void NullMediaPlayer_Pause_m1B385993EEBBB7A1E8CD982C800F94079EC5AEB5 (void);
// 0x00000917 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Stop()
extern void NullMediaPlayer_Stop_m2E4B3735F3600DC5A82442A9CFBAE0C9E044F9BC (void);
// 0x00000918 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsSeeking()
extern void NullMediaPlayer_IsSeeking_m6DBFD49480CC27B51BABB888CE8D7BAA57194124 (void);
// 0x00000919 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsPlaying()
extern void NullMediaPlayer_IsPlaying_mFF44BA9F250BFB1B5E714622AB6D29F63AF1FF44 (void);
// 0x0000091A System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsPaused()
extern void NullMediaPlayer_IsPaused_m8C26E9F65C043F1652652AA22549FFC070C2B6CD (void);
// 0x0000091B System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsFinished()
extern void NullMediaPlayer_IsFinished_mD237A2EAE75DB33F7F5AEB08A5C7A646744ABFA4 (void);
// 0x0000091C System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsBuffering()
extern void NullMediaPlayer_IsBuffering_mEE9F0E15C4A44051AA94C88EFECAE7676CB0C91A (void);
// 0x0000091D System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetDurationMs()
extern void NullMediaPlayer_GetDurationMs_m1BC8329F2CDE98204A091F6D871D9DF34B3217BE (void);
// 0x0000091E System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoWidth()
extern void NullMediaPlayer_GetVideoWidth_mCDC1F8707152D82D9E724C116656155D5E7B98E5 (void);
// 0x0000091F System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoHeight()
extern void NullMediaPlayer_GetVideoHeight_m92DABC37D41F3CE0428377F7AB9E038992BAB158 (void);
// 0x00000920 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoDisplayRate()
extern void NullMediaPlayer_GetVideoDisplayRate_m71FBC5D4B0D522F1C39CB72C21B46F88D5610C71 (void);
// 0x00000921 UnityEngine.Texture RenderHeads.Media.AVProVideo.NullMediaPlayer::GetTexture(System.Int32)
extern void NullMediaPlayer_GetTexture_mC2B52AE60436B35EFFEFBCA282FDE327474CA63F (void);
// 0x00000922 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetTextureFrameCount()
extern void NullMediaPlayer_GetTextureFrameCount_mAB0CBD3AF0CC0B5F9147411ED978F8F07B7957C8 (void);
// 0x00000923 System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::RequiresVerticalFlip()
extern void NullMediaPlayer_RequiresVerticalFlip_m8B33293C6218A315D5C14BD41B2B6739957B4530 (void);
// 0x00000924 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Seek(System.Single)
extern void NullMediaPlayer_Seek_mC26A9B0C6B0FC1C05F5525AF07A7DA29D29E80F1 (void);
// 0x00000925 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SeekFast(System.Single)
extern void NullMediaPlayer_SeekFast_m13A555AEBCC9DF364CDAF57236F7207A8697A6B5 (void);
// 0x00000926 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SeekWithTolerance(System.Single,System.Single,System.Single)
extern void NullMediaPlayer_SeekWithTolerance_m0F177406CCBCF9DD02D5A0C28E634BE0D17A9A0D (void);
// 0x00000927 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentTimeMs()
extern void NullMediaPlayer_GetCurrentTimeMs_m641EC97DA9B5F1432A4D0F8E836503504F8F763A (void);
// 0x00000928 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetPlaybackRate(System.Single)
extern void NullMediaPlayer_SetPlaybackRate_mF038B21BFE78769936AB17A64911D759A4A5AB4F (void);
// 0x00000929 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetPlaybackRate()
extern void NullMediaPlayer_GetPlaybackRate_m53E31C07A74BC07939A6D96A591EB8F93FC5202C (void);
// 0x0000092A System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetBufferingProgress()
extern void NullMediaPlayer_GetBufferingProgress_m5A86C137252AA1D41BA7E57A7AAACE7C30E024B3 (void);
// 0x0000092B System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::MuteAudio(System.Boolean)
extern void NullMediaPlayer_MuteAudio_m431F51C2C0522CCF72DB05B34246508901126F84 (void);
// 0x0000092C System.Boolean RenderHeads.Media.AVProVideo.NullMediaPlayer::IsMuted()
extern void NullMediaPlayer_IsMuted_m82BC4784E52D6E1B5C69215BC634DDCC5070C284 (void);
// 0x0000092D System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetVolume(System.Single)
extern void NullMediaPlayer_SetVolume_m89FDF2ECBE33CECD9A2CED3199B3C9749A1A1B50 (void);
// 0x0000092E System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVolume()
extern void NullMediaPlayer_GetVolume_m7EC9F8DEC28713E5069B0885B62D8914560E3DD6 (void);
// 0x0000092F System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetAudioTrackCount()
extern void NullMediaPlayer_GetAudioTrackCount_mE8C6B231BC0B80CFEE551DBF5194E9732E6F0493 (void);
// 0x00000930 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrack()
extern void NullMediaPlayer_GetCurrentAudioTrack_mEA7C55A0E9352294F9C825AAC3AF1687C3E25895 (void);
// 0x00000931 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetAudioTrack(System.Int32)
extern void NullMediaPlayer_SetAudioTrack_m30A10AE72350C73F6D7F3F514F553C15FF038B42 (void);
// 0x00000932 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoTrackCount()
extern void NullMediaPlayer_GetVideoTrackCount_mB78764E0160350C6CC9BCA8DB470A165267F8AB3 (void);
// 0x00000933 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrack()
extern void NullMediaPlayer_GetCurrentVideoTrack_m1CC33FD3327545698812CA4339045F9981683C6B (void);
// 0x00000934 System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrackId()
extern void NullMediaPlayer_GetCurrentAudioTrackId_m443816214E2FEF3F60F18A39CF4452285ABA86D2 (void);
// 0x00000935 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentAudioTrackBitrate()
extern void NullMediaPlayer_GetCurrentAudioTrackBitrate_m1F98E7EDDD4E3EF12C90F317ADA6E27F84F74DB7 (void);
// 0x00000936 System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::SetVideoTrack(System.Int32)
extern void NullMediaPlayer_SetVideoTrack_mFB9EA9FA2A16FA0F0900B840F81A5B1D99C4B6D4 (void);
// 0x00000937 System.String RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrackId()
extern void NullMediaPlayer_GetCurrentVideoTrackId_mE10D7C81EB97A5FD98BF0E5C9F7B06029667A17B (void);
// 0x00000938 System.Int32 RenderHeads.Media.AVProVideo.NullMediaPlayer::GetCurrentVideoTrackBitrate()
extern void NullMediaPlayer_GetCurrentVideoTrackBitrate_m36CF29E84149E68E31DF4BEB50D498D37655EC16 (void);
// 0x00000939 System.Single RenderHeads.Media.AVProVideo.NullMediaPlayer::GetVideoFrameRate()
extern void NullMediaPlayer_GetVideoFrameRate_mF407AA4BA66A4D51ED055DA40D42A996640117D5 (void);
// 0x0000093A System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Update()
extern void NullMediaPlayer_Update_m2B8B5596D048D9103E25C191EDC3ADABFF5B9DE9 (void);
// 0x0000093B System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Render()
extern void NullMediaPlayer_Render_m0DD70CF819E83B58056DA0DE1F1B599F3D7D8AB4 (void);
// 0x0000093C System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::Dispose()
extern void NullMediaPlayer_Dispose_m4132B60190D3EE92534A32C33C9CF9C5D8A853D7 (void);
// 0x0000093D System.Void RenderHeads.Media.AVProVideo.NullMediaPlayer::.ctor()
extern void NullMediaPlayer__ctor_m5156CF446D51B65FE47A6C142AC99012B48CDBAC (void);
// 0x0000093E System.Int32 RenderHeads.Media.AVProVideo.Resampler::get_DroppedFrames()
extern void Resampler_get_DroppedFrames_m1EA61AA75A5960424D62E22817ADB7D3C8BAE65A (void);
// 0x0000093F System.Int32 RenderHeads.Media.AVProVideo.Resampler::get_FrameDisplayedTimer()
extern void Resampler_get_FrameDisplayedTimer_mF250FCC5E3D962A0D9F1AB4710BA17964DB2A92B (void);
// 0x00000940 System.Int64 RenderHeads.Media.AVProVideo.Resampler::get_BaseTimestamp()
extern void Resampler_get_BaseTimestamp_m92514A0B60D0AC24A6C407DBBC016B24A6294B66 (void);
// 0x00000941 System.Void RenderHeads.Media.AVProVideo.Resampler::set_BaseTimestamp(System.Int64)
extern void Resampler_set_BaseTimestamp_m0A8A028FB04D4F4CFFEC6675CBE8F3C8C30604DE (void);
// 0x00000942 System.Single RenderHeads.Media.AVProVideo.Resampler::get_ElapsedTimeSinceBase()
extern void Resampler_get_ElapsedTimeSinceBase_m77620EEA1388DDD968E5E007A56A50FA10718703 (void);
// 0x00000943 System.Void RenderHeads.Media.AVProVideo.Resampler::set_ElapsedTimeSinceBase(System.Single)
extern void Resampler_set_ElapsedTimeSinceBase_m10CF5EED299EB36A9981466386B21427BDB214EE (void);
// 0x00000944 System.Single RenderHeads.Media.AVProVideo.Resampler::get_LastT()
extern void Resampler_get_LastT_m88ACC219257C611C4CAB2628E631CF6CE1ED32A2 (void);
// 0x00000945 System.Void RenderHeads.Media.AVProVideo.Resampler::set_LastT(System.Single)
extern void Resampler_set_LastT_m296C0C09FE43EC4CF459AA1DDF64CBB162ED819E (void);
// 0x00000946 System.Int64 RenderHeads.Media.AVProVideo.Resampler::get_TextureTimeStamp()
extern void Resampler_get_TextureTimeStamp_m049CD8BDBE6CAB268D77DE4D003BEC4C3B231E5F (void);
// 0x00000947 System.Void RenderHeads.Media.AVProVideo.Resampler::set_TextureTimeStamp(System.Int64)
extern void Resampler_set_TextureTimeStamp_m863FE886F11ACDBF61DFA61012DAC60FFE87D36B (void);
// 0x00000948 System.Void RenderHeads.Media.AVProVideo.Resampler::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void Resampler_OnVideoEvent_m2BD63DF30614F4A5D8778DACCB73A433F82B9422 (void);
// 0x00000949 System.Void RenderHeads.Media.AVProVideo.Resampler::.ctor(RenderHeads.Media.AVProVideo.MediaPlayer,System.String,System.Int32,RenderHeads.Media.AVProVideo.Resampler/ResampleMode)
extern void Resampler__ctor_mDA18B56AB34BDF3CE562998EE3B5CED682AA319A (void);
// 0x0000094A UnityEngine.Texture[] RenderHeads.Media.AVProVideo.Resampler::get_OutputTexture()
extern void Resampler_get_OutputTexture_m24C17095AB69CE9F42A4B4D314386148E7E7A98C (void);
// 0x0000094B System.Void RenderHeads.Media.AVProVideo.Resampler::Reset()
extern void Resampler_Reset_mC08E20EE1F0451C968BD38F8FB1EDFAFDCCE940E (void);
// 0x0000094C System.Void RenderHeads.Media.AVProVideo.Resampler::Release()
extern void Resampler_Release_m067A8411A8E4FD62802D330AF224280EC17841ED (void);
// 0x0000094D System.Void RenderHeads.Media.AVProVideo.Resampler::ReleaseRenderTextures()
extern void Resampler_ReleaseRenderTextures_mEDC445A66E2FB1E841AA031C84A1918CB3C5025D (void);
// 0x0000094E System.Void RenderHeads.Media.AVProVideo.Resampler::ConstructRenderTextures()
extern void Resampler_ConstructRenderTextures_mC94FAB6C3D09B12F036097E97A79E3D0C947EA1B (void);
// 0x0000094F System.Boolean RenderHeads.Media.AVProVideo.Resampler::CheckRenderTexturesValid()
extern void Resampler_CheckRenderTexturesValid_mE0A71AF053E365111AB3F289C8F2222CE22AE518 (void);
// 0x00000950 System.Int32 RenderHeads.Media.AVProVideo.Resampler::FindBeforeFrameIndex(System.Int32)
extern void Resampler_FindBeforeFrameIndex_m2FDDAF3496D08C89F6A90A14852215366F803743 (void);
// 0x00000951 System.Int32 RenderHeads.Media.AVProVideo.Resampler::FindClosestFrame(System.Int32)
extern void Resampler_FindClosestFrame_m8EEFF4A2103B8E3883B99F4D91CA136280BE424A (void);
// 0x00000952 System.Void RenderHeads.Media.AVProVideo.Resampler::PointUpdate()
extern void Resampler_PointUpdate_mCC19CB6726FEE54E971DA7DA581EA9510DB972CA (void);
// 0x00000953 System.Void RenderHeads.Media.AVProVideo.Resampler::SampleFrame(System.Int32,System.Int32)
extern void Resampler_SampleFrame_m60A059DB99F5453D6524B0A612AC0A650144330F (void);
// 0x00000954 System.Void RenderHeads.Media.AVProVideo.Resampler::SampleFrames(System.Int32,System.Int32,System.Int32,System.Single)
extern void Resampler_SampleFrames_m53EA62F83E449A508DB494BC2893F1DEE6E6018D (void);
// 0x00000955 System.Void RenderHeads.Media.AVProVideo.Resampler::LinearUpdate()
extern void Resampler_LinearUpdate_mCC402F1A6DCD4189C17B6E08F5F36BDDF176E203 (void);
// 0x00000956 System.Void RenderHeads.Media.AVProVideo.Resampler::InvalidateBuffer()
extern void Resampler_InvalidateBuffer_mC0AD36F9A5A1DA59D1E38E613E931C1AD91919FE (void);
// 0x00000957 System.Single RenderHeads.Media.AVProVideo.Resampler::GuessFrameRate()
extern void Resampler_GuessFrameRate_m333B2ACDD699112E8EF28D7B34BDB9A85E608E6F (void);
// 0x00000958 System.Void RenderHeads.Media.AVProVideo.Resampler::Update()
extern void Resampler_Update_m3FD7D39929FC49F6C369AAB23A5F85DC599A0E3A (void);
// 0x00000959 System.Void RenderHeads.Media.AVProVideo.Resampler::UpdateTimestamp()
extern void Resampler_UpdateTimestamp_mE5FB3749CC51BC6DC2D71FEF1E91CB3ACF8A6723 (void);
// 0x0000095A System.Void RenderHeads.Media.AVProVideo.Resampler/TimestampedRenderTexture::.ctor()
extern void TimestampedRenderTexture__ctor_mAB2C4E690144CFAC36F152869754C08B760F124C (void);
// 0x0000095B System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Width()
// 0x0000095C System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Height()
// 0x0000095D System.Int32 RenderHeads.Media.AVProVideo.Stream::get_Bandwidth()
// 0x0000095E System.String RenderHeads.Media.AVProVideo.Stream::get_URL()
// 0x0000095F System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.Stream::GetAllChunks()
// 0x00000960 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream/Chunk> RenderHeads.Media.AVProVideo.Stream::GetChunks()
// 0x00000961 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.Stream::GetAllStreams()
// 0x00000962 System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.Stream> RenderHeads.Media.AVProVideo.Stream::GetStreams()
// 0x00000963 System.Void RenderHeads.Media.AVProVideo.Stream::.ctor()
extern void Stream__ctor_m192C99ECFCD67CFA41561C424B6083F2E867CC8F (void);
// 0x00000964 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::InitialisePlatform()
extern void WindowsMediaPlayer_InitialisePlatform_mC8F11FD9D63586D0C71D9BE64834F1F1EC1CF08B (void);
// 0x00000965 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::DeinitPlatform()
extern void WindowsMediaPlayer_DeinitPlatform_m422A96A7A372606A0554732C2F8A3C24F80AC931 (void);
// 0x00000966 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetNumAudioChannels()
extern void WindowsMediaPlayer_GetNumAudioChannels_m074C778FD6FAD6B5CDE9471F1CDB33A20D002950 (void);
// 0x00000967 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::.ctor(RenderHeads.Media.AVProVideo.Windows/VideoApi,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Boolean,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern void WindowsMediaPlayer__ctor_mFBA50B0E2A91CF22613E6897F41966082DB1C6F1 (void);
// 0x00000968 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetOptions(RenderHeads.Media.AVProVideo.Windows/VideoApi,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Boolean,System.Boolean,System.Collections.Generic.List`1<System.String>)
extern void WindowsMediaPlayer_SetOptions_mF7504AB05C9162614BA717B32DC74D24E52EF397 (void);
// 0x00000969 System.String RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVersion()
extern void WindowsMediaPlayer_GetVersion_m4C2B98D71273A0013E33C15FD6BE278870055719 (void);
// 0x0000096A System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetUnityAudioSampleRate()
extern void WindowsMediaPlayer_GetUnityAudioSampleRate_mD531A7B1CFA7F079658D787D82F1E15E579F61EA (void);
// 0x0000096B System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::OpenVideoFromFile(System.String,System.Int64,System.String,System.UInt32,System.UInt32,System.Int32)
extern void WindowsMediaPlayer_OpenVideoFromFile_m16661F05D368EF0A4733671A896183D9566E6309 (void);
// 0x0000096C System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::OpenVideoFromBuffer(System.Byte[])
extern void WindowsMediaPlayer_OpenVideoFromBuffer_m52CB6EAFA4B0F864C693BB79DBAEAF00087FD787 (void);
// 0x0000096D System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::StartOpenVideoFromBuffer(System.UInt64)
extern void WindowsMediaPlayer_StartOpenVideoFromBuffer_m1A840B176BB6B8220885574F9BC7308C93062487 (void);
// 0x0000096E System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::AddChunkToVideoBuffer(System.Byte[],System.UInt64,System.UInt64)
extern void WindowsMediaPlayer_AddChunkToVideoBuffer_mB785ECD47C483B00A2B9295E003F1A7C0E55DA4F (void);
// 0x0000096F System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::EndOpenVideoFromBuffer()
extern void WindowsMediaPlayer_EndOpenVideoFromBuffer_m9BD32D8F6911FAAA85EAB743028AED37C50F40DA (void);
// 0x00000970 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::DisplayLoadFailureSuggestion(System.String)
extern void WindowsMediaPlayer_DisplayLoadFailureSuggestion_mDD75B22AEE57F13AEFE049CC4285F5F33BDA0924 (void);
// 0x00000971 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::CloseVideo()
extern void WindowsMediaPlayer_CloseVideo_mD554984E4F2B265B19C5AB1898629A2A48250A2E (void);
// 0x00000972 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetLooping(System.Boolean)
extern void WindowsMediaPlayer_SetLooping_m2F45E2B04CD6AC1F4D8FB1AC60A5D69616C5E157 (void);
// 0x00000973 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsLooping()
extern void WindowsMediaPlayer_IsLooping_m728FDC065397844C71E72C14703FA6D23FE4771A (void);
// 0x00000974 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::HasMetaData()
extern void WindowsMediaPlayer_HasMetaData_mD9A0AE09DFAD095F0D2A20FBDCAD594762D1CD33 (void);
// 0x00000975 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::HasAudio()
extern void WindowsMediaPlayer_HasAudio_mD8432A24FF4EB1CFAD784A0540BA26E9A9EF1736 (void);
// 0x00000976 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::HasVideo()
extern void WindowsMediaPlayer_HasVideo_m490A52885384652CD87B3CB6062EC713A105130F (void);
// 0x00000977 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::CanPlay()
extern void WindowsMediaPlayer_CanPlay_m3AB7538B29B0F721E1585B6AC9F8C2D9BD1396E4 (void);
// 0x00000978 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Play()
extern void WindowsMediaPlayer_Play_mCCD4294233646D82AF57EB7AB1CE348A9105F674 (void);
// 0x00000979 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Pause()
extern void WindowsMediaPlayer_Pause_mC78022EF51C6031713E5FB8DF520DC010F8E9695 (void);
// 0x0000097A System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Stop()
extern void WindowsMediaPlayer_Stop_m329B1A81E67BEFD41E065E17591EAF6951D8FDCA (void);
// 0x0000097B System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsSeeking()
extern void WindowsMediaPlayer_IsSeeking_mFAF4B7D7C4A93ECCBD4E951F5B4F1D8CF836AB07 (void);
// 0x0000097C System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsPlaying()
extern void WindowsMediaPlayer_IsPlaying_m68A26024CDB1E96619DE5A3DFE378E061BE76627 (void);
// 0x0000097D System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsPaused()
extern void WindowsMediaPlayer_IsPaused_mA0A59103210FCCAEEFE7D5C0D7D2DB932A70B4AC (void);
// 0x0000097E System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsFinished()
extern void WindowsMediaPlayer_IsFinished_mC3F894EF2A23860083042B955A214B7DEAF389C4 (void);
// 0x0000097F System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsBuffering()
extern void WindowsMediaPlayer_IsBuffering_mB2D3ED652E44CCCE11F6383AFDD18C915DA505FD (void);
// 0x00000980 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetDurationMs()
extern void WindowsMediaPlayer_GetDurationMs_m6AFDDB4CA12AD75B967C9D2A734D6AC4AF4E5005 (void);
// 0x00000981 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVideoWidth()
extern void WindowsMediaPlayer_GetVideoWidth_mD4B0C07567210E53AD44DD00511848CAEBAA2019 (void);
// 0x00000982 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVideoHeight()
extern void WindowsMediaPlayer_GetVideoHeight_m724A6288AE48C4696D92453B0806F187DA217CE9 (void);
// 0x00000983 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVideoFrameRate()
extern void WindowsMediaPlayer_GetVideoFrameRate_m137EE5158577B423A63F3DCA405076F8511FC97C (void);
// 0x00000984 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVideoDisplayRate()
extern void WindowsMediaPlayer_GetVideoDisplayRate_mB39613DBB30B735EE6963B26C61760618FD6E5E5 (void);
// 0x00000985 UnityEngine.Texture RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetTexture(System.Int32)
extern void WindowsMediaPlayer_GetTexture_mB125E8C6EB2AAA9AAE39BFC3451DAA8AF199079B (void);
// 0x00000986 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetTextureFrameCount()
extern void WindowsMediaPlayer_GetTextureFrameCount_m728C62CD9B326474B37A290788FEAACE4AD2F845 (void);
// 0x00000987 System.Int64 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetTextureTimeStamp()
extern void WindowsMediaPlayer_GetTextureTimeStamp_m1709D34DEE3288548A27BBD93CCCDDB5AAA22A14 (void);
// 0x00000988 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::RequiresVerticalFlip()
extern void WindowsMediaPlayer_RequiresVerticalFlip_mF05B4395EACAC6302B6C47616B4A6269AB319275 (void);
// 0x00000989 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Seek(System.Single)
extern void WindowsMediaPlayer_Seek_m75DF7B1B499B810500907A1FB521D883F8F9148D (void);
// 0x0000098A System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SeekFast(System.Single)
extern void WindowsMediaPlayer_SeekFast_mB6596C76C4753843618BA47BA276910714F58AF1 (void);
// 0x0000098B System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentTimeMs()
extern void WindowsMediaPlayer_GetCurrentTimeMs_m188FC3DC22A32FF9DE490C75AFA4D3EA34859372 (void);
// 0x0000098C System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetPlaybackRate(System.Single)
extern void WindowsMediaPlayer_SetPlaybackRate_mC9BDB0FDEF65E5D5E4F1FEC0E658A33B68273179 (void);
// 0x0000098D System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetPlaybackRate()
extern void WindowsMediaPlayer_GetPlaybackRate_m2DA85BE5CA54064AD87DA99DB131EB49618455D0 (void);
// 0x0000098E System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetBufferingProgress()
extern void WindowsMediaPlayer_GetBufferingProgress_m7791D40CBB6F9B46FDDE05AEE23BCB70A315B438 (void);
// 0x0000098F System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetBufferedTimeRangeCount()
extern void WindowsMediaPlayer_GetBufferedTimeRangeCount_m20572EB2AB5A29BB79DBE69C8A89517811E0F1A7 (void);
// 0x00000990 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetBufferedTimeRange(System.Int32,System.Single&,System.Single&)
extern void WindowsMediaPlayer_GetBufferedTimeRange_m122E6D1B572224B7AD218B5955A87BBFE5D8AFEE (void);
// 0x00000991 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::MuteAudio(System.Boolean)
extern void WindowsMediaPlayer_MuteAudio_m4EEE181F4434D7770EA37C6659FAB8CFECAB9758 (void);
// 0x00000992 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsMuted()
extern void WindowsMediaPlayer_IsMuted_mBA7B7FDFFEF234E26018B68C3A54A49E94A944AF (void);
// 0x00000993 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetVolume(System.Single)
extern void WindowsMediaPlayer_SetVolume_m50E95790E16E4B7F9C7B7A433413C40285F736F4 (void);
// 0x00000994 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVolume()
extern void WindowsMediaPlayer_GetVolume_mDDEB67681A3AF349846B0F33EB5BBE7F2A80151C (void);
// 0x00000995 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetBalance(System.Single)
extern void WindowsMediaPlayer_SetBalance_mBDE9D93E722719A62F6D59E74F3F988ED967BB68 (void);
// 0x00000996 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetBalance()
extern void WindowsMediaPlayer_GetBalance_mD9D1091B114DA2E19C590C2E0713E5CD96607862 (void);
// 0x00000997 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetAudioTrackCount()
extern void WindowsMediaPlayer_GetAudioTrackCount_mFAC00348F6DA1A8A0BFF7342FCE984295D808473 (void);
// 0x00000998 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentAudioTrack()
extern void WindowsMediaPlayer_GetCurrentAudioTrack_m1C8C30D36A13314A815954F8026557F40114B14C (void);
// 0x00000999 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioTrack(System.Int32)
extern void WindowsMediaPlayer_SetAudioTrack_m9E86040F50D0FED92B5596B3E39E4666EA254FA7 (void);
// 0x0000099A System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetVideoTrackCount()
extern void WindowsMediaPlayer_GetVideoTrackCount_m1CC21AD3EFDE2C167DDA54F58FD8B5B962BF66C4 (void);
// 0x0000099B System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IsPlaybackStalled()
extern void WindowsMediaPlayer_IsPlaybackStalled_mAFFF05F6C7B99DE483BC8555E01C7B3C0D9748B1 (void);
// 0x0000099C System.String RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentAudioTrackId()
extern void WindowsMediaPlayer_GetCurrentAudioTrackId_m25EF23A781452914E8F24B0635295B675BC64101 (void);
// 0x0000099D System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentAudioTrackBitrate()
extern void WindowsMediaPlayer_GetCurrentAudioTrackBitrate_m34C9F3CFEA43062C7519B73934E68DEBBA2B56D3 (void);
// 0x0000099E System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentVideoTrack()
extern void WindowsMediaPlayer_GetCurrentVideoTrack_mAE75179CD2F8872279AB65F3EF1F749BD19A842E (void);
// 0x0000099F System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetVideoTrack(System.Int32)
extern void WindowsMediaPlayer_SetVideoTrack_m4504BE8A35CED9E058F927995F7FF202A76E1CAB (void);
// 0x000009A0 System.String RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentVideoTrackId()
extern void WindowsMediaPlayer_GetCurrentVideoTrackId_m9757138D5D0FDBB86A5410884E5F41135361EF14 (void);
// 0x000009A1 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetCurrentVideoTrackBitrate()
extern void WindowsMediaPlayer_GetCurrentVideoTrackBitrate_m6146F86A9B512D5F72C8654CCF93C9C7CD091A7A (void);
// 0x000009A2 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::WaitForNextFrame(UnityEngine.Camera,System.Int32)
extern void WindowsMediaPlayer_WaitForNextFrame_m0B0D9C87C617D177362EA025366160FCE0763D6F (void);
// 0x000009A3 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioChannelMode(RenderHeads.Media.AVProVideo.Audio360ChannelMode)
extern void WindowsMediaPlayer_SetAudioChannelMode_m043A3DD8752F26255135244AE1AABA605088FEFD (void);
// 0x000009A4 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioHeadRotation(UnityEngine.Quaternion)
extern void WindowsMediaPlayer_SetAudioHeadRotation_m540FD70322BF43A9850EFBE4419FCBDC01FFB169 (void);
// 0x000009A5 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::ResetAudioHeadRotation()
extern void WindowsMediaPlayer_ResetAudioHeadRotation_mB7756FA165304A4E630CD0661EB22E5570DDFC4B (void);
// 0x000009A6 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioFocusEnabled(System.Boolean)
extern void WindowsMediaPlayer_SetAudioFocusEnabled_m2D08E6619E889F7606AE25F4A16EDE1E033C0032 (void);
// 0x000009A7 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioFocusProperties(System.Single,System.Single)
extern void WindowsMediaPlayer_SetAudioFocusProperties_m63D1AD9C9F2BA72DFB900544EF4473FF580358EC (void);
// 0x000009A8 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::SetAudioFocusRotation(UnityEngine.Quaternion)
extern void WindowsMediaPlayer_SetAudioFocusRotation_m0A9BE35195689BC399F04F5303C67E804874359D (void);
// 0x000009A9 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::ResetAudioFocus()
extern void WindowsMediaPlayer_ResetAudioFocus_m8449FEA1D7C2A7BB9205295B8BD458824E056DF5 (void);
// 0x000009AA System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Update()
extern void WindowsMediaPlayer_Update_m09F3322289D90A5210ED823A6D1446FDDC7CA39D (void);
// 0x000009AB System.Int64 RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetLastExtendedErrorCode()
extern void WindowsMediaPlayer_GetLastExtendedErrorCode_m04F70B981A089B53445C44E7595AE03594ABA324 (void);
// 0x000009AC System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::OnTextureSizeChanged()
extern void WindowsMediaPlayer_OnTextureSizeChanged_m950034589665B3FB95CBE8D565CBA9DC58E33DA9 (void);
// 0x000009AD System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::UpdateDisplayFrameRate()
extern void WindowsMediaPlayer_UpdateDisplayFrameRate_m261D9C480A2C5DEF7CA2A75175A4FFF7D6F5161B (void);
// 0x000009AE System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Render()
extern void WindowsMediaPlayer_Render_m857316997DA845091A38F1B01088FF3FBC8D2A13 (void);
// 0x000009AF System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::Dispose()
extern void WindowsMediaPlayer_Dispose_mA38523F1E4DA8CECFDF5CC3184484890BCD04E5E (void);
// 0x000009B0 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GrabAudio(System.Single[],System.Int32,System.Int32)
extern void WindowsMediaPlayer_GrabAudio_mF61EC2A7CCC13F1C2F9EE24B62395277AFDB852C (void);
// 0x000009B1 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer::PlayerSupportsLinearColorSpace()
extern void WindowsMediaPlayer_PlayerSupportsLinearColorSpace_mC4909274AF13EBDB35DAA9BB894E48156C86CF6D (void);
// 0x000009B2 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::IssueRenderThreadEvent(RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native/RenderThreadEvent)
extern void WindowsMediaPlayer_IssueRenderThreadEvent_m447F941BC3F232790F80486726A45FCC181B0011 (void);
// 0x000009B3 System.String RenderHeads.Media.AVProVideo.WindowsMediaPlayer::GetPluginVersion()
extern void WindowsMediaPlayer_GetPluginVersion_mB276437574B2589C4CCAA71767C66669F3823FEB (void);
// 0x000009B4 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::OnEnable()
extern void WindowsMediaPlayer_OnEnable_m90B611243ACF5B2AA01B7BF3DF6D5E96B2CCC33B (void);
// 0x000009B5 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer::.cctor()
extern void WindowsMediaPlayer__cctor_m6C92004CF153FCEAF9E4BF5E6E37ADE6AB0F29E3 (void);
// 0x000009B6 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::Init(System.Boolean,System.Boolean)
extern void Native_Init_mEFDC53C397F857EA48EE85CA0494913DAE5C5398 (void);
// 0x000009B7 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::Deinit()
extern void Native_Deinit_m4645663FBE532AD2F386431D7B6F128EDB8487E2 (void);
// 0x000009B8 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetPluginVersion()
extern void Native_GetPluginVersion_m9934A0E5D2A6AA31087796DDA932BED2B40E1B54 (void);
// 0x000009B9 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsTrialVersion()
extern void Native_IsTrialVersion_m2A6B98D189B056A340E8B586595E4C409A06028C (void);
// 0x000009BA System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::OpenSource(System.IntPtr,System.String,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Boolean,System.Boolean,System.Int32,System.IntPtr[],System.UInt32,System.Int32,System.UInt32,System.UInt32)
extern void Native_OpenSource_mFBA8BDB42931D37908E0E23BDFF5B42E1F4D46EA (void);
// 0x000009BB System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::OpenSourceFromBuffer(System.IntPtr,System.Byte[],System.UInt64,System.Int32,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Boolean,System.IntPtr[],System.UInt32)
extern void Native_OpenSourceFromBuffer_m63A74790CDC915D1900051D6324E2ADBB7166ACA (void);
// 0x000009BC System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::StartOpenSourceFromBuffer(System.IntPtr,System.Int32,System.UInt64)
extern void Native_StartOpenSourceFromBuffer_m7DEA9FBB2D9BE4ABA81583A1CF92A1F6DDADBED3 (void);
// 0x000009BD System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::AddChunkToSourceBuffer(System.IntPtr,System.Byte[],System.UInt64,System.UInt64)
extern void Native_AddChunkToSourceBuffer_mA784D8624D9D3C4BDE2FFD6CD1F3CC3B9D2E5A17 (void);
// 0x000009BE System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::EndOpenSourceFromBuffer(System.IntPtr,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.String,System.Boolean,System.IntPtr[],System.UInt32)
extern void Native_EndOpenSourceFromBuffer_m1E42065107A571DD4CA840F769B9407F3EFE89BB (void);
// 0x000009BF System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::CloseSource(System.IntPtr)
extern void Native_CloseSource_mE43750892A6C9A7B0CF6ED8F71B95CA40D2987F6 (void);
// 0x000009C0 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetPlayerDescription(System.IntPtr)
extern void Native_GetPlayerDescription_m50F871B0351FC5B3BE736C748BD34A6CBBBE11AA (void);
// 0x000009C1 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetLastErrorCode(System.IntPtr)
extern void Native_GetLastErrorCode_mEB13093E4C93B030FF994A2EC5A3D3D3C070C153 (void);
// 0x000009C2 System.Int64 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetLastExtendedErrorCode(System.IntPtr)
extern void Native_GetLastExtendedErrorCode_m92B73B3C5E15BDA54E5B00C899C695C728E3615C (void);
// 0x000009C3 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::Play(System.IntPtr)
extern void Native_Play_mD54A3E8A3FAB3B7000FA81E1F4FB7F86D0096136 (void);
// 0x000009C4 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::Pause(System.IntPtr)
extern void Native_Pause_m2E6B05F29875443B08F438B58841F4E010B728AA (void);
// 0x000009C5 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetMuted(System.IntPtr,System.Boolean)
extern void Native_SetMuted_mE7145B961814705AFCF6521F21F539E80E16EBD5 (void);
// 0x000009C6 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetVolume(System.IntPtr,System.Single)
extern void Native_SetVolume_m9E6EC2FD57F29EE4D9592D050D64A79023C417FD (void);
// 0x000009C7 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetBalance(System.IntPtr,System.Single)
extern void Native_SetBalance_m89D1C8CFA549C04EED2D913D88C933E0521EC62C (void);
// 0x000009C8 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetLooping(System.IntPtr,System.Boolean)
extern void Native_SetLooping_m1759CEC8C2A9B5A096425B5733FDB23E41C13F17 (void);
// 0x000009C9 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::HasVideo(System.IntPtr)
extern void Native_HasVideo_m865695FC5725BA9FC556F5A68080B1C771D3ABD7 (void);
// 0x000009CA System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::HasAudio(System.IntPtr)
extern void Native_HasAudio_m7C8FDCDFF57A0C77EFCA2AFC7AA680C23B4EA8D0 (void);
// 0x000009CB System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetWidth(System.IntPtr)
extern void Native_GetWidth_m62F11A0406FAF53B13278FE9E6037B3A5968F9E1 (void);
// 0x000009CC System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetHeight(System.IntPtr)
extern void Native_GetHeight_mD55DCF3FED165E0F69B23A9BAA6239AB1E58B6C0 (void);
// 0x000009CD System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetFrameRate(System.IntPtr)
extern void Native_GetFrameRate_m90D8AD7B08E9B93A7E6C85E4CB6D1C413A39C634 (void);
// 0x000009CE System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetDuration(System.IntPtr)
extern void Native_GetDuration_m3A2D5862D1BF5729B2B8F58B8E54D614D7ED40B4 (void);
// 0x000009CF System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetAudioTrackCount(System.IntPtr)
extern void Native_GetAudioTrackCount_mA5425541D8B10BD98DF69AA04BCB074E8744D18B (void);
// 0x000009D0 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsPlaybackStalled(System.IntPtr)
extern void Native_IsPlaybackStalled_m2D9166602079F96760CA915F6FBA157FC7EA0813 (void);
// 0x000009D1 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::HasMetaData(System.IntPtr)
extern void Native_HasMetaData_mE551B7DE502F68ED7E9CAE9B466B78911B825896 (void);
// 0x000009D2 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::CanPlay(System.IntPtr)
extern void Native_CanPlay_mB4B1DD0A8AC445E00A812B23AA6D3CF2B7EAD54D (void);
// 0x000009D3 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsSeeking(System.IntPtr)
extern void Native_IsSeeking_mF380D0DF862186DE120B10611F84CF0C1FEF1FDF (void);
// 0x000009D4 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsFinished(System.IntPtr)
extern void Native_IsFinished_mFE6549458880C0C20DE4AB2D937F7C6005DD0F6C (void);
// 0x000009D5 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsBuffering(System.IntPtr)
extern void Native_IsBuffering_mB3D1E0D381EE8CC9E52B81C5E896FBA68C4882DB (void);
// 0x000009D6 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetCurrentTime(System.IntPtr)
extern void Native_GetCurrentTime_m31FED24A64052BA271CC207A87B8588AD5F59471 (void);
// 0x000009D7 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetCurrentTime(System.IntPtr,System.Single,System.Boolean)
extern void Native_SetCurrentTime_m96F014C2D4EF01F3ABC7CE44C7F918CFAA665EF1 (void);
// 0x000009D8 System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetPlaybackRate(System.IntPtr)
extern void Native_GetPlaybackRate_m509CE798F044374ABD67F14CC99B362F419A2C3F (void);
// 0x000009D9 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetPlaybackRate(System.IntPtr,System.Single)
extern void Native_SetPlaybackRate_m5F53752EB3BEEEA7AA0A72D3F14E5E78FC79F8F2 (void);
// 0x000009DA System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetAudioTrack(System.IntPtr)
extern void Native_GetAudioTrack_m4E23F0B8D5EC4E47DD07E71E9EFB7E5137CBC9C8 (void);
// 0x000009DB System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetAudioTrack(System.IntPtr,System.Int32)
extern void Native_SetAudioTrack_m4899455AC470FDF458BB3434D5BA9BF5B93899A1 (void);
// 0x000009DC System.Single RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetBufferingProgress(System.IntPtr)
extern void Native_GetBufferingProgress_mA556B0C62BBA90A53C385139E4C799A520638B38 (void);
// 0x000009DD System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetBufferedRanges(System.IntPtr,System.Single[],System.Int32)
extern void Native_GetBufferedRanges_mBED227E3BD4B45312FEEB0A077AB67F7CB180D74 (void);
// 0x000009DE System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::StartExtractFrame(System.IntPtr)
extern void Native_StartExtractFrame_m20096E16247BD922F1B53AF7EFB5E27592961671 (void);
// 0x000009DF System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::WaitForExtract(System.IntPtr)
extern void Native_WaitForExtract_m983E15B9D4AB2873612928E96C4CD5450F35A418 (void);
// 0x000009E0 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::Update(System.IntPtr)
extern void Native_Update_mB3BDE28B0CF7801AD36F12C4C804D8802A32768B (void);
// 0x000009E1 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetTexturePointer(System.IntPtr)
extern void Native_GetTexturePointer_mED79CAD212D4574BE6F442385503793D025843BA (void);
// 0x000009E2 System.Boolean RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::IsTextureTopDown(System.IntPtr)
extern void Native_IsTextureTopDown_m6899F2991B8296B3BCD6EE68B4BE93FF486662B8 (void);
// 0x000009E3 System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetTextureFrameCount(System.IntPtr)
extern void Native_GetTextureFrameCount_m4A0D63B8C128CD73F81C8B7FAD278F76B44E4138 (void);
// 0x000009E4 System.Int64 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetTextureTimeStamp(System.IntPtr)
extern void Native_GetTextureTimeStamp_m1C38CE2C17C2217E55E5F96E58F45EEEBAA78AF5 (void);
// 0x000009E5 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetRenderEventFunc_UpdateAllTextures()
extern void Native_GetRenderEventFunc_UpdateAllTextures_m99E965F0728E9C0463EBCDF8EA22AA6A7DDECD37 (void);
// 0x000009E6 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetRenderEventFunc_FreeTextures()
extern void Native_GetRenderEventFunc_FreeTextures_m30253EE2D4C49B76934CF815650362927FFAA3CC (void);
// 0x000009E7 System.IntPtr RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetRenderEventFunc_WaitForNewFrame()
extern void Native_GetRenderEventFunc_WaitForNewFrame_m76D29D879A1E9BABACDF3B11C82E8047B719545D (void);
// 0x000009E8 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetUnityAudioEnabled(System.IntPtr,System.Boolean)
extern void Native_SetUnityAudioEnabled_mEA3194492AEDBB50294E44D322BF44BF324B836A (void);
// 0x000009E9 System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GrabAudio(System.IntPtr,System.Single[],System.Int32,System.Int32)
extern void Native_GrabAudio_m4DCB861977FD15A3FE7188C1A79D6A7F6200528D (void);
// 0x000009EA System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::GetAudioChannelCount(System.IntPtr)
extern void Native_GetAudioChannelCount_m439E2F217A57C0086943F74FA521208611564FDB (void);
// 0x000009EB System.Int32 RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetAudioChannelMode(System.IntPtr,System.Int32)
extern void Native_SetAudioChannelMode_m5EF87D92720CAD7A68C5BBA094CC392E8E21AED1 (void);
// 0x000009EC System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetHeadOrientation(System.IntPtr,System.Single,System.Single,System.Single,System.Single)
extern void Native_SetHeadOrientation_mC3642AA72A46F8AB6974D3BC82F5F7142BD263B4 (void);
// 0x000009ED System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetAudioFocusEnabled(System.IntPtr,System.Boolean)
extern void Native_SetAudioFocusEnabled_mA548C26440D8DD944F81C1E2AB419DE1006C4816 (void);
// 0x000009EE System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetAudioFocusProps(System.IntPtr,System.Single,System.Single)
extern void Native_SetAudioFocusProps_mDA2CF3A2E9E993A4BE942E525273FF0034236502 (void);
// 0x000009EF System.Void RenderHeads.Media.AVProVideo.WindowsMediaPlayer/Native::SetAudioFocusRotation(System.IntPtr,System.Single,System.Single,System.Single,System.Single)
extern void Native_SetAudioFocusRotation_m5E82F5B39147FB54390E1D3A88EA3CC845BED365 (void);
// 0x000009F0 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Awake()
extern void AutoRotate_Awake_m8A6F38CC70A0EEACF4409FDB238B9D22CC125827 (void);
// 0x000009F1 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Update()
extern void AutoRotate_Update_mE784F0DCB233C9733140BE71A3F6670C764E124B (void);
// 0x000009F2 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::Randomise()
extern void AutoRotate_Randomise_mE9E11DD6C9EE55568AB1FCD05D9EF0A2E84251C3 (void);
// 0x000009F3 System.Void RenderHeads.Media.AVProVideo.Demos.AutoRotate::.ctor()
extern void AutoRotate__ctor_mC6CD26AD20439865423845C1B509296FDD764626 (void);
// 0x000009F4 System.Void RenderHeads.Media.AVProVideo.Demos.DemoInfo::.ctor()
extern void DemoInfo__ctor_m9309499C389D89BD0CDD5E6B914B8C59B252E4F0 (void);
// 0x000009F5 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::Start()
extern void FrameExtract_Start_m624D7C4A0002C7083BDB353354BC7ED6DC0525D9 (void);
// 0x000009F6 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void FrameExtract_OnMediaPlayerEvent_m2884ED30BA45984B3E16A3C5E3577BE3A96F677C (void);
// 0x000009F7 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnNewMediaReady()
extern void FrameExtract_OnNewMediaReady_m7363DB357787FC9474FA2D486CF758AED4613776 (void);
// 0x000009F8 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnDestroy()
extern void FrameExtract_OnDestroy_mF1734CDA90E926AB36ED086F4544C9034EFDC65E (void);
// 0x000009F9 System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::Update()
extern void FrameExtract_Update_m02212BF8C47F48B296F26E3B85DC6F4551164407 (void);
// 0x000009FA System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::ProcessExtractedFrame(UnityEngine.Texture2D)
extern void FrameExtract_ProcessExtractedFrame_mDDCA242942C99AC29333BFDAB82FB9766BDF3E81 (void);
// 0x000009FB System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::ExtractNextFrame()
extern void FrameExtract_ExtractNextFrame_m2D5DE0399CD83607CAACBFF2FA327C85763CC486 (void);
// 0x000009FC System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::OnGUI()
extern void FrameExtract_OnGUI_mE9BCF528E03EFEAF055998A010A47CD47FE5A698 (void);
// 0x000009FD System.Void RenderHeads.Media.AVProVideo.Demos.FrameExtract::.ctor()
extern void FrameExtract__ctor_m4016D5DD96F482E85280288C58D85E585D2C3EEF (void);
// 0x000009FE System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::Update()
extern void Mapping3D_Update_mE93C103B982AE4AFA6C2F5B8A770029A4D5C5D35 (void);
// 0x000009FF System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::SpawnCube()
extern void Mapping3D_SpawnCube_m3C9287973623E4631EDBC024F9714DE0051AC5A2 (void);
// 0x00000A00 System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::RemoveCube()
extern void Mapping3D_RemoveCube_mF420949F2054B4D03C93B696B711129C92418E42 (void);
// 0x00000A01 System.Void RenderHeads.Media.AVProVideo.Demos.Mapping3D::.ctor()
extern void Mapping3D__ctor_m0801E9C63A5A1F6D0192E4E05C540EEA07945D09 (void);
// 0x00000A02 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::Update()
extern void SampleApp_Multiple_Update_mA299238F9B201180137610E19C40360FA9D81587 (void);
// 0x00000A03 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::UpdateVideosLayout()
extern void SampleApp_Multiple_UpdateVideosLayout_m4C37A5152E5EBD2440E3E524DE9685948CDF51F7 (void);
// 0x00000A04 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::AddVideoClicked()
extern void SampleApp_Multiple_AddVideoClicked_mDEBC0C0F84103A8A827EF86C04F6D9DDDE2C1ECB (void);
// 0x00000A05 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::RemoveVideoClicked()
extern void SampleApp_Multiple_RemoveVideoClicked_m7D3249A4E51B03BD1959BCCF724740AF5BEFEEE2 (void);
// 0x00000A06 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::OnDestroy()
extern void SampleApp_Multiple_OnDestroy_m0590F222FDDDCCD96FFD8008221BC49584E6A7B6 (void);
// 0x00000A07 System.Void RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::.ctor()
extern void SampleApp_Multiple__ctor_m20C539F251E6A956E23A13791C929EF034F61172 (void);
// 0x00000A08 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::OnEnable()
extern void ChangeAudioTrack_OnEnable_m75F693520F7AB2D689F6282C185F292CB5705509 (void);
// 0x00000A09 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::Update()
extern void ChangeAudioTrack_Update_m5425E11A36409F70681511679F7FF41C478E422B (void);
// 0x00000A0A System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::IsVideoLoaded()
extern void ChangeAudioTrack_IsVideoLoaded_mA5A8F22E752567D8D6ED2FBA8CC8939E70E0A0AB (void);
// 0x00000A0B System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::DoChangeAudioTrack(RenderHeads.Media.AVProVideo.MediaPlayer,System.Int32)
extern void ChangeAudioTrack_DoChangeAudioTrack_m9D4ABCA62680E421169BCC101AD8CA44DE347186 (void);
// 0x00000A0C System.Void RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::.ctor()
extern void ChangeAudioTrack__ctor_m1FB023CE311212491E9418FF1273695D616AA4C7 (void);
// 0x00000A0D System.Void RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::Update()
extern void ChangeStereoMode_Update_m7526E6BBBDFE067AED298A4563BC6D32C79F16B8 (void);
// 0x00000A0E System.Void RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::.ctor()
extern void ChangeStereoMode__ctor_mDF849CAFCFEFD27A26A2D2BDEFB1C07CED6DD797 (void);
// 0x00000A0F System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::LoadVideo(System.String)
extern void ChangeVideoExample_LoadVideo_mE22AA3BBAF747AA4723327B12B042D837A01337B (void);
// 0x00000A10 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::OnGUI()
extern void ChangeVideoExample_OnGUI_mE6E93D08591DDEFB4D2D0E3299C6737A9D6490D1 (void);
// 0x00000A11 System.Void RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::.ctor()
extern void ChangeVideoExample__ctor_mE286AAF97B55E64E20181A6EE453749248DD3DF5 (void);
// 0x00000A12 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::Start()
extern void LoadFromBuffer_Start_mA98E03E0D77C8BC7CBFB569C6279CD990F054B6B (void);
// 0x00000A13 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::.ctor()
extern void LoadFromBuffer__ctor_m06F6A560BB6F61E5D64579C33FAC671F3CEA00D1 (void);
// 0x00000A14 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::Start()
extern void LoadFromBufferInChunks_Start_m4036B4BF0C34FD755C0218833E45E86983837688 (void);
// 0x00000A15 System.Void RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::.ctor()
extern void LoadFromBufferInChunks__ctor_m61E1F375CB76F7B06B0FAAA41A02F5BC802AB246 (void);
// 0x00000A16 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::Start()
extern void NativeMediaOpen_Start_m044A8C77E479178A1BA9192590397A483EA6D87B (void);
// 0x00000A17 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::Update()
extern void NativeMediaOpen_Update_mE78D22D2F37FBAC4758A0873648FE65F46A523AB (void);
// 0x00000A18 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::OnGUI()
extern void NativeMediaOpen_OnGUI_m151B8192030A6330F461AE8E55A71D2B5AAFAAEB (void);
// 0x00000A19 System.Void RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::.ctor()
extern void NativeMediaOpen__ctor_m580D704173E7C42FF6F30EEC2CBBC2AF949D85FA (void);
// 0x00000A1A System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::Start()
extern void PlaybackSync_Start_mE6DB503FA3CE7AB16E604F6951050B1025375B99 (void);
// 0x00000A1B System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::LateUpdate()
extern void PlaybackSync_LateUpdate_m3B26ABD713378E2E0D90A159FD156183AC8E41B7 (void);
// 0x00000A1C System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsAllVideosLoaded()
extern void PlaybackSync_IsAllVideosLoaded_m8CAE02507E39719E709F3DD611EC14198ADF154C (void);
// 0x00000A1D System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsVideoLoaded(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void PlaybackSync_IsVideoLoaded_m5E20764B5BFD5A53203E24F90609DC3DA76F8E61 (void);
// 0x00000A1E System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::IsPlaybackFinished(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void PlaybackSync_IsPlaybackFinished_mDBE5C39D33BFC93DA747D64391C80738A98A4CD8 (void);
// 0x00000A1F System.Void RenderHeads.Media.AVProVideo.Demos.PlaybackSync::.ctor()
extern void PlaybackSync__ctor_m0F248D3CB7320263377D590F13E11BC4467FB4A9 (void);
// 0x00000A20 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::OnEnable()
extern void StartEndPoint_OnEnable_m02638CA3FC870BBDF1F415DB8C7BBB92A3355CE3 (void);
// 0x00000A21 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::Update()
extern void StartEndPoint_Update_mB6368F3BCF30D08529BD7F85E3CDBCF42AF51395 (void);
// 0x00000A22 System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::IsVideoLoaded(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void StartEndPoint_IsVideoLoaded_m0B96DB7EA00352C7462F7ACD574AB482EA5ECE9D (void);
// 0x00000A23 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoStart(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single)
extern void StartEndPoint_DoStart_mFAD879A1BBA946E07923D2B77E4AB8B465B68CAD (void);
// 0x00000A24 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoCheckEnd(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single)
extern void StartEndPoint_DoCheckEnd_mEA6ED57CA4DC4677EC6A6AB502E2F23E08AD284B (void);
// 0x00000A25 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::DoCheckLoop(RenderHeads.Media.AVProVideo.MediaPlayer,System.Single,System.Single)
extern void StartEndPoint_DoCheckLoop_m5804654DCA9248C03DDDFE8469B3FE3D2C7432D3 (void);
// 0x00000A26 System.Void RenderHeads.Media.AVProVideo.Demos.StartEndPoint::.ctor()
extern void StartEndPoint__ctor_m71615C24A6BDF28714196E5F2F41BE52446C5993 (void);
// 0x00000A27 System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerEnter(UnityEngine.Collider)
extern void VideoTrigger_OnTriggerEnter_m2983A2862DD2F5ECFF20DD7D3A713AB4575FA52F (void);
// 0x00000A28 System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::OnTriggerExit(UnityEngine.Collider)
extern void VideoTrigger_OnTriggerExit_m5A871DCA8036BE6D1E3C1E8CF866072C61239097 (void);
// 0x00000A29 System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::Update()
extern void VideoTrigger_Update_m77A64ED8BC8C23A8936DCE2452EA891C1F7EC236 (void);
// 0x00000A2A System.Void RenderHeads.Media.AVProVideo.Demos.VideoTrigger::.ctor()
extern void VideoTrigger__ctor_m0CA0DE7B4BB531ABBED01FEDC98104ADD8C221E7 (void);
// 0x00000A2B System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::Start()
extern void SimpleController_Start_mE67A5BE0BABD40F3843989BBD27226CAC9012DEA (void);
// 0x00000A2C System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnDestroy()
extern void SimpleController_OnDestroy_m2E889D579CFC9062E8AF4F402CE6871CC34D66CE (void);
// 0x00000A2D System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnMediaPlayerEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void SimpleController_OnMediaPlayerEvent_mA204E0516E4D889078AF3EF0F3464EADFDF9177C (void);
// 0x00000A2E System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::AddEvent(RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType)
extern void SimpleController_AddEvent_m1AD68B925D96DA06BDF7F1C70C391A702247E6AA (void);
// 0x00000A2F System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::GatherProperties()
extern void SimpleController_GatherProperties_mFA294185BA091A93A4B959BF103ED86E6BDB17B0 (void);
// 0x00000A30 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::Update()
extern void SimpleController_Update_m342957AE027132836B46B9E05B51C55F08B0A40E (void);
// 0x00000A31 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::LoadVideo(System.String,System.Boolean)
extern void SimpleController_LoadVideo_m556EDB14D348169F9136F6D231578F111770EF9F (void);
// 0x00000A32 System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::VideoIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SimpleController_VideoIsReady_mEE3EA61F987154B87C85EAA4C68FF35C79A61902 (void);
// 0x00000A33 System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::AudioIsReady(RenderHeads.Media.AVProVideo.MediaPlayer)
extern void SimpleController_AudioIsReady_mB8528AAB265BA0D6229DCE8BBF6D32AE2680F959 (void);
// 0x00000A34 System.Collections.IEnumerator RenderHeads.Media.AVProVideo.Demos.SimpleController::LoadVideoWithFading()
extern void SimpleController_LoadVideoWithFading_m79E36867177FC84073610479AAAA0F29326F3D1A (void);
// 0x00000A35 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::OnGUI()
extern void SimpleController_OnGUI_mE0F797243B99ACDDAE85F893F238BED637F998E5 (void);
// 0x00000A36 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController::.ctor()
extern void SimpleController__ctor_m6C9D1CE5E877156BB6D9852903BFAF52262EF08F (void);
// 0x00000A37 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::.ctor(System.Int32)
extern void U3CLoadVideoWithFadingU3Ed__23__ctor_m464D3CF77745A9FE30C1E20CC584C768A92E9AB6 (void);
// 0x00000A38 System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.IDisposable.Dispose()
extern void U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_m7D4A0E6A6F1E000404C5E9054476FFD6A6D731B4 (void);
// 0x00000A39 System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::MoveNext()
extern void U3CLoadVideoWithFadingU3Ed__23_MoveNext_m2C92DE80E3392939189C7A83EEFE7E88468A0AFB (void);
// 0x00000A3A System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35C73F9A099DE3EE7F662D4EF0D0F8184D54506D (void);
// 0x00000A3B System.Void RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.Reset()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m54C40996E5991A8777C08FDB3CEC1634D4765093 (void);
// 0x00000A3C System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::System.Collections.IEnumerator.get_Current()
extern void U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_mEA84603238447705495433D48D08BE4CD14E8986 (void);
// 0x00000A3D System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::IsVrPresent()
extern void SphereDemo_IsVrPresent_m74136D943CB8A446BF26B82CD2F320427C268D31 (void);
// 0x00000A3E System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::Start()
extern void SphereDemo_Start_mE1C38F6BF58B6BB3F405F198FDE9B5929B3F3FB4 (void);
// 0x00000A3F System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::OnDestroy()
extern void SphereDemo_OnDestroy_m58C3B0E3DB111C860DAB700275DA99D1E963E2E2 (void);
// 0x00000A40 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::Update()
extern void SphereDemo_Update_m29112EF6DEB073B4C5C126941C5EB0FF475F2511 (void);
// 0x00000A41 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::LateUpdate()
extern void SphereDemo_LateUpdate_m00E015DE096AB197DF33303F8204823305161B10 (void);
// 0x00000A42 System.Void RenderHeads.Media.AVProVideo.Demos.SphereDemo::.ctor()
extern void SphereDemo__ctor_m7C24A5C73C2FDB373D6977F2745FC315FE998F62 (void);
// 0x00000A43 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::get_PlayingPlayer()
extern void VCR_get_PlayingPlayer_mB0DB688C27DC07C53D8030C94F58DCE2BB46ACAA (void);
// 0x00000A44 RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::get_LoadingPlayer()
extern void VCR_get_LoadingPlayer_m43F07DE427B4F3069F1A46C98ADE5DB7649D5FD5 (void);
// 0x00000A45 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::SwapPlayers()
extern void VCR_SwapPlayers_m61EC274A60FBD89A46F6D030C90A01C62F271B0C (void);
// 0x00000A46 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnOpenVideoFile()
extern void VCR_OnOpenVideoFile_m28B4CBE85312F6CFD5A6C95FDE406452B8289E84 (void);
// 0x00000A47 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnAutoStartChange()
extern void VCR_OnAutoStartChange_m626C188F91FC04061D2AEA7B60165955E7FE4B47 (void);
// 0x00000A48 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnMuteChange()
extern void VCR_OnMuteChange_mF4A493C3FB66CCD3FAA2DFC8631BEE51CE318B8C (void);
// 0x00000A49 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnPlayButton()
extern void VCR_OnPlayButton_mADAD0866287A99B35EC535C3B7F0141F1FA5EBEA (void);
// 0x00000A4A System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnPauseButton()
extern void VCR_OnPauseButton_m8E90E83D52817F888430307FA8BD2B747537F624 (void);
// 0x00000A4B System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSeekSlider()
extern void VCR_OnVideoSeekSlider_mF4DDF3DE6BC48938D800BCCAF80C1610B6F784E8 (void);
// 0x00000A4C System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSliderDown()
extern void VCR_OnVideoSliderDown_mD52D67098FB0CBDFF08432B44C3A583378319E1E (void);
// 0x00000A4D System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoSliderUp()
extern void VCR_OnVideoSliderUp_mBC510AC64938D040DB5EEB60E95BFFC423E68A1A (void);
// 0x00000A4E System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnAudioVolumeSlider()
extern void VCR_OnAudioVolumeSlider_m4939762109F3226248B44502063D1EE06AFB3C1F (void);
// 0x00000A4F System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnRewindButton()
extern void VCR_OnRewindButton_mF841AAA17650665F33BC8FBEBC905978A2823CC3 (void);
// 0x00000A50 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Awake()
extern void VCR_Awake_m6EDE28767F9A35EACBF42D6760BCB7EC34D1560B (void);
// 0x00000A51 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Start()
extern void VCR_Start_mDACECB28CD16540902B54588C06A09D93A060E70 (void);
// 0x00000A52 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnDestroy()
extern void VCR_OnDestroy_m488F4FF96425A7F804FC23D44CAD2B8E45D3AC88 (void);
// 0x00000A53 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::Update()
extern void VCR_Update_m93E8FA09BBF987D984006163580F8348851D3B58 (void);
// 0x00000A54 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::OnVideoEvent(RenderHeads.Media.AVProVideo.MediaPlayer,RenderHeads.Media.AVProVideo.MediaPlayerEvent/EventType,RenderHeads.Media.AVProVideo.ErrorCode)
extern void VCR_OnVideoEvent_m16E494B43F890166ED2CC74576841D9B8C6CC961 (void);
// 0x00000A55 System.Void RenderHeads.Media.AVProVideo.Demos.VCR::.ctor()
extern void VCR__ctor_m9772243B63EC6A79EC853A9B36C8DF8EBDA21048 (void);
static Il2CppMethodPointer s_methodPointers[2645] = 
{
	ClientReceiver_Start_mEC9E753BB87B3FDD745D526D922050911AB6C3C0,
	ClientReceiver_Update_m17173BA23A7665ACA1B16B90A02D07E6DDE2BE04,
	ClientReceiver_FunctionReceive_m80D4988487FA6B5B1FBE3969C26D324B2884241E,
	ClientReceiver__ctor_m02241980F08639822CE42315EEBDCFFD24AA9D4F,
	UnityEventFloat__ctor_mC7D6C9C18641A39D7C257210EF668C341794E36C,
	UnityEventInt__ctor_m84D0CABE2FCC7052FF8D67EB3767A224D23FE27D,
	UnityEventBool__ctor_m9B937ABAF103E75E79DC2104A7B7EEFB6E444AF5,
	UnityEventString__ctor_mCE66A4594C42C4A82746C833FBE4145F91484656,
	UnityEventByteArray__ctor_m23B301FA7AABC26A59C88D8615503D77986024E2,
	UnityEventFloatArray__ctor_mA9B88E8929B24D14E96409EB0799FAD4F9766AE4,
	UnityEventTexture__ctor_m9028716C8991DE8500EFE3D238EB6FB8CF563851,
	UnityEventTexture2D__ctor_m0A227E24E5CC48D25E1AA3BBE53E748422DBA7F4,
	UnityEventRenderTexture__ctor_m5420132F74600A6756EEC00B5A2B25503C3669A8,
	UnityEventWebcamTexture__ctor_m92A1016EF2046F602286072F75493C61845517F4,
	UnityEventClass__ctor_m637B4A5B38E8D5CEA66D6685C2AF4B53FC7555D3,
	Loom_get_maxThreads_mBFDF05F02373CF221873B7795BB41E30A0EF52B8,
	Loom_get_Current_mD40AEFF1D0447BC2176470BC2AA958D16FBF56CD,
	Loom_Awake_m987F8BB9813558AE2440315CCA4487F28B5B851E,
	Loom_Initialize_m7AD8AE8E54D9B61CB2CC6A5E6CD5B3F81994CC6E,
	Loom_QueueOnMainThread_m1D1A0F1B02B10089C3F728F99A03014556ECC8F1,
	Loom_QueueOnMainThread_m01E9E67E96D2FBF8F5DB23F7D2A7AEEBCBAEB754,
	Loom_RunAsync_m60361CAE4DAF2E2B34F1281871CA7ADF7F2D4DEA,
	Loom_RunAction_mDA59C16D2E0D8E794B21FFAFA0B7ADA89B4360F2,
	Loom_OnDisable_mCB21F8F1E3CE52AEA8B0030C8DC629CB94F75C24,
	Loom_Start_m63DBD9D9C1128FABC8C1B93FE1A193CE870AE374,
	Loom_Update_mC403CC9D6F66697121E191A1EDBBF7AF17B205D8,
	Loom__ctor_m26E7344A62332D33A6E3091DD5374695D30A0A4F,
	Loom__cctor_m6161BC2506526A5D25E5692BF1FF063C2F4E5663,
	U3CU3Ec__cctor_mCAC5F226E1B6A1A5B723BB1E586D142068A79DEF,
	U3CU3Ec__ctor_m8CBFEE95D33DE68C847BCE1FD7103DA5ADE9AFCF,
	U3CU3Ec_U3CUpdateU3Eb__22_0_m9FC295F33486DA897E20972DBF8D38327830452D,
	EnergySavingManager_getGyroChanged_mAE35A89DD1FC6F0D2973104A72BF661C100E19EC,
	EnergySavingManager_getAnyKeyChanged_m40B5F5B75E89726BA5FF199BF7C5B1215572E729,
	EnergySavingManager_getTouchChanged_mA6997D0226800538704518F9FA378AC74135F5C4,
	EnergySavingManager_Awake_m9B379DF18D472454BE85BF71BAD42445146983F2,
	EnergySavingManager_Start_m4CF5B507EDD85D8AA6918F6C9B4DF3001A96C9B2,
	EnergySavingManager_Update_m7E8A32874D4315C245F9A77124C9F5229BA43143,
	EnergySavingManager_Action_ReloadScene_m779C8EA9C51C5A183C93DC47DD18893EBE6D175D,
	EnergySavingManager_DelayReloadScene_mF7A2AB7B939480498089DD8EC05FB35D902FBCF4,
	EnergySavingManager_Action_QuitApp_mEC4869447A6CA727C79C68F2247922D4FCD59817,
	EnergySavingManager_DelayQuitApp_m40B6738D77295EFBB81AEFA977CB3E759DD6F58A,
	EnergySavingManager__ctor_m6812540BA1398FCFDDA4772AF5AE5B87C56D871E,
	U3CDelayReloadSceneU3Ed__40__ctor_m9C2D4FCABFC9EADD8728467A959758E86FABE86D,
	U3CDelayReloadSceneU3Ed__40_System_IDisposable_Dispose_mEBF06A2B6A445F58ED8A59ACA890C2B6B0A85000,
	U3CDelayReloadSceneU3Ed__40_MoveNext_mD769F9DC7CDCF6049647F0B90EF14BE686F19DF7,
	U3CDelayReloadSceneU3Ed__40_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7CB2CB7AAC68E2F8FF7BBB0A8B866B8DF0CFF12E,
	U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_Reset_mD3D4AFD5BA4923D70EEAC2CDC7BA1A8617565EF8,
	U3CDelayReloadSceneU3Ed__40_System_Collections_IEnumerator_get_Current_mCCE823FFE54CAAB72EA600771F6EFCAB4F67FB4D,
	U3CDelayQuitAppU3Ed__42__ctor_mD835D2843CD39786B0E8B4493969D7303407FFA2,
	U3CDelayQuitAppU3Ed__42_System_IDisposable_Dispose_mADC91381BCDE6FCB627908B1C77D31057D306399,
	U3CDelayQuitAppU3Ed__42_MoveNext_m88363142B70765D3FC9EDCB98B21AE791613DB67,
	U3CDelayQuitAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m27A3D3C634F6D04EA49224B5121E8F04BF434E76,
	U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_Reset_m12DC3D27867D00F5F5711FA68E13A315EFB2663A,
	U3CDelayQuitAppU3Ed__42_System_Collections_IEnumerator_get_Current_m2012CD18A7958FE1CE6CD116718B220A86F78B30,
	FPSManager_Update_mC5FB1A7327C9855566378BC360F75CFE7C51E48C,
	FPSManager_Action_SetFPS_mB79FBF7A0CF4A52225CE7867E06F6F5AF8B2745B,
	FPSManager__ctor_mA7EA885C97F3C25E21C0587DE0782EAAFF465040,
	DemoObjectAnimation_Start_mC5A5FA2787F1BF6B7331CBF86377461668846B11,
	DemoObjectAnimation_Update_m146AAA28DF5D61F0A8158B4DD806819CA76D4752,
	DemoObjectAnimation__ctor_m16E3C4AC0AFEF8E404D01021F8DAAFEDC90B410C,
	DemoScreenshot_Reset_mEA1D117811D21202332F24B9E8C1D4B4FC947CA6,
	DemoScreenshot_Update_m704E9858DBF8B0887D644DBB47E9D4BA021296CF,
	DemoScreenshot_SaveScreenshot_m67711F8567152BE16D935D52655F27E3CE0C02BD,
	DemoScreenshot_SaveJPG_mA77A0626915ADB9F075EEF09C1CB6A04B7B30109,
	DemoScreenshot_SaveScreenshotPano_m166A4B075B7D0A12D14C157449298CAF29D04406,
	DemoScreenshot__ctor_mDFE42454CAF301642D867A5A417B3E11E4D63AD1,
	U3CSaveJPGU3Ed__10__ctor_m386C97FF710A502A5DD109699097996FAC80E844,
	U3CSaveJPGU3Ed__10_System_IDisposable_Dispose_mE48D4EF3C29A7661DCB7054FDEB399F421509A10,
	U3CSaveJPGU3Ed__10_MoveNext_mB4D7B683EA6797A048372892231910BA13E85A12,
	U3CSaveJPGU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m350A78EEC0F2BC37D985636B9E53D9EE08D2C089,
	U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_Reset_m47650C7D79E391BAC8F9A6FDC69A040F94B02D72,
	U3CSaveJPGU3Ed__10_System_Collections_IEnumerator_get_Current_m0C0561FABE8DADE8914B7CFE2E6ACB668C1C0509,
	U3CSaveScreenshotPanoU3Ed__11__ctor_mC5B18D77BB15BA8E62E9F4D69621F487CB7DF6DC,
	U3CSaveScreenshotPanoU3Ed__11_System_IDisposable_Dispose_mF64E38ADD60416E5AB6D008F50FD32EF32BB5F9C,
	U3CSaveScreenshotPanoU3Ed__11_MoveNext_mCCB0E917EFC9C49FA290AAFE5B754A82B1042F52,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0551D82882EFA01F6AB668CF7D8FB3B3F870169C,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_Reset_m55FFC76C7427098F5A8E637A5535D8FE5F7EC0D7,
	U3CSaveScreenshotPanoU3Ed__11_System_Collections_IEnumerator_get_Current_m0B1B6BC1A4115B6442C4A49280D06D42024640C2,
	FillCameraBackground_Start_m59ED5CFBA36D1D32540B49C1F06A25B4B0270974,
	FillCameraBackground_LateUpdate_mC787831631F44F8650BADB42ED16740B0312EE67,
	FillCameraBackground__ctor_m4BF8EEC0947E4143F4993BB4F6D8FF3AB95EB2D2,
	GyroController_Awake_mC2902E5004D2352B3071F88CC17C7C62EE884D33,
	GyroController_Start_mEC40C772A93C44744C7021CDB4A1F8940AE5B8EC,
	GyroController_EnableGyro_m597850219FA05002EC2469F9D3757C422899B3C4,
	GyroController_Update_m4E220AE541FA69B8F1E08A97889EA40B81EA3C8F,
	GyroController_AimingOffset_mE10E89A8AA15DA14BA06A831A27CB7E74C01DF16,
	GyroController_MouseDragRot_mC16070F127CE717C9144A0C68BA9B44FFEF74DDD,
	GyroController_TouchDragRot_mAA75FF07714BC9415249E984D0D768B09282D945,
	GyroController__ctor_m35DB854DB03DA01F19148B9B3573023B58F6BEF8,
	_UnityEventFloat__ctor_mF404A530353279730A7A450770D9512CAE7E8EAE,
	PointerEvents_Start_m0619591BD50210C2173550EA58603B5CFDEE27DE,
	PointerEvents_Update_m13290D9940D8B331488477FEBB2220CDBFE4701B,
	PointerEvents_OnPointerDown_mED61F3B782D38C8728A651B7C1D838939E9F2BE0,
	PointerEvents_OnPointerUp_mC3E8505699F6E3CB01EBEB6E1072215C5C58E7EF,
	PointerEvents_OnPointerEnter_mE5EA77DD8C92B353B8A2CF7E6EB9655BA4D5FDF5,
	PointerEvents_OnPointerExit_mE6616049BED7EBAF817BB3489E18E609BB453247,
	PointerEvents__ctor_m0C46DC0ED7304CE9F4984DD8A89F8654F84B04A9,
	TouchVisualization_Start_mE9826E2391900A7DB4CB8E064725A9A2AC956D33,
	TouchVisualization_Update_m985B411E26E043B601FAEA94DBF6F6DF8EAAA2A1,
	TouchVisualization__ctor_mD5E8D494581ED40336DA8B692BDD8004F54DACB3,
	OffScreenTrigger_Start_m1DA8D8FD100CFB63BCBDCC783D429BB0037B81BE,
	OffScreenTrigger_Update_m7F8B9926160286D53BB1A44AB8EDC2631D004FE3,
	OffScreenTrigger_Action_offscreen_mECD98558A5777297AFD5E58878CA6CA1B4F1FD81,
	OffScreenTrigger__ctor_m72FCBCD47F07ACC83413D9DED90008AE45AC74D5,
	WorldToScreenSpace_Start_m64F64FC3E4C8FA8D69ED364C4E5BD25C468633C3,
	WorldToScreenSpace_Update_m2FE5ADA33F0AF77704C773EA5A0562F3D7816385,
	WorldToScreenSpace_InvokeOnScreen_m6279AEBFA4AE793391F0E170A3FAD8A91A4A2CE9,
	WorldToScreenSpace_InvokeOffScreen_mDFCAD4E5FFAD7C4866232FF89AD24C24B52ED7D9,
	WorldToScreenSpace_OnDisable_m4EC05D8B91C08C09F3A889802970F7EF469AB74F,
	WorldToScreenSpace_OnEnable_m115A27B81FA39428D8C0AC43C4E76491A822088A,
	WorldToScreenSpace_DebugTest_m88873998A1257B961986D72F0B85727D442F99C2,
	WorldToScreenSpace__ctor_m5781646FA5241C4CF867EB77A4DC89CD9D613E59,
	ZoomManager_Start_m77C510FE9406539A9299F49C21AC7D16EBC746A0,
	ZoomManager_Update_m1250B595F983EC9A4E8E644CB36474C578EFAAF2,
	ZoomManager_GestureZoom_m6169EE03098A43B55A0BA1A525CB084637B44042,
	ZoomManager_ZoomIn_m15B67012D606434D0FE3F64250C29DA22B98909A,
	ZoomManager_ZoomOut_mD1E2A13519B774C144CDE623EBDE5AEB1D90D356,
	ZoomManager__ctor_m2CC2CF7A4F8DED39D25F2D27826C34B9DF0D1E42,
	SceneManager_helper_Action_LoadSceneName_m7D4039AA3780B88CE04BCE3B6A5CA60C002BE78C,
	SceneManager_helper_Action_Quit_mEE2D2B3DAED5D8D8D9F57F25CBDF7B8F0D8DC320,
	SceneManager_helper__ctor_m8FE1179083EDEA3CCBD8B8963DE371871FBDA356,
	TargetProjectionMatrix_Action_UpdateFOV_mE341D8AF055048386C552360EFF1AA3E2E3EADC5,
	TargetProjectionMatrix_Action_SetAllowUpdate_m5DF69304BCB6ACC412411FA08052F9087B803DC7,
	TargetProjectionMatrix_Action_SetForceDisableUpdate_mCDC2DDBEF44F38712DC40F73C4694BB0DBC811A5,
	TargetProjectionMatrix_UpdateProjectionMatrixLoop_mA4F3092CBAA52589E199C271C5C5F4310AA2A280,
	TargetProjectionMatrix_Start_m151B070FE2720E5F112472BCC5B76038473036E8,
	TargetProjectionMatrix__ctor_m645FBE1E100CD4D2C40320542AF123C03CDA0819,
	U3CUpdateProjectionMatrixLoopU3Ed__10__ctor_mF1F092D3580744393E65F26470ABFB5FA624205E,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_IDisposable_Dispose_m69E3ED3E3CC52EFF42639FC9CC20547412CEB760,
	U3CUpdateProjectionMatrixLoopU3Ed__10_MoveNext_m6427B67D9CC0687F909615E34FEE9CEAE09C0E92,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3FFF9756FD760615484DAD33BED17A5F64410B8A,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_Reset_m48BDCC8BCBDF2E199C2F0747709847FC77A9368D,
	U3CUpdateProjectionMatrixLoopU3Ed__10_System_Collections_IEnumerator_get_Current_mA53BBB6DCE402D2D9D24D7D9E09381B8D90E11CC,
	WebcamManager_Action_useFrontCam_m8ED258689D0EC42710B946D7E49F39AF32068513,
	WebcamManager_RestartWebcam_mAAEB6C09BE69612582B7F0B161B28D01AD8B54A6,
	WebcamManager_get_WebCamTexture_m35BDDA7BBF791071FA71CD6C867BC8A40773885F,
	WebcamManager_get_IsFlipped_m08320BB96217FE938F08C77B79B7C864B93CE2D2,
	WebcamManager_Start_mBDAA4410F75B7A6E5B3F4CE6DD63AE0BDEBBE47A,
	WebcamManager_Update_m6E5EB41C4A34E8C2A2ACE54173EE7904EA1D3A77,
	WebcamManager_OnApplicationQuit_m74199660E98163FB7E6D51199FC80C4541F0C113,
	WebcamManager_CalculateBackgroundQuad_mDDDB2D51DFBEE57B38A98B651BE962B91B1A9FE4,
	WebcamManager_Action_StopWebcam_mC18EABF0211B71C2A435D164C1138313B599E09A,
	WebcamManager_Action_StartWebcam_m7E278C81D0ACB8C6A1B56EA3328DF828BC19FB65,
	WebcamManager_OnEnable_mC2C792D9FDCC2E607A96F8823BB0B5161E671E2D,
	WebcamManager_OnDisable_mC7DDD33854300614CE34B2F00C4B1A5F08C861DF,
	WebcamManager_initAndWaitForWebCamTexture_m2038FC96D0B93758A70FFA9E3599BBC792665A3C,
	WebcamManager__ctor_m3B1211ACBDEBC4181E1591761814C889BE33063B,
	U3CRestartWebcamU3Ed__8__ctor_m558DDD3135B8F717F7DA1D08FABCB9A938DEB726,
	U3CRestartWebcamU3Ed__8_System_IDisposable_Dispose_mB65F777C48E205E4B8051407831AD5C9A6174F58,
	U3CRestartWebcamU3Ed__8_MoveNext_mBADBC7BAB84F045A65DFD2FBBAF2FF000F6C8972,
	U3CRestartWebcamU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF3A3164CF7A7D035E413E7C50F9E56F07F7E530,
	U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_Reset_m2684DEA1E75FDB1D383B0CA963ACAE2FE8C4258E,
	U3CRestartWebcamU3Ed__8_System_Collections_IEnumerator_get_Current_m90716B3793FFE4699BB46A762BD02777BC8B78F3,
	U3CinitAndWaitForWebCamTextureU3Ed__34__ctor_m13012C61A04A8289FFB98070BCCB13E25ACAEDE7,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_IDisposable_Dispose_m1D25632C5F2F60D269705EE1F5D7E6706D4042D8,
	U3CinitAndWaitForWebCamTextureU3Ed__34_MoveNext_mDF413DF7226F4F21703A20F310075B3DD94B5356,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m50FBA164EB4EC570F2970A9E97BF1D255E4B5707,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_Reset_m821CC481B994FF1BFA627B2CF48DE3C19B90B938,
	U3CinitAndWaitForWebCamTextureU3Ed__34_System_Collections_IEnumerator_get_Current_m9E35B5BF182FF5519B47E825AA7CF8B3E2A0083C,
	AudioDecoder_Start_m5475B508B4DFE94F8D70274CE38E8103BDCE79B7,
	AudioDecoder_Action_ProcessData_m62376715692CE83F5AE3F0913E2E8302E046AB05,
	AudioDecoder_get_Volume_mF02AD931449AB29B2AC9CE4BD52692A9DAE6F9CD,
	AudioDecoder_set_Volume_m644B238CB46A45955C70C8844B5628E7B78C97C0,
	AudioDecoder_ProcessAudioData_mB2F38413A2FF8AE50E38F965B40CFC61A8FAC5FB,
	AudioDecoder_CreateClip_m64D78B4D39B05BF08776572FBF1AA4F9B529620B,
	AudioDecoder_OnAudioRead_m568DEB832519F54C8D6BB5976F6909C2B46154D0,
	AudioDecoder_OnAudioSetPosition_mC171D96CEFE23F3AAA05093CD126AD64E492BC0A,
	AudioDecoder_ToFloatArray_m7365E20988C94B62B0880CFF96CB01F862F66869,
	AudioDecoder__ctor_m7428FA819C5CC591B785F7B21008B2D7305C40F4,
	U3CProcessAudioDataU3Ed__19__ctor_m4B88CC0E0F821B45617660761DC9B8DFB2F499D0,
	U3CProcessAudioDataU3Ed__19_System_IDisposable_Dispose_m930215C882E907A199C8FA9B963030F088AA0528,
	U3CProcessAudioDataU3Ed__19_MoveNext_mF66D5B9590FFF8E2B543FE8D1E2667670D6E6D62,
	U3CProcessAudioDataU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m44CB672888E763F716EEE1C09BC3AFB640583584,
	U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_Reset_m1D9CAA1A5872B486EB412357823782A971EA7FB2,
	U3CProcessAudioDataU3Ed__19_System_Collections_IEnumerator_get_Current_m4E20AB0886EAFEFB5C1514ECE053B692DAE069B8,
	AudioEncoder_Start_mDCB81765A426669CB1ED0771CF9294CDB113C212,
	AudioEncoder_OnAudioFilterRead_mC29FB707D0864B9850543CA7A6B5EAFBD9627EBD,
	AudioEncoder_SenderCOR_m76EC65EB4C6150B41F4142EFB891DF9F875428F6,
	AudioEncoder_FloatToInt16_mCDD0D0272A1E75F91AF06B6DF815D3CC95668A93,
	AudioEncoder_EncodeBytes_m366517BA3823C4322276A91E4BC4296E026B94C3,
	AudioEncoder_OnEnable_mD707839AEF3DAA3125438BFDBF246F6462650BD1,
	AudioEncoder_OnDisable_m42F8E5421D72F9F90767FB204A2AE400C5B21F85,
	AudioEncoder__ctor_mD67A2B52F6528C40A25E16B6DC9A5BC9F281FF23,
	U3CSenderCORU3Ed__22__ctor_mA61B9C1473DBEE4FCD50436184FEA2699A8F78FD,
	U3CSenderCORU3Ed__22_System_IDisposable_Dispose_m6E26F5E9A98A30EE1565B8B3A8A4DE7920784F74,
	U3CSenderCORU3Ed__22_MoveNext_m13C9958FA4E691BCC15F2E316DC7DB8F4A9DF186,
	U3CSenderCORU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC49BDBFB780B49D30F8E4F047363C9C950016565,
	U3CSenderCORU3Ed__22_System_Collections_IEnumerator_Reset_mF5740BB0AB5EAD53E7DA519DA7AE7C94D2584E94,
	U3CSenderCORU3Ed__22_System_Collections_IEnumerator_get_Current_mCC0D350AB3C6FE8E1EC9301105A6B01C726CEEBD,
	FMPCStreamDecoder_Reset_mCA58D8086FCF98EC123AE664F92887006F16C581,
	FMPCStreamDecoder_Start_m914A6E5746B23DF3B69C11EAB23D7B7F29D389A8,
	FMPCStreamDecoder_Update_mAC3F39B67DBF2EA99D28E343163A04C1AEDDE617,
	FMPCStreamDecoder_Action_ProcessPointCloudData_mF6442A3097E627CAE7DD9C67E195CF0BD6134E20,
	FMPCStreamDecoder_ProcessImageData_mD9FF4EF87D20EA43045108B6F9D4084EFF200471,
	FMPCStreamDecoder_OnDisable_m5D145A576F28245E5C5E95DA5F1D74AEC0B04A0F,
	FMPCStreamDecoder_Action_ProcessImage_mBF87C53684E3039948F95D27667EA4DB3B3CA775,
	FMPCStreamDecoder__ctor_mA97CBAB85033CED698376EAC562249C51BB2FBF9,
	U3CU3Ec__DisplayClass28_0__ctor_mDC5886C435106F338D4C84ADB1DF100277EC94E2,
	U3CU3Ec__DisplayClass28_1__ctor_m9D800CD136370CFBF3FE5877597105F3AC9DAAEB,
	U3CU3Ec__DisplayClass28_1_U3CProcessImageDataU3Eb__0_m4FA67DA6789CC38361FAD8A65877AF0741647A34,
	U3CProcessImageDataU3Ed__28__ctor_mFCF7164CF0FAB59E6AB8917741B3C6F3EEF79D30,
	U3CProcessImageDataU3Ed__28_System_IDisposable_Dispose_m2E2E49F74BFE9C572E6BD13984BBC1A3D0AE7153,
	U3CProcessImageDataU3Ed__28_MoveNext_mA27C953D5B7D3BD4BC8F0F9E40019E37E416CC5B,
	U3CProcessImageDataU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC23C1918B3A8403769110160090139AB506A4F6F,
	U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_Reset_m0A11C827AE83D9B52521DCBE5B72051B54F5938F,
	U3CProcessImageDataU3Ed__28_System_Collections_IEnumerator_get_Current_m7AB274E9CC84E8231455FC8BB41DC8D71F337363,
	FMPCStreamEncoder_Reset_m97928B002BBDC40E528A6FD43E31945B8F1FB761,
	FMPCStreamEncoder_CheckResolution_m2B95F9304C724F86ACED86081A886C1838066B2B,
	FMPCStreamEncoder_OnRenderImage_m6AD97D04BE044BB8577CE6CFB3C411B65FA694A5,
	FMPCStreamEncoder_LateUpdate_m617BC57ED5ECE413638B362B37A8927A2121FE1E,
	FMPCStreamEncoder_RequestTextureUpdate_m80C2F23F7FE8E9C6BAAF6F06AA9CACF6908BD2A8,
	FMPCStreamEncoder_get_SupportsAsyncGPUReadback_m6D459B7B91CFE756D7D2006C0369B09FD20FBD72,
	FMPCStreamEncoder_get_GetStreamTexture_mD3856A27A2E6054E87F8837CA8E9B969CF33A9D7,
	FMPCStreamEncoder_ProcessCapturedTexture_m29A4DC70FE8EFA0C5AAE66AB8DFEC044139FE74B,
	FMPCStreamEncoder_ProcessCapturedTextureCOR_mEEF58C9ACD8F623090D081ED5544F013411687C5,
	FMPCStreamEncoder_ProcessCapturedTextureGPUReadbackCOR_mF332AAAC334DD1B48EB5B0942B8DADCE67A99BB8,
	FMPCStreamEncoder_EncodeBytes_m1149580C4C4E67E7A75B774344BDF3A1BDD60B5B,
	FMPCStreamEncoder__ctor_m216674951E578D21752320559D7A3774665CF77E,
	U3CProcessCapturedTextureCORU3Ed__42__ctor_m9B0D8AC83A7A59E971A4F6E14618F00458E13B95,
	U3CProcessCapturedTextureCORU3Ed__42_System_IDisposable_Dispose_mB0EAD2DA81A514C9EBF867F4504928E367D4760A,
	U3CProcessCapturedTextureCORU3Ed__42_MoveNext_mD8C2A597937A8EDD0E027243CEC22A2DE2E0D9A4,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m432DF574611723EE7D90AFC05BAFAF519025D7E7,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_Reset_mA5A95C2FEF36C236B4E1ACC68D96F9D27B07776A,
	U3CProcessCapturedTextureCORU3Ed__42_System_Collections_IEnumerator_get_Current_m9CD50AC1A662E9A97D4013A384FB77D17A4E32D1,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43__ctor_m628D38CA50098205DC7961CC96D0969766998742,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_IDisposable_Dispose_mD3777C2FF27BF3822C0271654FB810F8A7BD0889,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_MoveNext_m5055C3A5AB0EA3B92D315A6773F5EE92BF8572D6,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5C9AD5DE44B3F60239BE9AB0421ABC1C94B38413,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_Reset_m58A0CECB176018FDFF73FAA5145ECE3BA60D1139,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__43_System_Collections_IEnumerator_get_Current_m7CCF691370A65E331963B42A4BAB1B316F409283,
	U3CU3Ec__DisplayClass44_0__ctor_m94C496F6DBDAB67C1669AA4E9DEE705B8B340AB8,
	U3CU3Ec__DisplayClass44_1__ctor_mE17FD129FC1E71888647150455ADF1B46991D1A9,
	U3CU3Ec__DisplayClass44_1_U3CEncodeBytesU3Eb__0_m6995E320DB1F67A4E95581C93B91BF0E9070A4D6,
	U3CEncodeBytesU3Ed__44__ctor_m89AC43A66209F3E6CA9C66A7EED633ECD87574DA,
	U3CEncodeBytesU3Ed__44_System_IDisposable_Dispose_mDA990CE2A65C441D6654CD47F4734A861C985A60,
	U3CEncodeBytesU3Ed__44_MoveNext_mD3BF17BE3F895BD8321A8569E3423E777FF16F03,
	U3CEncodeBytesU3Ed__44_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m34D2D1471592200E73B991DE08D37BF78292633B,
	U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_Reset_m719E2377FE8C0B41D256C40A8746863C79E34255,
	U3CEncodeBytesU3Ed__44_System_Collections_IEnumerator_get_Current_m637465828A3DC422301B364D6058290D43D59C4B,
	GameViewDecoder_get_ReceivedTexture_mC8D5BFB695908343AD33716F621616BF40B88E17,
	GameViewDecoder_Reset_m6E4BADFF8BF5AD016B1F46156D0E1079C85BA71D,
	GameViewDecoder_Start_m02A174BEA22D2C4D9782E741F1B0F327533DDDEE,
	GameViewDecoder_Action_ProcessImageData_m3F85268F93E00143F04267321D188A0133E1E933,
	GameViewDecoder_ProcessImageData_m4CC55BFFF33D854FD9594CB3F7A3273B56330194,
	GameViewDecoder_OnDisable_mD9F68509B11935C9F0E091D77E23723430681DE1,
	GameViewDecoder_Action_ProcessMJPEGData_m5331F366DDEB05B612EC6362336714B6690B1917,
	GameViewDecoder_parseStreamBuffer_m102267345D55FFB348419018E8413656D486C3B5,
	GameViewDecoder_searchPicture_m05F8D56CF9BB26BA5AB7817D96E255D42BA97170,
	GameViewDecoder_parsePicture_m4A1036265EE466424C265271C4C76DC31C702C01,
	GameViewDecoder__ctor_mE2B4B0BAA070CAB391F76A020683FC4BD0EDB976,
	U3CU3Ec__DisplayClass26_0__ctor_m96A07E4DE0753CE6F6691B27DCC5917982594D78,
	U3CU3Ec__DisplayClass26_1__ctor_m2FBE3B1C27912851E189E05EC005609ADAB7CF4B,
	U3CU3Ec__DisplayClass26_1_U3CProcessImageDataU3Eb__0_mD51C3F6EB6C5830012033595D0E4C847A9477DBA,
	U3CProcessImageDataU3Ed__26__ctor_mF2D2ACE7EEE752AEAE686BCDCD8EFFA06B08C293,
	U3CProcessImageDataU3Ed__26_System_IDisposable_Dispose_m0DE8BEF4B5A38A8C2873A613B488BDC1C8AA4F40,
	U3CProcessImageDataU3Ed__26_MoveNext_m7020AFA3E0D721C509DCD4DA7D0E3BE2BC47DEE9,
	U3CProcessImageDataU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m173DA41E891FEF77D2C0CE8B9F2C3D971E7679EE,
	U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_Reset_m8EE0C1FB6E65B9956EDB8A8F04B7FAD61D2638CC,
	U3CProcessImageDataU3Ed__26_System_Collections_IEnumerator_get_Current_mED336401EEB8B42E18D24CA260705B40C5D91587,
	GameViewEncoder_get_SupportsAsyncGPUReadback_m307889E9B3EF82B96B63C2CB42805822E9B1537A,
	GameViewEncoder_get_GetStreamTexture_m3B80F6AC311D8112299F6EE6AAF2C4D4B58D332B,
	GameViewEncoder_get_brightness_mB48985FF7799171F3870ECAE8E3675736656285C,
	GameViewEncoder_CaptureModeUpdate_mE20F5185E59FBEA746200D033129034D63A27D43,
	GameViewEncoder_Reset_m16BBE1C91978FC808D971510EA4FEFE274383CA5,
	GameViewEncoder_Start_m711CD69DEA6EC5A232DB1DB99A860124A4A90A7B,
	GameViewEncoder_Update_m40EEE8439FEEFC1ECB88B4EC58F96AA5577D69C9,
	GameViewEncoder_CheckResolution_m9E19E28728EE71B829D02D534BC222D95C75D0C2,
	GameViewEncoder_ProcessCapturedTexture_m85D51DDD505253E5959A847473E3D4C0FF42D347,
	GameViewEncoder_ProcessCapturedTextureCOR_m4CE12E97C197BC25686A06DE3C6DF495EDA7519A,
	GameViewEncoder_ProcessCapturedTextureGPUReadbackCOR_mD67F2991A429A2F3653FEAA41D5F14A1AB9EFF23,
	GameViewEncoder_OnRenderImage_mCCC857972D92691CB693EE22468BB660B979D12B,
	GameViewEncoder_RenderTextureRefresh_m55372366F394AD43E1D7BD18DC0849751ABECB48,
	GameViewEncoder_Action_UpdateTexture_m13F6BB9310FCF2729B107CC4B941BFDADFFAFC9C,
	GameViewEncoder_RequestTextureUpdate_m984BB1DA3D27181E8C83A9243B9CD527F4133CA0,
	GameViewEncoder_SenderCOR_mFE18AE7EF7B9D623234795730887A0857F426FD7,
	GameViewEncoder_EncodeBytes_mDE72CF51618F7FC8253001FC64D0435FA84984DF,
	GameViewEncoder_OnEnable_m922B2729372038A53A98AC56EDEFA99A68112B58,
	GameViewEncoder_OnDisable_m4EBC128C316E48C583EDC4567BDA50888DD1F883,
	GameViewEncoder_OnApplicationQuit_mBF83C40D20DCA2DB4B0D6BD6B61C34BD132B51B6,
	GameViewEncoder_OnDestroy_mB5A25D2943751726F379A9DBB1D29002D64EBB24,
	GameViewEncoder_StopAll_m1DF6708CA5E16BC6C75D685DA9F51EF0BF276626,
	GameViewEncoder_StartAll_m6308B2B05D7EF911D97A759E27DF336D154DB9DC,
	GameViewEncoder__ctor_mC5B6393F4082FDAF2E3D4BAE014C125D5AC7AAB1,
	U3CProcessCapturedTextureCORU3Ed__69__ctor_m349BA12399B95AA9CF528CE193AE14A6A0DC889C,
	U3CProcessCapturedTextureCORU3Ed__69_System_IDisposable_Dispose_mE6885B6EBA1687614681C0E6F70023B4C85E971B,
	U3CProcessCapturedTextureCORU3Ed__69_MoveNext_mE26D9D1B4F6AD3E56CCC47DD1731C88E572E433D,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC2A8853CF78817477FD9F9EDA951A636795BBD44,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_Reset_m0BD7910AEFC35C5DA7F6D396E58C89911C8EAD30,
	U3CProcessCapturedTextureCORU3Ed__69_System_Collections_IEnumerator_get_Current_m2F63B58BAF7737184B353CDE5CE59E88C573A4C6,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70__ctor_mE032DCBF94F2AF749A547BA3CDF0848E4D4C1611,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_IDisposable_Dispose_m3A3C3021524EBF569E24E6F5E88CC331A9A5E035,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_MoveNext_m9086D4FE8C5C349A458E0A51490593710D59735E,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mD04D1FB74E758F4E4366F40E0F5F777D44A83C02,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_Reset_m479B2599F6C84598BEF16687ACCC823E5BDC4AD6,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__70_System_Collections_IEnumerator_get_Current_m7DDE4BEF1104D014C21E07E6B41BF4BB44547B1C,
	U3CRenderTextureRefreshU3Ed__72__ctor_m9FBA53AD62A26B656D74D5FF7EBC1ED4D5058C8E,
	U3CRenderTextureRefreshU3Ed__72_System_IDisposable_Dispose_m96AA81C9A1C1A6D477DC7248915A860A35464D1E,
	U3CRenderTextureRefreshU3Ed__72_MoveNext_m529DF1FE044ECA76473D3F37DD799EA9366AF3D3,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m199E00D245FE84F06F945B32D651296E6CCF8C83,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_Reset_m157D8745BAEB1A3D271EB347E3B1F9D162643C8E,
	U3CRenderTextureRefreshU3Ed__72_System_Collections_IEnumerator_get_Current_m56670B5014CFA817C863BAC21A236A9020F86761,
	U3CSenderCORU3Ed__75__ctor_m5248213CF3B7629AE57D204251CB3C32CC909A68,
	U3CSenderCORU3Ed__75_System_IDisposable_Dispose_m4D5072E44AFE990223CE8302A75FC993E9E4B454,
	U3CSenderCORU3Ed__75_MoveNext_m6B0FE085DB2C4014080CF578F8BF07A7CCB6E71F,
	U3CSenderCORU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D72E6225B84BB0CAB451C5FA237AE7EA2620961,
	U3CSenderCORU3Ed__75_System_Collections_IEnumerator_Reset_mAAFB7A18426C402C9C931751E2CA48E719EE207A,
	U3CSenderCORU3Ed__75_System_Collections_IEnumerator_get_Current_m298A61AD42A74332B11A4F3FE6520899D31B2A76,
	U3CU3Ec__DisplayClass76_0__ctor_mBD2590CA5D804F1FC7FB9917E51AE295542A211B,
	U3CU3Ec__DisplayClass76_1__ctor_mB045CF39D33BA33C20D7C83D50BC2BF58DE823C6,
	U3CU3Ec__DisplayClass76_1_U3CEncodeBytesU3Eb__0_m1FB723FF38B2687580FD67AC5A982433E27E0BBB,
	U3CEncodeBytesU3Ed__76__ctor_m2D4DF2846C6A077D6CE0649718EBDDA075DAF9EE,
	U3CEncodeBytesU3Ed__76_System_IDisposable_Dispose_mE55A15277E473D9F2D4135393D008DBD6A1100BC,
	U3CEncodeBytesU3Ed__76_MoveNext_mAE62A232391C9E9DB9471CD19026CBEE54D088EF,
	U3CEncodeBytesU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1D6C24CC185C0BB17D5D992844654BF01D72FA10,
	U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_Reset_m2C431F2696EB5A00A934D87D93F282F96565C2C7,
	U3CEncodeBytesU3Ed__76_System_Collections_IEnumerator_get_Current_m2F175BEE870299917793DBBB52E3FB7E5ADBF973,
	MicEncoder_Start_m2DC3500C0FB63AB7DB2D74A6969423C4C0BCDD61,
	MicEncoder_CaptureMic_m20B4454E2E157822E9F9B99DF4EEC735E231F9D8,
	MicEncoder_FloatToInt16_mE492C780E2F11BA78D7DB9D7B360B9D7C38EE557,
	MicEncoder_AddMicData_mD38334C2E64D85EF325366ABC27B50EF9F751FAE,
	MicEncoder_SenderCOR_m4D54CDCA339EB0313D89E5FCD3372DFA06B73D3F,
	MicEncoder_EncodeBytes_mE627954E5410F9EB32C0A64D396857ADBCF55758,
	MicEncoder_OnEnable_mE3E85CF5A81A7D3A05A1C00832DA8167874F68A9,
	MicEncoder_OnDisable_m5704E679F1E757575272B920122D74B1B45EDBD9,
	MicEncoder_StartAll_mF2137D48FEA0E905A99FD4B9DDF78E5BA90BADF9,
	MicEncoder_StopAll_m2CB39B0755D59BDDAB21A72EE740FA9A65348866,
	MicEncoder__ctor_m785BC83D676DADE5B67A41D37A95CE6C8979E5AC,
	U3CCaptureMicU3Ed__25__ctor_mCCB0E36591F95A3EB566D6B7572EA298DFE9C12B,
	U3CCaptureMicU3Ed__25_System_IDisposable_Dispose_mDF2FDE4489AE0061221D6ADDE9B9CE4C44AB7419,
	U3CCaptureMicU3Ed__25_MoveNext_m5D2E82894CC545B60F44215031A877A8DF2C8D36,
	U3CCaptureMicU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1FBDAC6120F439A264A6232B62FE956E2B50FC3B,
	U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_Reset_mD46140D14D5E336728E2ACBFAD184D06ECD36ADF,
	U3CCaptureMicU3Ed__25_System_Collections_IEnumerator_get_Current_m5C7D3AB53FC19BDC813676D19AAC400CEC7E41DB,
	U3CSenderCORU3Ed__28__ctor_m4599307B8957A286498335806B28024B30147C43,
	U3CSenderCORU3Ed__28_System_IDisposable_Dispose_m720B454D1377C86EB4320E4901D75C76002136CC,
	U3CSenderCORU3Ed__28_MoveNext_m113B01C2E1795B6B00D604595E41A6D0F31D5F56,
	U3CSenderCORU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m01CC83D39095EAB79B390A0B03B8CA80514A76B9,
	U3CSenderCORU3Ed__28_System_Collections_IEnumerator_Reset_m48088E046EDEFB8EBFCEFD117D0F8DD554C2E2B6,
	U3CSenderCORU3Ed__28_System_Collections_IEnumerator_get_Current_mA2103C688782E250D58380931F78E3C58B0F4991,
	TextureEncoder_set_SetStreamWebCamTexture_m6E95DF6D926A8D67CF725A232C5EFE45689E6E50,
	TextureEncoder_get_GetStreamTexture_mE92CC2619C619E2B9264F2BA53D19169FDBAF991,
	TextureEncoder_get_GetPreviewTexture_m353A4445898BE7FBA1C16BBAC2F3EC75A3C3ED03,
	TextureEncoder_get_SupportsAsyncGPUReadback_mA189BBE116743BE5DFE4FF5F31EDF165E2AD4BC3,
	TextureEncoder_get_StreamWidth_m1F194961386571E5CF4D9936423585438704C788,
	TextureEncoder_get_StreamHeight_mB8C20A46F386C65FADA651B0099D2BCDA7ECE2D7,
	TextureEncoder_Start_m529A619E091D0517661737C0ECD97362F435BABC,
	TextureEncoder_Action_StreamTexture_m7F30752FC32A927EC96CCA3FD0F2884623E97318,
	TextureEncoder_Action_StreamTexture_m4394C73A567297ED27F23D803C82EB21CB3E4D64,
	TextureEncoder_Action_StreamWebcamTexture_m68C22C283EE3470DE9C34C35976F19E0831B857F,
	TextureEncoder_Action_StreamRenderTexture_mEF5834465A7923540928BE5054EB59D8BF47A892,
	TextureEncoder_ProcessCapturedTextureGPUReadbackCOR_mE98216B9C49ED531A9CE4BBAD86209DA07F2F2EA,
	TextureEncoder_RequestTextureUpdate_m53878A01F689338F0A032E6654995B4FFB6F85A3,
	TextureEncoder_SenderCOR_m104C7B8592F46AACB30D679D7BE67D0044C24811,
	TextureEncoder_EncodeBytes_m6C7371FB88101453BB519DB958B8962F394AAF89,
	TextureEncoder_OnEnable_mBF71F42C787977589B39646CB5D9ADE299EEDF8E,
	TextureEncoder_OnDisable_m412273B270C757AF203DE6B5E8070A3256C4D43A,
	TextureEncoder_OnApplicationQuit_mCE9D539FA0C437B7A6779D5DB0F29D131BF3A7B8,
	TextureEncoder_OnDestroy_m6FFA6FC72EA038D2C4D3B27394230FC2961941F8,
	TextureEncoder_StopAll_m25496EA60398851424A398B5639BFDC1A8A395B7,
	TextureEncoder_StartAll_mCDD5DF1747A784CF03FB25B97FB3D1A0F6726735,
	TextureEncoder__ctor_m72CD11EEB9878CDEAED87E5A22A8E751DEF6F384,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49__ctor_mC87CAD2F648891E0981CEF30D3E6D27509D81EBC,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_IDisposable_Dispose_m6A816B7D02CEE27A81BB54A3265BFD1E6794BE95,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_MoveNext_mD2E47E573D8891F9202AD8404A1042ED568F7669,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m161A5A209C5405CAA94D3927AD8ADD99716C1823,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_Reset_mEBE7C0CA8F0CB85DA36512EB77B767899B9B1C46,
	U3CProcessCapturedTextureGPUReadbackCORU3Ed__49_System_Collections_IEnumerator_get_Current_mBB96842995E784FE9B131940828407C1EFA4817A,
	U3CSenderCORU3Ed__51__ctor_m3A3DE179749EE5B0246188A55FF9DEA4715DC25C,
	U3CSenderCORU3Ed__51_System_IDisposable_Dispose_m4D3672B13294D2040F91E4E6939CCF61A1C8C529,
	U3CSenderCORU3Ed__51_MoveNext_m6FB439208F02D5A12C87CE9BE76D583AF26B2C12,
	U3CSenderCORU3Ed__51_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m70ACD69140004798E2FCA2F250052903E6ABBD04,
	U3CSenderCORU3Ed__51_System_Collections_IEnumerator_Reset_m6BEF057DF094C567EFA3665F7913CBFC6C2B0392,
	U3CSenderCORU3Ed__51_System_Collections_IEnumerator_get_Current_mA10486303E326BDB27D355835C7B3E9CE3D66C80,
	U3CU3Ec__DisplayClass52_0__ctor_m47C8B4213013AFC4EB7A1A48847B3D91F7F0E5A9,
	U3CU3Ec__DisplayClass52_0_U3CEncodeBytesU3Eb__0_mEF0607CD72BEF643FC20FBACFDB2EED7BFC0CA8D,
	U3CEncodeBytesU3Ed__52__ctor_m56FCD6473B32D5B1E608B1ABAC380AB684063136,
	U3CEncodeBytesU3Ed__52_System_IDisposable_Dispose_mBA680916D858702CB22BDD455DE7C17B0C3EE194,
	U3CEncodeBytesU3Ed__52_MoveNext_mE18E2596E658D91FA0062B85D3884438B46FE0E8,
	U3CEncodeBytesU3Ed__52_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79E11F5603810579ECB7ECCDFD25AECC0C341E61,
	U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_Reset_mE899EFFCEF5A5600448CB197DBFEFA3056382E77,
	U3CEncodeBytesU3Ed__52_System_Collections_IEnumerator_get_Current_mDA8A1B5058EB0B650A7C569384C33B439EDC8E0D,
	ConnectionDebugText_Start_m9F11FE5DF1F0B8F5BE4A631F95319DEF73EC537C,
	ConnectionDebugText_Update_m13B28363A6D1453E2AB7E64DF4CA916D185FCEF1,
	ConnectionDebugText__ctor_mF6E3404B2077E82E9868CABBC03163E64D5DDF4E,
	NetworkActionClient_Awake_m0129ECF53495C70E7D264424D83A9BD7EB46E532,
	NetworkActionClient_Action_SetIP_m1329683FA4CC4B7D5F0DE194694D3102F08446F4,
	NetworkActionClient_Action_SetServerListenPort_mFF3DB0256F47F9221B0F9556A4FDEAB4365E04BB,
	NetworkActionClient_GetCurrentMS_m7D13F79664DA785207D4DC1B51ABD12C177E2E47,
	NetworkActionClient_Action_ReceivedDataDebug_m6F4D86CC99381EAEE18A5944E5D90947D4069D80,
	NetworkActionClient_Action_RpcSend_m3A9E72A8E6DE779085C0E85BC451FA3F7B1E85D1,
	NetworkActionClient_Action_RpcSend_mCE72D264D14CFF800C70475D720281F4AFE75DCA,
	NetworkActionClient_Start_m4723B6DDC83B59F11DA4810DF37F274A0FCA012C,
	NetworkActionClient_Update_m94D09E65469CBF45EB9123F27A533B6975117508,
	NetworkActionClient_DataByteArrayToByteLength_m2AB6FABA3C8F0175EE6F1E590AD02D3E943A394F,
	NetworkActionClient_ReadDataByteSize_m6FEBE9380FB53EE49AEEE093A1150007837F6C96,
	NetworkActionClient_ReadDataByteArray_m9FAC77E4788D7E46E9E52E7B2CAA3FAA670E0AC4,
	NetworkActionClient_ProcessReceivedData_m5FF1C0DE231F1C60FCA8338927E611C6FC7F7DDA,
	NetworkActionClient_RelaunchApp_m729C27576144BB159677DC800587326119B2F2F6,
	NetworkActionClient_Action_Disconnect_m700CFDB523341F4DD6038140495308145E718488,
	NetworkActionClient_Action_ConnectServer_m8B8D1FDC28572A441A30228C929B603BF735E76F,
	NetworkActionClient_ConnectServer_m06918BC933FBA994E4C6F8046023893A739BD21C,
	NetworkActionClient_MainThreadSender_m0825837539C1C0491FA47C4C4292968988A040C0,
	NetworkActionClient_Sender_m7483290E0CD5431AC11966DA766F351CF83C1F30,
	NetworkActionClient_ClientEndConnect_mD2C9C67C21DCB5F118634DD1772EBE65422C92CB,
	NetworkActionClient_LOGWARNING_mFD5E2BBDC186CBF8CA0A6D9EF38765020C21F861,
	NetworkActionClient_OnApplicationQuit_mAECE415B5508C60F634C4A3248060DF4D637F93C,
	NetworkActionClient_OnDestroy_mC7EA6C36E605D5D5FDE16659B75DA2EF787DAB0E,
	NetworkActionClient_CheckPause_m59F07538EC48B84D1F779D2564E05F41FE9F3029,
	NetworkActionClient_OnApplicationFocus_mAA469CC852708A81D05B4D8517950105F2A4AF11,
	NetworkActionClient_OnApplicationPause_m4CC365C83844E1741C0D3CCC5470231CC7E8AFAA,
	NetworkActionClient_OnDisable_m6AD25F8B6DD0A8CA958AB3F5DBE328FA2280AE87,
	NetworkActionClient_OnEnable_m24443D74C46A76D6FA517E15175C76D02959D9A3,
	NetworkActionClient__ctor_mDE9AE6C068C52EA3DB849623995E64756060615C,
	NetworkActionClient_U3CConnectServerU3Eb__45_0_m7E387AEDD18614BE47F4386C66DA798638FFE6B2,
	NetworkActionClient_U3CConnectServerU3Eb__45_1_m7832DA779556CAFD32DC2DF43617C697B5E8F1BF,
	U3CRelaunchAppU3Ed__42__ctor_mC166E2E13D67362ACEEBAB3E211F28ECC2D9E696,
	U3CRelaunchAppU3Ed__42_System_IDisposable_Dispose_m824E8B74094A6B06F29F7B75E90B19AAC4A12CFF,
	U3CRelaunchAppU3Ed__42_MoveNext_m5EA62805E0763A0C1C3DFB17296F1BFF230D82D4,
	U3CRelaunchAppU3Ed__42_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m02533516AFC7B3B94CB7C92D548F9F8EA5B07083,
	U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_Reset_m4600363476C054520A4342D63CFBE64ABC1B01AD,
	U3CRelaunchAppU3Ed__42_System_Collections_IEnumerator_get_Current_m4538C6110478E7EE96D7A9E086D4F7656679BDE2,
	U3CConnectServerU3Ed__45__ctor_mEB1C6684AE9BD51E2B784FF20CFF65DA4F15D7B6,
	U3CConnectServerU3Ed__45_System_IDisposable_Dispose_m744DD31C7E7D6DE2BC1AD4D20DB497FCE25C381F,
	U3CConnectServerU3Ed__45_MoveNext_mDB6263EDC7254F687E5DB70F3EFC6E434C724388,
	U3CConnectServerU3Ed__45_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m656B315965F4C79B1B8D82515BBFC491AA19A0E0,
	U3CConnectServerU3Ed__45_System_Collections_IEnumerator_Reset_m7789E8076A903AF653ECAB8EA22BE776B5B92259,
	U3CConnectServerU3Ed__45_System_Collections_IEnumerator_get_Current_mE81591438BEF6C4A6BC372B5960D504CBE5B3A72,
	U3CMainThreadSenderU3Ed__46__ctor_m72D6EB3EE179291316E5E8C164A557F57AC85F1F,
	U3CMainThreadSenderU3Ed__46_System_IDisposable_Dispose_m7451DDFBC5D2BA1B1BCBEC5F7222E00B38F5AFB5,
	U3CMainThreadSenderU3Ed__46_MoveNext_mD080B387ADBF7309316E5060D54E526933401EF5,
	U3CMainThreadSenderU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m68B43672C8A3F269F239E63CBB71D966575691A8,
	U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_Reset_m0F47F7EB31CF4F2B413F260F0CA04402EEE11BE7,
	U3CMainThreadSenderU3Ed__46_System_Collections_IEnumerator_get_Current_m1AAC2AC837669AFE3B0A4D90A0131A72B254FB4F,
	NetworkActionDemo_Action_ProcessRpcMessage_m3F8F048923276D2122DCCAD79CCEDA00D6DD2F0C,
	NetworkActionDemo_Rpc_SetInt_mEBBE08D5E1344C92408FB6FA145E98D42397B61B,
	NetworkActionDemo_Rpc_SetFloat_mD86F473D5B9A30F6CD42E7BD987C47333D466BFC,
	NetworkActionDemo_Rpc_SetBool_m896F40BAD9170498E3D68D6990A7ACB1336BD267,
	NetworkActionDemo_Rpc_SetString_mDFE3A5D709D57FE685C243BE04A62003D9D3CA14,
	NetworkActionDemo_Rpc_SetVector3_m3E86BABD99D07E90B384942005B98522CC7CC527,
	NetworkActionDemo_Rpc_SetVector3Random_mFECF9BB2C316DFB29D0CE5E0B3C532C64C745B7F,
	NetworkActionDemo_Cmd_ServerSendByte_mB888EB2604FD758B75043C5F79A3CC367F147C07,
	NetworkActionDemo_Action_ProcessByteData_m3C3432D783402ACBD6B496CCA97ADEA6300967F6,
	NetworkActionDemo_Action_SetColorRed_m82F64D3A67C6FD6DCC0A7D3A7E319CCA75F8248A,
	NetworkActionDemo_Action_SetColorGreen_m8C420BD785548EC9EAF09DF64C99565E285F510C,
	NetworkActionDemo_Action_SetInt_m2F4D4153F43296823AE9B31CE160CEFC5D337AF2,
	NetworkActionDemo_Action_SetFloat_m5E7947E47E2D3B3EE5DDD4B82D6929F4BDBF7CFC,
	NetworkActionDemo_Action_SetBool_mC80C9D091E26CC8D5C644F2E2723EE41A532FA56,
	NetworkActionDemo_Action_SetString_m7780089255ED01EC5D4EA2F93C43E5B5D8289C4E,
	NetworkActionDemo_Action_SetVector3_m0327530139A146D5AA96E1D6506FD4A434A56E4D,
	NetworkActionDemo__ctor_mDC108827B590B45CBA0C048A4BDE53231BF7075A,
	NetworkActionServer_Awake_mFF5CC68AE73711BE3157B2C39C383B47D7CCB8DB,
	NetworkActionServer_Action_AddCmd_mF4D43D4B498051C922B889049A3469400E8F3FE2,
	NetworkActionServer_Action_AddCmd_m3E6B404027169B68E8483D392D39C7AFE8EE662B,
	NetworkActionServer_NetworkServerStart_m2688A65E422E89F6F3216E9FB7FEBA8914DC9EFF,
	NetworkActionServer_UdpReceiveCallback_m9AF08AAAD3055786AD8B497BB876348C7F397603,
	NetworkActionServer_StopServerListener_m37502E31CAA345E65E6C2C3E858382F79F3BBBCB,
	NetworkActionServer_Start_m04046C3163907AF3B7D948BB9E8FE9946BE3A275,
	NetworkActionServer_Update_m95B586DE2395189CBBC30E9600E5E01B20904C8A,
	NetworkActionServer_initServer_m37F92E6AEFAED7576D00B747B7D3A16336BD7277,
	NetworkActionServer_AcceptCallback_m8704F3571C35364A116135F77AB380261A7569DF,
	NetworkActionServer_senderCOR_m58BC12CB2B4A1257A332B462963F5651152AACFD,
	NetworkActionServer_Sender_m32027FCFC7F536EB5793330EBBD3F1980A1F8AE3,
	NetworkActionServer_byteLengthToDataByteArray_m95DB6C4D211DB8FCC639C7E4EC2D11FA353870B5,
	NetworkActionServer_StreamWrite_m302C090AF1DC2294A58073027D796E8511378391,
	NetworkActionServer_RemoveClientConnection_mBCA3F635EC6D407D301D66B1F09445C0B282A58E,
	NetworkActionServer_LOG_m4475F2FAD5E9BF5C85EA5B518D6D0F6815E21673,
	NetworkActionServer_StopAll_m5FBD1B20F9C906A250E03879495E4564E5233A0A,
	NetworkActionServer_OnApplicationQuit_mE85D2AED0E5FBD89C30DF7F96B24763C025FDFE2,
	NetworkActionServer_OnDisable_m56A4FF4D08752497E79828A2F74C0F2BAE88341E,
	NetworkActionServer_OnDestroy_m1D4FFF3BBEFFC782C910B257113618D8F196810B,
	NetworkActionServer_OnEnable_m9FAEA6E9C1AEC742BEB1F432B2E50C343AF44838,
	NetworkActionServer__ctor_mD2D39A5FCE26BAAAE14B4E3CA6564DA3D44C1662,
	NetworkActionServer_U3CNetworkServerStartU3Eb__27_0_m13882AB3D8A7FD03E6A869746FDADB3353A83F66,
	NetworkActionServer_U3CinitServerU3Eb__35_0_mEF779817712A934D8D21A0CC500E2A9927DE71C7,
	NetworkActionServer_U3CinitServerU3Eb__35_1_mE7807EA9C8BDC3AF1363B473621B79BD3E44F0E6,
	U3CNetworkServerStartU3Ed__27__ctor_m2C646D9D17A8BD6E720CDB981A41A0560FAFCBB9,
	U3CNetworkServerStartU3Ed__27_System_IDisposable_Dispose_m533F0C08C38526569CD1EB13067B801676272478,
	U3CNetworkServerStartU3Ed__27_MoveNext_m0002F7E9853B359B8CFC47257AD9EF1145296DBE,
	U3CNetworkServerStartU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m75759CFC10015BECC7F79EB2A53F87802AC7A108,
	U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_Reset_m96F2BF1B9713688F24E984C192AEA8367D15E200,
	U3CNetworkServerStartU3Ed__27_System_Collections_IEnumerator_get_Current_m34FEE03B5E5A351BDE9BAC79F515334DFA8C8B3C,
	U3CinitServerU3Ed__35__ctor_m35EDCFE7B1FB0BA4D7CE2CEE71918DF3035645A3,
	U3CinitServerU3Ed__35_System_IDisposable_Dispose_m1538E68F5057A36C9F641C716590E4225775C31D,
	U3CinitServerU3Ed__35_MoveNext_mB2B3704B8B39D8257922A44E4987DF846C584E96,
	U3CinitServerU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m13187C7D067445A8FD48F5BD8F19F5D741173A7D,
	U3CinitServerU3Ed__35_System_Collections_IEnumerator_Reset_m82D955E151C89F40E20B4E37E73A649492DCA561,
	U3CinitServerU3Ed__35_System_Collections_IEnumerator_get_Current_mDB4BD06A0F8DD85C73AC1854C292074FE10DF82B,
	U3CU3Ec__DisplayClass37_0__ctor_m226F9B77215E54E404F92C6753B0D482B8EB8A8A,
	U3CU3Ec__DisplayClass37_0_U3CsenderCORU3Eb__0_m25BD754690CE381A95838D867D206E015D13C1D0,
	U3CsenderCORU3Ed__37__ctor_mF334575352C3321DE1D342411B3ECB9CBD1EA96E,
	U3CsenderCORU3Ed__37_System_IDisposable_Dispose_mF4E8998740261CA0165CF75F1264077EB0A3E847,
	U3CsenderCORU3Ed__37_MoveNext_mC9FB2083E84C2C4231BA379ABD830AB63EF6FB6E,
	U3CsenderCORU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF22D6865B20A1B465C339C8E4E2D99F443A51415,
	U3CsenderCORU3Ed__37_System_Collections_IEnumerator_Reset_mDC28498A4A797A7813650075FD9A3C8CC638303C,
	U3CsenderCORU3Ed__37_System_Collections_IEnumerator_get_Current_m267C4C53E001D2589C1072BCF06FC874BB7780D3,
	NetworkActions_Debug_Action_TextUpdate_m89583A78522D14B92FBE1F3002AB2AF05947249F,
	NetworkActions_Debug_Start_m2A553719B77C9C9B2DFA7D86C94BF26C803F23F8,
	NetworkActions_Debug_Update_mE1F9C9C264509B369FC8C88A81F8ACB286CE680F,
	NetworkActions_Debug__ctor_mA59D1D164AC95AA2ECE2EE69748A4F1BDCDD0578,
	NetworkDiscovery_LocalIPAddress_m899D97D8486722BDB44FF5DC21938AD414E9FDDA,
	NetworkDiscovery_SetStreamingPort_m034B46BE8E395BC77691EFF405E2EAA044B128B7,
	NetworkDiscovery_Action_SetIsStreaming_m26F1D10B3721EE621C9D6B26F99EAA69C8D67B67,
	NetworkDiscovery_Start_mA4ECDDCD6F9ED39958B4CCF8925D194B42F6AA5D,
	NetworkDiscovery_Update_m5F90B443785D7576373A6B5A766224D8B2584EA2,
	NetworkDiscovery_Action_StartServer_m40A95A1B427BCEEDE75993C4C08EB1E02684985C,
	NetworkDiscovery_Action_StartClient_m7E87F3ECE1065853ED805D304BB79B4EF9AEBFFC,
	NetworkDiscovery_NetworkServerStart_mAA1A2A539471914D5C3C6CEE4BE22303725132E7,
	NetworkDiscovery_UdpReceiveCallback_mC09D6C0E64AE8BDBD498ECD679C1D6908EA8ADB6,
	NetworkDiscovery_NetworkClientStart_mA6CE469839598282D32F834948855D9382CD1A08,
	NetworkDiscovery_DebugLog_m2002FB01A4CFC4093E030EBE8E098C2672415AFB,
	NetworkDiscovery_OnApplicationQuit_m840B5586B84CBF8894E7AE478226AE2EACA0CD78,
	NetworkDiscovery_OnDisable_mFFA9C6E77731585FCACE941A17123127DEE0AF8D,
	NetworkDiscovery_OnDestroy_m41EDDE21ADF9E6A5AC08F82F0A4D5170446E8DF7,
	NetworkDiscovery_OnEnable_m6F3CD3D70AF7E31F8C92019795E2077EA22A526D,
	NetworkDiscovery_StartAll_m8E24754B92576DD7E43AAF2528D41B26DF49B280,
	NetworkDiscovery_StopAll_mC4B8203A989CD055E7FEF559E3FC157C967229DF,
	NetworkDiscovery__ctor_m0FC92465AC4A86371F98B088168111ABED326F45,
	NetworkDiscovery_U3CNetworkServerStartU3Eb__25_0_m4E0DC9FDE405ED9CF764E15035667AE5D4334BFC,
	U3CU3Ec__DisplayClass25_0__ctor_m203DA6941EB9343E3BE68B0437421ECD10221663,
	U3CU3Ec__DisplayClass25_0_U3CNetworkServerStartU3Eb__1_m453275FBA3251421E5FCBC1DC748BCCFF8D676D7,
	U3CNetworkServerStartU3Ed__25__ctor_mFAA855ABF9E5F50C4CF3325F5555839E75776051,
	U3CNetworkServerStartU3Ed__25_System_IDisposable_Dispose_m72F3867FCB52544BF1AB5D70DC37D669A688DA6F,
	U3CNetworkServerStartU3Ed__25_MoveNext_mAA6A0384BEEA5603C22AFA4B9CECA514CCCAA93C,
	U3CNetworkServerStartU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6C21120D45AFB33F348C5C03EF52D19C77B07659,
	U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_Reset_mEC78230C8DBA48F0728BC1A4BA4DDA029FE4B1B9,
	U3CNetworkServerStartU3Ed__25_System_Collections_IEnumerator_get_Current_m924B735731B289A454C795548B370C172FC61CBF,
	U3CU3Ec__DisplayClass28_0__ctor_m05A2ADD61078628E2E713C390F4A96EEF1DA981A,
	U3CU3Ec__DisplayClass28_0_U3CNetworkClientStartU3Eb__0_m22980F80AA34CA5B3B6100EA625DD6411B6F34EF,
	U3CU3Ec__DisplayClass28_1__ctor_mBD315E3EF46F8A35CD9B5F81C1E32F6CAF833FB4,
	U3CU3Ec__DisplayClass28_1_U3CNetworkClientStartU3Eb__1_mCA5B2A89C47BE3102A555E40465E7FB7708277B7,
	U3CNetworkClientStartU3Ed__28__ctor_m588398FEA5BB59B827665F538B0C67CC82ED61D1,
	U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mFB47488CB857B037562EE2879797882A285F6044,
	U3CNetworkClientStartU3Ed__28_MoveNext_mE2992BFB46A9D1422694FD490D3472740DAB2356,
	U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4F8FB0706D98F36F5870A48944ED8AF73ACD41AE,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_m3E308821787605440D93A440EDD66A5F852C22F2,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_m7EC0795415E3F366BCE7F0B6E920D6299E26BBE0,
	FMNetwork_Demo_Action_ProcessStringData_m7A2886FD3653BC709B033256872A04C17CEE2272,
	FMNetwork_Demo_Action_ProcessByteData_mC38EB4AFE7746483DA6D88A635DD6D1F45238EF2,
	FMNetwork_Demo_Action_ShowRawByteLength_m92F49EB9840876C51961CDB7BC026EFFBEE4F314,
	FMNetwork_Demo_Start_mD8433EB4B7A4A007090CC49906F40D8D1CCF8A65,
	FMNetwork_Demo_Action_SetTargetIP_mED976053F751BA1E6D08C813293D05D2D88F8842,
	FMNetwork_Demo_Action_SendRandomLargeFile_mC543B8FA119F65E56C07D670BDF7C60D7498787C,
	FMNetwork_Demo_Action_DemoReceivedLargeFile_m8825524BBE69F10E24F95E39F5AF69DBD0D6E588,
	FMNetwork_Demo_Update_m2B4BA43EB9BF273EF89EE5B1A6E332C160DFF23B,
	FMNetwork_Demo_Action_SendByteToAll_mE3BE1EAAC7F2B036DC2DF18B198C89B32F3F8BA8,
	FMNetwork_Demo_Action_SendByteToServer_m4FC261F8F2030734782D4DD9A5540379D09D900E,
	FMNetwork_Demo_Action_SendByteToOthers_m7F72C78765E3DD638E6B7DD879415179D72C6505,
	FMNetwork_Demo_Action_SendByteToTarget_m622243264F3EE20F1089A8C88364300C06D9B2AF,
	FMNetwork_Demo_Action_SendTextToAll_m006FA91BD4E2DAEF7CBECA9886F0D5490916DF66,
	FMNetwork_Demo_Action_SendTextToServer_mD921C4272C60C54BA1F92475AFD5773EEB195D86,
	FMNetwork_Demo_Action_SendTextToOthers_m8DB963E93D1CEF370C6AA5BEBCE981412C02CDC9,
	FMNetwork_Demo_Action_SendTextToTarget_m85CEB4B1C11C4D094062EA8A620064C12EC363A2,
	FMNetwork_Demo_Action_SendRandomTextToAll_m9A47DFFA21B7B8E5348E1E45393F219D6819D017,
	FMNetwork_Demo_Action_SendRandomTextToServer_m126799FA3059FE97D9B8F7E721DBB9ED6C459C04,
	FMNetwork_Demo_Action_SendRandomTextToOthers_m9128ED8777F95C4E099DE1F5683A3E3D61EAB3FA,
	FMNetwork_Demo_Action_SendRandomTextToTarget_mEC42914678A9DE97D29FB96A7A5C2C88D8FABE58,
	FMNetwork_Demo__ctor_mFFE4C215963020C8A1AE5149CEF7A5726685F391,
	FMNetwork_DemoAnimation_Start_m0A02B683C9F5FD5DFBE542BE0F37D587104AEEC5,
	FMNetwork_DemoAnimation_Update_mF484EB21E515F55FF49CAA30FF5BBBD519539139,
	FMNetwork_DemoAnimation__ctor_mA67A4914F35832EB07A70C8CB4E7781AF20E77D7,
	FMClient_Action_AddPacket_m6E0E5EEA363C5366B8B6AB731B8A5C47EB9F6D5C,
	FMClient_Action_AddPacket_m580FBF73630A5FC570036342E9A5E19EA6E1521B,
	FMClient_Action_AddPacket_m1B65C3B1F1AF6D1A5260193A1E356D799C68E513,
	FMClient_Action_AddPacket_m71648492CC5BD5B38719AB2EB8DCB0852B06DBAF,
	FMClient_Start_m7EF75BDA3F900D60B93D4CA48E37373CB2C98D52,
	FMClient_Update_mC0B21DB773CB78C46F10612F99FD856895E08D60,
	FMClient_Action_StartClient_m63159EE5DB88F36D586637A1B5FD77EC0DDFAAD5,
	FMClient_NetworkClientStart_m5E27078E24274AEC8230AFA4937615DC4601548B,
	FMClient_MainThreadSender_m196A72144F5000BDFB23C783D44BC2DC14E812A8,
	FMClient_Sender_m9858E1229F3183FCC17477AB0DA0F77F1970C883,
	FMClient_DebugLog_mFC9A94DBE93B5A642AAFEE1AE775383D3A7153AE,
	FMClient_OnApplicationQuit_m8E0563113DC634B4093271417FB17A181C95A261,
	FMClient_OnDisable_mB222D1B570190BC929C21218FEAEAFC3AF0D37D3,
	FMClient_OnDestroy_m97FEC1C579488DC892E8001B463607A3019361F6,
	FMClient_OnEnable_mBF31D51BB11E190FDC7764AAF1E975C6995249B0,
	FMClient_StartAll_m5383DFF40567DC16485F046FC4DC389507F2898A,
	FMClient_StopAll_m3AA2D18DD3E271369ED3341110D0CDB4341EE55E,
	FMClient__ctor_mF3630922996062720429DAC8AAB0CF54A9A81F39,
	FMClient_U3CNetworkClientStartU3Eb__28_0_mBBEBC74609803EA5F8DB22F41F196745521CBE5F,
	FMClient_U3CNetworkClientStartU3Eb__28_1_m43CFE2754BF5A51CA7F909A39580F2DC8E5ADF0B,
	U3CNetworkClientStartU3Ed__28__ctor_mD0C05F1F39BBABC6C0D70C7B30F89B6E3D0EB2B9,
	U3CNetworkClientStartU3Ed__28_System_IDisposable_Dispose_mF2BADF251C9477A3B1E70B3DDC1B7CD5358EC8AE,
	U3CNetworkClientStartU3Ed__28_MoveNext_m88F6943556E00A3B7D997E8BE0094CD44CE7EAE5,
	U3CNetworkClientStartU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D0C5EDA57496BEBA1CFFBAAB14A5F49E651AF5D,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_Reset_m24251982DA4EC8B562502778BA2EF43A417E8D50,
	U3CNetworkClientStartU3Ed__28_System_Collections_IEnumerator_get_Current_mC5E19B6425665B16DDFAC1F5BE654B1C5271CAAF,
	U3CMainThreadSenderU3Ed__30__ctor_mAC384BA67402126A0885FD1AD8A1954E829FBF68,
	U3CMainThreadSenderU3Ed__30_System_IDisposable_Dispose_mAFC05B450E4C58A6E68382D4335EA28161B2E52B,
	U3CMainThreadSenderU3Ed__30_MoveNext_m8B3F6AEF66AE4002C42370A90CB432B96D221D5E,
	U3CMainThreadSenderU3Ed__30_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0C134E2E4D965A7F2156A4C7BF03492BD14BD8DF,
	U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_Reset_mB5A814F21BE08CFA7F8652A198D9B0C871BB1736,
	U3CMainThreadSenderU3Ed__30_System_Collections_IEnumerator_get_Current_m8A2558B8300492A3C4BB912E460E30343133F346,
	FMNetworkManager_LocalIPAddress_mFF5C5D3101F680EAD193F0B8AB14E72180446D88,
	FMNetworkManager_get_ReadLocalIPAddress_m1734E265C3775E22F619B798BF4B2D1FE2E44B93,
	FMNetworkManager_Action_SendNetworkObjectTransform_mB21BA52BB427E2937CB4B0D59F2AE4488058E8B4,
	FMNetworkManager_EncodeTransformByte_mBD3C68CA6F4AF2CCFD3C61CDA2CBA9F9D30B363A,
	FMNetworkManager_DecodeByteToFloatArray_mDA7BD48F3E977D615F91E2A0E0557AFB779691B3,
	FMNetworkManager_Action_SyncNetworkObjectTransform_mBDD78DE536A6FD33666F850F4E5FEF0EC0071C70,
	FMNetworkManager_Action_InitAsServer_mD75997CBFB7810E3BA74F3B92AC4F841500E93CD,
	FMNetworkManager_Action_InitAsClient_mCAF26F52C065A121D76F7D38A46189D68812CC26,
	FMNetworkManager_Action_InitStereoPi_mC46FAA867B3D47E404AD63B55CB22C4F5CC30FDA,
	FMNetworkManager_Init_mDF26E04C4250C31C3CDF3E895D97BCA345543117,
	FMNetworkManager_Awake_mD0648E4D22E948CFED1E76BF694150FBA6CCB2ED,
	FMNetworkManager_Start_m731AE51B5EABFE66D44FB8CEE255AB457FB97262,
	FMNetworkManager_Update_m6F03B1E8BF42A1CE516732FBB8A4D6CEE21CA5F7,
	FMNetworkManager_Send_mD1CE1D270FB72C5A021C84B173318245264AE344,
	FMNetworkManager_Send_mA1E79ACB3C1202E3683D2AB618EC6D1DC86D27F3,
	FMNetworkManager_SendToAll_mAC74B498156B51511CE7B8AD00A306E383001FCE,
	FMNetworkManager_SendToServer_m680FBCA306390F1AC9CCAFA028A82AA5627FBFF6,
	FMNetworkManager_SendToOthers_m733696729DB2551537EFD2E773A724EB8EE5CF4E,
	FMNetworkManager_SendToAll_m6ED1B08A63EE6CFA249B6CB0B876F36AD9276CAE,
	FMNetworkManager_SendToServer_mDAD550229F4DF9C8F51FA4D00616BB4E3204AFBB,
	FMNetworkManager_SendToOthers_m76F6D91817597631893088F5D096124B6BCDF4E7,
	FMNetworkManager_SendToTarget_m8383491A137B24E5CA3221B1E46A9AE0FEC37CA6,
	FMNetworkManager_SendToTarget_mB9CDABD8072BD4F65E18F3F04D50214A9D4F84D1,
	FMNetworkManager_Send_m6D07FC063EF1F6AB020FD1A67653A5650F5F62CE,
	FMNetworkManager_Send_m87394D847F9E011E967D68341A56259D5AFDD18D,
	FMNetworkManager_Action_ReloadScene_m428B4CE135C87C69D4A30986A69E37BADBA8030D,
	FMNetworkManager__ctor_m255207BB8A01D0DCBEDACEF4FBDF1E91B5FA9050,
	FMServerSettings__ctor_m64A2752379B3561CF097041AA6099A4F2C558270,
	FMClientSettings__ctor_mE0AFD7FD7FB9C13E7097068E56458ADD1094B0FC,
	FMStereoPiSettings__ctor_m4D1EFA321536E48A1A6A9AC4C477BDC2E8BB93AA,
	FMServer_Action_CheckClientStatus_m697DDE3F22C5F94E96C19FA542AE624D429F5B80,
	FMServer_Action_AddPacket_m8B6FB971E0CF10570AEA2D893F76C8FFBB48CC26,
	FMServer_Action_AddPacket_m07633FBC0C78F4DEB4E5213FBF8A6E7F425AE294,
	FMServer_Action_AddPacket_mF05DE2811D4EFE814FAF5F09E6E57C330863E5DA,
	FMServer_Action_AddPacket_m9403C788A623023BD3D60777956E616EF07E8A07,
	FMServer_Action_AddPacket_mF8F31D6960D0B7DB6C13862B7C9605536EFAB8D9,
	FMServer_Action_AddNetworkObjectPacket_m2BAFF3FEAE295A507FA72F9B245CA19175CE78FE,
	FMServer_Start_m91ACB0E99A6AF95E484BA364CF6F88C643206DB9,
	FMServer_Update_m81796254B4462FA3D945D9AD86A2868877115C5C,
	FMServer_Action_StartServer_m339E23A122EF8AFCD0470F20BB1D5C593479343F,
	FMServer_InitializeServerListener_m4B19FAA611DDBB51DB70F52DCBA8AADFD0DC20B8,
	FMServer_MulticastChecker_m13C1C2475530617B7142AD568298BE2AE966CFB7,
	FMServer_MulticastCheckerCOR_m3A46927DB28F9946F4C899AF05151ECDFBAC7B8F,
	FMServer_NetworkServerStart_m53F1BB0509D324B11975F677FD07742D0973E5B8,
	FMServer_UdpReceiveCallback_mA83C94A8724BAC988799466EA13E1E27565F6F8F,
	FMServer_MainThreadSender_m70FD3C1A08C40D256E082E45E453BCC963A27828,
	FMServer_Sender_mCE3BF4BD1427311E5F5D5820AFE31DB239D29531,
	FMServer_DebugLog_m4CABEA630DD0DAB46FF89CC0DF8EAD4236B1E13C,
	FMServer_OnApplicationQuit_mC120710981FE52A740E7DE7407F98DEF20D0FED3,
	FMServer_OnDisable_mFDA76628E20DB16C1C299CF6B525F3470BF5650E,
	FMServer_OnDestroy_m754D033F470CC9DE3B5CC31A5CF4B6841553D230,
	FMServer_OnEnable_mEE044CFBE1D9A976AAF900029C34CA170E9A0F53,
	FMServer_StartAll_m552518A1DCE75B2C60B437502696AF77B7032A3F,
	FMServer_StopAll_m3B4D785EF1D5476CEB50CFF471F63FA3C6ED77F5,
	FMServer__ctor_m3F38AE880D79741720AA0CDACD5E7C2F1AF6A748,
	FMServer_U3CNetworkServerStartU3Eb__33_0_m019FF8207F1A320EACED7C2B21ABA5011000D0BC,
	FMServer_U3CNetworkServerStartU3Eb__33_1_mB8A5642B352FF0140ACCE2724A57FDD2A3F06E5F,
	ConnectedClient_Send_m5124596DFBAA7F50F7CA750322BDDFEBC0D4A8A0,
	ConnectedClient_Close_m0F38D5BBADA3E8929A01703747D5F00548BF15FA,
	ConnectedClient__ctor_m6BC6FA9C0877507C72C93EC74E03912402AB92BF,
	U3CMulticastCheckerCORU3Ed__32__ctor_mEBE29B27242A666E5DC2E35C2205FD9508AD4B56,
	U3CMulticastCheckerCORU3Ed__32_System_IDisposable_Dispose_mED7730E279EA6D79015C9B7AA9C9A54FF8B918CE,
	U3CMulticastCheckerCORU3Ed__32_MoveNext_m36C8B3214F59CA2317C98A17821A19C43AAEB535,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6D3E8BF7F56103F88160A2C35C110293D2801A52,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_Reset_mB2BFAA304CD76CBEBB072F14C2E754797C6FDD5D,
	U3CMulticastCheckerCORU3Ed__32_System_Collections_IEnumerator_get_Current_mA23AE4B1D341719C24DB531D037D1434C1196159,
	U3CNetworkServerStartU3Ed__33__ctor_m972BFA66770E884FAFECCAC141E33029EE25D1FF,
	U3CNetworkServerStartU3Ed__33_System_IDisposable_Dispose_m380952EF87F5E0FF391FA61DF5DC181C681918F3,
	U3CNetworkServerStartU3Ed__33_MoveNext_m9341645BB9BB9D6FD2C78FEE8CB544A3097A736F,
	U3CNetworkServerStartU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m724209347BFD6AFD23815CD817EA2782FF8994C2,
	U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_Reset_mC52F22569BF00478381E1042BFEA8539C62C9AF7,
	U3CNetworkServerStartU3Ed__33_System_Collections_IEnumerator_get_Current_m1C8242006350B74D05CCDFB1487A843014D0C282,
	U3CMainThreadSenderU3Ed__35__ctor_m9D0709E5C8EE5A30AFBC6500387A9355BA2B6220,
	U3CMainThreadSenderU3Ed__35_System_IDisposable_Dispose_mDC8120E6FE2B752B263CAA0006654F67108CE68B,
	U3CMainThreadSenderU3Ed__35_MoveNext_m5B24123688606EDE726867C8378A5776987FE63B,
	U3CMainThreadSenderU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m701C2588F8EF921C4303758C33A5CFBB85268914,
	U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_Reset_m0E92ACA965A1272BB82B14AE0D16119F8FCC51C6,
	U3CMainThreadSenderU3Ed__35_System_Collections_IEnumerator_get_Current_m7C6243AE661C6462A0EFA63461CE446F2106354B,
	FMStereoPi_MulticastChecker_mE6337322229810E52A878C54106CD0D2DFD582BA,
	FMStereoPi_NetworkServerStartTCP_m340A32E63D94C88F5D78E8B645153A80A273FC9B,
	FMStereoPi_TCPReceiverCOR_mE3E982BE3AAFCAA6F0247CA91A574BC4016DBF3A,
	FMStereoPi_NetworkClientStartUDP_m74ED068D5CA02A0B2A4D56C103A3DF94664BD35E,
	FMStereoPi_Action_StartClient_m7AFFEC5FBB05195B1C061D091EF40E8B6CC3698D,
	FMStereoPi_Action_StopClient_m42A4914C65C39BF9BDE18B33995E7C78D0C7B866,
	FMStereoPi_StartAll_m4DD907458FFACEFCB565F0D7E54AAA84297F1F08,
	FMStereoPi_StopAll_mBFA8A3D1577D67DA86416C286C405C45DABDD1F0,
	FMStereoPi_Start_m5C56E43A962EB2AB3142F131873E31B6C67C1252,
	FMStereoPi_Update_m669664D93070B477ECB1248013E98330F84C806C,
	FMStereoPi_DebugLog_mEA2F453091F62DEB2AD6AE5A10033B3D976E1BA1,
	FMStereoPi_OnApplicationQuit_m8D66AF5414B77B0A4BCDD4FD7F90C50199261665,
	FMStereoPi_OnDisable_mE178085A7511A88A5427B1403261910E76CD5AE9,
	FMStereoPi_OnDestroy_m05C6536B8E44AFCD384C4E2A27D51A1DA7E793C6,
	FMStereoPi_OnEnable_mA412A52C0D3D96BE2849529461109A93A32B861C,
	FMStereoPi__ctor_mC4389F4793CA039689A359AAE34B12E48B16A6CB,
	FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_0_mDE846926D20096AC69C860B2CC892E9EB5201017,
	FMStereoPi_U3CNetworkServerStartTCPU3Eb__17_1_mD393BF6020BF61F547EDF0B19A7A5315D8C81799,
	FMStereoPi_U3CNetworkClientStartUDPU3Eb__19_0_m31CE99B57F1168691E3675227BCE9D7735B38D8F,
	U3CNetworkServerStartTCPU3Ed__17__ctor_mD2F4B52884AAAC2EAACEB55877EEB9C49CFAB6D3,
	U3CNetworkServerStartTCPU3Ed__17_System_IDisposable_Dispose_m27ECC5885334F25784BD9A2BF7E1F1A8CCDAA3AB,
	U3CNetworkServerStartTCPU3Ed__17_MoveNext_mC7925593F077ECE3B5574BDFAD4DEF45D51FF8D6,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE3E5EE41438D96AB95C8ABE8B77BB8A86E0005D1,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_Reset_m576FFCB7C06814A17D54A5C3F36AE30327C79F3F,
	U3CNetworkServerStartTCPU3Ed__17_System_Collections_IEnumerator_get_Current_m3ACAB37C2F1469D1DC9EF6B0A6C483C2CC618FD3,
	U3CU3Ec__DisplayClass18_0__ctor_m30EACA80AEB3694A078D9ECFCBAD51AB8E8AFD30,
	U3CU3Ec__DisplayClass18_0_U3CTCPReceiverCORU3Eb__0_m1562A138DEF249C8159E454A2CA88EFAB97DE843,
	U3CTCPReceiverCORU3Ed__18__ctor_mBB60D670782FA0DA8072E1EF545AE0E2CCA8824A,
	U3CTCPReceiverCORU3Ed__18_System_IDisposable_Dispose_m80F58A7EFDFE8F6D6FC425EE4A4B23123FF35939,
	U3CTCPReceiverCORU3Ed__18_MoveNext_mFA1B27720A1B2BA4683FD60B6DE24B2A189640B0,
	U3CTCPReceiverCORU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m097C55C143D8A44C8E33887270757FB86F5641CE,
	U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_Reset_m8C6625E3E247A26AAD989B44FD4F284701951CE7,
	U3CTCPReceiverCORU3Ed__18_System_Collections_IEnumerator_get_Current_m4927905AEEA2663D8194895244E5C56583E9EF90,
	U3CNetworkClientStartUDPU3Ed__19__ctor_m3AB53F056F41B2ACABAF62903AB2415C6EDE2860,
	U3CNetworkClientStartUDPU3Ed__19_System_IDisposable_Dispose_m545790542E4EC408530C6BCDD0E9439A1A2CEA47,
	U3CNetworkClientStartUDPU3Ed__19_MoveNext_m41F1F39461142620FCBED9A8A72E0DEDC2193520,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m85409D5AA7C4F3CBACA5B77FC376B696C19D5100,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_Reset_mFD196C3E0E04341F0AB7074F52AE7BFF496EDEDB,
	U3CNetworkClientStartUDPU3Ed__19_System_Collections_IEnumerator_get_Current_m649785D485B0CB7DBCBFB913A00029BC129DA1FF,
	LargeFileDecoder_Start_m5E787EE1377276EC1ECAC717F61F2D41251C61D4,
	LargeFileDecoder_Action_ProcessData_mEB5A97A33C2FC65E98D093904347A4FA54FD2D5E,
	LargeFileDecoder_ProcessData_mEBF37621E853E17BB32619C02ED43CAB8C802369,
	LargeFileDecoder_OnDisable_m2C226054369BBFC62926B83C6EE42A3AA7B586DC,
	LargeFileDecoder__ctor_mA7F48FB18934D4C52414C5BF8FB3A2031F214620,
	U3CProcessDataU3Ed__9__ctor_m1D8753BB750B2BDBF34D090FC1AF8F54296A37A7,
	U3CProcessDataU3Ed__9_System_IDisposable_Dispose_mB8EE0592DB225F64A12DA26C9C2A96AC5994BB18,
	U3CProcessDataU3Ed__9_MoveNext_m9B87121401483778192C4EF68E21436F4A85ADFB,
	U3CProcessDataU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0D0BA7875E1CBA0DAF864D3C4F1DCCAAC19AB24D,
	U3CProcessDataU3Ed__9_System_Collections_IEnumerator_Reset_mE2F20B95EACBEB68F53ACEDDD1F6FECB7DD02488,
	U3CProcessDataU3Ed__9_System_Collections_IEnumerator_get_Current_m83F846C2A6C24061431D5F6CCF8E1E096430590D,
	LargeFileEncoder_Start_m4B8724341015C59F5476826EA1266AFB51B0F992,
	LargeFileEncoder_Action_SendLargeByte_m388FA72B2A0E4638C55FD7327ED14FF5DCB73D72,
	LargeFileEncoder_SenderCOR_mE44CB8852EFE29B44BE72ED1A0B777B585CE0394,
	LargeFileEncoder_OnDisable_mB07BA3442A13E8170209FB15D71DFA60D6B68F57,
	LargeFileEncoder_OnApplicationQuit_m5C59A630FBE659DA934B25ED543915979A810ABA,
	LargeFileEncoder_OnDestroy_m5AF3FD1F49F86FEF9E54C96E5FBA8DB51D79D7F1,
	LargeFileEncoder_StopAll_m21F4C498FEB1A921681BE3FA3D95957BB0F79E24,
	LargeFileEncoder__ctor_m4E846FFA06F2B85DC7B34704AD0E8D54A10F41C4,
	U3CSenderCORU3Ed__10__ctor_m979630D72EB333BD3D559E23D89A6C41034D73F3,
	U3CSenderCORU3Ed__10_System_IDisposable_Dispose_mDCFF2339BE6134EADF67579FE2B4E5A0995BABF1,
	U3CSenderCORU3Ed__10_MoveNext_mF4F366F5F8BCC982F47507B4E827ABA1A61EC626,
	U3CSenderCORU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6718863C0F3BF554552C0E21BF45F5018F8D24C,
	U3CSenderCORU3Ed__10_System_Collections_IEnumerator_Reset_m73B5BEC5CCEE11A1012F906BAEB1A6913FB4697A,
	U3CSenderCORU3Ed__10_System_Collections_IEnumerator_get_Current_m928A9082517545E01F08ABDE8D4F85B8489208B4,
	FMWebSocketNetwork_debug_Action_SendStringAll_m710AA9D18C9678FBA3E386242EE961E596B2738C,
	FMWebSocketNetwork_debug_Action_SendStringServer_m3DB4A603C84EC6ABB5CEB4BA1D12D37065C75055,
	FMWebSocketNetwork_debug_Action_SendStringOthers_mE93AF6930EA1511FCC58E3781578EB780DB5609E,
	FMWebSocketNetwork_debug_Action_SendByteAll_m64C0D4E3AA4E5072F2DBC53D27B50CD0638130F5,
	FMWebSocketNetwork_debug_Action_SendByteServer_m1B22593913893A54DE6F962F4443E734C455326E,
	FMWebSocketNetwork_debug_Action_SendByteOthers_mA3692507372718FE9170E4886799F75F8A5B93AF,
	FMWebSocketNetwork_debug_Start_m3549CDBD49A97D04753A2B15D7BA4114505722E2,
	FMWebSocketNetwork_debug_Update_mF36F69582E94D602BBCACCA65233E7C2DFFE603C,
	FMWebSocketNetwork_debug_Action_OnReceivedData_mE613FFB040B6376DFFEFB4250DE37B0E4CC7752A,
	FMWebSocketNetwork_debug_Action_OnReceivedData_m617A4C13DCA225022BF3C8AC858904C301DA69BE,
	FMWebSocketNetwork_debug__ctor_m6EA7FB1852EB66109002508769CCB878C5518608,
	FMSocketIOManager_Action_SetIP_m38911A13EEB9F983FDFD2B9F21671633428A8AD0,
	FMSocketIOManager_Action_SetPort_m7B100CDBA1ABA01B8154F318C49FBEE920E11EF1,
	FMSocketIOManager_Action_SetSslEnabled_m7CDDEA8981B461C9137BD0CC12E8020408063501,
	FMSocketIOManager_Action_SetPortRequired_m8D119A6B549E65FEE8EC8445A1C3B2007662B5DE,
	FMSocketIOManager_Action_SetSocketIORequired_m02269CAA46FFF3A7D3124E2C6C97E2DE6AC58CEC,
	FMSocketIOManager_DebugLog_mF393F44FCAF797283BFDE20104402C1E2F7A2807,
	FMSocketIOManager_WaitForSocketIOConnected_mC9DFC3F62F87645F9CF82654280DCBC590D62BB0,
	FMSocketIOManager_OnReceivedData_m9DE3C68690679FCF87DEB0434F21E6CE38D90488,
	FMSocketIOManager_OnReceivedData_m8D9F078E65AD33C8CE6E542E7DB7A6E15ECC6386,
	FMSocketIOManager_Action_OnReceivedData_m85951BD1CAA66149C7EFFE9397BA7C72DECB6C90,
	FMSocketIOManager_Action_OnReceivedData_mCB7DE948C9918409D115C764DE79BB2C7D78C866,
	FMSocketIOManager_Send_m20E19ECB679394E9F69B344469D8B684E3729342,
	FMSocketIOManager_Send_m3F7FD03DE01D2982ABDB56E5E5B37E4B163EEEED,
	FMSocketIOManager_SendToAll_m4C15155F9CBF231D3C1944A54FE55A7510D14883,
	FMSocketIOManager_SendToServer_m1AB99649FBD70015F4787CDFFA1B71A719AFC4CF,
	FMSocketIOManager_SendToOthers_mD8F13D9F1E857F9FDAC64135CBD3173E72D4C7B6,
	FMSocketIOManager_SendToAll_mE136CA2E7413C8FED36BCF1198E7258AF575BDEA,
	FMSocketIOManager_SendToServer_mED9E12DF8192F5615B5A4EEC901D6F0E733FF40D,
	FMSocketIOManager_SendToOthers_mB53EAB2B343442048B4B867DA052C1DFE6851391,
	FMSocketIOManager_Awake_m7AD2C757EC3E419F2CB37C288746EA6DDD5BE465,
	FMSocketIOManager_Start_m058F620AD42078333D93DB3DAC1AFBB4C4363DE2,
	FMSocketIOManager_Update_m093EE4B34A0F8C8F8B20E7E93D8974676E5B07A8,
	FMSocketIOManager_InitAsServer_mBDA50ED9CA25055F865FD0D596B3D53B398C272E,
	FMSocketIOManager_InitAsClient_m0C21E1BFA6FCFCE9ADE2000568C5EE5EF87985B1,
	FMSocketIOManager_Init_m10ED4BB67ECA9C071357F9F61EF22DC4B8137DB5,
	FMSocketIOManager_IsWebSocketConnected_m4B040DE7E90AFFF58C65B8DA413BC93F97AF10DD,
	FMSocketIOManager_Connect_mD43366D4F33D6FFA5A52689F657870028FBF5E55,
	FMSocketIOManager_Close_mFD37852F1355930A749967DCA7FCBDEC30466967,
	FMSocketIOManager_Emit_mF3D80A5715A7B189EEC6265A95D5076A1A2C3ABC,
	FMSocketIOManager_Emit_mC6719E1E45F72F0860005BA0D7C28FFACF983D69,
	FMSocketIOManager_Emit_m7E2F16793986058BA70EBC0FEA342FB4C5521733,
	FMSocketIOManager_Emit_m63A707CA81569DE6CBDACEFF59F08D61671C4140,
	FMSocketIOManager_On_m97401A83C13774A5DB9CBBCA699B1BF57AB1B8B9,
	FMSocketIOManager_Off_m327F0CEC43F76C072725B00346B0B23CFB8CF36E,
	FMSocketIOManager_OnEnable_m7E06DEEED0061D3AF7DC5313371CB2320F1D3D6C,
	FMSocketIOManager_OnDisable_m90A181726FA88AD67EC022F5868EF12234BC295A,
	FMSocketIOManager_OnApplicationQuit_m2FA72D0DB4CBA1C09E3BC49FAF50C7C4E3940DE9,
	FMSocketIOManager__ctor_mBDFA994C593B5F265CBEFA26A647C5EA77830EF8,
	FMSocketIOManager_U3CUpdateU3Eb__38_0_m6F5519D94A6940CD3CB3C6B7A81DE63C302332D9,
	SocketIOSettings__ctor_mA87553DEE71CC2BD0F04E1AA42604BF8F2D7C83A,
	U3CWaitForSocketIOConnectedU3Ed__23__ctor_m3236B60F9A2EB434CF870582C147666F22977B5C,
	U3CWaitForSocketIOConnectedU3Ed__23_System_IDisposable_Dispose_m49F83C457661CEA76D51CD99003472635CEC0824,
	U3CWaitForSocketIOConnectedU3Ed__23_MoveNext_m7772FFA73D1FA5B6E862FEB6246E6B74A084D7A3,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m97EF547E3759158A1EC60A04AE1398701DC4365A,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_Reset_m6291606BABD352B6800CE7C5637AF5EA2094CECE,
	U3CWaitForSocketIOConnectedU3Ed__23_System_Collections_IEnumerator_get_Current_m6ABD17DEC39D75E49B5B43FFAFD37F26229B6491,
	EventJson__ctor_m578301E6CD602AA80AD29BDF12DF2207B12D06A3,
	SocketIOComponentWebGL_Awake_mFFD18ABE4AD2A927E929285A73BF03CA9C0DDAD3,
	SocketIOComponentWebGL_DebugLog_m5EB0712BE4C850ECA4A17292AC060CAD4E20083B,
	SocketIOComponentWebGL_IsWebSocketConnected_m2AA9F0DE669F40D6D62AE78CAA22DC4A48982C26,
	SocketIOComponentWebGL_IsReady_m3252F7F80254C13ED6449D0B9EF85E59E49ADDD1,
	SocketIOComponentWebGL_Update_mC7A9764C2735E42E26DC0C45ACDFB320447ADAF2,
	SocketIOComponentWebGL_Init_m9BC833009A299D73291C27774367696C18C94009,
	SocketIOComponentWebGL_OnConnected_m9DB7A8704B9112D8A74DEC2D5D619904EFF0509A,
	SocketIOComponentWebGL_AddSocketIO_m531A7D0E1FB1EC7CA45EE8941C3E4FB29D10C859,
	SocketIOComponentWebGL_AddEventListeners_m9405B4EBC3F81FC27D37526265091EF2B16EBA53,
	SocketIOComponentWebGL_Connect_m27B3028A02989F528EE6D7196B20C52340C00981,
	SocketIOComponentWebGL_Close_m85C247694C36C9E9BF0B4BF0D5BB021E5C0BD39D,
	SocketIOComponentWebGL_Emit_m0253F2AA8EA759A4FE106B67B510A1595240C6B6,
	SocketIOComponentWebGL_Emit_m9CC2BA989466F8B011B65051CB8C19B4276491C5,
	SocketIOComponentWebGL_Emit_m8367518E54945C5467AEC7EA699063F7E7FE5E0D,
	SocketIOComponentWebGL_Emit_m499EBAFFDD6BDBA3EACAAB2B0FC0EC24B4080097,
	SocketIOComponentWebGL_On_m50A1393227149A7F64377545FB57A2418C081E45,
	SocketIOComponentWebGL_Off_m7B84AEE2E20769A96135CE1E29A8EDA51296A433,
	SocketIOComponentWebGL_InvokeAck_m3A07EBA5960382A5358A626D419ADF3DF403BB2F,
	SocketIOComponentWebGL_OnOpen_m4FA0157C82D243643B6A63180E9B6927A2A8F8A6,
	SocketIOComponentWebGL_SetSocketID_m6600E6EA388CD6E21A0F6DB953FF0FF4512D744A,
	SocketIOComponentWebGL_InvokeEventCallback_m321CAA1AB04A660A9DC19BD1A8D1D32D8E6254D8,
	SocketIOComponentWebGL_RegOnOpen_m0AB925DAF4BB84EA088752D3D1D429CD8C3370D7,
	SocketIOComponentWebGL_RegOnClose_m0C256244BF2BFA1D5A95977B102D5DD73E47ECB9,
	SocketIOComponentWebGL_RegOnMessage_mA860FE89E7B5236AAE352748447B0F85C5E0DB38,
	SocketIOComponentWebGL_RegOnError_m7606640BFC80E31102516BC5D8F6936407A12258,
	SocketIOComponentWebGL_RegWebSocketConnected_m8D21B5FDCC1C61496BA78B9585FE0EF4CD67EC48,
	SocketIOComponentWebGL_RegWebSocketDisconnected_m75D6345DF745FA0D435B669E4F9846F98E50048B,
	SocketIOComponentWebGL__ctor_mAF6B59F74804B5E81402F2902E7C2B449CBF0EE0,
	NetTcpCommandContentState__ctor_mB30B8111821F99DAE59D3A376E49688C2EB9D48D,
	ServerSender_Start_m5812E957F255B623FD61783241DC64221F9D1ED0,
	ServerSender_FunctionPlay_m4C15A74A0D6AEAC6F60EFB0C05B398F9E5970082,
	ServerSender_FunctionStop_m60E48C0CE6A8595185204126E19BFA8C8F204EB2,
	ServerSender_Update_m7A51483EA660D0D8C857D5C79DE39111F385749F,
	ServerSender_FunctionMR_m01C42C2239AD64FBB0C7BB18F7E23194C0825D75,
	ServerSender__ctor_m778C5D67E33926342310F6D46B2688253CF0248D,
	ServerSender_U3CStartU3Eb__7_0_m3FC2E125AECFFE5902D724285B39EF97437B60DD,
	ServerSender_U3CStartU3Eb__7_1_m8E999A233A9090B377098A86B8752196E4800398,
	ServerSender_U3CStartU3Eb__7_2_m6202A6242EA9131030805813921FB02B72053024,
	Randomizer_Start_mC4A70B1DCBC84FEBEFA238F22002B69C0DE644E2,
	Randomizer_Update_m40CECDF55DE2336D4500335F97D1D2844601B94C,
	Randomizer__ctor_m78CE5F7DC9A43B823EA742C8A4B246CC84363223,
	U3CStartU3Ed__1__ctor_m2956A75A24CA7BA18E319063A30F4A334A84A944,
	U3CStartU3Ed__1_System_IDisposable_Dispose_m86EB27051FD8CF0457ED59EA92C8E9C6DC8ECBDD,
	U3CStartU3Ed__1_MoveNext_mF07251FD51141E37FC667D2388865835BF37CCD1,
	U3CStartU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m78F8C1E52E2155D00A9EBBCE2B6F9B22F78B7996,
	U3CStartU3Ed__1_System_Collections_IEnumerator_Reset_m68F8B70553C3C1230AAB2EB4A437464E6160C054,
	U3CStartU3Ed__1_System_Collections_IEnumerator_get_Current_mB25A3BBFCFC9C4322B76D61E6E07249287267B0F,
	SourceSelector_Start_m8067A37546F88DB7AC2F30A0C443B932C2B1CC0E,
	SourceSelector_Update_m31F2A6A307C7035510CEC2EBEA9D75858BB04563,
	SourceSelector_OnChangeValue_m7FDB0914CDAEB00915FA01539B996593377E1DF6,
	SourceSelector__ctor_mAADF270CF83E1F0BB22002D010121725755E3B28,
	Readme__ctor_m23AE6143BDABB863B629ADE701E2998AB8651D4C,
	Section__ctor_mE73C1D6AE5454B5A67AAB04CAA5144A5CA0B0D96,
	VideoPlayer_Start_m7E53175EE0C7A14AEDC282140539EA796E789B2F,
	VideoPlayer_Update_m0349DF5F35E9BEC308B4D436BE343155F39691D3,
	VideoPlayer_GertDuration_m539E5D915BC544E8C09EAA9354E195376288D2DB,
	VideoPlayer_GetTime_mF0C75D3173DBBDF2F9F0FD7FCC1FE2A288C759DA,
	VideoPlayer_SetTime_mBC35E6A475D4D14CC3C3377CA96236ECCEB66949,
	VideoPlayer_Play_m26CA5DFB768F5A2B5B5BCBA07072CD76EBA6BC1A,
	VideoPlayer_Stop_m91E4E92710505D710713B03DCCBB88C767266A4C,
	VideoPlayer_Rewind_mED6A835D6A134D17F1866E9BDBD6EE42E3EA96B4,
	VideoPlayer__ctor_m413EFC9FAAA8D61D3E811C27882A7DE2DD785528,
	U3CStartU3Ed__7__ctor_mB34FEAA7BD5BC71B9EB90A424F0B650DE6C6F532,
	U3CStartU3Ed__7_System_IDisposable_Dispose_m3C7A251A2596B042F84254FD9D200FA51C46FB1B,
	U3CStartU3Ed__7_MoveNext_mC214364B7BB57009C58C36334BB31B4C5A33E0FE,
	U3CStartU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7E765C7F2FAC8D85FB50FC7E413AB9A153119DDB,
	U3CStartU3Ed__7_System_Collections_IEnumerator_Reset_mFB4759DEE674598DD4D804CB3967D01FAD3B0C52,
	U3CStartU3Ed__7_System_Collections_IEnumerator_get_Current_m0F2862D46B86E7C2F279201428924BE97768F456,
	DynamicBoneDemo1_Update_m41DAB2BE429EC00C7D3D240549F88ED2570A6D48,
	DynamicBoneDemo1_OnGUI_mFC144CE71DBDFE9D2DE0D34043459D12C654F14F,
	DynamicBoneDemo1__ctor_mE314C3805FCC23162262352076FD53038CB329B6,
	DynamicBone_Start_m09C19C501BA86409F55CABF271EE4D02531099AC,
	DynamicBone_FixedUpdate_m7BE96A041089E847876DDC49C7EADD2BEECC58A5,
	DynamicBone_Update_m333CFF83EAA30B78204E71D01360BC83DFEBC354,
	DynamicBone_LateUpdate_m3342FA910227203865B0ECE67D97E0B5F4181D2A,
	DynamicBone_PreUpdate_m3B1C81BDA01FFE96BEA337D1D81F348A473270D4,
	DynamicBone_CheckDistance_m8A4721DB0EC629666DC0345231DDD1399D47EB9B,
	DynamicBone_OnEnable_m67C88D6F3E5CD3B2666BBFDBC6E9FEF3067E5AC8,
	DynamicBone_OnDisable_m7C4AEA3DF4853BE12AE6C44C7DF8B33BEA70BD1D,
	DynamicBone_OnValidate_m35B188AEDAE18394BF72414CBEC2E1A3A41B940F,
	DynamicBone_OnDrawGizmosSelected_m33D1FDD944F70FE42DB2B223DE4E66B16D8162C9,
	DynamicBone_SetWeight_mDFB5B187C9045D0993D0F7E38193BD1BB208CD3A,
	DynamicBone_GetWeight_m7DBFEB86F41488B34CC4A7DC8BB9164918917E60,
	DynamicBone_UpdateDynamicBones_m19D20412B5B593AB8842BEA66A73BC632066C012,
	DynamicBone_SetupParticles_m2536944256CB1F5DF4094988E26F56C48698878E,
	DynamicBone_AppendParticles_mCAF941296F86AAA1D6DA6164509A7276EBCBC049,
	DynamicBone_UpdateParameters_mD6D2591B0434D8032D72DA0832ED64EF73562B5C,
	DynamicBone_InitTransforms_mCB14EA48C108D0F027A1B53BC0464984B7E5408D,
	DynamicBone_ResetParticlesPosition_mD0010153723916A63B72C76AFFF9F6C018D7186F,
	DynamicBone_UpdateParticles1_mFE4535A12897CFFC1D8B7217559917246F37DEC2,
	DynamicBone_UpdateParticles2_m182515EFD4911688E00359EBEC74C87935A4C198,
	DynamicBone_SkipUpdateParticles_m5B1CFA3094CEE47F138C55CE64EC3420DE1B9423,
	DynamicBone_MirrorVector_m875346828B6E210900E42E98C7D78822A5736F42,
	DynamicBone_ApplyParticlesToTransforms_m0CC5F5474DAE8F3C47FA4513E7FC3228EB0F3CF6,
	DynamicBone__ctor_mAAF795E0041D7D4946C2A2FB67684568139D86D6,
	Particle__ctor_m3A4D31F4AB1CF6B8EA8E7B145E263072E2911339,
	DynamicBoneCollider_OnValidate_m7F5F03E8043DCE6D37B984BF0F3C117541899389,
	DynamicBoneCollider_Collide_mABC62EE87827B7A6F93E5B931B6F859015D7860B,
	DynamicBoneCollider_OutsideSphere_m82BC29B4E79F3F2C1A1DCC502807A0899AC5B196,
	DynamicBoneCollider_InsideSphere_mF314A452C0EF709506A7FE30A5BEA569BCE2B594,
	DynamicBoneCollider_OutsideCapsule_mBD831688B6A973ADF1A1824F319E36148314777B,
	DynamicBoneCollider_InsideCapsule_mF0AF0601862231180629E77D619A32B3E9F4A1BF,
	DynamicBoneCollider_OnDrawGizmosSelected_mF2A6F406B9D84EC5DFEBF4DCC0832F47C8D1E960,
	DynamicBoneCollider__ctor_m7A732AE491D455DE001411A537A8B182C712BD10,
	DynamicBoneColliderBase_Collide_mDA4A4598657DD2BA8EE0BFC694041372198F54C9,
	DynamicBoneColliderBase__ctor_mDAC26A2319B6459A424DA163F7ECFCB975C5279C,
	DynamicBonePlaneCollider_OnValidate_mB48770F578629D7D21F154E82F945D0BB97F259B,
	DynamicBonePlaneCollider_Collide_m0949ED810AEA976BD66F52FB295B19CF17CE8655,
	DynamicBonePlaneCollider_OnDrawGizmosSelected_m4C4D0053900941332B4710B619686596B9B2E999,
	DynamicBonePlaneCollider__ctor_mD159B7430A7D692D15A8B53336EABA1FD1416939,
	LiveBase_Start_m23AB136BFB0BEC2DB358C0BEC40E88B149BFC989,
	LiveBase_OnDestroy_mD09C85171DA9CD80059A495F17A8A7CE144D3089,
	LiveBase_BaseUpdate_m82ED0920F76B0BBC0CED6D24BC2B6274156CDA9D,
	LiveBase_LateUpdate_m36CD1A4226C12289B178F2B33F4ECA685863BABF,
	LiveBase_GetControl_m56175D218F38E6052A3D552B8C7FF2FB0B3C5DB9,
	LiveBase_Connect_m5CF0034B7F9E494D55C3D9EE177165F2E2447ABC,
	LiveBase_Disconnect_m673DB56DD91C8BD55A14A43DA9EBBEFD3D1DAD23,
	LiveBase_OnSettingsChange_mB9F4A675C741700C1AE3BD3ED8AE4F9C90641529,
	LiveBase_OnApplicationQuit_mF676E9838419E483F284666CE040B94795BA6578,
	LiveBase_OnToggleRecording_m079449E65CC3851246CBF715E014164A6B877F7B,
	LiveBase_IntializeRecording_m3970946F203CAA1F2118B951FFC3066146BC35AE,
	LiveBase_RecordFrame_m0CAF21C2D01247947ABAFC18028612A10E10C845,
	LiveBase__ctor_mB9F977350761317318D71E7EE9E4F666B903A38E,
	FTIAnimCurve__ctor_m9B2DC10FB02ADE0E30B5477470EE36D3F55A5EF2,
	Utility_CombinePath_m6C52ED1A44B56B77443C339707667FC49D3999B5,
	Utility__ctor_m50E58B4CD220E2DF1CAF80E8952B04226B78F6F9,
	MetaObj__ctor_mFB17ED0F8B7A16F3A3447CD092677E4583F09E52,
	Expression__ctor_mCE3E7BEECC6CE1CA3A12199B922E0A8B69324D95,
	Expression__ctor_mB702B1853EC9C774CE86E8082E9387525CCD77C7,
	Data__ctor_mCD3AD6E8D195C4D9716A6478E83258C452C89C07,
	LiveCharacterSetup_add_ReportError_mC40A1A6C54A3098BD86055536ECB698C213E997D,
	LiveCharacterSetup_remove_ReportError_m78449C1699B336404D0A0097ED74F251989633E2,
	LiveCharacterSetup__ctor_m1CCF969306C7C302F20233B178FB4030BDB8B22F,
	LiveCharacterSetup_Init_m8274C58A3C0E1F5A4A1FAC079F63233ACF4E76E8,
	LiveCharacterSetup_UpdateData_m0ABB58DB509327E97D5C69E6D894553D792CA7CF,
	LiveCharacterSetup_Cleanup_m6718A5412432B4C65D58A0840EECB5B1D37651AA,
	LiveCharacterSetup_GetNeutralControlValues_m3B89DA1004949F854FA8627BA5133062E234700C,
	LiveCharacterSetup_FindExpression_mCC2056319862A29142EE6C4C8CA33EFCFED69A86,
	LiveCharacterSetup_GetExpressionNameAttrList_mD827FD81BAE17E5521AF9D1EBB2FAA4E0EA8392F,
	LiveCharacterSetup_GetControlValues_m2925A56F38C9F2494F290F3CF9D78EAE2911D563,
	LiveCharacterSetup_SetControlValues_m83033E00F83AD58169973094A7842CA9F9B945E0,
	LiveCharacterSetup_GetControlList_m8A31F9595212F3265C557AE53D3A5867D40B1BFB,
	LiveCharacterSetup_InUse_m4D701BCDE7F299C11D16A64148AEE55A311BA97F,
	LiveCharacterSetup_SetInUse_m2CA403718833EFD44B197F4FAC6F9A7014FDB8AE,
	LiveCharacterSetup_AddControls_m27CEE2BCE8CCA61C1950D73D752C482C66EF153D,
	LiveCharacterSetup_RemoveControls_m106CD67F7D9370463F1CABAD5E493D0E52A9E2AA,
	LiveCharacterSetup_ConstructRigValues_mB577F73A32F396BCBAC66D3DB475468C29C47C1F,
	LiveCharacterSetup_QuaternionToVector4_m2F4BEA97AD2F21633F7243FCBB1C39017DA72386,
	LiveCharacterSetup_Vector4ToQuaternion_m7831A5648E7E6354FDE32B52570DFA855059D226,
	LiveCharacterSetup__cctor_mA56F2AF11059CCCEF39C22ABF0832A1FAF222C47,
	ReportErrorHandler__ctor_mB17B2B1B05596589BC2446B37E17E61376212D8A,
	ReportErrorHandler_Invoke_mB0AC953F35928F8D5C36C0E6CD7104BA30E2D3BA,
	ReportErrorHandler_BeginInvoke_m75FF2797011A23D4D11FC45EC2749453BDE0654B,
	ReportErrorHandler_EndInvoke_m3D3872629757888EEE5A5C957FA96A4031C78667,
	U3CU3Ec__DisplayClass14_0__ctor_m7C1489C2E19FA5AEF596A41E60E3664EA8974B95,
	U3CU3Ec__DisplayClass14_0_U3CFindExpressionU3Eb__0_m596DABE95B2295D9A048361DDBB2DA4A065D9F5B,
	U3CU3Ec__DisplayClass17_0__ctor_m8F19CFCDD1020E3CFBAB491D9F5BD564F77344A8,
	U3CU3Ec__DisplayClass17_0_U3CSetControlValuesU3Eb__0_m6B69D17E0D34F06B231F4DD081E10B463F012A4C,
	LiveCharacterSetupFile__ctor_m47BA844EE91EF75A3418187249001E2F162082C1,
	LiveClient_Update_m6D665AEFB5B4B9C36CD0ED23BCE17FED001F6A2F,
	LiveClient__ctor_mD4E6377106BD4E517CEC6CBE7922CBAD89D33E4E,
	LiveClientAdv_Start_m9697325283FE39ED4B2E5E21A005AA43F76CDFCF,
	LiveClientAdv_Update_mA9634B70E4E2B35BAF0B1C2A11E40F0C191A29E4,
	LiveClientAdv__ctor_mFB7C0D68B6E278766F095DB32F40D0E094A99970,
	LiveConnection_get_m_HostIP_mFC35E87F09A068BA031B574C53FE8CFB5AA63346,
	LiveConnection_set_m_HostIP_mFF2977990A63C47D693437C03DC33EF086D91417,
	LiveConnection_get_m_HostPort_m89B5CD503F28DD27D716BCFA4E3F8CD5D5772058,
	LiveConnection_set_m_HostPort_mB7A7040CFA7F670CE8C21344A40ABCB71939CF5D,
	LiveConnection_get_m_Reconnect_mCB25C0C4516D938D0DDAE29C37E97B497849220A,
	LiveConnection_set_m_Reconnect_m01631CBCA4A96D90E2A8492AEB87CED4AC5D338F,
	LiveConnection_get_m_DropPackets_mC796B5B6EC11727E8DE164D8AA3C418915D4C38F,
	LiveConnection_set_m_DropPackets_m935E4C562B60393217509B4015B8531196F4872F,
	LiveConnection_get_m_RecievedNewData_m0FF9C54F09BD79822765ECB110649A496A0ED831,
	LiveConnection_set_m_RecievedNewData_m526C95804D4DD1BC5E6391BBA7194915E4F1AE80,
	LiveConnection__ctor_mB7C9F396DBC4A1F934574CB7FB11014A8B2AEBCE,
	LiveConnection__ctor_mD6FC8CBEB130B4B13E8B456BDCD917A8468B9CC7,
	LiveConnection_Connect_mB07373B1855AD818DFB3B0AF88929DA337911293,
	LiveConnection_Disconnect_m9A088A3B00AA8597C6BDB23FE684F3CB2CAB059B,
	LiveConnection_IsConnected_m2229C7DD058959C3E841A97A9AFB6AA86D7D78E9,
	LiveConnection_BeginConnectCallback_mDB9493EC2ED3B6DEBF0D0F28378A07B43D8022A6,
	LiveConnection_GetNextMessage_m5F94BE973F766528AFEA8FE1EA71D78BC21F3F17,
	LiveConnection_OnNewMessageComplete_mB3005CC10DF4E0DC4D29CED92EA2AD761FEBEAD2,
	LiveConnection_GetHeader_m53EB8E6FBEA3C809E163C21542C572D7EECA40A4,
	LiveConnection_GetMessage_m9A5301C0AB96CC14D467BB3987B44A107F924611,
	LiveConnection_GetLiveData_m9311FAECC8789A20E6CE86347CBB7B1956153431,
	LiveConnection_PrintMessage_mAB70C3E38FBA8F44224478E8DC3448458170F844,
	LiveConnection_PrintWarning_mA829AE99B4F69826D8F95483C0D4DB81F411C693,
	LiveConnection_PrintError_m998F3C9CF804EAC2BF8388F0C47AF5E6FFB6C610,
	U3CU3Ec__DisplayClass31_0__ctor_mF7136D163E537DD346D7F47FF8FC861DE7053CEC,
	U3CU3Ec__DisplayClass31_0_U3CGetHeaderU3Eb__0_m5A8A7C588566FE8736C31E767D2C67B87B838D4B,
	U3CU3Ec__DisplayClass32_0__ctor_m197EA3DE1D40439EED66DC9560ABDED4EF6D9F6F,
	U3CU3Ec__DisplayClass32_0_U3CGetMessageU3Eb__0_mC9A5B90470FF17466D0C4DF0118B8B0A78813299,
	LiveHelpers_CurrentVersion_m2F8481FE4B4B0681A42822A4D3953286B91B8432,
	LiveHelpers_JoinNameAttr_mAD880B7E3AAF6CE3C4A23BCA66910991F40D6D3A,
	LiveHelpers_SplitNameAttr_mF4CE3E6632B3DD26088C67E38856B07D89323F71,
	LiveHelpers_GetAttrString_m5FD5447F370A06EE88697388118126918809001A,
	LiveHelpers__ctor_m1AE366B731B087D84D6C55C44BA90B806A4689AB,
	LiveHelpers__cctor_m333A33AD8C7985D5F3FDE854BC505885D46AB195,
	TransformDeepChildExtension_FindDeepChild_m597CDC7F2947821AFD555C89C8747ADF6163C1AC,
	TransformDeepChildExtension_ReturnAllChildren_mE27C937BA2276371000830342C72221EB0A37366,
	LiveUnityInterface_ValidateControls_m1A9449C1FF52E0A8BFB46A77118977C51E4BC640,
	LiveUnityInterface_GetControlValues_m610888310FA4490A5D6AA4EADE77028CA6633488,
	LiveUnityInterface_ApplyControlValues_m374587EADF0BB6C4A62DB2C9DFBDF619D390D22C,
	LiveUnityInterface_GetControls_mC3D0EF497C612039E5D09E2210EED02EF14E8A2E,
	LiveUnityInterface_GetControls_m0BEA8A3937AFEF1292417F4BB1972728AB7405FE,
	LiveUnityInterface_GetBlendShapeIndex_mF2B6CBEB836EDDD0281BAC10131B5CBFED5E41F6,
	LiveUnityInterface_StringToDictionary_m2F8D927317FCE375932D330A05BB356FF2E61FA9,
	LiveUnityInterface_DictionaryToString_mCBB7AC09CCBAB2DB4846E141118C688CA5996FCE,
	LiveUnityInterface_GetRelativeGameObjectPath_mC58FB43A569928A68672F99C8E305DD32B928FD2,
	LiveUnityInterface__ctor_m8CB4A0C020607500D3EA0BFF68541CAE9B1969B0,
	AutoCannon_Awake_mAF615E661A1B704F2CD9ECFED744811BDBA251C0,
	AutoCannon_Update_m07A39FB9F356B8ED9FF151D6E4EE4CC837B4AE17,
	AutoCannon_Shoot_m9CBA0FF8E07B180AF0FA347E50A3418FB102E2E9,
	AutoCannon_Launch_mCEFA755A2CF58AA7F3D88ABB1562C136A501DF3B,
	AutoCannon__ctor_m1C39C78D51A3F0A736F93AAFDC4358DDC8C36AD5,
	U3CShootU3Ed__11__ctor_m582297B3D98641892729FEA14AA7B2949620E55A,
	U3CShootU3Ed__11_System_IDisposable_Dispose_mD871AE5420FAD32A2D546881D6CF453683A2135D,
	U3CShootU3Ed__11_MoveNext_m5F8E4FCE3BB5B992DC893440DAFA14334FBC91B6,
	U3CShootU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCC373A26FC8D14442CC85CD348B347D97B1D7398,
	U3CShootU3Ed__11_System_Collections_IEnumerator_Reset_m8F3DE312668C06C75A6FF727B8FE6C4DA095BD81,
	U3CShootU3Ed__11_System_Collections_IEnumerator_get_Current_mF67E4313990ECA2EE9AA34C0613954B506BCCA87,
	BoatController_Update_m5E830C0CA65DD23F0399418B01F06489F1B32C7D,
	BoatController_Respawn_mA5F68BDB54C493584178F59EF3118A687EF8D55F,
	BoatController__ctor_m8D82C71D4F650E7CEC7493F29C7D58820AF60D6F,
	BoatFinishLine_OnTriggerEnter_m4846DB56F482A8FF8CFC9266C5AB2EB78D3CB76C,
	BoatFinishLine__ctor_m1F1EAC4B284FDD3930751AE98892676F25A0356B,
	BoatGameController_Die_m2CDB5A46E14DF6AC20A1C367E029F179F4D91723,
	BoatGameController_Finish_m67A45535E98D76AC1C69A16475E3B25DAF50FBBA,
	BoatGameController_Update_mA75696DF95F96915B4CF99207BDD2E952FDDACD4,
	BoatGameController__ctor_m692B927DCFE12024C6FDF7FDDC9195D55977AACE,
	CannonBall_Awake_m39AAA42F20BD133CB13B7776329AF5C8B52B947E,
	CannonBall_OnTriggerEnter_m02677151980279A01283EFE3A91DC1687298C7CD,
	CannonBall__ctor_mBD9439BE37A985DBE5D9E75A1641D90AFD665D69,
	ClothTornEvent__ctor_mAE14AF606DBDB6DB9F4D9770B7749F46012F4A91,
	Mine_Awake_m8AB007A5D3E7E879E3BAE0418223ADB88EE5FBC5,
	Mine_Update_m92E900A05E6231B34CE617DA2BF534EC8E361E7B,
	Mine_OnCollisionEnter_m4B4BCCB775FB535F3B1ABDF0BA1F8EFCDA3AAD9B,
	Mine__ctor_m9FAC586E7870688D2482B01ECAE7D3EABFF575B1,
	RaycastLasers_Update_m2D7CCDC306D394CC173E3CF32A5F4201F9276A03,
	RaycastLasers__ctor_m7C45CF0FF758A8A6D8DC2A5BF9DB1FD123EA0494,
	Rudder_Awake_m95A18FFB6B7BE5C1DABE60C2949EED493F67E46B,
	Rudder_FixedUpdate_m26E509B1D680B4DCCB2476CAD04F1AFD99FC861B,
	Rudder__ctor_mCE7BE81EAC70BA50DFA8771B58145DAD162CA6A9,
	SackGenerator_GenerateSheetMesh_m4D897F59DA927E940897ED401B572CA5507C46F8,
	SackGenerator_GenerateSack_mE58E47F846441C8D48C977CEFCDD189204E49AE9,
	SackGenerator_Update_m5174D51EDADFE80FBF6F19D1137DFD5A8A0D0651,
	SackGenerator__ctor_mBCD2C75B309D51AA249B740F0B163D586727626D,
	U3CGenerateSackU3Ed__5__ctor_mAF9FC2B43596E379E045C6B942598B6DE8E1625A,
	U3CGenerateSackU3Ed__5_System_IDisposable_Dispose_m6A56564150536F7EBD1D1556F23310B127D9D69C,
	U3CGenerateSackU3Ed__5_MoveNext_mE447A4200E887F4CFF73220FCA2F0148C10C2DB4,
	U3CGenerateSackU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8520A6EF0BA6F050CF193C5625DB903AAEB49454,
	U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_Reset_mB9EECC48047030740283F89CFCFBF392A8C7AEFA,
	U3CGenerateSackU3Ed__5_System_Collections_IEnumerator_get_Current_m44D3B2101C02E3BAE05B5824B24B3777715F0DEF,
	StretchToColors_Start_m84A1D0DECFCBD6AE3542F7D81721ED0FA83A0E92,
	StretchToColors_OnDestroy_mFCE211E74D86B4B90113B5E1092C45F6F683D52B,
	StretchToColors_Cloth_OnEndStep_mD83FDE0E00587F2C318944D84FFFD65B49725E6F,
	StretchToColors__ctor_mE6F73607981E8FAF152C6AB6379BE50046743A6B,
	WaterTrigger__ctor_m39703604DD34C2041537BF8B8E29B0875A24CCAA,
	ActorCOMTransform_Update_m1F823F53C26B50577A9206EE84DF1A0431B521D3,
	ActorCOMTransform__ctor_mC943B9CEBED589C494795B5404E6F9EB27B66355,
	ActorSpawner_Update_m5FAF94492AE283E7A239154A04B35E7529DF26BF,
	ActorSpawner__ctor_mE0C9D4C3A5893A45204B40196A3FA8EEC26481FB,
	AddRandomVelocity_Update_mF22E9293D511149200299475467F71FB153422A5,
	AddRandomVelocity__ctor_m22A503480182A167097A851FC1608ADB9EDC6590,
	Blinker_Awake_m9C51885B879A52D7AE55F6B9C19A1FC548CC55F0,
	Blinker_Blink_mE2AF8E7C64ABDBD2923300009611949F995280AF,
	Blinker_LateUpdate_mCB43A52D3EC48BAE96EB43B64AEA0CE689FAA702,
	Blinker__ctor_m0BB5933E13D30BEAC6F1984A7CA20BB975B723FD,
	ColliderHighlighter_Awake_mD7548963E0E1A25B64C4ACF3ED60E0208AB7DA0F,
	ColliderHighlighter_OnEnable_mD405F376BE984D8C2DC65BA90F123045425889A2,
	ColliderHighlighter_OnDisable_m8167AC977EA9FB1824D0C7C94BD2FCEA5EB8152F,
	ColliderHighlighter_Solver_OnCollision_mD528A4449F2984FDEB04C486B4FF5D11B16D0C34,
	ColliderHighlighter__ctor_m9038E4292512B0C57E52D54BFAF3AD762E231CBC,
	CollisionEventHandler_Awake_mE879892B39EE071010C56B69B31A5E783DD6612B,
	CollisionEventHandler_OnEnable_mBA3BC1C421014CD940B0904AE15029255526E366,
	CollisionEventHandler_OnDisable_mF45281EA8987ADB067FDBA463D39366B33A7055F,
	CollisionEventHandler_Solver_OnCollision_mF61D0D8CED0666E40FC559BB115A6AC3B0AA8520,
	CollisionEventHandler_OnDrawGizmos_m79FA90D3A4AB307B865242CE6D4EAF3FBFBD63B7,
	CollisionEventHandler__ctor_m65FB1C1378DBE078E9C55CD6A501540A22E46D4E,
	DebugParticleFrames_Awake_mDEA8621CC4B7B374DF75A1BE149D6205122BFF2D,
	DebugParticleFrames_OnDrawGizmos_m1FF932B802AE7F050B9BD7B089FB59A242FD906D,
	DebugParticleFrames__ctor_m158351C055E1A30DDC179EC0A2E7F16D9EAD2802,
	ExtrapolationCamera_Start_m18BE363AB98243B04A0E91142AB5CF016505F791,
	ExtrapolationCamera_FixedUpdate_m710C67C238F8F586AD72E4F542039752D09C32EB,
	ExtrapolationCamera_LateUpdate_m5ECE6CAF70FB6F9773924ADE53A97521C42D48A4,
	ExtrapolationCamera_Teleport_m40D5CC8268DE24BF3592A2538AAD9B0E91D01206,
	ExtrapolationCamera__ctor_m9B6CB33C322252BC5DBA779DC64B5D4A8FDCEF99,
	FPSDisplay_get_CurrentFPS_mBF5EE37E66C4BD0BDE050A10B96908654CB14112,
	FPSDisplay_get_FPSMedian_mA32A92AC63660D7A6505380BE7B0C15F2679B981,
	FPSDisplay_get_FPSAverage_mBC9E6262603A5135AF4B3CF6E3A60188D951495B,
	FPSDisplay_Start_m9DE167885FC272684751D270809F5CD7CBE56661,
	FPSDisplay_Update_mE45C5E9F8AA0815DBDBC36FFE099E4E174985930,
	FPSDisplay_ResetMedianAndAverage_m700CC1AAF6E0654A6F110B25B10EF96790FECA0A,
	FPSDisplay__ctor_mE5C50AF5EDE70F699D1BDC3B56BF31972E2215CE,
	FirstPersonLauncher_Update_mFDCF1EF9E8CEF2D5D595B02E7E5F96F23C47C2A7,
	FirstPersonLauncher__ctor_mBD44730194215A6C9068F4C8CBD17A6B36B0053B,
	ObiActorTeleport_Teleport_m8ACCEBEFA199CC4C0783DB1A96B79E527E9796D4,
	ObiActorTeleport__ctor_m94A57B1EF294D587000F1A95B4B1202695A60EA2,
	ObiParticleCounter_Awake_mB252C00950761A480BAFEAAC8861A5CFF9550951,
	ObiParticleCounter_OnEnable_mBA0242368FF5BAB41CA489AFBC8E28947DF6F5EC,
	ObiParticleCounter_OnDisable_mFAFED9D4809DD1A2F751B0085E3C658E86AEF62E,
	ObiParticleCounter_Solver_OnCollision_m508741B60529B3DB6163E0C8B7816F6600692BCF,
	ObiParticleCounter__ctor_m084AC398EBDA0A6A47F8E48EFBA58982612BDE80,
	ObjectDragger_OnMouseDown_mD327370BC525D8D7547255D7FB659E348CD357D3,
	ObjectDragger_OnMouseDrag_mACC1C3F00553438D26EC4D58915E082960DE5F85,
	ObjectDragger__ctor_m0945194968142437BDB43638A1F691E34015FB5E,
	ObjectLimit_Update_m92A3CA606475C1A1D2AD3C46FD42C5A637E917BB,
	ObjectLimit__ctor_m0E7593045B03E02A861D17028B07076FE90E36A5,
	SlowmoToggler_Slowmo_m78F297084A3A842FCB021D175755FDD06F328042,
	SlowmoToggler__ctor_mCD5D9F92D3C3A1FA3C20BD57E5829E1959FE51B0,
	WorldSpaceGravity_Awake_mE1964D804311A22C20AFFA04E73EE8561865CA77,
	WorldSpaceGravity_Update_m5075642D9AE74E08C02C1EF8AF60F4A362E2561D,
	WorldSpaceGravity__ctor_m62438629F8754A79863207A1D7216A45C95F3601,
	CustomShadowResolution_Update_m4A82927108D89BBAE4A9B678697723AD9B2BDE7A,
	CustomShadowResolution__ctor_mEC5E0FA947B95A83CC9906600C052852E1E2EE08,
	FrameByFrameRendering_Default_Start_mDB34175A1329CA70EE9BADC4D93D4615684295F3,
	FrameByFrameRendering_Default_Update_m3651056826EC081A81578F9F33B0BE2E702C7311,
	FrameByFrameRendering_Default__ctor_m5C19E2780BE0259895809C153FA7DE9627D8EC47,
	FrameByFrameRendering_Manual_Start_m0FF7E063255BD97266AACC470DDCFDCA6D3864FF,
	FrameByFrameRendering_Manual_Update_mD33F8DE37196CF376D4CD1A2FAA7018A4BF4F876,
	FrameByFrameRendering_Manual__ctor_mEAEBCA964753257395356113A94E78C82B64EE69,
	SmoothObjectNormalHelper_LateUpdate_m7FCE4EFFAFB658DA2D02DF953013C16869D61283,
	SmoothObjectNormalHelper__ctor_mB626BE00D5D8A2FE44879DA086EB0F8A0BCB344F,
	EyeMovement_URP_Update_m54F920500EEF67518CD5800F199B8036A8A01FD6,
	EyeMovement_URP__ctor_mAE0D716EC73062762192F9798DA917B0FEE11D0D,
	ObiCharacter_Start_mD9672416FAC12184B8BDFCBBD2C3B584D9119967,
	ObiCharacter_Move_mDB588583845391E57799EBB3D9759C09B0CAD4D6,
	ObiCharacter_ScaleCapsuleForCrouching_m26D40D4BAE34DAB3C392B17BA39C8FA96671E0B9,
	ObiCharacter_PreventStandingInLowHeadroom_m4B56915CD2A223C7387E2C98DEA597BE6160DBEA,
	ObiCharacter_UpdateAnimator_m31A22B166E3BAABB61E5FBCAEC563403C47E8CEC,
	ObiCharacter_HandleAirborneMovement_mF9CB87C328B6E70CD75161167C2DBC24066D2F18,
	ObiCharacter_HandleGroundedMovement_mCE5389E5C95BBEDF93AEE8D8C702DD334632CA0A,
	ObiCharacter_ApplyExtraTurnRotation_m0158F9ED1DADBB4DB1778E1769404F4E4519EB3F,
	ObiCharacter_OnAnimatorMove_m1E554779AB1DB14D5C35B9776D904A93D398EE38,
	ObiCharacter_CheckGroundStatus_m9ECAA5E8638F313F5BCC5D7CB682717B0FA3BDA3,
	ObiCharacter__ctor_mB01C01015198C990E5D95E958A237B0F937E64D9,
	SampleCharacterController_Start_m60F6CB41550DAAFE822AC776E9991D2EA49F22AD,
	SampleCharacterController_FixedUpdate_mEF05AB0405E5C7DDD2541C9B11018A6F38D0C518,
	SampleCharacterController__ctor_mDBB94C75A2743501D65D57DFEBEAEF85A0B7C38C,
	ColorFromPhase_Awake_mFDAF56183456E601E4B67ADD52B6857CABA8DFA3,
	ColorFromPhase_LateUpdate_m341E0A8F72D57ABB72F0B731ADD6A3B92FEDB85C,
	ColorFromPhase__ctor_mE6F652DA0DCB109E6452388955C725262A3E75A6,
	ColorFromVelocity_Awake_mE5B9EAF1C7446ACE20FEAB352DF0BB7570E43CE6,
	ColorFromVelocity_OnEnable_mC10C6ED7634FB12AF978C9025E1E8233AF8E6939,
	ColorFromVelocity_LateUpdate_m643273185816893AD0D09C3A0B0DA5BB7491CB9D,
	ColorFromVelocity__ctor_m47F43FDEB8166D362328048C31F2D55685AAB729,
	ColorRandomizer_Start_m847D4C2A64460979346CA8ADDAFE1C30D29E7B11,
	ColorRandomizer__ctor_m717FD604F1CEF39623E0EB75DFCA9C055D94094D,
	LookAroundCamera_Awake_m836F7040257EA46F94A52A03E83D2BBE52A2110A,
	LookAroundCamera_LookAt_mFB2F30F7FCD622190F52F3CEF7C8034B2C333CED,
	LookAroundCamera_UpdateShot_m3B8DFE20DCAA2931E7DE8E50B29AA2FE2E6CF48F,
	LookAroundCamera_LateUpdate_m6EA5FE3CB3DA88F43717405B349639C1980BE1DA,
	LookAroundCamera__ctor_m506721CFC9EC4A3765D79AD4811BA19D1F295227,
	CameraShot__ctor_m5D4853E2044D14AAB4D03B53A4C7409EDB5B2006,
	MoveAndRotate_Start_m663E9DF6947BDDF2A719E6CDC4DC3DF42F6CE653,
	MoveAndRotate_FixedUpdate_m6B299C18D85880FA61BFE34D1D101D836949390C,
	MoveAndRotate__ctor_m298F94FA31EFD02624375FF0A44BF09B631608D4,
	Vector3andSpace__ctor_mC900A3B74BFD7DF5F677A5A010EB658632FEF7F2,
	JSONNode_get_Keys_mB2E6CD262A489290E32BDD22E29D1D972891CC98,
	JSONNode_get_Item_m15B56AF59515C383F438425F707DCD45A92F4865,
	JSONNode_set_Item_mEB2B1436A55A4EA01826840CE454DE6139DBFD96,
	JSONNode_get_Item_m60C1ABECBE0571F458998A9A8410EE8ED8D4BC9E,
	JSONNode_set_Item_m26390E552CD8420AFD0C634ED57597CC06625A26,
	JSONNode_get_Value_mA5FDEA6BB16F7B21AE6F41A85F5004120B552580,
	JSONNode_set_Value_mAAD460AEE30562A5B2729DB9545D2984D6496E76,
	JSONNode_get_Count_m37D2CF69CE110C3655E89366A3EE2262EA99933C,
	JSONNode_get_IsNumber_mAEC1A3CE41B21C02317EB63FD8BD1366327A4D1E,
	JSONNode_get_IsString_mBACE5A4D126E8011EE8D9D18510AAE31D8B51AE0,
	JSONNode_get_IsBoolean_mB9C9F7A3C14C7250032AE78BC885CE87F15C3FFD,
	JSONNode_get_IsNull_m71B4615E695BF588CE5EAC79F52F6EC66B1C1461,
	JSONNode_get_IsArray_m73A69DC1A6B8F910CEE872204A1FF2EA9CA65D31,
	JSONNode_get_IsObject_m9210B0CDD4B70E42E3EA0B8EE6C2D510A640EA60,
	JSONNode_Add_mB007E02475524C684D2032F44344FC0727D0AED1,
	JSONNode_Add_m72F8683C5AB8E6DDA52AAB63F5AB2CF5DE668AA4,
	JSONNode_Remove_m4C6ABDC53258E365E4BEDFFB471996D0595CE653,
	JSONNode_Remove_m40DC66D2BFA64F462C956249568569C640D65FBE,
	JSONNode_Remove_mEC2EA47AADD914B3468F912E6EED1DA02821D118,
	JSONNode_get_Children_m0BDB86A86A43943A02FBEA21A8FEDE6D91906395,
	JSONNode_get_DeepChildren_m843513089FDD8B254DCC3232FD4DA85056923D9F,
	JSONNode_ToString_m8CFDF7832FB437B9D9285EA5913ACFA8BF529C75,
	JSONNode_ToString_m959FD63EB02266172C30D5608E92E9B478EA1698,
	NULL,
	NULL,
	JSONNode_get_AsDouble_mA9A87A9DDF3DB8A927705894B0A70369513743B8,
	JSONNode_set_AsDouble_mA2D6CA445FBB3B93D4E135F91CF1CE9779375098,
	JSONNode_get_AsInt_m35E5CF8D5FCA1E5D1697C6E666FF3CD5FC1B2BC1,
	JSONNode_set_AsInt_m3D8AFBE4D49B29711CCECBDAD4C145BD72C47D5C,
	JSONNode_get_AsFloat_m53D151773142FEECC3886C1ADEB2BEC22A0C3CAC,
	JSONNode_set_AsFloat_m6D669252ACE2A695075685624BDB94A60260FF63,
	JSONNode_get_AsBool_mC1732D312696100E7F429542BB876EC949DA9947,
	JSONNode_set_AsBool_m91CAA7562009099B02BD538F37D94CEE8F8882B7,
	JSONNode_get_AsArray_m7DF6AB373218A86EFF6A9478A824D5BB8C01421A,
	JSONNode_get_AsObject_m8BC40A325C24DE488E73E7DAFEA530B270BBD95B,
	JSONNode_op_Implicit_m94391C275D1BE4AC26A04A098F4B65BBB7D7AF84,
	JSONNode_op_Implicit_mB446B8B500123D3F4F3C5D66F095316FF663385D,
	JSONNode_op_Implicit_m112A87AB176887F93C5660CFD3C169A5BB1C3CB4,
	JSONNode_op_Implicit_m9B6B63FDCCE9EC0F0CF543B13E3863936D283487,
	JSONNode_op_Implicit_mC458C08484535F8FD3B586B5D5337B9AC093C837,
	JSONNode_op_Implicit_m3DDE962F828FB4DB318C6D8C336FFD51AF19C552,
	JSONNode_op_Implicit_m279DCD5A06BDC389C2AF42381AC41E3EC06700D5,
	JSONNode_op_Implicit_mE6EE79025736C59E41C99A0C9101BAF32BE77E18,
	JSONNode_op_Implicit_mBA5DC585F9071890732C4805B3FA7E1134E76477,
	JSONNode_op_Implicit_m2E4691C3EE93FD2CB2570E30EBE1F84E25A53099,
	JSONNode_op_Equality_mFA64BB19805777C603E6E1BD8D6628B52DEB4A1E,
	JSONNode_op_Inequality_m65F2F76C1716D266797A058589DF280A44E2CBA5,
	JSONNode_Equals_mD1DBB741A272720F18B24437CD78B338B65900C0,
	JSONNode_GetHashCode_mDDA191EC3E9FA81A238FCB2B1C8024FCA97677AC,
	JSONNode_Escape_mABAE2F4F85D5F926A6B80D5181FD69D69669EFA6,
	JSONNode_ParseElement_m7C0AE6936FF1C5678FF55A838971E8E0E24C8A69,
	JSONNode_Parse_m1F5205640E23CB1423043FA1C5379D0BF309E4F9,
	JSONNode_Serialize_mD8A4B156C9FA37AB8FA4ABA6B22D8F1A9B27CC3B,
	JSONNode_SaveToStream_m46C0008EB9F1F7350E55DD57CEFAEFF36DDCF523,
	JSONNode_SaveToCompressedStream_mF87F91D2D54DA91BCAA6715B42B5D8F4482DF062,
	JSONNode_SaveToCompressedFile_m72489371227B0206AC10DDDAC077225AB02D0FAB,
	JSONNode_SaveToCompressedBase64_mE6579E9F545077594603F08862654B3FABF11186,
	JSONNode_SaveToFile_m11795B826D93616FA6C477ED66A40CB3D2C1E372,
	JSONNode_SaveToBase64_mF2EE9BA46DB1AF86A0B455DA13A3550E8B641B16,
	JSONNode_Deserialize_m30D4EEBBFA933D823AA50A5DE03E07C570F443A4,
	JSONNode_LoadFromCompressedFile_m576D63C52D5BF2EE31524033FB81BFA831B4EEB0,
	JSONNode_LoadFromCompressedStream_mA161803714EFBF3EC65934585069E8DB530DAC55,
	JSONNode_LoadFromCompressedBase64_m07944ECC5086AEFB5DB67E5BCA4E3FCE98FCB39C,
	JSONNode_LoadFromStream_mCF525FAC941AD38BB9BC0A7BE0128C44BE96A783,
	JSONNode_LoadFromFile_mA14F24B61F89F4C5D8465C908AC104BCA6561A31,
	JSONNode_LoadFromBase64_m0B9BCACA8F672CB6EE0BDD06C570CDDD3B928E2F,
	JSONNode__ctor_m5C8FC3D0D04154FFC73CDEB6D6D051422907D3CE,
	JSONNode__cctor_mA6EEB9601A65ECE220047266B8986E7B975909CD,
	U3Cget_KeysU3Ed__1__ctor_m31B22F928A08E7B411030A15D7D511E9C6826567,
	U3Cget_KeysU3Ed__1_System_IDisposable_Dispose_mCEE33DEA3B47BE82D24E16E43A3405BB12911FF7,
	U3Cget_KeysU3Ed__1_MoveNext_m4B6CE8B2D02CF534EAAB72E262AAE8A07B5680F5,
	U3Cget_KeysU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m75F3E48104D74C7DEC2FAB44B93E2E7F4A274C73,
	U3Cget_KeysU3Ed__1_System_Collections_IEnumerator_Reset_mC84561A668E4BB4FAC92044F2DFAACB32C4934D4,
	U3Cget_KeysU3Ed__1_System_Collections_IEnumerator_get_Current_m0D5E782AC56841198E470369005371EAA86AB3EC,
	U3Cget_KeysU3Ed__1_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_mE403F886111FC9CB8936AC10998F237687EC9547,
	U3Cget_KeysU3Ed__1_System_Collections_IEnumerable_GetEnumerator_m7F27D903D8E8B11413C5D576B6A5DF90B4F48A29,
	U3Cget_ChildrenU3Ed__31__ctor_m8F6FEDFD2CBB567FB3C44794CF5B5FEA0E7D0981,
	U3Cget_ChildrenU3Ed__31_System_IDisposable_Dispose_mA50D783DEF319BC009881D9258382CC9D4A3FF68,
	U3Cget_ChildrenU3Ed__31_MoveNext_m40E4302F968AB61EB4995AFBEC337FE878B2BCD8,
	U3Cget_ChildrenU3Ed__31_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m621BDAC43DA20BF53C7413FFD49DD655000C13B1,
	U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerator_Reset_m47AC690AE79658F787D1CB8C08D3D1C0488A39EC,
	U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerator_get_Current_m631428405E88D0CA4CB1EB5495BEE79598F9146B,
	U3Cget_ChildrenU3Ed__31_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m529466030E53F1DCA736E9EAD8ECD0C8EFF091DE,
	U3Cget_ChildrenU3Ed__31_System_Collections_IEnumerable_GetEnumerator_m158067C4E4DEE7D06269BFF13156BBC6165A409C,
	U3Cget_DeepChildrenU3Ed__33__ctor_m6556D8B2C4E7ED9752AFFA558BFF563240D0C419,
	U3Cget_DeepChildrenU3Ed__33_System_IDisposable_Dispose_m0C16C4312921C7F1FFA135FABE659152DA55D864,
	U3Cget_DeepChildrenU3Ed__33_MoveNext_m35A09F1D6B7CF89200082A8B14A3A0EBCC6DBA5E,
	U3Cget_DeepChildrenU3Ed__33_U3CU3Em__Finally1_m391A8C008DB875F4176B93BE66EFC26A4829A02B,
	U3Cget_DeepChildrenU3Ed__33_U3CU3Em__Finally2_m3E1A5C6226630A2C82545B43701EB82DFB03C51D,
	U3Cget_DeepChildrenU3Ed__33_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF4C5AFBCF250BF1CA96F70247B53704BD20903D7,
	U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerator_Reset_mE9263C830CF629B7F8D65FEA16666E756330C49A,
	U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerator_get_Current_mA6B6DDC581FB0CF09EAD8FD26B3B86463CC22286,
	U3Cget_DeepChildrenU3Ed__33_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF3DE030FDACE36ACD6E736CA90D8B3A02671485E,
	U3Cget_DeepChildrenU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m7ABE068F347314D21AAABEC79E46B34E3FE5F56C,
	JSONArray_get_Tag_m51CD8C530354A4AEA428DC009FD95C27D4AB53CE,
	JSONArray_get_IsArray_m50C77F695B10048CA997D53BE3E3DC88E1689B03,
	JSONArray_get_Item_mFEBC0948C9244F2C604E80F5A4098BFB51255D10,
	JSONArray_set_Item_mB2AFD08A9F2CE998903ABA685D88C9CF00894885,
	JSONArray_get_Item_m1B541EE05E39CD8EB7A5541EF2ADC0031FC81215,
	JSONArray_set_Item_m90E948A121CE1B37388237F92828AFA5C539B2CE,
	JSONArray_get_Count_m77CEECDA838C7D2B99D2A0ADBFD9F216BD50C647,
	JSONArray_Add_m2F2793895033D6E8C9CA77D0440FEFD164C1F31D,
	JSONArray_Remove_mD5817778EF93C826B92D5E8CC86CA211D056879F,
	JSONArray_Remove_m36719C3D5868724800574C299C59A3398347D049,
	JSONArray_get_Children_mB77EB8E1B351B82181B50334A57FE456AB8BBB85,
	JSONArray_GetEnumerator_m946A4A17D8F354B270A313F0967F6539662C5B14,
	JSONArray_Serialize_m2D871FA5430E31F65BE9EE079ACB39A6559E753C,
	JSONArray_WriteToStringBuilder_m4FEE53C6F7BF56C22BC919A982F9A1047362819C,
	JSONArray__ctor_m80F3F8569F953F1B2448EF688ADBE2BE06B85EAA,
	U3Cget_ChildrenU3Ed__18__ctor_m8A2BBBE94FF9E527B67162C99C5BE2E41D21B3C8,
	U3Cget_ChildrenU3Ed__18_System_IDisposable_Dispose_m131403239E7888DF9608A2086961BA6203163F73,
	U3Cget_ChildrenU3Ed__18_MoveNext_m1CD60520FBB35990617570CCD0C6C54A954E2371,
	U3Cget_ChildrenU3Ed__18_U3CU3Em__Finally1_m79651C2EEC5E19C6A774899A3FB1AF290FB7D632,
	U3Cget_ChildrenU3Ed__18_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mE655E5ABB445B1286650C5150968A5DF162108BA,
	U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerator_Reset_mA0096AB464F0880367669DCB22255BCF8270C2AE,
	U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerator_get_Current_m352AF0622FC2693D4ADEC8234288CC87DD1431A6,
	U3Cget_ChildrenU3Ed__18_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m975BED2D279FF4DC2672F45E0CAAB074B5B14D24,
	U3Cget_ChildrenU3Ed__18_System_Collections_IEnumerable_GetEnumerator_m17D327415306FBB8B51C370A0E513D682A67F7C7,
	U3CGetEnumeratorU3Ed__19__ctor_m3519C4DACB103DC807C9DF71E541DEE82EF9808B,
	U3CGetEnumeratorU3Ed__19_System_IDisposable_Dispose_mE824F34CC804B44662C65501124D8A1CAD898858,
	U3CGetEnumeratorU3Ed__19_MoveNext_mA13864872CE8E506B539447FD8FB82E364D11A19,
	U3CGetEnumeratorU3Ed__19_U3CU3Em__Finally1_mE58BA16207C8BF557C667239DF595312E8DEF157,
	U3CGetEnumeratorU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9DDA20D156CFC5FC1184371E2AEE55A7171412E,
	U3CGetEnumeratorU3Ed__19_System_Collections_IEnumerator_Reset_m3C8120B53988728D4E5645CD89BC20BD1B82982A,
	U3CGetEnumeratorU3Ed__19_System_Collections_IEnumerator_get_Current_m7CD471DEA843AEA7E0E892A970BE2143D17E41E3,
	JSONObject_get_Tag_m24A4D8474C6803E5B36B9C7B16CC35F48C00C486,
	JSONObject_get_IsObject_mD17F33216C3AA982747307A9783FC34B4C249ACD,
	JSONObject_get_Keys_m3A9DB65AF2CA08FD679CC57F7F0E17C4DA798D7B,
	JSONObject_get_Item_mC02C1B3017199E1B58E09012760D91B0236A79DF,
	JSONObject_set_Item_mAF05A0803D9648754295AD43922D8750E248AB5B,
	JSONObject_get_Item_m7B5F21E465C8A06F0FD00EE62B59CFD229360976,
	JSONObject_set_Item_mD3F2E832E77FDAEB95AF4C6113F0314E3C286D3E,
	JSONObject_get_Count_mDDB2E49A1D4DC4C26FAB083943E4F47B39C233AF,
	JSONObject_Add_m75EF35AE491E67AEDCAC14A8810985463681353D,
	JSONObject_Remove_m56A85EFAE9C4DDF43C755D4938665178D59186D7,
	JSONObject_Remove_m56C844643C6B6E81D4E3467DA869A39FF93C79FE,
	JSONObject_Remove_m9943021B70272C24F3FE7D87421F3DD6E1EADB15,
	JSONObject_get_Children_m553BB3AF1E134F5FB59F325E8D3E811C204E6AD6,
	JSONObject_GetEnumerator_m5BFA47C417BF72FA5A9DDD18822A3D92FCCE2802,
	JSONObject_Serialize_m705D9632D662D77E8A6EAD74EE69FEA48EE0DF2C,
	JSONObject_WriteToStringBuilder_mCC982C03B19077CC8BC60D61B1893FD5543A7B1E,
	JSONObject__ctor_m0F87A09A5AB3C1141E7E82D8C45D9C6B44F87E5F,
	U3Cget_KeysU3Ed__7__ctor_m8256372ABD3AD5AAB0C9DB23F779DBAFB396E877,
	U3Cget_KeysU3Ed__7_System_IDisposable_Dispose_m709766446222B475AB0550365840C2DF96568E3A,
	U3Cget_KeysU3Ed__7_MoveNext_m1290B50AD99CBEB9E1B3822BC69B2CA4B92FAC3E,
	U3Cget_KeysU3Ed__7_U3CU3Em__Finally1_m1F5A55E18106B1B85F04C8640960A71FB1B663FD,
	U3Cget_KeysU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_m5F91D9FA31882E9EFADE7628A032D1BE9ED01EEC,
	U3Cget_KeysU3Ed__7_System_Collections_IEnumerator_Reset_m8CCFF1DB60A4659FD0DFD8053DC5BB1D79236CD2,
	U3Cget_KeysU3Ed__7_System_Collections_IEnumerator_get_Current_m1E9FF99A66827940C935823D10BD1B228988C81C,
	U3Cget_KeysU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m27F64964C2D5F9B90BDD39A97F4BEE7D873943E0,
	U3Cget_KeysU3Ed__7_System_Collections_IEnumerable_GetEnumerator_mC743CC524C8244B1252852B1B9DAB61B1D889D82,
	U3CU3Ec__DisplayClass19_0__ctor_m1A4CD6E6D85675EE935ECF89DD4029272B6E2D4A,
	U3CU3Ec__DisplayClass19_0_U3CRemoveU3Eb__0_m0031F9D24A86A93EC7282002744BA70B5C0BADD0,
	U3Cget_ChildrenU3Ed__21__ctor_mFF58DDBB7D0C1CE865AA301B26B6FDF260988FF7,
	U3Cget_ChildrenU3Ed__21_System_IDisposable_Dispose_m74E7A8F92CB73C9F3459155A08FEC13013AA07AA,
	U3Cget_ChildrenU3Ed__21_MoveNext_mDE2E2C71804DC023D743BF7B3260B5065A2DDC27,
	U3Cget_ChildrenU3Ed__21_U3CU3Em__Finally1_mABCC4CA7E7DA6BFBF5B8D25750656D146D76D9AB,
	U3Cget_ChildrenU3Ed__21_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mCFF54B790DE5CB1B9D12EA2BD0EA0F3517F85249,
	U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerator_Reset_mE92B65B3DDA55AB7049F67DAF4D6E8D9BC8F45EA,
	U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerator_get_Current_m2669987DCE20D237DE880133D52DE160363689E1,
	U3Cget_ChildrenU3Ed__21_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mF0A115DF1C4FFDF65675931C6E9296F8F173EACD,
	U3Cget_ChildrenU3Ed__21_System_Collections_IEnumerable_GetEnumerator_m4BC7E51A8F4519F5A87014F8BF064C2F09B06490,
	U3CGetEnumeratorU3Ed__22__ctor_mD19E8726BE2F6BF6FA86D25009FB93C52C1B3E82,
	U3CGetEnumeratorU3Ed__22_System_IDisposable_Dispose_m4DE772784AE4D0ACCF401372E2E9E52F021968B0,
	U3CGetEnumeratorU3Ed__22_MoveNext_m89DBEB3B00A4969674554AD576B4B90B76CAB6A2,
	U3CGetEnumeratorU3Ed__22_U3CU3Em__Finally1_m34F486AC0FCAFBE94E8916FF21102B50FEDEA8D9,
	U3CGetEnumeratorU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBEDA3BCA87876262144946F76847AB270591FEB6,
	U3CGetEnumeratorU3Ed__22_System_Collections_IEnumerator_Reset_m0E6C15FFF0ED45C2DB4DB3989D9B80D814046549,
	U3CGetEnumeratorU3Ed__22_System_Collections_IEnumerator_get_Current_m63EA31BA062240254C552EB36D0479C9D13C4529,
	JSONString_get_Tag_m04F409B5247C8DCD38D24808764CF24D869E6185,
	JSONString_get_IsString_mD933D266CC563B1D5E85CFA7C7480CB2E5ACA48E,
	JSONString_get_Value_m29504F709941AD379852FDD0B1A08E8DC4B2E58A,
	JSONString_set_Value_mECAA9F7BFEE67F189A1493F90CDC4475318ED6FF,
	JSONString__ctor_m3DE989CC0DA12FE3E23174CD280D86971771ADAA,
	JSONString_Serialize_mB6AF2698D7B61FB566B258B381056A5291804168,
	JSONString_WriteToStringBuilder_m5297F304170133F94CB9E813928B07D57FFEB5C8,
	JSONString_Equals_mCE7D7CAD2604883600D10531380FECF4AFB92F09,
	JSONString_GetHashCode_mC05889B33A8AF366C4C8456D54F94F252C2300B1,
	JSONNumber_get_Tag_m5810C73995BD124FB40ADDFEB7172AE1295D3C3F,
	JSONNumber_get_IsNumber_mD941320E66AA55D3391E146E3E624AFFCCCC5912,
	JSONNumber_get_Value_m285D5CFDCF3259112D7128D9EC962DF7C1C638CB,
	JSONNumber_set_Value_m7B6A055194CFD54CE0F8360809DF0516696D04B0,
	JSONNumber_get_AsDouble_m837CA051FAF2D45E0B415B1CE91C6DE1A9F5C399,
	JSONNumber_set_AsDouble_mE9AE823BDDDD4CE0E3BD37ED70B0330A3D303E68,
	JSONNumber__ctor_mA5B174BD1A163979DCDD304E4A679A1D9E8801B8,
	JSONNumber__ctor_mCA0A65AB3C617FC6F8066EB9C38E2B413971E28F,
	JSONNumber_Serialize_m56B7577953CB962BEAA82D7677523CAAEE91A074,
	JSONNumber_WriteToStringBuilder_mB754D2A3988C1E35BDEF7A2FB9E842ECBAFE1F4B,
	JSONNumber_IsNumeric_m88429E8156B5500398D25C0C5A0ED127BBABC887,
	JSONNumber_Equals_mF299CAFC541A41B0B4D9B12A6967B35B9F554931,
	JSONNumber_GetHashCode_m33B32859776F731322E20E05B527B92255F130F5,
	JSONBool_get_Tag_mCBABB544B93C712D3F57CC8FBEC49260FEA9284A,
	JSONBool_get_IsBoolean_m98A1AEE6EC63912E54A15116CDB7C8418209FDBA,
	JSONBool_get_Value_mDDA4D24E9F16DED3152898F98F16B06327BEF9F6,
	JSONBool_set_Value_m068B69773876CB81DF3C699ABC743FB1CE861F1A,
	JSONBool_get_AsBool_mCA60BF3904B572629003D2887EF27F89C1E764E9,
	JSONBool_set_AsBool_m4E1A515A31B59A3F2B683F4ECCEA388684320B91,
	JSONBool__ctor_mB729B68264E989BA9AEE3B86AF17E3906FF7C9AD,
	JSONBool__ctor_mFD5C92FB72B70A027D45ED45E42E6E7516858CEE,
	JSONBool_Serialize_m649615169DA6E401935D5AFFAB7EEF65A1E1A6C5,
	JSONBool_WriteToStringBuilder_mABA45EF0C30EE803D2FD1AC77FA7B2EA967BCFD9,
	JSONBool_Equals_m853E2A5B2F12290669528B9E27BB2CBB2F8FDE09,
	JSONBool_GetHashCode_m13AE52408BA8EFA79CD151D50EC9DCF4F7CAB73A,
	JSONNull_get_Tag_m498F7F5444421EDA491F023F0B76AC1D1D735936,
	JSONNull_get_IsNull_m4ED1FF25799E79A71002A79A4AC27B2177635FAA,
	JSONNull_get_Value_m07942D8E26A0D88514646E5603F55C4E3D16DD60,
	JSONNull_set_Value_mA2582B26943415A7DE20E1DAD3C96D7A0E451922,
	JSONNull_get_AsBool_m60DE1D508FB98AAF328AB1965DAC1BB43881CD98,
	JSONNull_set_AsBool_m5C230E9709559FD14099B0848A4D5C4C1A815000,
	JSONNull_Equals_m522306B502C21C1E2EBE989556F3F16331848600,
	JSONNull_GetHashCode_m950BC0E73B87DC3336908BEF882459BC20E5A55B,
	JSONNull_Serialize_m750D4DFC23CE85F6E833E89B6CE53D6476C9BB48,
	JSONNull_WriteToStringBuilder_m84A9A7C230BBEE86E8A548B8446FF19B5E469B6E,
	JSONNull__ctor_m49798992779B2B1E8D1BAFDF4498C2F8AEA76A4F,
	JSONLazyCreator_get_Tag_mC1AC02C25A03BF67F0D89FD9900CBC6EEB090A5D,
	JSONLazyCreator__ctor_m8FC6D598D0237C9350588BD29072C041A61F0798,
	JSONLazyCreator__ctor_mC6E81F011E8C8956780A3B334A91DA44147BF188,
	JSONLazyCreator_Set_m746D69028C9A2C5E0B1FBBA1F8F008C2052A1779,
	JSONLazyCreator_get_Item_m5EA50EE949F36710942A9FF8EF5777D233B3C047,
	JSONLazyCreator_set_Item_m3B7837F849B2388008C45743CD3140CE828F2606,
	JSONLazyCreator_get_Item_m95C7B93F385B7EF2D47052C39794763732D6FF0C,
	JSONLazyCreator_set_Item_m00DFAB6CD611C42D6B22D575ECDCFE265710FBCC,
	JSONLazyCreator_Add_m4361ED9A8E16E28BAB9DCFA1FC1AFB27124FB670,
	JSONLazyCreator_Add_m6A8B2BEC3941E48339A2AD67047FC6D0BADFA17F,
	JSONLazyCreator_op_Equality_m68D0526912FAFBC3D3332DEBACCD45DEA750C2C7,
	JSONLazyCreator_op_Inequality_mA05185BE9E99126A18F9EF368C747CE578FBAD95,
	JSONLazyCreator_Equals_mB6087EC316E745F8D61D2068D2A98CA634113D4E,
	JSONLazyCreator_GetHashCode_m51335A4464EA4383300746254D92B163803C8C43,
	JSONLazyCreator_get_AsInt_mD1A90C028EA0FEC6DB194136F7AD88FDE408F67A,
	JSONLazyCreator_set_AsInt_m8FE656CA5259D0B4320F3DBF60BD19A686A26857,
	JSONLazyCreator_get_AsFloat_mC7748D1573024F4EC5A1E1EA1357D812FEDDF870,
	JSONLazyCreator_set_AsFloat_m5846F6BF2B58995E98DB57813A6C78CC1A7E1934,
	JSONLazyCreator_get_AsDouble_m64481355784007008FE0CDEB3CC984B2B02FD465,
	JSONLazyCreator_set_AsDouble_m278B0693BF4190C6FACC267DB451E8E190577350,
	JSONLazyCreator_get_AsBool_m6675B2D4E0C4D06469EF0B66D3E0CF5B8C5EB7BE,
	JSONLazyCreator_set_AsBool_m3DA5CF7B6A12BE73753CDF5FD3F72D06932942B2,
	JSONLazyCreator_get_AsArray_m7ED16496F0BC83E265C51066E586108D22850C5D,
	JSONLazyCreator_get_AsObject_m49378AF7ED532AAB2664C02C21E537630528B9C2,
	JSONLazyCreator_WriteToStringBuilder_m691033C947C63F4764EF87A8DC5A17CE3EF2D59F,
	JSON_Parse_mAB3D7D96F2A4170914DA8D0A54787AAC1762BCE9,
	SimpleRotater_FixedUpdate_mBCA4D633FA62C6F78827C34702753DCBB2926936,
	SimpleRotater__ctor_m3557D6B06C2004B51FDBF6111124102E10E1AE5E,
	SimpleCameraController_OnEnable_mE3D6E47455F101F2DEEBC2A58D09A97CF38E80B8,
	SimpleCameraController_GetInputTranslationDirection_m73C99DB69CEB467834BBA00A62415D1CEEF0CB47,
	SimpleCameraController_Update_mBCD24408A4A2C4053F2F98DB808BD6DE88CA998F,
	SimpleCameraController_GetBoostFactor_m6C4E35CBB2577F112CF6F1FDF139E9187BAE8264,
	SimpleCameraController_GetInputLookRotation_mF8174D1397CCC68B1D36541A2AC09F183DAD70DF,
	SimpleCameraController_IsBoostPressed_m11707E0D8E2E4FCC72DBDE58D2C9D213AF34C39B,
	SimpleCameraController_IsEscapePressed_m481C4E60AB6E6D91CF8049996FA0F143EDE04697,
	SimpleCameraController_IsCameraRotationAllowed_mA079105CE1BFC131A87F6FA443C583CE16338F86,
	SimpleCameraController_IsRightMouseButtonDown_m76909E007FF408C0B24965901DEA129CC4D328CB,
	SimpleCameraController_IsRightMouseButtonUp_m5F0915186A6101ED2059B9AFE8ED96311608145A,
	SimpleCameraController__ctor_m8DE12FC1A6C31D2D60ED78F0B574CE3F864F546E,
	CameraState_SetFromTransform_m6467352ED87301E5F4A76456060A765CAB96AF3E,
	CameraState_Translate_m76BCC104A48EA7F125D5A50D874A2DEEA7967247,
	CameraState_LerpTowards_m883AAF2D3C7F5045B64CAF655FB84EF0FC98F282,
	CameraState_UpdateTransform_mE3349362276789C1617C01276F7DE533BBA22623,
	CameraState__ctor_m4A83DF36C7D280050EA1B101E61B7E345C31A322,
	MoondreamGlobal_get_ContentDefaultName_m2940A865FC9DA770A6E189520E112898C0B54F9F,
	MoondreamGlobal_get_ContentDefaultPath_m28B2BA85F67E924DF9EE7EE17F7D6D0D75C69020,
	MoondreamGlobal__ctor_m5753C1A2502D3A8A500ADA94C5ADE097429C88BB,
	Anchor__ctor_mEDBD78046974D904938525CBCA94A79E40CAC908,
	AnchorFinderManager_Start_m8F731531D23301AFAB5BEBF942D2B341F6F41D48,
	AnchorFinderManager_IsSlamActive_mAD7DF966CDF3EE20BE24A6BC910E8B8A7E403188,
	AnchorFinderManager_GetSlamState_m45AFEFED9E5D3C4F0F2D3F68B5A7A4CCB78A3644,
	AnchorFinderManager_Update_m3B309B456200B6CAD9EB3E6D61F0A8D2EE73A778,
	AnchorFinderManager_SetAnchorsLocalPosition_mB53B16D7604AAB95D590DA6A57D71C32139018B8,
	AnchorFinderManager_SetAnchorKeys_m362C176A35A39B9FFA8C34D8F0563A66E0737138,
	AnchorFinderManager_SetConfig_m17A3D629412AC7431560BC1628B7A6835BD7EE70,
	AnchorFinderManager_SetMainAnchorLoacte_m210EA3F9662FCCC5766BC279A9A8D009F64AE9AA,
	AnchorFinderManager_PosDest_m3F194A7FB9A27A1349DD7681649C22A7DFE1C25F,
	AnchorFinderManager_RotDest_m8B7C74E8455665394ED8284D224CD1CB770746C4,
	AnchorFinderManager_Average_m5452E04BA4877DE53718FBA920F72E6A4485CAA9,
	AnchorFinderManager_Average_mB7789F4122E0BF21C3CCE5D3DAE2926DEBB67BD8,
	AnchorFinderManager_IndexCheck_mB191D430575C2CAEB11F22BCFB023ED2178464CB,
	AnchorFinderManager_CloudManager_AnchorLocated_m458F48B2CD499FF0322EE872FF6A2E33F92E31A7,
	AnchorFinderManager_AnchorDicAdd_m8CB5852A76527B4087BB3667DDCC844DD8FC78F5,
	AnchorFinderManager_IEResetCountDown_mF16FD936B5F802C679FAEA5D8465F367B853786D,
	AnchorFinderManager_OnWorldAnchorTrackingChanged_m2598E5C63C9CC15835B05B6E6565C6B239F66696,
	AnchorFinderManager_OnWorldAnchorPositionalLocatorStateChanged_mF24D09840D99232F348AD086536D06538949898B,
	AnchorFinderManager_StartSession_m11CACBD5FE67BE9D656F8CAFB2D8DFFCCB17B321,
	AnchorFinderManager_StopSession_m851520B6A1C4402692609AD6A438A65917C2AA59,
	AnchorFinderManager_SetText_mE05B15387D3B26ABCC7A4CD8713CABA0E005117F,
	AnchorFinderManager_AddKeyStorage_m473D850E93646CA53356B391216F048A2473C18F,
	AnchorFinderManager_ClearAnchors_mB5C71D00B8706289CA97C768CF78AE5808867A56,
	AnchorFinderManager_ResetAnchorKeyAndRebootSession_m77205760B1DDBB7EF4DE578C2123F502526AEE03,
	AnchorFinderManager_OnAnchorError_mECD32768CB3D6DBFF41863A2C3BE58E29700DEB9,
	AnchorFinderManager__ctor_m993C8D123894CD50ADCDD80AE24885C1399C4CEE,
	AnchorLocatInfo__ctor_m2A7B6A75D5CA881F344CCEA9634994F54E7298A7,
	TextEvent__ctor_mBF1408D3E57E1D67441750BB4E06923224E8E2A0,
	U3CStartU3Ed__22__ctor_m3AE7F0E26310E1ABE65F85760E823DA506DE626C,
	U3CStartU3Ed__22_System_IDisposable_Dispose_m2F58623F23AA9C86966B1F64FE3502331BABF5CE,
	U3CStartU3Ed__22_MoveNext_m56833A3F89A0F071CF678D9CCAD69F1D66AFCDF0,
	U3CStartU3Ed__22_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m77DE77A7BAF2F35FFEE0EA6C5DB36D4A4914CC21,
	U3CStartU3Ed__22_System_Collections_IEnumerator_Reset_m27E280184A27CC5326AD33B96BB846EA4507E7FC,
	U3CStartU3Ed__22_System_Collections_IEnumerator_get_Current_m8D61C11DF8CDB41C7343B5DF8B0FF3BF10EAE2D5,
	U3CU3Ec__DisplayClass32_0__ctor_m16200422F20C49FAE9DB10CE789053116E20CA7F,
	U3CU3Ec__DisplayClass32_0_U3CAverageU3Eb__0_mE897262A8555E3185AD5B3D0AAED1E05DD6E324E,
	U3CU3Ec__DisplayClass33_0__ctor_mF8F8F8E5EE89481645AAEDD182B6AFBC2B93B68F,
	U3CU3Ec__DisplayClass33_0_U3CAverageU3Eb__0_m0BA3723C10BBF1E356687DCCA3389FBECB18D576,
	U3CU3Ec__DisplayClass35_0__ctor_m3A1C2512C541CB79243EDA8E04FE5D04B2599DCD,
	U3CU3Ec__DisplayClass35_0_U3CCloudManager_AnchorLocatedU3Eb__0_mCCA73668A15007B5FB0D7B41800964E4F65151B7,
	U3CIEResetCountDownU3Ed__39__ctor_m4E8C2F91FB0E57D54693303E0CDC77AB66001EA4,
	U3CIEResetCountDownU3Ed__39_System_IDisposable_Dispose_mBE802B6017D0F5B99C2F7040DDDFFC114222A6AD,
	U3CIEResetCountDownU3Ed__39_MoveNext_mD0BE91B96EB6B2BF399D549659152912C74310CE,
	U3CIEResetCountDownU3Ed__39_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE405334C887C6B6CA00A42F06C6D629D12EFED96,
	U3CIEResetCountDownU3Ed__39_System_Collections_IEnumerator_Reset_m4806F0F21202BC9A7BCCF5381177B201C33CE6D4,
	U3CIEResetCountDownU3Ed__39_System_Collections_IEnumerator_get_Current_m4AFA30E73D2DDFDF3BC367F030A49D62BE2E335E,
	U3CStartSessionU3Ed__42_MoveNext_m329796E6A3238FB5B238DEA7FAF14414FE812257,
	U3CStartSessionU3Ed__42_SetStateMachine_mDC00E8F92E6128821F8B1C0B6FBEE6C8B4B88CAC,
	U3CStopSessionU3Ed__43_MoveNext_mCD54D758D216F67B1069575F29A48D3F96C0AA43,
	U3CStopSessionU3Ed__43_SetStateMachine_m88F137D413FEB1B7F9ED0B53F41DFCAD92232FB3,
	AnchorKeysStorage__ctor_m8E6AC3AA3993257E48B4855D1238CDB0FA52C510,
	HideConsole_Start_m33BC63F5E9929CB5F5781BCD316CFC1D515BE6BF,
	HideConsole__ctor_mF53E92BC63661611B114E192F0EF7A66D331BC51,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	LocationTransfer_get_IsLocat_mC0F66F4188480310D3FDA8679EAA1DFD9B7DFE44,
	LocationTransfer_Start_mE89044231A13DB33B812F641A866682EC3991A83,
	LocationTransfer_IESend_mC6E0A3FD1DB20C3D941C6E3D5ADCC072536AB8A2,
	LocationTransfer__ctor_m951F2850C584150B05521445DFE2A0B039B01AFB,
	LocationTransfer_U3CStartU3Eb__6_0_mA1BBC945E3A303B41B0E006BB6247F032F757E85,
	LocationTransfer_U3CStartU3Eb__6_1_mE8B4C1348EB7CE1571081FBA8B3E0CB93C1C5EDB,
	LocationTransfer_U3CStartU3Eb__6_2_m7B24A9A804574139544E545F63E471C74E6CC27B,
	U3CIESendU3Ed__7__ctor_m2B1D9B9B0F210343256EF72153D0A3EC44D0975C,
	U3CIESendU3Ed__7_System_IDisposable_Dispose_m1233795C699E6E5391CC5FF9D9165C52646FC7E0,
	U3CIESendU3Ed__7_MoveNext_mCC7C43C525A4C0CA35580F5469170B44F2507534,
	U3CIESendU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B259E96E9F2B54BD386C873E9CB1456E8EE1279,
	U3CIESendU3Ed__7_System_Collections_IEnumerator_Reset_m6DF6C0A186AA997896FC0AEF424A1C7A1A0F2028,
	U3CIESendU3Ed__7_System_Collections_IEnumerator_get_Current_m093B757F42BD4F52C4F3C0E7F97A13D63E616E7B,
	ShowPoseText_Start_m8114BE306F69870C4F7BFE26631374E3B128112B,
	ShowPoseText_Update_m2DC1AC93D81F8663F1EE4E627A26152EF0DFB43B,
	ShowPoseText__ctor_m5FACCE7BED4174973FB0E70CAC76F2066FC2595F,
	PointEventManagerNetTcpReceiver_Awake_m0C71BA7443FAA801016B078CA8D5F39E8A0B1213,
	PointEventManagerNetTcpReceiver_SendManagerInfo_m1B76C2B7CF63495BFEFC199DED915C22AD62D0A4,
	PointEventManagerNetTcpReceiver_EventInvoke_mA0DC5281A9EBE48E29D5E0598D61F632C4221C32,
	PointEventManagerNetTcpReceiver_OnPointEvent_mD5922E4017D46BA88EA0C8A85C779287BFB5C3A9,
	PointEventManagerNetTcpReceiver_CheckRemove_mFDB40509B2FD477E19383FFCB19B7BD4FCAE4324,
	PointEventManagerNetTcpReceiver__ctor_m064CE06198B483C78DDD9E7C29BE3D13CD9DB776,
	PointEventManagerNetTcpReceiver_U3CAwakeU3Eb__15_0_m39AEA1674AF3107A0C40E296E6FE3006EFFF3862,
	PointEventManagerNetTcpReceiver_U3CCheckRemoveU3Eb__19_0_m4772E4408849296B81488FC1F781586C613CF956,
	MrPointOption__ctor_mA8E5AB01C01482AB20551D06F6571DB6A72BDC48,
	RadarPointEventToEventType_Invoke_mB645B98854574DF724D06CF4E145A320A9A00A91,
	RadarPointEventToEventType__ctor_mB27BDE5D7016852C37368B5A8C4A87C9092B76C0,
	RadarPointEventToProjection_Invoke_m2672339344D9AE6E3531C7F2F73385727CC26179,
	RadarPointEventToProjection__ctor_mF097C78983C314E0069B1AF07211F4CCC402691E,
	RadarPointEventAllApart_Invoke_mF73D5474D59178DEC433F6AB859C8294F7775790,
	RadarPointEventAllApart__ctor_mADB89D5C7D977A9BDF380F0C2B98476E5BB05DE2,
	U3CCheckRemoveU3Ed__19__ctor_m650D80292D981B04C59F59A7651DAE4E00C67F20,
	U3CCheckRemoveU3Ed__19_System_IDisposable_Dispose_m6B2120F06500CC5E9F76E530513678A0C2E78C45,
	U3CCheckRemoveU3Ed__19_MoveNext_m83B904F1A0C82C3A7B3808D048A3AE1C12C6DEA4,
	U3CCheckRemoveU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2C7B72BF923176448792D2F0D8FAA7E48783B648,
	U3CCheckRemoveU3Ed__19_System_Collections_IEnumerator_Reset_m17A58011263019A2F9AE87E01A0570B0C469861F,
	U3CCheckRemoveU3Ed__19_System_Collections_IEnumerator_get_Current_mB6F8A4AC9DAFA3078DE4CDFFD837243B7BF91DAD,
	PointEventReceiverSample_OnReceivePoint_mA790C6887EC7E585777EC91DE14A059183875CB0,
	PointEventReceiverSample__ctor_m982181823EBA4062106034B5CBDB57B068C689A6,
	NetTcpCommandLoopSender_get_WaitTime_mEDF32F282B945D7B0BC4FAE8D26FAC2C6FC35732,
	NetTcpCommandLoopSender_get_CommandKey_mB66621B49376BC51D63AA7875DF0ED6AE0D54C1F,
	NetTcpCommandLoopSender_Start_m97FD49F84A7BAAAC69411774EFDC70FE5D9B26B9,
	NetTcpCommandLoopSender_IEAutoSend_m2033FDE385F0034049C1A3A4D73A71768F2431A9,
	NetTcpCommandLoopSender_GetCmd_m9D7950C5E77539ED4EF2E31DE16EC47D0456D977,
	NetTcpCommandLoopSender_Send_m41CF44FDBC157A902C27F603439C98A7AB927BD3,
	NetTcpCommandLoopSender__ctor_m80A421AB84842DBEE5C4DB8EF80CFE09B08953A6,
	U3CIEAutoSendU3Ed__6__ctor_m0069DB3ACF16DCB499EB9888D06844040DF7219B,
	U3CIEAutoSendU3Ed__6_System_IDisposable_Dispose_mFA94AAB72D4814AD87EFC49B5E8FFC3D4DCAB3E8,
	U3CIEAutoSendU3Ed__6_MoveNext_m348C0C8E5710FD2F9E50A9002D9721986CC345B2,
	U3CIEAutoSendU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D579B832156C15D9E02B2B42D8BA7A347689DA7,
	U3CIEAutoSendU3Ed__6_System_Collections_IEnumerator_Reset_mDD93896E985965B1E9598A0CCF95FCE69ED47B86,
	U3CIEAutoSendU3Ed__6_System_Collections_IEnumerator_get_Current_mE91AB47AC2218A01D81999D185B170BCEB09ACCF,
	NetTcpCommandReceiver_get_CommandKey_m10078225A29BC944370B4E509F94E8E4EF37386C,
	NetTcpCommandReceiver_OnStart_mB50F5BC285166596E7C6BE622E77D450A37DCA05,
	NetTcpCommandReceiver_OnReceive_m26A04A75D9F2A4B46883003379F395AD67DC5652,
	NetTcpCommandReceiver_Start_m5504921AE35DD66EFFB306CE89E1046C1A019822,
	NetTcpCommandReceiver_OnDestroy_mA92EC2CC4841DFC97C55CDE0D05A113AD354C11E,
	NetTcpCommandReceiver__ctor_m87A6D8ACD93482C755B54F5D9F326D78AFB3615A,
	NetTcpCommandStartSender_get_CommandKey_m7D8A78CCE981C2968435B97713784ECFEE40204A,
	NetTcpCommandStartSender_Send_m836435175A3F58CD5FB8EE883880C3B3D9B1A9F1,
	NetTcpCommandStartSender_Start_m0BF337B212483553627CF52F5D075CEA616BBCFE,
	NetTcpCommandStartSender_OnDestroy_m5A8B5087A03312483E46D9B745C4F222A92419E4,
	NetTcpCommandStartSender__ctor_m0CEB80360FECF460FAA211633A76ECBB0D9B3251,
	NetTcpCommandStartSender_U3CStartU3Eb__4_0_m232C7B1A32E284D57EB77F4493BCCBB32037B4FC,
	NetTcpCommandStartSender_U3COnDestroyU3Eb__5_0_m9F2E2EFA85C61BBF0045218728ED2340E2C301AE,
	Command_get_EndChar_m9A09580D18E7CE7317E80760095D25B276746E6A,
	Command_ToMsg_mC96618D476B4F93DF8837490AC5AFF7242657038,
	NULL,
	Command__ctor_m77EBF5C65FCA259C4346C6388C7934EC7A9945BF,
	CommandMachineType__ctor_m63B254FEAB69055EE65B813227FE2C229D8A5A80,
	CommandTest__ctor_m02FFA4146EB57D497D98AB810D54430E8EE5FD4E,
	NetTcpCommandContentName__ctor_m0284AB94416E8E45505EB9B88864AAD873C3F3DD,
	NetTcpCommandContentNameAsker_get_CommandKey_m9A7A5E14BBB54AD81AAF5FB618C82CD61BE5CF43,
	NetTcpCommandContentNameAsker__ctor_mA92279D0054239CCEF6E218CEE9AB6C2773B7178,
	NetTcpCommandContentNameMobiReceiver_get_ContentName_m9F59AD5E9FA554ACA0002460843F8C5E1A6468CB,
	NetTcpCommandContentNameMobiReceiver_get_CommandKey_m04B3974BAABFBD626FFD4FA8925732ED9388011B,
	NetTcpCommandContentNameMobiReceiver_OnReceive_m6AA989C775F0D01246C0AA4FA755FF4AD57CF602,
	NetTcpCommandContentNameMobiReceiver_LaunchMoondream_mB239F5C9F09189204B07B83168AA84C204D70E8F,
	NetTcpCommandContentNameMobiReceiver_Launch_m6CD6D1F025AD365FBC92E0292E442EA83F60A85E,
	NetTcpCommandContentNameMobiReceiver_Quit_m107525E8AE96BC382D7597247CFE835E52616277,
	NetTcpCommandContentNameMobiReceiver__ctor_m33FF0FE8094C10FC8B14F52074F3051FE72FF8AF,
	U3CU3Ec__DisplayClass7_0__ctor_m977754DDE02CA9CD4A1570726CFBBA5255778CA4,
	U3CU3Ec__DisplayClass7_0_U3CLaunchU3Eb__0_m70A036C4601D79CDE57FCDD3E0E0D0BC7F88D103,
	U3CU3CLaunchU3Eb__0U3Ed_MoveNext_m1F3D2B6EE42AA4393605AB5ECD32A58CAB1517DC,
	U3CU3CLaunchU3Eb__0U3Ed_SetStateMachine_m257EBACFAB8335CC954A8F191BA4BCBDCF3FB134,
	U3CQuitU3Ed__8__ctor_m83871648FC6BB0B5EB4AEA754CA6FD28795DDE1F,
	U3CQuitU3Ed__8_System_IDisposable_Dispose_m1A900FB77CB57314AA40A86BC3618A395139DC2B,
	U3CQuitU3Ed__8_MoveNext_m8EB528A1B0C1086C9674ABDA55479ED46820B297,
	U3CQuitU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB21B4981BE91F804C92C5280A7ABE6BAED63C482,
	U3CQuitU3Ed__8_System_Collections_IEnumerator_Reset_m67AD25F6414A987F61D58A35F22A0141F15A16F8,
	U3CQuitU3Ed__8_System_Collections_IEnumerator_get_Current_m4E509938DCED0A2CD04F4C470D8B8BF64C95D237,
	NetTcpCommandContentStateMinorSender_get_m_CommandKey_m97E4E84EF907A0A30D8A6E1F23C9862FA25E419D,
	NetTcpCommandContentStateMinorSender_Send_m745A0A0ECDE78A199692E53125F9F5D9C3B03506,
	NetTcpCommandContentStateMinorSender__ctor_m3152C06FB109329C296A976B7E70921B0C4FD596,
	NetTcpCommandContentStatePriorAsker_get_CommandKey_mAB5AF8D5C5BD5DA2347F255D58CE65C11D0514B9,
	NetTcpCommandContentStatePriorAsker__ctor_m115BD48B4EAA6139F31E06FFD1C4B5B8BF269AB6,
	NetTcpCommandContentStatePriorReceiver_get_GameState_mD4CCA951132A8233FB6D9E691C6138D9DB807E99,
	NetTcpCommandContentStatePriorReceiver_set_GameState_m56AD84B94D6530BC30BD30DE4817616A2A4BE74C,
	NetTcpCommandContentStatePriorReceiver_get_CommandKey_m8619B2444FC612906E679015189C0BD87278496E,
	NetTcpCommandContentStatePriorReceiver_OnReceive_m53AEEEDF7BD2FB89093CA4A2347223A08BC8D7E7,
	NetTcpCommandContentStatePriorReceiver__ctor_m5626A6FE1F04CC61F77D26F9A5ABA37A64121448,
	ContentStateEvent__ctor_m63FF1F75C9A94FF7D5F1C46899FE7DEEA51C7709,
	NetTcpCommandMobi__ctor_m245DF3C983967472263D5EE6F42C38858C17BBDD,
	Locat__ctor_m89645C09C50AD912C1DEA2022475A6040BF4F228,
	LocatInt__ctor_m2644F877404B89E29DEA143449CDC3874A3F943F,
	LocatInt__ctor_mFAB019D56DE214E4BC415FD81F1DF0CAE780F553,
	LocatInt_get_FloatMuti_mDF01F5884727F4C627C55B838E9BB270D522B181,
	LocatInt_Set_mD004B3503BA3A85DC70E15FE1ECD43E3B079DE32,
	LocatInt_Get_m2C2F6A7C7BD04ED97E0569C998FE69427A5064CB,
	LocatData__ctor_mFDDFC6C741439B91D83C913E823BA02D4CAAF26C,
	LocatInfo__ctor_m9434A81E8531F43A1B84C9B6299822D78D469CD3,
	CommandLocatInfo__ctor_mA9C2AFD74B9D17E9F935F508A012662C5575E43B,
	CommandLocatData__ctor_m25BE57F4A73602D89C0983C3CE3C96992D09F1D3,
	NetTcpCommandMobiLocatDataSender_get_WaitTime_mF9206BFB02179FDF329817DDEC532FF728EC0732,
	NetTcpCommandMobiLocatDataSender_get_CommandKey_m670A48F6422CC99ADF67C7FCCA766555F1E1D72F,
	NetTcpCommandMobiLocatDataSender_GetCmd_mC14E9321E48EB81AC5F0E66744A33A1167C6FFD3,
	NetTcpCommandMobiLocatDataSender__ctor_m225F6C1574A2E46B10DF0F150E7005A9CF9453FF,
	NetTcpCommandRadar__ctor_m1E8318A3ED7136AF12DD81026B2AD3B5F2CD64A6,
	MrPointEvent__ctor_m17A9534F42E7846AFD0863D02BE95817F0C98284,
	MrPoint__ctor_m8E801A5F0F9F71C4E0F9EDEA19D1DFC9588A975B,
	MrPointInt_Get_m5F94AFEC39983D71ECCD73ECB1F80AC01DB79BE7,
	MrPointInt_Set_m6EF7162785708C3D6185E2635DCCD08ABF453E34,
	MrPointInt_get_FloatMuti_m1B7D8CEABF6C332E87758A8736D6CAE876A256DC,
	MrPointInt__ctor_m727654EE8A68F7E3CA87196C3A1E8F16C1F5C337,
	ManagerInfo__ctor_mA6C27EC67B8E2EAC1D4EF1EDB2E79C232B9D824A,
	ManagerInfoInt_Get_m39809FD72816424D91476E6B916237B6325829DC,
	ManagerInfoInt_Set_m9155C5B2EB22BEB3C299DE59223F08ACEF36F1B1,
	ManagerInfoInt_get_FloatMuti_m3E12D33EF7D06A4E9B6E08E56FB6DF27695AA66B,
	ManagerInfoInt__ctor_m5B8160F856BE990E00D3C2A6ED85EE420E731A96,
	CommandMrPointInt__ctor_mCF364D38914E6DB4E91712B7B38B0924EED79765,
	CommandManagerInfoInt__ctor_m5BB15E897E8ABD6B22A2E26BF1BCD9398C8C0E7B,
	NetTcpCommandRadarEventReceiver_get_CommandKey_m7DFE35B95046F570B7CDB4557B3F5A91D1C3D7F7,
	NetTcpCommandRadarEventReceiver_OnReceive_m2CBAD579181195D5725310AF5BD4668A8A1CE19D,
	NetTcpCommandRadarEventReceiver__ctor_mAA34C55FD68AAEC370A38895CE2EBDB2F78BEB10,
	PointEvent__ctor_mDED15E845649252D96352EEA178D5844964DC43B,
	NetTcpBase_Send_m5229E9056BEF1DDF03A2FC87CAB96030DD6329ED,
	NetTcpBase_OnReceive_mCFCC08BA749AC78A2E5AAFF292351F4BBC834B4B,
	NetTcpBase_OnRemove_mA030BFB6CBEB0BA7550A8E313161AE2FFE04D95D,
	NetTcpBase_Start_mAFECA91A6D7B01F78E9EA64F9974AC182B349C5E,
	NetTcpBase_Update_m24B2A56971CA97BA8583C9585F31C9E39C73C12E,
	NetTcpBase_OnDestroy_mBE8F642D8B8B4E45AB8A07488215AC1C7EA06A3A,
	NetTcpBase_OnNetStart_m554A3B7CE09A5BB57768910D16314851BC41C4C0,
	NetTcpBase_OnNetUpdate_m49B0AB12009A7CD849D9738F7116560F7A2DC0F2,
	NetTcpBase_OnNetDestroy_m902769AAD4D861ED9F09662D4F8D72AF6578E51A,
	NetTcpBase_TcpClientReceiveProcess_mECDD2F5EF18750622DACAE164331B9921C7BA355,
	NetTcpBase_ParseJsonCommand_m89313DB8DAC00805CB8D64447D8A7A058B7FA06C,
	NetTcpBase_AddReceive_mFC7D4AB6F014D39D44BB7CEE16CFF614A32A1A97,
	NetTcpBase_AddReceive_mA07BE09459FF87EA10102DF24AA33E8AA748261E,
	NetTcpBase_RemoveReceive_m84F153C2388137303D4CDCAE9E6A81FD47375DD2,
	NetTcpBase_RemoveReceive_m03CFDFF5928FFF834E4251012697EDE7427FEF8C,
	NetTcpBase_GetSelfIP_m8EE4F1D489156A78440FD88382E99EBEDF39A95E,
	NetTcpBase__ctor_mBC289CF4CB7D736497D60AC279DB553C95E4DF90,
	NetTcpClient_get_State_mE2A95D990953181B048F28A8963AFB998B392198,
	NetTcpClient_set_State_m27337EE737FA5F730A0C6FF77E90CF534676B7F9,
	NetTcpClient_get_IsConnected_m44DDE5F97F3013463D1A2647F3A284DECC273716,
	NetTcpClient_OnNetStart_mB977D1201C8589B6AE2E0FC76A7C40FB5DD5AA28,
	NetTcpClient_OnNetUpdate_mACD7D4F80DFCC8B6A0A6AD6621EFAE76A1AD8F20,
	NetTcpClient_OnNetDestroy_m2515A6821A1B5DBF9A19DCFA03973BFF1A020291,
	NetTcpClient_IEConnectChecker_m65AF567FDA8580CB6E511D6973CBB1FDEE354EBA,
	NetTcpClient_StartConnection_m980500371FD3DC817B5AA91C144596ADBFCED67C,
	NetTcpClient_CloseConnection_mD6074A0496B50EF173D999428277BAB7D741E3E8,
	NetTcpClient_ListenForData_m855A8EA35E9E327F423820AF6885A5F804C6084C,
	NetTcpClient_Send_m6C2A8469FF603DF0855CE236DF7A59DD12640C39,
	NetTcpClient_StartConnect_mE48822326008B46D7F2D153020C1E2758083189A,
	NetTcpClient_StopConnect_mBB4919C3A3C1C5F3314534C12893193B7D00FB70,
	NetTcpClient_TestSend_m838FC6C444919FDFAFBFADCE59A029A7C82F13E3,
	NetTcpClient__ctor_m17878DD54EF77E6C5D3C733ECA0895AB1EC58579,
	ServerConfig__ctor_m74167241DE821404E2DFB7B6AF4CCAB417EE14D9,
	U3CIEConnectCheckerU3Ed__18__ctor_m7667FA4CAFD90218927D7B52D5C7DCC92D29D42F,
	U3CIEConnectCheckerU3Ed__18_System_IDisposable_Dispose_mA419303A04EC03BA765C4A7660F46207C19C4B87,
	U3CIEConnectCheckerU3Ed__18_MoveNext_m17D24B9035EC2C4C5713FF8AA61FB2BCB8479D7E,
	U3CIEConnectCheckerU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB75DB48057F0B87063D27C3B63974091B5541E52,
	U3CIEConnectCheckerU3Ed__18_System_Collections_IEnumerator_Reset_mDF7EC0D8FDF7631E4EFD68C95E0C6482B86BBFF2,
	U3CIEConnectCheckerU3Ed__18_System_Collections_IEnumerator_get_Current_m2D26B9DDC6D4094049C6CC87CA4989579576863E,
	NetTcpClientUI_Awake_m0539284B8A151778038293AFC8FFD8A3DA3FF2D7,
	NetTcpClientUI_Update_m2B739F1B6947162971EE7350B879BD5E89F4CB87,
	NetTcpClientUI_StartConnect_mDE897BB7596A5ABF82BB0298428833679181AA72,
	NetTcpClientUI_StopConnect_m2087F3E9AE5AA60D769D2EED15318C0649DA491B,
	NetTcpClientUI_SendText_m7C56BDB19D020F07D7C7A925F418BB6749494889,
	NetTcpClientUI_OnReceive_m3E70C489683BD75B7D4B8936A79666F3324E1CF6,
	NetTcpClientUI_LogText_m226C76143CEBBF3225B8DD7D440DE6E1B5CD8D73,
	NetTcpClientUI__ctor_mCB8B0382BABA84B5590B0AEF80ED28B7FCDC90A5,
	NetTcpClientUI_U3CAwakeU3Eb__13_0_mED77C26440357DDBC30367A58769D7B63B597971,
	NetTcpClientUI_U3CAwakeU3Eb__13_1_m51316CC0B16C59046404A2BEB870D2444D87B8C3,
	NetTcpClientUI_U3CAwakeU3Eb__13_2_m6926B73F4DD0D775FF0CFB223C7F0A22BE39BCE5,
	U3CU3Ec__DisplayClass13_0__ctor_m58419B6A7FA249D003044A59D1E507585F5721F6,
	U3CU3Ec__DisplayClass13_0_U3CAwakeU3Eb__3_mD9952291B26422C5D0B84DA7F9D124E72FF58FC6,
	ContentStateSample_Start_m322A42195FD7C6192DF1EEC8E7D29E01CD3A9EF8,
	ContentStateSample_OnChangeState_m2EA4B5B62C76F3B373D6D3C2640BADAA363AD58F,
	ContentStateSample_SwitchOn_m7B78C709579965631DAB4078C3A095AA288F294F,
	ContentStateSample_IEPlay_m1B4A4B53DD450D3B5247595F56E85438275FAC34,
	ContentStateSample__ctor_m0397B1EB314ABDBE3459644C953CD7F072528C54,
	U3CIEPlayU3Ed__7__ctor_mD4955882B86C000ACC0498B6865B3815131879E9,
	U3CIEPlayU3Ed__7_System_IDisposable_Dispose_m79F6A7CA3E3EDDF66F1AD65AE609B0DA3B6B3B37,
	U3CIEPlayU3Ed__7_MoveNext_m6A2083F9971992F9BA2B4E8758BF7D8649B5BC46,
	U3CIEPlayU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8AEB494184E6F93754B3C92BC69DAA9A9123D5F9,
	U3CIEPlayU3Ed__7_System_Collections_IEnumerator_Reset_m1390D4048199AF8E1DE64A53BD215F65F6A68C8D,
	U3CIEPlayU3Ed__7_System_Collections_IEnumerator_get_Current_m63E5BAAC69E6912B5EB54ADE17538BD7F605A0EA,
	ScreenSetter_Start_m5509B5038C81DECECB1518783F59A63642F612C2,
	ScreenSetter__ctor_m5C1148FAD264EB456C53E84D3D65EADF78548FC5,
	Ack__ctor_m08234500CEC4683E23A3BB6488CF3E80C0DD1AE6,
	Ack_Invoke_m1684EF9DA9D3477DB4291EC7C7B22E64617D7A3E,
	Ack_ToString_m18C39847D0BEE63C19DACC22F41472D21744F11D,
	SocketIOEvent_get_name_m8BF41B50247424907D32828F045A9AEFD4100A7B,
	SocketIOEvent_set_name_mAB1E4E8E4BEE26376B357723E139F1E00C36285E,
	SocketIOEvent_get_data_mD0BFDDFAF5CED0377B199B49D3E469C8FC9436E3,
	SocketIOEvent_set_data_m0E8C7171DD9F42F8B3B432BE7C4200B5161392D0,
	SocketIOEvent__ctor_mA59F67E832EB9B9A32755553A146B5F09E49FB99,
	SocketIOEvent__ctor_m0F88CC221E37B4C2534F54B6905A36E190D7F3BB,
	Decoder_Decode_mE9916415893AC5EAC4A9591E7986D70BE6356323,
	Decoder__ctor_mFBACFE8DBA1DDF616646C8EED85CA2B648895A9E,
	Encoder_Encode_m32540EDF9016B706E02CB246EA2DBA0EE7993F15,
	Encoder__ctor_m40C2B47D58C46A830F9A97E6316C0C5FB8A4563E,
	Parser_Parse_mCDB90B11F5EE900C8201D843E63E52842D5B28F8,
	Parser_ParseData_mCED4BE76844CA9226D7C5BE92E353EC14D3B86D0,
	Parser__ctor_m3DD403B121B9CC71093A4D8092BA2F37BAA5DAB1,
	SocketOpenData__ctor_mF5E43B32190B2259A1B8DB582B6246AD216513C5,
	SocketIOComponent_get_socket_mA884EBCA27838812B6E294D27FD36E0C3CCFD1A7,
	SocketIOComponent_get_sid_m0C46865BD1B328FED251F23E84F0768B27B470BD,
	SocketIOComponent_set_sid_mA53C6F8F0D778DE207234664352D150C2BA2ADDC,
	SocketIOComponent_get_IsConnected_m9ED854A70EFAE10B4EFC6209EA9A50CFF6332E77,
	SocketIOComponent_IsWebSocketConnected_mA30BCB0FC0190FD722BA9D85232526B35A0D63F7,
	SocketIOComponent_IsWebSocketConnected_m097D3A1E04DACB2220068D4B171B7723ADB9BDFB,
	SocketIOComponent_DebugLog_m8DD4451BC850A1382014C701B91A7AF5D6A79FE8,
	SocketIOComponent_Awake_m68239C7A2402548546E6798F030787D946B09F5D,
	SocketIOComponent_Init_m6E1B61362320E5FAB20CD66D9061AF28D2AC49F4,
	SocketIOComponent_Update_m1956204DD766153E837C3CAE978C534E0EA0C2E0,
	SocketIOComponent_OnDestroy_m830362FAD05E1C5367CBAF76DC68296167600F5F,
	SocketIOComponent_OnApplicationQuit_mF7FF8EE476DFF358E629D6FB68CDB9897163B993,
	SocketIOComponent_Connect_m62FAA7EAE40B164CFE919172454BE0031E778924,
	SocketIOComponent_Close_mF0F34BF8A82E9C34FC5B64253833C8F63D059A71,
	SocketIOComponent_On_m59A96433B41B36469DD95C8E34C3902CE7F89CCE,
	SocketIOComponent_Off_m5C653B03FA1B99F63AE91783395DFBCF44A8C060,
	SocketIOComponent_EmitRaw_mC3061234D412577D3A5EBECBF818BFCEBD52993B,
	SocketIOComponent_Emit_mD6C33729F2C0139BC5D803903476E33B3BA2E69D,
	SocketIOComponent_Emit_m8DA5788D2FBBBF6FC99DF0BF58689C7D9F47608A,
	SocketIOComponent_Emit_mAC61D914F84B78301CD978C01DD5A73E12FA298F,
	SocketIOComponent_Emit_m6C169DB7463C5EA6E415CFFC0D9DB3EB049AE720,
	SocketIOComponent_RunSocketThread_m363B2B632BBF024C54AB8BB194C37573B98FDE46,
	SocketIOComponent_RunPingThread_mBC668F3C3CE8368A7F04CF3C8C8DA72829868EDB,
	SocketIOComponent_EmitMessage_m13602AE9F37F31561E9737869DEEA3214CCBC222,
	SocketIOComponent_EmitClose_m53A0841BB638D99F5E95CCB667E9E0E0D0485350,
	SocketIOComponent_EmitPacket_m87EA1D5B4E9042BB1D7D9BB95B86AE1E9D112968,
	SocketIOComponent_OnOpen_mBDB7B465DA5508D96CAAD6D7AD0A9F075F6C4167,
	SocketIOComponent_OnMessage_m45B84D7E90C2F947A0C7B665BB7E7EE195417CAB,
	SocketIOComponent_HandleOpen_m9CEC78ED9C6141EFCC67A87263B2F2B00E55D0D9,
	SocketIOComponent_HandlePing_m97B4AAA6970A059AFAA628E99E4BDB0DA841134E,
	SocketIOComponent_HandlePong_m4C7BC4C912FA04DCDCFD4F21E8AA282C08C0362B,
	SocketIOComponent_HandleMessage_m8C2D8164F0E809D8ECED2CB4C5B67E5B4E2CEF95,
	SocketIOComponent_OnError_mE37CF1EBD007D80766F71FCFF61159829038D3AB,
	SocketIOComponent_OnClose_m502B56788A12AC02BF7A57DC4BA6DB7189E490CD,
	SocketIOComponent_EmitEvent_mDEF25F0171CBECA7C9E3BF3D9E2EC484ACBB19A3,
	SocketIOComponent_EmitEvent_m3CA2EF1EEAA7B71AADBF48C80A116308E047A08D,
	SocketIOComponent_InvokeAck_m67F9ECDC987E0AB81C68D08136D8619A967BED43,
	SocketIOComponent__ctor_mA0E54A729810BC4451F24547B3EEA6F07AF6D3BC,
	SocketIOException__ctor_mCFF7B8D15E809DB1DA844E1FFEE0676D71F1F7B4,
	SocketIOException__ctor_mC788EB73AD818F9F9BA787DCC771B4D51C7EE27B,
	SocketIOException__ctor_mFC0408FCE9B6F6DF878E7178EFA2A1D62A258624,
	Packet__ctor_mF51931D050DA480074A1DEF47033792270B7A4DC,
	Packet__ctor_m574DBF18FC45DF64A934726B04BD456628659999,
	Packet__ctor_m40039C66ED685CD2480AD0571FBA9B5C9D1AF139,
	ApplyToMaterial_get_Player_mB6C8A0EF379B2463DA0545E230B21EAD550B8E57,
	ApplyToMaterial_set_Player_m01EC27B0C640E4009F2878B73122A01D8ABD9D72,
	ApplyToMaterial_get_DefaultTexture_m4C03F0037670A9B098D7AB3AD72705C9F96C9ECA,
	ApplyToMaterial_set_DefaultTexture_m64F519D1141C4C13C498E08B69F1A2786DDA1A96,
	ApplyToMaterial_get_Material_m8DF293249B9F2C1A20FEFA2D4E91FBB14E808C49,
	ApplyToMaterial_set_Material_m79C35328320609A3F46FAC341952715AF67C5F95,
	ApplyToMaterial_get_TexturePropertyName_m0EBCDD3F8141A6ACEFE40083833B0BBE6D68F990,
	ApplyToMaterial_set_TexturePropertyName_m68E4267E3E9EF5BD0668646E3B4B2EDDDA964624,
	ApplyToMaterial_get_Offset_m2F7740DDBE943479863AB8073927D8DBD0117EBC,
	ApplyToMaterial_set_Offset_m8FF499A647BEF4A7ECDC2BA3C75C28F577263516,
	ApplyToMaterial_get_Scale_m389E6835F369AF3E1E8594B69C190F34B44BFFD9,
	ApplyToMaterial_set_Scale_m9D411AC85C79799D9C0E8C6F61B5C6C6174DEBAB,
	ApplyToMaterial_Awake_m7E57B5B508BC0E5CE63E5EAC7371EE0760201858,
	ApplyToMaterial_ChangeMediaPlayer_m0745378FC9807E03AA1609DE4DCC8D92DA75AD4F,
	ApplyToMaterial_ForceUpdate_m3AE2E809819157595FCD1FDC8201B5B8366E996B,
	ApplyToMaterial_OnMediaPlayerEvent_m3A938D7E9BC8DD5434039E4DC5610BFEFB8E9352,
	ApplyToMaterial_LateUpdate_m901EFD1FC7B6BA3DC9963B546008E9AB93AB8FF1,
	ApplyToMaterial_ApplyMapping_mA9DC832B6875DF510DB1B8B8FFC169402A5F9D2B,
	ApplyToMaterial_Start_m2BB52F08B0BC2F1AE024F1AEC8C1B1297D18BE52,
	ApplyToMaterial_OnEnable_m846E313CC473E6FCA45D5A54E1BD5B5320E6F79A,
	ApplyToMaterial_OnDisable_mF74CA0E13A95F4F466326E3D15D51FDB9348B29E,
	ApplyToMaterial_SaveProperties_mFB26241C77CFBBA00F2331E67F8B7C49CEB266EB,
	ApplyToMaterial_RestoreProperties_m7FF3523DC1A19EA6A9D1D8BF16D1D96A13DD07CA,
	ApplyToMaterial__ctor_m962029797C27CD72C83644B71FF42FAC2640C931,
	ApplyToMesh_get_Player_mA443704ECC9CAB41E66661C62837724881005F14,
	ApplyToMesh_set_Player_mFEEB90240904C21B6D4AB9E3A7A38563100CFC33,
	ApplyToMesh_get_DefaultTexture_m4FF557DE6887E102DC0D8FFDE62A7BEF89450D59,
	ApplyToMesh_set_DefaultTexture_m2B230BE4C850A453459C8E32FE2FDCD5A4E09FC9,
	ApplyToMesh_get_MeshRenderer_m00FAA70B2A56BC20EF9E578C47ACAC454C8BB1B0,
	ApplyToMesh_set_MeshRenderer_m6CA21013EB5DF529CFE2EB772A52B1914EADC2A5,
	ApplyToMesh_get_TexturePropertyName_m71146112A3E29E7C3D4BED6C1693FEBAB939893C,
	ApplyToMesh_set_TexturePropertyName_m2C04ED369F1B3F05F2BA6C147B9ADEE0D51FE37D,
	ApplyToMesh_get_Offset_mC45ACBF08437FABCF7CBAAB56690DEA13C9C89BD,
	ApplyToMesh_set_Offset_mF7CA89E15E34A84976CE82D4271DECD8FF3496E5,
	ApplyToMesh_get_Scale_m1A934B3665631E10868E07034CAAFFBFE0106F97,
	ApplyToMesh_set_Scale_m2C74ABE8E9B235E05579C2400A2F16F360F37248,
	ApplyToMesh_Awake_mC9F830518F1F041ADF55A41661783FAFD070E634,
	ApplyToMesh_ForceUpdate_mC90728E69359AB7AB69A608DDB421BBE52CA54E2,
	ApplyToMesh_OnMediaPlayerEvent_m97E527922684F252D4BAD9EA3C1B59E18CAAD8F4,
	ApplyToMesh_ChangeMediaPlayer_mE3B842C698FF969B7A60AEACD39A0EBF0F38E20B,
	ApplyToMesh_LateUpdate_m43ED3BD5D74AE724DC018AE44C62D339CE914D9D,
	ApplyToMesh_ApplyMapping_mC917218EF3DE8D576C73A4273271100FBCAC1811,
	ApplyToMesh_OnEnable_m5302692EC0A37A56FA7200903B043F0149510879,
	ApplyToMesh_OnDisable_mA154C54CD3A11D695E453AC06B2F6EEE4B35B41E,
	ApplyToMesh_OnDestroy_mD9B923F54EE32F7D3A8B10EA49358DD85B87F241,
	ApplyToMesh__ctor_m9853935678DD2E16331C07E8E4C2BFFACE641E6A,
	AudioChannelMixer_get_Channel_m36DE91B7CC4EF0018A665D0F751C42EB8961FB2E,
	AudioChannelMixer_set_Channel_m17FB468A2757EDCC01E04C0CD5E8110BC95787A5,
	AudioChannelMixer_Reset_m72348CC0D4BA9583B644CD63142A18C976226762,
	AudioChannelMixer_ChangeChannelCount_m1A227F7C5982F0E664D5187F612D0833B5A25B85,
	AudioChannelMixer_OnAudioFilterRead_m07A362C3079507ED94ECECD378DC4A848068D897,
	AudioChannelMixer__ctor_m15D089E1CFD6D6AFC204941FDF4805F1E3B0E35A,
	AudioOutput_Awake_mBB71EB6182D93E59CAD7C6DE66925C21A7DD1E4E,
	AudioOutput_Start_m1E8E1637B93BB98B3DB1C47C3E1D4C4252E42934,
	AudioOutput_OnDestroy_m1732E24E26E46EA8A55A286B422F9A5AA3F5A834,
	AudioOutput_Update_m8DDA88A39B472AC19C3701C6955EA53C382F178E,
	AudioOutput_ChangeMediaPlayer_m5C841D792F1E33838683AF29FEA0CA10224EE255,
	AudioOutput_OnMediaPlayerEvent_m640548026E67B82F85AE39691D83AAA9CED8E928,
	AudioOutput_ApplyAudioSettings_m155E212F219AB6E0F6B16B38F1899D7E488CDFF1,
	AudioOutput_OnAudioFilterRead_m92C2E01B546CC4C225ABC63F1083FEF45138C4DE,
	AudioOutput__ctor_mD734F7D2FF41D3BC077C59058EB696F8B9CC525F,
	CubemapCube_set_Player_m786DBA437A00F219B8724B4B790127041BFD3F1A,
	CubemapCube_get_Player_m45C5250BADD23763B85A8B24348C659A56B9A070,
	CubemapCube_Awake_mE13D80223A30E1051C55C73E76CDF29C202E85FE,
	CubemapCube_Start_m0C9A1CD846CA7780FDEF2839C1D5D94E533D7B22,
	CubemapCube_OnDestroy_m529E238B85070B02314BFE3236F72E043528981F,
	CubemapCube_LateUpdate_m85A7FC1AF25749568D38BAA10877993B86C4E15F,
	CubemapCube_BuildMesh_m11D141C43A762E2773E547628158FB6F7B3E0C92,
	CubemapCube_UpdateMeshUV_m8B9A0835BB3532A1FB4F5A45300E0DE3C5090281,
	CubemapCube__ctor_mA2E10BF9977ADC4D8E9192A46A078F4EBB2FB3F7,
	DebugOverlay_get_DisplayControls_mA25296A3959714C32C8944C41F6FDE3F89696C91,
	DebugOverlay_set_DisplayControls_m8574F621F6A16842A6D1532A5E423C1D381A595F,
	DebugOverlay_get_CurrentMediaPlayer_m68170AB21C0C1F9EF31238EA3CE20A9C3F2A7E6A,
	DebugOverlay_set_CurrentMediaPlayer_m807A6CF1DEF8F34106BFEA2E45E2D9753D0A4EA9,
	DebugOverlay_SetGuiPositionFromVideoIndex_mE2C944733349B606C8FD4B0FAFC0EA982BC2DBD9,
	DebugOverlay_Update_mB215173AEF7E7062D045B338365B90CBC55C1E1B,
	DebugOverlay_OnGUI_mF431BD39EC8BE99F13FF588DCB6110DF56236DEC,
	DebugOverlay__ctor_mA38FC6D9F4B3ED5BCD236D24AA1C994E28FFC0A0,
	DisplayBackground_OnRenderObject_m13305EE4E1780947B07A41385E7503A0BDCD86A6,
	DisplayBackground__ctor_mAD78B661699F7B4C90000B3B906772EDA2B9AAA8,
	DisplayIMGUI_Awake_mA008F02B94AA77426ABA34822495C45A965A3FEC,
	DisplayIMGUI_Start_mC915CC4ABC539AB9BD10795DC320CCB20B251293,
	DisplayIMGUI_OnDestroy_mD32A66825532BF56B9136001F325D5B3AB4C163D,
	DisplayIMGUI_GetRequiredShader_m85B047763F7B734407E0CEDBEC51CEDABAF50D78,
	DisplayIMGUI_Update_m4C60931D8BFD20E349E2E601B701D029618F2940,
	DisplayIMGUI_OnGUI_mB90CC061E60EFB384DC4DC5E742E6BE85831B795,
	DisplayIMGUI_GetRect_m62FA7CE5192D732BF264126F4437A186FDD9B922,
	DisplayIMGUI__ctor_m4C1F51E569AAED48140CE7CFF37A7516C06AB1B8,
	DisplayUGUI_Awake_m1100BEFC95A78728DED3C156861B8351C0077552,
	DisplayUGUI_HasMask_m5DC9A6B4A7CA49383C70AA100F3D4AE9B41CDA03,
	DisplayUGUI_EnsureShader_m26A38D72BDBF78F5AEBE8A5DBC65E5EAA60BB758,
	DisplayUGUI_EnsureAlphaPackingShader_mF6561C2FB86D60F3AB5CF2D9698A25DD400A0EB3,
	DisplayUGUI_EnsureStereoPackingShader_mBA86459C1A369B131F2069A91B3C528D1A3C7E30,
	DisplayUGUI_Start_m340D6AC57FCC4A534FC8F7D601EAD815687246A8,
	DisplayUGUI_OnDestroy_m36BDD82F1030860A005BEC3D1983AF2FB2865E9E,
	DisplayUGUI_GetRequiredShader_mEEE05823A9985B7E30669FEA97A876676EC73E76,
	DisplayUGUI_get_mainTexture_mD641E773EBB910A6F937B11A68A6BA8D5EE2EEB0,
	DisplayUGUI_HasValidTexture_mC46FA36F2F674D62B0C0FBE269311F3F07E3D626,
	DisplayUGUI_UpdateInternalMaterial_m4D8CCFF9B776EF4523B908ED69A3A9276FC7337E,
	DisplayUGUI_LateUpdate_m803830F93553EFECE1BD1027151043FB301B5B9D,
	DisplayUGUI_get_CurrentMediaPlayer_m6413594BD172B2BE0F01A23A22ECD6E0A44E914A,
	DisplayUGUI_set_CurrentMediaPlayer_m0137026F76CDB76222CB7D154FBE5BFA44F6D415,
	DisplayUGUI_get_uvRect_m5BA7C10E45A554F5134B740A498CF296FA77A48F,
	DisplayUGUI_set_uvRect_mE6FD948006B313F5E722F0EF75D7EE4E4EE148F4,
	DisplayUGUI_SetNativeSize_mBFB78E70F72FACE80348730711BC1EBED76D430D,
	DisplayUGUI_OnPopulateMesh_mB6B84D334C9C2EB105650CD0DFDE9D57FDDAB757,
	DisplayUGUI_OnFillVBO_m4B2E50B68D5D2288CFF34E5770AEF75A9EE350E1,
	DisplayUGUI__OnFillVBO_mE5A8F484001FC74FA85E4269124511E781C3F3CD,
	DisplayUGUI_GetDrawingDimensions_mBA562A0DD5E74F6B38E54DACF1431589D11494F9,
	DisplayUGUI__ctor_m173552BE5F1B4158F144E3EA4B5401C604E57C0C,
	DisplayUGUI__cctor_mDA8776C0893A01DC3126AF144EFCFE11EE7D8701,
	MediaPlayer_get_FrameResampler_mFD4B144D49EBC8C5E71D2EC06EA5496016CEC775,
	MediaPlayer_get_Persistent_m8B7F0802B6A0FEF69554DBA04714612FABB9AD9C,
	MediaPlayer_set_Persistent_m0DA7A5514D93EFCF8161B0F830CBE54E910EDC43,
	MediaPlayer_get_VideoLayoutMapping_m6DA508DD29B8E3845E79BA5CB3AED45D1B8DB4D4,
	MediaPlayer_set_VideoLayoutMapping_mAA98A49170DDC5F7B1951E7A38B6CE84CBFAD383,
	MediaPlayer_get_Info_m892C1B5EE61D1CA6EAE5D4B7CEE6BCC5095F5E25,
	MediaPlayer_get_Control_m9F501D1DC633B3B0C9A7F19F4DEC9942CB502785,
	MediaPlayer_get_Player_mD38D49159D63F0392915E24501953BA7222A3C94,
	MediaPlayer_get_TextureProducer_m859AA3620F1A0EF46BB8CB76A4ECC1D952734D30,
	MediaPlayer_get_Subtitles_mC487084FFA754521D3673E9CC9088CA66BB21EC9,
	MediaPlayer_get_Events_m6AA188A1F784751E6E4C011CDF9C435BC22747D1,
	MediaPlayer_get_VideoOpened_m0ECBBEAEF0C2318D2825FBB2954781AF6A1DB554,
	MediaPlayer_get_PauseMediaOnAppPause_mB510C4F2BF54D080F4C16B2C993CC51E21976EF7,
	MediaPlayer_set_PauseMediaOnAppPause_m181588E3BBE8EF18B3D62D429E82E6A13A54638B,
	MediaPlayer_get_PlayMediaOnAppUnpause_m34DC30B0AD6C2FCB474B0AC28C53BF40C744F37D,
	MediaPlayer_set_PlayMediaOnAppUnpause_m52D2A2587E418FFDF3345AF9B171AAB917A7EC83,
	MediaPlayer_get_ForceFileFormat_mEF3A7984D5B24D7D13975680FBF7445100B5F691,
	MediaPlayer_set_ForceFileFormat_m7574D7273C22D98272945A528BA205113C6CFAC1,
	MediaPlayer_set_AudioHeadTransform_m3BF627799C6270FCCE5479547086BFCCC4F3865E,
	MediaPlayer_get_AudioHeadTransform_m19E267C75F48D9A9CDECC0F7D6607ACACB37C8C5,
	MediaPlayer_get_AudioFocusEnabled_m78DBBD149307F7E97754F7A44F751F14D3170E40,
	MediaPlayer_set_AudioFocusEnabled_m1072BCF69DE55ECD9E4A0FEE88ED44E4CF47F962,
	MediaPlayer_get_AudioFocusOffLevelDB_mC4284F2F6BFB7C1D983D270983AA798D07A7B78D,
	MediaPlayer_set_AudioFocusOffLevelDB_mE858792F9C6BC1E75F5C189221D68B3471BD81AB,
	MediaPlayer_get_AudioFocusWidthDegrees_m52071D006D7F6D06E2C61B0F5508426FA838AB85,
	MediaPlayer_set_AudioFocusWidthDegrees_m9067556A5EB455E5DD5E64810A3C32522EF1A79C,
	MediaPlayer_get_AudioFocusTransform_mA161A65E069EBF3BABB6F553470EB1935F6CF46C,
	MediaPlayer_set_AudioFocusTransform_m5B46E3A23179B11CBE23390467EB42E159DDF47A,
	MediaPlayer_get_PlatformOptionsWindows_m32BEE4FD8E72E2FC34401A4AFC60D6B924847E12,
	MediaPlayer_get_PlatformOptionsMacOSX_m6DFEA8DE608A93BD9481F99D76934A8CDBDCCA5E,
	MediaPlayer_get_PlatformOptionsIOS_m80010BFF3D40711BEE55AC6318A3C17CCF20D3BF,
	MediaPlayer_get_PlatformOptionsTVOS_m0749CF890B381DF64D6791F25948604BBBD530CB,
	MediaPlayer_get_PlatformOptionsAndroid_m587557F7E207174FDD4DEBEF10E14DF565DAEF68,
	MediaPlayer_get_PlatformOptionsWindowsPhone_mEDAE7CA6631BFE9374BF274134DA6C0381CE306C,
	MediaPlayer_get_PlatformOptionsWindowsUWP_m41EFFEA11D55D7DA8D6A4F1B2C782E4EAB332793,
	MediaPlayer_get_PlatformOptionsWebGL_m8A785B44082027BD6050750E123874834F309348,
	MediaPlayer_get_PlatformOptionsPS4_m272F9F2F70F840B2704477A8F225E85F912F424D,
	MediaPlayer_Awake_m81585DF80FFC96ED2FAF6B5694A285DEE2E25CC5,
	MediaPlayer_Initialise_mC02870F200DBB011EF0D69F35B91556886BAFE1C,
	MediaPlayer_Start_mA2B1902059842C2F2A6452D34300B6378F2BDD86,
	MediaPlayer_OpenVideoFromFile_mF8BBE1AF6D372DA420FAEA6544FD99B3F852C7D4,
	MediaPlayer_OpenVideoFromBuffer_mFD6AEA9C0F03D1ACF8CBABBE452B1887D8854E6C,
	MediaPlayer_StartOpenChunkedVideoFromBuffer_mB26D87D4A0FFC1D39FED64D1A7D743458D311300,
	MediaPlayer_AddChunkToVideoBuffer_m84548CCDD0FD75D4EC9CBA422F4514D2545AEF10,
	MediaPlayer_EndOpenChunkedVideoFromBuffer_m936370BFA408F2F4685C8D7AAC7A651F0FA87C30,
	MediaPlayer_get_SubtitlesEnabled_m41791B6BE4871E561B7E1515280EA27D17482FC0,
	MediaPlayer_get_SubtitlePath_m7C69DA4B86C83150E3F298CCA675702B015C5221,
	MediaPlayer_get_SubtitleLocation_mDBD69B1ED28FFC0B7D7E37569F0F59EB643D020D,
	MediaPlayer_EnableSubtitles_mF697604562DF14D9337BCB5AA1E1FFBCA6CE84ED,
	MediaPlayer_LoadSubtitlesCoroutine_mC1C588A71F4166374D28AE67922782263215AB90,
	MediaPlayer_DisableSubtitles_mDD634C56194D865C80B899AE2FDE8306B2B60651,
	MediaPlayer_OpenVideoFromBufferInternal_m8B3AA056B2B742D23080B83DDCCA810AA20AD691,
	MediaPlayer_StartOpenVideoFromBufferInternal_m859DA6C32CF39B1E44414F81F41AA2357B6946CA,
	MediaPlayer_AddChunkToBufferInternal_m8B621222B739B3AA135B244C0AD23BECAF61BFA5,
	MediaPlayer_EndOpenVideoFromBufferInternal_mB6E615E12FF5AEEE02E9F52842901B9D2CD47397,
	MediaPlayer_OpenVideoFromFile_m1E7E6CBB1217E4CC631EF0C40F980184E4E3C3AD,
	MediaPlayer_SetPlaybackOptions_mE1766C4B3918B7B3C46EBE05EDE96F3026B096A9,
	MediaPlayer_CloseVideo_mFA38C016D94EB68335AF5FD633C4656B2985E0BA,
	MediaPlayer_Play_mDCA8D2FD3713AD945BA974B834C4BC2B3333CA10,
	MediaPlayer_Pause_m5DCF2D66E3D766151FFD28C01601ED58EDB6D2BF,
	MediaPlayer_Stop_m7D021D8158E851FC42FFD244AEC95694D87C6F5F,
	MediaPlayer_Rewind_mF70EC8972BD5C7C80E27069ABFA28B2AD4922814,
	MediaPlayer_Update_mAB86CC034891635C1DF6EDA82D2D4C3C38A1B380,
	MediaPlayer_LateUpdate_m981B468BA9E097CAAC93EC48C535DB8984CB1D5B,
	MediaPlayer_UpdateResampler_m9CCE634CC4598192B07B2CDB66F11DFC4439F38F,
	MediaPlayer_OnEnable_mB8E7F8702477636CA36075BCBC08E855F5CA1317,
	MediaPlayer_OnDisable_mC64A3FCF151089B3F87BD0DC89E4DB5F3F22010B,
	MediaPlayer_OnDestroy_m33D0537F397377727E7A2FC4C856FF20C750C7F1,
	MediaPlayer_OnApplicationQuit_m6413D5BE3FB31758E3BF472E67D0084EA6203D82,
	MediaPlayer_StartRenderCoroutine_mFB469F063776D12BE46098E7BFD909D75F98A5DE,
	MediaPlayer_StopRenderCoroutine_m7175FD751272DFAB1BBB26D8EBB2D48341246FC5,
	MediaPlayer_FinalRenderCapture_m595B57A33925C770854F1072776D29CA813858B5,
	MediaPlayer_GetPlatform_m56A303D68A3D0BBF592A6062850C02F5B392430F,
	MediaPlayer_GetCurrentPlatformOptions_m625311690AC47FBADEE660C6C264F0FDEDA851A6,
	MediaPlayer_GetPath_mF9FC7476D5B2BCBC16F4BDE13F88736B3869E306,
	MediaPlayer_GetFilePath_mB6F36FF9EAAB948837D93821086E2FAAE86A4312,
	MediaPlayer_GetPlatformVideoApiString_m5EF740D370B119FFCB15A4587857AA949A65FA45,
	MediaPlayer_GetPlatformFileOffset_mF5C5FFDCA07EB8F9654796EC6BBB403DF744CAC5,
	MediaPlayer_GetPlatformHttpHeaderJson_mDB1709D38721AA79E78D4DD97DDABC0457A42BB9,
	MediaPlayer_GetPlatformFilePath_m83BDED03DFFCF7589DBD8AB1ED589C3588297D9C,
	MediaPlayer_CreatePlatformMediaPlayer_m944F7C6C6CBB983AE00F37BCC01D7406FF5565D0,
	MediaPlayer_ForceWaitForNewFrame_mAC2324027286B166003C076538393B4BD596176F,
	MediaPlayer_UpdateAudioFocus_mCB989D0794C9B95C324B56FD022E84EB365D9B03,
	MediaPlayer_UpdateAudioHeadTransform_m0BB617E534FE76BFBD82D01ADB7ECAEB9D4C2632,
	MediaPlayer_UpdateErrors_mE4B8A878D2788E1E7C9C1ACCFA44D56E34FE5E3E,
	MediaPlayer_UpdateEvents_m50151BA7A595CA1F79DE9D3CBA873CD1AA3DCD50,
	MediaPlayer_IsHandleEvent_m7B9626A1AD683F5B668CCBF54C73B9CD536F77BC,
	MediaPlayer_FireEventIfPossible_mAF2739727EC4B7EF7369B6C33BC72C9987FC1C33,
	MediaPlayer_CanFireEvent_m4A853CD3B2DFFB1397A41EBD832A0006AD8C2594,
	MediaPlayer_OnApplicationFocus_mC2EE8C5FA60A9D479B1403A7D1915D3894F1A238,
	MediaPlayer_OnApplicationPause_mAF10B4F42272741F5318794AD4042E01692C8DCA,
	MediaPlayer_GetDummyCamera_m34919C27E907E4965F535B45314BEE3E97BBB707,
	MediaPlayer_ExtractFrameCoroutine_m753D87BA7EEC8FB35B19A3B3BCD0CD8B094D5ADB,
	MediaPlayer_ExtractFrameAsync_mAFC39B40F066960CE802DA0988C830DAEEEC0FE5,
	MediaPlayer_ExtractFrame_mBF7C7650151D9549A9F680B70EF80205C3C19BBA,
	MediaPlayer_ExtractFrame_mF22A5879E2BDCC4509A1D1219D5BAF6EBF80EC74,
	MediaPlayer__ctor_mEA0B8EE8782727537BAA83041F31E886AE7A68F8,
	MediaPlayer__cctor_mD9BDEB0FD8D2CFCD5211CF04AA64CC023BC950A6,
	Setup__ctor_m2DD91DC7933F9E014D61CDD6E9C3F9E884F9E061,
	PlatformOptions_IsModified_m71C51B232FA4BD4F26E2128AC47A3399BC9A6B33,
	PlatformOptions_GetKeyServerURL_m4707689FF84155F90F0BCB978672DF512D5CFC2B,
	PlatformOptions_GetKeyServerAuthToken_m3E67CC2EEC1E45DAD52BAA67AA7B06EA2960191A,
	PlatformOptions_GetDecryptionKey_mB1B68C3D50435FB3C52CA657FE0B9DADECE28ABA,
	PlatformOptions_StringAsJsonString_mBA55018151E7D4D86B209D3781F51289997AF4F4,
	PlatformOptions_ParseJsonHTTPHeadersIntoHTTPHeaderList_mD8B18ABB2BFF9E4F5059E83F0062155CCFC4669E,
	PlatformOptions__ctor_mF91EEB8BD3ACDD8C1C8D5E3FFE2D64B8320CF778,
	HTTPHeader__ctor_mA1B6C1144262977F342F0E56019D842A4253C638,
	OptionsWindows_IsModified_mDAEA9CB1817C744E9DCBE74B0BC8BD5BFD14FC53,
	OptionsWindows__ctor_m2E1577E0C22C717A9D6FF6ED394DB042D3293EAE,
	OptionsApple_GetHTTPHeadersAsJSON_m011DE9BDDC01267B30B8B9134F176758BCC8315A,
	OptionsApple_IsModified_m49D70F8F37E58D8DD498E54748CF339BED533973,
	OptionsApple_GetKeyServerURL_mE02C721161486FF7610C9D359A71174D797AE100,
	OptionsApple_GetKeyServerAuthToken_m4DAE19686FD1FA3B33B083AFC4B4042EA3115C05,
	OptionsApple_GetDecryptionKey_mFE8625B7498515C5694FBC205DAEE4FAA2F1DD8E,
	OptionsApple_OnBeforeSerialize_m6748B4B5D209AEFB8FC4247FC4AAFBD0C9AE43CF,
	OptionsApple_OnAfterDeserialize_mF63E8E76CDD82A22CA90CD166C916E5FD9D0E001,
	OptionsApple__ctor_m73BE9C207C01BA66D9D3F5B03C6E8429E8419A4D,
	OptionsMacOSX__ctor_m1014A2B088839E88607252E8C6E7E1B555B9A24E,
	OptionsIOS_IsModified_m92628B29EF881A6881A5BDC3BE0646D80C7E5C81,
	OptionsIOS__ctor_m35ED509D6724A6999C186E40CAC9611A9A542297,
	OptionsTVOS__ctor_m25AE150249DC2EB468D63C69494194D53CE3A419,
	OptionsAndroid_GetHTTPHeadersAsJSON_m4F395FEEDA90E4EF61104DD370B3455AA989617F,
	OptionsAndroid_IsModified_mE87FA7C79CDC62FB4B21ABD7B40639AFED2D4B9E,
	OptionsAndroid_OnBeforeSerialize_mBBAD61956D439415F29E77F553F439A3A2FF3758,
	OptionsAndroid_OnAfterDeserialize_mF2BE9837CDB7B2954E9B8B38CB4CAEAC6D5F4D78,
	OptionsAndroid__ctor_m928A6791182AF8083CA560AF4EA43580CD7D6921,
	OptionsWindowsPhone_IsModified_mCF06D8E87EC7E282F72E185201BB6D6398DCD0FB,
	OptionsWindowsPhone__ctor_m40497DE961E324A4C54E7E2DA2BC7F6D1EEF49E0,
	OptionsWindowsUWP_IsModified_mD1B24222D21BB71742D025747AC2BCD35D092CCA,
	OptionsWindowsUWP__ctor_m25F9B0B015AA4552C41CCA9B7FCEA92E12277623,
	OptionsWebGL_IsModified_mE9612729CCD42B4AB1B3240EF0706A9DC955851B,
	OptionsWebGL__ctor_mD167207B291900B8BDEEE7478203783CBF1409A9,
	OptionsPS4__ctor_m5FCC4BF70D296EA41734D313CC8081052158F83F,
	ProcessExtractedFrame__ctor_mDEA4881A022C1FB4FAA06AE958EB08D21D811109,
	ProcessExtractedFrame_Invoke_mE91683088863043AEABFD0ABAE4D1B68EF606D2E,
	ProcessExtractedFrame_BeginInvoke_mEC4DB2F3EF07BFFC7940E451FA628B87FC4438D7,
	ProcessExtractedFrame_EndInvoke_mD784C44DD4F9C1B5E634A4643127B5B22850C3D4,
	U3CLoadSubtitlesCoroutineU3Ed__166__ctor_m3BA5CF390664C02F54BAE83EE3AD1894453FCABE,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_IDisposable_Dispose_m7C6143DF57A1AA20F88AFCA1FEA975830612BA18,
	U3CLoadSubtitlesCoroutineU3Ed__166_MoveNext_mA91A20EBBEE60FA01B42B5AFD2EB0A643C45DD57,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m870B6EA79DC2C40F77AB31033DE628C6026EC048,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_Reset_mFBED6405DCDF1AD12602131C68CF0E05115BF583,
	U3CLoadSubtitlesCoroutineU3Ed__166_System_Collections_IEnumerator_get_Current_m18D39C4AB130195E8C8551EF9B6E42575C587210,
	U3CFinalRenderCaptureU3Ed__188__ctor_m83AEB76C2A2B5F62EF60A95F1F876F0E30A870DB,
	U3CFinalRenderCaptureU3Ed__188_System_IDisposable_Dispose_m169F9FB9630C9B699CA8E558353DC0133E1D4846,
	U3CFinalRenderCaptureU3Ed__188_MoveNext_m5A26C83C7146D00BAABB7FDD64756432853E6587,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m36D557AD55F693C628CA2C0013C8638AD245E0B0,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_Reset_m85561D95C6DA29995D900F1CD05EE916ADBB5BBC,
	U3CFinalRenderCaptureU3Ed__188_System_Collections_IEnumerator_get_Current_m91F1095E060AB713424B739E24D53791659C0C21,
	U3CExtractFrameCoroutineU3Ed__209__ctor_mA11B8601F5A169D372F306455057C7B2D907D040,
	U3CExtractFrameCoroutineU3Ed__209_System_IDisposable_Dispose_m8C7D508575FE7B7BFEF701CC14E48810F84E87A7,
	U3CExtractFrameCoroutineU3Ed__209_MoveNext_m77BB9A547A71459D4145517F51544D2BBAE0E2CE,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3C2D5F368AFFE770F3644C92EB05991CB4AAECF,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_Reset_m36A003F3565EF812B375B183BDBAFBC032E7A63E,
	U3CExtractFrameCoroutineU3Ed__209_System_Collections_IEnumerator_get_Current_m2F5AD6BB6BC4507304D46E6CC65AC53D9A1A2739,
	MediaPlaylist_get_Items_mD2A5662FB9ACA992687BFD278CCC3046B688C649,
	MediaPlaylist_HasItemAt_m7576A76184605ABC57C833A18C9E5FF673F8CF59,
	MediaPlaylist__ctor_m9C7ED936C5544B6BC82C2D392D5522C9B272592C,
	MediaItem__ctor_mE27521C47613D523F763838EA55107E722A58CFC,
	PlaylistMediaPlayer_get_CurrentPlayer_m6A41483F6DB31986090E5110093BF50A6200F159,
	PlaylistMediaPlayer_get_NextPlayer_m14BE7712C77CB964ACD5D3601D2ADF40C392F549,
	PlaylistMediaPlayer_get_Playlist_m8718E4C723952CE65990D7075FE3B8DB22FE6E68,
	PlaylistMediaPlayer_get_PlaylistIndex_m9534F26937A29B1D0906DC88987A10096AA94156,
	PlaylistMediaPlayer_get_PlaylistItem_mE9C2E47E38EBA2AAE0892AF32D974BB44DDD85F4,
	PlaylistMediaPlayer_get_LoopMode_m4F26EDC7E0163F05C7F19328DC5C170E70783503,
	PlaylistMediaPlayer_set_LoopMode_m6C617F2E4BAB35FDA9AE24C661AC701F575C6C56,
	PlaylistMediaPlayer_get_AutoProgress_m00E75AC847D3B435AA4F131A83BD97643816D393,
	PlaylistMediaPlayer_set_AutoProgress_m5839E3AF4D61C2FEC681187E1D6E573521A2F310,
	PlaylistMediaPlayer_get_Info_m8861FFDCABB53D1B116E1166B2F7A66BA615E2DF,
	PlaylistMediaPlayer_get_Control_mB2433EDC44C5FF8919CAE207E807A723C8498872,
	PlaylistMediaPlayer_get_TextureProducer_mE7E07EB5BFF6D22C40694BE74158BA038C8AC353,
	PlaylistMediaPlayer_SwapPlayers_m6AA34F0D90C14E76CF2088C3E8161AE4563499D2,
	PlaylistMediaPlayer_GetCurrentTexture_m00FF396CBAD27D08E1718EE7DF044D5756F7D007,
	PlaylistMediaPlayer_GetNextTexture_m1D779DFC4F971548B55FE030C9E5A25D93E68C49,
	PlaylistMediaPlayer_Awake_m6F70FB582484067750B4E136EC28C4C3D100F1CB,
	PlaylistMediaPlayer_OnDestroy_mE90A892567B8F8FDEED6CDB5D8C61C02FE0D6D67,
	PlaylistMediaPlayer_Start_m9FAA02764BEDCF0517A4F00F8604191500ADB209,
	PlaylistMediaPlayer_OnVideoEvent_m993B9C42D3D6997D97CA7DA2720F9474C297C3BC,
	PlaylistMediaPlayer_PrevItem_mAB331FFAAF8C25DBFD21A80426B228F8C46D3A17,
	PlaylistMediaPlayer_NextItem_m20F76EEBBF8E14B4AA1DCB425070D6C8D1D171AD,
	PlaylistMediaPlayer_CanJumpToItem_m9543998971993E852240D1660A3749735105A900,
	PlaylistMediaPlayer_JumpToItem_mD4B299AC55241F56D56B252723D1F969536110F3,
	PlaylistMediaPlayer_OpenVideoFile_m73586FE7E3BABCCFCB64F747AEC6EDB43979988C,
	PlaylistMediaPlayer_IsTransitioning_m1AE32A0D518D6A243FDCE8D84B2B71F2921AB093,
	PlaylistMediaPlayer_SetTransition_mCBF924A30F232BB6AB5162C7156E35818EFAB9AA,
	PlaylistMediaPlayer_Update_mE39BCA59DB824B314C15EB06169DE4F5C70CEFAF,
	PlaylistMediaPlayer_GetTexture_m85B29C83F825CC1248BC4438FA2F64FFEC7A5CF2,
	PlaylistMediaPlayer_GetTextureCount_m1BF08422B9047D3B82BB2509934C98698BF2EB71,
	PlaylistMediaPlayer_GetTextureFrameCount_mE125507DCF3AFCAF443C82815D326C03D8D84432,
	PlaylistMediaPlayer_SupportsTextureFrameCount_m4C7E4D445D5C1DE3ADDE7799FEF2400D1B066F61,
	PlaylistMediaPlayer_GetTextureTimeStamp_mE10D738CDFB09D11EEBEC65554EDBBBCD9E537C4,
	PlaylistMediaPlayer_RequiresVerticalFlip_m2670C70EEBBCBBD7CCB1B2C48721BBD22C12CAA4,
	PlaylistMediaPlayer_GetYpCbCrTransform_m9B3F18D5361A8090FD820ADCECB4C8B6F341D89E,
	PlaylistMediaPlayer_GetTransitionName_m55D1F1A8F22AA0E02ED97C06B3997F716E54C2B5,
	PlaylistMediaPlayer__ctor_mCD990CF574CF2B4664FFDD29F64F4E5DBC9F76A6,
	Easing_GetFunction_m0A3A998B12B19042FEC59052821830FA5A244FB0,
	Easing_PowerEaseIn_m12E2B5F13FAC43680CDE866416B9F865AB408752,
	Easing_PowerEaseOut_mC47935AAD5B0A356D751ADEE8771F7A3E3B3426E,
	Easing_PowerEaseInOut_mB11B4206841BB35B6C7FB19AC1ADC6D2F32663E2,
	Easing_Step_m8EC854B0DA4F982CE742CD89D61FF5CD8CD3A6B5,
	Easing_Linear_m0F7845F63905AF3DEDD3E0874FF4EBD50F5F9C5B,
	Easing_InQuad_m0910CE9C6A4DEC3CC2D488716CE1DE81AE0E8D94,
	Easing_OutQuad_m4EFCE47F8757465279E674493A385FE0A85D8FA7,
	Easing_InOutQuad_m84C8A0A99A683B57A40380D847CCDD210BE6A6FB,
	Easing_InCubic_m3882BD96AEF88F265A14C50B0E0065620FB337C3,
	Easing_OutCubic_m06A435FE7E0BF4A082D4248E81B619ADEEDAF621,
	Easing_InOutCubic_mD85410418A70EB023B08AC363617CE29D3C18818,
	Easing_InQuart_mCD5C6C2A5EB808E0C2745B2A0109C3651FF7FF07,
	Easing_OutQuart_mAA2813D82B15C85F42628F01C29F4EE798811C84,
	Easing_InOutQuart_mBF80AD6F229C65D98105C6BE124E06CA0F5DACDF,
	Easing_InQuint_m96144186DB2B204C0BD4C2D3371BE0EE15D302BE,
	Easing_OutQuint_mA61B78C42D6C2A54D6AE6517F8C4E402829B02B2,
	Easing_InOutQuint_mFA82331F78A14A0147DA221645351A996CA5FFC6,
	Easing_InExpo_m26C2FEBD5602614B836585ABDE6EBA6AC941FEF1,
	Easing_OutExpo_m792D836881D4844DAFE638E40B9F49E72F152013,
	Easing_InOutExpo_m3C31A5337EC51EA2B21ED36136F0DA47AA7AD736,
	Easing__ctor_m69F5D01C5C4E7801ECC79823E327068E777E9E35,
	StreamParserEvent__ctor_m1AB02AA693ABF9BED4483AA27AF35536356E9D99,
	StreamParser_get_Events_m82D43156A8A018AD68844A707E56914E54D95DC2,
	StreamParser_LoadFile_mA4778595E7AE78BB3BEF70F20BCD20CC973D4D8D,
	StreamParser_get_Loaded_m67AD40A7AD72ACD95A4D6853E57022655CA0FF0C,
	StreamParser_get_Root_mA022BE599C672DD272842F6D1BDEFA0DB5F3FA50,
	StreamParser_get_SubStreams_m7D4A5FC7B2777866C606E46655213C8E768CDFBC,
	StreamParser_get_Chunks_mF01316B8D9CB08BC2D7A4C564B173BCAC14AEBDE,
	StreamParser_ParseStream_m921884BC1A514287242F9CE087ECCDD96B5321ED,
	StreamParser_Start_m7743BC53818D9DF8E63AEA4B8361C6946E341132,
	StreamParser__ctor_m6707C579E3361430F9B4580985240B5D7C4113D1,
	SubtitlesUGUI_Start_m81020E68C72F44C7F365119C104CA5AC6D069C83,
	SubtitlesUGUI_OnDestroy_mBB3C4F5C149E76F98FE0ADB2E9076B9CC9316BFA,
	SubtitlesUGUI_ChangeMediaPlayer_mFFF8749831CDB17D89AB552558A74B006C6DC27A,
	SubtitlesUGUI_OnMediaPlayerEvent_m45912EB24F60297AB0D940A3D4C612F0F976C6BD,
	SubtitlesUGUI__ctor_m09F63FA6D7B6DBADFEDBC9931A68C94BFFD69034,
	UpdateStereoMaterial_get_ForceEyeMode_m3313BC2D25C21E7E3FE4C0D7CB9E96BD9D467236,
	UpdateStereoMaterial_set_ForceEyeMode_m6A3722F8E86AD1584278ACAF0B56F97BD028696B,
	UpdateStereoMaterial_Awake_m5C7BC1FC7C350AD68BEA4F48E1F856F9555B5A4D,
	UpdateStereoMaterial_SetupMaterial_mE9ABE9B3D62B30F7B85E70D2063D9B7A09D9EA6C,
	UpdateStereoMaterial_LateUpdate_mE13E0CA32652B0833042CA16EA17CD26B355C4A6,
	UpdateStereoMaterial__ctor_m3B5DAF7BD246113BB67579975E2F0FCB257EB5E9,
	AudioOutputManager_get_Instance_m21A0B52D43DD53D989089023AAC34C307580BE62,
	AudioOutputManager__ctor_m12F37DB8F5B1DF79399DFDD975EBC86A0A31FBBE,
	AudioOutputManager_RequestAudio_m95F63CEA8221E9EA65686941AEE7809DDE860EE2,
	AudioOutputManager_GrabAudio_m546B648A8930F6AC77D910ABE65764BB49E783A4,
	AudioOutputManager__cctor_m820C810B05A2150FF714519A8710A157037CF6B2,
	NULL,
	NULL,
	BaseMediaPlayer_OpenVideoFromBuffer_mC593EB17CC16F086B7529871FBF82FD5C3DDF01E,
	BaseMediaPlayer_StartOpenVideoFromBuffer_m1CE803B3BA190E946AC5B6521DF01599889E0495,
	BaseMediaPlayer_AddChunkToVideoBuffer_m70424D78C585DF8AB4424CAFB3C991F7EC5785B3,
	BaseMediaPlayer_EndOpenVideoFromBuffer_m6E9468D3BEBC1563C9D1854974E95E98ABF50AFF,
	BaseMediaPlayer_CloseVideo_m8A46A0EFF2937C84C37B84D2CE96B83286BAB255,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_Rewind_m21BD0FC7F5E14F57AB8E1ADA8B14203CBD4301F2,
	NULL,
	NULL,
	BaseMediaPlayer_SeekWithTolerance_m5187D00A5EFCF2C04432484B54D3009F02A98245,
	NULL,
	BaseMediaPlayer_GetCurrentDateTimeSecondsSince1970_mB71250974B99A7087F39A1A0B5B0903C3E37225E,
	BaseMediaPlayer_GetSeekableTimeRanges_m1458191D20E86DCD6BABAA2C3E038695E99857F7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetCropRect_m9D43A4E5CC9F5E93EA543095D6D2CF37E10F18E1,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_WaitForNextFrame_mAF4E01AF17AA338A80826E916CBCD72A3012BBBD,
	BaseMediaPlayer_SetPlayWithoutBuffering_m84657EABD2A7F681E41DA921053EF9F7E5A32FE0,
	BaseMediaPlayer_SetKeyServerURL_m68329F6EB28C2D3D4DDF9FF6C6BE2DD77888085E,
	BaseMediaPlayer_SetKeyServerAuthToken_m7B67176FF00D0AEF2E75D08FF48912CAE153B200,
	BaseMediaPlayer_SetDecryptionKeyBase64_m2965B2C1792CC50902D0FA8C1258F98059343CE3,
	BaseMediaPlayer_SetDecryptionKey_mD824E456CB891A3791026B94E040725A77501306,
	BaseMediaPlayer_GetTextureCount_m3163275E7B317107EAC6D953E7C1D0382CEF378A,
	NULL,
	NULL,
	BaseMediaPlayer_SupportsTextureFrameCount_m622928C95F61D05684B3F6B9925ECC930C9F2E8A,
	BaseMediaPlayer_GetTextureTimeStamp_m676503683CF7AA0077A0276837801B3FC91BFFC4,
	NULL,
	BaseMediaPlayer_GetTextureTransform_m4A6ED13D46A85CA3D313E8FAC84B44C52097ADE7,
	BaseMediaPlayer_GetYpCbCrTransform_mA98D5704137DCC314ABFC4D18B4E7A1AB295220B,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_SetBalance_m4ACBBCC574A73F920ACD4D79BCB579EF416F0508,
	NULL,
	BaseMediaPlayer_GetBalance_m68AD9ACF463F71B6F5D6D3A3A70C65B2ECC5F9B3,
	NULL,
	BaseMediaPlayer_GetAudioTrackId_m91237F46ED4BDB69C8EA0263687C609ED5434640,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetNumAudioChannels_m038B5490B28622997B55DC335762285E9AB04E9B,
	BaseMediaPlayer_SetAudioHeadRotation_mEEEF49611403C804F60732FF49F6486DF369AABD,
	BaseMediaPlayer_ResetAudioHeadRotation_m6FD57161928DD898649A27ED05D18AE6113F8785,
	BaseMediaPlayer_SetAudioChannelMode_m8CDC213D8EE7FED6CC9FF39FF37A7800E5334DB9,
	BaseMediaPlayer_SetAudioFocusEnabled_mDD8B1A0E83E729EFE4206D45ACB66FA4D8F388C9,
	BaseMediaPlayer_SetAudioFocusProperties_m238FB862DFFB14FD1780476AD6AAF2CF6E025B63,
	BaseMediaPlayer_SetAudioFocusRotation_m3CACBE6DA9DBE53A90B29C28709C616BB7C916E5,
	BaseMediaPlayer_ResetAudioFocus_mDF5A065D0C38363C05F59245E0A165F77FE4ABB3,
	NULL,
	BaseMediaPlayer_GetVideoTrackId_mCA57D735551EBEF02F6A603EFEFB02B3FD4CE77A,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetEstimatedTotalBandwidthUsed_m37FE0EB8CEA51EFBB767DCD8283B2BFDDD5C9EB7,
	NULL,
	NULL,
	NULL,
	NULL,
	BaseMediaPlayer_GetLastError_m33EC86CDFC8FA0AE2F0337610C3BF2BCE03F7DD7,
	BaseMediaPlayer_GetLastExtendedErrorCode_m383E605472A35AE521086941B23708F70F0589FF,
	BaseMediaPlayer_GetPlayerDescription_mD051AF98C918B186134053C6152BE8AD0CBA46FE,
	BaseMediaPlayer_PlayerSupportsLinearColorSpace_m6A1E690E4994B174A8F25568F540DA5E81C38331,
	BaseMediaPlayer_GetBufferedTimeRangeCount_m2E26A5993EC0CC528461EFBBCD171276FFEC4651,
	BaseMediaPlayer_GetBufferedTimeRange_mEB2F52809E4685560C07155F0A1F182F5E6982F3,
	BaseMediaPlayer_SetTextureProperties_mB51EE0AAE3C6A751C7C27E5906159AA2775DC569,
	BaseMediaPlayer_ApplyTextureProperties_mA7690675AFD1C19C4364F1B4AF30AAB6FB26805D,
	BaseMediaPlayer_GrabAudio_mDCC211A1CC2DF1581D299B889339B49264E9E2BA,
	BaseMediaPlayer_IsExpectingNewVideoFrame_mF272D2074F469EAEB4356322D6DB1DFAB8D7700D,
	BaseMediaPlayer_IsPlaybackStalled_mA4E8DBDFE8CF1448634321782034DEA4FF0E3DEC,
	BaseMediaPlayer_LoadSubtitlesSRT_mC662A14864D9F44643C4A5A37D53B73901C1859F,
	BaseMediaPlayer_UpdateSubtitles_m64F8C65ADC9E604171C087D90CC928B01CD37E5B,
	BaseMediaPlayer_GetSubtitleIndex_mE96BE5A9DFF09753063EEDC44C09A0E2E2720C1B,
	BaseMediaPlayer_GetSubtitleText_m221600D40D605ACCED95B6857864ACBEB132DA2C,
	BaseMediaPlayer_OnEnable_mC94F1DF3F741AFFC55F3C4A8C5E3B7D9AEF82AE8,
	BaseMediaPlayer__ctor_mDC46CC0B61C09BE8D457D48244A2A3E5E0E3B6A9,
	HLSStream_get_Width_mE2C9215D2017DB5F1DD95BDE257208CC7A06BBBB,
	HLSStream_get_Height_mED68CB6841284E54938AA5CD620F3579AB63CB3B,
	HLSStream_get_Bandwidth_m7108E1C0D4C8B56BB87551E543E8CEBEF40FED70,
	HLSStream_get_URL_mA87D6580CFCE96996C51F2B8B002453428D6489B,
	HLSStream_GetAllChunks_m07F1CF2A1708C1CF63581D6CBBB52D14CFAF0AD1,
	HLSStream_GetChunks_mE5B2DBE348285D9559FD44FA73A4F73F7FE56F4F,
	HLSStream_GetAllStreams_m75B7666CD2212065D0E2ED2B30FD8511567D41F5,
	HLSStream_GetStreams_m5CA80839B74ABCCCFD1CB7BD3B46829064289A4C,
	HLSStream_ExtractStreamInfo_m84998280D507E38FDB70FEB34A7C231E89B81734,
	HLSStream_IsChunk_mF58D1A3CFD8BF16B35267A4C81792845D8D15C51,
	HLSStream_ParseFile_m6E91648162A2F952FBF64FF8146D6E4BF7934345,
	HLSStream__ctor_m5DEE09F9AD646EB9BA3056970141EFBC18FD57D7,
	MediaPlayerEvent_HasListeners_m1D4A700D7A2F1A3DAAA1A9D2AF5FB8A2258D0944,
	MediaPlayerEvent_AddListener_m53C4058E9896017113BC52444B1E2A2694339396,
	MediaPlayerEvent_RemoveListener_m98B2B41347E72C42DDA1FAF4BC9F2F8F8A3AC4D4,
	MediaPlayerEvent_RemoveAllListeners_mC26BC9FFB1C15B8EECF122696EC8C5BB45A34FED,
	MediaPlayerEvent__ctor_m00B69F6545EB57638E5D342D6146F3C9ADCCABC4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Subtitle_IsBefore_m3E4D52E009AEA07E6587FC4B18C4293CC35F6F00,
	Subtitle_IsTime_m65474521C2FB8EE455BAEDFF1D385E965C252381,
	Subtitle__ctor_m0E660134DF475780C8E2DD7EC7B9D9FEC0E0B156,
	Helper_GetName_m54E21394A5707C2DF0FC55BFCBA45D967AA7267D,
	Helper_GetErrorMessage_m9079D5C4500C27D45D27BB21B13F8BDFD5BF0AAD,
	Helper_GetPlatformNames_m32907ACC4F350516C837341125B1BF45A1474973,
	Helper_LogInfo_m57E62F23D3724009DCFA601668DE51D00FA68D0B,
	Helper_GetTimeString_mAB04F8B7709C7383F8B83EBB7EEDAC74A8571975,
	Helper_GetOrientation_m7F57989C7488CC06CFE72165F96A80958B56C3E7,
	Helper_GetMatrixForOrientation_m992E825423E87F735A42C75B1105DC06F3A541DA,
	Helper_SetupStereoEyeModeMaterial_m73164807B8944B82455B1170DE7C49ABECEDECB1,
	Helper_SetupLayoutMaterial_mA9E72958D7B85BE9550BE01546B7D4E207F2B8EF,
	Helper_SetupStereoMaterial_m7D47161AD4D7DE7D58593F18E064F5742235F4E1,
	Helper_SetupAlphaPackedMaterial_m2A7D2B8E3A4FBDB682E39F139AF2423E79FE5260,
	Helper_SetupGammaMaterial_m016674C657C3EB56580F0EE8D37411FB99546012,
	Helper_ConvertTimeSecondsToFrame_m888568E8ADB79C4FEF9642A8DE6B07B0B64EB8B5,
	Helper_ConvertFrameToTimeSeconds_m56B24BD3E7BC25DBF0B4B032017D0E7D9BD1C0D9,
	Helper_FindNextKeyFrameTimeSeconds_m8CACA1571C74BE5757F29776BF796D356DD0845A,
	Helper_ConvertSecondsSince1970ToDateTime_mEBAA30D44CAA0CFA31386B3C1AB5CCF71C913733,
	Helper_DrawTexture_m253452BF0111F499B7CC4614B81B84B67A51CFED,
	Helper_GetReadableTexture_m02D1A423BD93C3502237CF8626BD93498DD16863,
	Helper_ParseTimeToMs_m74FA06613620F1693563E873BA58E71A695E3F9A,
	Helper_LoadSubtitlesSRT_m6B5236684F29839B4000110A1903BA1B633C68DA,
	NullMediaPlayer_GetVersion_m2D32E17AB7A45268AA1974BBA65F9E13CCB39F10,
	NullMediaPlayer_OpenVideoFromFile_m67087280552E4185DFD4140156C655BF339D963F,
	NullMediaPlayer_CloseVideo_m846FD21829C18686C24A8C35D74BFC5433D8697A,
	NullMediaPlayer_SetLooping_m36E203C918FCDDF95AE29AA557962DD0417AF675,
	NullMediaPlayer_IsLooping_mBD79AE53183451C1CBFEEC3251923557A93F6937,
	NullMediaPlayer_HasMetaData_m1C27C69454261BBF3AC9EC3442BF560F899A7083,
	NullMediaPlayer_CanPlay_m65C35A4AFFB0ED793C89350779AD1D11EB1ACE69,
	NullMediaPlayer_HasAudio_mACF01EA2350A8249A4FE958C04070126A7DA488C,
	NullMediaPlayer_HasVideo_m2C8A3CC70091CF1696EE546F8C9851DE07201359,
	NullMediaPlayer_Play_m74B6D3E0E0C629FAFAB8B5147561E6C14CD8E35B,
	NullMediaPlayer_Pause_m1B385993EEBBB7A1E8CD982C800F94079EC5AEB5,
	NullMediaPlayer_Stop_m2E4B3735F3600DC5A82442A9CFBAE0C9E044F9BC,
	NullMediaPlayer_IsSeeking_m6DBFD49480CC27B51BABB888CE8D7BAA57194124,
	NullMediaPlayer_IsPlaying_mFF44BA9F250BFB1B5E714622AB6D29F63AF1FF44,
	NullMediaPlayer_IsPaused_m8C26E9F65C043F1652652AA22549FFC070C2B6CD,
	NullMediaPlayer_IsFinished_mD237A2EAE75DB33F7F5AEB08A5C7A646744ABFA4,
	NullMediaPlayer_IsBuffering_mEE9F0E15C4A44051AA94C88EFECAE7676CB0C91A,
	NullMediaPlayer_GetDurationMs_m1BC8329F2CDE98204A091F6D871D9DF34B3217BE,
	NullMediaPlayer_GetVideoWidth_mCDC1F8707152D82D9E724C116656155D5E7B98E5,
	NullMediaPlayer_GetVideoHeight_m92DABC37D41F3CE0428377F7AB9E038992BAB158,
	NullMediaPlayer_GetVideoDisplayRate_m71FBC5D4B0D522F1C39CB72C21B46F88D5610C71,
	NullMediaPlayer_GetTexture_mC2B52AE60436B35EFFEFBCA282FDE327474CA63F,
	NullMediaPlayer_GetTextureFrameCount_mAB0CBD3AF0CC0B5F9147411ED978F8F07B7957C8,
	NullMediaPlayer_RequiresVerticalFlip_m8B33293C6218A315D5C14BD41B2B6739957B4530,
	NullMediaPlayer_Seek_mC26A9B0C6B0FC1C05F5525AF07A7DA29D29E80F1,
	NullMediaPlayer_SeekFast_m13A555AEBCC9DF364CDAF57236F7207A8697A6B5,
	NullMediaPlayer_SeekWithTolerance_m0F177406CCBCF9DD02D5A0C28E634BE0D17A9A0D,
	NullMediaPlayer_GetCurrentTimeMs_m641EC97DA9B5F1432A4D0F8E836503504F8F763A,
	NullMediaPlayer_SetPlaybackRate_mF038B21BFE78769936AB17A64911D759A4A5AB4F,
	NullMediaPlayer_GetPlaybackRate_m53E31C07A74BC07939A6D96A591EB8F93FC5202C,
	NullMediaPlayer_GetBufferingProgress_m5A86C137252AA1D41BA7E57A7AAACE7C30E024B3,
	NullMediaPlayer_MuteAudio_m431F51C2C0522CCF72DB05B34246508901126F84,
	NullMediaPlayer_IsMuted_m82BC4784E52D6E1B5C69215BC634DDCC5070C284,
	NullMediaPlayer_SetVolume_m89FDF2ECBE33CECD9A2CED3199B3C9749A1A1B50,
	NullMediaPlayer_GetVolume_m7EC9F8DEC28713E5069B0885B62D8914560E3DD6,
	NullMediaPlayer_GetAudioTrackCount_mE8C6B231BC0B80CFEE551DBF5194E9732E6F0493,
	NullMediaPlayer_GetCurrentAudioTrack_mEA7C55A0E9352294F9C825AAC3AF1687C3E25895,
	NullMediaPlayer_SetAudioTrack_m30A10AE72350C73F6D7F3F514F553C15FF038B42,
	NullMediaPlayer_GetVideoTrackCount_mB78764E0160350C6CC9BCA8DB470A165267F8AB3,
	NullMediaPlayer_GetCurrentVideoTrack_m1CC33FD3327545698812CA4339045F9981683C6B,
	NullMediaPlayer_GetCurrentAudioTrackId_m443816214E2FEF3F60F18A39CF4452285ABA86D2,
	NullMediaPlayer_GetCurrentAudioTrackBitrate_m1F98E7EDDD4E3EF12C90F317ADA6E27F84F74DB7,
	NullMediaPlayer_SetVideoTrack_mFB9EA9FA2A16FA0F0900B840F81A5B1D99C4B6D4,
	NullMediaPlayer_GetCurrentVideoTrackId_mE10D7C81EB97A5FD98BF0E5C9F7B06029667A17B,
	NullMediaPlayer_GetCurrentVideoTrackBitrate_m36CF29E84149E68E31DF4BEB50D498D37655EC16,
	NullMediaPlayer_GetVideoFrameRate_mF407AA4BA66A4D51ED055DA40D42A996640117D5,
	NullMediaPlayer_Update_m2B8B5596D048D9103E25C191EDC3ADABFF5B9DE9,
	NullMediaPlayer_Render_m0DD70CF819E83B58056DA0DE1F1B599F3D7D8AB4,
	NullMediaPlayer_Dispose_m4132B60190D3EE92534A32C33C9CF9C5D8A853D7,
	NullMediaPlayer__ctor_m5156CF446D51B65FE47A6C142AC99012B48CDBAC,
	Resampler_get_DroppedFrames_m1EA61AA75A5960424D62E22817ADB7D3C8BAE65A,
	Resampler_get_FrameDisplayedTimer_mF250FCC5E3D962A0D9F1AB4710BA17964DB2A92B,
	Resampler_get_BaseTimestamp_m92514A0B60D0AC24A6C407DBBC016B24A6294B66,
	Resampler_set_BaseTimestamp_m0A8A028FB04D4F4CFFEC6675CBE8F3C8C30604DE,
	Resampler_get_ElapsedTimeSinceBase_m77620EEA1388DDD968E5E007A56A50FA10718703,
	Resampler_set_ElapsedTimeSinceBase_m10CF5EED299EB36A9981466386B21427BDB214EE,
	Resampler_get_LastT_m88ACC219257C611C4CAB2628E631CF6CE1ED32A2,
	Resampler_set_LastT_m296C0C09FE43EC4CF459AA1DDF64CBB162ED819E,
	Resampler_get_TextureTimeStamp_m049CD8BDBE6CAB268D77DE4D003BEC4C3B231E5F,
	Resampler_set_TextureTimeStamp_m863FE886F11ACDBF61DFA61012DAC60FFE87D36B,
	Resampler_OnVideoEvent_m2BD63DF30614F4A5D8778DACCB73A433F82B9422,
	Resampler__ctor_mDA18B56AB34BDF3CE562998EE3B5CED682AA319A,
	Resampler_get_OutputTexture_m24C17095AB69CE9F42A4B4D314386148E7E7A98C,
	Resampler_Reset_mC08E20EE1F0451C968BD38F8FB1EDFAFDCCE940E,
	Resampler_Release_m067A8411A8E4FD62802D330AF224280EC17841ED,
	Resampler_ReleaseRenderTextures_mEDC445A66E2FB1E841AA031C84A1918CB3C5025D,
	Resampler_ConstructRenderTextures_mC94FAB6C3D09B12F036097E97A79E3D0C947EA1B,
	Resampler_CheckRenderTexturesValid_mE0A71AF053E365111AB3F289C8F2222CE22AE518,
	Resampler_FindBeforeFrameIndex_m2FDDAF3496D08C89F6A90A14852215366F803743,
	Resampler_FindClosestFrame_m8EEFF4A2103B8E3883B99F4D91CA136280BE424A,
	Resampler_PointUpdate_mCC19CB6726FEE54E971DA7DA581EA9510DB972CA,
	Resampler_SampleFrame_m60A059DB99F5453D6524B0A612AC0A650144330F,
	Resampler_SampleFrames_m53EA62F83E449A508DB494BC2893F1DEE6E6018D,
	Resampler_LinearUpdate_mCC402F1A6DCD4189C17B6E08F5F36BDDF176E203,
	Resampler_InvalidateBuffer_mC0AD36F9A5A1DA59D1E38E613E931C1AD91919FE,
	Resampler_GuessFrameRate_m333B2ACDD699112E8EF28D7B34BDB9A85E608E6F,
	Resampler_Update_m3FD7D39929FC49F6C369AAB23A5F85DC599A0E3A,
	Resampler_UpdateTimestamp_mE5FB3749CC51BC6DC2D71FEF1E91CB3ACF8A6723,
	TimestampedRenderTexture__ctor_mAB2C4E690144CFAC36F152869754C08B760F124C,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Stream__ctor_m192C99ECFCD67CFA41561C424B6083F2E867CC8F,
	WindowsMediaPlayer_InitialisePlatform_mC8F11FD9D63586D0C71D9BE64834F1F1EC1CF08B,
	WindowsMediaPlayer_DeinitPlatform_m422A96A7A372606A0554732C2F8A3C24F80AC931,
	WindowsMediaPlayer_GetNumAudioChannels_m074C778FD6FAD6B5CDE9471F1CDB33A20D002950,
	WindowsMediaPlayer__ctor_mFBA50B0E2A91CF22613E6897F41966082DB1C6F1,
	WindowsMediaPlayer_SetOptions_mF7504AB05C9162614BA717B32DC74D24E52EF397,
	WindowsMediaPlayer_GetVersion_m4C2B98D71273A0013E33C15FD6BE278870055719,
	WindowsMediaPlayer_GetUnityAudioSampleRate_mD531A7B1CFA7F079658D787D82F1E15E579F61EA,
	WindowsMediaPlayer_OpenVideoFromFile_m16661F05D368EF0A4733671A896183D9566E6309,
	WindowsMediaPlayer_OpenVideoFromBuffer_m52CB6EAFA4B0F864C693BB79DBAEAF00087FD787,
	WindowsMediaPlayer_StartOpenVideoFromBuffer_m1A840B176BB6B8220885574F9BC7308C93062487,
	WindowsMediaPlayer_AddChunkToVideoBuffer_mB785ECD47C483B00A2B9295E003F1A7C0E55DA4F,
	WindowsMediaPlayer_EndOpenVideoFromBuffer_m9BD32D8F6911FAAA85EAB743028AED37C50F40DA,
	WindowsMediaPlayer_DisplayLoadFailureSuggestion_mDD75B22AEE57F13AEFE049CC4285F5F33BDA0924,
	WindowsMediaPlayer_CloseVideo_mD554984E4F2B265B19C5AB1898629A2A48250A2E,
	WindowsMediaPlayer_SetLooping_m2F45E2B04CD6AC1F4D8FB1AC60A5D69616C5E157,
	WindowsMediaPlayer_IsLooping_m728FDC065397844C71E72C14703FA6D23FE4771A,
	WindowsMediaPlayer_HasMetaData_mD9A0AE09DFAD095F0D2A20FBDCAD594762D1CD33,
	WindowsMediaPlayer_HasAudio_mD8432A24FF4EB1CFAD784A0540BA26E9A9EF1736,
	WindowsMediaPlayer_HasVideo_m490A52885384652CD87B3CB6062EC713A105130F,
	WindowsMediaPlayer_CanPlay_m3AB7538B29B0F721E1585B6AC9F8C2D9BD1396E4,
	WindowsMediaPlayer_Play_mCCD4294233646D82AF57EB7AB1CE348A9105F674,
	WindowsMediaPlayer_Pause_mC78022EF51C6031713E5FB8DF520DC010F8E9695,
	WindowsMediaPlayer_Stop_m329B1A81E67BEFD41E065E17591EAF6951D8FDCA,
	WindowsMediaPlayer_IsSeeking_mFAF4B7D7C4A93ECCBD4E951F5B4F1D8CF836AB07,
	WindowsMediaPlayer_IsPlaying_m68A26024CDB1E96619DE5A3DFE378E061BE76627,
	WindowsMediaPlayer_IsPaused_mA0A59103210FCCAEEFE7D5C0D7D2DB932A70B4AC,
	WindowsMediaPlayer_IsFinished_mC3F894EF2A23860083042B955A214B7DEAF389C4,
	WindowsMediaPlayer_IsBuffering_mB2D3ED652E44CCCE11F6383AFDD18C915DA505FD,
	WindowsMediaPlayer_GetDurationMs_m6AFDDB4CA12AD75B967C9D2A734D6AC4AF4E5005,
	WindowsMediaPlayer_GetVideoWidth_mD4B0C07567210E53AD44DD00511848CAEBAA2019,
	WindowsMediaPlayer_GetVideoHeight_m724A6288AE48C4696D92453B0806F187DA217CE9,
	WindowsMediaPlayer_GetVideoFrameRate_m137EE5158577B423A63F3DCA405076F8511FC97C,
	WindowsMediaPlayer_GetVideoDisplayRate_mB39613DBB30B735EE6963B26C61760618FD6E5E5,
	WindowsMediaPlayer_GetTexture_mB125E8C6EB2AAA9AAE39BFC3451DAA8AF199079B,
	WindowsMediaPlayer_GetTextureFrameCount_m728C62CD9B326474B37A290788FEAACE4AD2F845,
	WindowsMediaPlayer_GetTextureTimeStamp_m1709D34DEE3288548A27BBD93CCCDDB5AAA22A14,
	WindowsMediaPlayer_RequiresVerticalFlip_mF05B4395EACAC6302B6C47616B4A6269AB319275,
	WindowsMediaPlayer_Seek_m75DF7B1B499B810500907A1FB521D883F8F9148D,
	WindowsMediaPlayer_SeekFast_mB6596C76C4753843618BA47BA276910714F58AF1,
	WindowsMediaPlayer_GetCurrentTimeMs_m188FC3DC22A32FF9DE490C75AFA4D3EA34859372,
	WindowsMediaPlayer_SetPlaybackRate_mC9BDB0FDEF65E5D5E4F1FEC0E658A33B68273179,
	WindowsMediaPlayer_GetPlaybackRate_m2DA85BE5CA54064AD87DA99DB131EB49618455D0,
	WindowsMediaPlayer_GetBufferingProgress_m7791D40CBB6F9B46FDDE05AEE23BCB70A315B438,
	WindowsMediaPlayer_GetBufferedTimeRangeCount_m20572EB2AB5A29BB79DBE69C8A89517811E0F1A7,
	WindowsMediaPlayer_GetBufferedTimeRange_m122E6D1B572224B7AD218B5955A87BBFE5D8AFEE,
	WindowsMediaPlayer_MuteAudio_m4EEE181F4434D7770EA37C6659FAB8CFECAB9758,
	WindowsMediaPlayer_IsMuted_mBA7B7FDFFEF234E26018B68C3A54A49E94A944AF,
	WindowsMediaPlayer_SetVolume_m50E95790E16E4B7F9C7B7A433413C40285F736F4,
	WindowsMediaPlayer_GetVolume_mDDEB67681A3AF349846B0F33EB5BBE7F2A80151C,
	WindowsMediaPlayer_SetBalance_mBDE9D93E722719A62F6D59E74F3F988ED967BB68,
	WindowsMediaPlayer_GetBalance_mD9D1091B114DA2E19C590C2E0713E5CD96607862,
	WindowsMediaPlayer_GetAudioTrackCount_mFAC00348F6DA1A8A0BFF7342FCE984295D808473,
	WindowsMediaPlayer_GetCurrentAudioTrack_m1C8C30D36A13314A815954F8026557F40114B14C,
	WindowsMediaPlayer_SetAudioTrack_m9E86040F50D0FED92B5596B3E39E4666EA254FA7,
	WindowsMediaPlayer_GetVideoTrackCount_m1CC21AD3EFDE2C167DDA54F58FD8B5B962BF66C4,
	WindowsMediaPlayer_IsPlaybackStalled_mAFFF05F6C7B99DE483BC8555E01C7B3C0D9748B1,
	WindowsMediaPlayer_GetCurrentAudioTrackId_m25EF23A781452914E8F24B0635295B675BC64101,
	WindowsMediaPlayer_GetCurrentAudioTrackBitrate_m34C9F3CFEA43062C7519B73934E68DEBBA2B56D3,
	WindowsMediaPlayer_GetCurrentVideoTrack_mAE75179CD2F8872279AB65F3EF1F749BD19A842E,
	WindowsMediaPlayer_SetVideoTrack_m4504BE8A35CED9E058F927995F7FF202A76E1CAB,
	WindowsMediaPlayer_GetCurrentVideoTrackId_m9757138D5D0FDBB86A5410884E5F41135361EF14,
	WindowsMediaPlayer_GetCurrentVideoTrackBitrate_m6146F86A9B512D5F72C8654CCF93C9C7CD091A7A,
	WindowsMediaPlayer_WaitForNextFrame_m0B0D9C87C617D177362EA025366160FCE0763D6F,
	WindowsMediaPlayer_SetAudioChannelMode_m043A3DD8752F26255135244AE1AABA605088FEFD,
	WindowsMediaPlayer_SetAudioHeadRotation_m540FD70322BF43A9850EFBE4419FCBDC01FFB169,
	WindowsMediaPlayer_ResetAudioHeadRotation_mB7756FA165304A4E630CD0661EB22E5570DDFC4B,
	WindowsMediaPlayer_SetAudioFocusEnabled_m2D08E6619E889F7606AE25F4A16EDE1E033C0032,
	WindowsMediaPlayer_SetAudioFocusProperties_m63D1AD9C9F2BA72DFB900544EF4473FF580358EC,
	WindowsMediaPlayer_SetAudioFocusRotation_m0A9BE35195689BC399F04F5303C67E804874359D,
	WindowsMediaPlayer_ResetAudioFocus_m8449FEA1D7C2A7BB9205295B8BD458824E056DF5,
	WindowsMediaPlayer_Update_m09F3322289D90A5210ED823A6D1446FDDC7CA39D,
	WindowsMediaPlayer_GetLastExtendedErrorCode_m04F70B981A089B53445C44E7595AE03594ABA324,
	WindowsMediaPlayer_OnTextureSizeChanged_m950034589665B3FB95CBE8D565CBA9DC58E33DA9,
	WindowsMediaPlayer_UpdateDisplayFrameRate_m261D9C480A2C5DEF7CA2A75175A4FFF7D6F5161B,
	WindowsMediaPlayer_Render_m857316997DA845091A38F1B01088FF3FBC8D2A13,
	WindowsMediaPlayer_Dispose_mA38523F1E4DA8CECFDF5CC3184484890BCD04E5E,
	WindowsMediaPlayer_GrabAudio_mF61EC2A7CCC13F1C2F9EE24B62395277AFDB852C,
	WindowsMediaPlayer_PlayerSupportsLinearColorSpace_mC4909274AF13EBDB35DAA9BB894E48156C86CF6D,
	WindowsMediaPlayer_IssueRenderThreadEvent_m447F941BC3F232790F80486726A45FCC181B0011,
	WindowsMediaPlayer_GetPluginVersion_mB276437574B2589C4CCAA71767C66669F3823FEB,
	WindowsMediaPlayer_OnEnable_m90B611243ACF5B2AA01B7BF3DF6D5E96B2CCC33B,
	WindowsMediaPlayer__cctor_m6C92004CF153FCEAF9E4BF5E6E37ADE6AB0F29E3,
	Native_Init_mEFDC53C397F857EA48EE85CA0494913DAE5C5398,
	Native_Deinit_m4645663FBE532AD2F386431D7B6F128EDB8487E2,
	Native_GetPluginVersion_m9934A0E5D2A6AA31087796DDA932BED2B40E1B54,
	Native_IsTrialVersion_m2A6B98D189B056A340E8B586595E4C409A06028C,
	Native_OpenSource_mFBA8BDB42931D37908E0E23BDFF5B42E1F4D46EA,
	Native_OpenSourceFromBuffer_m63A74790CDC915D1900051D6324E2ADBB7166ACA,
	Native_StartOpenSourceFromBuffer_m7DEA9FBB2D9BE4ABA81583A1CF92A1F6DDADBED3,
	Native_AddChunkToSourceBuffer_mA784D8624D9D3C4BDE2FFD6CD1F3CC3B9D2E5A17,
	Native_EndOpenSourceFromBuffer_m1E42065107A571DD4CA840F769B9407F3EFE89BB,
	Native_CloseSource_mE43750892A6C9A7B0CF6ED8F71B95CA40D2987F6,
	Native_GetPlayerDescription_m50F871B0351FC5B3BE736C748BD34A6CBBBE11AA,
	Native_GetLastErrorCode_mEB13093E4C93B030FF994A2EC5A3D3D3C070C153,
	Native_GetLastExtendedErrorCode_m92B73B3C5E15BDA54E5B00C899C695C728E3615C,
	Native_Play_mD54A3E8A3FAB3B7000FA81E1F4FB7F86D0096136,
	Native_Pause_m2E6B05F29875443B08F438B58841F4E010B728AA,
	Native_SetMuted_mE7145B961814705AFCF6521F21F539E80E16EBD5,
	Native_SetVolume_m9E6EC2FD57F29EE4D9592D050D64A79023C417FD,
	Native_SetBalance_m89D1C8CFA549C04EED2D913D88C933E0521EC62C,
	Native_SetLooping_m1759CEC8C2A9B5A096425B5733FDB23E41C13F17,
	Native_HasVideo_m865695FC5725BA9FC556F5A68080B1C771D3ABD7,
	Native_HasAudio_m7C8FDCDFF57A0C77EFCA2AFC7AA680C23B4EA8D0,
	Native_GetWidth_m62F11A0406FAF53B13278FE9E6037B3A5968F9E1,
	Native_GetHeight_mD55DCF3FED165E0F69B23A9BAA6239AB1E58B6C0,
	Native_GetFrameRate_m90D8AD7B08E9B93A7E6C85E4CB6D1C413A39C634,
	Native_GetDuration_m3A2D5862D1BF5729B2B8F58B8E54D614D7ED40B4,
	Native_GetAudioTrackCount_mA5425541D8B10BD98DF69AA04BCB074E8744D18B,
	Native_IsPlaybackStalled_m2D9166602079F96760CA915F6FBA157FC7EA0813,
	Native_HasMetaData_mE551B7DE502F68ED7E9CAE9B466B78911B825896,
	Native_CanPlay_mB4B1DD0A8AC445E00A812B23AA6D3CF2B7EAD54D,
	Native_IsSeeking_mF380D0DF862186DE120B10611F84CF0C1FEF1FDF,
	Native_IsFinished_mFE6549458880C0C20DE4AB2D937F7C6005DD0F6C,
	Native_IsBuffering_mB3D1E0D381EE8CC9E52B81C5E896FBA68C4882DB,
	Native_GetCurrentTime_m31FED24A64052BA271CC207A87B8588AD5F59471,
	Native_SetCurrentTime_m96F014C2D4EF01F3ABC7CE44C7F918CFAA665EF1,
	Native_GetPlaybackRate_m509CE798F044374ABD67F14CC99B362F419A2C3F,
	Native_SetPlaybackRate_m5F53752EB3BEEEA7AA0A72D3F14E5E78FC79F8F2,
	Native_GetAudioTrack_m4E23F0B8D5EC4E47DD07E71E9EFB7E5137CBC9C8,
	Native_SetAudioTrack_m4899455AC470FDF458BB3434D5BA9BF5B93899A1,
	Native_GetBufferingProgress_mA556B0C62BBA90A53C385139E4C799A520638B38,
	Native_GetBufferedRanges_mBED227E3BD4B45312FEEB0A077AB67F7CB180D74,
	Native_StartExtractFrame_m20096E16247BD922F1B53AF7EFB5E27592961671,
	Native_WaitForExtract_m983E15B9D4AB2873612928E96C4CD5450F35A418,
	Native_Update_mB3BDE28B0CF7801AD36F12C4C804D8802A32768B,
	Native_GetTexturePointer_mED79CAD212D4574BE6F442385503793D025843BA,
	Native_IsTextureTopDown_m6899F2991B8296B3BCD6EE68B4BE93FF486662B8,
	Native_GetTextureFrameCount_m4A0D63B8C128CD73F81C8B7FAD278F76B44E4138,
	Native_GetTextureTimeStamp_m1C38CE2C17C2217E55E5F96E58F45EEEBAA78AF5,
	Native_GetRenderEventFunc_UpdateAllTextures_m99E965F0728E9C0463EBCDF8EA22AA6A7DDECD37,
	Native_GetRenderEventFunc_FreeTextures_m30253EE2D4C49B76934CF815650362927FFAA3CC,
	Native_GetRenderEventFunc_WaitForNewFrame_m76D29D879A1E9BABACDF3B11C82E8047B719545D,
	Native_SetUnityAudioEnabled_mEA3194492AEDBB50294E44D322BF44BF324B836A,
	Native_GrabAudio_m4DCB861977FD15A3FE7188C1A79D6A7F6200528D,
	Native_GetAudioChannelCount_m439E2F217A57C0086943F74FA521208611564FDB,
	Native_SetAudioChannelMode_m5EF87D92720CAD7A68C5BBA094CC392E8E21AED1,
	Native_SetHeadOrientation_mC3642AA72A46F8AB6974D3BC82F5F7142BD263B4,
	Native_SetAudioFocusEnabled_mA548C26440D8DD944F81C1E2AB419DE1006C4816,
	Native_SetAudioFocusProps_mDA2CF3A2E9E993A4BE942E525273FF0034236502,
	Native_SetAudioFocusRotation_m5E82F5B39147FB54390E1D3A88EA3CC845BED365,
	AutoRotate_Awake_m8A6F38CC70A0EEACF4409FDB238B9D22CC125827,
	AutoRotate_Update_mE784F0DCB233C9733140BE71A3F6670C764E124B,
	AutoRotate_Randomise_mE9E11DD6C9EE55568AB1FCD05D9EF0A2E84251C3,
	AutoRotate__ctor_mC6CD26AD20439865423845C1B509296FDD764626,
	DemoInfo__ctor_m9309499C389D89BD0CDD5E6B914B8C59B252E4F0,
	FrameExtract_Start_m624D7C4A0002C7083BDB353354BC7ED6DC0525D9,
	FrameExtract_OnMediaPlayerEvent_m2884ED30BA45984B3E16A3C5E3577BE3A96F677C,
	FrameExtract_OnNewMediaReady_m7363DB357787FC9474FA2D486CF758AED4613776,
	FrameExtract_OnDestroy_mF1734CDA90E926AB36ED086F4544C9034EFDC65E,
	FrameExtract_Update_m02212BF8C47F48B296F26E3B85DC6F4551164407,
	FrameExtract_ProcessExtractedFrame_mDDCA242942C99AC29333BFDAB82FB9766BDF3E81,
	FrameExtract_ExtractNextFrame_m2D5DE0399CD83607CAACBFF2FA327C85763CC486,
	FrameExtract_OnGUI_mE9BCF528E03EFEAF055998A010A47CD47FE5A698,
	FrameExtract__ctor_m4016D5DD96F482E85280288C58D85E585D2C3EEF,
	Mapping3D_Update_mE93C103B982AE4AFA6C2F5B8A770029A4D5C5D35,
	Mapping3D_SpawnCube_m3C9287973623E4631EDBC024F9714DE0051AC5A2,
	Mapping3D_RemoveCube_mF420949F2054B4D03C93B696B711129C92418E42,
	Mapping3D__ctor_m0801E9C63A5A1F6D0192E4E05C540EEA07945D09,
	SampleApp_Multiple_Update_mA299238F9B201180137610E19C40360FA9D81587,
	SampleApp_Multiple_UpdateVideosLayout_m4C37A5152E5EBD2440E3E524DE9685948CDF51F7,
	SampleApp_Multiple_AddVideoClicked_mDEBC0C0F84103A8A827EF86C04F6D9DDDE2C1ECB,
	SampleApp_Multiple_RemoveVideoClicked_m7D3249A4E51B03BD1959BCCF724740AF5BEFEEE2,
	SampleApp_Multiple_OnDestroy_m0590F222FDDDCCD96FFD8008221BC49584E6A7B6,
	SampleApp_Multiple__ctor_m20C539F251E6A956E23A13791C929EF034F61172,
	ChangeAudioTrack_OnEnable_m75F693520F7AB2D689F6282C185F292CB5705509,
	ChangeAudioTrack_Update_m5425E11A36409F70681511679F7FF41C478E422B,
	ChangeAudioTrack_IsVideoLoaded_mA5A8F22E752567D8D6ED2FBA8CC8939E70E0A0AB,
	ChangeAudioTrack_DoChangeAudioTrack_m9D4ABCA62680E421169BCC101AD8CA44DE347186,
	ChangeAudioTrack__ctor_m1FB023CE311212491E9418FF1273695D616AA4C7,
	ChangeStereoMode_Update_m7526E6BBBDFE067AED298A4563BC6D32C79F16B8,
	ChangeStereoMode__ctor_mDF849CAFCFEFD27A26A2D2BDEFB1C07CED6DD797,
	ChangeVideoExample_LoadVideo_mE22AA3BBAF747AA4723327B12B042D837A01337B,
	ChangeVideoExample_OnGUI_mE6E93D08591DDEFB4D2D0E3299C6737A9D6490D1,
	ChangeVideoExample__ctor_mE286AAF97B55E64E20181A6EE453749248DD3DF5,
	LoadFromBuffer_Start_mA98E03E0D77C8BC7CBFB569C6279CD990F054B6B,
	LoadFromBuffer__ctor_m06F6A560BB6F61E5D64579C33FAC671F3CEA00D1,
	LoadFromBufferInChunks_Start_m4036B4BF0C34FD755C0218833E45E86983837688,
	LoadFromBufferInChunks__ctor_m61E1F375CB76F7B06B0FAAA41A02F5BC802AB246,
	NativeMediaOpen_Start_m044A8C77E479178A1BA9192590397A483EA6D87B,
	NativeMediaOpen_Update_mE78D22D2F37FBAC4758A0873648FE65F46A523AB,
	NativeMediaOpen_OnGUI_m151B8192030A6330F461AE8E55A71D2B5AAFAAEB,
	NativeMediaOpen__ctor_m580D704173E7C42FF6F30EEC2CBBC2AF949D85FA,
	PlaybackSync_Start_mE6DB503FA3CE7AB16E604F6951050B1025375B99,
	PlaybackSync_LateUpdate_m3B26ABD713378E2E0D90A159FD156183AC8E41B7,
	PlaybackSync_IsAllVideosLoaded_m8CAE02507E39719E709F3DD611EC14198ADF154C,
	PlaybackSync_IsVideoLoaded_m5E20764B5BFD5A53203E24F90609DC3DA76F8E61,
	PlaybackSync_IsPlaybackFinished_mDBE5C39D33BFC93DA747D64391C80738A98A4CD8,
	PlaybackSync__ctor_m0F248D3CB7320263377D590F13E11BC4467FB4A9,
	StartEndPoint_OnEnable_m02638CA3FC870BBDF1F415DB8C7BBB92A3355CE3,
	StartEndPoint_Update_mB6368F3BCF30D08529BD7F85E3CDBCF42AF51395,
	StartEndPoint_IsVideoLoaded_m0B96DB7EA00352C7462F7ACD574AB482EA5ECE9D,
	StartEndPoint_DoStart_mFAD879A1BBA946E07923D2B77E4AB8B465B68CAD,
	StartEndPoint_DoCheckEnd_mEA6ED57CA4DC4677EC6A6AB502E2F23E08AD284B,
	StartEndPoint_DoCheckLoop_m5804654DCA9248C03DDDFE8469B3FE3D2C7432D3,
	StartEndPoint__ctor_m71615C24A6BDF28714196E5F2F41BE52446C5993,
	VideoTrigger_OnTriggerEnter_m2983A2862DD2F5ECFF20DD7D3A713AB4575FA52F,
	VideoTrigger_OnTriggerExit_m5A871DCA8036BE6D1E3C1E8CF866072C61239097,
	VideoTrigger_Update_m77A64ED8BC8C23A8936DCE2452EA891C1F7EC236,
	VideoTrigger__ctor_m0CA0DE7B4BB531ABBED01FEDC98104ADD8C221E7,
	SimpleController_Start_mE67A5BE0BABD40F3843989BBD27226CAC9012DEA,
	SimpleController_OnDestroy_m2E889D579CFC9062E8AF4F402CE6871CC34D66CE,
	SimpleController_OnMediaPlayerEvent_mA204E0516E4D889078AF3EF0F3464EADFDF9177C,
	SimpleController_AddEvent_m1AD68B925D96DA06BDF7F1C70C391A702247E6AA,
	SimpleController_GatherProperties_mFA294185BA091A93A4B959BF103ED86E6BDB17B0,
	SimpleController_Update_m342957AE027132836B46B9E05B51C55F08B0A40E,
	SimpleController_LoadVideo_m556EDB14D348169F9136F6D231578F111770EF9F,
	SimpleController_VideoIsReady_mEE3EA61F987154B87C85EAA4C68FF35C79A61902,
	SimpleController_AudioIsReady_mB8528AAB265BA0D6229DCE8BBF6D32AE2680F959,
	SimpleController_LoadVideoWithFading_m79E36867177FC84073610479AAAA0F29326F3D1A,
	SimpleController_OnGUI_mE0F797243B99ACDDAE85F893F238BED637F998E5,
	SimpleController__ctor_m6C9D1CE5E877156BB6D9852903BFAF52262EF08F,
	U3CLoadVideoWithFadingU3Ed__23__ctor_m464D3CF77745A9FE30C1E20CC584C768A92E9AB6,
	U3CLoadVideoWithFadingU3Ed__23_System_IDisposable_Dispose_m7D4A0E6A6F1E000404C5E9054476FFD6A6D731B4,
	U3CLoadVideoWithFadingU3Ed__23_MoveNext_m2C92DE80E3392939189C7A83EEFE7E88468A0AFB,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35C73F9A099DE3EE7F662D4EF0D0F8184D54506D,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_Reset_m54C40996E5991A8777C08FDB3CEC1634D4765093,
	U3CLoadVideoWithFadingU3Ed__23_System_Collections_IEnumerator_get_Current_mEA84603238447705495433D48D08BE4CD14E8986,
	SphereDemo_IsVrPresent_m74136D943CB8A446BF26B82CD2F320427C268D31,
	SphereDemo_Start_mE1C38F6BF58B6BB3F405F198FDE9B5929B3F3FB4,
	SphereDemo_OnDestroy_m58C3B0E3DB111C860DAB700275DA99D1E963E2E2,
	SphereDemo_Update_m29112EF6DEB073B4C5C126941C5EB0FF475F2511,
	SphereDemo_LateUpdate_m00E015DE096AB197DF33303F8204823305161B10,
	SphereDemo__ctor_m7C24A5C73C2FDB373D6977F2745FC315FE998F62,
	VCR_get_PlayingPlayer_mB0DB688C27DC07C53D8030C94F58DCE2BB46ACAA,
	VCR_get_LoadingPlayer_m43F07DE427B4F3069F1A46C98ADE5DB7649D5FD5,
	VCR_SwapPlayers_m61EC274A60FBD89A46F6D030C90A01C62F271B0C,
	VCR_OnOpenVideoFile_m28B4CBE85312F6CFD5A6C95FDE406452B8289E84,
	VCR_OnAutoStartChange_m626C188F91FC04061D2AEA7B60165955E7FE4B47,
	VCR_OnMuteChange_mF4A493C3FB66CCD3FAA2DFC8631BEE51CE318B8C,
	VCR_OnPlayButton_mADAD0866287A99B35EC535C3B7F0141F1FA5EBEA,
	VCR_OnPauseButton_m8E90E83D52817F888430307FA8BD2B747537F624,
	VCR_OnVideoSeekSlider_mF4DDF3DE6BC48938D800BCCAF80C1610B6F784E8,
	VCR_OnVideoSliderDown_mD52D67098FB0CBDFF08432B44C3A583378319E1E,
	VCR_OnVideoSliderUp_mBC510AC64938D040DB5EEB60E95BFFC423E68A1A,
	VCR_OnAudioVolumeSlider_m4939762109F3226248B44502063D1EE06AFB3C1F,
	VCR_OnRewindButton_mF841AAA17650665F33BC8FBEBC905978A2823CC3,
	VCR_Awake_m6EDE28767F9A35EACBF42D6760BCB7EC34D1560B,
	VCR_Start_mDACECB28CD16540902B54588C06A09D93A060E70,
	VCR_OnDestroy_m488F4FF96425A7F804FC23D44CAD2B8E45D3AC88,
	VCR_Update_m93E8FA09BBF987D984006163580F8348851D3B58,
	VCR_OnVideoEvent_m16E494B43F890166ED2CC74576841D9B8C6CC961,
	VCR__ctor_m9772243B63EC6A79EC853A9B36C8DF8EBDA21048,
};
extern void CameraShot__ctor_m5D4853E2044D14AAB4D03B53A4C7409EDB5B2006_AdjustorThunk (void);
extern void U3CStartSessionU3Ed__42_MoveNext_m329796E6A3238FB5B238DEA7FAF14414FE812257_AdjustorThunk (void);
extern void U3CStartSessionU3Ed__42_SetStateMachine_mDC00E8F92E6128821F8B1C0B6FBEE6C8B4B88CAC_AdjustorThunk (void);
extern void U3CStopSessionU3Ed__43_MoveNext_mCD54D758D216F67B1069575F29A48D3F96C0AA43_AdjustorThunk (void);
extern void U3CStopSessionU3Ed__43_SetStateMachine_m88F137D413FEB1B7F9ED0B53F41DFCAD92232FB3_AdjustorThunk (void);
extern void U3CU3CLaunchU3Eb__0U3Ed_MoveNext_m1F3D2B6EE42AA4393605AB5ECD32A58CAB1517DC_AdjustorThunk (void);
extern void U3CU3CLaunchU3Eb__0U3Ed_SetStateMachine_m257EBACFAB8335CC954A8F191BA4BCBDCF3FB134_AdjustorThunk (void);
extern void HTTPHeader__ctor_mA1B6C1144262977F342F0E56019D842A4253C638_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[8] = 
{
	{ 0x0600047A, CameraShot__ctor_m5D4853E2044D14AAB4D03B53A4C7409EDB5B2006_AdjustorThunk },
	{ 0x060005B3, U3CStartSessionU3Ed__42_MoveNext_m329796E6A3238FB5B238DEA7FAF14414FE812257_AdjustorThunk },
	{ 0x060005B4, U3CStartSessionU3Ed__42_SetStateMachine_mDC00E8F92E6128821F8B1C0B6FBEE6C8B4B88CAC_AdjustorThunk },
	{ 0x060005B5, U3CStopSessionU3Ed__43_MoveNext_mCD54D758D216F67B1069575F29A48D3F96C0AA43_AdjustorThunk },
	{ 0x060005B6, U3CStopSessionU3Ed__43_SetStateMachine_m88F137D413FEB1B7F9ED0B53F41DFCAD92232FB3_AdjustorThunk },
	{ 0x06000612, U3CU3CLaunchU3Eb__0U3Ed_MoveNext_m1F3D2B6EE42AA4393605AB5ECD32A58CAB1517DC_AdjustorThunk },
	{ 0x06000613, U3CU3CLaunchU3Eb__0U3Ed_SetStateMachine_m257EBACFAB8335CC954A8F191BA4BCBDCF3FB134_AdjustorThunk },
	{ 0x0600079D, HTTPHeader__ctor_mA1B6C1144262977F342F0E56019D842A4253C638_AdjustorThunk },
};
static const int32_t s_InvokerIndices[2645] = 
{
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	115,
	4,
	23,
	3,
	168,
	1467,
	0,
	168,
	23,
	23,
	23,
	23,
	3,
	3,
	23,
	3366,
	89,
	89,
	89,
	23,
	23,
	23,
	136,
	205,
	136,
	205,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	337,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	337,
	31,
	31,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	31,
	14,
	14,
	89,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	726,
	337,
	28,
	23,
	26,
	32,
	28,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	137,
	14,
	3367,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	26,
	28,
	23,
	26,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	27,
	23,
	23,
	89,
	14,
	23,
	14,
	14,
	28,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	23,
	23,
	26,
	28,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	89,
	14,
	726,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	27,
	14,
	23,
	23,
	14,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	14,
	3367,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	14,
	14,
	89,
	10,
	10,
	23,
	35,
	26,
	26,
	26,
	28,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	32,
	10,
	26,
	26,
	26,
	23,
	23,
	121,
	37,
	32,
	26,
	14,
	23,
	23,
	14,
	14,
	23,
	26,
	26,
	23,
	23,
	23,
	31,
	31,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	32,
	337,
	31,
	26,
	1380,
	23,
	23,
	26,
	23,
	23,
	32,
	337,
	31,
	26,
	1380,
	23,
	23,
	26,
	26,
	14,
	26,
	23,
	23,
	23,
	14,
	26,
	14,
	23,
	62,
	212,
	32,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	23,
	23,
	23,
	14,
	32,
	31,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	26,
	26,
	23,
	26,
	23,
	26,
	23,
	32,
	32,
	32,
	32,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	137,
	137,
	27,
	27,
	23,
	23,
	23,
	14,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	23,
	28,
	58,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	137,
	137,
	26,
	26,
	26,
	26,
	26,
	26,
	27,
	27,
	116,
	116,
	23,
	23,
	23,
	23,
	23,
	26,
	137,
	137,
	27,
	27,
	3368,
	137,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	26,
	14,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	14,
	114,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	28,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	28,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	26,
	26,
	31,
	31,
	31,
	26,
	14,
	26,
	3369,
	26,
	26,
	137,
	137,
	26,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	23,
	23,
	26,
	27,
	27,
	212,
	27,
	27,
	23,
	23,
	23,
	23,
	26,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	26,
	89,
	89,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	27,
	27,
	212,
	27,
	27,
	26,
	23,
	26,
	26,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	32,
	23,
	23,
	23,
	14,
	23,
	726,
	726,
	337,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	337,
	726,
	337,
	23,
	1957,
	23,
	23,
	23,
	337,
	337,
	23,
	1568,
	23,
	23,
	23,
	23,
	3370,
	3371,
	3371,
	3372,
	3372,
	23,
	23,
	3370,
	23,
	23,
	3370,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	0,
	23,
	23,
	23,
	955,
	23,
	26,
	26,
	23,
	27,
	27,
	23,
	14,
	28,
	14,
	28,
	90,
	14,
	9,
	102,
	26,
	26,
	114,
	3373,
	3374,
	3,
	131,
	27,
	132,
	26,
	23,
	9,
	23,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	10,
	32,
	89,
	31,
	89,
	31,
	89,
	31,
	23,
	137,
	23,
	23,
	89,
	26,
	23,
	26,
	62,
	611,
	14,
	26,
	26,
	26,
	23,
	26,
	23,
	26,
	4,
	1,
	459,
	0,
	23,
	3,
	1,
	1,
	1,
	1,
	143,
	1,
	0,
	144,
	0,
	0,
	1,
	23,
	23,
	23,
	14,
	26,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	1651,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	939,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1651,
	23,
	726,
	726,
	726,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	23,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3375,
	31,
	23,
	1380,
	23,
	42,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1377,
	23,
	23,
	23,
	3376,
	23,
	23,
	23,
	23,
	14,
	34,
	62,
	28,
	27,
	14,
	26,
	10,
	89,
	89,
	89,
	89,
	89,
	89,
	27,
	26,
	28,
	34,
	28,
	14,
	14,
	14,
	34,
	207,
	10,
	458,
	338,
	10,
	32,
	726,
	337,
	89,
	31,
	14,
	14,
	0,
	0,
	98,
	284,
	97,
	283,
	43,
	94,
	814,
	109,
	141,
	141,
	9,
	10,
	0,
	827,
	0,
	26,
	26,
	26,
	26,
	14,
	26,
	14,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	23,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	10,
	89,
	34,
	62,
	28,
	27,
	10,
	27,
	34,
	28,
	14,
	14,
	26,
	207,
	23,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	10,
	89,
	14,
	28,
	27,
	34,
	62,
	10,
	27,
	28,
	34,
	28,
	14,
	14,
	26,
	207,
	23,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	23,
	3377,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	10,
	89,
	14,
	26,
	26,
	26,
	207,
	9,
	10,
	10,
	89,
	14,
	26,
	458,
	338,
	338,
	26,
	26,
	207,
	109,
	9,
	10,
	10,
	89,
	14,
	26,
	89,
	31,
	31,
	26,
	26,
	207,
	9,
	10,
	10,
	89,
	14,
	26,
	89,
	31,
	9,
	10,
	26,
	207,
	23,
	10,
	26,
	27,
	26,
	34,
	62,
	28,
	27,
	26,
	27,
	141,
	141,
	9,
	10,
	10,
	32,
	726,
	337,
	458,
	338,
	89,
	31,
	14,
	14,
	207,
	0,
	23,
	23,
	23,
	1379,
	23,
	726,
	1395,
	89,
	89,
	89,
	89,
	89,
	23,
	26,
	1380,
	1637,
	26,
	23,
	4,
	4,
	23,
	23,
	14,
	89,
	14,
	23,
	23,
	32,
	23,
	23,
	3378,
	3011,
	2526,
	2527,
	121,
	27,
	116,
	14,
	102,
	136,
	140,
	23,
	26,
	121,
	23,
	32,
	27,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	1380,
	23,
	1655,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	89,
	23,
	14,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	14,
	23,
	26,
	32,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	23,
	726,
	10,
	23,
	14,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	10,
	23,
	26,
	23,
	23,
	23,
	10,
	23,
	23,
	23,
	23,
	26,
	26,
	4,
	14,
	-1,
	23,
	23,
	23,
	23,
	10,
	23,
	14,
	10,
	26,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	10,
	23,
	23,
	10,
	23,
	10,
	32,
	10,
	26,
	23,
	23,
	23,
	23,
	23,
	26,
	10,
	26,
	14,
	23,
	23,
	23,
	23,
	726,
	10,
	14,
	23,
	23,
	23,
	23,
	14,
	26,
	10,
	23,
	23,
	14,
	26,
	10,
	23,
	23,
	23,
	10,
	26,
	23,
	23,
	26,
	27,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	62,
	62,
	62,
	62,
	43,
	23,
	10,
	32,
	89,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	62,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	23,
	32,
	26,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	62,
	26,
	14,
	14,
	26,
	14,
	26,
	26,
	27,
	28,
	23,
	28,
	23,
	28,
	28,
	23,
	23,
	14,
	14,
	26,
	89,
	89,
	9,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	26,
	26,
	27,
	27,
	212,
	26,
	26,
	62,
	23,
	26,
	27,
	27,
	26,
	23,
	23,
	26,
	27,
	27,
	26,
	26,
	26,
	23,
	23,
	26,
	27,
	23,
	32,
	3379,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1395,
	1396,
	1395,
	1396,
	23,
	26,
	23,
	35,
	23,
	1224,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	1395,
	1396,
	1395,
	1396,
	23,
	23,
	35,
	26,
	23,
	1224,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	32,
	137,
	23,
	23,
	23,
	23,
	23,
	26,
	35,
	143,
	137,
	23,
	26,
	14,
	23,
	23,
	23,
	23,
	23,
	1277,
	23,
	89,
	31,
	14,
	26,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	1356,
	23,
	23,
	109,
	1,
	4,
	4,
	23,
	23,
	14,
	14,
	89,
	23,
	23,
	14,
	26,
	1356,
	1357,
	23,
	26,
	26,
	26,
	3380,
	23,
	3,
	14,
	89,
	31,
	10,
	32,
	14,
	14,
	14,
	14,
	14,
	14,
	89,
	89,
	31,
	89,
	31,
	10,
	32,
	26,
	14,
	89,
	31,
	726,
	337,
	726,
	337,
	14,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	3381,
	476,
	851,
	3382,
	89,
	89,
	14,
	10,
	2088,
	483,
	23,
	9,
	434,
	3382,
	89,
	89,
	23,
	23,
	23,
	23,
	23,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	115,
	14,
	43,
	126,
	14,
	187,
	14,
	2617,
	14,
	3383,
	23,
	23,
	23,
	23,
	30,
	849,
	849,
	31,
	31,
	4,
	3384,
	3385,
	3386,
	3387,
	23,
	3,
	23,
	89,
	14,
	14,
	14,
	0,
	0,
	23,
	27,
	89,
	23,
	14,
	89,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	89,
	23,
	23,
	14,
	89,
	23,
	23,
	23,
	89,
	23,
	89,
	23,
	89,
	23,
	23,
	131,
	26,
	218,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	30,
	23,
	23,
	14,
	14,
	14,
	10,
	14,
	10,
	32,
	89,
	31,
	14,
	14,
	14,
	23,
	14,
	14,
	23,
	23,
	23,
	35,
	89,
	89,
	30,
	30,
	26,
	89,
	3388,
	23,
	34,
	10,
	10,
	89,
	187,
	89,
	1358,
	43,
	23,
	43,
	441,
	441,
	441,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	280,
	23,
	23,
	14,
	23,
	89,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	26,
	35,
	23,
	10,
	32,
	23,
	27,
	23,
	23,
	4,
	23,
	3389,
	110,
	3,
	14,
	3390,
	9,
	434,
	3382,
	89,
	23,
	31,
	89,
	89,
	89,
	23,
	23,
	23,
	23,
	337,
	337,
	1534,
	726,
	458,
	14,
	726,
	337,
	726,
	10,
	10,
	1356,
	726,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	147,
	31,
	26,
	26,
	26,
	26,
	10,
	34,
	10,
	89,
	187,
	89,
	14,
	1358,
	31,
	89,
	337,
	337,
	726,
	726,
	10,
	34,
	10,
	32,
	14,
	10,
	10,
	1655,
	23,
	32,
	31,
	1341,
	1655,
	23,
	10,
	34,
	10,
	32,
	14,
	10,
	726,
	187,
	726,
	23,
	23,
	23,
	10,
	187,
	14,
	89,
	10,
	3017,
	38,
	26,
	35,
	89,
	89,
	9,
	23,
	10,
	14,
	23,
	23,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	2245,
	109,
	27,
	207,
	89,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	9,
	10,
	14,
	3390,
	9,
	434,
	3382,
	89,
	23,
	31,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	23,
	23,
	23,
	23,
	337,
	337,
	1534,
	726,
	458,
	14,
	726,
	337,
	31,
	89,
	337,
	337,
	726,
	726,
	10,
	32,
	10,
	32,
	726,
	10,
	3017,
	10,
	187,
	38,
	35,
	10,
	1655,
	23,
	32,
	31,
	1341,
	1655,
	23,
	147,
	31,
	26,
	26,
	26,
	26,
	726,
	10,
	10,
	1356,
	726,
	726,
	89,
	89,
	10,
	34,
	14,
	10,
	10,
	34,
	14,
	10,
	14,
	89,
	89,
	14,
	187,
	10,
	34,
	10,
	89,
	187,
	89,
	1358,
	485,
	485,
	23,
	43,
	43,
	4,
	143,
	3391,
	94,
	3392,
	372,
	372,
	3343,
	372,
	610,
	2238,
	3393,
	3394,
	3395,
	3396,
	3397,
	94,
	0,
	14,
	3390,
	23,
	31,
	89,
	89,
	89,
	89,
	89,
	23,
	23,
	23,
	89,
	89,
	89,
	89,
	89,
	726,
	10,
	10,
	726,
	34,
	10,
	89,
	337,
	337,
	1534,
	726,
	337,
	726,
	726,
	31,
	89,
	337,
	726,
	10,
	10,
	32,
	10,
	10,
	14,
	10,
	32,
	14,
	10,
	726,
	23,
	23,
	23,
	23,
	10,
	10,
	187,
	213,
	726,
	337,
	726,
	337,
	187,
	213,
	35,
	36,
	14,
	23,
	23,
	23,
	23,
	89,
	37,
	37,
	23,
	136,
	3398,
	23,
	23,
	726,
	23,
	23,
	23,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	23,
	49,
	3,
	10,
	3399,
	3399,
	14,
	115,
	3390,
	9,
	434,
	3382,
	89,
	26,
	23,
	31,
	89,
	89,
	89,
	89,
	89,
	23,
	23,
	23,
	89,
	89,
	89,
	89,
	89,
	726,
	10,
	10,
	726,
	726,
	34,
	10,
	187,
	89,
	337,
	337,
	726,
	337,
	726,
	726,
	10,
	3017,
	31,
	89,
	337,
	726,
	337,
	726,
	10,
	10,
	32,
	10,
	89,
	14,
	10,
	10,
	32,
	14,
	10,
	147,
	32,
	1655,
	23,
	31,
	1341,
	1655,
	23,
	23,
	187,
	23,
	23,
	23,
	23,
	35,
	89,
	178,
	4,
	23,
	3,
	2338,
	3,
	767,
	49,
	3400,
	3401,
	3402,
	3403,
	3404,
	25,
	1007,
	169,
	597,
	25,
	25,
	3405,
	1683,
	1683,
	3405,
	589,
	589,
	169,
	169,
	2596,
	2596,
	169,
	589,
	589,
	589,
	589,
	589,
	589,
	2596,
	3406,
	2596,
	1683,
	169,
	1445,
	2596,
	2665,
	25,
	25,
	25,
	1007,
	589,
	169,
	597,
	767,
	767,
	767,
	3405,
	1010,
	169,
	2662,
	3407,
	3405,
	3408,
	3407,
	23,
	23,
	23,
	23,
	23,
	23,
	35,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	106,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	109,
	109,
	23,
	23,
	23,
	109,
	1467,
	1467,
	2694,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	35,
	32,
	23,
	23,
	102,
	109,
	109,
	14,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	49,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	35,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[5] = 
{
	{ 0x060005BA, { 0, 1 } },
	{ 0x060005BB, { 1, 1 } },
	{ 0x060005BC, { 2, 2 } },
	{ 0x060005BD, { 4, 1 } },
	{ 0x06000602, { 5, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[6] = 
{
	{ (Il2CppRGCTXDataType)2, 62720 },
	{ (Il2CppRGCTXDataType)3, 63007 },
	{ (Il2CppRGCTXDataType)2, 63806 },
	{ (Il2CppRGCTXDataType)3, 63008 },
	{ (Il2CppRGCTXDataType)3, 63009 },
	{ (Il2CppRGCTXDataType)3, 63010 },
};
extern const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharpCodeGenModule = 
{
	"Assembly-CSharp.dll",
	2645,
	s_methodPointers,
	8,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	5,
	s_rgctxIndices,
	6,
	s_rgctxValues,
	NULL,
};
