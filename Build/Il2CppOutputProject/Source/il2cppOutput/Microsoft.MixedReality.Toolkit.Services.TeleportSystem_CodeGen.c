﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar)
extern void MixedRealityTeleportSystem__ctor_mA9006CDD4B83E33BF87B736276105D9BEDB586B9 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.ctor()
extern void MixedRealityTeleportSystem__ctor_mE44E7256CD06DDF4375BAD586037C313072D16BB (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_Name()
extern void MixedRealityTeleportSystem_get_Name_m4084C2D8544573799A6D484264E201E1249B94A0 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::set_Name(System.String)
extern void MixedRealityTeleportSystem_set_Name_m680050575BF745D609849FF601F623A428FA1666 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Initialize()
extern void MixedRealityTeleportSystem_Initialize_mF995F68102905F80FFD0AEA4F9E468D6AB058894 (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::InitializeInternal()
extern void MixedRealityTeleportSystem_InitializeInternal_m92CEB3532339E8365A24E00D1907D75D9B79C653 (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Destroy()
extern void MixedRealityTeleportSystem_Destroy_m1865626ED6825DD462F887067A4FFF90C88314BB (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::HandleEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Register(UnityEngine.GameObject)
extern void MixedRealityTeleportSystem_Register_m120E5A12B85C54DF61CE18F9F213CE9E2587BF11 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::Unregister(UnityEngine.GameObject)
extern void MixedRealityTeleportSystem_Unregister_m48A6A7CBE66D8856B550B4D1757E77F8F259D871 (void);
// 0x0000000B System.Boolean Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_IsInputSystemEnabled()
extern void MixedRealityTeleportSystem_get_IsInputSystemEnabled_m0E1B9B3D33609252557F4F45D1E4F41F4816BD25 (void);
// 0x0000000C System.Single Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::get_TeleportDuration()
extern void MixedRealityTeleportSystem_get_TeleportDuration_mBB86F970C582EF0B98758DC0F562122FD42F6E16 (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::set_TeleportDuration(System.Single)
extern void MixedRealityTeleportSystem_set_TeleportDuration_mCF2064E8CFAAA30DD8C9C10A0F0363EAC0262F36 (void);
// 0x0000000E System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportRequest(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotspot)
extern void MixedRealityTeleportSystem_RaiseTeleportRequest_m36D327139DD4D01B7EEC3F9613B171CEFB2A6E67 (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotspot)
extern void MixedRealityTeleportSystem_RaiseTeleportStarted_mC675E685F79EACB94CB0F083A93E880EFADC37FC (void);
// 0x00000010 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportComplete(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotspot)
extern void MixedRealityTeleportSystem_RaiseTeleportComplete_m30BF50881A4D307769B854BCE637A71325F66C7E (void);
// 0x00000011 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::RaiseTeleportCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHotspot)
extern void MixedRealityTeleportSystem_RaiseTeleportCanceled_m6B73DCC020DD973E414BCEDCD81D3A7035D69843 (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::ProcessTeleportationRequest(Microsoft.MixedReality.Toolkit.Teleport.TeleportEventData)
extern void MixedRealityTeleportSystem_ProcessTeleportationRequest_mE26F0D892F7CAA07A13AE2E40A2A63C7BF4C0391 (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem::.cctor()
extern void MixedRealityTeleportSystem__cctor_mCDE783D7B748CB8C7A5676F532598773746E6B54 (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::.cctor()
extern void U3CU3Ec__cctor_mA6BBD8EC8682FAF890FF41FC32FBBC88A3082E21 (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::.ctor()
extern void U3CU3Ec__ctor_m16FCB1037F7BE83AF2B00A5D6580036091D2D27B (void);
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_0(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_0_m92849586A9F43D8C5E03B429661E91F5C0DA3581 (void);
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_1(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_1_m8D9D9756B155F034F9224D7551704554B3A3AE39 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_2(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_2_m45A2559CE96E3379F5FEB16EBE3F8E22E4CFA692 (void);
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Teleport.MixedRealityTeleportSystem/<>c::<.cctor>b__39_3(Microsoft.MixedReality.Toolkit.Teleport.IMixedRealityTeleportHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__39_3_mBE770857CBBF8B29471BE81447D715B39067E822 (void);
static Il2CppMethodPointer s_methodPointers[25] = 
{
	MixedRealityTeleportSystem__ctor_mA9006CDD4B83E33BF87B736276105D9BEDB586B9,
	MixedRealityTeleportSystem__ctor_mE44E7256CD06DDF4375BAD586037C313072D16BB,
	MixedRealityTeleportSystem_get_Name_m4084C2D8544573799A6D484264E201E1249B94A0,
	MixedRealityTeleportSystem_set_Name_m680050575BF745D609849FF601F623A428FA1666,
	MixedRealityTeleportSystem_Initialize_mF995F68102905F80FFD0AEA4F9E468D6AB058894,
	MixedRealityTeleportSystem_InitializeInternal_m92CEB3532339E8365A24E00D1907D75D9B79C653,
	MixedRealityTeleportSystem_Destroy_m1865626ED6825DD462F887067A4FFF90C88314BB,
	NULL,
	MixedRealityTeleportSystem_Register_m120E5A12B85C54DF61CE18F9F213CE9E2587BF11,
	MixedRealityTeleportSystem_Unregister_m48A6A7CBE66D8856B550B4D1757E77F8F259D871,
	MixedRealityTeleportSystem_get_IsInputSystemEnabled_m0E1B9B3D33609252557F4F45D1E4F41F4816BD25,
	MixedRealityTeleportSystem_get_TeleportDuration_mBB86F970C582EF0B98758DC0F562122FD42F6E16,
	MixedRealityTeleportSystem_set_TeleportDuration_mCF2064E8CFAAA30DD8C9C10A0F0363EAC0262F36,
	MixedRealityTeleportSystem_RaiseTeleportRequest_m36D327139DD4D01B7EEC3F9613B171CEFB2A6E67,
	MixedRealityTeleportSystem_RaiseTeleportStarted_mC675E685F79EACB94CB0F083A93E880EFADC37FC,
	MixedRealityTeleportSystem_RaiseTeleportComplete_m30BF50881A4D307769B854BCE637A71325F66C7E,
	MixedRealityTeleportSystem_RaiseTeleportCanceled_m6B73DCC020DD973E414BCEDCD81D3A7035D69843,
	MixedRealityTeleportSystem_ProcessTeleportationRequest_mE26F0D892F7CAA07A13AE2E40A2A63C7BF4C0391,
	MixedRealityTeleportSystem__cctor_mCDE783D7B748CB8C7A5676F532598773746E6B54,
	U3CU3Ec__cctor_mA6BBD8EC8682FAF890FF41FC32FBBC88A3082E21,
	U3CU3Ec__ctor_m16FCB1037F7BE83AF2B00A5D6580036091D2D27B,
	U3CU3Ec_U3C_cctorU3Eb__39_0_m92849586A9F43D8C5E03B429661E91F5C0DA3581,
	U3CU3Ec_U3C_cctorU3Eb__39_1_m8D9D9756B155F034F9224D7551704554B3A3AE39,
	U3CU3Ec_U3C_cctorU3Eb__39_2_m45A2559CE96E3379F5FEB16EBE3F8E22E4CFA692,
	U3CU3Ec_U3C_cctorU3Eb__39_3_mBE770857CBBF8B29471BE81447D715B39067E822,
};
static const int32_t s_InvokerIndices[25] = 
{
	26,
	23,
	14,
	26,
	23,
	23,
	23,
	-1,
	26,
	26,
	89,
	726,
	337,
	27,
	27,
	27,
	27,
	26,
	3,
	3,
	23,
	27,
	27,
	27,
	27,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000008, { 0, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[1] = 
{
	{ (Il2CppRGCTXDataType)3, 62975 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_TeleportSystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_TeleportSystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.TeleportSystem.dll",
	25,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	1,
	s_rgctxValues,
	NULL,
};
