﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Byte[] FMExtensionMethods::FMEncodeToJPG(UnityEngine.Texture2D,System.Int32,FMChromaSubsamplingOption)
extern void FMExtensionMethods_FMEncodeToJPG_mEA8B88031312980B401A7B162B793EB50AFE5A05 (void);
// 0x00000002 System.Void FMExtensionMethods::FMLoadJPG(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Byte[])
extern void FMExtensionMethods_FMLoadJPG_m34CC978D5E7FA1A6E8143998C2E94C5F09982D2D (void);
// 0x00000003 System.Void FMExtensionMethods::FMMatchResolution(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Int32,System.Int32)
extern void FMExtensionMethods_FMMatchResolution_m849F96713529AB6091F6D6D6FE5F433F2070E63E (void);
// 0x00000004 System.Byte[] FMExtensionMethods::FMRawTextureDataToJPG(System.Byte[],System.Int32,System.Int32,System.Int32,FMChromaSubsamplingOption)
extern void FMExtensionMethods_FMRawTextureDataToJPG_mABD75BC62916FC0962EEAB016ABFFCEAEDCF62F1 (void);
// 0x00000005 System.Void FMExtensionMethods::FMJPGToRawTextureData(System.Byte[],System.Byte[]&,System.Int32&,System.Int32&,UnityEngine.TextureFormat)
extern void FMExtensionMethods_FMJPGToRawTextureData_mC207C0F855721B7FCE13135C9DF893528F3FFAEC (void);
// 0x00000006 FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
extern void FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A (void);
// 0x00000007 System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
extern void TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1 (void);
// 0x00000008 System.Void FMTurboJpegWrapper.TJDecompressor::Finalize()
extern void TJDecompressor_Finalize_m2CDEB66ADEECF9C2F7DD8312DC92B9D6DAF636FA (void);
// 0x00000009 System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74 (void);
// 0x0000000A System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E (void);
// 0x0000000B FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
extern void TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A (void);
// 0x0000000C FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
extern void TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997 (void);
// 0x0000000D FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
extern void TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911 (void);
// 0x0000000E System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F (void);
// 0x0000000F System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
extern void TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95 (void);
// 0x00000010 System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean)
extern void TJDecompressor_Dispose_m168AF7BF325AF4116571288FED73411FAC783242 (void);
// 0x00000011 System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
extern void DecompressedImage__ctor_mAA66065D12D8A58743FC653804A5D9E02358F448 (void);
// 0x00000012 System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
extern void DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41 (void);
// 0x00000013 System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
extern void DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D (void);
// 0x00000014 System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
extern void DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8 (void);
// 0x00000015 System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
extern void TurboJpegImport_TjInitCompress_m5D481C98E7233C1F96A8EBD1B887B52323D7CFE1 (void);
// 0x00000016 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjCompress2_mE47CB34033396A32565D6C22162B0CA15A6DC9A9 (void);
// 0x00000017 System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
extern void TurboJpegImport_TjInitDecompress_mFEFD01E2373F2F6F2277C1BB679B2B445C00EFCD (void);
// 0x00000018 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B (void);
// 0x00000019 System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402 (void);
// 0x0000001A System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
extern void TurboJpegImport_TjFree_m75A67BF5A45741A79826A366D0584C652D55EFF5 (void);
// 0x0000001B System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
extern void TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78 (void);
// 0x0000001C System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader3_x86_mC406308CA370922203E3FB4464CE1DD78ACBEE3C (void);
// 0x0000001D System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
extern void TurboJpegImport_TjDecompressHeader3_x64_m8ABE9493363F1014B8119E207F22BF353500688A (void);
// 0x0000001E System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress2_x86_m16C2D62F861785C4C2C3155444DCF363F1C992B1 (void);
// 0x0000001F System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TurboJpegImport_TjDecompress2_x64_mCF8035CD678FA2504EDD42B3C164855E1B437D0D (void);
// 0x00000020 System.Void FMTurboJpegWrapper.TurboJpegImport::.cctor()
extern void TurboJpegImport__cctor_m14EDB1B829C6218B7400940F71BD02D5323240CD (void);
// 0x00000021 System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
extern void TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9 (void);
// 0x00000022 System.Void FMTurboJpegWrapper.TJCompressor::Finalize()
extern void TJCompressor_Finalize_mAE14595B586EDE505718E5EBFC902D745257E20E (void);
// 0x00000023 System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
extern void TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2 (void);
// 0x00000024 System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
extern void TJCompressor_EncodeFMJPG_m720CCE4185E435735EE1813878DA35631FF6F09B (void);
// 0x00000025 System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
extern void TJCompressor_EncodeFMJPG_m286309881E43E4E7DD480AFD688500CA182EDA16 (void);
// 0x00000026 System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
extern void TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0 (void);
// 0x00000027 System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean)
extern void TJCompressor_Dispose_m0CD2BEEDF78D19BB1B9C9E99B474A4FAAB117913 (void);
// 0x00000028 System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
extern void TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2 (void);
// 0x00000029 System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
extern void TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232 (void);
// 0x0000002A System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
extern void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE (void);
// 0x0000002B System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
extern void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947 (void);
// 0x0000002C System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
extern void TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004 (void);
static Il2CppMethodPointer s_methodPointers[44] = 
{
	FMExtensionMethods_FMEncodeToJPG_mEA8B88031312980B401A7B162B793EB50AFE5A05,
	FMExtensionMethods_FMLoadJPG_m34CC978D5E7FA1A6E8143998C2E94C5F09982D2D,
	FMExtensionMethods_FMMatchResolution_m849F96713529AB6091F6D6D6FE5F433F2070E63E,
	FMExtensionMethods_FMRawTextureDataToJPG_mABD75BC62916FC0962EEAB016ABFFCEAEDCF62F1,
	FMExtensionMethods_FMJPGToRawTextureData_mC207C0F855721B7FCE13135C9DF893528F3FFAEC,
	FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A,
	TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1,
	TJDecompressor_Finalize_m2CDEB66ADEECF9C2F7DD8312DC92B9D6DAF636FA,
	TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74,
	TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E,
	TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A,
	TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997,
	TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911,
	TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F,
	TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95,
	TJDecompressor_Dispose_m168AF7BF325AF4116571288FED73411FAC783242,
	DecompressedImage__ctor_mAA66065D12D8A58743FC653804A5D9E02358F448,
	DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41,
	DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D,
	DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8,
	TurboJpegImport_TjInitCompress_m5D481C98E7233C1F96A8EBD1B887B52323D7CFE1,
	TurboJpegImport_TjCompress2_mE47CB34033396A32565D6C22162B0CA15A6DC9A9,
	TurboJpegImport_TjInitDecompress_mFEFD01E2373F2F6F2277C1BB679B2B445C00EFCD,
	TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B,
	TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402,
	TurboJpegImport_TjFree_m75A67BF5A45741A79826A366D0584C652D55EFF5,
	TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78,
	TurboJpegImport_TjDecompressHeader3_x86_mC406308CA370922203E3FB4464CE1DD78ACBEE3C,
	TurboJpegImport_TjDecompressHeader3_x64_m8ABE9493363F1014B8119E207F22BF353500688A,
	TurboJpegImport_TjDecompress2_x86_m16C2D62F861785C4C2C3155444DCF363F1C992B1,
	TurboJpegImport_TjDecompress2_x64_mCF8035CD678FA2504EDD42B3C164855E1B437D0D,
	TurboJpegImport__cctor_m14EDB1B829C6218B7400940F71BD02D5323240CD,
	TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9,
	TJCompressor_Finalize_mAE14595B586EDE505718E5EBFC902D745257E20E,
	TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2,
	TJCompressor_EncodeFMJPG_m720CCE4185E435735EE1813878DA35631FF6F09B,
	TJCompressor_EncodeFMJPG_m286309881E43E4E7DD480AFD688500CA182EDA16,
	TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0,
	TJCompressor_Dispose_m0CD2BEEDF78D19BB1B9C9E99B474A4FAAB117913,
	TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2,
	TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232,
	TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE,
	TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947,
	TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004,
};
extern void TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232_AdjustorThunk (void);
extern void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_AdjustorThunk (void);
extern void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[3] = 
{
	{ 0x06000029, TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232_AdjustorThunk },
	{ 0x0600002A, TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_AdjustorThunk },
	{ 0x0600002B, TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_AdjustorThunk },
};
static const int32_t s_InvokerIndices[44] = 
{
	208,
	2957,
	2958,
	2214,
	2959,
	21,
	23,
	23,
	2960,
	2961,
	2962,
	54,
	58,
	2963,
	23,
	31,
	2964,
	10,
	10,
	14,
	767,
	2965,
	767,
	2966,
	2967,
	25,
	169,
	2968,
	2966,
	2969,
	2967,
	3,
	23,
	23,
	2970,
	54,
	2971,
	23,
	31,
	179,
	136,
	32,
	32,
	3,
};
extern const Il2CppCodeGenModule g_FMTurbojpegWrapperCodeGenModule;
const Il2CppCodeGenModule g_FMTurbojpegWrapperCodeGenModule = 
{
	"FMTurbojpegWrapper.dll",
	44,
	s_methodPointers,
	3,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
