﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 Microsoft.MixedReality.Toolkit.Input.IMixedRealityController[] Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetActiveControllers()
extern void BaseInputSimulationService_GetActiveControllers_m05E8D5AE89F5FD0D08559C4DD577A3DA11EB1C7B (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void BaseInputSimulationService__ctor_m0265D7AEEFA6C5B9CE6210B1E411F9E6CCAF78B0 (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void BaseInputSimulationService__ctor_m34196730145BEBC0B2C51C87F1BFA0FB6CF40FEF (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateControllerDevice(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Object)
extern void BaseInputSimulationService_UpdateControllerDevice_m0C30B55C493E8383AA678EFD2824EF5A591C5EEF (void);
// 0x00000005 Microsoft.MixedReality.Toolkit.Input.BaseController Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_GetControllerDevice_m3936A77EA42F44771402B8EA3DA41122313C6721 (void);
// 0x00000006 Microsoft.MixedReality.Toolkit.Input.BaseController Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetOrAddControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void BaseInputSimulationService_GetOrAddControllerDevice_m224B441ED5213BA6BA57014A020ABCBA66ABBEB5 (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveControllerDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_RemoveControllerDevice_m5CC7BF04881D631A42483C2DD87E4749CB9D3832 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveAllControllerDevices()
extern void BaseInputSimulationService_RemoveAllControllerDevices_m903F4559E986EFEF79A46C46B0DDBDF99077CAEF (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateActiveControllers()
extern void BaseInputSimulationService_UpdateActiveControllers_m918DE29D57E1292D879B3CC6CA2F61FBC075EC66 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::UpdateHandDevice(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void BaseInputSimulationService_UpdateHandDevice_m55EBC6180BAE62AAA6A3950861DB19C93EE4ECBA (void);
// 0x0000000B Microsoft.MixedReality.Toolkit.Input.SimulatedHand Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_GetHandDevice_mEBC11B92E6917087F5315FBBAAFF19B52610790A (void);
// 0x0000000C Microsoft.MixedReality.Toolkit.Input.SimulatedHand Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::GetOrAddHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void BaseInputSimulationService_GetOrAddHandDevice_m052B36AD2BF800D919496E58A08DC7089C5828AF (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveHandDevice(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void BaseInputSimulationService_RemoveHandDevice_m03D23B1433F6902E22A6B095EBB0FE8D21DD0CBE (void);
// 0x0000000E System.Void Microsoft.MixedReality.Toolkit.Input.BaseInputSimulationService::RemoveAllHandDevices()
extern void BaseInputSimulationService_RemoveAllHandDevices_mE9DFF00F3FEDC9236A25312342FA475414009C9D (void);
// 0x0000000F Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_InputSimulationProfile()
// 0x00000010 Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_EyeGazeSimulationMode()
// 0x00000011 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_EyeGazeSimulationMode(Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode)
// 0x00000012 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerSimulationMode()
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerSimulationMode(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
// 0x00000014 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandDataLeft()
// 0x00000015 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandDataRight()
// 0x00000016 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_MotionControllerDataLeft()
// 0x00000017 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_MotionControllerDataRight()
// 0x00000018 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_UserInputEnabled()
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_UserInputEnabled(System.Boolean)
// 0x0000001A System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingControllerLeft()
// 0x0000001B System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingControllerRight()
// 0x0000001C System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleControllerLeft()
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleControllerLeft(System.Boolean)
// 0x0000001E System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleControllerRight()
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleControllerRight(System.Boolean)
// 0x00000020 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerPositionLeft()
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerPositionLeft(UnityEngine.Vector3)
// 0x00000022 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerPositionRight()
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerPositionRight(UnityEngine.Vector3)
// 0x00000024 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerRotationLeft()
// 0x00000025 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerRotationLeft(UnityEngine.Vector3)
// 0x00000026 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_ControllerRotationRight()
// 0x00000027 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_ControllerRotationRight(UnityEngine.Vector3)
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetControllerLeft()
// 0x00000029 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetControllerRight()
// 0x0000002A Microsoft.MixedReality.Toolkit.Input.HandSimulationMode Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandSimulationMode()
// 0x0000002B System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandSimulationMode(Microsoft.MixedReality.Toolkit.Input.HandSimulationMode)
// 0x0000002C System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingHandLeft()
// 0x0000002D System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsSimulatingHandRight()
// 0x0000002E System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleHandLeft()
// 0x0000002F System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleHandLeft(System.Boolean)
// 0x00000030 System.Boolean Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_IsAlwaysVisibleHandRight()
// 0x00000031 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_IsAlwaysVisibleHandRight(System.Boolean)
// 0x00000032 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandPositionLeft()
// 0x00000033 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandPositionLeft(UnityEngine.Vector3)
// 0x00000034 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandPositionRight()
// 0x00000035 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandPositionRight(UnityEngine.Vector3)
// 0x00000036 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandRotationLeft()
// 0x00000037 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandRotationLeft(UnityEngine.Vector3)
// 0x00000038 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::get_HandRotationRight()
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::set_HandRotationRight(UnityEngine.Vector3)
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetHandLeft()
// 0x0000003B System.Void Microsoft.MixedReality.Toolkit.Input.IInputSimulationService::ResetHandRight()
// 0x0000003C Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_Animation()
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::set_Animation(Microsoft.MixedReality.Toolkit.Input.InputAnimation)
// 0x0000003E System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_IsPlaying()
// 0x0000003F System.Single Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::get_LocalTime()
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::set_LocalTime(System.Single)
// 0x00000041 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Play()
// 0x00000042 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Stop()
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::Pause()
// 0x00000044 System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::LoadInputAnimation(System.String)
// 0x00000045 System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputPlaybackService::LoadInputAnimationAsync(System.String)
// 0x00000046 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::add_OnPlaybackStarted(System.Action)
extern void InputPlaybackService_add_OnPlaybackStarted_m5AC93F1472BA1BB0FFC382379D546EC9AB339919 (void);
// 0x00000047 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::remove_OnPlaybackStarted(System.Action)
extern void InputPlaybackService_remove_OnPlaybackStarted_m6962CFEAF53A7D93AC60F6C2EE91BDCD20B49165 (void);
// 0x00000048 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::add_OnPlaybackStopped(System.Action)
extern void InputPlaybackService_add_OnPlaybackStopped_mFB31462B8FE233A3621B13E23D04E2B62773BC76 (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::remove_OnPlaybackStopped(System.Action)
extern void InputPlaybackService_remove_OnPlaybackStopped_mFA4829346567F91380EEEAE98A7155A1CF583D93 (void);
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::add_OnPlaybackPaused(System.Action)
extern void InputPlaybackService_add_OnPlaybackPaused_m7D49B13E2A79CD7B77D6328AC81C16E2CF7F3759 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::remove_OnPlaybackPaused(System.Action)
extern void InputPlaybackService_remove_OnPlaybackPaused_mA21FACF88619E041515F55F4931DD33D95DEFDD2 (void);
// 0x0000004C System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_IsPlaying()
extern void InputPlaybackService_get_IsPlaying_mA9A61EBADD8A19E2074B7C50B2A68A98B38DCF7F (void);
// 0x0000004D System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void InputPlaybackService_CheckCapability_m0C2D26D5A9F855B911EDF68F1E39642DE5DA5EB4 (void);
// 0x0000004E System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_SmoothEyeTracking()
extern void InputPlaybackService_get_SmoothEyeTracking_m341A956D165FF43430EEADBEAC075DBB42DF0AEE (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::set_SmoothEyeTracking(System.Boolean)
extern void InputPlaybackService_set_SmoothEyeTracking_mF1037502FB11DDDF6D81CED18846E37597B08627 (void);
// 0x00000050 System.Single Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_Duration()
extern void InputPlaybackService_get_Duration_m22CF2A3E25A6C0A90E7E193CE9F35E6D507FCD12 (void);
// 0x00000051 System.Single Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_LocalTime()
extern void InputPlaybackService_get_LocalTime_m011BF09D0AA6C01D0AF15D920078B97165B29C3E (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::set_LocalTime(System.Single)
extern void InputPlaybackService_set_LocalTime_m3946AB090F68C0EE00896E9C00FF9384330351A0 (void);
// 0x00000053 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_HandDataLeft()
extern void InputPlaybackService_get_HandDataLeft_m5C9F2DA4A43E0AF618EEF622CDDDA69C2FC1F52A (void);
// 0x00000054 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_HandDataRight()
extern void InputPlaybackService_get_HandDataRight_m3BA065CC310290E2715AAA9E273DA54E9BE9B47B (void);
// 0x00000055 Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_Animation()
extern void InputPlaybackService_get_Animation_m5EF441B3C380F5DE2086AED98EE42C4B71B46AE0 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::set_Animation(Microsoft.MixedReality.Toolkit.Input.InputAnimation)
extern void InputPlaybackService_set_Animation_m203783DF1D5254992311B6A7D704056E414B5221 (void);
// 0x00000057 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::get_SaccadeProvider()
extern void InputPlaybackService_get_SaccadeProvider_m07E75E291AB4E00AA562D95E3EDBD7D4134643E8 (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputPlaybackService__ctor_m13DCF985AECDD38573A048B0A959246C5F412E8D (void);
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputPlaybackService__ctor_m3CD92FCD2DCB2763A187B947CFC1082B48A49511 (void);
// 0x0000005A System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Play()
extern void InputPlaybackService_Play_m060DD8123CA114D803C67EC6BB3568636601D41D (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Stop()
extern void InputPlaybackService_Stop_mF30B8E1755BEF17E35FE5B8186C9495B3BC152A8 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Pause()
extern void InputPlaybackService_Pause_m2E4B7F02F901B2379EF61223C1B539D5CB41DFA9 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Update()
extern void InputPlaybackService_Update_m474E98D4F8EEBCB26002786C31F58FB1F694980F (void);
// 0x0000005E System.Boolean Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::LoadInputAnimation(System.String)
extern void InputPlaybackService_LoadInputAnimation_m75472EA2A71D0152816CB227B3AD38AE8028556D (void);
// 0x0000005F System.Threading.Tasks.Task`1<System.Boolean> Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::LoadInputAnimationAsync(System.String)
extern void InputPlaybackService_LoadInputAnimationAsync_m6DDA8A5C5E78746F0CC7987B663CEC70609D69B1 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::Evaluate()
extern void InputPlaybackService_Evaluate_mB80A9844EA964DCDBD85A6DA2C476EFFE31BEBC0 (void);
// 0x00000061 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::EvaluateHandData(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void InputPlaybackService_EvaluateHandData_m7F0AFD19C083A27A1708C26D7D9352FCD6EED644 (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService::EvaluateEyeGaze()
extern void InputPlaybackService_EvaluateEyeGaze_m66A166CC52970E6C370660B3F8ACDC991BDDC896 (void);
// 0x00000063 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<LoadInputAnimationAsync>d__42::MoveNext()
extern void U3CLoadInputAnimationAsyncU3Ed__42_MoveNext_mCB77CB5EBF05BA2DB56E3E94ADF85B71E4A160E0 (void);
// 0x00000064 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<LoadInputAnimationAsync>d__42::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CLoadInputAnimationAsyncU3Ed__42_SetStateMachine_m217D1D4DB2AF680882855177E5862454AA89920E (void);
// 0x00000065 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<>c__DisplayClass44_0::.ctor()
extern void U3CU3Ec__DisplayClass44_0__ctor_m58B276C3F93511EE02DD77EAF5297FD54B35D02E (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Input.InputPlaybackService/<>c__DisplayClass44_0::<EvaluateHandData>b__0(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void U3CU3Ec__DisplayClass44_0_U3CEvaluateHandDataU3Eb__0_m7701FC3B31D63B9FA61E2580EE0AA6E719B2EF3C (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationHelpGuide::Start()
extern void InputSimulationHelpGuide_Start_mD9A562D7547A69D8890D06476AFE25809346E8DE (void);
// 0x00000068 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationHelpGuide::Update()
extern void InputSimulationHelpGuide_Update_m4E3E7888A93ABB70616F4679F5158AADABC6335A (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationHelpGuide::.ctor()
extern void InputSimulationHelpGuide__ctor_m2BFAE73AA8F79DA39B2A9275832ED127BF78F865 (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Input.MouseDelta::Reset()
extern void MouseDelta_Reset_mF0C2124433823D1F885660BF3BBED6B7E16E89E5 (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Input.MouseDelta::.ctor()
extern void MouseDelta__ctor_m84A7D341A6DE896BE9B59D90B24B6087AA8F83B5 (void);
// 0x0000006C Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerSimulationMode()
extern void InputSimulationService_get_ControllerSimulationMode_m04D121FE5C9BF09F4A0EA953B67B6AA21B3E1E48 (void);
// 0x0000006D System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerSimulationMode(Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode)
extern void InputSimulationService_set_ControllerSimulationMode_m7998183B132D860552FBD133355040E93D475054 (void);
// 0x0000006E Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataLeft()
extern void InputSimulationService_get_HandDataLeft_mFF2B211699074B5F55734D9BC468868EC39302DC (void);
// 0x0000006F Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataRight()
extern void InputSimulationService_get_HandDataRight_m5A62E12CF2F9A34B2D3B02BEEBEBB456E5BEF10A (void);
// 0x00000070 Microsoft.MixedReality.Toolkit.Input.SimulatedHandData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandDataGaze()
extern void InputSimulationService_get_HandDataGaze_m0F21C6A87142D483E0FC7C84A62FBB70C58A208A (void);
// 0x00000071 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_MotionControllerDataLeft()
extern void InputSimulationService_get_MotionControllerDataLeft_mBD4DD0C5ADA616189FD0671E38CA4A3AC0D18426 (void);
// 0x00000072 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_MotionControllerDataRight()
extern void InputSimulationService_get_MotionControllerDataRight_m70553ED7AB321C5D29EA07F22E650CDA64451B6F (void);
// 0x00000073 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingControllerLeft()
extern void InputSimulationService_get_IsSimulatingControllerLeft_mA52158B4BF2E7102A30CE019A78EF96B5CFBF0BB (void);
// 0x00000074 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingControllerRight()
extern void InputSimulationService_get_IsSimulatingControllerRight_m2CBDAC1318B405D812BF260C9ACF9ABE5612D782 (void);
// 0x00000075 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleControllerLeft()
extern void InputSimulationService_get_IsAlwaysVisibleControllerLeft_mC0232013CF65271020843D76AFB3AC086EAF3983 (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleControllerLeft(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleControllerLeft_mB9C1F76F383BE2A53B26A0D1A988D2D77FA847FA (void);
// 0x00000077 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleControllerRight()
extern void InputSimulationService_get_IsAlwaysVisibleControllerRight_m3003C05499E158A209874AF46C3CC2743C8DB795 (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleControllerRight(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleControllerRight_mB6AC46A1490507DC1941A9BA3268B286A80395E8 (void);
// 0x00000079 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerPositionLeft()
extern void InputSimulationService_get_ControllerPositionLeft_m726D69DD8A7599642F7F007A2D4AD8CE0BFAC8E8 (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerPositionLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerPositionLeft_m4ECDB2B43D26A155BDAF0F43ED7ACFE27807D88A (void);
// 0x0000007B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerPositionRight()
extern void InputSimulationService_get_ControllerPositionRight_mED6C4701E21172BC3EC80872A793C0BCBDAAE10C (void);
// 0x0000007C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerPositionRight(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerPositionRight_mEF735A8BAF734AC5C63B2F754E46BA52D551F77E (void);
// 0x0000007D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerRotationLeft()
extern void InputSimulationService_get_ControllerRotationLeft_mA025329C59A1B047864B58007E2C5F06AC5EC7B7 (void);
// 0x0000007E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerRotationLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerRotationLeft_mD32E599767967D9AFDAF9AAFEE85B0D8A97310EB (void);
// 0x0000007F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_ControllerRotationRight()
extern void InputSimulationService_get_ControllerRotationRight_m870EA3CFC2C6B5770BDB4FFD8EE4F16B135CB1F0 (void);
// 0x00000080 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_ControllerRotationRight(UnityEngine.Vector3)
extern void InputSimulationService_set_ControllerRotationRight_mD5151366906E4277BFE06F9DA13CFBB22276ACEA (void);
// 0x00000081 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetControllerLeft()
extern void InputSimulationService_ResetControllerLeft_m3BBB9678C6C3A2E213A045A2B6DEABF5525B29E0 (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetControllerRight()
extern void InputSimulationService_ResetControllerRight_mAB880F3E52DCE36C5F6FED160ED14A80CBE0C344 (void);
// 0x00000083 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_SimulateEyePosition()
extern void InputSimulationService_get_SimulateEyePosition_mBE6F207A8821316CDC53B56DBF49BA43842C408F (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_SimulateEyePosition(System.Boolean)
extern void InputSimulationService_set_SimulateEyePosition_m375509BB2674BAEAE75488B6801AEAD6C5C85C5B (void);
// 0x00000085 Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_EyeGazeSimulationMode()
extern void InputSimulationService_get_EyeGazeSimulationMode_m1F1A71EC0873AF788AD63D00E96C5C0342BB0EEE (void);
// 0x00000086 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_EyeGazeSimulationMode(Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode)
extern void InputSimulationService_set_EyeGazeSimulationMode_m518061FE0AF81B935B28BBD12578479BF18601A2 (void);
// 0x00000087 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_UserInputEnabled()
extern void InputSimulationService_get_UserInputEnabled_m46B8489814402ED3507CE497A7E90B89CFDF1D1B (void);
// 0x00000088 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_UserInputEnabled(System.Boolean)
extern void InputSimulationService_set_UserInputEnabled_m7C678861A4DA6E2970698E24FC262D1BF1B448C8 (void);
// 0x00000089 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputSimulationService__ctor_m104C418EB48CA8271185FF549901170FCA53D832 (void);
// 0x0000008A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputSimulationService__ctor_m8EEEC849B1992C63269C1C472196DF98A5606CB2 (void);
// 0x0000008B System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void InputSimulationService_CheckCapability_mC7DB80017AB2E26CC6534E204E30BE6CDF70D6E1 (void);
// 0x0000008C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Initialize()
extern void InputSimulationService_Initialize_m4E5E52FB8F274393597B2455B5E7098F775AF99E (void);
// 0x0000008D System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Destroy()
extern void InputSimulationService_Destroy_m8CD9ABA55140C346F342E43FDEF4F86DC4154D63 (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Enable()
extern void InputSimulationService_Enable_mB6B5363394ACF73DD42A9AA3E91D77EE4C8AEABE (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Disable()
extern void InputSimulationService_Disable_mF4C0ABBBA912FE6C45CBB8DEFF1B9C097B202E72 (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Update()
extern void InputSimulationService_Update_mFAEC71E865A740C897FB315569D3CBCFF1D04C60 (void);
// 0x00000091 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::LateUpdate()
extern void InputSimulationService_LateUpdate_m18404EBA3103A7CCC78942AD20303D5DE4F3D9C4 (void);
// 0x00000092 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_InputSimulationProfile()
extern void InputSimulationService_get_InputSimulationProfile_mE2845BB4599F11E7DA48E5C09C7F13E8932D0E46 (void);
// 0x00000093 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_InputSimulationProfile(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void InputSimulationService_set_InputSimulationProfile_m63E35784873EB3290E96FE3B4B4887E56CF00BD5 (void);
// 0x00000094 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeSaccadeProvider Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.get_SaccadeProvider()
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SaccadeProvider_mDED11ACF9E1667B9231FCC9F86FD9EC9463519D2 (void);
// 0x00000095 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.get_SmoothEyeTracking()
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SmoothEyeTracking_m81CD64073FE2BC350E46A5A02208E73510527CC7 (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider.set_SmoothEyeTracking(System.Boolean)
extern void InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_set_SmoothEyeTracking_m17E4187FA60FD28C171AD57F5FAD094B4A60A3DB (void);
// 0x00000097 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableCameraControl()
extern void InputSimulationService_EnableCameraControl_m41E5F76341BB823F1C8C1953F5D8427831C467AD (void);
// 0x00000098 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::DisableCameraControl()
extern void InputSimulationService_DisableCameraControl_m1FEC84AA8AD4C5A6AB40F88AF883DCAFE5C9631A (void);
// 0x00000099 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableHandSimulation()
extern void InputSimulationService_EnableHandSimulation_m50F9C9EB53DDA2BDA1009520F1BF6470A836E48C (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::EnableMotionControllerSimulation()
extern void InputSimulationService_EnableMotionControllerSimulation_m01015D5B4D82CB119E18EA1E88EF4746C4B7E53C (void);
// 0x0000009B System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::DisableControllerSimulation()
extern void InputSimulationService_DisableControllerSimulation_m22E30EBEDEC9628F10909EDB3F1EE6C93109DEA7 (void);
// 0x0000009C System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetMouseDelta()
extern void InputSimulationService_ResetMouseDelta_m8979F414BC45A324AC8BD6C33C0E40166267FA5C (void);
// 0x0000009D System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::UpdateMouseDelta()
extern void InputSimulationService_UpdateMouseDelta_mD41D395AD55F22D7932C104AAD4662920BE51084 (void);
// 0x0000009E UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ScreenToWorld(UnityEngine.Vector3)
extern void InputSimulationService_ScreenToWorld_mD7DA2614660E0BC41B27D7351D2DF9603EBB3206 (void);
// 0x0000009F UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::WorldToScreen(UnityEngine.Vector2)
extern void InputSimulationService_WorldToScreen_m02EF6C1DD61E08D3B65EC16F4D138874580702E0 (void);
// 0x000000A0 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::WorldToViewport(UnityEngine.Vector2)
extern void InputSimulationService_WorldToViewport_mE81057173AF7F94466B1471D17386E11B3B5F449 (void);
// 0x000000A1 Microsoft.MixedReality.Toolkit.Input.HandSimulationMode Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandSimulationMode()
extern void InputSimulationService_get_HandSimulationMode_mF1677CAAFDFA275C49E01C06199704CDC70FA983 (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandSimulationMode(Microsoft.MixedReality.Toolkit.Input.HandSimulationMode)
extern void InputSimulationService_set_HandSimulationMode_mA77585516604C057E3CDD2D93DB3295E95D424F3 (void);
// 0x000000A3 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingHandLeft()
extern void InputSimulationService_get_IsSimulatingHandLeft_m347BD6FC43318F6C063D71401D400E88C2F86406 (void);
// 0x000000A4 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsSimulatingHandRight()
extern void InputSimulationService_get_IsSimulatingHandRight_m541D9A8C02201D50FCF0D474A9F2FF5D54699B77 (void);
// 0x000000A5 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleHandLeft()
extern void InputSimulationService_get_IsAlwaysVisibleHandLeft_mB0A3AA67A4803445E3F6F692B9DBA80DC6AAFE68 (void);
// 0x000000A6 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleHandLeft(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleHandLeft_m50DDB62DDE47BF4F1E24770B8D71C81075719BFF (void);
// 0x000000A7 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_IsAlwaysVisibleHandRight()
extern void InputSimulationService_get_IsAlwaysVisibleHandRight_m839D7B1D552748CD3087D41D369F4FB474966552 (void);
// 0x000000A8 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_IsAlwaysVisibleHandRight(System.Boolean)
extern void InputSimulationService_set_IsAlwaysVisibleHandRight_mF83EA39D237CE933A0D962CEF4677E89C95ACFD2 (void);
// 0x000000A9 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandPositionLeft()
extern void InputSimulationService_get_HandPositionLeft_m8E8B38E356DCF75440D54B912C50B9FA1C5AC16E (void);
// 0x000000AA System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandPositionLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_HandPositionLeft_mBC59A580B1998B35AFDB2BA3411479DCF50AA928 (void);
// 0x000000AB UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandPositionRight()
extern void InputSimulationService_get_HandPositionRight_m32CA28F0B585858452DB6ED84C0D311A4D0293BB (void);
// 0x000000AC System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandPositionRight(UnityEngine.Vector3)
extern void InputSimulationService_set_HandPositionRight_m053C708A25B40FE04CF7F2EC7A1B95BF679A3629 (void);
// 0x000000AD UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandRotationLeft()
extern void InputSimulationService_get_HandRotationLeft_m2AB278FDA86297E4D449FC24EE33F2EFD8431103 (void);
// 0x000000AE System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandRotationLeft(UnityEngine.Vector3)
extern void InputSimulationService_set_HandRotationLeft_m7BD30BDD4A5D5C36984D1566E5BB729150077860 (void);
// 0x000000AF UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.InputSimulationService::get_HandRotationRight()
extern void InputSimulationService_get_HandRotationRight_m009960BD08F3F6949EE3D5CAFD61B7396F0F39B0 (void);
// 0x000000B0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::set_HandRotationRight(UnityEngine.Vector3)
extern void InputSimulationService_set_HandRotationRight_m587AD9CA088BFEBCC471CADE5BCCA305B529676B (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetHandLeft()
extern void InputSimulationService_ResetHandLeft_m33AF6C61E13A1A3E9FCAA4FB9F2636384C450371 (void);
// 0x000000B2 System.Void Microsoft.MixedReality.Toolkit.Input.InputSimulationService::ResetHandRight()
extern void InputSimulationService_ResetHandRight_mEF12FB605AFD1CA93116A80BE6EB003D37397360 (void);
// 0x000000B3 System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding::.cctor()
extern void KeyBinding__cctor_m3B6BB968002426A12AA680CAF1C2597736845659 (void);
// 0x000000B4 Microsoft.MixedReality.Toolkit.Input.KeyBinding/KeyType Microsoft.MixedReality.Toolkit.Input.KeyBinding::get_BindingType()
extern void KeyBinding_get_BindingType_m7C7EF3E41A13693DDB16E72C138EB0C22C2E7F5E (void);
// 0x000000B5 System.String Microsoft.MixedReality.Toolkit.Input.KeyBinding::ToString()
extern void KeyBinding_ToString_m2F76400346E6FB11F289439F5B076A1E443D20E0 (void);
// 0x000000B6 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetKeyCode(UnityEngine.KeyCode&)
extern void KeyBinding_TryGetKeyCode_m996EA797CC8F560919DC92E5315D7D6C27CDB8A5 (void);
// 0x000000B7 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetMouseButton(System.Int32&)
extern void KeyBinding_TryGetMouseButton_m6AFB2C2A57C62D1356C08DFC3AA2E21EE7D96988 (void);
// 0x000000B8 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyBinding::TryGetMouseButton(Microsoft.MixedReality.Toolkit.Input.KeyBinding/MouseButton&)
extern void KeyBinding_TryGetMouseButton_mB24709C0C7A9DC5DF21F2950D47BA7865795B35E (void);
// 0x000000B9 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::Unbound()
extern void KeyBinding_Unbound_m738C3BBDDAE557517EDE9E7B02AFAE9574FE3E13 (void);
// 0x000000BA Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromKey(UnityEngine.KeyCode)
extern void KeyBinding_FromKey_m94DEF759DCCFF8CB4A3A80F198DB1E864004E6A1 (void);
// 0x000000BB Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromMouseButton(System.Int32)
extern void KeyBinding_FromMouseButton_mE5283BA7642A597CDCBFC18C4EE7268F150629CC (void);
// 0x000000BC Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.KeyBinding::FromMouseButton(Microsoft.MixedReality.Toolkit.Input.KeyBinding/MouseButton)
extern void KeyBinding_FromMouseButton_m15379136513EBB95E9530721D1E5E64FD0234114 (void);
// 0x000000BD System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding/<>c__DisplayClass5_0::.ctor()
extern void U3CU3Ec__DisplayClass5_0__ctor_m546E153A2930436656C03AA4E7D634ADCD7A4205 (void);
// 0x000000BE System.Void Microsoft.MixedReality.Toolkit.Input.KeyBinding/<>c__DisplayClass5_0::<.cctor>b__0(Microsoft.MixedReality.Toolkit.Input.KeyBinding/KeyType,System.Int32)
extern void U3CU3Ec__DisplayClass5_0_U3C_cctorU3Eb__0_m6D3AE2E8E892009D434A32B0B10C539F331740C2 (void);
// 0x000000BF System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::get_SimulatingInput()
extern void KeyInputSystem_get_SimulatingInput_m30AAA0B95668460F2CE6EE3F9FD89C9E4EE54360 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::StartKeyInputStimulation()
extern void KeyInputSystem_StartKeyInputStimulation_mF1E2AE4DDF80852286C25ACA7C7F75EDF1B2C74B (void);
// 0x000000C1 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::StopKeyInputSimulation()
extern void KeyInputSystem_StopKeyInputSimulation_m6BF0551272A04F2A7705110A22998397073D0634 (void);
// 0x000000C2 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::ResetKeyInputSimulation()
extern void KeyInputSystem_ResetKeyInputSimulation_mA44CB4548D9BFDC2D86FE8E39548B9E295BB2A5A (void);
// 0x000000C3 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::PressKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_PressKey_m5FDB43470A2DEE3D7E183A70257D808737B7CB48 (void);
// 0x000000C4 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::ReleaseKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_ReleaseKey_m8B09C804DEAFBD7930E879929856462F9DE416B5 (void);
// 0x000000C5 System.Void Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::AdvanceSimulation()
extern void KeyInputSystem_AdvanceSimulation_m51F35B119EB6F24E225B0BFD59AB167DC25E7573 (void);
// 0x000000C6 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKey(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKey_m391F25E4669A02CB118966FB3FFA6C87B18F7384 (void);
// 0x000000C7 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKeyDown(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKeyDown_m2C6F7C4DEAF7B225A9B2BB8042FA3B0ABD19F89A (void);
// 0x000000C8 System.Boolean Microsoft.MixedReality.Toolkit.Input.KeyInputSystem::GetKeyUp(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void KeyInputSystem_GetKeyUp_mB361F891B2C6C5DDA2A75729CC28274D3B90E56B (void);
// 0x000000C9 System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void ManualCameraControl__ctor_m24B8A8B645FFB7CC701E9CFA3A009EAC4AF48CDE (void);
// 0x000000CA System.Single Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::InputCurve(System.Single)
extern void ManualCameraControl_InputCurve_m157A17E6046F2B6CD1A101A8155785A4D694BC99 (void);
// 0x000000CB System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::SetInitialTransform(UnityEngine.Transform)
extern void ManualCameraControl_SetInitialTransform_mAE98501ED0867A46762554267F711ACAD73D9EA3 (void);
// 0x000000CC System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::UpdateTransform(UnityEngine.Transform,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void ManualCameraControl_UpdateTransform_mB58B00387B45DB252AE4F11054CCBC3F9B93A83E (void);
// 0x000000CD System.Single Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetKeyDir(System.String,System.String)
extern void ManualCameraControl_GetKeyDir_m2A266A473C0C2B57D3FEF02B01E10A5D49EF7323 (void);
// 0x000000CE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetCameraControlTranslation(UnityEngine.Transform)
extern void ManualCameraControl_GetCameraControlTranslation_mCCE22B2AC69A66376A1002932E1DEAC61F77EFD3 (void);
// 0x000000CF UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::GetCameraControlRotation(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void ManualCameraControl_GetCameraControlRotation_mBD73E47397B9ACD2614C74D683F0D53FCF312FA8 (void);
// 0x000000D0 System.Void Microsoft.MixedReality.Toolkit.Input.ManualCameraControl::.cctor()
extern void ManualCameraControl__cctor_mB505EB226EAC24A0C8E2751C6E71A339CE430538 (void);
// 0x000000D1 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IndicatorsPrefab()
extern void MixedRealityInputSimulationProfile_get_IndicatorsPrefab_m9282C93F8F400A5D693319F339D613236CA0FB4E (void);
// 0x000000D2 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseRotationSensitivity()
extern void MixedRealityInputSimulationProfile_get_MouseRotationSensitivity_mD3C9B3113E93A402B025B20D76437B81336FF3CE (void);
// 0x000000D3 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseX()
extern void MixedRealityInputSimulationProfile_get_MouseX_m899FCA700D0E04763779EA4C697D16F047523C05 (void);
// 0x000000D4 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseY()
extern void MixedRealityInputSimulationProfile_get_MouseY_m10BD42099B7C90463DC71BF0D3B7D360EBFB4E23 (void);
// 0x000000D5 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseScroll()
extern void MixedRealityInputSimulationProfile_get_MouseScroll_m3AC4206CA14713636D3218EFE137A0C49267E991 (void);
// 0x000000D6 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_InteractionButton()
extern void MixedRealityInputSimulationProfile_get_InteractionButton_mC81E84A643354A93077DC9F764B4CFC77701A9B2 (void);
// 0x000000D7 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DoublePressTime()
extern void MixedRealityInputSimulationProfile_get_DoublePressTime_m24E4AFDFC2394F0647BA43454C837B6ED89549EC (void);
// 0x000000D8 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsHandsFreeInputEnabled()
extern void MixedRealityInputSimulationProfile_get_IsHandsFreeInputEnabled_mE517EE789BE07FB284C6943A3C7356CA0CA3E227 (void);
// 0x000000D9 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsCameraControlEnabled()
extern void MixedRealityInputSimulationProfile_get_IsCameraControlEnabled_m2D44C87D0FAA6FB0E975C903C3A7343AFD5310A8 (void);
// 0x000000DA System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseLookSpeed_mF9212C1AC3F0D73D365BCF8C72020572FC2A228D (void);
// 0x000000DB Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookButton()
extern void MixedRealityInputSimulationProfile_get_MouseLookButton_mD873055273103F1AFC501DC459B3B49D896E7B91 (void);
// 0x000000DC System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseLookToggle()
extern void MixedRealityInputSimulationProfile_get_MouseLookToggle_m6CA2017345631674A820AD6E864659BC40A2AFE1 (void);
// 0x000000DD System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_IsControllerLookInverted()
extern void MixedRealityInputSimulationProfile_get_IsControllerLookInverted_m84A6C879D9B10702ACB860AE5B09426F461C80A0 (void);
// 0x000000DE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_CameraOriginOffset()
extern void MixedRealityInputSimulationProfile_get_CameraOriginOffset_mCF94D7139F4CEB911FA7BEC42C7C3E3AC582D094 (void);
// 0x000000DF Microsoft.MixedReality.Toolkit.Input.InputSimulationControlMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_CurrentControlMode()
extern void MixedRealityInputSimulationProfile_get_CurrentControlMode_m2170CB0167713942A79D5D710E3C0D257A2ABFFD (void);
// 0x000000E0 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_FastControlKey()
extern void MixedRealityInputSimulationProfile_get_FastControlKey_mA9859A7E72A6E0FCC29378FF747F73CA3ACC33C2 (void);
// 0x000000E1 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControlSlowSpeed()
extern void MixedRealityInputSimulationProfile_get_ControlSlowSpeed_mFD8FE5ED589A2BB0B083FC6564DA74553088A1E5 (void);
// 0x000000E2 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControlFastSpeed()
extern void MixedRealityInputSimulationProfile_get_ControlFastSpeed_mF7DF539E0D370D0816E2FA9421BF06823BC9689D (void);
// 0x000000E3 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveHorizontal()
extern void MixedRealityInputSimulationProfile_get_MoveHorizontal_mB6BF339C7E1DD329602F010F6E0AE521C8F90A5E (void);
// 0x000000E4 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveVertical()
extern void MixedRealityInputSimulationProfile_get_MoveVertical_m7281AC26D862C0E6DB1BEB3095C51491905FBE58 (void);
// 0x000000E5 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MoveUpDown()
extern void MixedRealityInputSimulationProfile_get_MoveUpDown_mBAFE4AA37C858816965DD2C1BDD74BBD4B0C35BB (void);
// 0x000000E6 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LookHorizontal()
extern void MixedRealityInputSimulationProfile_get_LookHorizontal_mF49ACE9F1236152B23D757EEC44F40F41D697209 (void);
// 0x000000E7 System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LookVertical()
extern void MixedRealityInputSimulationProfile_get_LookVertical_m07B4C975068FE6E543C2FAF0B62E3305B42EB921 (void);
// 0x000000E8 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_SimulateEyePosition()
extern void MixedRealityInputSimulationProfile_get_SimulateEyePosition_m07171E1640B46580C86E949A30C08C5C89324A70 (void);
// 0x000000E9 Microsoft.MixedReality.Toolkit.Input.EyeGazeSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultEyeGazeSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultEyeGazeSimulationMode_m1A6296AB187208634879A3DFC393F6F86EE9C141 (void);
// 0x000000EA Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultControllerSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultControllerSimulationMode_m90ECFC92C9AD7CAAC9F9A0663A6CD10372BDE2D1 (void);
// 0x000000EB Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleLeftControllerKey()
extern void MixedRealityInputSimulationProfile_get_ToggleLeftControllerKey_m708147C69748F10AAA94557F174E0997CA5E8192 (void);
// 0x000000EC Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleRightControllerKey()
extern void MixedRealityInputSimulationProfile_get_ToggleRightControllerKey_mD9AD2C0CCE10DDB2CFDDA472DAD0D779CA07CFA4 (void);
// 0x000000ED System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerHideTimeout()
extern void MixedRealityInputSimulationProfile_get_ControllerHideTimeout_m22EB5A286D955023FEAD704579F508CE41363715 (void);
// 0x000000EE Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftControllerManipulationKey()
extern void MixedRealityInputSimulationProfile_get_LeftControllerManipulationKey_mBE036948026D69856389BB8FD47FA4834EC9BF22 (void);
// 0x000000EF Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightControllerManipulationKey()
extern void MixedRealityInputSimulationProfile_get_RightControllerManipulationKey_m225CEDE48D195E6CDF9DA737C9EB76EAEFB8F573 (void);
// 0x000000F0 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseControllerRotationSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseControllerRotationSpeed_mC147D5238186F08DE50F00350DF4849223783265 (void);
// 0x000000F1 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerRotateButton()
extern void MixedRealityInputSimulationProfile_get_ControllerRotateButton_m1F87F19A2CCFE4E8C7C2012A5C1D1EA9B0630163 (void);
// 0x000000F2 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandGesture()
extern void MixedRealityInputSimulationProfile_get_DefaultHandGesture_m9BC3502D42BD31BE03566F9FFACD849E24C5E453 (void);
// 0x000000F3 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_LeftMouseHandGesture_m7C1A34A3EA6A15BFC1FD08ECA22D74F22404F336 (void);
// 0x000000F4 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MiddleMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_MiddleMouseHandGesture_m24C2D088D2794BEAB5D3AA56404D4285A9101FA4 (void);
// 0x000000F5 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightMouseHandGesture()
extern void MixedRealityInputSimulationProfile_get_RightMouseHandGesture_m3B63561925E28FD6F0D87994A21158CFAB061B9F (void);
// 0x000000F6 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandGestureAnimationSpeed()
extern void MixedRealityInputSimulationProfile_get_HandGestureAnimationSpeed_mED8155B476ED6415422CECF27B44671EA6A9C955 (void);
// 0x000000F7 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HoldStartDuration()
extern void MixedRealityInputSimulationProfile_get_HoldStartDuration_m2D17F9FF0C1C8BE61307C381BD32A3CD5B6562FC (void);
// 0x000000F8 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_NavigationStartThreshold()
extern void MixedRealityInputSimulationProfile_get_NavigationStartThreshold_mBF0A07520AF83CA39398618F7E3BC2296E0DC035 (void);
// 0x000000F9 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultControllerDistance()
extern void MixedRealityInputSimulationProfile_get_DefaultControllerDistance_mA521FA318F87200C8D2C2BAC74BA165DFF927093 (void);
// 0x000000FA System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerDepthMultiplier()
extern void MixedRealityInputSimulationProfile_get_ControllerDepthMultiplier_m7E9EFF1DD822343AB0A08095AD4DEE210AE33BA8 (void);
// 0x000000FB System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ControllerJitterAmount()
extern void MixedRealityInputSimulationProfile_get_ControllerJitterAmount_m31F0BC01A21457E24B4BD97C69011178A977496C (void);
// 0x000000FC Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerTriggerKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerTriggerKey_m9634B8D226C7D26B1E6C798CE0984F2E35858BB5 (void);
// 0x000000FD Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerGrabKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerGrabKey_m341958DAF88AC2E76F38BC67323DBB69F90352A5 (void);
// 0x000000FE Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MotionControllerMenuKey()
extern void MixedRealityInputSimulationProfile_get_MotionControllerMenuKey_m56F1580EDD94F5958CE761A3B640F11C91960139 (void);
// 0x000000FF Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandSimulationMode()
extern void MixedRealityInputSimulationProfile_get_DefaultHandSimulationMode_mDDAF4222DFAA34D4882D67FF170FDB0B13F074C8 (void);
// 0x00000100 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleLeftHandKey()
extern void MixedRealityInputSimulationProfile_get_ToggleLeftHandKey_m42CD0D69BB6E326B5FB45D8B3A31E7273D723334 (void);
// 0x00000101 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_ToggleRightHandKey()
extern void MixedRealityInputSimulationProfile_get_ToggleRightHandKey_m6063381798DEAE17E29D20F11872082CF887991F (void);
// 0x00000102 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandHideTimeout()
extern void MixedRealityInputSimulationProfile_get_HandHideTimeout_m6C7AEE62989485E76F1BA9207DE04E61E21F85F7 (void);
// 0x00000103 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_LeftHandManipulationKey()
extern void MixedRealityInputSimulationProfile_get_LeftHandManipulationKey_m79A2154D03305AA19E2182907A560C11EEFE4C34 (void);
// 0x00000104 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_RightHandManipulationKey()
extern void MixedRealityInputSimulationProfile_get_RightHandManipulationKey_mD1CE98933BC5FC6F1D4B5734B1DD380416982F24 (void);
// 0x00000105 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_MouseHandRotationSpeed()
extern void MixedRealityInputSimulationProfile_get_MouseHandRotationSpeed_m77E30CD0B3F89F8EF90619A7A3CAD9236FDC2740 (void);
// 0x00000106 Microsoft.MixedReality.Toolkit.Input.KeyBinding Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandRotateButton()
extern void MixedRealityInputSimulationProfile_get_HandRotateButton_m4938D0427B31BD1FB4FE9DFE1E8A47D1825B0021 (void);
// 0x00000107 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_DefaultHandDistance()
extern void MixedRealityInputSimulationProfile_get_DefaultHandDistance_m9417C04C375CE73A16FB7B62E1BEE1392E2A6D03 (void);
// 0x00000108 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandDepthMultiplier()
extern void MixedRealityInputSimulationProfile_get_HandDepthMultiplier_mD6417E98994A6F06D744384431479E7C4360CD27 (void);
// 0x00000109 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::get_HandJitterAmount()
extern void MixedRealityInputSimulationProfile_get_HandJitterAmount_m32FBC40E5E94E04A670F9936A3BD43D4548A816D (void);
// 0x0000010A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile::.ctor()
extern void MixedRealityInputSimulationProfile__ctor_m264E684D38DDA516D89D5F2E29339D58A6AFFFDB (void);
// 0x0000010B System.Boolean Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::get_IsRotating()
extern void MouseRotationProvider_get_IsRotating_mF171085CA0A90AC9EC2CEBFB35F23BC9B5761E22 (void);
// 0x0000010C System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::Update(Microsoft.MixedReality.Toolkit.Input.KeyBinding,Microsoft.MixedReality.Toolkit.Input.KeyBinding,System.Boolean)
extern void MouseRotationProvider_Update_mF1119040C324C1D10166BE9E11012C800700B77C (void);
// 0x0000010D System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::OnStartRotating(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void MouseRotationProvider_OnStartRotating_m9B33BD0C0A8922CE86A193A33AE3A95CEC9C269B (void);
// 0x0000010E System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::OnEndRotating(Microsoft.MixedReality.Toolkit.Input.KeyBinding)
extern void MouseRotationProvider_OnEndRotating_mE9B806A7740625A38D671E00B7DBC4A851414A04 (void);
// 0x0000010F System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::SetWantsMouseJumping(System.Boolean)
extern void MouseRotationProvider_SetWantsMouseJumping_mE63564E3692CB60F86C5947008F04E6A58434440 (void);
// 0x00000110 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::.ctor()
extern void MouseRotationProvider__ctor_mEAA8FF65CBC5C2A280B73FD5D2C63182CE115914 (void);
// 0x00000111 System.Void Microsoft.MixedReality.Toolkit.Input.MouseRotationProvider::.cctor()
extern void MouseRotationProvider__cctor_m4B24C0ED50B3F0459B9BF31A070E9B314D40243D (void);
// 0x00000112 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::get_SimulationMode()
extern void SimulatedArticulatedHand_get_SimulationMode_mF90E4B9592AE080FC902CB3CCC04E70FFF0C1D5C (void);
// 0x00000113 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedArticulatedHand__ctor_m58D26C00ACB816BFD48E90A8D6B45E05B415B281 (void);
// 0x00000114 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::UpdateHandJoints(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedArticulatedHand_UpdateHandJoints_mEAAEB96A29DE36DF45C10DF8F0B5234E762ED91F (void);
// 0x00000115 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedArticulatedHand_UpdateInteractions_mECC62222A95D77839C9D5432282FBF46AB63E946 (void);
// 0x00000116 Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::GetGesturePose(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedArticulatedHandPoses_GetGesturePose_m0487A43A9189DCA5AAD562F8CBE35FC4A99E2819 (void);
// 0x00000117 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::SetGesturePose(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId,Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose)
extern void SimulatedArticulatedHandPoses_SetGesturePose_m85C4E5745AA1B852DE9C5F8600965E6EDCFFC0AA (void);
// 0x00000118 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::LoadDefaultGesturePoses()
extern void SimulatedArticulatedHandPoses_LoadDefaultGesturePoses_m31925DB7949B93687613228AEE80DBBBA14A9502 (void);
// 0x00000119 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedArticulatedHandPoses::.cctor()
extern void SimulatedArticulatedHandPoses__cctor_m63422650D9BFCE5E59A9342DAAD7C5A5E333359C (void);
// 0x0000011A Microsoft.MixedReality.Toolkit.Utilities.Handedness Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::get_Handedness()
extern void SimulatedControllerState_get_Handedness_m844F4189E08977F2312876A1108AD041B56248BD (void);
// 0x0000011B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedControllerState__ctor_mB12E801723385B4CF8D31CC2B38E1BE631031047 (void);
// 0x0000011C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::SimulateInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean,System.Single,System.Single,System.Single)
extern void SimulatedControllerState_SimulateInput_m1E7AB6B66D768AC1F89A0A2AADD458EFC0079A74 (void);
// 0x0000011D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::ResetPosition(UnityEngine.Vector3)
extern void SimulatedControllerState_ResetPosition_m14FBDF8FA01D27DF584323C3503BB302E38E6A28 (void);
// 0x0000011E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::ResetRotation()
// 0x0000011F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState::Update()
extern void SimulatedControllerState_Update_m98D6F91DAA77FB47ED04E46E7E2676518F278F29 (void);
// 0x00000120 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_isSimulatingGaze()
extern void SimulatedControllerDataProvider_get_isSimulatingGaze_m9D4989666F01478204337ABA85F92200274E34C8 (void);
// 0x00000121 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_IsSimulatingLeft()
extern void SimulatedControllerDataProvider_get_IsSimulatingLeft_m55621674AEBD8451E546D461D0566FCCE6A9B80D (void);
// 0x00000122 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::set_IsSimulatingLeft(System.Boolean)
extern void SimulatedControllerDataProvider_set_IsSimulatingLeft_m7101DC35494A82C8743A98CD1E2AD28BB68903B3 (void);
// 0x00000123 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::get_IsSimulatingRight()
extern void SimulatedControllerDataProvider_get_IsSimulatingRight_mE78856E27F73BC4846D085D71C96D53C903C7668 (void);
// 0x00000124 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::set_IsSimulatingRight(System.Boolean)
extern void SimulatedControllerDataProvider_set_IsSimulatingRight_m0DE698BE7E9A49A0F5BBDDF0BDB7DC18509B548F (void);
// 0x00000125 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedControllerDataProvider__ctor_mF75C3F2B65B1EEF8127FDDB3F237FBAD41986E5F (void);
// 0x00000126 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::SimulateUserInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedControllerDataProvider_SimulateUserInput_mD8BA571C883E2154A59FA18C48FF5B6686DBDD28 (void);
// 0x00000127 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
// 0x00000128 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedControllerDataProvider_ResetInput_m8B96EC4613A315F0546211F120D265AAB1188F99 (void);
// 0x00000129 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedControllerDataProvider_ResetInput_m59B1AB1CDA7193F0436665150161E4CDBCFEAEA9 (void);
// 0x0000012A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedControllerDataProvider::.cctor()
extern void SimulatedControllerDataProvider__cctor_m752094A4375FB847EF6DDF54D99126FB3E7D975B (void);
// 0x0000012B Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::get_SimulationMode()
extern void SimulatedGestureHand_get_SimulationMode_m488387D3F1AB9FAD1D6485F29733ECD6749DED51 (void);
// 0x0000012C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::get_NavigationDelta()
extern void SimulatedGestureHand_get_NavigationDelta_m13DE38F4CA58300DBB5E5405B809334779D8DC89 (void);
// 0x0000012D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedGestureHand__ctor_mD0262912635E115B68A6F0797460216E49BD1861 (void);
// 0x0000012E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::EnsureProfileSettings()
extern void SimulatedGestureHand_EnsureProfileSettings_m8D677E39710B7C958A0410F7E05D363D904EAEC5 (void);
// 0x0000012F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateHandJoints(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedGestureHand_UpdateHandJoints_m38FFF531652CF0B161E6076E64FCE99C89FAE187 (void);
// 0x00000130 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedGestureHand_UpdateInteractions_mD10DDF592D2CA83767B0D2E3D664E07DA6F40722 (void);
// 0x00000131 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartHold()
extern void SimulatedGestureHand_TryStartHold_m14CCA7964C45EABAF322F4D3F49190C1A6BE0913 (void);
// 0x00000132 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteHold()
extern void SimulatedGestureHand_TryCompleteHold_m842AD31A4C883187D1F22234A5E6B542573B6A49 (void);
// 0x00000133 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelHold()
extern void SimulatedGestureHand_TryCancelHold_m31063521F4E96FA0653DE89D812321048267EF47 (void);
// 0x00000134 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartManipulation()
extern void SimulatedGestureHand_TryStartManipulation_m38BB3BCBBD27D2AC37B9898B7289DA4613093373 (void);
// 0x00000135 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateManipulation()
extern void SimulatedGestureHand_UpdateManipulation_m87A993A399B69B531D308580F54874A39D30F824 (void);
// 0x00000136 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteManipulation()
extern void SimulatedGestureHand_TryCompleteManipulation_mA573A32CAB1735DCB6B6EEC9F6E03742BBDC07BC (void);
// 0x00000137 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelManipulation()
extern void SimulatedGestureHand_TryCancelManipulation_m514E3AB32666DD10BD1E876B313B059D72DDBD60 (void);
// 0x00000138 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteSelect()
extern void SimulatedGestureHand_TryCompleteSelect_m2732AB094743A329DF2341548D1F2EE28E84756C (void);
// 0x00000139 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryStartNavigation()
extern void SimulatedGestureHand_TryStartNavigation_mF1EABCB39A28C61D7B54D21F3605DE4CD28F852D (void);
// 0x0000013A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateNavigation()
extern void SimulatedGestureHand_UpdateNavigation_mD966BE3A885D08FC9A4567723D6DCBBBE8946420 (void);
// 0x0000013B System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCompleteNavigation()
extern void SimulatedGestureHand_TryCompleteNavigation_m9566E95CB70F59BFC869F0A6356D980C56D1676F (void);
// 0x0000013C System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::TryCancelNavigation()
extern void SimulatedGestureHand_TryCancelNavigation_m60E0004951F6543F7420402F706A8D9EACB61C9E (void);
// 0x0000013D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedGestureHand::UpdateNavigationRails()
extern void SimulatedGestureHand_UpdateNavigationRails_mF12FD740A6AC7D063CB9B8ADA4F1F6CA624D3014 (void);
// 0x0000013E System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_IsTracked()
extern void SimulatedHandData_get_IsTracked_m7D9D2348F5D0AC0EAB28B1D4772ABC932EE904BB (void);
// 0x0000013F Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[] Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_Joints()
extern void SimulatedHandData_get_Joints_m7F946E75420EB197B9D4CDAF189FC0C0D31E9395 (void);
// 0x00000140 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::get_IsPinching()
extern void SimulatedHandData_get_IsPinching_m456058425802EB1F87C4D3BB00B7A0C5F7AE8BA0 (void);
// 0x00000141 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::Copy(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedHandData_Copy_m76A5E2C62D05FC9AF412DA935231AAB45F2DEE93 (void);
// 0x00000142 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::Update(System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator)
extern void SimulatedHandData_Update_m78CDDE5129B7AA2FEE7DA7CB269255709E24F403 (void);
// 0x00000143 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData::.ctor()
extern void SimulatedHandData__ctor_m16E35E38836CA52E125475C7A3C1B86E3C9EF383 (void);
// 0x00000144 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::.ctor(System.Object,System.IntPtr)
extern void HandJointDataGenerator__ctor_m506C42E4C1BD1A7B867BED83CBE1C31105970905 (void);
// 0x00000145 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::Invoke(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void HandJointDataGenerator_Invoke_mEE8BB7423D473BCBD0C1702F6E3B300C4C5BFB12 (void);
// 0x00000146 System.IAsyncResult Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::BeginInvoke(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[],System.AsyncCallback,System.Object)
extern void HandJointDataGenerator_BeginInvoke_mFCE59FC7B891B18FC581F56C62873660F7CEF52B (void);
// 0x00000147 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandData/HandJointDataGenerator::EndInvoke(System.IAsyncResult)
extern void HandJointDataGenerator_EndInvoke_m7FDF675A9C4F07855B3D595ADFA6B6CACA3EF247 (void);
// 0x00000148 Microsoft.MixedReality.Toolkit.Input.ControllerSimulationMode Microsoft.MixedReality.Toolkit.Input.SimulatedHand::get_SimulationMode()
// 0x00000149 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[],Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSourceDefinition)
extern void SimulatedHand__ctor_m45933026E7DCA4DBCDE6DD4C7282E6CB3985A3F0 (void);
// 0x0000014A System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHand::TryGetJoint(Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose&)
extern void SimulatedHand_TryGetJoint_mDC4EE8F6905F634CFF4F998E70C6F52A4AAF5F20 (void);
// 0x0000014B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::UpdateState(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
extern void SimulatedHand_UpdateState_m9BAD7E64923D90CCA01C91BB3F7F1489314FC33F (void);
// 0x0000014C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::UpdateHandJoints(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
// 0x0000014D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHand::UpdateInteractions(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData)
// 0x0000014E System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_IsPinching()
extern void SimulatedHandState_get_IsPinching_m0C619C2F1214BBC8C9ACAA24E11F1161CE7DF6E3 (void);
// 0x0000014F Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_Gesture()
extern void SimulatedHandState_get_Gesture_m5F76C235A87EAA5D5A3763465D0F6CC9A177D4F7 (void);
// 0x00000150 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::set_Gesture(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedHandState_set_Gesture_m3B54BFEC8EDAA952A1C2E02F75B5314DA653AFA9 (void);
// 0x00000151 System.Single Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::get_GestureBlending()
extern void SimulatedHandState_get_GestureBlending_mFC11DDE0D307ADD1DB402B96055AA7EA199067E9 (void);
// 0x00000152 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::set_GestureBlending(System.Single)
extern void SimulatedHandState_set_GestureBlending_mEFDE049EC403736DDF35AA5F37AE0F2ACF66896C (void);
// 0x00000153 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedHandState__ctor_m1F0CB1B090E1FB9073C9FB8640AA55086B550FC7 (void);
// 0x00000154 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::ResetGesture()
extern void SimulatedHandState_ResetGesture_m11506C99F82141CD0E81B41167337F49C68655BC (void);
// 0x00000155 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::ResetRotation()
extern void SimulatedHandState_ResetRotation_mFF357C326CE0569E53561C282B4B462641B8605E (void);
// 0x00000156 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandState::FillCurrentFrame(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose[])
extern void SimulatedHandState_FillCurrentFrame_m04FABD6FAFFBA686EFB338A6486E79F3A9295C1A (void);
// 0x00000157 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedHandDataProvider__ctor_mD8260D4CCAE93AB7E9BB17554BE10439DEBB0910 (void);
// 0x00000158 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::UpdateHandData(Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.SimulatedHandData,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedHandDataProvider_UpdateHandData_mCE40A949263B72A3BC1B082463886E4F8CA726A8 (void);
// 0x00000159 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SimulateUserInput(Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedHandDataProvider_SimulateUserInput_mF88D9FE33CB48A66942FF3FC440906B146A7E24D (void);
// 0x0000015A System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
extern void SimulatedHandDataProvider_SimulateInput_mEB7EB7A8C1B8620CC92976AF89CBADBFB4784C8D (void);
// 0x0000015B System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedHandDataProvider_ResetInput_mAADDCADDA2F79F34EB6FE25329077C1C1F37BD8C (void);
// 0x0000015C Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::SelectGesture()
extern void SimulatedHandDataProvider_SelectGesture_mE774ECF1F9F8EAD4A5C78F23F33EC5BA9C7092FB (void);
// 0x0000015D Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::ToggleGesture(Microsoft.MixedReality.Toolkit.Utilities.ArticulatedHandPose/GestureId)
extern void SimulatedHandDataProvider_ToggleGesture_m4E6E42897C9EAA552A647D15CB135D8C4AD837BB (void);
// 0x0000015E Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateLeft()
extern void SimulatedHandDataProvider_get_HandStateLeft_mF8A3808F2390D352CA80BD2E2242339433710CBD (void);
// 0x0000015F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateLeft(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateLeft_m48FE8012A82CC0A3512C21887223068489AC6B49 (void);
// 0x00000160 Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateRight()
extern void SimulatedHandDataProvider_get_HandStateRight_mC8CC7DD6E8597B0C6E6C4442CE4C825D5253C3A5 (void);
// 0x00000161 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateRight(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateRight_mDFF932289369DE32EE5C21C2615A39618E08DB47 (void);
// 0x00000162 Microsoft.MixedReality.Toolkit.Input.SimulatedHandState Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::get_HandStateGaze()
extern void SimulatedHandDataProvider_get_HandStateGaze_mDD70B4F9C467C8DFA688A3C67C3DABA563B6D244 (void);
// 0x00000163 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandDataProvider::set_HandStateGaze(Microsoft.MixedReality.Toolkit.Input.SimulatedHandState)
extern void SimulatedHandDataProvider_set_HandStateGaze_m2DCC3D93C9F20E284F954155E4D56908AC0E3A39 (void);
// 0x00000164 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::CalculateJointRotations(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[],UnityEngine.Quaternion[])
extern void SimulatedHandUtils_CalculateJointRotations_mDCE723FF3E3A21ED95AA2AEC5E0D179C0F1A890D (void);
// 0x00000165 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmForwardVector(UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmForwardVector_mAD80A1144F3FA499C205871629A85F875137EBB7 (void);
// 0x00000166 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmUpVector(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmUpVector_mBBFC4E357E8C7EF7C71DBCFF54D7D510ABDA07B1 (void);
// 0x00000167 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::GetPalmRightVector(Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3[])
extern void SimulatedHandUtils_GetPalmRightVector_m8222C68806F9B85395E14894EBC043FF45385450 (void);
// 0x00000168 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedHandUtils::.ctor()
extern void SimulatedHandUtils__ctor_m42453478DCEBCA8636A816B16CC7B352076760F2 (void);
// 0x00000169 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_IsTracked()
extern void SimulatedMotionControllerData_get_IsTracked_mA8302F34C478CD4315163A2A110E4289A461F500 (void);
// 0x0000016A Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_ButtonState()
extern void SimulatedMotionControllerData_get_ButtonState_m69D9DF5C12895C0780394196B20CF2BE5A597601 (void);
// 0x0000016B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_Position()
extern void SimulatedMotionControllerData_get_Position_mA828A96EF7583F38D94BE5D5187F7F97E7D649F5 (void);
// 0x0000016C System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::set_Position(UnityEngine.Vector3)
extern void SimulatedMotionControllerData_set_Position_m316893C2EFE530EFDCB0D9C0A0EF3D9D54B63FF6 (void);
// 0x0000016D UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::get_Rotation()
extern void SimulatedMotionControllerData_get_Rotation_m72A7B603281FEEC0D475F21FB558C30A03C2DE6D (void);
// 0x0000016E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::set_Rotation(UnityEngine.Quaternion)
extern void SimulatedMotionControllerData_set_Rotation_m164B6D5F54D298F093E9EA846D9705E7DA34C87E (void);
// 0x0000016F System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::Update(System.Boolean,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater)
extern void SimulatedMotionControllerData_Update_mFC06CDB3124E91A33FA1D830334F60BD81EE7646 (void);
// 0x00000170 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData::.ctor()
extern void SimulatedMotionControllerData__ctor_m356429BE25B7C968DD5CB29CB29D47A50386F387 (void);
// 0x00000171 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::.ctor(System.Object,System.IntPtr)
extern void MotionControllerPoseUpdater__ctor_m3DF94AD6C1C6FA5DC44E695956D0017516295CA0 (void);
// 0x00000172 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::Invoke()
extern void MotionControllerPoseUpdater_Invoke_mA982C7ED520192FB18029758B577A6CFD744AF05 (void);
// 0x00000173 System.IAsyncResult Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::BeginInvoke(System.AsyncCallback,System.Object)
extern void MotionControllerPoseUpdater_BeginInvoke_m2572D069E9B7B98BA93D732D0ABAE3AED189B10D (void);
// 0x00000174 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData/MotionControllerPoseUpdater::EndInvoke(System.IAsyncResult)
extern void MotionControllerPoseUpdater_EndInvoke_m23D6AAC9C64E4C50C486F3DFD3747EE08B9D3328 (void);
// 0x00000175 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController::.ctor(Microsoft.MixedReality.Toolkit.TrackingState,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.MixedRealityInteractionMapping[])
extern void SimulatedMotionController__ctor_m35BEDD0F38D96A4BD01D0336D9C0E530FF47A530 (void);
// 0x00000176 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionController::UpdateState(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData)
extern void SimulatedMotionController_UpdateState_m564D23766C867C75978D0F7AD2AF1EC8AADEE961 (void);
// 0x00000177 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::Equals(System.Object)
extern void SimulatedMotionControllerButtonState_Equals_m7A0C3719EB3DE4EAF4E2494CB100D170CF2AFF1F (void);
// 0x00000178 System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::Equals(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_Equals_m93DCA25B3C29E2959854E6C83DFD43E58EA21C2B (void);
// 0x00000179 System.Int32 Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::GetHashCode()
extern void SimulatedMotionControllerButtonState_GetHashCode_m7FA358C68C1A1C2E7D2FD3FB8DDDBA04101BCA09 (void);
// 0x0000017A System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::op_Equality(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_op_Equality_m60A336839EC2B0F104244FB62662121393F40B07 (void);
// 0x0000017B System.Boolean Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState::op_Inequality(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerButtonState_op_Inequality_mEEFE1D2856F995E3AFBBEFB3FDD05011F79F02EE (void);
// 0x0000017C Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::get_ButtonState()
extern void SimulatedMotionControllerState_get_ButtonState_mFC3FCC11AED88A8CEEA87E6299439ABFF60461E5 (void);
// 0x0000017D System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::set_ButtonState(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerButtonState)
extern void SimulatedMotionControllerState_set_ButtonState_m9EE0DB8A94E11AFDC083C7FBD9FC9BE8C05655A7 (void);
// 0x0000017E System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::.ctor(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void SimulatedMotionControllerState__ctor_m5C910FE955162DA3CD18C3DD5A8D3FE3112FE494 (void);
// 0x0000017F System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::ResetRotation()
extern void SimulatedMotionControllerState_ResetRotation_mAF3C750FC8A5A4C15596F02B9E25B79736010186 (void);
// 0x00000180 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::ResetButtonStates()
extern void SimulatedMotionControllerState_ResetButtonStates_mED26F384151490ED6D4646538C028BE8D6A2C1C8 (void);
// 0x00000181 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerState::UpdateControllerPose()
extern void SimulatedMotionControllerState_UpdateControllerPose_m621C4EE9E8F47F050D90670121DDBD77240697D5 (void);
// 0x00000182 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSimulationProfile)
extern void SimulatedMotionControllerDataProvider__ctor_mF98AB0CBD032DF41553E427FEFBC4F32702DB649 (void);
// 0x00000183 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::SimulateInput(System.Int64&,Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean,System.Boolean,Microsoft.MixedReality.Toolkit.Input.MouseDelta,System.Boolean)
extern void SimulatedMotionControllerDataProvider_SimulateInput_m209F00150266ECF24B54828AEDB6AE2F7E4018D9 (void);
// 0x00000184 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::UpdateControllerData(Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData,Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerData,Microsoft.MixedReality.Toolkit.Input.MouseDelta)
extern void SimulatedMotionControllerDataProvider_UpdateControllerData_m792F0B8C74432400F3FCA39AC74A68FE0C8A7A46 (void);
// 0x00000185 System.Void Microsoft.MixedReality.Toolkit.Input.SimulatedMotionControllerDataProvider::ResetInput(Microsoft.MixedReality.Toolkit.Input.SimulatedControllerState,System.Boolean)
extern void SimulatedMotionControllerDataProvider_ResetInput_m49C5891D6AF3CADDA2E68A3FD807C3F4BD7004D5 (void);
static Il2CppMethodPointer s_methodPointers[389] = 
{
	BaseInputSimulationService_GetActiveControllers_m05E8D5AE89F5FD0D08559C4DD577A3DA11EB1C7B,
	BaseInputSimulationService__ctor_m0265D7AEEFA6C5B9CE6210B1E411F9E6CCAF78B0,
	BaseInputSimulationService__ctor_m34196730145BEBC0B2C51C87F1BFA0FB6CF40FEF,
	BaseInputSimulationService_UpdateControllerDevice_m0C30B55C493E8383AA678EFD2824EF5A591C5EEF,
	BaseInputSimulationService_GetControllerDevice_m3936A77EA42F44771402B8EA3DA41122313C6721,
	BaseInputSimulationService_GetOrAddControllerDevice_m224B441ED5213BA6BA57014A020ABCBA66ABBEB5,
	BaseInputSimulationService_RemoveControllerDevice_m5CC7BF04881D631A42483C2DD87E4749CB9D3832,
	BaseInputSimulationService_RemoveAllControllerDevices_m903F4559E986EFEF79A46C46B0DDBDF99077CAEF,
	BaseInputSimulationService_UpdateActiveControllers_m918DE29D57E1292D879B3CC6CA2F61FBC075EC66,
	BaseInputSimulationService_UpdateHandDevice_m55EBC6180BAE62AAA6A3950861DB19C93EE4ECBA,
	BaseInputSimulationService_GetHandDevice_mEBC11B92E6917087F5315FBBAAFF19B52610790A,
	BaseInputSimulationService_GetOrAddHandDevice_m052B36AD2BF800D919496E58A08DC7089C5828AF,
	BaseInputSimulationService_RemoveHandDevice_m03D23B1433F6902E22A6B095EBB0FE8D21DD0CBE,
	BaseInputSimulationService_RemoveAllHandDevices_mE9DFF00F3FEDC9236A25312342FA475414009C9D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InputPlaybackService_add_OnPlaybackStarted_m5AC93F1472BA1BB0FFC382379D546EC9AB339919,
	InputPlaybackService_remove_OnPlaybackStarted_m6962CFEAF53A7D93AC60F6C2EE91BDCD20B49165,
	InputPlaybackService_add_OnPlaybackStopped_mFB31462B8FE233A3621B13E23D04E2B62773BC76,
	InputPlaybackService_remove_OnPlaybackStopped_mFA4829346567F91380EEEAE98A7155A1CF583D93,
	InputPlaybackService_add_OnPlaybackPaused_m7D49B13E2A79CD7B77D6328AC81C16E2CF7F3759,
	InputPlaybackService_remove_OnPlaybackPaused_mA21FACF88619E041515F55F4931DD33D95DEFDD2,
	InputPlaybackService_get_IsPlaying_mA9A61EBADD8A19E2074B7C50B2A68A98B38DCF7F,
	InputPlaybackService_CheckCapability_m0C2D26D5A9F855B911EDF68F1E39642DE5DA5EB4,
	InputPlaybackService_get_SmoothEyeTracking_m341A956D165FF43430EEADBEAC075DBB42DF0AEE,
	InputPlaybackService_set_SmoothEyeTracking_mF1037502FB11DDDF6D81CED18846E37597B08627,
	InputPlaybackService_get_Duration_m22CF2A3E25A6C0A90E7E193CE9F35E6D507FCD12,
	InputPlaybackService_get_LocalTime_m011BF09D0AA6C01D0AF15D920078B97165B29C3E,
	InputPlaybackService_set_LocalTime_m3946AB090F68C0EE00896E9C00FF9384330351A0,
	InputPlaybackService_get_HandDataLeft_m5C9F2DA4A43E0AF618EEF622CDDDA69C2FC1F52A,
	InputPlaybackService_get_HandDataRight_m3BA065CC310290E2715AAA9E273DA54E9BE9B47B,
	InputPlaybackService_get_Animation_m5EF441B3C380F5DE2086AED98EE42C4B71B46AE0,
	InputPlaybackService_set_Animation_m203783DF1D5254992311B6A7D704056E414B5221,
	InputPlaybackService_get_SaccadeProvider_m07E75E291AB4E00AA562D95E3EDBD7D4134643E8,
	InputPlaybackService__ctor_m13DCF985AECDD38573A048B0A959246C5F412E8D,
	InputPlaybackService__ctor_m3CD92FCD2DCB2763A187B947CFC1082B48A49511,
	InputPlaybackService_Play_m060DD8123CA114D803C67EC6BB3568636601D41D,
	InputPlaybackService_Stop_mF30B8E1755BEF17E35FE5B8186C9495B3BC152A8,
	InputPlaybackService_Pause_m2E4B7F02F901B2379EF61223C1B539D5CB41DFA9,
	InputPlaybackService_Update_m474E98D4F8EEBCB26002786C31F58FB1F694980F,
	InputPlaybackService_LoadInputAnimation_m75472EA2A71D0152816CB227B3AD38AE8028556D,
	InputPlaybackService_LoadInputAnimationAsync_m6DDA8A5C5E78746F0CC7987B663CEC70609D69B1,
	InputPlaybackService_Evaluate_mB80A9844EA964DCDBD85A6DA2C476EFFE31BEBC0,
	InputPlaybackService_EvaluateHandData_m7F0AFD19C083A27A1708C26D7D9352FCD6EED644,
	InputPlaybackService_EvaluateEyeGaze_m66A166CC52970E6C370660B3F8ACDC991BDDC896,
	U3CLoadInputAnimationAsyncU3Ed__42_MoveNext_mCB77CB5EBF05BA2DB56E3E94ADF85B71E4A160E0,
	U3CLoadInputAnimationAsyncU3Ed__42_SetStateMachine_m217D1D4DB2AF680882855177E5862454AA89920E,
	U3CU3Ec__DisplayClass44_0__ctor_m58B276C3F93511EE02DD77EAF5297FD54B35D02E,
	U3CU3Ec__DisplayClass44_0_U3CEvaluateHandDataU3Eb__0_m7701FC3B31D63B9FA61E2580EE0AA6E719B2EF3C,
	InputSimulationHelpGuide_Start_mD9A562D7547A69D8890D06476AFE25809346E8DE,
	InputSimulationHelpGuide_Update_m4E3E7888A93ABB70616F4679F5158AADABC6335A,
	InputSimulationHelpGuide__ctor_m2BFAE73AA8F79DA39B2A9275832ED127BF78F865,
	MouseDelta_Reset_mF0C2124433823D1F885660BF3BBED6B7E16E89E5,
	MouseDelta__ctor_m84A7D341A6DE896BE9B59D90B24B6087AA8F83B5,
	InputSimulationService_get_ControllerSimulationMode_m04D121FE5C9BF09F4A0EA953B67B6AA21B3E1E48,
	InputSimulationService_set_ControllerSimulationMode_m7998183B132D860552FBD133355040E93D475054,
	InputSimulationService_get_HandDataLeft_mFF2B211699074B5F55734D9BC468868EC39302DC,
	InputSimulationService_get_HandDataRight_m5A62E12CF2F9A34B2D3B02BEEBEBB456E5BEF10A,
	InputSimulationService_get_HandDataGaze_m0F21C6A87142D483E0FC7C84A62FBB70C58A208A,
	InputSimulationService_get_MotionControllerDataLeft_mBD4DD0C5ADA616189FD0671E38CA4A3AC0D18426,
	InputSimulationService_get_MotionControllerDataRight_m70553ED7AB321C5D29EA07F22E650CDA64451B6F,
	InputSimulationService_get_IsSimulatingControllerLeft_mA52158B4BF2E7102A30CE019A78EF96B5CFBF0BB,
	InputSimulationService_get_IsSimulatingControllerRight_m2CBDAC1318B405D812BF260C9ACF9ABE5612D782,
	InputSimulationService_get_IsAlwaysVisibleControllerLeft_mC0232013CF65271020843D76AFB3AC086EAF3983,
	InputSimulationService_set_IsAlwaysVisibleControllerLeft_mB9C1F76F383BE2A53B26A0D1A988D2D77FA847FA,
	InputSimulationService_get_IsAlwaysVisibleControllerRight_m3003C05499E158A209874AF46C3CC2743C8DB795,
	InputSimulationService_set_IsAlwaysVisibleControllerRight_mB6AC46A1490507DC1941A9BA3268B286A80395E8,
	InputSimulationService_get_ControllerPositionLeft_m726D69DD8A7599642F7F007A2D4AD8CE0BFAC8E8,
	InputSimulationService_set_ControllerPositionLeft_m4ECDB2B43D26A155BDAF0F43ED7ACFE27807D88A,
	InputSimulationService_get_ControllerPositionRight_mED6C4701E21172BC3EC80872A793C0BCBDAAE10C,
	InputSimulationService_set_ControllerPositionRight_mEF735A8BAF734AC5C63B2F754E46BA52D551F77E,
	InputSimulationService_get_ControllerRotationLeft_mA025329C59A1B047864B58007E2C5F06AC5EC7B7,
	InputSimulationService_set_ControllerRotationLeft_mD32E599767967D9AFDAF9AAFEE85B0D8A97310EB,
	InputSimulationService_get_ControllerRotationRight_m870EA3CFC2C6B5770BDB4FFD8EE4F16B135CB1F0,
	InputSimulationService_set_ControllerRotationRight_mD5151366906E4277BFE06F9DA13CFBB22276ACEA,
	InputSimulationService_ResetControllerLeft_m3BBB9678C6C3A2E213A045A2B6DEABF5525B29E0,
	InputSimulationService_ResetControllerRight_mAB880F3E52DCE36C5F6FED160ED14A80CBE0C344,
	InputSimulationService_get_SimulateEyePosition_mBE6F207A8821316CDC53B56DBF49BA43842C408F,
	InputSimulationService_set_SimulateEyePosition_m375509BB2674BAEAE75488B6801AEAD6C5C85C5B,
	InputSimulationService_get_EyeGazeSimulationMode_m1F1A71EC0873AF788AD63D00E96C5C0342BB0EEE,
	InputSimulationService_set_EyeGazeSimulationMode_m518061FE0AF81B935B28BBD12578479BF18601A2,
	InputSimulationService_get_UserInputEnabled_m46B8489814402ED3507CE497A7E90B89CFDF1D1B,
	InputSimulationService_set_UserInputEnabled_m7C678861A4DA6E2970698E24FC262D1BF1B448C8,
	InputSimulationService__ctor_m104C418EB48CA8271185FF549901170FCA53D832,
	InputSimulationService__ctor_m8EEEC849B1992C63269C1C472196DF98A5606CB2,
	InputSimulationService_CheckCapability_mC7DB80017AB2E26CC6534E204E30BE6CDF70D6E1,
	InputSimulationService_Initialize_m4E5E52FB8F274393597B2455B5E7098F775AF99E,
	InputSimulationService_Destroy_m8CD9ABA55140C346F342E43FDEF4F86DC4154D63,
	InputSimulationService_Enable_mB6B5363394ACF73DD42A9AA3E91D77EE4C8AEABE,
	InputSimulationService_Disable_mF4C0ABBBA912FE6C45CBB8DEFF1B9C097B202E72,
	InputSimulationService_Update_mFAEC71E865A740C897FB315569D3CBCFF1D04C60,
	InputSimulationService_LateUpdate_m18404EBA3103A7CCC78942AD20303D5DE4F3D9C4,
	InputSimulationService_get_InputSimulationProfile_mE2845BB4599F11E7DA48E5C09C7F13E8932D0E46,
	InputSimulationService_set_InputSimulationProfile_m63E35784873EB3290E96FE3B4B4887E56CF00BD5,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SaccadeProvider_mDED11ACF9E1667B9231FCC9F86FD9EC9463519D2,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_get_SmoothEyeTracking_m81CD64073FE2BC350E46A5A02208E73510527CC7,
	InputSimulationService_Microsoft_MixedReality_Toolkit_Input_IMixedRealityEyeGazeDataProvider_set_SmoothEyeTracking_m17E4187FA60FD28C171AD57F5FAD094B4A60A3DB,
	InputSimulationService_EnableCameraControl_m41E5F76341BB823F1C8C1953F5D8427831C467AD,
	InputSimulationService_DisableCameraControl_m1FEC84AA8AD4C5A6AB40F88AF883DCAFE5C9631A,
	InputSimulationService_EnableHandSimulation_m50F9C9EB53DDA2BDA1009520F1BF6470A836E48C,
	InputSimulationService_EnableMotionControllerSimulation_m01015D5B4D82CB119E18EA1E88EF4746C4B7E53C,
	InputSimulationService_DisableControllerSimulation_m22E30EBEDEC9628F10909EDB3F1EE6C93109DEA7,
	InputSimulationService_ResetMouseDelta_m8979F414BC45A324AC8BD6C33C0E40166267FA5C,
	InputSimulationService_UpdateMouseDelta_mD41D395AD55F22D7932C104AAD4662920BE51084,
	InputSimulationService_ScreenToWorld_mD7DA2614660E0BC41B27D7351D2DF9603EBB3206,
	InputSimulationService_WorldToScreen_m02EF6C1DD61E08D3B65EC16F4D138874580702E0,
	InputSimulationService_WorldToViewport_mE81057173AF7F94466B1471D17386E11B3B5F449,
	InputSimulationService_get_HandSimulationMode_mF1677CAAFDFA275C49E01C06199704CDC70FA983,
	InputSimulationService_set_HandSimulationMode_mA77585516604C057E3CDD2D93DB3295E95D424F3,
	InputSimulationService_get_IsSimulatingHandLeft_m347BD6FC43318F6C063D71401D400E88C2F86406,
	InputSimulationService_get_IsSimulatingHandRight_m541D9A8C02201D50FCF0D474A9F2FF5D54699B77,
	InputSimulationService_get_IsAlwaysVisibleHandLeft_mB0A3AA67A4803445E3F6F692B9DBA80DC6AAFE68,
	InputSimulationService_set_IsAlwaysVisibleHandLeft_m50DDB62DDE47BF4F1E24770B8D71C81075719BFF,
	InputSimulationService_get_IsAlwaysVisibleHandRight_m839D7B1D552748CD3087D41D369F4FB474966552,
	InputSimulationService_set_IsAlwaysVisibleHandRight_mF83EA39D237CE933A0D962CEF4677E89C95ACFD2,
	InputSimulationService_get_HandPositionLeft_m8E8B38E356DCF75440D54B912C50B9FA1C5AC16E,
	InputSimulationService_set_HandPositionLeft_mBC59A580B1998B35AFDB2BA3411479DCF50AA928,
	InputSimulationService_get_HandPositionRight_m32CA28F0B585858452DB6ED84C0D311A4D0293BB,
	InputSimulationService_set_HandPositionRight_m053C708A25B40FE04CF7F2EC7A1B95BF679A3629,
	InputSimulationService_get_HandRotationLeft_m2AB278FDA86297E4D449FC24EE33F2EFD8431103,
	InputSimulationService_set_HandRotationLeft_m7BD30BDD4A5D5C36984D1566E5BB729150077860,
	InputSimulationService_get_HandRotationRight_m009960BD08F3F6949EE3D5CAFD61B7396F0F39B0,
	InputSimulationService_set_HandRotationRight_m587AD9CA088BFEBCC471CADE5BCCA305B529676B,
	InputSimulationService_ResetHandLeft_m33AF6C61E13A1A3E9FCAA4FB9F2636384C450371,
	InputSimulationService_ResetHandRight_mEF12FB605AFD1CA93116A80BE6EB003D37397360,
	KeyBinding__cctor_m3B6BB968002426A12AA680CAF1C2597736845659,
	KeyBinding_get_BindingType_m7C7EF3E41A13693DDB16E72C138EB0C22C2E7F5E,
	KeyBinding_ToString_m2F76400346E6FB11F289439F5B076A1E443D20E0,
	KeyBinding_TryGetKeyCode_m996EA797CC8F560919DC92E5315D7D6C27CDB8A5,
	KeyBinding_TryGetMouseButton_m6AFB2C2A57C62D1356C08DFC3AA2E21EE7D96988,
	KeyBinding_TryGetMouseButton_mB24709C0C7A9DC5DF21F2950D47BA7865795B35E,
	KeyBinding_Unbound_m738C3BBDDAE557517EDE9E7B02AFAE9574FE3E13,
	KeyBinding_FromKey_m94DEF759DCCFF8CB4A3A80F198DB1E864004E6A1,
	KeyBinding_FromMouseButton_mE5283BA7642A597CDCBFC18C4EE7268F150629CC,
	KeyBinding_FromMouseButton_m15379136513EBB95E9530721D1E5E64FD0234114,
	U3CU3Ec__DisplayClass5_0__ctor_m546E153A2930436656C03AA4E7D634ADCD7A4205,
	U3CU3Ec__DisplayClass5_0_U3C_cctorU3Eb__0_m6D3AE2E8E892009D434A32B0B10C539F331740C2,
	KeyInputSystem_get_SimulatingInput_m30AAA0B95668460F2CE6EE3F9FD89C9E4EE54360,
	KeyInputSystem_StartKeyInputStimulation_mF1E2AE4DDF80852286C25ACA7C7F75EDF1B2C74B,
	KeyInputSystem_StopKeyInputSimulation_m6BF0551272A04F2A7705110A22998397073D0634,
	KeyInputSystem_ResetKeyInputSimulation_mA44CB4548D9BFDC2D86FE8E39548B9E295BB2A5A,
	KeyInputSystem_PressKey_m5FDB43470A2DEE3D7E183A70257D808737B7CB48,
	KeyInputSystem_ReleaseKey_m8B09C804DEAFBD7930E879929856462F9DE416B5,
	KeyInputSystem_AdvanceSimulation_m51F35B119EB6F24E225B0BFD59AB167DC25E7573,
	KeyInputSystem_GetKey_m391F25E4669A02CB118966FB3FFA6C87B18F7384,
	KeyInputSystem_GetKeyDown_m2C6F7C4DEAF7B225A9B2BB8042FA3B0ABD19F89A,
	KeyInputSystem_GetKeyUp_mB361F891B2C6C5DDA2A75729CC28274D3B90E56B,
	ManualCameraControl__ctor_m24B8A8B645FFB7CC701E9CFA3A009EAC4AF48CDE,
	ManualCameraControl_InputCurve_m157A17E6046F2B6CD1A101A8155785A4D694BC99,
	ManualCameraControl_SetInitialTransform_mAE98501ED0867A46762554267F711ACAD73D9EA3,
	ManualCameraControl_UpdateTransform_mB58B00387B45DB252AE4F11054CCBC3F9B93A83E,
	ManualCameraControl_GetKeyDir_m2A266A473C0C2B57D3FEF02B01E10A5D49EF7323,
	ManualCameraControl_GetCameraControlTranslation_mCCE22B2AC69A66376A1002932E1DEAC61F77EFD3,
	ManualCameraControl_GetCameraControlRotation_mBD73E47397B9ACD2614C74D683F0D53FCF312FA8,
	ManualCameraControl__cctor_mB505EB226EAC24A0C8E2751C6E71A339CE430538,
	MixedRealityInputSimulationProfile_get_IndicatorsPrefab_m9282C93F8F400A5D693319F339D613236CA0FB4E,
	MixedRealityInputSimulationProfile_get_MouseRotationSensitivity_mD3C9B3113E93A402B025B20D76437B81336FF3CE,
	MixedRealityInputSimulationProfile_get_MouseX_m899FCA700D0E04763779EA4C697D16F047523C05,
	MixedRealityInputSimulationProfile_get_MouseY_m10BD42099B7C90463DC71BF0D3B7D360EBFB4E23,
	MixedRealityInputSimulationProfile_get_MouseScroll_m3AC4206CA14713636D3218EFE137A0C49267E991,
	MixedRealityInputSimulationProfile_get_InteractionButton_mC81E84A643354A93077DC9F764B4CFC77701A9B2,
	MixedRealityInputSimulationProfile_get_DoublePressTime_m24E4AFDFC2394F0647BA43454C837B6ED89549EC,
	MixedRealityInputSimulationProfile_get_IsHandsFreeInputEnabled_mE517EE789BE07FB284C6943A3C7356CA0CA3E227,
	MixedRealityInputSimulationProfile_get_IsCameraControlEnabled_m2D44C87D0FAA6FB0E975C903C3A7343AFD5310A8,
	MixedRealityInputSimulationProfile_get_MouseLookSpeed_mF9212C1AC3F0D73D365BCF8C72020572FC2A228D,
	MixedRealityInputSimulationProfile_get_MouseLookButton_mD873055273103F1AFC501DC459B3B49D896E7B91,
	MixedRealityInputSimulationProfile_get_MouseLookToggle_m6CA2017345631674A820AD6E864659BC40A2AFE1,
	MixedRealityInputSimulationProfile_get_IsControllerLookInverted_m84A6C879D9B10702ACB860AE5B09426F461C80A0,
	MixedRealityInputSimulationProfile_get_CameraOriginOffset_mCF94D7139F4CEB911FA7BEC42C7C3E3AC582D094,
	MixedRealityInputSimulationProfile_get_CurrentControlMode_m2170CB0167713942A79D5D710E3C0D257A2ABFFD,
	MixedRealityInputSimulationProfile_get_FastControlKey_mA9859A7E72A6E0FCC29378FF747F73CA3ACC33C2,
	MixedRealityInputSimulationProfile_get_ControlSlowSpeed_mFD8FE5ED589A2BB0B083FC6564DA74553088A1E5,
	MixedRealityInputSimulationProfile_get_ControlFastSpeed_mF7DF539E0D370D0816E2FA9421BF06823BC9689D,
	MixedRealityInputSimulationProfile_get_MoveHorizontal_mB6BF339C7E1DD329602F010F6E0AE521C8F90A5E,
	MixedRealityInputSimulationProfile_get_MoveVertical_m7281AC26D862C0E6DB1BEB3095C51491905FBE58,
	MixedRealityInputSimulationProfile_get_MoveUpDown_mBAFE4AA37C858816965DD2C1BDD74BBD4B0C35BB,
	MixedRealityInputSimulationProfile_get_LookHorizontal_mF49ACE9F1236152B23D757EEC44F40F41D697209,
	MixedRealityInputSimulationProfile_get_LookVertical_m07B4C975068FE6E543C2FAF0B62E3305B42EB921,
	MixedRealityInputSimulationProfile_get_SimulateEyePosition_m07171E1640B46580C86E949A30C08C5C89324A70,
	MixedRealityInputSimulationProfile_get_DefaultEyeGazeSimulationMode_m1A6296AB187208634879A3DFC393F6F86EE9C141,
	MixedRealityInputSimulationProfile_get_DefaultControllerSimulationMode_m90ECFC92C9AD7CAAC9F9A0663A6CD10372BDE2D1,
	MixedRealityInputSimulationProfile_get_ToggleLeftControllerKey_m708147C69748F10AAA94557F174E0997CA5E8192,
	MixedRealityInputSimulationProfile_get_ToggleRightControllerKey_mD9AD2C0CCE10DDB2CFDDA472DAD0D779CA07CFA4,
	MixedRealityInputSimulationProfile_get_ControllerHideTimeout_m22EB5A286D955023FEAD704579F508CE41363715,
	MixedRealityInputSimulationProfile_get_LeftControllerManipulationKey_mBE036948026D69856389BB8FD47FA4834EC9BF22,
	MixedRealityInputSimulationProfile_get_RightControllerManipulationKey_m225CEDE48D195E6CDF9DA737C9EB76EAEFB8F573,
	MixedRealityInputSimulationProfile_get_MouseControllerRotationSpeed_mC147D5238186F08DE50F00350DF4849223783265,
	MixedRealityInputSimulationProfile_get_ControllerRotateButton_m1F87F19A2CCFE4E8C7C2012A5C1D1EA9B0630163,
	MixedRealityInputSimulationProfile_get_DefaultHandGesture_m9BC3502D42BD31BE03566F9FFACD849E24C5E453,
	MixedRealityInputSimulationProfile_get_LeftMouseHandGesture_m7C1A34A3EA6A15BFC1FD08ECA22D74F22404F336,
	MixedRealityInputSimulationProfile_get_MiddleMouseHandGesture_m24C2D088D2794BEAB5D3AA56404D4285A9101FA4,
	MixedRealityInputSimulationProfile_get_RightMouseHandGesture_m3B63561925E28FD6F0D87994A21158CFAB061B9F,
	MixedRealityInputSimulationProfile_get_HandGestureAnimationSpeed_mED8155B476ED6415422CECF27B44671EA6A9C955,
	MixedRealityInputSimulationProfile_get_HoldStartDuration_m2D17F9FF0C1C8BE61307C381BD32A3CD5B6562FC,
	MixedRealityInputSimulationProfile_get_NavigationStartThreshold_mBF0A07520AF83CA39398618F7E3BC2296E0DC035,
	MixedRealityInputSimulationProfile_get_DefaultControllerDistance_mA521FA318F87200C8D2C2BAC74BA165DFF927093,
	MixedRealityInputSimulationProfile_get_ControllerDepthMultiplier_m7E9EFF1DD822343AB0A08095AD4DEE210AE33BA8,
	MixedRealityInputSimulationProfile_get_ControllerJitterAmount_m31F0BC01A21457E24B4BD97C69011178A977496C,
	MixedRealityInputSimulationProfile_get_MotionControllerTriggerKey_m9634B8D226C7D26B1E6C798CE0984F2E35858BB5,
	MixedRealityInputSimulationProfile_get_MotionControllerGrabKey_m341958DAF88AC2E76F38BC67323DBB69F90352A5,
	MixedRealityInputSimulationProfile_get_MotionControllerMenuKey_m56F1580EDD94F5958CE761A3B640F11C91960139,
	MixedRealityInputSimulationProfile_get_DefaultHandSimulationMode_mDDAF4222DFAA34D4882D67FF170FDB0B13F074C8,
	MixedRealityInputSimulationProfile_get_ToggleLeftHandKey_m42CD0D69BB6E326B5FB45D8B3A31E7273D723334,
	MixedRealityInputSimulationProfile_get_ToggleRightHandKey_m6063381798DEAE17E29D20F11872082CF887991F,
	MixedRealityInputSimulationProfile_get_HandHideTimeout_m6C7AEE62989485E76F1BA9207DE04E61E21F85F7,
	MixedRealityInputSimulationProfile_get_LeftHandManipulationKey_m79A2154D03305AA19E2182907A560C11EEFE4C34,
	MixedRealityInputSimulationProfile_get_RightHandManipulationKey_mD1CE98933BC5FC6F1D4B5734B1DD380416982F24,
	MixedRealityInputSimulationProfile_get_MouseHandRotationSpeed_m77E30CD0B3F89F8EF90619A7A3CAD9236FDC2740,
	MixedRealityInputSimulationProfile_get_HandRotateButton_m4938D0427B31BD1FB4FE9DFE1E8A47D1825B0021,
	MixedRealityInputSimulationProfile_get_DefaultHandDistance_m9417C04C375CE73A16FB7B62E1BEE1392E2A6D03,
	MixedRealityInputSimulationProfile_get_HandDepthMultiplier_mD6417E98994A6F06D744384431479E7C4360CD27,
	MixedRealityInputSimulationProfile_get_HandJitterAmount_m32FBC40E5E94E04A670F9936A3BD43D4548A816D,
	MixedRealityInputSimulationProfile__ctor_m264E684D38DDA516D89D5F2E29339D58A6AFFFDB,
	MouseRotationProvider_get_IsRotating_mF171085CA0A90AC9EC2CEBFB35F23BC9B5761E22,
	MouseRotationProvider_Update_mF1119040C324C1D10166BE9E11012C800700B77C,
	MouseRotationProvider_OnStartRotating_m9B33BD0C0A8922CE86A193A33AE3A95CEC9C269B,
	MouseRotationProvider_OnEndRotating_mE9B806A7740625A38D671E00B7DBC4A851414A04,
	MouseRotationProvider_SetWantsMouseJumping_mE63564E3692CB60F86C5947008F04E6A58434440,
	MouseRotationProvider__ctor_mEAA8FF65CBC5C2A280B73FD5D2C63182CE115914,
	MouseRotationProvider__cctor_m4B24C0ED50B3F0459B9BF31A070E9B314D40243D,
	SimulatedArticulatedHand_get_SimulationMode_mF90E4B9592AE080FC902CB3CCC04E70FFF0C1D5C,
	SimulatedArticulatedHand__ctor_m58D26C00ACB816BFD48E90A8D6B45E05B415B281,
	SimulatedArticulatedHand_UpdateHandJoints_mEAAEB96A29DE36DF45C10DF8F0B5234E762ED91F,
	SimulatedArticulatedHand_UpdateInteractions_mECC62222A95D77839C9D5432282FBF46AB63E946,
	SimulatedArticulatedHandPoses_GetGesturePose_m0487A43A9189DCA5AAD562F8CBE35FC4A99E2819,
	SimulatedArticulatedHandPoses_SetGesturePose_m85C4E5745AA1B852DE9C5F8600965E6EDCFFC0AA,
	SimulatedArticulatedHandPoses_LoadDefaultGesturePoses_m31925DB7949B93687613228AEE80DBBBA14A9502,
	SimulatedArticulatedHandPoses__cctor_m63422650D9BFCE5E59A9342DAAD7C5A5E333359C,
	SimulatedControllerState_get_Handedness_m844F4189E08977F2312876A1108AD041B56248BD,
	SimulatedControllerState__ctor_mB12E801723385B4CF8D31CC2B38E1BE631031047,
	SimulatedControllerState_SimulateInput_m1E7AB6B66D768AC1F89A0A2AADD458EFC0079A74,
	SimulatedControllerState_ResetPosition_m14FBDF8FA01D27DF584323C3503BB302E38E6A28,
	NULL,
	SimulatedControllerState_Update_m98D6F91DAA77FB47ED04E46E7E2676518F278F29,
	SimulatedControllerDataProvider_get_isSimulatingGaze_m9D4989666F01478204337ABA85F92200274E34C8,
	SimulatedControllerDataProvider_get_IsSimulatingLeft_m55621674AEBD8451E546D461D0566FCCE6A9B80D,
	SimulatedControllerDataProvider_set_IsSimulatingLeft_m7101DC35494A82C8743A98CD1E2AD28BB68903B3,
	SimulatedControllerDataProvider_get_IsSimulatingRight_mE78856E27F73BC4846D085D71C96D53C903C7668,
	SimulatedControllerDataProvider_set_IsSimulatingRight_m0DE698BE7E9A49A0F5BBDDF0BDB7DC18509B548F,
	SimulatedControllerDataProvider__ctor_mF75C3F2B65B1EEF8127FDDB3F237FBAD41986E5F,
	SimulatedControllerDataProvider_SimulateUserInput_mD8BA571C883E2154A59FA18C48FF5B6686DBDD28,
	NULL,
	SimulatedControllerDataProvider_ResetInput_m8B96EC4613A315F0546211F120D265AAB1188F99,
	SimulatedControllerDataProvider_ResetInput_m59B1AB1CDA7193F0436665150161E4CDBCFEAEA9,
	SimulatedControllerDataProvider__cctor_m752094A4375FB847EF6DDF54D99126FB3E7D975B,
	SimulatedGestureHand_get_SimulationMode_m488387D3F1AB9FAD1D6485F29733ECD6749DED51,
	SimulatedGestureHand_get_NavigationDelta_m13DE38F4CA58300DBB5E5405B809334779D8DC89,
	SimulatedGestureHand__ctor_mD0262912635E115B68A6F0797460216E49BD1861,
	SimulatedGestureHand_EnsureProfileSettings_m8D677E39710B7C958A0410F7E05D363D904EAEC5,
	SimulatedGestureHand_UpdateHandJoints_m38FFF531652CF0B161E6076E64FCE99C89FAE187,
	SimulatedGestureHand_UpdateInteractions_mD10DDF592D2CA83767B0D2E3D664E07DA6F40722,
	SimulatedGestureHand_TryStartHold_m14CCA7964C45EABAF322F4D3F49190C1A6BE0913,
	SimulatedGestureHand_TryCompleteHold_m842AD31A4C883187D1F22234A5E6B542573B6A49,
	SimulatedGestureHand_TryCancelHold_m31063521F4E96FA0653DE89D812321048267EF47,
	SimulatedGestureHand_TryStartManipulation_m38BB3BCBBD27D2AC37B9898B7289DA4613093373,
	SimulatedGestureHand_UpdateManipulation_m87A993A399B69B531D308580F54874A39D30F824,
	SimulatedGestureHand_TryCompleteManipulation_mA573A32CAB1735DCB6B6EEC9F6E03742BBDC07BC,
	SimulatedGestureHand_TryCancelManipulation_m514E3AB32666DD10BD1E876B313B059D72DDBD60,
	SimulatedGestureHand_TryCompleteSelect_m2732AB094743A329DF2341548D1F2EE28E84756C,
	SimulatedGestureHand_TryStartNavigation_mF1EABCB39A28C61D7B54D21F3605DE4CD28F852D,
	SimulatedGestureHand_UpdateNavigation_mD966BE3A885D08FC9A4567723D6DCBBBE8946420,
	SimulatedGestureHand_TryCompleteNavigation_m9566E95CB70F59BFC869F0A6356D980C56D1676F,
	SimulatedGestureHand_TryCancelNavigation_m60E0004951F6543F7420402F706A8D9EACB61C9E,
	SimulatedGestureHand_UpdateNavigationRails_mF12FD740A6AC7D063CB9B8ADA4F1F6CA624D3014,
	SimulatedHandData_get_IsTracked_m7D9D2348F5D0AC0EAB28B1D4772ABC932EE904BB,
	SimulatedHandData_get_Joints_m7F946E75420EB197B9D4CDAF189FC0C0D31E9395,
	SimulatedHandData_get_IsPinching_m456058425802EB1F87C4D3BB00B7A0C5F7AE8BA0,
	SimulatedHandData_Copy_m76A5E2C62D05FC9AF412DA935231AAB45F2DEE93,
	SimulatedHandData_Update_m78CDDE5129B7AA2FEE7DA7CB269255709E24F403,
	SimulatedHandData__ctor_m16E35E38836CA52E125475C7A3C1B86E3C9EF383,
	HandJointDataGenerator__ctor_m506C42E4C1BD1A7B867BED83CBE1C31105970905,
	HandJointDataGenerator_Invoke_mEE8BB7423D473BCBD0C1702F6E3B300C4C5BFB12,
	HandJointDataGenerator_BeginInvoke_mFCE59FC7B891B18FC581F56C62873660F7CEF52B,
	HandJointDataGenerator_EndInvoke_m7FDF675A9C4F07855B3D595ADFA6B6CACA3EF247,
	NULL,
	SimulatedHand__ctor_m45933026E7DCA4DBCDE6DD4C7282E6CB3985A3F0,
	SimulatedHand_TryGetJoint_mDC4EE8F6905F634CFF4F998E70C6F52A4AAF5F20,
	SimulatedHand_UpdateState_m9BAD7E64923D90CCA01C91BB3F7F1489314FC33F,
	NULL,
	NULL,
	SimulatedHandState_get_IsPinching_m0C619C2F1214BBC8C9ACAA24E11F1161CE7DF6E3,
	SimulatedHandState_get_Gesture_m5F76C235A87EAA5D5A3763465D0F6CC9A177D4F7,
	SimulatedHandState_set_Gesture_m3B54BFEC8EDAA952A1C2E02F75B5314DA653AFA9,
	SimulatedHandState_get_GestureBlending_mFC11DDE0D307ADD1DB402B96055AA7EA199067E9,
	SimulatedHandState_set_GestureBlending_mEFDE049EC403736DDF35AA5F37AE0F2ACF66896C,
	SimulatedHandState__ctor_m1F0CB1B090E1FB9073C9FB8640AA55086B550FC7,
	SimulatedHandState_ResetGesture_m11506C99F82141CD0E81B41167337F49C68655BC,
	SimulatedHandState_ResetRotation_mFF357C326CE0569E53561C282B4B462641B8605E,
	SimulatedHandState_FillCurrentFrame_m04FABD6FAFFBA686EFB338A6486E79F3A9295C1A,
	SimulatedHandDataProvider__ctor_mD8260D4CCAE93AB7E9BB17554BE10439DEBB0910,
	SimulatedHandDataProvider_UpdateHandData_mCE40A949263B72A3BC1B082463886E4F8CA726A8,
	SimulatedHandDataProvider_SimulateUserInput_mF88D9FE33CB48A66942FF3FC440906B146A7E24D,
	SimulatedHandDataProvider_SimulateInput_mEB7EB7A8C1B8620CC92976AF89CBADBFB4784C8D,
	SimulatedHandDataProvider_ResetInput_mAADDCADDA2F79F34EB6FE25329077C1C1F37BD8C,
	SimulatedHandDataProvider_SelectGesture_mE774ECF1F9F8EAD4A5C78F23F33EC5BA9C7092FB,
	SimulatedHandDataProvider_ToggleGesture_m4E6E42897C9EAA552A647D15CB135D8C4AD837BB,
	SimulatedHandDataProvider_get_HandStateLeft_mF8A3808F2390D352CA80BD2E2242339433710CBD,
	SimulatedHandDataProvider_set_HandStateLeft_m48FE8012A82CC0A3512C21887223068489AC6B49,
	SimulatedHandDataProvider_get_HandStateRight_mC8CC7DD6E8597B0C6E6C4442CE4C825D5253C3A5,
	SimulatedHandDataProvider_set_HandStateRight_mDFF932289369DE32EE5C21C2615A39618E08DB47,
	SimulatedHandDataProvider_get_HandStateGaze_mDD70B4F9C467C8DFA688A3C67C3DABA563B6D244,
	SimulatedHandDataProvider_set_HandStateGaze_m2DCC3D93C9F20E284F954155E4D56908AC0E3A39,
	SimulatedHandUtils_CalculateJointRotations_mDCE723FF3E3A21ED95AA2AEC5E0D179C0F1A890D,
	SimulatedHandUtils_GetPalmForwardVector_mAD80A1144F3FA499C205871629A85F875137EBB7,
	SimulatedHandUtils_GetPalmUpVector_mBBFC4E357E8C7EF7C71DBCFF54D7D510ABDA07B1,
	SimulatedHandUtils_GetPalmRightVector_m8222C68806F9B85395E14894EBC043FF45385450,
	SimulatedHandUtils__ctor_m42453478DCEBCA8636A816B16CC7B352076760F2,
	SimulatedMotionControllerData_get_IsTracked_mA8302F34C478CD4315163A2A110E4289A461F500,
	SimulatedMotionControllerData_get_ButtonState_m69D9DF5C12895C0780394196B20CF2BE5A597601,
	SimulatedMotionControllerData_get_Position_mA828A96EF7583F38D94BE5D5187F7F97E7D649F5,
	SimulatedMotionControllerData_set_Position_m316893C2EFE530EFDCB0D9C0A0EF3D9D54B63FF6,
	SimulatedMotionControllerData_get_Rotation_m72A7B603281FEEC0D475F21FB558C30A03C2DE6D,
	SimulatedMotionControllerData_set_Rotation_m164B6D5F54D298F093E9EA846D9705E7DA34C87E,
	SimulatedMotionControllerData_Update_mFC06CDB3124E91A33FA1D830334F60BD81EE7646,
	SimulatedMotionControllerData__ctor_m356429BE25B7C968DD5CB29CB29D47A50386F387,
	MotionControllerPoseUpdater__ctor_m3DF94AD6C1C6FA5DC44E695956D0017516295CA0,
	MotionControllerPoseUpdater_Invoke_mA982C7ED520192FB18029758B577A6CFD744AF05,
	MotionControllerPoseUpdater_BeginInvoke_m2572D069E9B7B98BA93D732D0ABAE3AED189B10D,
	MotionControllerPoseUpdater_EndInvoke_m23D6AAC9C64E4C50C486F3DFD3747EE08B9D3328,
	SimulatedMotionController__ctor_m35BEDD0F38D96A4BD01D0336D9C0E530FF47A530,
	SimulatedMotionController_UpdateState_m564D23766C867C75978D0F7AD2AF1EC8AADEE961,
	SimulatedMotionControllerButtonState_Equals_m7A0C3719EB3DE4EAF4E2494CB100D170CF2AFF1F,
	SimulatedMotionControllerButtonState_Equals_m93DCA25B3C29E2959854E6C83DFD43E58EA21C2B,
	SimulatedMotionControllerButtonState_GetHashCode_m7FA358C68C1A1C2E7D2FD3FB8DDDBA04101BCA09,
	SimulatedMotionControllerButtonState_op_Equality_m60A336839EC2B0F104244FB62662121393F40B07,
	SimulatedMotionControllerButtonState_op_Inequality_mEEFE1D2856F995E3AFBBEFB3FDD05011F79F02EE,
	SimulatedMotionControllerState_get_ButtonState_mFC3FCC11AED88A8CEEA87E6299439ABFF60461E5,
	SimulatedMotionControllerState_set_ButtonState_m9EE0DB8A94E11AFDC083C7FBD9FC9BE8C05655A7,
	SimulatedMotionControllerState__ctor_m5C910FE955162DA3CD18C3DD5A8D3FE3112FE494,
	SimulatedMotionControllerState_ResetRotation_mAF3C750FC8A5A4C15596F02B9E25B79736010186,
	SimulatedMotionControllerState_ResetButtonStates_mED26F384151490ED6D4646538C028BE8D6A2C1C8,
	SimulatedMotionControllerState_UpdateControllerPose_m621C4EE9E8F47F050D90670121DDBD77240697D5,
	SimulatedMotionControllerDataProvider__ctor_mF98AB0CBD032DF41553E427FEFBC4F32702DB649,
	SimulatedMotionControllerDataProvider_SimulateInput_m209F00150266ECF24B54828AEDB6AE2F7E4018D9,
	SimulatedMotionControllerDataProvider_UpdateControllerData_m792F0B8C74432400F3FCA39AC74A68FE0C8A7A46,
	SimulatedMotionControllerDataProvider_ResetInput_m49C5891D6AF3CADDA2E68A3FD807C3F4BD7004D5,
};
extern void U3CLoadInputAnimationAsyncU3Ed__42_MoveNext_mCB77CB5EBF05BA2DB56E3E94ADF85B71E4A160E0_AdjustorThunk (void);
extern void U3CLoadInputAnimationAsyncU3Ed__42_SetStateMachine_m217D1D4DB2AF680882855177E5862454AA89920E_AdjustorThunk (void);
extern void KeyBinding_get_BindingType_m7C7EF3E41A13693DDB16E72C138EB0C22C2E7F5E_AdjustorThunk (void);
extern void KeyBinding_ToString_m2F76400346E6FB11F289439F5B076A1E443D20E0_AdjustorThunk (void);
extern void KeyBinding_TryGetKeyCode_m996EA797CC8F560919DC92E5315D7D6C27CDB8A5_AdjustorThunk (void);
extern void KeyBinding_TryGetMouseButton_m6AFB2C2A57C62D1356C08DFC3AA2E21EE7D96988_AdjustorThunk (void);
extern void KeyBinding_TryGetMouseButton_mB24709C0C7A9DC5DF21F2950D47BA7865795B35E_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_Equals_m7A0C3719EB3DE4EAF4E2494CB100D170CF2AFF1F_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_Equals_m93DCA25B3C29E2959854E6C83DFD43E58EA21C2B_AdjustorThunk (void);
extern void SimulatedMotionControllerButtonState_GetHashCode_m7FA358C68C1A1C2E7D2FD3FB8DDDBA04101BCA09_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[10] = 
{
	{ 0x06000063, U3CLoadInputAnimationAsyncU3Ed__42_MoveNext_mCB77CB5EBF05BA2DB56E3E94ADF85B71E4A160E0_AdjustorThunk },
	{ 0x06000064, U3CLoadInputAnimationAsyncU3Ed__42_SetStateMachine_m217D1D4DB2AF680882855177E5862454AA89920E_AdjustorThunk },
	{ 0x060000B4, KeyBinding_get_BindingType_m7C7EF3E41A13693DDB16E72C138EB0C22C2E7F5E_AdjustorThunk },
	{ 0x060000B5, KeyBinding_ToString_m2F76400346E6FB11F289439F5B076A1E443D20E0_AdjustorThunk },
	{ 0x060000B6, KeyBinding_TryGetKeyCode_m996EA797CC8F560919DC92E5315D7D6C27CDB8A5_AdjustorThunk },
	{ 0x060000B7, KeyBinding_TryGetMouseButton_m6AFB2C2A57C62D1356C08DFC3AA2E21EE7D96988_AdjustorThunk },
	{ 0x060000B8, KeyBinding_TryGetMouseButton_mB24709C0C7A9DC5DF21F2950D47BA7865795B35E_AdjustorThunk },
	{ 0x06000177, SimulatedMotionControllerButtonState_Equals_m7A0C3719EB3DE4EAF4E2494CB100D170CF2AFF1F_AdjustorThunk },
	{ 0x06000178, SimulatedMotionControllerButtonState_Equals_m93DCA25B3C29E2959854E6C83DFD43E58EA21C2B_AdjustorThunk },
	{ 0x06000179, SimulatedMotionControllerButtonState_GetHashCode_m7FA358C68C1A1C2E7D2FD3FB8DDDBA04101BCA09_AdjustorThunk },
};
static const int32_t s_InvokerIndices[389] = 
{
	14,
	1292,
	1269,
	632,
	130,
	2689,
	31,
	23,
	23,
	632,
	130,
	2689,
	31,
	23,
	14,
	10,
	32,
	10,
	32,
	14,
	14,
	14,
	14,
	89,
	31,
	89,
	89,
	89,
	31,
	89,
	31,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	23,
	23,
	10,
	32,
	89,
	89,
	89,
	31,
	89,
	31,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	23,
	23,
	14,
	26,
	89,
	726,
	337,
	23,
	23,
	23,
	9,
	28,
	26,
	26,
	26,
	26,
	26,
	26,
	89,
	30,
	89,
	31,
	726,
	726,
	337,
	14,
	14,
	14,
	26,
	14,
	1292,
	1269,
	23,
	23,
	23,
	23,
	9,
	28,
	23,
	102,
	23,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	14,
	14,
	14,
	14,
	14,
	89,
	89,
	89,
	31,
	89,
	31,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	23,
	23,
	89,
	31,
	10,
	32,
	89,
	31,
	1292,
	1269,
	30,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	14,
	89,
	31,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	3051,
	2167,
	2167,
	10,
	32,
	89,
	89,
	89,
	31,
	89,
	31,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	1379,
	1380,
	23,
	23,
	3,
	10,
	14,
	840,
	840,
	840,
	3248,
	3249,
	3249,
	3249,
	23,
	136,
	49,
	3,
	3,
	3,
	3250,
	3250,
	3,
	3251,
	3251,
	3251,
	26,
	280,
	26,
	27,
	275,
	2526,
	2526,
	3,
	14,
	726,
	14,
	14,
	14,
	3252,
	726,
	89,
	89,
	726,
	3252,
	89,
	89,
	1379,
	10,
	3252,
	726,
	726,
	14,
	14,
	14,
	14,
	14,
	89,
	10,
	10,
	3252,
	3252,
	726,
	3252,
	3252,
	726,
	3252,
	10,
	10,
	10,
	10,
	726,
	726,
	726,
	726,
	726,
	726,
	3252,
	3252,
	3252,
	10,
	3252,
	3252,
	726,
	3252,
	3252,
	726,
	3252,
	726,
	726,
	726,
	23,
	89,
	3253,
	3250,
	3250,
	848,
	23,
	3,
	10,
	2555,
	26,
	26,
	43,
	575,
	3,
	3,
	89,
	31,
	3254,
	1380,
	23,
	23,
	89,
	89,
	31,
	89,
	31,
	26,
	26,
	3255,
	31,
	102,
	3,
	10,
	1379,
	2555,
	23,
	26,
	26,
	89,
	89,
	89,
	89,
	23,
	89,
	89,
	89,
	89,
	23,
	89,
	89,
	23,
	89,
	14,
	89,
	26,
	3256,
	23,
	131,
	26,
	218,
	26,
	10,
	2550,
	1774,
	26,
	26,
	26,
	89,
	10,
	32,
	726,
	337,
	31,
	23,
	23,
	26,
	26,
	979,
	26,
	3255,
	102,
	10,
	37,
	14,
	26,
	14,
	26,
	14,
	26,
	3257,
	2358,
	3258,
	3258,
	23,
	89,
	3259,
	1379,
	1380,
	1552,
	1655,
	3260,
	23,
	131,
	2407,
	114,
	3261,
	2555,
	26,
	9,
	3262,
	10,
	3263,
	3263,
	3259,
	3264,
	31,
	23,
	23,
	2407,
	26,
	3255,
	212,
	102,
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSimulationCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSimulationCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.InputSimulation.dll",
	389,
	s_methodPointers,
	10,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
