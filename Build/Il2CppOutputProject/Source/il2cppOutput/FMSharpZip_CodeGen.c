﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Byte[] FMZipHelper::FMZipBytes(System.Byte[])
extern void FMZipHelper_FMZipBytes_m89FC46CD06FD9F5B911D8E4177F4A5B5A558B2EC (void);
// 0x00000002 System.Byte[] FMZipHelper::FMUnzipBytes(System.Byte[])
extern void FMZipHelper_FMUnzipBytes_m73843117F77A036B36568A3C8905CD16A5622CA1 (void);
// 0x00000003 System.Byte[] FMZipHelper::ReadAllBytes(System.IO.BinaryReader)
extern void FMZipHelper_ReadAllBytes_m63D2211F37608D05AB2DC43197D12E204E1BBD79 (void);
// 0x00000004 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor()
extern void StreamDecodingException__ctor_m3861C591A50B13FB0EFB4216CE6B59FB7A07834B (void);
// 0x00000005 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.String)
extern void StreamDecodingException__ctor_m4EB604F7593D48437D457774B993B183450E3931 (void);
// 0x00000006 System.Void FMETP.SharpZipLib.StreamDecodingException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void StreamDecodingException__ctor_m7514AA294B9F595D78AB796AF7C32E1F6354A74F (void);
// 0x00000007 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.String)
extern void ValueOutOfRangeException__ctor_m06D85F8BD146772ED317C82BC36EE3AD1978BDA3 (void);
// 0x00000008 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor()
extern void ValueOutOfRangeException__ctor_m117C299C94190456E4243AA8DBA981EA90C2972E (void);
// 0x00000009 System.Void FMETP.SharpZipLib.ValueOutOfRangeException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void ValueOutOfRangeException__ctor_m37E4CBDE576A56A968D1ED054EA51C584276CFBC (void);
// 0x0000000A System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor()
extern void SharpZipBaseException__ctor_m0C3DF8DAA7E98949C614960F475930F20866C734 (void);
// 0x0000000B System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.String)
extern void SharpZipBaseException__ctor_m46F5B8EEEFDC1927B5A86D31309F3C4144AB3795 (void);
// 0x0000000C System.Void FMETP.SharpZipLib.SharpZipBaseException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void SharpZipBaseException__ctor_mCBA216AB2CE7912C2CDD8B7ECF7486E59A80F1BD (void);
// 0x0000000D System.Void FMETP.SharpZipLib.Checksum.Crc32::.ctor()
extern void Crc32__ctor_m4DDB0237065AAFF5D3DC0A000FA9F913D4E83876 (void);
// 0x0000000E System.Void FMETP.SharpZipLib.Checksum.Crc32::Reset()
extern void Crc32_Reset_m75B0BD8D3F613C47DEF357C8850511CC87223E2B (void);
// 0x0000000F System.Int64 FMETP.SharpZipLib.Checksum.Crc32::get_Value()
extern void Crc32_get_Value_m79C0A69FED134B671369FF7A70C4D13D1B8EC97D (void);
// 0x00000010 System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.Int32)
extern void Crc32_Update_m17688C34F507767331AB6E56AB317DDBEFB724E0 (void);
// 0x00000011 System.Void FMETP.SharpZipLib.Checksum.Crc32::Update(System.ArraySegment`1<System.Byte>)
extern void Crc32_Update_m3E20582F35DFC3959430B3CD93C821B0285636E7 (void);
// 0x00000012 System.Void FMETP.SharpZipLib.Checksum.Crc32::.cctor()
extern void Crc32__cctor_m084ABD60B52D8CEE12FF21852DDFE69C3CF12839 (void);
// 0x00000013 System.Void FMETP.SharpZipLib.Checksum.Adler32::.ctor()
extern void Adler32__ctor_m75E5243E681A76C932F3A9DC3AB91C117DCB344C (void);
// 0x00000014 System.Void FMETP.SharpZipLib.Checksum.Adler32::Reset()
extern void Adler32_Reset_m1251AE8E4EE59A4A70BF08FC44F1A51DB75D8481 (void);
// 0x00000015 System.Int64 FMETP.SharpZipLib.Checksum.Adler32::get_Value()
extern void Adler32_get_Value_m02426228A87B1427781CB2E65CA07BC6B94FE01F (void);
// 0x00000016 System.Void FMETP.SharpZipLib.Checksum.Adler32::Update(System.ArraySegment`1<System.Byte>)
extern void Adler32_Update_mF1C9538E4D88BC157A70F6991DCA5C13FCF67319 (void);
// 0x00000017 System.Void FMETP.SharpZipLib.Checksum.Adler32::.cctor()
extern void Adler32__cctor_mABF2B8CF4F0483A100ADB381B003D64FEAFFC7DA (void);
// 0x00000018 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::AttemptRead()
extern void InflaterDynHeader_AttemptRead_mE56BBEAD91A58D6AB5069C41421DCE84CE7FA11F (void);
// 0x00000019 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.ctor(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterDynHeader__ctor_m02FBB790871BA51C12164D7EE107D680BF0F05B2 (void);
// 0x0000001A System.Collections.Generic.IEnumerable`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::CreateStateMachine()
extern void InflaterDynHeader_CreateStateMachine_m8BE79D7E4056521B4C92F58B1A796D34955A6FC1 (void);
// 0x0000001B FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_LiteralLengthTree()
extern void InflaterDynHeader_get_LiteralLengthTree_m194F2D1B08475A7F53A240306641123B9A493C8D (void);
// 0x0000001C FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::get_DistanceTree()
extern void InflaterDynHeader_get_DistanceTree_mACBCD5B581B4EF1C7022F964438B53EC7ED13DCF (void);
// 0x0000001D System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader::.cctor()
extern void InflaterDynHeader__cctor_m5D8E774E76340751FBAA7FB662FF93D675090DC7 (void);
// 0x0000001E System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::.ctor(System.Int32)
extern void U3CCreateStateMachineU3Ed__7__ctor_mCA17E25DBEDA1754081C1BAB0497DA8A85FC8A27 (void);
// 0x0000001F System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.IDisposable.Dispose()
extern void U3CCreateStateMachineU3Ed__7_System_IDisposable_Dispose_mB0B2DE1F5E42E4A0E2BC87A5954F32253D1DCA2E (void);
// 0x00000020 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::MoveNext()
extern void U3CCreateStateMachineU3Ed__7_MoveNext_m3F6C525AA3564DBD175BA552617450C0F03B97BE (void);
// 0x00000021 System.Boolean FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerator<System.Boolean>.get_Current()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_m4DA40419B6DE9932D1B662652D75D2D2D59C3D0A (void);
// 0x00000022 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.Reset()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m04A2214AC63EB9C1466A3606E9F4E4E7D5BFE825 (void);
// 0x00000023 System.Object FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_get_Current_mC8425FC6A3459C499E036D4B237983677A271C63 (void);
// 0x00000024 System.Collections.Generic.IEnumerator`1<System.Boolean> FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.Generic.IEnumerable<System.Boolean>.GetEnumerator()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_mAB8AB2CFF5ACEE24B3B22AD86694BFF4556C8477 (void);
// 0x00000025 System.Collections.IEnumerator FMETP.SharpZipLib.Zip.Compression.InflaterDynHeader/<CreateStateMachine>d__7::System.Collections.IEnumerable.GetEnumerator()
extern void U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerable_GetEnumerator_m4291D1C83BB91703B30338BD098A4F5C811DA653 (void);
// 0x00000026 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterConstants::.cctor()
extern void DeflaterConstants__cctor_mEEBE7822F529022D8EA8640C583E5599726701EA (void);
// 0x00000027 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.cctor()
extern void InflaterHuffmanTree__cctor_mF09BFE537E53F018A4D1AFD3E77187BE0BE57AAA (void);
// 0x00000028 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::.ctor(System.Collections.Generic.IList`1<System.Byte>)
extern void InflaterHuffmanTree__ctor_m4E8CB2766D953CE8D45F8ED9761B694D322071AD (void);
// 0x00000029 System.Void FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::BuildTree(System.Collections.Generic.IList`1<System.Byte>)
extern void InflaterHuffmanTree_BuildTree_m3BD4D5785176D839642004CA93A4B289C0077192 (void);
// 0x0000002A System.Int32 FMETP.SharpZipLib.Zip.Compression.InflaterHuffmanTree::GetSymbol(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator)
extern void InflaterHuffmanTree_GetSymbol_m04421C2044832501C7DD8329EC3462309FC04E8C (void);
// 0x0000002B System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::.ctor(System.Int32)
extern void PendingBuffer__ctor_mF24DCBCBC0C0206C54967D195EAB06DB886DD0EF (void);
// 0x0000002C System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Reset()
extern void PendingBuffer_Reset_m25327D1D6764AD93237EB332BCE77FF262C51C3F (void);
// 0x0000002D System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShort(System.Int32)
extern void PendingBuffer_WriteShort_m72DDB100B55939A680D8DD751AAB9CE8EB356B81 (void);
// 0x0000002E System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBlock(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_WriteBlock_m413BE31B95C9A3D21736196ED19EA5FF54E96CAA (void);
// 0x0000002F System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_BitCount()
extern void PendingBuffer_get_BitCount_m8979487A5B1F06E17590060AC74DA4AE23CA9D49 (void);
// 0x00000030 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::AlignToByte()
extern void PendingBuffer_AlignToByte_m5EECBB4FD913EE1A0921CC8641706840D53F8E39 (void);
// 0x00000031 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteBits(System.Int32,System.Int32)
extern void PendingBuffer_WriteBits_mE44F49C7E556D140E8A8B018494243B85741305B (void);
// 0x00000032 System.Void FMETP.SharpZipLib.Zip.Compression.PendingBuffer::WriteShortMSB(System.Int32)
extern void PendingBuffer_WriteShortMSB_m2471626EAEE6EABAE02C34EC9DD6A0D445893A29 (void);
// 0x00000033 System.Boolean FMETP.SharpZipLib.Zip.Compression.PendingBuffer::get_IsFlushed()
extern void PendingBuffer_get_IsFlushed_mCA4BBD1DCF273AFB286E9BCC6ECD0A11DDDBCE24 (void);
// 0x00000034 System.Int32 FMETP.SharpZipLib.Zip.Compression.PendingBuffer::Flush(System.Byte[],System.Int32,System.Int32)
extern void PendingBuffer_Flush_mEA2E59AC113053DCD6A48BAF475F9D0873793E98 (void);
// 0x00000035 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending,System.Boolean)
extern void DeflaterEngine__ctor_m49CA0C68AF208224BF3F208BA14F506122E6708F (void);
// 0x00000036 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Deflate(System.Boolean,System.Boolean)
extern void DeflaterEngine_Deflate_mD7649335F93EF2EBD54B53416B1D53EAEB14FDFB (void);
// 0x00000037 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetInput(System.Byte[],System.Int32,System.Int32)
extern void DeflaterEngine_SetInput_m0E8F82E209CFCDA4A5D103DEE8EAA398EB03A664 (void);
// 0x00000038 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::NeedsInput()
extern void DeflaterEngine_NeedsInput_mB6C7079F4B76D166126F3D1E8972047BD68FBD21 (void);
// 0x00000039 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::Reset()
extern void DeflaterEngine_Reset_mC4EC01A7280C9D9F83980BFBB3699F425B298468 (void);
// 0x0000003A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::ResetAdler()
extern void DeflaterEngine_ResetAdler_mBD23875B071A44950E26D8731B14D3E9F1711864 (void);
// 0x0000003B System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_Adler()
extern void DeflaterEngine_get_Adler_mF0F8AAEC1180E13E0F9C3BD0F422AC0857CAF771 (void);
// 0x0000003C System.Int64 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::get_TotalIn()
extern void DeflaterEngine_get_TotalIn_m8D54302C469F3E33B96D285E3F0695E97845E0FF (void);
// 0x0000003D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::set_Strategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void DeflaterEngine_set_Strategy_m20456E9716789B5CF7AD7D7FB2C35873A579FA29 (void);
// 0x0000003E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SetLevel(System.Int32)
extern void DeflaterEngine_SetLevel_m79FEA76872F9E47D6DA344597AC1B6486CDFC67A (void);
// 0x0000003F System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FillWindow()
extern void DeflaterEngine_FillWindow_m1DFC97F8BBE5F4C3B5AAA09D1F60D8F0ED322DF1 (void);
// 0x00000040 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::UpdateHash()
extern void DeflaterEngine_UpdateHash_m97404C0389AD0573F993011460719883C28C070F (void);
// 0x00000041 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::InsertString()
extern void DeflaterEngine_InsertString_m03669E8C6A17120CF79FF57E9AC77D414AB7CB1F (void);
// 0x00000042 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::SlideWindow()
extern void DeflaterEngine_SlideWindow_m662F2AA9EA126E8C085F19B59DBAE65FDA0F348A (void);
// 0x00000043 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::FindLongestMatch(System.Int32)
extern void DeflaterEngine_FindLongestMatch_mDE0FA41B8BFA58EF80094F59FA8173C93AFE43DD (void);
// 0x00000044 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateStored(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateStored_mE57C7110A964C257A00E7E1BC847CAC1B4DCFD25 (void);
// 0x00000045 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateFast(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateFast_m1DE21CE5E1FEA9A2B7F17DD9B47FCC8581EE8CB2 (void);
// 0x00000046 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterEngine::DeflateSlow(System.Boolean,System.Boolean)
extern void DeflaterEngine_DeflateSlow_m1F7D02894FBAA737841AF7347EE9887367CE6DFF (void);
// 0x00000047 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterPending::.ctor()
extern void DeflaterPending__ctor_mE52F34B9D8C39B919CFD94957D0D0EEDC3573C00 (void);
// 0x00000048 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.cctor()
extern void DeflaterHuffman__cctor_m8AB7B1176894889198F76CDA63FB1ECFD2D28815 (void);
// 0x00000049 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterPending)
extern void DeflaterHuffman__ctor_mD85386E5F3985164250C7ABE280C10580EA765BB (void);
// 0x0000004A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Reset()
extern void DeflaterHuffman_Reset_m508AC41AB14FCD90F96133E8220A824C98D55EBB (void);
// 0x0000004B System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::SendAllTrees(System.Int32)
extern void DeflaterHuffman_SendAllTrees_mDE5FFF60ADFB3CF62D05B13062BA1B3895EA9D5B (void);
// 0x0000004C System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::CompressBlock()
extern void DeflaterHuffman_CompressBlock_mAB2390E4E22B89CA4852618322199CA60B2E3192 (void);
// 0x0000004D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushStoredBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushStoredBlock_mBDB56504633B60E92DBAB856720380C76AEEFA0E (void);
// 0x0000004E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::FlushBlock(System.Byte[],System.Int32,System.Int32,System.Boolean)
extern void DeflaterHuffman_FlushBlock_m56CC7289E9F2D491EE9C4F7470BB3404868F2816 (void);
// 0x0000004F System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::IsFull()
extern void DeflaterHuffman_IsFull_mCC23C32E5AB8449343B788ED50228016738C8D19 (void);
// 0x00000050 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyLit(System.Int32)
extern void DeflaterHuffman_TallyLit_m5E8F5ECB75595BD4D16003EBB0E9B894474AA105 (void);
// 0x00000051 System.Boolean FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::TallyDist(System.Int32,System.Int32)
extern void DeflaterHuffman_TallyDist_mB0F02A1330D2767CECF1C302E5B034EA9047693F (void);
// 0x00000052 System.Int16 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::BitReverse(System.Int32)
extern void DeflaterHuffman_BitReverse_m78B4E384ECFA096A05F135BEF8D1F0110865CC68 (void);
// 0x00000053 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Lcode(System.Int32)
extern void DeflaterHuffman_Lcode_m75D814944C47AC99049AE4A4D579C597CC42AE7B (void);
// 0x00000054 System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman::Dcode(System.Int32)
extern void DeflaterHuffman_Dcode_mF1C3C3E3C020BCC88646CA232ED7AC2BF1FE2791 (void);
// 0x00000055 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::.ctor(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman,System.Int32,System.Int32,System.Int32)
extern void Tree__ctor_m715AEAFB7CDDDF6031585CF29C798B49EA82B89D (void);
// 0x00000056 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::Reset()
extern void Tree_Reset_mE8FB5A53E24FC6166AE4158F609860C68D80F197 (void);
// 0x00000057 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteSymbol(System.Int32)
extern void Tree_WriteSymbol_mEC70901BFD2B14A935CF925646D16674F1DAEE00 (void);
// 0x00000058 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::SetStaticCodes(System.Int16[],System.Byte[])
extern void Tree_SetStaticCodes_m962B9C34019FE50C82BBD4FC4E9A804CA6F941F9 (void);
// 0x00000059 System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildCodes()
extern void Tree_BuildCodes_mA4CFE9D8CDEE245FC8CDAF5BECD607FF4E424A94 (void);
// 0x0000005A System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildTree()
extern void Tree_BuildTree_m76A39EF27D7401EF54618DAF929AF61693BE23AB (void);
// 0x0000005B System.Int32 FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::GetEncodedLength()
extern void Tree_GetEncodedLength_m1285E9384082DF659FA0EC6C787F6D009B24AA6E (void);
// 0x0000005C System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::CalcBLFreq(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern void Tree_CalcBLFreq_m7BCDDA0A9C6A779CD61718F87EF4188B6C904969 (void);
// 0x0000005D System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::WriteTree(FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree)
extern void Tree_WriteTree_m88F8FA7D3761E802256A522578C0F45C61B08B77 (void);
// 0x0000005E System.Void FMETP.SharpZipLib.Zip.Compression.DeflaterHuffman/Tree::BuildLength(System.Int32[])
extern void Tree_BuildLength_mEC250C633C2D99FC0A0E64B69B89AF4BDE940DA0 (void);
// 0x0000005F System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::.ctor(System.Int32,System.Boolean)
extern void Deflater__ctor_mC070DE3357E2C6FFA2FEBE7D8B64A01133AE7777 (void);
// 0x00000060 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Reset()
extern void Deflater_Reset_m6C28E1CD3AB911690B060AD73A2E5B11616D191B (void);
// 0x00000061 System.Int64 FMETP.SharpZipLib.Zip.Compression.Deflater::get_TotalIn()
extern void Deflater_get_TotalIn_m8A1203B1B7D589CBEC00F9AC10747AB9DCB02986 (void);
// 0x00000062 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Flush()
extern void Deflater_Flush_m0105C0A5EE91ED0282CBDABD0215CEC7ABDA5B9E (void);
// 0x00000063 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::Finish()
extern void Deflater_Finish_m071FE3522078BF06780B2C5527E69072CF02AD69 (void);
// 0x00000064 System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsFinished()
extern void Deflater_get_IsFinished_mA39E4E604C2B1D9CF4B25B9AC0C991F6E3029D35 (void);
// 0x00000065 System.Boolean FMETP.SharpZipLib.Zip.Compression.Deflater::get_IsNeedingInput()
extern void Deflater_get_IsNeedingInput_m03DF7A48D9E87FBF5B43DB155522976A274C15B0 (void);
// 0x00000066 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Deflater_SetInput_mAB7323846D0CC2D96DEE383CC4CF9CA27E274022 (void);
// 0x00000067 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetLevel(System.Int32)
extern void Deflater_SetLevel_m4B6B1EC9F77AAA975044DDCB41CA048CB730368E (void);
// 0x00000068 System.Void FMETP.SharpZipLib.Zip.Compression.Deflater::SetStrategy(FMETP.SharpZipLib.Zip.Compression.DeflateStrategy)
extern void Deflater_SetStrategy_m0610F265622EE046326AEBD259441D6EF6D056E6 (void);
// 0x00000069 System.Int32 FMETP.SharpZipLib.Zip.Compression.Deflater::Deflate(System.Byte[],System.Int32,System.Int32)
extern void Deflater_Deflate_m1A20A090542DEA6ADD2A4C2944A2DE368DDA3C30 (void);
// 0x0000006A System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.ctor(System.Boolean)
extern void Inflater__ctor_m7A16DCD47DB2F4271C3E1026938E92E455F21D64 (void);
// 0x0000006B System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::Reset()
extern void Inflater_Reset_m1246E5F5CC63A42E112647327206E786D4628E1C (void);
// 0x0000006C System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHeader()
extern void Inflater_DecodeHeader_m66936478C983A9ED794EEF9481562E8E9A61EBC4 (void);
// 0x0000006D System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeDict()
extern void Inflater_DecodeDict_m6F7650014EE8294E1E83C77B86A4ADA555D29D56 (void);
// 0x0000006E System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeHuffman()
extern void Inflater_DecodeHuffman_mEAC74684D2821A659C02D58F6672AE1D4C04F779 (void);
// 0x0000006F System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::DecodeChksum()
extern void Inflater_DecodeChksum_mCF86A57F59F86F74E2866B523B40B54D60266EC4 (void);
// 0x00000070 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::Decode()
extern void Inflater_Decode_m3F286CF2DB17CC9E63F5E7FFEE196560FF04C3FE (void);
// 0x00000071 System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::SetInput(System.Byte[],System.Int32,System.Int32)
extern void Inflater_SetInput_mC70BA081C59F54EEEAB0D09E99B6A960511DFAE5 (void);
// 0x00000072 System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::Inflate(System.Byte[],System.Int32,System.Int32)
extern void Inflater_Inflate_m346D8204009BCDDC5E631C6BB02E0CAE94265293 (void);
// 0x00000073 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingInput()
extern void Inflater_get_IsNeedingInput_m8C2A1335BF0AE2B9611E2B4EC8C29EF30838E861 (void);
// 0x00000074 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsNeedingDictionary()
extern void Inflater_get_IsNeedingDictionary_mBF2600CD449D3C67812B71BD8142B26E40957949 (void);
// 0x00000075 System.Boolean FMETP.SharpZipLib.Zip.Compression.Inflater::get_IsFinished()
extern void Inflater_get_IsFinished_mEE0EDD8F0042A9F983434FEA706673C0128D27B7 (void);
// 0x00000076 System.Int64 FMETP.SharpZipLib.Zip.Compression.Inflater::get_TotalOut()
extern void Inflater_get_TotalOut_m404FF486E2B5770EF4CC5AC0A97EB96F8C9CFB7F (void);
// 0x00000077 System.Int32 FMETP.SharpZipLib.Zip.Compression.Inflater::get_RemainingInput()
extern void Inflater_get_RemainingInput_mF83D61DB9F0B6E7271DCD7DBEFABE227F31F539D (void);
// 0x00000078 System.Void FMETP.SharpZipLib.Zip.Compression.Inflater::.cctor()
extern void Inflater__cctor_mB4CB7271CD820549DE671F90FE0BE74485EEB45E (void);
// 0x00000079 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Deflater,System.Int32)
extern void DeflaterOutputStream__ctor_mB1261970E9121772DD6F12FD398D83CDBCEA2115 (void);
// 0x0000007A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Finish()
extern void DeflaterOutputStream_Finish_m06EA2E87DD31088E67860556666E0D789BF753E3 (void);
// 0x0000007B System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_IsStreamOwner()
extern void DeflaterOutputStream_get_IsStreamOwner_m553DBD4E391EA788C37D11359521100FE87D5731 (void);
// 0x0000007C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::EncryptBlock(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_EncryptBlock_m032C850D6AB2A2A2EE00DC18E25FD62115D335CA (void);
// 0x0000007D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate()
extern void DeflaterOutputStream_Deflate_m12969E1E0DA40E05575E172F25A6DAAA212E9412 (void);
// 0x0000007E System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Deflate(System.Boolean)
extern void DeflaterOutputStream_Deflate_mCC807ED3FFEBF1FCB97B4C708AA7191DE35D8599 (void);
// 0x0000007F System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanRead()
extern void DeflaterOutputStream_get_CanRead_m8C38B293B34D3F77204473350DBA9F9094FC0CD7 (void);
// 0x00000080 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanSeek()
extern void DeflaterOutputStream_get_CanSeek_mA2D35AD53B6CF522D001F3F9FA54558B0EB54DB8 (void);
// 0x00000081 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_CanWrite()
extern void DeflaterOutputStream_get_CanWrite_m1592C8B8C1DE13791D05CC233AE1012259375528 (void);
// 0x00000082 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Length()
extern void DeflaterOutputStream_get_Length_m1A13A217D4FD4EA5E62123E87D801F11483FE5FC (void);
// 0x00000083 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::get_Position()
extern void DeflaterOutputStream_get_Position_m9D6382BEB6E7E27FA7580BC7CDCAD6E4C0BE0906 (void);
// 0x00000084 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::set_Position(System.Int64)
extern void DeflaterOutputStream_set_Position_m95D7D85435EBAD181FE4D236FCAD10EA4641D7DF (void);
// 0x00000085 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void DeflaterOutputStream_Seek_m053FCC3919464D1B69EAE8F21D9A0BEED3A46317 (void);
// 0x00000086 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::ReadByte()
extern void DeflaterOutputStream_ReadByte_mAEB07D1CE229951DD765ECED7AFFBF511CF41450 (void);
// 0x00000087 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Read_mB730D90D6AB1803DCD96432F1A5596345B7380D7 (void);
// 0x00000088 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Flush()
extern void DeflaterOutputStream_Flush_m6D11C38708A6B9271685D504D55208DADA0E7DE5 (void);
// 0x00000089 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Dispose(System.Boolean)
extern void DeflaterOutputStream_Dispose_m5845CA008DC28D8579B9D2DBDFCE979F3FF26430 (void);
// 0x0000008A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::GetAuthCodeIfAES()
extern void DeflaterOutputStream_GetAuthCodeIfAES_mD864AF782ABC637DDA246CBA918556EBFDDE1F49 (void);
// 0x0000008B System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::WriteByte(System.Byte)
extern void DeflaterOutputStream_WriteByte_m40CFC3E4BED9A9CA6E7558648D2B3BF41AD57504 (void);
// 0x0000008C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void DeflaterOutputStream_Write_m1D628B819635B5BBED43EA3476F668B5B4D280C4 (void);
// 0x0000008D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.DeflaterOutputStream::.cctor()
extern void DeflaterOutputStream__cctor_m321272A71F03117DB57AD61B055B6A94AF0A5AB1 (void);
// 0x0000008E System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::PeekBits(System.Int32)
extern void StreamManipulator_PeekBits_m3029F53241B4173314904FE21E62467655866569 (void);
// 0x0000008F System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Int32&,System.Int32)
extern void StreamManipulator_TryGetBits_m5D7356FD3192830E9CF2FF644E2C861D5D9E2D3D (void);
// 0x00000090 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::TryGetBits(System.Int32,System.Byte[]&,System.Int32)
extern void StreamManipulator_TryGetBits_mDE96F81CCE5B6B45746ADCF3B9895CE287F24EF3 (void);
// 0x00000091 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::DropBits(System.Int32)
extern void StreamManipulator_DropBits_m664F5274D56C5C97016B430173698BF43A79F71E (void);
// 0x00000092 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBits()
extern void StreamManipulator_get_AvailableBits_mCE6733DCB49FAD7A6178B9B83600A42E56EA5FFF (void);
// 0x00000093 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_AvailableBytes()
extern void StreamManipulator_get_AvailableBytes_m0A3C9739CFFC18E31F012C43B6263418D24104B8 (void);
// 0x00000094 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SkipToByteBoundary()
extern void StreamManipulator_SkipToByteBoundary_m973AD4D90AE3F74C1C060A66CCA15DE1FD7F51A0 (void);
// 0x00000095 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::get_IsNeedingInput()
extern void StreamManipulator_get_IsNeedingInput_m7128D6EF06AC2C4DA0B29F0A8491F02738E1D6D9 (void);
// 0x00000096 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::CopyBytes(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_CopyBytes_m5FD5042F40739DB0C7C5A252863B230F54B2F5A9 (void);
// 0x00000097 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::Reset()
extern void StreamManipulator_Reset_m356D7FDDD19521BCBF07184CA7A88BACEFB77DD8 (void);
// 0x00000098 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::SetInput(System.Byte[],System.Int32,System.Int32)
extern void StreamManipulator_SetInput_m349E809D9DFAC989395E08B8658412CB7F6B7888 (void);
// 0x00000099 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator::.ctor()
extern void StreamManipulator__ctor_m281AA9FFAE44EB3FF205353102BB993404799DF4 (void);
// 0x0000009A System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::.ctor(System.IO.Stream,System.Int32)
extern void InflaterInputBuffer__ctor_m7FB77C4A9E9A2B7DE1B60F30D9099CEFED743B0B (void);
// 0x0000009B System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::get_Available()
extern void InflaterInputBuffer_get_Available_mC51EF33380C222268AAE7974CEA404DA47FBF4BE (void);
// 0x0000009C System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::set_Available(System.Int32)
extern void InflaterInputBuffer_set_Available_m39985FB6AAC6E3EF6220C3D7016446E723B4171E (void);
// 0x0000009D System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::SetInflaterInput(FMETP.SharpZipLib.Zip.Compression.Inflater)
extern void InflaterInputBuffer_SetInflaterInput_mB3BC966CA28AA9B88716E79BBEBD095C87F8DFB1 (void);
// 0x0000009E System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::Fill()
extern void InflaterInputBuffer_Fill_mA262624C815193E4630117A6768C9FEBB7061A04 (void);
// 0x0000009F System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadClearTextBuffer(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputBuffer_ReadClearTextBuffer_mB4650EB155309D2BB20C94BA7DF9B02A73AAC85D (void);
// 0x000000A0 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputBuffer::ReadLeByte()
extern void InflaterInputBuffer_ReadLeByte_mEB2B68A9EF28CA25FF86129E5E13B8E4FD87D4BA (void);
// 0x000000A1 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::.ctor(System.IO.Stream,FMETP.SharpZipLib.Zip.Compression.Inflater,System.Int32)
extern void InflaterInputStream__ctor_m9D2A99DC39904A79DAA49139A61E9EB7AF3C545E (void);
// 0x000000A2 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_IsStreamOwner()
extern void InflaterInputStream_get_IsStreamOwner_m29F7FBF7749E5641F843EE0559F06C230E53C00B (void);
// 0x000000A3 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Fill()
extern void InflaterInputStream_Fill_m2696D717D791862FCBE7D5676ED1C51A67999B28 (void);
// 0x000000A4 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanRead()
extern void InflaterInputStream_get_CanRead_mAA0FB2F549A8F728D023BE8586EBD8E9635A4C3C (void);
// 0x000000A5 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanSeek()
extern void InflaterInputStream_get_CanSeek_mF0A0653CDC23019FFAAF68D7C7159DC6313C74CB (void);
// 0x000000A6 System.Boolean FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_CanWrite()
extern void InflaterInputStream_get_CanWrite_m71B8B21CA09E583D877FA0917161C597D90FC60F (void);
// 0x000000A7 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Length()
extern void InflaterInputStream_get_Length_mE3E432D72C9E4A7503B56699BBB1B1DB53185A3A (void);
// 0x000000A8 System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::get_Position()
extern void InflaterInputStream_get_Position_mF4915CBC215376A6A52C96D12AA6D53DABF96EE2 (void);
// 0x000000A9 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::set_Position(System.Int64)
extern void InflaterInputStream_set_Position_mE6A05914B313BCE2143BBE878B10769BC6470C60 (void);
// 0x000000AA System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Flush()
extern void InflaterInputStream_Flush_m2AB84DA85A9219AA52648759EC03D7B33BADB23D (void);
// 0x000000AB System.Int64 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Seek(System.Int64,System.IO.SeekOrigin)
extern void InflaterInputStream_Seek_mC92C41A9130B2259775EB0D5459E98BBDF30FF46 (void);
// 0x000000AC System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Write_mDA15E5F732AB2519A778FBC8E0BD264664CBDA32 (void);
// 0x000000AD System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::WriteByte(System.Byte)
extern void InflaterInputStream_WriteByte_m62A4D3B6E7BBDDA1F0055A357D4CA8D0CB274A8A (void);
// 0x000000AE System.Void FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Dispose(System.Boolean)
extern void InflaterInputStream_Dispose_mFF716157D79A4AECA6160DFC0071DBC882103A69 (void);
// 0x000000AF System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.InflaterInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void InflaterInputStream_Read_m9B13CD881985CF994D08FE6FBF621ACDB650844B (void);
// 0x000000B0 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Write(System.Int32)
extern void OutputWindow_Write_m30C4FE7DAC167F268A1FC791209BFEEFC4A21BA5 (void);
// 0x000000B1 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::SlowRepeat(System.Int32,System.Int32,System.Int32)
extern void OutputWindow_SlowRepeat_m2E9F479A3F19672244FA83CB9D98A554AA0EA2AB (void);
// 0x000000B2 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Repeat(System.Int32,System.Int32)
extern void OutputWindow_Repeat_m85715F8EEC2D508C2DA7A9E857AB77773F28D0BC (void);
// 0x000000B3 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyStored(FMETP.SharpZipLib.Zip.Compression.Streams.StreamManipulator,System.Int32)
extern void OutputWindow_CopyStored_m7AC0D8A03B77E0A8FEE63B11661AF9FF4FAC68D5 (void);
// 0x000000B4 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetFreeSpace()
extern void OutputWindow_GetFreeSpace_m74D8C5035C83AD95D966251F055A5739E0271A4B (void);
// 0x000000B5 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::GetAvailable()
extern void OutputWindow_GetAvailable_m2C5AAC58C94FCD1424FE4279C6EBBAF5079DE177 (void);
// 0x000000B6 System.Int32 FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::CopyOutput(System.Byte[],System.Int32,System.Int32)
extern void OutputWindow_CopyOutput_mBE47FDBC54F5EDE41764B323AA50D22B33A815C4 (void);
// 0x000000B7 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::Reset()
extern void OutputWindow_Reset_m22BD6F8F80797AF0C87C83D31993EE44EA427323 (void);
// 0x000000B8 System.Void FMETP.SharpZipLib.Zip.Compression.Streams.OutputWindow::.ctor()
extern void OutputWindow__ctor_m17E0FB5A61A2FDC9AD888A1FB8003BE61AC0D697 (void);
// 0x000000B9 System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream)
extern void GZipInputStream__ctor_m95AA608A7AB2A7F6B7B474AE573F7A4BC01BFA24 (void);
// 0x000000BA System.Void FMETP.SharpZipLib.GZip.GZipInputStream::.ctor(System.IO.Stream,System.Int32)
extern void GZipInputStream__ctor_mB96AD468B784F5A701CCE8CE52386EEE35191F04 (void);
// 0x000000BB System.Int32 FMETP.SharpZipLib.GZip.GZipInputStream::Read(System.Byte[],System.Int32,System.Int32)
extern void GZipInputStream_Read_m0B93E0D361CD6E673AEF0E304FD1DE42C8C7E2FA (void);
// 0x000000BC System.Boolean FMETP.SharpZipLib.GZip.GZipInputStream::ReadHeader()
extern void GZipInputStream_ReadHeader_mEC04BF0DA6E09714A4267F04395C4209CCDC3713 (void);
// 0x000000BD System.Void FMETP.SharpZipLib.GZip.GZipInputStream::ReadFooter()
extern void GZipInputStream_ReadFooter_m758A0BC59B4C92174AB2C68093AA889773191B30 (void);
// 0x000000BE System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream)
extern void GZipOutputStream__ctor_m5AAC3AAE319ECFE9900F9DF2A5C98767E841191A (void);
// 0x000000BF System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::.ctor(System.IO.Stream,System.Int32)
extern void GZipOutputStream__ctor_mE2209BAF129080B4C76FA979B3291F9DF7D2D3FE (void);
// 0x000000C0 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Write(System.Byte[],System.Int32,System.Int32)
extern void GZipOutputStream_Write_m3F6FDB5EEF5FD2BE84F958721F5930966F20DF26 (void);
// 0x000000C1 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Dispose(System.Boolean)
extern void GZipOutputStream_Dispose_mCE77A83345EAB71437E9A27531F53980B8CB3B61 (void);
// 0x000000C2 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::Finish()
extern void GZipOutputStream_Finish_m0597338A3DCA06C31E37602606B93C940D865807 (void);
// 0x000000C3 System.Void FMETP.SharpZipLib.GZip.GZipOutputStream::WriteHeader()
extern void GZipOutputStream_WriteHeader_m278636143B02AEF73522AD196644FBBDB2C4FA6A (void);
// 0x000000C4 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor()
extern void GZipException__ctor_m570FEF730A0A7CBD0984113BBC4D80613C3A5845 (void);
// 0x000000C5 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.String)
extern void GZipException__ctor_mBFE9328C44E5B5F41B4FB93477FF7B47A5C55806 (void);
// 0x000000C6 System.Void FMETP.SharpZipLib.GZip.GZipException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void GZipException__ctor_m91989114A82D905BAA991430896E5C574E8D69FF (void);
static Il2CppMethodPointer s_methodPointers[198] = 
{
	FMZipHelper_FMZipBytes_m89FC46CD06FD9F5B911D8E4177F4A5B5A558B2EC,
	FMZipHelper_FMUnzipBytes_m73843117F77A036B36568A3C8905CD16A5622CA1,
	FMZipHelper_ReadAllBytes_m63D2211F37608D05AB2DC43197D12E204E1BBD79,
	StreamDecodingException__ctor_m3861C591A50B13FB0EFB4216CE6B59FB7A07834B,
	StreamDecodingException__ctor_m4EB604F7593D48437D457774B993B183450E3931,
	StreamDecodingException__ctor_m7514AA294B9F595D78AB796AF7C32E1F6354A74F,
	ValueOutOfRangeException__ctor_m06D85F8BD146772ED317C82BC36EE3AD1978BDA3,
	ValueOutOfRangeException__ctor_m117C299C94190456E4243AA8DBA981EA90C2972E,
	ValueOutOfRangeException__ctor_m37E4CBDE576A56A968D1ED054EA51C584276CFBC,
	SharpZipBaseException__ctor_m0C3DF8DAA7E98949C614960F475930F20866C734,
	SharpZipBaseException__ctor_m46F5B8EEEFDC1927B5A86D31309F3C4144AB3795,
	SharpZipBaseException__ctor_mCBA216AB2CE7912C2CDD8B7ECF7486E59A80F1BD,
	Crc32__ctor_m4DDB0237065AAFF5D3DC0A000FA9F913D4E83876,
	Crc32_Reset_m75B0BD8D3F613C47DEF357C8850511CC87223E2B,
	Crc32_get_Value_m79C0A69FED134B671369FF7A70C4D13D1B8EC97D,
	Crc32_Update_m17688C34F507767331AB6E56AB317DDBEFB724E0,
	Crc32_Update_m3E20582F35DFC3959430B3CD93C821B0285636E7,
	Crc32__cctor_m084ABD60B52D8CEE12FF21852DDFE69C3CF12839,
	Adler32__ctor_m75E5243E681A76C932F3A9DC3AB91C117DCB344C,
	Adler32_Reset_m1251AE8E4EE59A4A70BF08FC44F1A51DB75D8481,
	Adler32_get_Value_m02426228A87B1427781CB2E65CA07BC6B94FE01F,
	Adler32_Update_mF1C9538E4D88BC157A70F6991DCA5C13FCF67319,
	Adler32__cctor_mABF2B8CF4F0483A100ADB381B003D64FEAFFC7DA,
	InflaterDynHeader_AttemptRead_mE56BBEAD91A58D6AB5069C41421DCE84CE7FA11F,
	InflaterDynHeader__ctor_m02FBB790871BA51C12164D7EE107D680BF0F05B2,
	InflaterDynHeader_CreateStateMachine_m8BE79D7E4056521B4C92F58B1A796D34955A6FC1,
	InflaterDynHeader_get_LiteralLengthTree_m194F2D1B08475A7F53A240306641123B9A493C8D,
	InflaterDynHeader_get_DistanceTree_mACBCD5B581B4EF1C7022F964438B53EC7ED13DCF,
	InflaterDynHeader__cctor_m5D8E774E76340751FBAA7FB662FF93D675090DC7,
	U3CCreateStateMachineU3Ed__7__ctor_mCA17E25DBEDA1754081C1BAB0497DA8A85FC8A27,
	U3CCreateStateMachineU3Ed__7_System_IDisposable_Dispose_mB0B2DE1F5E42E4A0E2BC87A5954F32253D1DCA2E,
	U3CCreateStateMachineU3Ed__7_MoveNext_m3F6C525AA3564DBD175BA552617450C0F03B97BE,
	U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_BooleanU3E_get_Current_m4DA40419B6DE9932D1B662652D75D2D2D59C3D0A,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_Reset_m04A2214AC63EB9C1466A3606E9F4E4E7D5BFE825,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerator_get_Current_mC8425FC6A3459C499E036D4B237983677A271C63,
	U3CCreateStateMachineU3Ed__7_System_Collections_Generic_IEnumerableU3CSystem_BooleanU3E_GetEnumerator_mAB8AB2CFF5ACEE24B3B22AD86694BFF4556C8477,
	U3CCreateStateMachineU3Ed__7_System_Collections_IEnumerable_GetEnumerator_m4291D1C83BB91703B30338BD098A4F5C811DA653,
	DeflaterConstants__cctor_mEEBE7822F529022D8EA8640C583E5599726701EA,
	InflaterHuffmanTree__cctor_mF09BFE537E53F018A4D1AFD3E77187BE0BE57AAA,
	InflaterHuffmanTree__ctor_m4E8CB2766D953CE8D45F8ED9761B694D322071AD,
	InflaterHuffmanTree_BuildTree_m3BD4D5785176D839642004CA93A4B289C0077192,
	InflaterHuffmanTree_GetSymbol_m04421C2044832501C7DD8329EC3462309FC04E8C,
	PendingBuffer__ctor_mF24DCBCBC0C0206C54967D195EAB06DB886DD0EF,
	PendingBuffer_Reset_m25327D1D6764AD93237EB332BCE77FF262C51C3F,
	PendingBuffer_WriteShort_m72DDB100B55939A680D8DD751AAB9CE8EB356B81,
	PendingBuffer_WriteBlock_m413BE31B95C9A3D21736196ED19EA5FF54E96CAA,
	PendingBuffer_get_BitCount_m8979487A5B1F06E17590060AC74DA4AE23CA9D49,
	PendingBuffer_AlignToByte_m5EECBB4FD913EE1A0921CC8641706840D53F8E39,
	PendingBuffer_WriteBits_mE44F49C7E556D140E8A8B018494243B85741305B,
	PendingBuffer_WriteShortMSB_m2471626EAEE6EABAE02C34EC9DD6A0D445893A29,
	PendingBuffer_get_IsFlushed_mCA4BBD1DCF273AFB286E9BCC6ECD0A11DDDBCE24,
	PendingBuffer_Flush_mEA2E59AC113053DCD6A48BAF475F9D0873793E98,
	DeflaterEngine__ctor_m49CA0C68AF208224BF3F208BA14F506122E6708F,
	DeflaterEngine_Deflate_mD7649335F93EF2EBD54B53416B1D53EAEB14FDFB,
	DeflaterEngine_SetInput_m0E8F82E209CFCDA4A5D103DEE8EAA398EB03A664,
	DeflaterEngine_NeedsInput_mB6C7079F4B76D166126F3D1E8972047BD68FBD21,
	DeflaterEngine_Reset_mC4EC01A7280C9D9F83980BFBB3699F425B298468,
	DeflaterEngine_ResetAdler_mBD23875B071A44950E26D8731B14D3E9F1711864,
	DeflaterEngine_get_Adler_mF0F8AAEC1180E13E0F9C3BD0F422AC0857CAF771,
	DeflaterEngine_get_TotalIn_m8D54302C469F3E33B96D285E3F0695E97845E0FF,
	DeflaterEngine_set_Strategy_m20456E9716789B5CF7AD7D7FB2C35873A579FA29,
	DeflaterEngine_SetLevel_m79FEA76872F9E47D6DA344597AC1B6486CDFC67A,
	DeflaterEngine_FillWindow_m1DFC97F8BBE5F4C3B5AAA09D1F60D8F0ED322DF1,
	DeflaterEngine_UpdateHash_m97404C0389AD0573F993011460719883C28C070F,
	DeflaterEngine_InsertString_m03669E8C6A17120CF79FF57E9AC77D414AB7CB1F,
	DeflaterEngine_SlideWindow_m662F2AA9EA126E8C085F19B59DBAE65FDA0F348A,
	DeflaterEngine_FindLongestMatch_mDE0FA41B8BFA58EF80094F59FA8173C93AFE43DD,
	DeflaterEngine_DeflateStored_mE57C7110A964C257A00E7E1BC847CAC1B4DCFD25,
	DeflaterEngine_DeflateFast_m1DE21CE5E1FEA9A2B7F17DD9B47FCC8581EE8CB2,
	DeflaterEngine_DeflateSlow_m1F7D02894FBAA737841AF7347EE9887367CE6DFF,
	DeflaterPending__ctor_mE52F34B9D8C39B919CFD94957D0D0EEDC3573C00,
	DeflaterHuffman__cctor_m8AB7B1176894889198F76CDA63FB1ECFD2D28815,
	DeflaterHuffman__ctor_mD85386E5F3985164250C7ABE280C10580EA765BB,
	DeflaterHuffman_Reset_m508AC41AB14FCD90F96133E8220A824C98D55EBB,
	DeflaterHuffman_SendAllTrees_mDE5FFF60ADFB3CF62D05B13062BA1B3895EA9D5B,
	DeflaterHuffman_CompressBlock_mAB2390E4E22B89CA4852618322199CA60B2E3192,
	DeflaterHuffman_FlushStoredBlock_mBDB56504633B60E92DBAB856720380C76AEEFA0E,
	DeflaterHuffman_FlushBlock_m56CC7289E9F2D491EE9C4F7470BB3404868F2816,
	DeflaterHuffman_IsFull_mCC23C32E5AB8449343B788ED50228016738C8D19,
	DeflaterHuffman_TallyLit_m5E8F5ECB75595BD4D16003EBB0E9B894474AA105,
	DeflaterHuffman_TallyDist_mB0F02A1330D2767CECF1C302E5B034EA9047693F,
	DeflaterHuffman_BitReverse_m78B4E384ECFA096A05F135BEF8D1F0110865CC68,
	DeflaterHuffman_Lcode_m75D814944C47AC99049AE4A4D579C597CC42AE7B,
	DeflaterHuffman_Dcode_mF1C3C3E3C020BCC88646CA232ED7AC2BF1FE2791,
	Tree__ctor_m715AEAFB7CDDDF6031585CF29C798B49EA82B89D,
	Tree_Reset_mE8FB5A53E24FC6166AE4158F609860C68D80F197,
	Tree_WriteSymbol_mEC70901BFD2B14A935CF925646D16674F1DAEE00,
	Tree_SetStaticCodes_m962B9C34019FE50C82BBD4FC4E9A804CA6F941F9,
	Tree_BuildCodes_mA4CFE9D8CDEE245FC8CDAF5BECD607FF4E424A94,
	Tree_BuildTree_m76A39EF27D7401EF54618DAF929AF61693BE23AB,
	Tree_GetEncodedLength_m1285E9384082DF659FA0EC6C787F6D009B24AA6E,
	Tree_CalcBLFreq_m7BCDDA0A9C6A779CD61718F87EF4188B6C904969,
	Tree_WriteTree_m88F8FA7D3761E802256A522578C0F45C61B08B77,
	Tree_BuildLength_mEC250C633C2D99FC0A0E64B69B89AF4BDE940DA0,
	Deflater__ctor_mC070DE3357E2C6FFA2FEBE7D8B64A01133AE7777,
	Deflater_Reset_m6C28E1CD3AB911690B060AD73A2E5B11616D191B,
	Deflater_get_TotalIn_m8A1203B1B7D589CBEC00F9AC10747AB9DCB02986,
	Deflater_Flush_m0105C0A5EE91ED0282CBDABD0215CEC7ABDA5B9E,
	Deflater_Finish_m071FE3522078BF06780B2C5527E69072CF02AD69,
	Deflater_get_IsFinished_mA39E4E604C2B1D9CF4B25B9AC0C991F6E3029D35,
	Deflater_get_IsNeedingInput_m03DF7A48D9E87FBF5B43DB155522976A274C15B0,
	Deflater_SetInput_mAB7323846D0CC2D96DEE383CC4CF9CA27E274022,
	Deflater_SetLevel_m4B6B1EC9F77AAA975044DDCB41CA048CB730368E,
	Deflater_SetStrategy_m0610F265622EE046326AEBD259441D6EF6D056E6,
	Deflater_Deflate_m1A20A090542DEA6ADD2A4C2944A2DE368DDA3C30,
	Inflater__ctor_m7A16DCD47DB2F4271C3E1026938E92E455F21D64,
	Inflater_Reset_m1246E5F5CC63A42E112647327206E786D4628E1C,
	Inflater_DecodeHeader_m66936478C983A9ED794EEF9481562E8E9A61EBC4,
	Inflater_DecodeDict_m6F7650014EE8294E1E83C77B86A4ADA555D29D56,
	Inflater_DecodeHuffman_mEAC74684D2821A659C02D58F6672AE1D4C04F779,
	Inflater_DecodeChksum_mCF86A57F59F86F74E2866B523B40B54D60266EC4,
	Inflater_Decode_m3F286CF2DB17CC9E63F5E7FFEE196560FF04C3FE,
	Inflater_SetInput_mC70BA081C59F54EEEAB0D09E99B6A960511DFAE5,
	Inflater_Inflate_m346D8204009BCDDC5E631C6BB02E0CAE94265293,
	Inflater_get_IsNeedingInput_m8C2A1335BF0AE2B9611E2B4EC8C29EF30838E861,
	Inflater_get_IsNeedingDictionary_mBF2600CD449D3C67812B71BD8142B26E40957949,
	Inflater_get_IsFinished_mEE0EDD8F0042A9F983434FEA706673C0128D27B7,
	Inflater_get_TotalOut_m404FF486E2B5770EF4CC5AC0A97EB96F8C9CFB7F,
	Inflater_get_RemainingInput_mF83D61DB9F0B6E7271DCD7DBEFABE227F31F539D,
	Inflater__cctor_mB4CB7271CD820549DE671F90FE0BE74485EEB45E,
	DeflaterOutputStream__ctor_mB1261970E9121772DD6F12FD398D83CDBCEA2115,
	DeflaterOutputStream_Finish_m06EA2E87DD31088E67860556666E0D789BF753E3,
	DeflaterOutputStream_get_IsStreamOwner_m553DBD4E391EA788C37D11359521100FE87D5731,
	DeflaterOutputStream_EncryptBlock_m032C850D6AB2A2A2EE00DC18E25FD62115D335CA,
	DeflaterOutputStream_Deflate_m12969E1E0DA40E05575E172F25A6DAAA212E9412,
	DeflaterOutputStream_Deflate_mCC807ED3FFEBF1FCB97B4C708AA7191DE35D8599,
	DeflaterOutputStream_get_CanRead_m8C38B293B34D3F77204473350DBA9F9094FC0CD7,
	DeflaterOutputStream_get_CanSeek_mA2D35AD53B6CF522D001F3F9FA54558B0EB54DB8,
	DeflaterOutputStream_get_CanWrite_m1592C8B8C1DE13791D05CC233AE1012259375528,
	DeflaterOutputStream_get_Length_m1A13A217D4FD4EA5E62123E87D801F11483FE5FC,
	DeflaterOutputStream_get_Position_m9D6382BEB6E7E27FA7580BC7CDCAD6E4C0BE0906,
	DeflaterOutputStream_set_Position_m95D7D85435EBAD181FE4D236FCAD10EA4641D7DF,
	DeflaterOutputStream_Seek_m053FCC3919464D1B69EAE8F21D9A0BEED3A46317,
	DeflaterOutputStream_ReadByte_mAEB07D1CE229951DD765ECED7AFFBF511CF41450,
	DeflaterOutputStream_Read_mB730D90D6AB1803DCD96432F1A5596345B7380D7,
	DeflaterOutputStream_Flush_m6D11C38708A6B9271685D504D55208DADA0E7DE5,
	DeflaterOutputStream_Dispose_m5845CA008DC28D8579B9D2DBDFCE979F3FF26430,
	DeflaterOutputStream_GetAuthCodeIfAES_mD864AF782ABC637DDA246CBA918556EBFDDE1F49,
	DeflaterOutputStream_WriteByte_m40CFC3E4BED9A9CA6E7558648D2B3BF41AD57504,
	DeflaterOutputStream_Write_m1D628B819635B5BBED43EA3476F668B5B4D280C4,
	DeflaterOutputStream__cctor_m321272A71F03117DB57AD61B055B6A94AF0A5AB1,
	StreamManipulator_PeekBits_m3029F53241B4173314904FE21E62467655866569,
	StreamManipulator_TryGetBits_m5D7356FD3192830E9CF2FF644E2C861D5D9E2D3D,
	StreamManipulator_TryGetBits_mDE96F81CCE5B6B45746ADCF3B9895CE287F24EF3,
	StreamManipulator_DropBits_m664F5274D56C5C97016B430173698BF43A79F71E,
	StreamManipulator_get_AvailableBits_mCE6733DCB49FAD7A6178B9B83600A42E56EA5FFF,
	StreamManipulator_get_AvailableBytes_m0A3C9739CFFC18E31F012C43B6263418D24104B8,
	StreamManipulator_SkipToByteBoundary_m973AD4D90AE3F74C1C060A66CCA15DE1FD7F51A0,
	StreamManipulator_get_IsNeedingInput_m7128D6EF06AC2C4DA0B29F0A8491F02738E1D6D9,
	StreamManipulator_CopyBytes_m5FD5042F40739DB0C7C5A252863B230F54B2F5A9,
	StreamManipulator_Reset_m356D7FDDD19521BCBF07184CA7A88BACEFB77DD8,
	StreamManipulator_SetInput_m349E809D9DFAC989395E08B8658412CB7F6B7888,
	StreamManipulator__ctor_m281AA9FFAE44EB3FF205353102BB993404799DF4,
	InflaterInputBuffer__ctor_m7FB77C4A9E9A2B7DE1B60F30D9099CEFED743B0B,
	InflaterInputBuffer_get_Available_mC51EF33380C222268AAE7974CEA404DA47FBF4BE,
	InflaterInputBuffer_set_Available_m39985FB6AAC6E3EF6220C3D7016446E723B4171E,
	InflaterInputBuffer_SetInflaterInput_mB3BC966CA28AA9B88716E79BBEBD095C87F8DFB1,
	InflaterInputBuffer_Fill_mA262624C815193E4630117A6768C9FEBB7061A04,
	InflaterInputBuffer_ReadClearTextBuffer_mB4650EB155309D2BB20C94BA7DF9B02A73AAC85D,
	InflaterInputBuffer_ReadLeByte_mEB2B68A9EF28CA25FF86129E5E13B8E4FD87D4BA,
	InflaterInputStream__ctor_m9D2A99DC39904A79DAA49139A61E9EB7AF3C545E,
	InflaterInputStream_get_IsStreamOwner_m29F7FBF7749E5641F843EE0559F06C230E53C00B,
	InflaterInputStream_Fill_m2696D717D791862FCBE7D5676ED1C51A67999B28,
	InflaterInputStream_get_CanRead_mAA0FB2F549A8F728D023BE8586EBD8E9635A4C3C,
	InflaterInputStream_get_CanSeek_mF0A0653CDC23019FFAAF68D7C7159DC6313C74CB,
	InflaterInputStream_get_CanWrite_m71B8B21CA09E583D877FA0917161C597D90FC60F,
	InflaterInputStream_get_Length_mE3E432D72C9E4A7503B56699BBB1B1DB53185A3A,
	InflaterInputStream_get_Position_mF4915CBC215376A6A52C96D12AA6D53DABF96EE2,
	InflaterInputStream_set_Position_mE6A05914B313BCE2143BBE878B10769BC6470C60,
	InflaterInputStream_Flush_m2AB84DA85A9219AA52648759EC03D7B33BADB23D,
	InflaterInputStream_Seek_mC92C41A9130B2259775EB0D5459E98BBDF30FF46,
	InflaterInputStream_Write_mDA15E5F732AB2519A778FBC8E0BD264664CBDA32,
	InflaterInputStream_WriteByte_m62A4D3B6E7BBDDA1F0055A357D4CA8D0CB274A8A,
	InflaterInputStream_Dispose_mFF716157D79A4AECA6160DFC0071DBC882103A69,
	InflaterInputStream_Read_m9B13CD881985CF994D08FE6FBF621ACDB650844B,
	OutputWindow_Write_m30C4FE7DAC167F268A1FC791209BFEEFC4A21BA5,
	OutputWindow_SlowRepeat_m2E9F479A3F19672244FA83CB9D98A554AA0EA2AB,
	OutputWindow_Repeat_m85715F8EEC2D508C2DA7A9E857AB77773F28D0BC,
	OutputWindow_CopyStored_m7AC0D8A03B77E0A8FEE63B11661AF9FF4FAC68D5,
	OutputWindow_GetFreeSpace_m74D8C5035C83AD95D966251F055A5739E0271A4B,
	OutputWindow_GetAvailable_m2C5AAC58C94FCD1424FE4279C6EBBAF5079DE177,
	OutputWindow_CopyOutput_mBE47FDBC54F5EDE41764B323AA50D22B33A815C4,
	OutputWindow_Reset_m22BD6F8F80797AF0C87C83D31993EE44EA427323,
	OutputWindow__ctor_m17E0FB5A61A2FDC9AD888A1FB8003BE61AC0D697,
	GZipInputStream__ctor_m95AA608A7AB2A7F6B7B474AE573F7A4BC01BFA24,
	GZipInputStream__ctor_mB96AD468B784F5A701CCE8CE52386EEE35191F04,
	GZipInputStream_Read_m0B93E0D361CD6E673AEF0E304FD1DE42C8C7E2FA,
	GZipInputStream_ReadHeader_mEC04BF0DA6E09714A4267F04395C4209CCDC3713,
	GZipInputStream_ReadFooter_m758A0BC59B4C92174AB2C68093AA889773191B30,
	GZipOutputStream__ctor_m5AAC3AAE319ECFE9900F9DF2A5C98767E841191A,
	GZipOutputStream__ctor_mE2209BAF129080B4C76FA979B3291F9DF7D2D3FE,
	GZipOutputStream_Write_m3F6FDB5EEF5FD2BE84F958721F5930966F20DF26,
	GZipOutputStream_Dispose_mCE77A83345EAB71437E9A27531F53980B8CB3B61,
	GZipOutputStream_Finish_m0597338A3DCA06C31E37602606B93C940D865807,
	GZipOutputStream_WriteHeader_m278636143B02AEF73522AD196644FBBDB2C4FA6A,
	GZipException__ctor_m570FEF730A0A7CBD0984113BBC4D80613C3A5845,
	GZipException__ctor_mBFE9328C44E5B5F41B4FB93477FF7B47A5C55806,
	GZipException__ctor_m91989114A82D905BAA991430896E5C574E8D69FF,
};
static const int32_t s_InvokerIndices[198] = 
{
	0,
	0,
	0,
	23,
	26,
	120,
	26,
	23,
	120,
	23,
	26,
	120,
	23,
	23,
	187,
	32,
	2955,
	3,
	23,
	23,
	187,
	2955,
	3,
	89,
	26,
	14,
	14,
	14,
	3,
	32,
	23,
	89,
	89,
	23,
	14,
	14,
	14,
	3,
	3,
	26,
	26,
	121,
	32,
	23,
	32,
	35,
	10,
	23,
	136,
	32,
	89,
	504,
	102,
	657,
	35,
	89,
	23,
	23,
	10,
	187,
	32,
	32,
	23,
	23,
	10,
	23,
	30,
	657,
	657,
	657,
	23,
	3,
	26,
	23,
	32,
	23,
	1034,
	1034,
	89,
	30,
	52,
	224,
	21,
	21,
	207,
	23,
	32,
	27,
	23,
	23,
	10,
	26,
	26,
	26,
	140,
	23,
	187,
	23,
	23,
	89,
	89,
	35,
	32,
	32,
	504,
	31,
	23,
	89,
	89,
	89,
	89,
	89,
	35,
	504,
	89,
	89,
	89,
	187,
	10,
	3,
	110,
	23,
	89,
	35,
	23,
	31,
	89,
	89,
	89,
	187,
	187,
	213,
	732,
	10,
	504,
	23,
	31,
	23,
	31,
	35,
	3,
	37,
	2956,
	2956,
	32,
	10,
	10,
	23,
	89,
	504,
	23,
	35,
	23,
	137,
	10,
	32,
	26,
	23,
	504,
	10,
	110,
	89,
	23,
	89,
	89,
	89,
	187,
	187,
	213,
	23,
	732,
	35,
	31,
	31,
	504,
	32,
	38,
	136,
	503,
	10,
	10,
	504,
	23,
	23,
	26,
	137,
	504,
	89,
	23,
	26,
	137,
	35,
	31,
	23,
	23,
	23,
	26,
	120,
};
extern const Il2CppCodeGenModule g_FMSharpZipCodeGenModule;
const Il2CppCodeGenModule g_FMSharpZipCodeGenModule = 
{
	"FMSharpZip.dll",
	198,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
