﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.IO.MemoryStream WebSocketSharp.Ext::compress(System.IO.Stream)
extern void Ext_compress_mFF2386B72F7575399F75588F6E3C1C984D26490A (void);
// 0x00000002 System.Byte[] WebSocketSharp.Ext::decompress(System.Byte[])
extern void Ext_decompress_m92AE0E65DCF5B64050FB33F6C849F15AC39F2BCA (void);
// 0x00000003 System.IO.MemoryStream WebSocketSharp.Ext::decompress(System.IO.Stream)
extern void Ext_decompress_m2F10FEFF7998061ECE575378251BD96B5A95408D (void);
// 0x00000004 System.Byte[] WebSocketSharp.Ext::decompressToArray(System.IO.Stream)
extern void Ext_decompressToArray_mA55F0BFC13285DF96BD11DDC5D260BD253CF90EE (void);
// 0x00000005 System.Byte[] WebSocketSharp.Ext::Append(System.UInt16,System.String)
extern void Ext_Append_m4FA13F20B7E3A7A864BC88491735A9167E9A8503 (void);
// 0x00000006 System.IO.Stream WebSocketSharp.Ext::Compress(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_Compress_mE7112B59E9B39467AFF41EA993663FE74F693BBA (void);
// 0x00000007 System.Boolean WebSocketSharp.Ext::Contains(System.String,System.Char[])
extern void Ext_Contains_m468B61CA7D664E5CEDE17145AFB6F2CA56959E3F (void);
// 0x00000008 System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Specialized.NameValueCollection,System.String,System.String,System.StringComparison)
extern void Ext_Contains_m2BDB1B6B268DF96B7C2BD8CF43FEA867AE249729 (void);
// 0x00000009 System.Boolean WebSocketSharp.Ext::Contains(System.Collections.Generic.IEnumerable`1<T>,System.Func`2<T,System.Boolean>)
// 0x0000000A System.Boolean WebSocketSharp.Ext::ContainsTwice(System.String[])
extern void Ext_ContainsTwice_m616A23353EE47259F5CB47E5C3CC6DA68D32A0C6 (void);
// 0x0000000B System.Void WebSocketSharp.Ext::CopyTo(System.IO.Stream,System.IO.Stream,System.Int32)
extern void Ext_CopyTo_m450F90EDB45896AA6596D30766ECDEDE9FCC5457 (void);
// 0x0000000C System.Byte[] WebSocketSharp.Ext::Decompress(System.Byte[],WebSocketSharp.CompressionMethod)
extern void Ext_Decompress_m0E21E4E11FFEB50F172B160165BE817AE130601B (void);
// 0x0000000D System.Byte[] WebSocketSharp.Ext::DecompressToArray(System.IO.Stream,WebSocketSharp.CompressionMethod)
extern void Ext_DecompressToArray_m65411E96820D478E034900AE18E3747B868A6B1E (void);
// 0x0000000E System.Void WebSocketSharp.Ext::Emit(System.EventHandler,System.Object,System.EventArgs)
extern void Ext_Emit_m79C1C2BA565E53B878BF76C87844E0022502C99F (void);
// 0x0000000F System.Void WebSocketSharp.Ext::Emit(System.EventHandler`1<TEventArgs>,System.Object,TEventArgs)
// 0x00000010 System.Boolean WebSocketSharp.Ext::EqualsWith(System.Int32,System.Char,System.Action`1<System.Int32>)
extern void Ext_EqualsWith_m453D92B0326540A985F6C718D1E1DDA71FEF1240 (void);
// 0x00000011 System.String WebSocketSharp.Ext::GetAbsolutePath(System.Uri)
extern void Ext_GetAbsolutePath_m25DE5774315CF299178EBA6A1F8B99BCA94AD5BF (void);
// 0x00000012 WebSocketSharp.Net.CookieCollection WebSocketSharp.Ext::GetCookies(System.Collections.Specialized.NameValueCollection,System.Boolean)
extern void Ext_GetCookies_m741901826D65BA10CA3ADF6436BEE0290B47E42B (void);
// 0x00000013 System.String WebSocketSharp.Ext::GetMessage(WebSocketSharp.CloseStatusCode)
extern void Ext_GetMessage_mA6ADF06B7A7EA49BE8751DD416FC653A662E3111 (void);
// 0x00000014 System.Byte[] WebSocketSharp.Ext::GetUTF8EncodedBytes(System.String)
extern void Ext_GetUTF8EncodedBytes_m20B5440F8509FD751A7A53AE89FAD74E5CBD7788 (void);
// 0x00000015 System.String WebSocketSharp.Ext::GetValue(System.String,System.Char,System.Boolean)
extern void Ext_GetValue_mFEEBFBA15B52F78F307754B1D00C45222CD1A2A4 (void);
// 0x00000016 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt16,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_m3F99508A37A0619D30906895C6ECE38030CB4DE8 (void);
// 0x00000017 System.Byte[] WebSocketSharp.Ext::InternalToByteArray(System.UInt64,WebSocketSharp.ByteOrder)
extern void Ext_InternalToByteArray_m168D68DED64267436B0D6D20797F37513DC594B7 (void);
// 0x00000018 System.Boolean WebSocketSharp.Ext::IsCompressionExtension(System.String,WebSocketSharp.CompressionMethod)
extern void Ext_IsCompressionExtension_m91B585169EE04BDC3CDBA39DA61FBDAE33FF390D (void);
// 0x00000019 System.Boolean WebSocketSharp.Ext::IsControl(System.Byte)
extern void Ext_IsControl_m4A4FF9555814C2E44EAB0078CD9ABE519A3AA03D (void);
// 0x0000001A System.Boolean WebSocketSharp.Ext::IsData(System.Byte)
extern void Ext_IsData_m13B213B41ABBE20EAD75F88961F2CF8A32AB2CFD (void);
// 0x0000001B System.Boolean WebSocketSharp.Ext::IsData(WebSocketSharp.Opcode)
extern void Ext_IsData_mA4C217749016C4E5E0A32AE4EE6D372723C1F87F (void);
// 0x0000001C System.Boolean WebSocketSharp.Ext::IsReserved(System.UInt16)
extern void Ext_IsReserved_m93BD0ADD3D1136DE3C7F4CECC81EA7F30AFCDFF7 (void);
// 0x0000001D System.Boolean WebSocketSharp.Ext::IsSupported(System.Byte)
extern void Ext_IsSupported_m24DF4666B377F458FD4D50F442CE5CB01D43EA7B (void);
// 0x0000001E System.Boolean WebSocketSharp.Ext::IsText(System.String)
extern void Ext_IsText_m86C585BA822A51E96AC3E1DE949C36CDBCA3051E (void);
// 0x0000001F System.Boolean WebSocketSharp.Ext::IsToken(System.String)
extern void Ext_IsToken_mF9777D58046D57F66FE51DA6068EBC96B17FE9D2 (void);
// 0x00000020 System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int32)
extern void Ext_ReadBytes_m6431F23AF3667D9E2D0E6E68003E5F7278E772B9 (void);
// 0x00000021 System.Byte[] WebSocketSharp.Ext::ReadBytes(System.IO.Stream,System.Int64,System.Int32)
extern void Ext_ReadBytes_mB0A91D33133441E119E3C8D573F393B838FE8A78 (void);
// 0x00000022 System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_m24CD08D1E3004C0AC48080C76676F2DC9B210539 (void);
// 0x00000023 System.Void WebSocketSharp.Ext::ReadBytesAsync(System.IO.Stream,System.Int64,System.Int32,System.Action`1<System.Byte[]>,System.Action`1<System.Exception>)
extern void Ext_ReadBytesAsync_mD883C095A1445643302A7E6E7347243121A5C01A (void);
// 0x00000024 T[] WebSocketSharp.Ext::Reverse(T[])
// 0x00000025 System.Collections.Generic.IEnumerable`1<System.String> WebSocketSharp.Ext::SplitHeaderValue(System.String,System.Char[])
extern void Ext_SplitHeaderValue_mD31D552531654796E0E4F609ED8590FAA1FB4D4B (void);
// 0x00000026 System.Byte[] WebSocketSharp.Ext::ToByteArray(System.IO.Stream)
extern void Ext_ToByteArray_m9786E30EE1DD790929E73B857694D9F7767A759D (void);
// 0x00000027 System.String WebSocketSharp.Ext::ToExtensionString(WebSocketSharp.CompressionMethod,System.String[])
extern void Ext_ToExtensionString_mEB7F6250D25025276211B2B70462A3ABC843FFB2 (void);
// 0x00000028 System.Collections.Generic.List`1<TSource> WebSocketSharp.Ext::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000029 System.UInt16 WebSocketSharp.Ext::ToUInt16(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt16_mA8D2B7C9D6A2BC136B2B95AC7E2F7E3D584C7DB8 (void);
// 0x0000002A System.UInt64 WebSocketSharp.Ext::ToUInt64(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToUInt64_m849E03EE7EFB1F1F9AA0FB96D3404E776ED0B36C (void);
// 0x0000002B System.Boolean WebSocketSharp.Ext::TryCreateWebSocketUri(System.String,System.Uri&,System.String&)
extern void Ext_TryCreateWebSocketUri_m063824BF650BF49B4B72A82B621655F178CC6216 (void);
// 0x0000002C System.Boolean WebSocketSharp.Ext::TryGetUTF8DecodedString(System.Byte[],System.String&)
extern void Ext_TryGetUTF8DecodedString_m1B53E24777D6CAEC191868363B27DE159425923C (void);
// 0x0000002D System.Boolean WebSocketSharp.Ext::TryGetUTF8EncodedBytes(System.String,System.Byte[]&)
extern void Ext_TryGetUTF8EncodedBytes_m80427A7B0B3B9830D7E01E0626D4FC62AE195C45 (void);
// 0x0000002E System.String WebSocketSharp.Ext::Unquote(System.String)
extern void Ext_Unquote_m424EE4B1486884BC3360463EF5610A48CAB697A8 (void);
// 0x0000002F System.Boolean WebSocketSharp.Ext::Upgrades(System.Collections.Specialized.NameValueCollection,System.String)
extern void Ext_Upgrades_m33D72C9CD39201FAB54A32C06939E4E41FBE0CEC (void);
// 0x00000030 System.Void WebSocketSharp.Ext::WriteBytes(System.IO.Stream,System.Byte[],System.Int32)
extern void Ext_WriteBytes_m5856E58AE264D91FADCA7C952996604F584D32EA (void);
// 0x00000031 System.Boolean WebSocketSharp.Ext::IsEnclosedIn(System.String,System.Char)
extern void Ext_IsEnclosedIn_mCA6CE5321CD97EE93D12B82B9E71EEBC1AC6F5AA (void);
// 0x00000032 System.Boolean WebSocketSharp.Ext::IsHostOrder(WebSocketSharp.ByteOrder)
extern void Ext_IsHostOrder_mADCA5FA344E9B1EA5644B19328DCDFF60C0AEC47 (void);
// 0x00000033 System.Boolean WebSocketSharp.Ext::IsNullOrEmpty(System.String)
extern void Ext_IsNullOrEmpty_m459B890885FE2BFC7EC73849927ED35BBAE1B907 (void);
// 0x00000034 System.Boolean WebSocketSharp.Ext::IsPredefinedScheme(System.String)
extern void Ext_IsPredefinedScheme_m4A02B68F03400153CA3D469AFC32B77BF2B470FB (void);
// 0x00000035 System.Boolean WebSocketSharp.Ext::MaybeUri(System.String)
extern void Ext_MaybeUri_m84DB0A02813B60D91A57CA74B10FBC270624E20A (void);
// 0x00000036 T[] WebSocketSharp.Ext::SubArray(T[],System.Int32,System.Int32)
// 0x00000037 T[] WebSocketSharp.Ext::SubArray(T[],System.Int64,System.Int64)
// 0x00000038 System.Byte[] WebSocketSharp.Ext::ToHostOrder(System.Byte[],WebSocketSharp.ByteOrder)
extern void Ext_ToHostOrder_m4B341B05107B49BF842745AEC9B27113A0ECCC1C (void);
// 0x00000039 System.String WebSocketSharp.Ext::ToString(T[],System.String)
// 0x0000003A System.Uri WebSocketSharp.Ext::ToUri(System.String)
extern void Ext_ToUri_m3ECD656039D3E369DDD14B54608FDC910964EC53 (void);
// 0x0000003B System.Void WebSocketSharp.Ext::.cctor()
extern void Ext__cctor_m82BE2DC4CEE777B9F0535D04493B8BBA4AD601F5 (void);
// 0x0000003C System.Void WebSocketSharp.Ext/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m4D95DCC3A614785D3620A9FBBD53EAB65E42C120 (void);
// 0x0000003D System.Boolean WebSocketSharp.Ext/<>c__DisplayClass21_0::<ContainsTwice>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass21_0_U3CContainsTwiceU3Eb__0_mEF444A313D0C8DE0C1533E4DDD9247AADAFE06B8 (void);
// 0x0000003E System.Void WebSocketSharp.Ext/<>c__DisplayClass59_0::.ctor()
extern void U3CU3Ec__DisplayClass59_0__ctor_m6CF091F86167E328AB130BD134BC4379A497C66E (void);
// 0x0000003F System.Void WebSocketSharp.Ext/<>c__DisplayClass59_0::<ReadBytesAsync>b__0(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass59_0_U3CReadBytesAsyncU3Eb__0_mC598FE8327165061E8789168FDD65F14CF34E40F (void);
// 0x00000040 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_0::.ctor()
extern void U3CU3Ec__DisplayClass60_0__ctor_m90B9DFB6D1D8EB5F79A7E27DF4550CA7AAD36F40 (void);
// 0x00000041 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_0::<ReadBytesAsync>b__0(System.Int64)
extern void U3CU3Ec__DisplayClass60_0_U3CReadBytesAsyncU3Eb__0_mA94324D78097EFE93C6FEAC0ABD355B0FD31DF38 (void);
// 0x00000042 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_1::.ctor()
extern void U3CU3Ec__DisplayClass60_1__ctor_m0A0C7AFC9E9833FA308E5B51B218A0E489C63235 (void);
// 0x00000043 System.Void WebSocketSharp.Ext/<>c__DisplayClass60_1::<ReadBytesAsync>b__1(System.IAsyncResult)
extern void U3CU3Ec__DisplayClass60_1_U3CReadBytesAsyncU3Eb__1_m1590B1FE581500ED3BAA2926D51352D21B38C140 (void);
// 0x00000044 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::.ctor(System.Int32)
extern void U3CSplitHeaderValueU3Ed__62__ctor_m4947AD83FCF1E7D05FFB1A5A7AE6BDA6B8B61636 (void);
// 0x00000045 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.IDisposable.Dispose()
extern void U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m485A81956BF2962DC4C5756DED791F8C7E7CF296 (void);
// 0x00000046 System.Boolean WebSocketSharp.Ext/<SplitHeaderValue>d__62::MoveNext()
extern void U3CSplitHeaderValueU3Ed__62_MoveNext_mF5F340FDF2E8293C4F8FE2686C7AA388080B6CFE (void);
// 0x00000047 System.String WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.Generic.IEnumerator<System.String>.get_Current()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mF988C53F31C9F5B42F4F5F04155156F523393EB7 (void);
// 0x00000048 System.Void WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerator.Reset()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m2B1FD2E85EC99BBCBE67688F13EC518BD0354DA9 (void);
// 0x00000049 System.Object WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_m1E4C862449F6B389CBB48FAADC2A48B7B0BAAD16 (void);
// 0x0000004A System.Collections.Generic.IEnumerator`1<System.String> WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.Generic.IEnumerable<System.String>.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m8B0217989B5DAF54298793065C0850A7E564636D (void);
// 0x0000004B System.Collections.IEnumerator WebSocketSharp.Ext/<SplitHeaderValue>d__62::System.Collections.IEnumerable.GetEnumerator()
extern void U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mB0FEE593E87A323370D786D610EC1B05BCA963B9 (void);
// 0x0000004C System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.WebSocketFrame)
extern void MessageEventArgs__ctor_m1678081D31D7CD4B692E9608FB9E7B83D091FD1B (void);
// 0x0000004D System.Void WebSocketSharp.MessageEventArgs::.ctor(WebSocketSharp.Opcode,System.Byte[])
extern void MessageEventArgs__ctor_m708E0D41654ED75BB7871A33B1B3E822783BEEFC (void);
// 0x0000004E System.String WebSocketSharp.MessageEventArgs::get_Data()
extern void MessageEventArgs_get_Data_mFEC4784B676BB8FF003998987406CD3F97611A24 (void);
// 0x0000004F System.Void WebSocketSharp.MessageEventArgs::setData()
extern void MessageEventArgs_setData_m2EA766C5D004AD5A87725A928F2F0527ACCF1D78 (void);
// 0x00000050 System.Void WebSocketSharp.CloseEventArgs::.ctor(WebSocketSharp.PayloadData,System.Boolean)
extern void CloseEventArgs__ctor_m4EBC593FB0CBF76A010514143E2697FDF6F91B11 (void);
// 0x00000051 System.Void WebSocketSharp.ErrorEventArgs::.ctor(System.String,System.Exception)
extern void ErrorEventArgs__ctor_m12DC0127CF8C6C9C17200421E6EBC3C87CD371F1 (void);
// 0x00000052 System.Void WebSocketSharp.WebSocket::.cctor()
extern void WebSocket__cctor_m43A6D37B49988E5A926C9F28A07D5431443E7E18 (void);
// 0x00000053 System.Void WebSocketSharp.WebSocket::.ctor(System.String,System.String[])
extern void WebSocket__ctor_mF920780429570048096CE4C53E52047BDB4A96E3 (void);
// 0x00000054 System.Boolean WebSocketSharp.WebSocket::get_HasMessage()
extern void WebSocket_get_HasMessage_mC65BD115FB02B6CEEECD554DBDE13C3AB3A8D167 (void);
// 0x00000055 WebSocketSharp.WebSocketState WebSocketSharp.WebSocket::get_ReadyState()
extern void WebSocket_get_ReadyState_m6EC9CAA4FC7E05324A5511A636ABBE7D246CD205 (void);
// 0x00000056 WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::get_SslConfiguration()
extern void WebSocket_get_SslConfiguration_m3842ED20C16DE807EBABDACCA1601FFD9E86C52F (void);
// 0x00000057 System.Void WebSocketSharp.WebSocket::add_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern void WebSocket_add_OnClose_m87DA5064206524E18A2E53398D4E5C0E764247EC (void);
// 0x00000058 System.Void WebSocketSharp.WebSocket::remove_OnClose(System.EventHandler`1<WebSocketSharp.CloseEventArgs>)
extern void WebSocket_remove_OnClose_m550C4E629A1DDE62423D99F4FF614365E0FF83C3 (void);
// 0x00000059 System.Void WebSocketSharp.WebSocket::add_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern void WebSocket_add_OnError_mD7A6AB282F3FA80E678B630F5A4252AB5F44270E (void);
// 0x0000005A System.Void WebSocketSharp.WebSocket::remove_OnError(System.EventHandler`1<WebSocketSharp.ErrorEventArgs>)
extern void WebSocket_remove_OnError_m499E42EAC4E822756FF619AB8F79EDC6FAC33533 (void);
// 0x0000005B System.Void WebSocketSharp.WebSocket::add_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_add_OnMessage_m302E620ACF771C3E9484820E86A716F491B3A114 (void);
// 0x0000005C System.Void WebSocketSharp.WebSocket::remove_OnMessage(System.EventHandler`1<WebSocketSharp.MessageEventArgs>)
extern void WebSocket_remove_OnMessage_mA8746E68E24230FDACD7DBEB06BB88CDD22BA294 (void);
// 0x0000005D System.Void WebSocketSharp.WebSocket::add_OnOpen(System.EventHandler)
extern void WebSocket_add_OnOpen_m094F4D363E4DD05F673B24DD08B3E65D34A3DD56 (void);
// 0x0000005E System.Void WebSocketSharp.WebSocket::remove_OnOpen(System.EventHandler)
extern void WebSocket_remove_OnOpen_m0489D4798A1F7A7505B2221F080AE793814C25C9 (void);
// 0x0000005F System.Boolean WebSocketSharp.WebSocket::checkHandshakeResponse(WebSocketSharp.HttpResponse,System.String&)
extern void WebSocket_checkHandshakeResponse_m89E29EE43D10B0BA774BD8A1B5758E70BEE3632F (void);
// 0x00000060 System.Boolean WebSocketSharp.WebSocket::checkProtocols(System.String[],System.String&)
extern void WebSocket_checkProtocols_mFD547A5A23DB78291BB972EC241282FE57F084BA (void);
// 0x00000061 System.Boolean WebSocketSharp.WebSocket::checkReceivedFrame(WebSocketSharp.WebSocketFrame,System.String&)
extern void WebSocket_checkReceivedFrame_m1D5AF13E27983E50BCC4A726A459237B5178CCE6 (void);
// 0x00000062 System.Void WebSocketSharp.WebSocket::close(System.UInt16,System.String)
extern void WebSocket_close_m6E08B4EB032759DB499E454471D60A4B1671D6B6 (void);
// 0x00000063 System.Void WebSocketSharp.WebSocket::close(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_close_m75F44D7E0440AE608D0DE690B76DAB06D5C2BB32 (void);
// 0x00000064 System.Boolean WebSocketSharp.WebSocket::closeHandshake(WebSocketSharp.PayloadData,System.Boolean,System.Boolean,System.Boolean)
extern void WebSocket_closeHandshake_mED49C83540A15079ED6232C5CDD790978ACE2870 (void);
// 0x00000065 System.Boolean WebSocketSharp.WebSocket::connect()
extern void WebSocket_connect_m3DED6601C29145009A4379169E5D95D08A9B9994 (void);
// 0x00000066 System.String WebSocketSharp.WebSocket::createExtensions()
extern void WebSocket_createExtensions_mC18C77C4FBF3BBAB2D690E556217E2BE3BAAEAD5 (void);
// 0x00000067 WebSocketSharp.HttpRequest WebSocketSharp.WebSocket::createHandshakeRequest()
extern void WebSocket_createHandshakeRequest_mB53780ED31913EBC94D335203B944AA6770AEEC8 (void);
// 0x00000068 System.Void WebSocketSharp.WebSocket::doHandshake()
extern void WebSocket_doHandshake_m88CA65138730B519469C6344E2C3B9DEB6BF6443 (void);
// 0x00000069 System.Void WebSocketSharp.WebSocket::enqueueToMessageEventQueue(WebSocketSharp.MessageEventArgs)
extern void WebSocket_enqueueToMessageEventQueue_m7A3B74DCB04612793239E63604235944DABE3554 (void);
// 0x0000006A System.Void WebSocketSharp.WebSocket::error(System.String,System.Exception)
extern void WebSocket_error_m476B8617EC5262BF91D8BE45E7AAF9E5F58818BD (void);
// 0x0000006B System.Void WebSocketSharp.WebSocket::fatal(System.String,System.Exception)
extern void WebSocket_fatal_m92DB97F5F3B0B7CD1C2F910E94AA86B126CCF66A (void);
// 0x0000006C System.Void WebSocketSharp.WebSocket::fatal(System.String,System.UInt16)
extern void WebSocket_fatal_m52D64EAC64042C2D118081F76208FDE74F47C8B8 (void);
// 0x0000006D System.Void WebSocketSharp.WebSocket::fatal(System.String,WebSocketSharp.CloseStatusCode)
extern void WebSocket_fatal_mD576696A36A8E220915D076BCF1DFB8137278842 (void);
// 0x0000006E WebSocketSharp.Net.ClientSslConfiguration WebSocketSharp.WebSocket::getSslConfiguration()
extern void WebSocket_getSslConfiguration_m58728D2C50138B4BD550042AD972C953CFE7C0FB (void);
// 0x0000006F System.Void WebSocketSharp.WebSocket::init()
extern void WebSocket_init_mF0F10F1C6278FF997F63DB76DC94656E0982687E (void);
// 0x00000070 System.Void WebSocketSharp.WebSocket::message()
extern void WebSocket_message_m60D5929947E7F99F3A6BF16AF772C34DEE049B26 (void);
// 0x00000071 System.Void WebSocketSharp.WebSocket::messagec(WebSocketSharp.MessageEventArgs)
extern void WebSocket_messagec_m6E35DD1FA27A0E4246214EE1BF3001A6ACDC0DF6 (void);
// 0x00000072 System.Void WebSocketSharp.WebSocket::open()
extern void WebSocket_open_m073654D707503FF174619367143CB424BDF5D3C5 (void);
// 0x00000073 System.Boolean WebSocketSharp.WebSocket::processCloseFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processCloseFrame_m6C3E7E4B10079C4EBBB8ABEBC2F38AFC27915C6C (void);
// 0x00000074 System.Void WebSocketSharp.WebSocket::processCookies(WebSocketSharp.Net.CookieCollection)
extern void WebSocket_processCookies_m46F0A73A853B8E0EFE166CDD21A39E7F3F60F213 (void);
// 0x00000075 System.Boolean WebSocketSharp.WebSocket::processDataFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processDataFrame_mA6B8C27D98B98E87F3B41616ED799189666C968C (void);
// 0x00000076 System.Boolean WebSocketSharp.WebSocket::processFragmentFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processFragmentFrame_mC3DCB3DB694188974E320E9308F5B8651248F729 (void);
// 0x00000077 System.Boolean WebSocketSharp.WebSocket::processPingFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPingFrame_mA820D830C477EF97D2DDC9E3DB2D0ED3BDAB9EE0 (void);
// 0x00000078 System.Boolean WebSocketSharp.WebSocket::processPongFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processPongFrame_m0E4BDFEA37639774C9C9936AD391E8D5523C7470 (void);
// 0x00000079 System.Boolean WebSocketSharp.WebSocket::processReceivedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processReceivedFrame_m3750D43D5A16FE2D88ABAC40458C95A114B2C48A (void);
// 0x0000007A System.Void WebSocketSharp.WebSocket::processSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_processSecWebSocketExtensionsServerHeader_m31A1F42B0BF48C53F6EFDDBA155B07EF8304CD0C (void);
// 0x0000007B System.Boolean WebSocketSharp.WebSocket::processUnsupportedFrame(WebSocketSharp.WebSocketFrame)
extern void WebSocket_processUnsupportedFrame_m11BEAB3A278652B2318383C6604C291EA8B9BE37 (void);
// 0x0000007C System.Void WebSocketSharp.WebSocket::releaseClientResources()
extern void WebSocket_releaseClientResources_m012236D1A15E0D5272115759B2AB1BFE0C34C78C (void);
// 0x0000007D System.Void WebSocketSharp.WebSocket::releaseCommonResources()
extern void WebSocket_releaseCommonResources_m2C48EB30FCCC0A62E18B22ACD2EE5C1356CD590C (void);
// 0x0000007E System.Void WebSocketSharp.WebSocket::releaseResources()
extern void WebSocket_releaseResources_m337E04A4C465E91FE06C9A622CD43295BFE42C5A (void);
// 0x0000007F System.Void WebSocketSharp.WebSocket::releaseServerResources()
extern void WebSocket_releaseServerResources_m97AEA3904AB71BCEB67095064C0F45C06357F64C (void);
// 0x00000080 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream)
extern void WebSocket_send_m60E4951F34AF2663FAA8B347C842FDD64C3B479C (void);
// 0x00000081 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Opcode,System.IO.Stream,System.Boolean)
extern void WebSocket_send_mEEB5585DB1485647F06B044CE86634F6CDE99D25 (void);
// 0x00000082 System.Boolean WebSocketSharp.WebSocket::send(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean)
extern void WebSocket_send_mEEB96347B838FBB42716F4B5C72547F58E84BBC8 (void);
// 0x00000083 System.Boolean WebSocketSharp.WebSocket::sendBytes(System.Byte[])
extern void WebSocket_sendBytes_m4E9702DB4E947B7E32862D0CAA6204DAF057DAC7 (void);
// 0x00000084 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHandshakeRequest()
extern void WebSocket_sendHandshakeRequest_m0D51578A6F735BF225C2DBD26C8D0CEC27CBC8E9 (void);
// 0x00000085 WebSocketSharp.HttpResponse WebSocketSharp.WebSocket::sendHttpRequest(WebSocketSharp.HttpRequest,System.Int32)
extern void WebSocket_sendHttpRequest_m8F39E3875D617905195C12EE3FAE773847858B8C (void);
// 0x00000086 System.Void WebSocketSharp.WebSocket::sendProxyConnectRequest()
extern void WebSocket_sendProxyConnectRequest_m4012D0E637058DAF072410021411C70DC6C2E565 (void);
// 0x00000087 System.Void WebSocketSharp.WebSocket::setClientStream()
extern void WebSocket_setClientStream_m31358BF98AC0D365EF190B7FF0CE422F50AF60E2 (void);
// 0x00000088 System.Void WebSocketSharp.WebSocket::startReceiving()
extern void WebSocket_startReceiving_mEC0B934892E7AA5260579421AE08AAD88A712B22 (void);
// 0x00000089 System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketAcceptHeader(System.String)
extern void WebSocket_validateSecWebSocketAcceptHeader_m3FE2B3B24BCECB3CC9799C334BA04964B3BC94DE (void);
// 0x0000008A System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketExtensionsServerHeader(System.String)
extern void WebSocket_validateSecWebSocketExtensionsServerHeader_mCD01F80B3AB645D7F37DC2646CFACEFA92FBA7AC (void);
// 0x0000008B System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketProtocolServerHeader(System.String)
extern void WebSocket_validateSecWebSocketProtocolServerHeader_mF1D7F3AF8ACB1A3333607ADA844771F14FB97CB7 (void);
// 0x0000008C System.Boolean WebSocketSharp.WebSocket::validateSecWebSocketVersionServerHeader(System.String)
extern void WebSocket_validateSecWebSocketVersionServerHeader_mD210DB2EC20908262EBE8A1DE6EC1B386B24B7CB (void);
// 0x0000008D System.String WebSocketSharp.WebSocket::CreateBase64Key()
extern void WebSocket_CreateBase64Key_m99849C279F55F47E3B75588648B70E2DE0705129 (void);
// 0x0000008E System.String WebSocketSharp.WebSocket::CreateResponseKey(System.String)
extern void WebSocket_CreateResponseKey_m2C91C613261CBF7622381B23D28EC53E17EC1D25 (void);
// 0x0000008F System.Void WebSocketSharp.WebSocket::Close()
extern void WebSocket_Close_mC5B5E2D838145811D2C0B95239C06F27830B2457 (void);
// 0x00000090 System.Void WebSocketSharp.WebSocket::Connect()
extern void WebSocket_Connect_mF39921089B05F788F74C2D4F302F734D694655CB (void);
// 0x00000091 System.Void WebSocketSharp.WebSocket::Send(System.String)
extern void WebSocket_Send_mEDFB10DEBA56C1EDBCBE946A35483CB33DD668E7 (void);
// 0x00000092 System.Void WebSocketSharp.WebSocket::System.IDisposable.Dispose()
extern void WebSocket_System_IDisposable_Dispose_m88E2268CF6E7677361A64E1182EA3C75F28C1B69 (void);
// 0x00000093 System.Void WebSocketSharp.WebSocket::<open>b__146_0(System.IAsyncResult)
extern void WebSocket_U3CopenU3Eb__146_0_mF9501C7E369ACF5B42D12F621C8AD99EDD791D16 (void);
// 0x00000094 System.Void WebSocketSharp.WebSocket/<>c::.cctor()
extern void U3CU3Ec__cctor_m997D0CC0E78135A5F4D22F841C9C4036C5BD802A (void);
// 0x00000095 System.Void WebSocketSharp.WebSocket/<>c::.ctor()
extern void U3CU3Ec__ctor_mC77D1EB2199A666AF11F6D450E4284B48EDFB80E (void);
// 0x00000096 System.Boolean WebSocketSharp.WebSocket/<>c::<checkProtocols>b__120_0(System.String)
extern void U3CU3Ec_U3CcheckProtocolsU3Eb__120_0_mBB5AEEEBF723C8139AC88D3C6443A62B1EF6D1F8 (void);
// 0x00000097 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::.ctor()
extern void U3CU3Ec__DisplayClass174_0__ctor_mFD40AE931F1EB441AE8E4F4A57A4F07F1A9695AC (void);
// 0x00000098 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__0()
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__0_mE022932040D7BCFEC430F2B3CC66AE4968BE3795 (void);
// 0x00000099 System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__1_m0DE13D272693941D43FBA626C3D0E4687774224D (void);
// 0x0000009A System.Void WebSocketSharp.WebSocket/<>c__DisplayClass174_0::<startReceiving>b__2(System.Exception)
extern void U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__2_mF4EAAED569908D918BFED1EB89121051DE23F774 (void);
// 0x0000009B System.Void WebSocketSharp.WebSocket/<>c__DisplayClass176_0::.ctor()
extern void U3CU3Ec__DisplayClass176_0__ctor_mBBE2A8FDEC5CAFC8650309CFEB6DC30298ECFC6F (void);
// 0x0000009C System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass176_0::<validateSecWebSocketExtensionsServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass176_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_mEF2C8B7989241C284625F1EEFAECB6233B2CAA87 (void);
// 0x0000009D System.Void WebSocketSharp.WebSocket/<>c__DisplayClass177_0::.ctor()
extern void U3CU3Ec__DisplayClass177_0__ctor_m9688AF2B60774E0438CD1B9967A6531D8E56B643 (void);
// 0x0000009E System.Boolean WebSocketSharp.WebSocket/<>c__DisplayClass177_0::<validateSecWebSocketProtocolServerHeader>b__0(System.String)
extern void U3CU3Ec__DisplayClass177_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_m4B896A77D7AE967FC9489116E821CE4CB32CB25E (void);
// 0x0000009F System.Void WebSocketSharp.PayloadData::.cctor()
extern void PayloadData__cctor_mCC0B4C41AB15F8A8FF2DFD8B5A766C3E09D413FC (void);
// 0x000000A0 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[])
extern void PayloadData__ctor_m8C2F6B3C49CA40C5E49CC4F4AA4C5C3549536059 (void);
// 0x000000A1 System.Void WebSocketSharp.PayloadData::.ctor(System.Byte[],System.Int64)
extern void PayloadData__ctor_mFE0A9E867715E7797A60FE2098281B6C842FF3C5 (void);
// 0x000000A2 System.Void WebSocketSharp.PayloadData::.ctor(System.UInt16,System.String)
extern void PayloadData__ctor_m16D098A88C53C43F0F6B95D66766116D403D1290 (void);
// 0x000000A3 System.UInt16 WebSocketSharp.PayloadData::get_Code()
extern void PayloadData_get_Code_m08AEA1D6C72194E60F367163B348ACFBB0D8A2B5 (void);
// 0x000000A4 System.Boolean WebSocketSharp.PayloadData::get_HasReservedCode()
extern void PayloadData_get_HasReservedCode_mD224366677D4EDA15D1DDB51568152680D4657FD (void);
// 0x000000A5 System.Byte[] WebSocketSharp.PayloadData::get_ApplicationData()
extern void PayloadData_get_ApplicationData_m5DCA2540F328CDB2CD4654F46E6D16BA06721F1B (void);
// 0x000000A6 System.UInt64 WebSocketSharp.PayloadData::get_Length()
extern void PayloadData_get_Length_mF4AE5CDDE931D807598A8C3700C54DBDB7479326 (void);
// 0x000000A7 System.Void WebSocketSharp.PayloadData::Mask(System.Byte[])
extern void PayloadData_Mask_m21B14F9AF19A81CFC728368916F9C79520821A7F (void);
// 0x000000A8 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.PayloadData::GetEnumerator()
extern void PayloadData_GetEnumerator_m0A78BE9E677E27E7BD6D93BD1656E6A36585117C (void);
// 0x000000A9 System.Byte[] WebSocketSharp.PayloadData::ToArray()
extern void PayloadData_ToArray_mBD195843E959B2CF4B689E0CFAA1F06EEC2D5FF9 (void);
// 0x000000AA System.String WebSocketSharp.PayloadData::ToString()
extern void PayloadData_ToString_mE0593CD7665F8F1138BB9F36F139AA80BD8EA061 (void);
// 0x000000AB System.Collections.IEnumerator WebSocketSharp.PayloadData::System.Collections.IEnumerable.GetEnumerator()
extern void PayloadData_System_Collections_IEnumerable_GetEnumerator_mB2C65ADD4D1D4D0B68581B9D247A1B7B068AF115 (void);
// 0x000000AC System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__25__ctor_mEC4773BDB32A7F860EE711AB860A92C95C8FF3E5 (void);
// 0x000000AD System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m6C047B2406BDB116F7659A1BED31A613A3FAC62B (void);
// 0x000000AE System.Boolean WebSocketSharp.PayloadData/<GetEnumerator>d__25::MoveNext()
extern void U3CGetEnumeratorU3Ed__25_MoveNext_m545483F181D8B19384B6C50A2228E19CCD48E005 (void);
// 0x000000AF System.Byte WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m4EF48828AC26EED610858272E954323C3E3D7D96 (void);
// 0x000000B0 System.Void WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m7D396DDF6B2E7DB0C795304A47FC34C7B60A7CFC (void);
// 0x000000B1 System.Object WebSocketSharp.PayloadData/<GetEnumerator>d__25::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m07E3D9A4D39D4ECD072716868D1753CB9C9F9421 (void);
// 0x000000B2 System.Void WebSocketSharp.WebSocketException::.ctor(System.String)
extern void WebSocketException__ctor_mE03D1C75C25F011141F070E56F2A68E36EB42E33 (void);
// 0x000000B3 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode)
extern void WebSocketException__ctor_mB1BD02FE74FA2ECFE16C5FDFB45A2772DA8BD53B (void);
// 0x000000B4 System.Void WebSocketSharp.WebSocketException::.ctor(System.String,System.Exception)
extern void WebSocketException__ctor_mC922F25C693428D877B64DE1B9DEF2F37404E356 (void);
// 0x000000B5 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.Exception)
extern void WebSocketException__ctor_m0CE78BB4EEF20CC235C7AA8CCC7D15E9068D1663 (void);
// 0x000000B6 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String)
extern void WebSocketException__ctor_m6F1C0FCFC10B22398810AD9877D47396DB9ADACB (void);
// 0x000000B7 System.Void WebSocketSharp.WebSocketException::.ctor(WebSocketSharp.CloseStatusCode,System.String,System.Exception)
extern void WebSocketException__ctor_m1B6AC7EE1F96C0CF54B1E706B604AD84C52F5739 (void);
// 0x000000B8 WebSocketSharp.CloseStatusCode WebSocketSharp.WebSocketException::get_Code()
extern void WebSocketException_get_Code_m5DD6F0F32504A49E59BD8C4D7DCA4EC8D596089D (void);
// 0x000000B9 System.Void WebSocketSharp.LogData::.ctor(WebSocketSharp.LogLevel,System.Diagnostics.StackFrame,System.String)
extern void LogData__ctor_m0FB260BE27F33BFBCB6B89AE2AEEC8534ECB69CD (void);
// 0x000000BA System.String WebSocketSharp.LogData::ToString()
extern void LogData_ToString_m1786313AF452EEADD58C95A7DB2C1B7B31DD992C (void);
// 0x000000BB System.Void WebSocketSharp.Logger::.ctor()
extern void Logger__ctor_m188F37801EB484DA8FB0C470D674156EE80FEE47 (void);
// 0x000000BC System.Void WebSocketSharp.Logger::.ctor(WebSocketSharp.LogLevel,System.String,System.Action`2<WebSocketSharp.LogData,System.String>)
extern void Logger__ctor_mBB45C64E3EC8C7A330B86A0D43BAB679D07F0C7F (void);
// 0x000000BD System.Void WebSocketSharp.Logger::defaultOutput(WebSocketSharp.LogData,System.String)
extern void Logger_defaultOutput_m86A2F5C370D7AE1DCBF742C7E6E25B4EB3B5DE2E (void);
// 0x000000BE System.Void WebSocketSharp.Logger::output(System.String,WebSocketSharp.LogLevel)
extern void Logger_output_m8219D15E1D9A9959B9753B572F34671051A62FF3 (void);
// 0x000000BF System.Void WebSocketSharp.Logger::writeToFile(System.String,System.String)
extern void Logger_writeToFile_mE6BC853E148075107F323E5AF097B4C4160C8E6D (void);
// 0x000000C0 System.Void WebSocketSharp.Logger::Debug(System.String)
extern void Logger_Debug_m3C57355209E31A0EA2DDE10CC117F44C5AC0EA6E (void);
// 0x000000C1 System.Void WebSocketSharp.Logger::Error(System.String)
extern void Logger_Error_mD95F27AD968016872B1B16CEDA3111ED91F7711A (void);
// 0x000000C2 System.Void WebSocketSharp.Logger::Fatal(System.String)
extern void Logger_Fatal_mF470B1B726FCD446A2AF77A4C26135C39ECB9324 (void);
// 0x000000C3 System.Void WebSocketSharp.Logger::Info(System.String)
extern void Logger_Info_m95673A43768120650EF063537EEB89BA9741CDE2 (void);
// 0x000000C4 System.Void WebSocketSharp.Logger::Trace(System.String)
extern void Logger_Trace_m4E727C4328507A6C26007532BAF2850908A1DC9B (void);
// 0x000000C5 System.Void WebSocketSharp.Logger::Warn(System.String)
extern void Logger_Warn_m6DB65389722F56CA2E7CC8D9646F00B109753DC5 (void);
// 0x000000C6 System.Void WebSocketSharp.WebSocketFrame::.cctor()
extern void WebSocketFrame__cctor_m91615407503A0D088B9504E38ED5BA7174789D06 (void);
// 0x000000C7 System.Void WebSocketSharp.WebSocketFrame::.ctor()
extern void WebSocketFrame__ctor_mE99CD753932524E5F8851D80950C151D46D6F280 (void);
// 0x000000C8 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,System.Byte[],System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_m9AB8F64F675C8F46504A4378107972B2886931FE (void);
// 0x000000C9 System.Void WebSocketSharp.WebSocketFrame::.ctor(WebSocketSharp.Fin,WebSocketSharp.Opcode,WebSocketSharp.PayloadData,System.Boolean,System.Boolean)
extern void WebSocketFrame__ctor_m14DD6951BEE0243E9E22C540F6788AAC722F90FD (void);
// 0x000000CA System.UInt64 WebSocketSharp.WebSocketFrame::get_ExactPayloadLength()
extern void WebSocketFrame_get_ExactPayloadLength_m605F587F3F54965D633FF133D1BD434F13E4A4A2 (void);
// 0x000000CB System.Int32 WebSocketSharp.WebSocketFrame::get_ExtendedPayloadLengthWidth()
extern void WebSocketFrame_get_ExtendedPayloadLengthWidth_m0D415F7771AAEF7A232B73615C78A2299DCD0E09 (void);
// 0x000000CC System.Boolean WebSocketSharp.WebSocketFrame::get_IsClose()
extern void WebSocketFrame_get_IsClose_mD8FBFC6D324E9346E0151EC3BB8B02807566F872 (void);
// 0x000000CD System.Boolean WebSocketSharp.WebSocketFrame::get_IsCompressed()
extern void WebSocketFrame_get_IsCompressed_m3DEA00AB4A0779963FEFA6A1AE43B8E5E04ED349 (void);
// 0x000000CE System.Boolean WebSocketSharp.WebSocketFrame::get_IsContinuation()
extern void WebSocketFrame_get_IsContinuation_m63A8419363A23A9C1C366D4A2CD4134530756F89 (void);
// 0x000000CF System.Boolean WebSocketSharp.WebSocketFrame::get_IsData()
extern void WebSocketFrame_get_IsData_m10663EEBF3BBE9C6BCA77008C08C3870F93FEE53 (void);
// 0x000000D0 System.Boolean WebSocketSharp.WebSocketFrame::get_IsFinal()
extern void WebSocketFrame_get_IsFinal_mB4CF208B4204EC5FF33B71D3EC4CFF48D339ECB1 (void);
// 0x000000D1 System.Boolean WebSocketSharp.WebSocketFrame::get_IsFragment()
extern void WebSocketFrame_get_IsFragment_m44E622C54F3A344561EE7A84FC91AE5BE2DAD95E (void);
// 0x000000D2 System.Boolean WebSocketSharp.WebSocketFrame::get_IsMasked()
extern void WebSocketFrame_get_IsMasked_m49F042A1576CB2128FA718566665250BABF25BEC (void);
// 0x000000D3 System.Boolean WebSocketSharp.WebSocketFrame::get_IsPing()
extern void WebSocketFrame_get_IsPing_m3FEC05F0737AFA86DF7768C6DACE8440B6FE76A1 (void);
// 0x000000D4 System.Boolean WebSocketSharp.WebSocketFrame::get_IsPong()
extern void WebSocketFrame_get_IsPong_mA4B8FD203DB5A8BC42BD47A6F6395E3850D44C04 (void);
// 0x000000D5 System.Boolean WebSocketSharp.WebSocketFrame::get_IsText()
extern void WebSocketFrame_get_IsText_m101237563F9DE72B5EA70A95F79921CF2CF245C0 (void);
// 0x000000D6 System.UInt64 WebSocketSharp.WebSocketFrame::get_Length()
extern void WebSocketFrame_get_Length_m770761839F3A8166C2ACF5105558DABE1F67883B (void);
// 0x000000D7 WebSocketSharp.Opcode WebSocketSharp.WebSocketFrame::get_Opcode()
extern void WebSocketFrame_get_Opcode_m1FCDC8C889A75E128758775AF83BCEFBBA0AA74C (void);
// 0x000000D8 WebSocketSharp.PayloadData WebSocketSharp.WebSocketFrame::get_PayloadData()
extern void WebSocketFrame_get_PayloadData_mA55D15155851FBB72EC8CAF272E37CD8E4BDD9CD (void);
// 0x000000D9 WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv2()
extern void WebSocketFrame_get_Rsv2_m0A02760B830871708FEEC6A9DD4E61D580820CA7 (void);
// 0x000000DA WebSocketSharp.Rsv WebSocketSharp.WebSocketFrame::get_Rsv3()
extern void WebSocketFrame_get_Rsv3_m25DDD5B4D361551F85803B4DFD9EA8EA3FAD2E2E (void);
// 0x000000DB System.Byte[] WebSocketSharp.WebSocketFrame::createMaskingKey()
extern void WebSocketFrame_createMaskingKey_m293E3CFB87AD37B21F073510C0A4446CE86A7409 (void);
// 0x000000DC System.String WebSocketSharp.WebSocketFrame::dump(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_dump_m9FD919B931DDE6E5A5B09FB7ABA543EE4FDF67E0 (void);
// 0x000000DD System.String WebSocketSharp.WebSocketFrame::print(WebSocketSharp.WebSocketFrame)
extern void WebSocketFrame_print_m5E0101FB29EF074F79E310511FEEDCB946213259 (void);
// 0x000000DE WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::processHeader(System.Byte[])
extern void WebSocketFrame_processHeader_m5CF3AF4145C8D118A56498F0FA3A80ADC7AC2924 (void);
// 0x000000DF System.Void WebSocketSharp.WebSocketFrame::readExtendedPayloadLengthAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readExtendedPayloadLengthAsync_m0FFD3BA5AD62D60E62DA087B07A013C5C43E3521 (void);
// 0x000000E0 System.Void WebSocketSharp.WebSocketFrame::readHeaderAsync(System.IO.Stream,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readHeaderAsync_mD4F2B5FBD6A279ACAE7D5678DA1E56D580247227 (void);
// 0x000000E1 System.Void WebSocketSharp.WebSocketFrame::readMaskingKeyAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readMaskingKeyAsync_m1CF02B707402B69F8BB4F764D8402B4539A20F16 (void);
// 0x000000E2 System.Void WebSocketSharp.WebSocketFrame::readPayloadDataAsync(System.IO.Stream,WebSocketSharp.WebSocketFrame,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_readPayloadDataAsync_m51C17AB8DE4F0652A672122D911F18F5450DD8F9 (void);
// 0x000000E3 System.String WebSocketSharp.WebSocketFrame::utf8Decode(System.Byte[])
extern void WebSocketFrame_utf8Decode_m64622C695D35BDCEA02C7CD4AFFDEB304FC29CC0 (void);
// 0x000000E4 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreateCloseFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreateCloseFrame_mA229A746CC15F41B0C80BE4EF78960727CA68890 (void);
// 0x000000E5 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePingFrame(System.Boolean)
extern void WebSocketFrame_CreatePingFrame_m92B7DA5E6B62621A64E6130E54D11DC9C8826EF1 (void);
// 0x000000E6 WebSocketSharp.WebSocketFrame WebSocketSharp.WebSocketFrame::CreatePongFrame(WebSocketSharp.PayloadData,System.Boolean)
extern void WebSocketFrame_CreatePongFrame_m51B76225D048BF803152D886343F4CF0033EECBC (void);
// 0x000000E7 System.Void WebSocketSharp.WebSocketFrame::ReadFrameAsync(System.IO.Stream,System.Boolean,System.Action`1<WebSocketSharp.WebSocketFrame>,System.Action`1<System.Exception>)
extern void WebSocketFrame_ReadFrameAsync_m8D58108EA4B5E245BE55A90A8B34D00FDA79239C (void);
// 0x000000E8 System.Void WebSocketSharp.WebSocketFrame::Unmask()
extern void WebSocketFrame_Unmask_m2EC9DB25D033D729F917E1671C01E1FDFFB826A9 (void);
// 0x000000E9 System.Collections.Generic.IEnumerator`1<System.Byte> WebSocketSharp.WebSocketFrame::GetEnumerator()
extern void WebSocketFrame_GetEnumerator_mA04DC82E8CC70687C4B0343D394E08B1C1A1405E (void);
// 0x000000EA System.String WebSocketSharp.WebSocketFrame::PrintToString(System.Boolean)
extern void WebSocketFrame_PrintToString_m2A9BE958EABE1D835ABC247C2F02DCA2BC9ED280 (void);
// 0x000000EB System.Byte[] WebSocketSharp.WebSocketFrame::ToArray()
extern void WebSocketFrame_ToArray_m929D6B5B1D8D7FA6373A422DDA71FF7EE571EBD2 (void);
// 0x000000EC System.String WebSocketSharp.WebSocketFrame::ToString()
extern void WebSocketFrame_ToString_m6FE7EC48017642D3230980BD2F300936B858C5DC (void);
// 0x000000ED System.Collections.IEnumerator WebSocketSharp.WebSocketFrame::System.Collections.IEnumerable.GetEnumerator()
extern void WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m5DFC9900829CB188E15BD971487116512EC5A487 (void);
// 0x000000EE System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::.ctor()
extern void U3CU3Ec__DisplayClass67_0__ctor_m978C7BA4386A44DD8E6760E2F373E2BDAAFDD9DD (void);
// 0x000000EF System.Action`4<System.String,System.String,System.String,System.String> WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_0::<dump>b__0()
extern void U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_mFE1EBF1D8036E5294CB9B6EDAC0A794C1AC9AF13 (void);
// 0x000000F0 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::.ctor()
extern void U3CU3Ec__DisplayClass67_1__ctor_m30785213BA90551F86B24FA5088978DD52FAF9A0 (void);
// 0x000000F1 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass67_1::<dump>b__1(System.String,System.String,System.String,System.String)
extern void U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m70418A2B861DED1452DFEA96014A46B5377F32F3 (void);
// 0x000000F2 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::.ctor()
extern void U3CU3Ec__DisplayClass71_0__ctor_m3DEC4374692DCD7D5082AC31829694D45F1A14D7 (void);
// 0x000000F3 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass71_0::<readExtendedPayloadLengthAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m19AF767DDCADA3B9DEAC6CE8499818E8D5DCE2FA (void);
// 0x000000F4 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_m96F1E0FA4ACE30782081D890B2A05A0A4F87E458 (void);
// 0x000000F5 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass73_0::<readHeaderAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m0FA594A94DD8A7E82AB941C67B5F31AC36557DE7 (void);
// 0x000000F6 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_m9D8EFE8C1FD803CC76471F1B738B4D8912E91FA1 (void);
// 0x000000F7 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass75_0::<readMaskingKeyAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_m82927E0D099EAD926A3AB402EA2B331106B15D50 (void);
// 0x000000F8 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::.ctor()
extern void U3CU3Ec__DisplayClass77_0__ctor_mE1B6304D6E0B374CF457F8BF710E4ADB191C43AD (void);
// 0x000000F9 System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass77_0::<readPayloadDataAsync>b__0(System.Byte[])
extern void U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_m23195F5DE3434C2A0047C313CCC8629F68A8EDDD (void);
// 0x000000FA System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::.ctor()
extern void U3CU3Ec__DisplayClass84_0__ctor_m8CD6DF83D40066E2E4F3F09EAB26B05A2FD1CD7D (void);
// 0x000000FB System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__0(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__0_m8AD35ABF3DB66FB0F12D6F43630242969E35BF0F (void);
// 0x000000FC System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__1(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__1_m1FEA1B1FFB470548B342816B7FCB9B5566E04FF7 (void);
// 0x000000FD System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__2(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__2_m1323A87FF1330665554C62C2BDF2A81D707B1746 (void);
// 0x000000FE System.Void WebSocketSharp.WebSocketFrame/<>c__DisplayClass84_0::<ReadFrameAsync>b__3(WebSocketSharp.WebSocketFrame)
extern void U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__3_m0007EF78D8E6B2F7E2E3898EA3F5A1427E4C64E6 (void);
// 0x000000FF System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::.ctor(System.Int32)
extern void U3CGetEnumeratorU3Ed__86__ctor_m3534EFFACACFDE35A5B01293B1FC61312F996C63 (void);
// 0x00000100 System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.IDisposable.Dispose()
extern void U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_m85D50301C9284D31439B4B9096B93B41BA0EF18B (void);
// 0x00000101 System.Boolean WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::MoveNext()
extern void U3CGetEnumeratorU3Ed__86_MoveNext_m8BE980A0427DDC379068FF91220FD2944F66E90B (void);
// 0x00000102 System.Byte WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.Generic.IEnumerator<System.Byte>.get_Current()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mF93BF87617181C1665503998BC1855E534D52437 (void);
// 0x00000103 System.Void WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.IEnumerator.Reset()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m190045B074085CC054A2E63F9C4520ADC553A2C5 (void);
// 0x00000104 System.Object WebSocketSharp.WebSocketFrame/<GetEnumerator>d__86::System.Collections.IEnumerator.get_Current()
extern void U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_m10B4AACDE813921FBBBA83A4E9FAC4E4A1A1F970 (void);
// 0x00000105 System.Void WebSocketSharp.HttpBase::.ctor(System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpBase__ctor_m6410A7FC51E49A3A7F55CE5FF49B4EE1571455F7 (void);
// 0x00000106 System.String WebSocketSharp.HttpBase::get_EntityBody()
extern void HttpBase_get_EntityBody_mE08132C946C8F47778E559F36BD32D49B5FD059C (void);
// 0x00000107 System.Collections.Specialized.NameValueCollection WebSocketSharp.HttpBase::get_Headers()
extern void HttpBase_get_Headers_m25BE165BFF37F78E1BAEEBFFCC30A2D853507A81 (void);
// 0x00000108 System.Version WebSocketSharp.HttpBase::get_ProtocolVersion()
extern void HttpBase_get_ProtocolVersion_mA5593022C745F3C9E31C857C1F2E0B81B7B342CA (void);
// 0x00000109 System.Byte[] WebSocketSharp.HttpBase::readEntityBody(System.IO.Stream,System.String)
extern void HttpBase_readEntityBody_mC45967336F8B3EDE429BB3023749D17BD4C29D32 (void);
// 0x0000010A System.String[] WebSocketSharp.HttpBase::readHeaders(System.IO.Stream,System.Int32)
extern void HttpBase_readHeaders_m40ADE558276E6F64AB3C77A61FBA957B97C9CE36 (void);
// 0x0000010B T WebSocketSharp.HttpBase::Read(System.IO.Stream,System.Func`2<System.String[],T>,System.Int32)
// 0x0000010C System.Byte[] WebSocketSharp.HttpBase::ToByteArray()
extern void HttpBase_ToByteArray_m4B9B4F475614BF1F54F3ACC3CB2060D460A64E8C (void);
// 0x0000010D System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_m51C643CEFE62CDD10248D47C3402C56288B0C040 (void);
// 0x0000010E System.Void WebSocketSharp.HttpBase/<>c__DisplayClass13_0::<readHeaders>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m7966C6C1502F6DF0EFB91EF521F476D68A7B4797 (void);
// 0x0000010F System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::.ctor()
// 0x00000110 System.Void WebSocketSharp.HttpBase/<>c__DisplayClass14_0`1::<Read>b__0(System.Object)
// 0x00000111 System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpRequest__ctor_m7FADDCF5DC773405AB6A036319CA0780F5B15917 (void);
// 0x00000112 System.Void WebSocketSharp.HttpRequest::.ctor(System.String,System.String)
extern void HttpRequest__ctor_m285303CEE5EE9C3F44EBC174A0CC4E00AC7BA207 (void);
// 0x00000113 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateConnectRequest(System.Uri)
extern void HttpRequest_CreateConnectRequest_m08DD15312D4FD78D58493B70DCCAC64692431160 (void);
// 0x00000114 WebSocketSharp.HttpRequest WebSocketSharp.HttpRequest::CreateWebSocketRequest(System.Uri)
extern void HttpRequest_CreateWebSocketRequest_m33086CF1B925A33079CF4E637B7560DC114B17D4 (void);
// 0x00000115 WebSocketSharp.HttpResponse WebSocketSharp.HttpRequest::GetResponse(System.IO.Stream,System.Int32)
extern void HttpRequest_GetResponse_m9021A6B2CA836B094FD48CF87E4E99C3AEAF33CD (void);
// 0x00000116 System.Void WebSocketSharp.HttpRequest::SetCookies(WebSocketSharp.Net.CookieCollection)
extern void HttpRequest_SetCookies_mC533EE1997B4C590AD0368386CE9D0785D480CC6 (void);
// 0x00000117 System.String WebSocketSharp.HttpRequest::ToString()
extern void HttpRequest_ToString_m923F26F03BF4F6990FE74C265C1E2ED82895D828 (void);
// 0x00000118 System.Void WebSocketSharp.HttpResponse::.ctor(System.String,System.String,System.Version,System.Collections.Specialized.NameValueCollection)
extern void HttpResponse__ctor_m76F880A7B55BBFEA6DF52354CC177E213B13728B (void);
// 0x00000119 WebSocketSharp.Net.CookieCollection WebSocketSharp.HttpResponse::get_Cookies()
extern void HttpResponse_get_Cookies_mA00E684F6D50629DE16A2451EA0648E0840527EC (void);
// 0x0000011A System.Boolean WebSocketSharp.HttpResponse::get_HasConnectionClose()
extern void HttpResponse_get_HasConnectionClose_mA94A71096F02608F465BEC8306DD7F2EF1FC284F (void);
// 0x0000011B System.Boolean WebSocketSharp.HttpResponse::get_IsProxyAuthenticationRequired()
extern void HttpResponse_get_IsProxyAuthenticationRequired_m0FF90ABD0D5A97FD5B98FB52A90DFC947F4B3DA7 (void);
// 0x0000011C System.Boolean WebSocketSharp.HttpResponse::get_IsRedirect()
extern void HttpResponse_get_IsRedirect_mB5964EB771B6D05E52E9381E7459363A6CF57687 (void);
// 0x0000011D System.Boolean WebSocketSharp.HttpResponse::get_IsUnauthorized()
extern void HttpResponse_get_IsUnauthorized_mBB9B176B318F53162B226AC9BFD2E00D5B5FAC68 (void);
// 0x0000011E System.Boolean WebSocketSharp.HttpResponse::get_IsWebSocketResponse()
extern void HttpResponse_get_IsWebSocketResponse_mE4ED10EA6F9796994480FA06DD15AC5955538B66 (void);
// 0x0000011F System.String WebSocketSharp.HttpResponse::get_StatusCode()
extern void HttpResponse_get_StatusCode_mC8D3E715DA17FA6C3DD4CDA3B1ACAB407F6638D3 (void);
// 0x00000120 WebSocketSharp.HttpResponse WebSocketSharp.HttpResponse::Parse(System.String[])
extern void HttpResponse_Parse_mD5DD2BDB86688A41EC2579C07AA56069302AD554 (void);
// 0x00000121 System.String WebSocketSharp.HttpResponse::ToString()
extern void HttpResponse_ToString_m7A260B2DA36C3D2A52A13D70B9A5F5700CA74C52 (void);
// 0x00000122 System.Void WebSocketSharp.Net.Cookie::.cctor()
extern void Cookie__cctor_m07BACF458BB412F7D0A70907EDF41B4DB79EF946 (void);
// 0x00000123 System.Void WebSocketSharp.Net.Cookie::.ctor()
extern void Cookie__ctor_mF6E35CA69CCCE2F3C3BAE6B14DADD82C82E80AF1 (void);
// 0x00000124 System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String)
extern void Cookie__ctor_mF5D2A664D8EC2B231F415E88EC6E1F324381C397 (void);
// 0x00000125 System.Void WebSocketSharp.Net.Cookie::.ctor(System.String,System.String,System.String,System.String)
extern void Cookie__ctor_m174AFEC71A470E616B6425C170DA3ECB260E4F50 (void);
// 0x00000126 System.Void WebSocketSharp.Net.Cookie::set_MaxAge(System.Int32)
extern void Cookie_set_MaxAge_m86545D003C2E0C08EEECE50BFBBDCE52313459DA (void);
// 0x00000127 System.Void WebSocketSharp.Net.Cookie::set_SameSite(System.String)
extern void Cookie_set_SameSite_m816D245EC12E7DD8EC6D1E2B63AE1E999C60DAF9 (void);
// 0x00000128 System.Void WebSocketSharp.Net.Cookie::set_Comment(System.String)
extern void Cookie_set_Comment_m26AFC2233BB294325E866D033CF84DE5F7341249 (void);
// 0x00000129 System.Void WebSocketSharp.Net.Cookie::set_CommentUri(System.Uri)
extern void Cookie_set_CommentUri_m228C7E1B8B6D31C6B655663B77143F2BD46D49E0 (void);
// 0x0000012A System.Void WebSocketSharp.Net.Cookie::set_Discard(System.Boolean)
extern void Cookie_set_Discard_mE57DCF3EE566B46272DDF54EEFF1FBC4A97E0E3A (void);
// 0x0000012B System.Void WebSocketSharp.Net.Cookie::set_Domain(System.String)
extern void Cookie_set_Domain_mF3A14D39CD41A65F0C17B0B53FC60E8C551BCEF8 (void);
// 0x0000012C System.Boolean WebSocketSharp.Net.Cookie::get_Expired()
extern void Cookie_get_Expired_m8EC7F7EFBBED40F72BC5B96C4EE81295062D643C (void);
// 0x0000012D System.DateTime WebSocketSharp.Net.Cookie::get_Expires()
extern void Cookie_get_Expires_m3AE44AFB79CE495922E2FDE6B9870036A8741F32 (void);
// 0x0000012E System.Void WebSocketSharp.Net.Cookie::set_Expires(System.DateTime)
extern void Cookie_set_Expires_m340A433911EC90D5707185619201EBBC06E1C2F9 (void);
// 0x0000012F System.Void WebSocketSharp.Net.Cookie::set_HttpOnly(System.Boolean)
extern void Cookie_set_HttpOnly_m5A3A15346F84567C1E8E309A82DE0EBA780E13BE (void);
// 0x00000130 System.String WebSocketSharp.Net.Cookie::get_Name()
extern void Cookie_get_Name_m395E432C349675E3FBC78FD697D89E2C7F373221 (void);
// 0x00000131 System.String WebSocketSharp.Net.Cookie::get_Path()
extern void Cookie_get_Path_m19F17342CF37FE9A602398E3C88EBB9E446F2CBE (void);
// 0x00000132 System.Void WebSocketSharp.Net.Cookie::set_Path(System.String)
extern void Cookie_set_Path_m7CCAD956F9CFD54E2F9F5BE36C40E8C1EA074179 (void);
// 0x00000133 System.Void WebSocketSharp.Net.Cookie::set_Port(System.String)
extern void Cookie_set_Port_mD49691D11FB3FE738E51553B1332E6E16D9869B9 (void);
// 0x00000134 System.Void WebSocketSharp.Net.Cookie::set_Secure(System.Boolean)
extern void Cookie_set_Secure_mD569D72B70205D3156A0C426E05FD231B79961FA (void);
// 0x00000135 System.Int32 WebSocketSharp.Net.Cookie::get_Version()
extern void Cookie_get_Version_mF59096D7225D85322328397F3D4922CD2E1743ED (void);
// 0x00000136 System.Void WebSocketSharp.Net.Cookie::set_Version(System.Int32)
extern void Cookie_set_Version_m77A60028CD71562419DB6479EB2C093F93FE2C97 (void);
// 0x00000137 System.Int32 WebSocketSharp.Net.Cookie::hash(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void Cookie_hash_mB953932A1AFBB34D13C6FD478D4C73B49CAB7F09 (void);
// 0x00000138 System.Void WebSocketSharp.Net.Cookie::init(System.String,System.String,System.String,System.String)
extern void Cookie_init_mD6774A6A2106E8BFEEAF95BA3073A5A3DCFAD95E (void);
// 0x00000139 System.Boolean WebSocketSharp.Net.Cookie::tryCreatePorts(System.String,System.Int32[]&)
extern void Cookie_tryCreatePorts_mF11D29D69E4EBAE3B5CE935C711EDA35538C0649 (void);
// 0x0000013A System.Boolean WebSocketSharp.Net.Cookie::EqualsWithoutValue(WebSocketSharp.Net.Cookie)
extern void Cookie_EqualsWithoutValue_mE199341AF932B39EB9FD1E20132A718F89ACF220 (void);
// 0x0000013B System.String WebSocketSharp.Net.Cookie::ToRequestString(System.Uri)
extern void Cookie_ToRequestString_m9FE9617FDC4CCBFABCFF8586260C6ED59E57204F (void);
// 0x0000013C System.Boolean WebSocketSharp.Net.Cookie::TryCreate(System.String,System.String,WebSocketSharp.Net.Cookie&)
extern void Cookie_TryCreate_m8453EB906605500C578BEBC9F2E0563AB1487B7D (void);
// 0x0000013D System.Boolean WebSocketSharp.Net.Cookie::Equals(System.Object)
extern void Cookie_Equals_mEADC36AAC11684B2B5F6E46A0E1C2ECCA7A723EE (void);
// 0x0000013E System.Int32 WebSocketSharp.Net.Cookie::GetHashCode()
extern void Cookie_GetHashCode_m878E2266AAF7598E34199F60AD848D0548A05EB7 (void);
// 0x0000013F System.String WebSocketSharp.Net.Cookie::ToString()
extern void Cookie_ToString_mC9E15F34A8487C582DB87C22B59C50F78B48DB9A (void);
// 0x00000140 System.Void WebSocketSharp.Net.CookieCollection::.ctor()
extern void CookieCollection__ctor_mD6A730BFD4A8682CEF34F578CEC489D723FAC7C4 (void);
// 0x00000141 System.Collections.Generic.IEnumerable`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::get_Sorted()
extern void CookieCollection_get_Sorted_m9D884937831EC235D75B4F4C8E15BD2B1581C9BB (void);
// 0x00000142 System.Int32 WebSocketSharp.Net.CookieCollection::get_Count()
extern void CookieCollection_get_Count_mE025F97CCB76F1D2DC31E18319C0D31CFE093896 (void);
// 0x00000143 System.Boolean WebSocketSharp.Net.CookieCollection::get_IsReadOnly()
extern void CookieCollection_get_IsReadOnly_m7857A4F54D4F2D01558481957F5AEB59BB7A065C (void);
// 0x00000144 System.Void WebSocketSharp.Net.CookieCollection::add(WebSocketSharp.Net.Cookie)
extern void CookieCollection_add_m8014254B58F787E6F7CC6605136FA86F901AC354 (void);
// 0x00000145 System.Int32 WebSocketSharp.Net.CookieCollection::compareForSorted(WebSocketSharp.Net.Cookie,WebSocketSharp.Net.Cookie)
extern void CookieCollection_compareForSorted_mE7A71F15F2AB5BE16C8E4F99753322BE800337BD (void);
// 0x00000146 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseRequest(System.String)
extern void CookieCollection_parseRequest_mAB79611FA5D5D0AD862680C5D553C9041ADF5E30 (void);
// 0x00000147 WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::parseResponse(System.String)
extern void CookieCollection_parseResponse_m78E4FA322AEA68E3DF216A102D9ACE8385B9A1B2 (void);
// 0x00000148 System.Int32 WebSocketSharp.Net.CookieCollection::search(WebSocketSharp.Net.Cookie)
extern void CookieCollection_search_mDC2EF547B9F49E08A2E8AD6BF57C990001D0EA58 (void);
// 0x00000149 System.String WebSocketSharp.Net.CookieCollection::urlDecode(System.String,System.Text.Encoding)
extern void CookieCollection_urlDecode_mA72EDAADCFE3504F812DACF06D41D30A8AE5A514 (void);
// 0x0000014A WebSocketSharp.Net.CookieCollection WebSocketSharp.Net.CookieCollection::Parse(System.String,System.Boolean)
extern void CookieCollection_Parse_m55CCAEED8509FC5E746EA090B5712173D2E95284 (void);
// 0x0000014B System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.Cookie)
extern void CookieCollection_SetOrRemove_m072D1649CFE56117B927AC33D3B91BD95C4B278E (void);
// 0x0000014C System.Void WebSocketSharp.Net.CookieCollection::SetOrRemove(WebSocketSharp.Net.CookieCollection)
extern void CookieCollection_SetOrRemove_m644171D09DD61F11190B08F825FAFB75C534F5EE (void);
// 0x0000014D System.Void WebSocketSharp.Net.CookieCollection::Add(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Add_m334EB67DCC37317C004EED5BF1A3A9579975BCCF (void);
// 0x0000014E System.Void WebSocketSharp.Net.CookieCollection::Clear()
extern void CookieCollection_Clear_m79BA8B4F40B8B3930342D8BA03461B05680AE94C (void);
// 0x0000014F System.Boolean WebSocketSharp.Net.CookieCollection::Contains(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Contains_m04C9C46A3203F1B5D62332CBA64BB6DEBFD9254E (void);
// 0x00000150 System.Void WebSocketSharp.Net.CookieCollection::CopyTo(WebSocketSharp.Net.Cookie[],System.Int32)
extern void CookieCollection_CopyTo_m57CA7A5CBDE6A06947DE530CBB424624FE3403D2 (void);
// 0x00000151 System.Collections.Generic.IEnumerator`1<WebSocketSharp.Net.Cookie> WebSocketSharp.Net.CookieCollection::GetEnumerator()
extern void CookieCollection_GetEnumerator_m96C386CCA13979305BEDF3C119A6C659F40C1D76 (void);
// 0x00000152 System.Boolean WebSocketSharp.Net.CookieCollection::Remove(WebSocketSharp.Net.Cookie)
extern void CookieCollection_Remove_mBEFBE9C876C3FE47A23140835BC4268B2CD85929 (void);
// 0x00000153 System.Collections.IEnumerator WebSocketSharp.Net.CookieCollection::System.Collections.IEnumerable.GetEnumerator()
extern void CookieCollection_System_Collections_IEnumerable_GetEnumerator_m629141A43B9B7FA068E6AEA485BAC29971624805 (void);
// 0x00000154 System.Void WebSocketSharp.Net.CookieException::.ctor(System.String,System.Exception)
extern void CookieException__ctor_mB0CFD42EC8F601C6319264D18383081F63A94438 (void);
// 0x00000155 System.Void WebSocketSharp.Net.CookieException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException__ctor_m9E78B0D68512826A19CDECFB01512C0C55476167 (void);
// 0x00000156 System.Void WebSocketSharp.Net.CookieException::.ctor()
extern void CookieException__ctor_m11AFD4DD0630C66345C989DF6E0199F065C4D257 (void);
// 0x00000157 System.Void WebSocketSharp.Net.CookieException::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_GetObjectData_m4A68E43EDD01A85DA3958BFB67A4720DEC74807B (void);
// 0x00000158 System.Void WebSocketSharp.Net.CookieException::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m54C79DDA7AEE083DF337665649088A5533927F92 (void);
// 0x00000159 System.Void WebSocketSharp.Net.HttpUtility::.cctor()
extern void HttpUtility__cctor_m8F8E3B2E694350C431B2E2801C71E3F33F12BB09 (void);
// 0x0000015A System.Int32 WebSocketSharp.Net.HttpUtility::getNumber(System.Char)
extern void HttpUtility_getNumber_mC477ECA96D529EE51964FC0FC6BB7E70141148DB (void);
// 0x0000015B System.Int32 WebSocketSharp.Net.HttpUtility::getNumber(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_getNumber_m2A5E60E84032C21CE58894CA6F97A8B5807F258D (void);
// 0x0000015C System.Byte[] WebSocketSharp.Net.HttpUtility::urlDecodeToBytes(System.Byte[],System.Int32,System.Int32)
extern void HttpUtility_urlDecodeToBytes_m0531F0EF78A40882F97D36F401EB662F1B4700C4 (void);
// 0x0000015D System.Text.Encoding WebSocketSharp.Net.HttpUtility::GetEncoding(System.String)
extern void HttpUtility_GetEncoding_m797DA56C36D3E3167F6E61397E5486A9E480693F (void);
// 0x0000015E System.String WebSocketSharp.Net.HttpUtility::UrlDecode(System.String,System.Text.Encoding)
extern void HttpUtility_UrlDecode_mB9E42353EBEBBB13F1784C7F00A718CA43AF72CB (void);
// 0x0000015F System.Void WebSocketSharp.Net.WebHeaderCollection::.cctor()
extern void WebHeaderCollection__cctor_m267B6CCFC243CA9913C1EA8046A7D173A71C6A33 (void);
// 0x00000160 System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection__ctor_mC9592A7BAD3DAEC954D1C68778F564B9802110DF (void);
// 0x00000161 System.Void WebSocketSharp.Net.WebHeaderCollection::.ctor()
extern void WebHeaderCollection__ctor_mB7C7027B1B1162AAF4C24451BCDE12452120BA9D (void);
// 0x00000162 System.String[] WebSocketSharp.Net.WebHeaderCollection::get_AllKeys()
extern void WebHeaderCollection_get_AllKeys_mF2E7E003E0A2C429CD0410BB53380B03BB6EF68B (void);
// 0x00000163 System.Int32 WebSocketSharp.Net.WebHeaderCollection::get_Count()
extern void WebHeaderCollection_get_Count_m9FF09EE578FE53D4E39EFC2AAA6A914E8A157811 (void);
// 0x00000164 System.Void WebSocketSharp.Net.WebHeaderCollection::add(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_add_mF05DE9CE4B6783A568CCBD1C3ECA0BB2BFC3F59D (void);
// 0x00000165 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingName_mBE54E66829EE6B3BFC5711EDEF02CB584F2A6354 (void);
// 0x00000166 System.Void WebSocketSharp.Net.WebHeaderCollection::addWithoutCheckingNameAndRestricted(System.String,System.String)
extern void WebHeaderCollection_addWithoutCheckingNameAndRestricted_mBFF5C90396A190F6A27D17C4AB569AD315159F3C (void);
// 0x00000167 WebSocketSharp.Net.HttpHeaderType WebSocketSharp.Net.WebHeaderCollection::checkHeaderType(System.String)
extern void WebHeaderCollection_checkHeaderType_m8157A359CDC9454E6742E26D3D7E87F34F45590F (void);
// 0x00000168 System.String WebSocketSharp.Net.WebHeaderCollection::checkName(System.String)
extern void WebHeaderCollection_checkName_m1D378B16A49730F918654A2F439978D4A6619ADE (void);
// 0x00000169 System.Void WebSocketSharp.Net.WebHeaderCollection::checkRestricted(System.String)
extern void WebHeaderCollection_checkRestricted_mA0E3BA3FE61611BFBD6011D0B74BA54468DD66D2 (void);
// 0x0000016A System.Void WebSocketSharp.Net.WebHeaderCollection::checkState(System.Boolean)
extern void WebHeaderCollection_checkState_mA817555DDFA4B95A117A3849091606C1E3693DEC (void);
// 0x0000016B System.String WebSocketSharp.Net.WebHeaderCollection::checkValue(System.String)
extern void WebHeaderCollection_checkValue_mFE23D810596119F48D584F10F7D9701885628CB1 (void);
// 0x0000016C System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_m12BDD18B6E80BF89056D8BFBD0E62007C0CE4E0F (void);
// 0x0000016D System.Void WebSocketSharp.Net.WebHeaderCollection::doWithCheckingState(System.Action`2<System.String,System.String>,System.String,System.String,System.Boolean,System.Boolean)
extern void WebHeaderCollection_doWithCheckingState_mE5E8CBA7BC66741521C8541A74FC72A35A151410 (void);
// 0x0000016E System.Void WebSocketSharp.Net.WebHeaderCollection::doWithoutCheckingName(System.Action`2<System.String,System.String>,System.String,System.String)
extern void WebHeaderCollection_doWithoutCheckingName_mFB02AA03A2ECE9CD9E8C44AE56032BF217F30109 (void);
// 0x0000016F WebSocketSharp.Net.HttpHeaderInfo WebSocketSharp.Net.WebHeaderCollection::getHeaderInfo(System.String)
extern void WebHeaderCollection_getHeaderInfo_m6F7796834288982DAD38D4C9EE0D815005797A95 (void);
// 0x00000170 System.Boolean WebSocketSharp.Net.WebHeaderCollection::isMultiValue(System.String,System.Boolean)
extern void WebHeaderCollection_isMultiValue_m3DFD8B34EE3FC9B6E342C10E13C6D68C2F40739B (void);
// 0x00000171 System.Boolean WebSocketSharp.Net.WebHeaderCollection::isRestricted(System.String,System.Boolean)
extern void WebHeaderCollection_isRestricted_mCC4F7A9B1609A4BBEA95B2F32335C4005672E5B2 (void);
// 0x00000172 System.Void WebSocketSharp.Net.WebHeaderCollection::setWithoutCheckingName(System.String,System.String)
extern void WebHeaderCollection_setWithoutCheckingName_m4B6C061AC842A940A0FBF41F781265F10B3890FB (void);
// 0x00000173 System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m9661440478A12153990740148323279EEE372FC1 (void);
// 0x00000174 System.Void WebSocketSharp.Net.WebHeaderCollection::InternalSet(System.String,System.String,System.Boolean)
extern void WebHeaderCollection_InternalSet_m4F2E0D7CBDE816E0658277201AE7C14155D9B8BC (void);
// 0x00000175 System.Void WebSocketSharp.Net.WebHeaderCollection::Add(System.String,System.String)
extern void WebHeaderCollection_Add_mC1AD2FAF5A75D9DBB08ED75103C060E50157F15D (void);
// 0x00000176 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.Int32)
extern void WebHeaderCollection_Get_mFBC05DC1D01A5D6F75A2727DE04EFBFB13C98105 (void);
// 0x00000177 System.String WebSocketSharp.Net.WebHeaderCollection::Get(System.String)
extern void WebHeaderCollection_Get_m2ACD6D76927F5B2C385D978603E5B91E9979B399 (void);
// 0x00000178 System.Collections.IEnumerator WebSocketSharp.Net.WebHeaderCollection::GetEnumerator()
extern void WebHeaderCollection_GetEnumerator_m2584519B2CC2EC9C35A9EF8E204958CA2827971A (void);
// 0x00000179 System.String WebSocketSharp.Net.WebHeaderCollection::GetKey(System.Int32)
extern void WebHeaderCollection_GetKey_m9DC3B0918B36F290D6CF5924F144698345B4D4B9 (void);
// 0x0000017A System.Void WebSocketSharp.Net.WebHeaderCollection::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_GetObjectData_m80A17AF53CD06A61E74E95776354E0462361EDF1 (void);
// 0x0000017B System.Void WebSocketSharp.Net.WebHeaderCollection::OnDeserialization(System.Object)
extern void WebHeaderCollection_OnDeserialization_m14CD79B0B96926FB6BE6345B03DB293092AE6865 (void);
// 0x0000017C System.Void WebSocketSharp.Net.WebHeaderCollection::Set(System.String,System.String)
extern void WebHeaderCollection_Set_mE09547AD9C58236C42125AD98789E4CC4483ACD7 (void);
// 0x0000017D System.String WebSocketSharp.Net.WebHeaderCollection::ToString()
extern void WebHeaderCollection_ToString_m71A86D619F341B35E337D02D49AEDC0C6799D5B9 (void);
// 0x0000017E System.Void WebSocketSharp.Net.WebHeaderCollection::System.Runtime.Serialization.ISerializable.GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m1112DB1C93A5D03616CD16AB414F9B842B6B37B1 (void);
// 0x0000017F System.Void WebSocketSharp.Net.HttpVersion::.cctor()
extern void HttpVersion__cctor_m451D97B21FFC971885FDF699D8520BEE3D7B03A7 (void);
// 0x00000180 System.Void WebSocketSharp.Net.HttpHeaderInfo::.ctor(System.String,WebSocketSharp.Net.HttpHeaderType)
extern void HttpHeaderInfo__ctor_m82FC5E0079770F60B09B1D082940E7CB252EDDE0 (void);
// 0x00000181 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInRequest()
extern void HttpHeaderInfo_get_IsMultiValueInRequest_m0C02D84427289DCE654C7FC27C7E45307425E684 (void);
// 0x00000182 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsMultiValueInResponse()
extern void HttpHeaderInfo_get_IsMultiValueInResponse_m5E03A8D2E20FD46319953871856051D50F74AE76 (void);
// 0x00000183 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsRequest()
extern void HttpHeaderInfo_get_IsRequest_mEEF7C56E5AA3AADDB3AEDAF29B4C28DC3CE733F1 (void);
// 0x00000184 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::get_IsResponse()
extern void HttpHeaderInfo_get_IsResponse_m8445CDB310A70C402D4CC537564B364A8393CC77 (void);
// 0x00000185 System.String WebSocketSharp.Net.HttpHeaderInfo::get_Name()
extern void HttpHeaderInfo_get_Name_m7D0A675E048572A75343E4306954B5C76959337B (void);
// 0x00000186 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsMultiValue(System.Boolean)
extern void HttpHeaderInfo_IsMultiValue_m9B9A07EA640C9DADD177B60384A7FD996CFD1750 (void);
// 0x00000187 System.Boolean WebSocketSharp.Net.HttpHeaderInfo::IsRestricted(System.Boolean)
extern void HttpHeaderInfo_IsRestricted_mEBB9F5F13225DBEC4A37B91FD24307599EC30B96 (void);
// 0x00000188 System.Void WebSocketSharp.Net.NetworkCredential::.cctor()
extern void NetworkCredential__cctor_mB993C68B494A4BC7246CA00465E3A5F8F36B1A9A (void);
// 0x00000189 System.String WebSocketSharp.Net.NetworkCredential::get_Domain()
extern void NetworkCredential_get_Domain_mFB94D1598E8802AF23D7F646281C0B08812DCAC9 (void);
// 0x0000018A System.String WebSocketSharp.Net.NetworkCredential::get_Password()
extern void NetworkCredential_get_Password_m003DF0C4EF9ACBDFFAC11A2F9C64940ED3642A6E (void);
// 0x0000018B System.String WebSocketSharp.Net.NetworkCredential::get_Username()
extern void NetworkCredential_get_Username_m5E1E9C64A75266156B02D4024C519BA79EF401F4 (void);
// 0x0000018C System.Void WebSocketSharp.Net.AuthenticationChallenge::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationChallenge__ctor_m093CF14F79B08EBD75DA4B938ED96E7E536D0494 (void);
// 0x0000018D WebSocketSharp.Net.AuthenticationChallenge WebSocketSharp.Net.AuthenticationChallenge::Parse(System.String)
extern void AuthenticationChallenge_Parse_mECC81F05F7AAA7C7DE9E63F02DE9522FA12F7610 (void);
// 0x0000018E System.String WebSocketSharp.Net.AuthenticationChallenge::ToBasicString()
extern void AuthenticationChallenge_ToBasicString_m296A8E3622C55FD24EB7F3EF8EFEF6FA04FC4D93 (void);
// 0x0000018F System.String WebSocketSharp.Net.AuthenticationChallenge::ToDigestString()
extern void AuthenticationChallenge_ToDigestString_m9595BFE696F7A048E91F51E3611C305794211E80 (void);
// 0x00000190 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.NetworkCredential)
extern void AuthenticationResponse__ctor_m5A29D469AE3F8C2C4C19D8E7F4C484D8215A064F (void);
// 0x00000191 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationChallenge,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m6C106C1C0FAF085626D4A5CFF9FF981F6F579946 (void);
// 0x00000192 System.Void WebSocketSharp.Net.AuthenticationResponse::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection,WebSocketSharp.Net.NetworkCredential,System.UInt32)
extern void AuthenticationResponse__ctor_m9C6F58B5D6B6E7977AB1116EA7B783DED2C81C84 (void);
// 0x00000193 System.UInt32 WebSocketSharp.Net.AuthenticationResponse::get_NonceCount()
extern void AuthenticationResponse_get_NonceCount_m83BE4DEE2F61244C6034DC53FEBDC8D218713220 (void);
// 0x00000194 System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_mEFB9D214211E52CBB3772AFB3856200F181F06D6 (void);
// 0x00000195 System.String WebSocketSharp.Net.AuthenticationResponse::createA1(System.String,System.String,System.String,System.String,System.String)
extern void AuthenticationResponse_createA1_m99AD601D5FD346C25B04CC8F3EB754184F20EF22 (void);
// 0x00000196 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String)
extern void AuthenticationResponse_createA2_mB1D942C7444E7B8A8597BC2B4B4B62D60C095F95 (void);
// 0x00000197 System.String WebSocketSharp.Net.AuthenticationResponse::createA2(System.String,System.String,System.String)
extern void AuthenticationResponse_createA2_m0AA474181F8D6697E514201CC130A09DD4E268A0 (void);
// 0x00000198 System.String WebSocketSharp.Net.AuthenticationResponse::hash(System.String)
extern void AuthenticationResponse_hash_mC7CACE6EBEB9740FE79ADA5F873783D24889C369 (void);
// 0x00000199 System.Void WebSocketSharp.Net.AuthenticationResponse::initAsDigest()
extern void AuthenticationResponse_initAsDigest_m99BA9CCF665FBA240918C4E1A205DDD4572181D1 (void);
// 0x0000019A System.String WebSocketSharp.Net.AuthenticationResponse::CreateRequestDigest(System.Collections.Specialized.NameValueCollection)
extern void AuthenticationResponse_CreateRequestDigest_m94F30288C630197B8F39C0BE19AF2B3D53545B3D (void);
// 0x0000019B System.String WebSocketSharp.Net.AuthenticationResponse::ToBasicString()
extern void AuthenticationResponse_ToBasicString_mC790A6C8E34845332EB0F7CF6979088D9F36EAA3 (void);
// 0x0000019C System.String WebSocketSharp.Net.AuthenticationResponse::ToDigestString()
extern void AuthenticationResponse_ToDigestString_mDE0CBBCBC3F831137E40AAE66D083453B58C037E (void);
// 0x0000019D System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.cctor()
extern void U3CU3Ec__cctor_m3876D76DD9E98B4290B37ECA433EE655DD0264E3 (void);
// 0x0000019E System.Void WebSocketSharp.Net.AuthenticationResponse/<>c::.ctor()
extern void U3CU3Ec__ctor_m258B1D1A43217D71BCBBBCAA69386719E7C41565 (void);
// 0x0000019F System.Boolean WebSocketSharp.Net.AuthenticationResponse/<>c::<initAsDigest>b__24_0(System.String)
extern void U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m0FB685128CCF5AB22CEB9C30A013A13F05941CA7 (void);
// 0x000001A0 System.Void WebSocketSharp.Net.AuthenticationBase::.ctor(WebSocketSharp.Net.AuthenticationSchemes,System.Collections.Specialized.NameValueCollection)
extern void AuthenticationBase__ctor_m780045CE3A2E666A12151B05B2503DE97B5F06D2 (void);
// 0x000001A1 WebSocketSharp.Net.AuthenticationSchemes WebSocketSharp.Net.AuthenticationBase::get_Scheme()
extern void AuthenticationBase_get_Scheme_mFA5FF18363FF1CA3A6DC2B0557E21E0D9959C4F3 (void);
// 0x000001A2 System.String WebSocketSharp.Net.AuthenticationBase::CreateNonceValue()
extern void AuthenticationBase_CreateNonceValue_mA0D8B9815412B228BA2F04C52235A6192A2C6FA3 (void);
// 0x000001A3 System.Collections.Specialized.NameValueCollection WebSocketSharp.Net.AuthenticationBase::ParseParameters(System.String)
extern void AuthenticationBase_ParseParameters_mA6BDFA3DD1413212C29627E293DACD4194E2AAAF (void);
// 0x000001A4 System.String WebSocketSharp.Net.AuthenticationBase::ToBasicString()
// 0x000001A5 System.String WebSocketSharp.Net.AuthenticationBase::ToDigestString()
// 0x000001A6 System.String WebSocketSharp.Net.AuthenticationBase::ToString()
extern void AuthenticationBase_ToString_m7C6E576CBFCF9A92C1884174EA42575B60249976 (void);
// 0x000001A7 System.Void WebSocketSharp.Net.ClientSslConfiguration::.ctor(System.String)
extern void ClientSslConfiguration__ctor_m1B167F2E0B348B91263983CA4AA61AD4838F536F (void);
// 0x000001A8 System.Boolean WebSocketSharp.Net.ClientSslConfiguration::get_CheckCertificateRevocation()
extern void ClientSslConfiguration_get_CheckCertificateRevocation_m7B24B8F0790DE1869936E8347F5B1FB1EB7738ED (void);
// 0x000001A9 System.Security.Cryptography.X509Certificates.X509CertificateCollection WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificates()
extern void ClientSslConfiguration_get_ClientCertificates_mE381B4EC8A853D762129B598419E749EE27A2950 (void);
// 0x000001AA System.Net.Security.LocalCertificateSelectionCallback WebSocketSharp.Net.ClientSslConfiguration::get_ClientCertificateSelectionCallback()
extern void ClientSslConfiguration_get_ClientCertificateSelectionCallback_m06FB336BE16A661973238BDDC1625B0F675B1C69 (void);
// 0x000001AB System.Security.Authentication.SslProtocols WebSocketSharp.Net.ClientSslConfiguration::get_EnabledSslProtocols()
extern void ClientSslConfiguration_get_EnabledSslProtocols_mB7BF9D20D4EA97D59AEF45024DB47F812BAA29AF (void);
// 0x000001AC System.Void WebSocketSharp.Net.ClientSslConfiguration::set_EnabledSslProtocols(System.Security.Authentication.SslProtocols)
extern void ClientSslConfiguration_set_EnabledSslProtocols_m0CBC87A0D52656CA1D16660C0FBE8E58DE9FD878 (void);
// 0x000001AD System.Net.Security.RemoteCertificateValidationCallback WebSocketSharp.Net.ClientSslConfiguration::get_ServerCertificateValidationCallback()
extern void ClientSslConfiguration_get_ServerCertificateValidationCallback_m41FD7F244D3291DCEB2DB977912CA5D28D797D67 (void);
// 0x000001AE System.String WebSocketSharp.Net.ClientSslConfiguration::get_TargetHost()
extern void ClientSslConfiguration_get_TargetHost_mC07B9820A396D43B1118D4866013464121FD30C0 (void);
// 0x000001AF System.Security.Cryptography.X509Certificates.X509Certificate WebSocketSharp.Net.ClientSslConfiguration::defaultSelectClientCertificate(System.Object,System.String,System.Security.Cryptography.X509Certificates.X509CertificateCollection,System.Security.Cryptography.X509Certificates.X509Certificate,System.String[])
extern void ClientSslConfiguration_defaultSelectClientCertificate_mA4432C90BB5A7D76FF77ECA4ADA8AA8C7699AA87 (void);
// 0x000001B0 System.Boolean WebSocketSharp.Net.ClientSslConfiguration::defaultValidateServerCertificate(System.Object,System.Security.Cryptography.X509Certificates.X509Certificate,System.Security.Cryptography.X509Certificates.X509Chain,System.Net.Security.SslPolicyErrors)
extern void ClientSslConfiguration_defaultValidateServerCertificate_mC5B71EAE915901128F41BC432B4FCE7110D99D58 (void);
static Il2CppMethodPointer s_methodPointers[432] = 
{
	Ext_compress_mFF2386B72F7575399F75588F6E3C1C984D26490A,
	Ext_decompress_m92AE0E65DCF5B64050FB33F6C849F15AC39F2BCA,
	Ext_decompress_m2F10FEFF7998061ECE575378251BD96B5A95408D,
	Ext_decompressToArray_mA55F0BFC13285DF96BD11DDC5D260BD253CF90EE,
	Ext_Append_m4FA13F20B7E3A7A864BC88491735A9167E9A8503,
	Ext_Compress_mE7112B59E9B39467AFF41EA993663FE74F693BBA,
	Ext_Contains_m468B61CA7D664E5CEDE17145AFB6F2CA56959E3F,
	Ext_Contains_m2BDB1B6B268DF96B7C2BD8CF43FEA867AE249729,
	NULL,
	Ext_ContainsTwice_m616A23353EE47259F5CB47E5C3CC6DA68D32A0C6,
	Ext_CopyTo_m450F90EDB45896AA6596D30766ECDEDE9FCC5457,
	Ext_Decompress_m0E21E4E11FFEB50F172B160165BE817AE130601B,
	Ext_DecompressToArray_m65411E96820D478E034900AE18E3747B868A6B1E,
	Ext_Emit_m79C1C2BA565E53B878BF76C87844E0022502C99F,
	NULL,
	Ext_EqualsWith_m453D92B0326540A985F6C718D1E1DDA71FEF1240,
	Ext_GetAbsolutePath_m25DE5774315CF299178EBA6A1F8B99BCA94AD5BF,
	Ext_GetCookies_m741901826D65BA10CA3ADF6436BEE0290B47E42B,
	Ext_GetMessage_mA6ADF06B7A7EA49BE8751DD416FC653A662E3111,
	Ext_GetUTF8EncodedBytes_m20B5440F8509FD751A7A53AE89FAD74E5CBD7788,
	Ext_GetValue_mFEEBFBA15B52F78F307754B1D00C45222CD1A2A4,
	Ext_InternalToByteArray_m3F99508A37A0619D30906895C6ECE38030CB4DE8,
	Ext_InternalToByteArray_m168D68DED64267436B0D6D20797F37513DC594B7,
	Ext_IsCompressionExtension_m91B585169EE04BDC3CDBA39DA61FBDAE33FF390D,
	Ext_IsControl_m4A4FF9555814C2E44EAB0078CD9ABE519A3AA03D,
	Ext_IsData_m13B213B41ABBE20EAD75F88961F2CF8A32AB2CFD,
	Ext_IsData_mA4C217749016C4E5E0A32AE4EE6D372723C1F87F,
	Ext_IsReserved_m93BD0ADD3D1136DE3C7F4CECC81EA7F30AFCDFF7,
	Ext_IsSupported_m24DF4666B377F458FD4D50F442CE5CB01D43EA7B,
	Ext_IsText_m86C585BA822A51E96AC3E1DE949C36CDBCA3051E,
	Ext_IsToken_mF9777D58046D57F66FE51DA6068EBC96B17FE9D2,
	Ext_ReadBytes_m6431F23AF3667D9E2D0E6E68003E5F7278E772B9,
	Ext_ReadBytes_mB0A91D33133441E119E3C8D573F393B838FE8A78,
	Ext_ReadBytesAsync_m24CD08D1E3004C0AC48080C76676F2DC9B210539,
	Ext_ReadBytesAsync_mD883C095A1445643302A7E6E7347243121A5C01A,
	NULL,
	Ext_SplitHeaderValue_mD31D552531654796E0E4F609ED8590FAA1FB4D4B,
	Ext_ToByteArray_m9786E30EE1DD790929E73B857694D9F7767A759D,
	Ext_ToExtensionString_mEB7F6250D25025276211B2B70462A3ABC843FFB2,
	NULL,
	Ext_ToUInt16_mA8D2B7C9D6A2BC136B2B95AC7E2F7E3D584C7DB8,
	Ext_ToUInt64_m849E03EE7EFB1F1F9AA0FB96D3404E776ED0B36C,
	Ext_TryCreateWebSocketUri_m063824BF650BF49B4B72A82B621655F178CC6216,
	Ext_TryGetUTF8DecodedString_m1B53E24777D6CAEC191868363B27DE159425923C,
	Ext_TryGetUTF8EncodedBytes_m80427A7B0B3B9830D7E01E0626D4FC62AE195C45,
	Ext_Unquote_m424EE4B1486884BC3360463EF5610A48CAB697A8,
	Ext_Upgrades_m33D72C9CD39201FAB54A32C06939E4E41FBE0CEC,
	Ext_WriteBytes_m5856E58AE264D91FADCA7C952996604F584D32EA,
	Ext_IsEnclosedIn_mCA6CE5321CD97EE93D12B82B9E71EEBC1AC6F5AA,
	Ext_IsHostOrder_mADCA5FA344E9B1EA5644B19328DCDFF60C0AEC47,
	Ext_IsNullOrEmpty_m459B890885FE2BFC7EC73849927ED35BBAE1B907,
	Ext_IsPredefinedScheme_m4A02B68F03400153CA3D469AFC32B77BF2B470FB,
	Ext_MaybeUri_m84DB0A02813B60D91A57CA74B10FBC270624E20A,
	NULL,
	NULL,
	Ext_ToHostOrder_m4B341B05107B49BF842745AEC9B27113A0ECCC1C,
	NULL,
	Ext_ToUri_m3ECD656039D3E369DDD14B54608FDC910964EC53,
	Ext__cctor_m82BE2DC4CEE777B9F0535D04493B8BBA4AD601F5,
	U3CU3Ec__DisplayClass21_0__ctor_m4D95DCC3A614785D3620A9FBBD53EAB65E42C120,
	U3CU3Ec__DisplayClass21_0_U3CContainsTwiceU3Eb__0_mEF444A313D0C8DE0C1533E4DDD9247AADAFE06B8,
	U3CU3Ec__DisplayClass59_0__ctor_m6CF091F86167E328AB130BD134BC4379A497C66E,
	U3CU3Ec__DisplayClass59_0_U3CReadBytesAsyncU3Eb__0_mC598FE8327165061E8789168FDD65F14CF34E40F,
	U3CU3Ec__DisplayClass60_0__ctor_m90B9DFB6D1D8EB5F79A7E27DF4550CA7AAD36F40,
	U3CU3Ec__DisplayClass60_0_U3CReadBytesAsyncU3Eb__0_mA94324D78097EFE93C6FEAC0ABD355B0FD31DF38,
	U3CU3Ec__DisplayClass60_1__ctor_m0A0C7AFC9E9833FA308E5B51B218A0E489C63235,
	U3CU3Ec__DisplayClass60_1_U3CReadBytesAsyncU3Eb__1_m1590B1FE581500ED3BAA2926D51352D21B38C140,
	U3CSplitHeaderValueU3Ed__62__ctor_m4947AD83FCF1E7D05FFB1A5A7AE6BDA6B8B61636,
	U3CSplitHeaderValueU3Ed__62_System_IDisposable_Dispose_m485A81956BF2962DC4C5756DED791F8C7E7CF296,
	U3CSplitHeaderValueU3Ed__62_MoveNext_mF5F340FDF2E8293C4F8FE2686C7AA388080B6CFE,
	U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_StringU3E_get_Current_mF988C53F31C9F5B42F4F5F04155156F523393EB7,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_Reset_m2B1FD2E85EC99BBCBE67688F13EC518BD0354DA9,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerator_get_Current_m1E4C862449F6B389CBB48FAADC2A48B7B0BAAD16,
	U3CSplitHeaderValueU3Ed__62_System_Collections_Generic_IEnumerableU3CSystem_StringU3E_GetEnumerator_m8B0217989B5DAF54298793065C0850A7E564636D,
	U3CSplitHeaderValueU3Ed__62_System_Collections_IEnumerable_GetEnumerator_mB0FEE593E87A323370D786D610EC1B05BCA963B9,
	MessageEventArgs__ctor_m1678081D31D7CD4B692E9608FB9E7B83D091FD1B,
	MessageEventArgs__ctor_m708E0D41654ED75BB7871A33B1B3E822783BEEFC,
	MessageEventArgs_get_Data_mFEC4784B676BB8FF003998987406CD3F97611A24,
	MessageEventArgs_setData_m2EA766C5D004AD5A87725A928F2F0527ACCF1D78,
	CloseEventArgs__ctor_m4EBC593FB0CBF76A010514143E2697FDF6F91B11,
	ErrorEventArgs__ctor_m12DC0127CF8C6C9C17200421E6EBC3C87CD371F1,
	WebSocket__cctor_m43A6D37B49988E5A926C9F28A07D5431443E7E18,
	WebSocket__ctor_mF920780429570048096CE4C53E52047BDB4A96E3,
	WebSocket_get_HasMessage_mC65BD115FB02B6CEEECD554DBDE13C3AB3A8D167,
	WebSocket_get_ReadyState_m6EC9CAA4FC7E05324A5511A636ABBE7D246CD205,
	WebSocket_get_SslConfiguration_m3842ED20C16DE807EBABDACCA1601FFD9E86C52F,
	WebSocket_add_OnClose_m87DA5064206524E18A2E53398D4E5C0E764247EC,
	WebSocket_remove_OnClose_m550C4E629A1DDE62423D99F4FF614365E0FF83C3,
	WebSocket_add_OnError_mD7A6AB282F3FA80E678B630F5A4252AB5F44270E,
	WebSocket_remove_OnError_m499E42EAC4E822756FF619AB8F79EDC6FAC33533,
	WebSocket_add_OnMessage_m302E620ACF771C3E9484820E86A716F491B3A114,
	WebSocket_remove_OnMessage_mA8746E68E24230FDACD7DBEB06BB88CDD22BA294,
	WebSocket_add_OnOpen_m094F4D363E4DD05F673B24DD08B3E65D34A3DD56,
	WebSocket_remove_OnOpen_m0489D4798A1F7A7505B2221F080AE793814C25C9,
	WebSocket_checkHandshakeResponse_m89E29EE43D10B0BA774BD8A1B5758E70BEE3632F,
	WebSocket_checkProtocols_mFD547A5A23DB78291BB972EC241282FE57F084BA,
	WebSocket_checkReceivedFrame_m1D5AF13E27983E50BCC4A726A459237B5178CCE6,
	WebSocket_close_m6E08B4EB032759DB499E454471D60A4B1671D6B6,
	WebSocket_close_m75F44D7E0440AE608D0DE690B76DAB06D5C2BB32,
	WebSocket_closeHandshake_mED49C83540A15079ED6232C5CDD790978ACE2870,
	WebSocket_connect_m3DED6601C29145009A4379169E5D95D08A9B9994,
	WebSocket_createExtensions_mC18C77C4FBF3BBAB2D690E556217E2BE3BAAEAD5,
	WebSocket_createHandshakeRequest_mB53780ED31913EBC94D335203B944AA6770AEEC8,
	WebSocket_doHandshake_m88CA65138730B519469C6344E2C3B9DEB6BF6443,
	WebSocket_enqueueToMessageEventQueue_m7A3B74DCB04612793239E63604235944DABE3554,
	WebSocket_error_m476B8617EC5262BF91D8BE45E7AAF9E5F58818BD,
	WebSocket_fatal_m92DB97F5F3B0B7CD1C2F910E94AA86B126CCF66A,
	WebSocket_fatal_m52D64EAC64042C2D118081F76208FDE74F47C8B8,
	WebSocket_fatal_mD576696A36A8E220915D076BCF1DFB8137278842,
	WebSocket_getSslConfiguration_m58728D2C50138B4BD550042AD972C953CFE7C0FB,
	WebSocket_init_mF0F10F1C6278FF997F63DB76DC94656E0982687E,
	WebSocket_message_m60D5929947E7F99F3A6BF16AF772C34DEE049B26,
	WebSocket_messagec_m6E35DD1FA27A0E4246214EE1BF3001A6ACDC0DF6,
	WebSocket_open_m073654D707503FF174619367143CB424BDF5D3C5,
	WebSocket_processCloseFrame_m6C3E7E4B10079C4EBBB8ABEBC2F38AFC27915C6C,
	WebSocket_processCookies_m46F0A73A853B8E0EFE166CDD21A39E7F3F60F213,
	WebSocket_processDataFrame_mA6B8C27D98B98E87F3B41616ED799189666C968C,
	WebSocket_processFragmentFrame_mC3DCB3DB694188974E320E9308F5B8651248F729,
	WebSocket_processPingFrame_mA820D830C477EF97D2DDC9E3DB2D0ED3BDAB9EE0,
	WebSocket_processPongFrame_m0E4BDFEA37639774C9C9936AD391E8D5523C7470,
	WebSocket_processReceivedFrame_m3750D43D5A16FE2D88ABAC40458C95A114B2C48A,
	WebSocket_processSecWebSocketExtensionsServerHeader_m31A1F42B0BF48C53F6EFDDBA155B07EF8304CD0C,
	WebSocket_processUnsupportedFrame_m11BEAB3A278652B2318383C6604C291EA8B9BE37,
	WebSocket_releaseClientResources_m012236D1A15E0D5272115759B2AB1BFE0C34C78C,
	WebSocket_releaseCommonResources_m2C48EB30FCCC0A62E18B22ACD2EE5C1356CD590C,
	WebSocket_releaseResources_m337E04A4C465E91FE06C9A622CD43295BFE42C5A,
	WebSocket_releaseServerResources_m97AEA3904AB71BCEB67095064C0F45C06357F64C,
	WebSocket_send_m60E4951F34AF2663FAA8B347C842FDD64C3B479C,
	WebSocket_send_mEEB5585DB1485647F06B044CE86634F6CDE99D25,
	WebSocket_send_mEEB96347B838FBB42716F4B5C72547F58E84BBC8,
	WebSocket_sendBytes_m4E9702DB4E947B7E32862D0CAA6204DAF057DAC7,
	WebSocket_sendHandshakeRequest_m0D51578A6F735BF225C2DBD26C8D0CEC27CBC8E9,
	WebSocket_sendHttpRequest_m8F39E3875D617905195C12EE3FAE773847858B8C,
	WebSocket_sendProxyConnectRequest_m4012D0E637058DAF072410021411C70DC6C2E565,
	WebSocket_setClientStream_m31358BF98AC0D365EF190B7FF0CE422F50AF60E2,
	WebSocket_startReceiving_mEC0B934892E7AA5260579421AE08AAD88A712B22,
	WebSocket_validateSecWebSocketAcceptHeader_m3FE2B3B24BCECB3CC9799C334BA04964B3BC94DE,
	WebSocket_validateSecWebSocketExtensionsServerHeader_mCD01F80B3AB645D7F37DC2646CFACEFA92FBA7AC,
	WebSocket_validateSecWebSocketProtocolServerHeader_mF1D7F3AF8ACB1A3333607ADA844771F14FB97CB7,
	WebSocket_validateSecWebSocketVersionServerHeader_mD210DB2EC20908262EBE8A1DE6EC1B386B24B7CB,
	WebSocket_CreateBase64Key_m99849C279F55F47E3B75588648B70E2DE0705129,
	WebSocket_CreateResponseKey_m2C91C613261CBF7622381B23D28EC53E17EC1D25,
	WebSocket_Close_mC5B5E2D838145811D2C0B95239C06F27830B2457,
	WebSocket_Connect_mF39921089B05F788F74C2D4F302F734D694655CB,
	WebSocket_Send_mEDFB10DEBA56C1EDBCBE946A35483CB33DD668E7,
	WebSocket_System_IDisposable_Dispose_m88E2268CF6E7677361A64E1182EA3C75F28C1B69,
	WebSocket_U3CopenU3Eb__146_0_mF9501C7E369ACF5B42D12F621C8AD99EDD791D16,
	U3CU3Ec__cctor_m997D0CC0E78135A5F4D22F841C9C4036C5BD802A,
	U3CU3Ec__ctor_mC77D1EB2199A666AF11F6D450E4284B48EDFB80E,
	U3CU3Ec_U3CcheckProtocolsU3Eb__120_0_mBB5AEEEBF723C8139AC88D3C6443A62B1EF6D1F8,
	U3CU3Ec__DisplayClass174_0__ctor_mFD40AE931F1EB441AE8E4F4A57A4F07F1A9695AC,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__0_mE022932040D7BCFEC430F2B3CC66AE4968BE3795,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__1_m0DE13D272693941D43FBA626C3D0E4687774224D,
	U3CU3Ec__DisplayClass174_0_U3CstartReceivingU3Eb__2_mF4EAAED569908D918BFED1EB89121051DE23F774,
	U3CU3Ec__DisplayClass176_0__ctor_mBBE2A8FDEC5CAFC8650309CFEB6DC30298ECFC6F,
	U3CU3Ec__DisplayClass176_0_U3CvalidateSecWebSocketExtensionsServerHeaderU3Eb__0_mEF2C8B7989241C284625F1EEFAECB6233B2CAA87,
	U3CU3Ec__DisplayClass177_0__ctor_m9688AF2B60774E0438CD1B9967A6531D8E56B643,
	U3CU3Ec__DisplayClass177_0_U3CvalidateSecWebSocketProtocolServerHeaderU3Eb__0_m4B896A77D7AE967FC9489116E821CE4CB32CB25E,
	PayloadData__cctor_mCC0B4C41AB15F8A8FF2DFD8B5A766C3E09D413FC,
	PayloadData__ctor_m8C2F6B3C49CA40C5E49CC4F4AA4C5C3549536059,
	PayloadData__ctor_mFE0A9E867715E7797A60FE2098281B6C842FF3C5,
	PayloadData__ctor_m16D098A88C53C43F0F6B95D66766116D403D1290,
	PayloadData_get_Code_m08AEA1D6C72194E60F367163B348ACFBB0D8A2B5,
	PayloadData_get_HasReservedCode_mD224366677D4EDA15D1DDB51568152680D4657FD,
	PayloadData_get_ApplicationData_m5DCA2540F328CDB2CD4654F46E6D16BA06721F1B,
	PayloadData_get_Length_mF4AE5CDDE931D807598A8C3700C54DBDB7479326,
	PayloadData_Mask_m21B14F9AF19A81CFC728368916F9C79520821A7F,
	PayloadData_GetEnumerator_m0A78BE9E677E27E7BD6D93BD1656E6A36585117C,
	PayloadData_ToArray_mBD195843E959B2CF4B689E0CFAA1F06EEC2D5FF9,
	PayloadData_ToString_mE0593CD7665F8F1138BB9F36F139AA80BD8EA061,
	PayloadData_System_Collections_IEnumerable_GetEnumerator_mB2C65ADD4D1D4D0B68581B9D247A1B7B068AF115,
	U3CGetEnumeratorU3Ed__25__ctor_mEC4773BDB32A7F860EE711AB860A92C95C8FF3E5,
	U3CGetEnumeratorU3Ed__25_System_IDisposable_Dispose_m6C047B2406BDB116F7659A1BED31A613A3FAC62B,
	U3CGetEnumeratorU3Ed__25_MoveNext_m545483F181D8B19384B6C50A2228E19CCD48E005,
	U3CGetEnumeratorU3Ed__25_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_m4EF48828AC26EED610858272E954323C3E3D7D96,
	U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_Reset_m7D396DDF6B2E7DB0C795304A47FC34C7B60A7CFC,
	U3CGetEnumeratorU3Ed__25_System_Collections_IEnumerator_get_Current_m07E3D9A4D39D4ECD072716868D1753CB9C9F9421,
	WebSocketException__ctor_mE03D1C75C25F011141F070E56F2A68E36EB42E33,
	WebSocketException__ctor_mB1BD02FE74FA2ECFE16C5FDFB45A2772DA8BD53B,
	WebSocketException__ctor_mC922F25C693428D877B64DE1B9DEF2F37404E356,
	WebSocketException__ctor_m0CE78BB4EEF20CC235C7AA8CCC7D15E9068D1663,
	WebSocketException__ctor_m6F1C0FCFC10B22398810AD9877D47396DB9ADACB,
	WebSocketException__ctor_m1B6AC7EE1F96C0CF54B1E706B604AD84C52F5739,
	WebSocketException_get_Code_m5DD6F0F32504A49E59BD8C4D7DCA4EC8D596089D,
	LogData__ctor_m0FB260BE27F33BFBCB6B89AE2AEEC8534ECB69CD,
	LogData_ToString_m1786313AF452EEADD58C95A7DB2C1B7B31DD992C,
	Logger__ctor_m188F37801EB484DA8FB0C470D674156EE80FEE47,
	Logger__ctor_mBB45C64E3EC8C7A330B86A0D43BAB679D07F0C7F,
	Logger_defaultOutput_m86A2F5C370D7AE1DCBF742C7E6E25B4EB3B5DE2E,
	Logger_output_m8219D15E1D9A9959B9753B572F34671051A62FF3,
	Logger_writeToFile_mE6BC853E148075107F323E5AF097B4C4160C8E6D,
	Logger_Debug_m3C57355209E31A0EA2DDE10CC117F44C5AC0EA6E,
	Logger_Error_mD95F27AD968016872B1B16CEDA3111ED91F7711A,
	Logger_Fatal_mF470B1B726FCD446A2AF77A4C26135C39ECB9324,
	Logger_Info_m95673A43768120650EF063537EEB89BA9741CDE2,
	Logger_Trace_m4E727C4328507A6C26007532BAF2850908A1DC9B,
	Logger_Warn_m6DB65389722F56CA2E7CC8D9646F00B109753DC5,
	WebSocketFrame__cctor_m91615407503A0D088B9504E38ED5BA7174789D06,
	WebSocketFrame__ctor_mE99CD753932524E5F8851D80950C151D46D6F280,
	WebSocketFrame__ctor_m9AB8F64F675C8F46504A4378107972B2886931FE,
	WebSocketFrame__ctor_m14DD6951BEE0243E9E22C540F6788AAC722F90FD,
	WebSocketFrame_get_ExactPayloadLength_m605F587F3F54965D633FF133D1BD434F13E4A4A2,
	WebSocketFrame_get_ExtendedPayloadLengthWidth_m0D415F7771AAEF7A232B73615C78A2299DCD0E09,
	WebSocketFrame_get_IsClose_mD8FBFC6D324E9346E0151EC3BB8B02807566F872,
	WebSocketFrame_get_IsCompressed_m3DEA00AB4A0779963FEFA6A1AE43B8E5E04ED349,
	WebSocketFrame_get_IsContinuation_m63A8419363A23A9C1C366D4A2CD4134530756F89,
	WebSocketFrame_get_IsData_m10663EEBF3BBE9C6BCA77008C08C3870F93FEE53,
	WebSocketFrame_get_IsFinal_mB4CF208B4204EC5FF33B71D3EC4CFF48D339ECB1,
	WebSocketFrame_get_IsFragment_m44E622C54F3A344561EE7A84FC91AE5BE2DAD95E,
	WebSocketFrame_get_IsMasked_m49F042A1576CB2128FA718566665250BABF25BEC,
	WebSocketFrame_get_IsPing_m3FEC05F0737AFA86DF7768C6DACE8440B6FE76A1,
	WebSocketFrame_get_IsPong_mA4B8FD203DB5A8BC42BD47A6F6395E3850D44C04,
	WebSocketFrame_get_IsText_m101237563F9DE72B5EA70A95F79921CF2CF245C0,
	WebSocketFrame_get_Length_m770761839F3A8166C2ACF5105558DABE1F67883B,
	WebSocketFrame_get_Opcode_m1FCDC8C889A75E128758775AF83BCEFBBA0AA74C,
	WebSocketFrame_get_PayloadData_mA55D15155851FBB72EC8CAF272E37CD8E4BDD9CD,
	WebSocketFrame_get_Rsv2_m0A02760B830871708FEEC6A9DD4E61D580820CA7,
	WebSocketFrame_get_Rsv3_m25DDD5B4D361551F85803B4DFD9EA8EA3FAD2E2E,
	WebSocketFrame_createMaskingKey_m293E3CFB87AD37B21F073510C0A4446CE86A7409,
	WebSocketFrame_dump_m9FD919B931DDE6E5A5B09FB7ABA543EE4FDF67E0,
	WebSocketFrame_print_m5E0101FB29EF074F79E310511FEEDCB946213259,
	WebSocketFrame_processHeader_m5CF3AF4145C8D118A56498F0FA3A80ADC7AC2924,
	WebSocketFrame_readExtendedPayloadLengthAsync_m0FFD3BA5AD62D60E62DA087B07A013C5C43E3521,
	WebSocketFrame_readHeaderAsync_mD4F2B5FBD6A279ACAE7D5678DA1E56D580247227,
	WebSocketFrame_readMaskingKeyAsync_m1CF02B707402B69F8BB4F764D8402B4539A20F16,
	WebSocketFrame_readPayloadDataAsync_m51C17AB8DE4F0652A672122D911F18F5450DD8F9,
	WebSocketFrame_utf8Decode_m64622C695D35BDCEA02C7CD4AFFDEB304FC29CC0,
	WebSocketFrame_CreateCloseFrame_mA229A746CC15F41B0C80BE4EF78960727CA68890,
	WebSocketFrame_CreatePingFrame_m92B7DA5E6B62621A64E6130E54D11DC9C8826EF1,
	WebSocketFrame_CreatePongFrame_m51B76225D048BF803152D886343F4CF0033EECBC,
	WebSocketFrame_ReadFrameAsync_m8D58108EA4B5E245BE55A90A8B34D00FDA79239C,
	WebSocketFrame_Unmask_m2EC9DB25D033D729F917E1671C01E1FDFFB826A9,
	WebSocketFrame_GetEnumerator_mA04DC82E8CC70687C4B0343D394E08B1C1A1405E,
	WebSocketFrame_PrintToString_m2A9BE958EABE1D835ABC247C2F02DCA2BC9ED280,
	WebSocketFrame_ToArray_m929D6B5B1D8D7FA6373A422DDA71FF7EE571EBD2,
	WebSocketFrame_ToString_m6FE7EC48017642D3230980BD2F300936B858C5DC,
	WebSocketFrame_System_Collections_IEnumerable_GetEnumerator_m5DFC9900829CB188E15BD971487116512EC5A487,
	U3CU3Ec__DisplayClass67_0__ctor_m978C7BA4386A44DD8E6760E2F373E2BDAAFDD9DD,
	U3CU3Ec__DisplayClass67_0_U3CdumpU3Eb__0_mFE1EBF1D8036E5294CB9B6EDAC0A794C1AC9AF13,
	U3CU3Ec__DisplayClass67_1__ctor_m30785213BA90551F86B24FA5088978DD52FAF9A0,
	U3CU3Ec__DisplayClass67_1_U3CdumpU3Eb__1_m70418A2B861DED1452DFEA96014A46B5377F32F3,
	U3CU3Ec__DisplayClass71_0__ctor_m3DEC4374692DCD7D5082AC31829694D45F1A14D7,
	U3CU3Ec__DisplayClass71_0_U3CreadExtendedPayloadLengthAsyncU3Eb__0_m19AF767DDCADA3B9DEAC6CE8499818E8D5DCE2FA,
	U3CU3Ec__DisplayClass73_0__ctor_m96F1E0FA4ACE30782081D890B2A05A0A4F87E458,
	U3CU3Ec__DisplayClass73_0_U3CreadHeaderAsyncU3Eb__0_m0FA594A94DD8A7E82AB941C67B5F31AC36557DE7,
	U3CU3Ec__DisplayClass75_0__ctor_m9D8EFE8C1FD803CC76471F1B738B4D8912E91FA1,
	U3CU3Ec__DisplayClass75_0_U3CreadMaskingKeyAsyncU3Eb__0_m82927E0D099EAD926A3AB402EA2B331106B15D50,
	U3CU3Ec__DisplayClass77_0__ctor_mE1B6304D6E0B374CF457F8BF710E4ADB191C43AD,
	U3CU3Ec__DisplayClass77_0_U3CreadPayloadDataAsyncU3Eb__0_m23195F5DE3434C2A0047C313CCC8629F68A8EDDD,
	U3CU3Ec__DisplayClass84_0__ctor_m8CD6DF83D40066E2E4F3F09EAB26B05A2FD1CD7D,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__0_m8AD35ABF3DB66FB0F12D6F43630242969E35BF0F,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__1_m1FEA1B1FFB470548B342816B7FCB9B5566E04FF7,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__2_m1323A87FF1330665554C62C2BDF2A81D707B1746,
	U3CU3Ec__DisplayClass84_0_U3CReadFrameAsyncU3Eb__3_m0007EF78D8E6B2F7E2E3898EA3F5A1427E4C64E6,
	U3CGetEnumeratorU3Ed__86__ctor_m3534EFFACACFDE35A5B01293B1FC61312F996C63,
	U3CGetEnumeratorU3Ed__86_System_IDisposable_Dispose_m85D50301C9284D31439B4B9096B93B41BA0EF18B,
	U3CGetEnumeratorU3Ed__86_MoveNext_m8BE980A0427DDC379068FF91220FD2944F66E90B,
	U3CGetEnumeratorU3Ed__86_System_Collections_Generic_IEnumeratorU3CSystem_ByteU3E_get_Current_mF93BF87617181C1665503998BC1855E534D52437,
	U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_Reset_m190045B074085CC054A2E63F9C4520ADC553A2C5,
	U3CGetEnumeratorU3Ed__86_System_Collections_IEnumerator_get_Current_m10B4AACDE813921FBBBA83A4E9FAC4E4A1A1F970,
	HttpBase__ctor_m6410A7FC51E49A3A7F55CE5FF49B4EE1571455F7,
	HttpBase_get_EntityBody_mE08132C946C8F47778E559F36BD32D49B5FD059C,
	HttpBase_get_Headers_m25BE165BFF37F78E1BAEEBFFCC30A2D853507A81,
	HttpBase_get_ProtocolVersion_mA5593022C745F3C9E31C857C1F2E0B81B7B342CA,
	HttpBase_readEntityBody_mC45967336F8B3EDE429BB3023749D17BD4C29D32,
	HttpBase_readHeaders_m40ADE558276E6F64AB3C77A61FBA957B97C9CE36,
	NULL,
	HttpBase_ToByteArray_m4B9B4F475614BF1F54F3ACC3CB2060D460A64E8C,
	U3CU3Ec__DisplayClass13_0__ctor_m51C643CEFE62CDD10248D47C3402C56288B0C040,
	U3CU3Ec__DisplayClass13_0_U3CreadHeadersU3Eb__0_m7966C6C1502F6DF0EFB91EF521F476D68A7B4797,
	NULL,
	NULL,
	HttpRequest__ctor_m7FADDCF5DC773405AB6A036319CA0780F5B15917,
	HttpRequest__ctor_m285303CEE5EE9C3F44EBC174A0CC4E00AC7BA207,
	HttpRequest_CreateConnectRequest_m08DD15312D4FD78D58493B70DCCAC64692431160,
	HttpRequest_CreateWebSocketRequest_m33086CF1B925A33079CF4E637B7560DC114B17D4,
	HttpRequest_GetResponse_m9021A6B2CA836B094FD48CF87E4E99C3AEAF33CD,
	HttpRequest_SetCookies_mC533EE1997B4C590AD0368386CE9D0785D480CC6,
	HttpRequest_ToString_m923F26F03BF4F6990FE74C265C1E2ED82895D828,
	HttpResponse__ctor_m76F880A7B55BBFEA6DF52354CC177E213B13728B,
	HttpResponse_get_Cookies_mA00E684F6D50629DE16A2451EA0648E0840527EC,
	HttpResponse_get_HasConnectionClose_mA94A71096F02608F465BEC8306DD7F2EF1FC284F,
	HttpResponse_get_IsProxyAuthenticationRequired_m0FF90ABD0D5A97FD5B98FB52A90DFC947F4B3DA7,
	HttpResponse_get_IsRedirect_mB5964EB771B6D05E52E9381E7459363A6CF57687,
	HttpResponse_get_IsUnauthorized_mBB9B176B318F53162B226AC9BFD2E00D5B5FAC68,
	HttpResponse_get_IsWebSocketResponse_mE4ED10EA6F9796994480FA06DD15AC5955538B66,
	HttpResponse_get_StatusCode_mC8D3E715DA17FA6C3DD4CDA3B1ACAB407F6638D3,
	HttpResponse_Parse_mD5DD2BDB86688A41EC2579C07AA56069302AD554,
	HttpResponse_ToString_m7A260B2DA36C3D2A52A13D70B9A5F5700CA74C52,
	Cookie__cctor_m07BACF458BB412F7D0A70907EDF41B4DB79EF946,
	Cookie__ctor_mF6E35CA69CCCE2F3C3BAE6B14DADD82C82E80AF1,
	Cookie__ctor_mF5D2A664D8EC2B231F415E88EC6E1F324381C397,
	Cookie__ctor_m174AFEC71A470E616B6425C170DA3ECB260E4F50,
	Cookie_set_MaxAge_m86545D003C2E0C08EEECE50BFBBDCE52313459DA,
	Cookie_set_SameSite_m816D245EC12E7DD8EC6D1E2B63AE1E999C60DAF9,
	Cookie_set_Comment_m26AFC2233BB294325E866D033CF84DE5F7341249,
	Cookie_set_CommentUri_m228C7E1B8B6D31C6B655663B77143F2BD46D49E0,
	Cookie_set_Discard_mE57DCF3EE566B46272DDF54EEFF1FBC4A97E0E3A,
	Cookie_set_Domain_mF3A14D39CD41A65F0C17B0B53FC60E8C551BCEF8,
	Cookie_get_Expired_m8EC7F7EFBBED40F72BC5B96C4EE81295062D643C,
	Cookie_get_Expires_m3AE44AFB79CE495922E2FDE6B9870036A8741F32,
	Cookie_set_Expires_m340A433911EC90D5707185619201EBBC06E1C2F9,
	Cookie_set_HttpOnly_m5A3A15346F84567C1E8E309A82DE0EBA780E13BE,
	Cookie_get_Name_m395E432C349675E3FBC78FD697D89E2C7F373221,
	Cookie_get_Path_m19F17342CF37FE9A602398E3C88EBB9E446F2CBE,
	Cookie_set_Path_m7CCAD956F9CFD54E2F9F5BE36C40E8C1EA074179,
	Cookie_set_Port_mD49691D11FB3FE738E51553B1332E6E16D9869B9,
	Cookie_set_Secure_mD569D72B70205D3156A0C426E05FD231B79961FA,
	Cookie_get_Version_mF59096D7225D85322328397F3D4922CD2E1743ED,
	Cookie_set_Version_m77A60028CD71562419DB6479EB2C093F93FE2C97,
	Cookie_hash_mB953932A1AFBB34D13C6FD478D4C73B49CAB7F09,
	Cookie_init_mD6774A6A2106E8BFEEAF95BA3073A5A3DCFAD95E,
	Cookie_tryCreatePorts_mF11D29D69E4EBAE3B5CE935C711EDA35538C0649,
	Cookie_EqualsWithoutValue_mE199341AF932B39EB9FD1E20132A718F89ACF220,
	Cookie_ToRequestString_m9FE9617FDC4CCBFABCFF8586260C6ED59E57204F,
	Cookie_TryCreate_m8453EB906605500C578BEBC9F2E0563AB1487B7D,
	Cookie_Equals_mEADC36AAC11684B2B5F6E46A0E1C2ECCA7A723EE,
	Cookie_GetHashCode_m878E2266AAF7598E34199F60AD848D0548A05EB7,
	Cookie_ToString_mC9E15F34A8487C582DB87C22B59C50F78B48DB9A,
	CookieCollection__ctor_mD6A730BFD4A8682CEF34F578CEC489D723FAC7C4,
	CookieCollection_get_Sorted_m9D884937831EC235D75B4F4C8E15BD2B1581C9BB,
	CookieCollection_get_Count_mE025F97CCB76F1D2DC31E18319C0D31CFE093896,
	CookieCollection_get_IsReadOnly_m7857A4F54D4F2D01558481957F5AEB59BB7A065C,
	CookieCollection_add_m8014254B58F787E6F7CC6605136FA86F901AC354,
	CookieCollection_compareForSorted_mE7A71F15F2AB5BE16C8E4F99753322BE800337BD,
	CookieCollection_parseRequest_mAB79611FA5D5D0AD862680C5D553C9041ADF5E30,
	CookieCollection_parseResponse_m78E4FA322AEA68E3DF216A102D9ACE8385B9A1B2,
	CookieCollection_search_mDC2EF547B9F49E08A2E8AD6BF57C990001D0EA58,
	CookieCollection_urlDecode_mA72EDAADCFE3504F812DACF06D41D30A8AE5A514,
	CookieCollection_Parse_m55CCAEED8509FC5E746EA090B5712173D2E95284,
	CookieCollection_SetOrRemove_m072D1649CFE56117B927AC33D3B91BD95C4B278E,
	CookieCollection_SetOrRemove_m644171D09DD61F11190B08F825FAFB75C534F5EE,
	CookieCollection_Add_m334EB67DCC37317C004EED5BF1A3A9579975BCCF,
	CookieCollection_Clear_m79BA8B4F40B8B3930342D8BA03461B05680AE94C,
	CookieCollection_Contains_m04C9C46A3203F1B5D62332CBA64BB6DEBFD9254E,
	CookieCollection_CopyTo_m57CA7A5CBDE6A06947DE530CBB424624FE3403D2,
	CookieCollection_GetEnumerator_m96C386CCA13979305BEDF3C119A6C659F40C1D76,
	CookieCollection_Remove_mBEFBE9C876C3FE47A23140835BC4268B2CD85929,
	CookieCollection_System_Collections_IEnumerable_GetEnumerator_m629141A43B9B7FA068E6AEA485BAC29971624805,
	CookieException__ctor_mB0CFD42EC8F601C6319264D18383081F63A94438,
	CookieException__ctor_m9E78B0D68512826A19CDECFB01512C0C55476167,
	CookieException__ctor_m11AFD4DD0630C66345C989DF6E0199F065C4D257,
	CookieException_GetObjectData_m4A68E43EDD01A85DA3958BFB67A4720DEC74807B,
	CookieException_System_Runtime_Serialization_ISerializable_GetObjectData_m54C79DDA7AEE083DF337665649088A5533927F92,
	HttpUtility__cctor_m8F8E3B2E694350C431B2E2801C71E3F33F12BB09,
	HttpUtility_getNumber_mC477ECA96D529EE51964FC0FC6BB7E70141148DB,
	HttpUtility_getNumber_m2A5E60E84032C21CE58894CA6F97A8B5807F258D,
	HttpUtility_urlDecodeToBytes_m0531F0EF78A40882F97D36F401EB662F1B4700C4,
	HttpUtility_GetEncoding_m797DA56C36D3E3167F6E61397E5486A9E480693F,
	HttpUtility_UrlDecode_mB9E42353EBEBBB13F1784C7F00A718CA43AF72CB,
	WebHeaderCollection__cctor_m267B6CCFC243CA9913C1EA8046A7D173A71C6A33,
	WebHeaderCollection__ctor_mC9592A7BAD3DAEC954D1C68778F564B9802110DF,
	WebHeaderCollection__ctor_mB7C7027B1B1162AAF4C24451BCDE12452120BA9D,
	WebHeaderCollection_get_AllKeys_mF2E7E003E0A2C429CD0410BB53380B03BB6EF68B,
	WebHeaderCollection_get_Count_m9FF09EE578FE53D4E39EFC2AAA6A914E8A157811,
	WebHeaderCollection_add_mF05DE9CE4B6783A568CCBD1C3ECA0BB2BFC3F59D,
	WebHeaderCollection_addWithoutCheckingName_mBE54E66829EE6B3BFC5711EDEF02CB584F2A6354,
	WebHeaderCollection_addWithoutCheckingNameAndRestricted_mBFF5C90396A190F6A27D17C4AB569AD315159F3C,
	WebHeaderCollection_checkHeaderType_m8157A359CDC9454E6742E26D3D7E87F34F45590F,
	WebHeaderCollection_checkName_m1D378B16A49730F918654A2F439978D4A6619ADE,
	WebHeaderCollection_checkRestricted_mA0E3BA3FE61611BFBD6011D0B74BA54468DD66D2,
	WebHeaderCollection_checkState_mA817555DDFA4B95A117A3849091606C1E3693DEC,
	WebHeaderCollection_checkValue_mFE23D810596119F48D584F10F7D9701885628CB1,
	WebHeaderCollection_doWithCheckingState_m12BDD18B6E80BF89056D8BFBD0E62007C0CE4E0F,
	WebHeaderCollection_doWithCheckingState_mE5E8CBA7BC66741521C8541A74FC72A35A151410,
	WebHeaderCollection_doWithoutCheckingName_mFB02AA03A2ECE9CD9E8C44AE56032BF217F30109,
	WebHeaderCollection_getHeaderInfo_m6F7796834288982DAD38D4C9EE0D815005797A95,
	WebHeaderCollection_isMultiValue_m3DFD8B34EE3FC9B6E342C10E13C6D68C2F40739B,
	WebHeaderCollection_isRestricted_mCC4F7A9B1609A4BBEA95B2F32335C4005672E5B2,
	WebHeaderCollection_setWithoutCheckingName_m4B6C061AC842A940A0FBF41F781265F10B3890FB,
	WebHeaderCollection_InternalSet_m9661440478A12153990740148323279EEE372FC1,
	WebHeaderCollection_InternalSet_m4F2E0D7CBDE816E0658277201AE7C14155D9B8BC,
	WebHeaderCollection_Add_mC1AD2FAF5A75D9DBB08ED75103C060E50157F15D,
	WebHeaderCollection_Get_mFBC05DC1D01A5D6F75A2727DE04EFBFB13C98105,
	WebHeaderCollection_Get_m2ACD6D76927F5B2C385D978603E5B91E9979B399,
	WebHeaderCollection_GetEnumerator_m2584519B2CC2EC9C35A9EF8E204958CA2827971A,
	WebHeaderCollection_GetKey_m9DC3B0918B36F290D6CF5924F144698345B4D4B9,
	WebHeaderCollection_GetObjectData_m80A17AF53CD06A61E74E95776354E0462361EDF1,
	WebHeaderCollection_OnDeserialization_m14CD79B0B96926FB6BE6345B03DB293092AE6865,
	WebHeaderCollection_Set_mE09547AD9C58236C42125AD98789E4CC4483ACD7,
	WebHeaderCollection_ToString_m71A86D619F341B35E337D02D49AEDC0C6799D5B9,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m1112DB1C93A5D03616CD16AB414F9B842B6B37B1,
	HttpVersion__cctor_m451D97B21FFC971885FDF699D8520BEE3D7B03A7,
	HttpHeaderInfo__ctor_m82FC5E0079770F60B09B1D082940E7CB252EDDE0,
	HttpHeaderInfo_get_IsMultiValueInRequest_m0C02D84427289DCE654C7FC27C7E45307425E684,
	HttpHeaderInfo_get_IsMultiValueInResponse_m5E03A8D2E20FD46319953871856051D50F74AE76,
	HttpHeaderInfo_get_IsRequest_mEEF7C56E5AA3AADDB3AEDAF29B4C28DC3CE733F1,
	HttpHeaderInfo_get_IsResponse_m8445CDB310A70C402D4CC537564B364A8393CC77,
	HttpHeaderInfo_get_Name_m7D0A675E048572A75343E4306954B5C76959337B,
	HttpHeaderInfo_IsMultiValue_m9B9A07EA640C9DADD177B60384A7FD996CFD1750,
	HttpHeaderInfo_IsRestricted_mEBB9F5F13225DBEC4A37B91FD24307599EC30B96,
	NetworkCredential__cctor_mB993C68B494A4BC7246CA00465E3A5F8F36B1A9A,
	NetworkCredential_get_Domain_mFB94D1598E8802AF23D7F646281C0B08812DCAC9,
	NetworkCredential_get_Password_m003DF0C4EF9ACBDFFAC11A2F9C64940ED3642A6E,
	NetworkCredential_get_Username_m5E1E9C64A75266156B02D4024C519BA79EF401F4,
	AuthenticationChallenge__ctor_m093CF14F79B08EBD75DA4B938ED96E7E536D0494,
	AuthenticationChallenge_Parse_mECC81F05F7AAA7C7DE9E63F02DE9522FA12F7610,
	AuthenticationChallenge_ToBasicString_m296A8E3622C55FD24EB7F3EF8EFEF6FA04FC4D93,
	AuthenticationChallenge_ToDigestString_m9595BFE696F7A048E91F51E3611C305794211E80,
	AuthenticationResponse__ctor_m5A29D469AE3F8C2C4C19D8E7F4C484D8215A064F,
	AuthenticationResponse__ctor_m6C106C1C0FAF085626D4A5CFF9FF981F6F579946,
	AuthenticationResponse__ctor_m9C6F58B5D6B6E7977AB1116EA7B783DED2C81C84,
	AuthenticationResponse_get_NonceCount_m83BE4DEE2F61244C6034DC53FEBDC8D218713220,
	AuthenticationResponse_createA1_mEFB9D214211E52CBB3772AFB3856200F181F06D6,
	AuthenticationResponse_createA1_m99AD601D5FD346C25B04CC8F3EB754184F20EF22,
	AuthenticationResponse_createA2_mB1D942C7444E7B8A8597BC2B4B4B62D60C095F95,
	AuthenticationResponse_createA2_m0AA474181F8D6697E514201CC130A09DD4E268A0,
	AuthenticationResponse_hash_mC7CACE6EBEB9740FE79ADA5F873783D24889C369,
	AuthenticationResponse_initAsDigest_m99BA9CCF665FBA240918C4E1A205DDD4572181D1,
	AuthenticationResponse_CreateRequestDigest_m94F30288C630197B8F39C0BE19AF2B3D53545B3D,
	AuthenticationResponse_ToBasicString_mC790A6C8E34845332EB0F7CF6979088D9F36EAA3,
	AuthenticationResponse_ToDigestString_mDE0CBBCBC3F831137E40AAE66D083453B58C037E,
	U3CU3Ec__cctor_m3876D76DD9E98B4290B37ECA433EE655DD0264E3,
	U3CU3Ec__ctor_m258B1D1A43217D71BCBBBCAA69386719E7C41565,
	U3CU3Ec_U3CinitAsDigestU3Eb__24_0_m0FB685128CCF5AB22CEB9C30A013A13F05941CA7,
	AuthenticationBase__ctor_m780045CE3A2E666A12151B05B2503DE97B5F06D2,
	AuthenticationBase_get_Scheme_mFA5FF18363FF1CA3A6DC2B0557E21E0D9959C4F3,
	AuthenticationBase_CreateNonceValue_mA0D8B9815412B228BA2F04C52235A6192A2C6FA3,
	AuthenticationBase_ParseParameters_mA6BDFA3DD1413212C29627E293DACD4194E2AAAF,
	NULL,
	NULL,
	AuthenticationBase_ToString_m7C6E576CBFCF9A92C1884174EA42575B60249976,
	ClientSslConfiguration__ctor_m1B167F2E0B348B91263983CA4AA61AD4838F536F,
	ClientSslConfiguration_get_CheckCertificateRevocation_m7B24B8F0790DE1869936E8347F5B1FB1EB7738ED,
	ClientSslConfiguration_get_ClientCertificates_mE381B4EC8A853D762129B598419E749EE27A2950,
	ClientSslConfiguration_get_ClientCertificateSelectionCallback_m06FB336BE16A661973238BDDC1625B0F675B1C69,
	ClientSslConfiguration_get_EnabledSslProtocols_mB7BF9D20D4EA97D59AEF45024DB47F812BAA29AF,
	ClientSslConfiguration_set_EnabledSslProtocols_m0CBC87A0D52656CA1D16660C0FBE8E58DE9FD878,
	ClientSslConfiguration_get_ServerCertificateValidationCallback_m41FD7F244D3291DCEB2DB977912CA5D28D797D67,
	ClientSslConfiguration_get_TargetHost_mC07B9820A396D43B1118D4866013464121FD30C0,
	ClientSslConfiguration_defaultSelectClientCertificate_mA4432C90BB5A7D76FF77ECA4ADA8AA8C7699AA87,
	ClientSslConfiguration_defaultValidateServerCertificate_mC5B71EAE915901128F41BC432B4FCE7110D99D58,
};
static const int32_t s_InvokerIndices[432] = 
{
	0,
	0,
	0,
	0,
	299,
	167,
	141,
	3194,
	-1,
	109,
	210,
	167,
	167,
	201,
	-1,
	3195,
	0,
	167,
	104,
	0,
	3196,
	3197,
	302,
	617,
	5,
	5,
	5,
	48,
	5,
	109,
	109,
	126,
	3198,
	573,
	3199,
	-1,
	1,
	0,
	2247,
	-1,
	222,
	223,
	364,
	229,
	229,
	0,
	141,
	210,
	2289,
	46,
	109,
	109,
	109,
	-1,
	-1,
	126,
	-1,
	0,
	3,
	23,
	30,
	23,
	26,
	23,
	213,
	23,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	26,
	88,
	14,
	23,
	102,
	27,
	3,
	27,
	89,
	250,
	14,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	924,
	229,
	924,
	1100,
	1478,
	3200,
	89,
	14,
	14,
	23,
	26,
	27,
	27,
	938,
	938,
	14,
	23,
	23,
	26,
	23,
	9,
	26,
	9,
	9,
	9,
	9,
	9,
	26,
	9,
	23,
	23,
	23,
	23,
	2868,
	3201,
	3202,
	9,
	14,
	58,
	23,
	23,
	23,
	9,
	9,
	9,
	9,
	4,
	0,
	23,
	23,
	26,
	23,
	26,
	3,
	23,
	9,
	23,
	23,
	26,
	26,
	23,
	9,
	23,
	9,
	3,
	26,
	186,
	1100,
	250,
	89,
	14,
	187,
	26,
	14,
	14,
	14,
	14,
	32,
	23,
	89,
	89,
	23,
	14,
	26,
	602,
	27,
	1100,
	1100,
	3203,
	250,
	368,
	14,
	23,
	368,
	143,
	137,
	143,
	26,
	26,
	26,
	26,
	26,
	26,
	3,
	23,
	3204,
	3204,
	187,
	10,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	89,
	187,
	89,
	14,
	89,
	89,
	4,
	0,
	0,
	0,
	2354,
	201,
	2354,
	2354,
	0,
	167,
	814,
	167,
	3205,
	23,
	14,
	130,
	14,
	14,
	14,
	23,
	14,
	23,
	442,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	26,
	26,
	26,
	32,
	23,
	89,
	89,
	23,
	14,
	27,
	14,
	14,
	14,
	1,
	126,
	-1,
	14,
	23,
	32,
	-1,
	-1,
	442,
	27,
	0,
	0,
	58,
	26,
	14,
	442,
	14,
	89,
	89,
	89,
	89,
	89,
	14,
	0,
	14,
	3,
	23,
	27,
	442,
	32,
	26,
	26,
	26,
	31,
	26,
	89,
	119,
	331,
	31,
	14,
	14,
	26,
	26,
	31,
	10,
	32,
	3206,
	442,
	229,
	9,
	28,
	388,
	9,
	10,
	14,
	23,
	14,
	10,
	89,
	26,
	144,
	0,
	0,
	121,
	1,
	167,
	26,
	26,
	26,
	23,
	9,
	137,
	14,
	9,
	14,
	27,
	120,
	23,
	120,
	120,
	3,
	103,
	579,
	208,
	0,
	1,
	3,
	120,
	23,
	14,
	10,
	162,
	27,
	27,
	94,
	0,
	26,
	31,
	0,
	955,
	3207,
	212,
	0,
	617,
	617,
	27,
	102,
	162,
	27,
	34,
	28,
	14,
	34,
	120,
	26,
	27,
	14,
	120,
	3,
	137,
	89,
	89,
	89,
	89,
	14,
	227,
	227,
	3,
	14,
	14,
	14,
	62,
	0,
	14,
	14,
	26,
	110,
	910,
	10,
	2,
	508,
	1,
	2,
	0,
	23,
	0,
	14,
	14,
	3,
	23,
	9,
	62,
	10,
	4,
	0,
	14,
	14,
	14,
	26,
	89,
	14,
	14,
	10,
	32,
	14,
	14,
	508,
	3194,
};
static const Il2CppTokenRangePair s_rgctxIndices[8] = 
{
	{ 0x06000009, { 0, 3 } },
	{ 0x0600000F, { 3, 1 } },
	{ 0x06000024, { 4, 1 } },
	{ 0x06000028, { 5, 2 } },
	{ 0x06000036, { 7, 1 } },
	{ 0x06000037, { 8, 1 } },
	{ 0x06000039, { 9, 1 } },
	{ 0x0600010B, { 10, 5 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[15] = 
{
	{ (Il2CppRGCTXDataType)2, 60368 },
	{ (Il2CppRGCTXDataType)2, 63788 },
	{ (Il2CppRGCTXDataType)3, 62958 },
	{ (Il2CppRGCTXDataType)3, 62959 },
	{ (Il2CppRGCTXDataType)2, 60375 },
	{ (Il2CppRGCTXDataType)2, 60379 },
	{ (Il2CppRGCTXDataType)3, 62960 },
	{ (Il2CppRGCTXDataType)2, 60380 },
	{ (Il2CppRGCTXDataType)2, 60382 },
	{ (Il2CppRGCTXDataType)2, 60385 },
	{ (Il2CppRGCTXDataType)2, 63789 },
	{ (Il2CppRGCTXDataType)3, 62961 },
	{ (Il2CppRGCTXDataType)3, 62962 },
	{ (Il2CppRGCTXDataType)3, 62963 },
	{ (Il2CppRGCTXDataType)2, 60497 },
};
extern const Il2CppCodeGenModule g_websocketU2DsharpCodeGenModule;
const Il2CppCodeGenModule g_websocketU2DsharpCodeGenModule = 
{
	"websocket-sharp.dll",
	432,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	8,
	s_rgctxIndices,
	15,
	s_rgctxValues,
	NULL,
};
