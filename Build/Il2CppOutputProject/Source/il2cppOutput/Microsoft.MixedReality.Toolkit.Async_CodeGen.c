﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Runtime.CompilerServices.ConfiguredTaskAwaitable/ConfiguredTaskAwaiter Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread::GetAwaiter()
extern void WaitForBackgroundThread_GetAwaiter_mF29D4014CC0BC2F1AFA6D9AB5F7162142F057471 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread::.ctor()
extern void WaitForBackgroundThread__ctor_m42B7B7F831C858FC8598884B369B1A28361013D3 (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::.cctor()
extern void U3CU3Ec__cctor_m3D2D720D6837BA9ED83F10784AE853C99FCFAE79 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::.ctor()
extern void U3CU3Ec__ctor_m90FD90DFBE825E43AC563BF4F641B24A7F4F5A8F (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForBackgroundThread/<>c::<GetAwaiter>b__0_0()
extern void U3CU3Ec_U3CGetAwaiterU3Eb__0_0_mB6C7BA1DCC8C793CEB2E106DC9A3D96D084F9F0B (void);
// 0x00000006 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate::get_keepWaiting()
extern void WaitForUpdate_get_keepWaiting_mF47C3FF644CD41D90BA469D241A8EAB9288ADDDF (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate::.ctor()
extern void WaitForUpdate__ctor_mDE62FF2503784F90BDF2FAF89A3487D3B7D7F275 (void);
// 0x00000008 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForSeconds)
extern void AwaiterExtensions_GetAwaiter_m324EA9E3ACDC2C9414FD592629FCA8BE3CD922B6 (void);
// 0x00000009 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(Microsoft.MixedReality.Toolkit.Utilities.WaitForUpdate)
extern void AwaiterExtensions_GetAwaiter_mB043B125836C7573DC2670EA01974ACC6E82C7EC (void);
// 0x0000000A Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForEndOfFrame)
extern void AwaiterExtensions_GetAwaiter_mC0E37527543B5C05B1B2BD7AC6D286CB0B5D0061 (void);
// 0x0000000B Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForFixedUpdate)
extern void AwaiterExtensions_GetAwaiter_mF80542AFFD14B8DC8C7036CC04859D9B3804F7D0 (void);
// 0x0000000C Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitForSecondsRealtime)
extern void AwaiterExtensions_GetAwaiter_m198603EA0D4D08EAC7774AF160F03813A44206C7 (void);
// 0x0000000D Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitUntil)
extern void AwaiterExtensions_GetAwaiter_m27292A47E155CEEFAB3A0197F9BD00A7A4281A18 (void);
// 0x0000000E Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.WaitWhile)
extern void AwaiterExtensions_GetAwaiter_m739FD4646E8CDA42AF2BF43EA26B88E789F02B32 (void);
// 0x0000000F Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AsyncOperation> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AsyncOperation)
extern void AwaiterExtensions_GetAwaiter_m3D0C8453B54B0090DE37328BE6C362B53DEE40E0 (void);
// 0x00000010 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.ResourceRequest)
extern void AwaiterExtensions_GetAwaiter_m5FB2C3D42DFBF088BFA7620E79EB1802C3DB5B94 (void);
// 0x00000011 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AssetBundleCreateRequest)
extern void AwaiterExtensions_GetAwaiter_m1A0EB4C6A4AE4D2186CB646C755B68C255B9E90C (void);
// 0x00000012 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(UnityEngine.AssetBundleRequest)
extern void AwaiterExtensions_GetAwaiter_m5D272B9C2A9F9489ADE066BA4350104CAFB097DD (void);
// 0x00000013 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(System.Collections.Generic.IEnumerator`1<T>)
// 0x00000014 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<System.Object> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiter(System.Collections.IEnumerator)
extern void AwaiterExtensions_GetAwaiter_m36EB058F2E3363C4B0BB0F9AB6DABF01538DE690 (void);
// 0x00000015 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiterReturnVoid(System.Object)
extern void AwaiterExtensions_GetAwaiterReturnVoid_m1434E97189D5984D79E1F4BA09DC6781359DDF83 (void);
// 0x00000016 Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::GetAwaiterReturnSelf(T)
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions::RunOnUnityScheduler(System.Action)
extern void AwaiterExtensions_RunOnUnityScheduler_m95A50C3FD69CC807FDCE7671B494749C204D4465 (void);
// 0x00000018 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::get_IsCompleted()
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::set_IsCompleted(System.Boolean)
// 0x0000001A T Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::GetResult()
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::Complete(T,System.Exception)
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1::.ctor()
// 0x0000001E System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::get_IsCompleted()
extern void SimpleCoroutineAwaiter_get_IsCompleted_m4B266CB59DA91A3FFA3514B0FAEE2F619EC63D98 (void);
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::set_IsCompleted(System.Boolean)
extern void SimpleCoroutineAwaiter_set_IsCompleted_m9B75EFD3243F22A72403FFACA03E3E7E1C22515B (void);
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::GetResult()
extern void SimpleCoroutineAwaiter_GetResult_m50B1134C9B4A58EC645E63DEA52F86548108B331 (void);
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::Complete(System.Exception)
extern void SimpleCoroutineAwaiter_Complete_m7572B5F64BE682084E84BC4E808BD0670A094197 (void);
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::System.Runtime.CompilerServices.INotifyCompletion.OnCompleted(System.Action)
extern void SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_m50D444D463485A6CE4EA57D51B348FDAA6B33484 (void);
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter::.ctor()
extern void SimpleCoroutineAwaiter__ctor_m44AC75C00F88A2BC4F60AB3EDC93C6B88A34EFF4 (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::.ctor(System.Collections.IEnumerator,Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T>)
// 0x00000025 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::Run()
// 0x00000026 System.String Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::GenerateObjectTraceMessage(System.Collections.Generic.List`1<System.Type>)
// 0x00000027 System.Collections.Generic.List`1<System.Type> Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1::GenerateObjectTrace(System.Collections.Generic.IEnumerable`1<System.Collections.IEnumerator>)
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::.ctor(System.Int32)
// 0x00000029 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.IDisposable.Dispose()
// 0x0000002A System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::MoveNext()
// 0x0000002B System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x0000002C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.IEnumerator.Reset()
// 0x0000002D System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/CoroutineWrapper`1/<Run>d__3::System.Collections.IEnumerator.get_Current()
// 0x0000002E System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ReturnVoid(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter,System.Object)
extern void InstructionWrappers_ReturnVoid_m8D7F247AECE409AA047AF587190F1FD92684C715 (void);
// 0x0000002F System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::AssetBundleCreateRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.AssetBundle>,UnityEngine.AssetBundleCreateRequest)
extern void InstructionWrappers_AssetBundleCreateRequest_m828AAA2BB41B2B046614C5EAEA736F6446255DD7 (void);
// 0x00000030 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ReturnSelf(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<T>,T)
// 0x00000031 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::AssetBundleRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.AssetBundleRequest)
extern void InstructionWrappers_AssetBundleRequest_m99D9BB7AE69D9F8440C00D93AA39190D71365CD3 (void);
// 0x00000032 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers::ResourceRequest(Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/SimpleCoroutineAwaiter`1<UnityEngine.Object>,UnityEngine.ResourceRequest)
extern void InstructionWrappers_ResourceRequest_mC345475B9656ECB95DBC5ECD05D2E2004E133F28 (void);
// 0x00000033 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::.ctor(System.Int32)
extern void U3CReturnVoidU3Ed__0__ctor_m805A1734D6DBEE2460BB98AC5BF3E82179CA3722 (void);
// 0x00000034 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.IDisposable.Dispose()
extern void U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_mA1CA9DD2DBE226609FF183767D3626A462AB2CB7 (void);
// 0x00000035 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::MoveNext()
extern void U3CReturnVoidU3Ed__0_MoveNext_m1E79DBE6F055D98F2AF8604CD8C57DF8CB920A32 (void);
// 0x00000036 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9CAEB64248E96850C929AA666E5D5C03FF1CA03 (void);
// 0x00000037 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.IEnumerator.Reset()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mAFC1FF57ECB84AFD250A7B0F513D104733094547 (void);
// 0x00000038 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnVoid>d__0::System.Collections.IEnumerator.get_Current()
extern void U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m5C99A70142367642508C2548477AF5D8356B3784 (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::.ctor(System.Int32)
extern void U3CAssetBundleCreateRequestU3Ed__1__ctor_m44D821D6CA6DFCD990EA8005BF3C2C9D567F283F (void);
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.IDisposable.Dispose()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_mE7BB85C0FF6808E07E90C906A04009182ACEEBD5 (void);
// 0x0000003B System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::MoveNext()
extern void U3CAssetBundleCreateRequestU3Ed__1_MoveNext_mA2F9CDA63204BC6CA3BFEFFCC130B4C5E1A5C0F3 (void);
// 0x0000003C System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m227B617D2B7B447EC6902D2F5677FB23975337E6 (void);
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mEE7D5C17B89131D2E37EF531E66D01BE5C4ABB72 (void);
// 0x0000003E System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleCreateRequest>d__1::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m6DBFD12D2F9DA29B78423D95E3B22025111CC4AB (void);
// 0x0000003F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::.ctor(System.Int32)
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.IDisposable.Dispose()
// 0x00000041 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::MoveNext()
// 0x00000042 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.IEnumerator.Reset()
// 0x00000044 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ReturnSelf>d__2`1::System.Collections.IEnumerator.get_Current()
// 0x00000045 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::.ctor(System.Int32)
extern void U3CAssetBundleRequestU3Ed__3__ctor_mBF07E9116C350F6C46F587234EDB1B94F50B035E (void);
// 0x00000046 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.IDisposable.Dispose()
extern void U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_m54745D4578749F50E54341C8D50990BCD7C3978D (void);
// 0x00000047 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::MoveNext()
extern void U3CAssetBundleRequestU3Ed__3_MoveNext_m80C9588F8FC22FFD1FAE0FE841D39B2941727013 (void);
// 0x00000048 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEEF40678B46E9CB5AC9236A7873B4EBF2E21EAB (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_m5C3BACF5B7FCC6BDAD515A521C414C3EB9B677A4 (void);
// 0x0000004A System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<AssetBundleRequest>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m87D8F2BAE159643CBA663931A3A3CFD4BF6E0084 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::.ctor(System.Int32)
extern void U3CResourceRequestU3Ed__4__ctor_mCF13DFD05F952E7D395946F0EBAAA24A5A0BCB0E (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.IDisposable.Dispose()
extern void U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m0E474CEB06CD40AD2F0E60B2ABEC2BB2A2578108 (void);
// 0x0000004D System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::MoveNext()
extern void U3CResourceRequestU3Ed__4_MoveNext_mFC8AE2D6F5662AB5EC5FEB9C7779CA4D2F07EC0A (void);
// 0x0000004E System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9945BEA3D6FFAD8BFA4BC43BA7D44E93164BEFF1 (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.IEnumerator.Reset()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_m55AC746612ADCD3A2FFFFF46357FD8B8F00B0B1D (void);
// 0x00000050 System.Object Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/InstructionWrappers/<ResourceRequest>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m4D4E39621DC00EBC860A6513554924DF6556BE6F (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass8_0::.ctor()
extern void U3CU3Ec__DisplayClass8_0__ctor_m99EAB9413C996411B9A7EF0434ACBE3FBB9FCBC1 (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass8_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_mB180584EB8D17858A3DC709EA2231C9407F05D40 (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass9_0::.ctor()
extern void U3CU3Ec__DisplayClass9_0__ctor_mE8AEA5C7BE9AE97B004389DC2E1B1A28D6AE617D (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass9_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass9_0_U3CGetAwaiterU3Eb__0_m07F802B97F2C6B6034E0B4EAADE90F216BBD7E89 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mBAB6E4408F9F2A9261164AE1CF4B766D4154B752 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass10_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_m7D52CC75C171B5618EC8C1EC5D7213F22BEBE52D (void);
// 0x00000057 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass11_0`1::.ctor()
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass11_0`1::<GetAwaiter>b__0()
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_mBB768393642DE4653381997CE084717F40E7A3DC (void);
// 0x0000005A System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass12_0::<GetAwaiter>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CGetAwaiterU3Eb__0_m1475956890798CA4D422D52E48A335B2A4814922 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mE8D93F2782E19AC5A8E6AEFD1C3882B1AAC886D8 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass13_0::<GetAwaiterReturnVoid>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CGetAwaiterReturnVoidU3Eb__0_m87B5B15336947B87858E72181AD99E64B37D5918 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass14_0`1::.ctor()
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Utilities.AwaiterExtensions/<>c__DisplayClass14_0`1::<GetAwaiterReturnSelf>b__0()
// 0x0000005F Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::get_Instance()
extern void AsyncCoroutineRunner_get_Instance_m070A37E4D0F8D16A3444BA2E1882D925D039BA50 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::Post(System.Action)
extern void AsyncCoroutineRunner_Post_mA77F8E794E3EB7BBFCC51BD4892A703DE3E48126 (void);
// 0x00000061 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::get_IsInstanceRunning()
extern void AsyncCoroutineRunner_get_IsInstanceRunning_mE9E2F0E08860AF52D490AB4ACFB7A011836B1B51 (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::Update()
extern void AsyncCoroutineRunner_Update_m21F2B8A7BEF3AFE1F291F95E42978F2EEF60384F (void);
// 0x00000063 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::OnDisable()
extern void AsyncCoroutineRunner_OnDisable_m831BE7C60D098B3323D36558193EB2CA4C4E8F3C (void);
// 0x00000064 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::OnEnable()
extern void AsyncCoroutineRunner_OnEnable_mAC4F7A847BA35E9E98881D17A7FB93AC36950E5E (void);
// 0x00000065 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::.ctor()
extern void AsyncCoroutineRunner__ctor_m4398F02D58DD271B5F46A4EA42CA17D3C1F4AAB9 (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Utilities.AsyncCoroutineRunner::.cctor()
extern void AsyncCoroutineRunner__cctor_m5E7989000EF63FF6C8956D97519A10696E5E863E (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::Initialize()
extern void SyncContextUtility_Initialize_m431D8FD0A793DD412FE02D417E8297CE1B96D995 (void);
// 0x00000068 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_UnityThreadId()
extern void SyncContextUtility_get_UnityThreadId_m715F42A84E74371E3F5736A6BE04319D2A165539 (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::set_UnityThreadId(System.Int32)
extern void SyncContextUtility_set_UnityThreadId_mE039A497B5205A9B49019229DF15566C3139216F (void);
// 0x0000006A System.Threading.SynchronizationContext Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_UnitySynchronizationContext()
extern void SyncContextUtility_get_UnitySynchronizationContext_mB35C545797F2EAE60F081CD20E02A36BCA2A5521 (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::set_UnitySynchronizationContext(System.Threading.SynchronizationContext)
extern void SyncContextUtility_set_UnitySynchronizationContext_mF45FCEAFCB314FC48A1FD14CCBE4BFDA321FFD91 (void);
// 0x0000006C System.Boolean Microsoft.MixedReality.Toolkit.Utilities.SyncContextUtility::get_IsMainThread()
extern void SyncContextUtility_get_IsMainThread_m54AC36A18A6EF0E73E512CFA96E102AE6F571961 (void);
static Il2CppMethodPointer s_methodPointers[108] = 
{
	WaitForBackgroundThread_GetAwaiter_mF29D4014CC0BC2F1AFA6D9AB5F7162142F057471,
	WaitForBackgroundThread__ctor_m42B7B7F831C858FC8598884B369B1A28361013D3,
	U3CU3Ec__cctor_m3D2D720D6837BA9ED83F10784AE853C99FCFAE79,
	U3CU3Ec__ctor_m90FD90DFBE825E43AC563BF4F641B24A7F4F5A8F,
	U3CU3Ec_U3CGetAwaiterU3Eb__0_0_mB6C7BA1DCC8C793CEB2E106DC9A3D96D084F9F0B,
	WaitForUpdate_get_keepWaiting_mF47C3FF644CD41D90BA469D241A8EAB9288ADDDF,
	WaitForUpdate__ctor_mDE62FF2503784F90BDF2FAF89A3487D3B7D7F275,
	AwaiterExtensions_GetAwaiter_m324EA9E3ACDC2C9414FD592629FCA8BE3CD922B6,
	AwaiterExtensions_GetAwaiter_mB043B125836C7573DC2670EA01974ACC6E82C7EC,
	AwaiterExtensions_GetAwaiter_mC0E37527543B5C05B1B2BD7AC6D286CB0B5D0061,
	AwaiterExtensions_GetAwaiter_mF80542AFFD14B8DC8C7036CC04859D9B3804F7D0,
	AwaiterExtensions_GetAwaiter_m198603EA0D4D08EAC7774AF160F03813A44206C7,
	AwaiterExtensions_GetAwaiter_m27292A47E155CEEFAB3A0197F9BD00A7A4281A18,
	AwaiterExtensions_GetAwaiter_m739FD4646E8CDA42AF2BF43EA26B88E789F02B32,
	AwaiterExtensions_GetAwaiter_m3D0C8453B54B0090DE37328BE6C362B53DEE40E0,
	AwaiterExtensions_GetAwaiter_m5FB2C3D42DFBF088BFA7620E79EB1802C3DB5B94,
	AwaiterExtensions_GetAwaiter_m1A0EB4C6A4AE4D2186CB646C755B68C255B9E90C,
	AwaiterExtensions_GetAwaiter_m5D272B9C2A9F9489ADE066BA4350104CAFB097DD,
	NULL,
	AwaiterExtensions_GetAwaiter_m36EB058F2E3363C4B0BB0F9AB6DABF01538DE690,
	AwaiterExtensions_GetAwaiterReturnVoid_m1434E97189D5984D79E1F4BA09DC6781359DDF83,
	NULL,
	AwaiterExtensions_RunOnUnityScheduler_m95A50C3FD69CC807FDCE7671B494749C204D4465,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	SimpleCoroutineAwaiter_get_IsCompleted_m4B266CB59DA91A3FFA3514B0FAEE2F619EC63D98,
	SimpleCoroutineAwaiter_set_IsCompleted_m9B75EFD3243F22A72403FFACA03E3E7E1C22515B,
	SimpleCoroutineAwaiter_GetResult_m50B1134C9B4A58EC645E63DEA52F86548108B331,
	SimpleCoroutineAwaiter_Complete_m7572B5F64BE682084E84BC4E808BD0670A094197,
	SimpleCoroutineAwaiter_System_Runtime_CompilerServices_INotifyCompletion_OnCompleted_m50D444D463485A6CE4EA57D51B348FDAA6B33484,
	SimpleCoroutineAwaiter__ctor_m44AC75C00F88A2BC4F60AB3EDC93C6B88A34EFF4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InstructionWrappers_ReturnVoid_m8D7F247AECE409AA047AF587190F1FD92684C715,
	InstructionWrappers_AssetBundleCreateRequest_m828AAA2BB41B2B046614C5EAEA736F6446255DD7,
	NULL,
	InstructionWrappers_AssetBundleRequest_m99D9BB7AE69D9F8440C00D93AA39190D71365CD3,
	InstructionWrappers_ResourceRequest_mC345475B9656ECB95DBC5ECD05D2E2004E133F28,
	U3CReturnVoidU3Ed__0__ctor_m805A1734D6DBEE2460BB98AC5BF3E82179CA3722,
	U3CReturnVoidU3Ed__0_System_IDisposable_Dispose_mA1CA9DD2DBE226609FF183767D3626A462AB2CB7,
	U3CReturnVoidU3Ed__0_MoveNext_m1E79DBE6F055D98F2AF8604CD8C57DF8CB920A32,
	U3CReturnVoidU3Ed__0_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mC9CAEB64248E96850C929AA666E5D5C03FF1CA03,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_Reset_mAFC1FF57ECB84AFD250A7B0F513D104733094547,
	U3CReturnVoidU3Ed__0_System_Collections_IEnumerator_get_Current_m5C99A70142367642508C2548477AF5D8356B3784,
	U3CAssetBundleCreateRequestU3Ed__1__ctor_m44D821D6CA6DFCD990EA8005BF3C2C9D567F283F,
	U3CAssetBundleCreateRequestU3Ed__1_System_IDisposable_Dispose_mE7BB85C0FF6808E07E90C906A04009182ACEEBD5,
	U3CAssetBundleCreateRequestU3Ed__1_MoveNext_mA2F9CDA63204BC6CA3BFEFFCC130B4C5E1A5C0F3,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m227B617D2B7B447EC6902D2F5677FB23975337E6,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_Reset_mEE7D5C17B89131D2E37EF531E66D01BE5C4ABB72,
	U3CAssetBundleCreateRequestU3Ed__1_System_Collections_IEnumerator_get_Current_m6DBFD12D2F9DA29B78423D95E3B22025111CC4AB,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CAssetBundleRequestU3Ed__3__ctor_mBF07E9116C350F6C46F587234EDB1B94F50B035E,
	U3CAssetBundleRequestU3Ed__3_System_IDisposable_Dispose_m54745D4578749F50E54341C8D50990BCD7C3978D,
	U3CAssetBundleRequestU3Ed__3_MoveNext_m80C9588F8FC22FFD1FAE0FE841D39B2941727013,
	U3CAssetBundleRequestU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDEEF40678B46E9CB5AC9236A7873B4EBF2E21EAB,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_Reset_m5C3BACF5B7FCC6BDAD515A521C414C3EB9B677A4,
	U3CAssetBundleRequestU3Ed__3_System_Collections_IEnumerator_get_Current_m87D8F2BAE159643CBA663931A3A3CFD4BF6E0084,
	U3CResourceRequestU3Ed__4__ctor_mCF13DFD05F952E7D395946F0EBAAA24A5A0BCB0E,
	U3CResourceRequestU3Ed__4_System_IDisposable_Dispose_m0E474CEB06CD40AD2F0E60B2ABEC2BB2A2578108,
	U3CResourceRequestU3Ed__4_MoveNext_mFC8AE2D6F5662AB5EC5FEB9C7779CA4D2F07EC0A,
	U3CResourceRequestU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9945BEA3D6FFAD8BFA4BC43BA7D44E93164BEFF1,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_Reset_m55AC746612ADCD3A2FFFFF46357FD8B8F00B0B1D,
	U3CResourceRequestU3Ed__4_System_Collections_IEnumerator_get_Current_m4D4E39621DC00EBC860A6513554924DF6556BE6F,
	U3CU3Ec__DisplayClass8_0__ctor_m99EAB9413C996411B9A7EF0434ACBE3FBB9FCBC1,
	U3CU3Ec__DisplayClass8_0_U3CGetAwaiterU3Eb__0_mB180584EB8D17858A3DC709EA2231C9407F05D40,
	U3CU3Ec__DisplayClass9_0__ctor_mE8AEA5C7BE9AE97B004389DC2E1B1A28D6AE617D,
	U3CU3Ec__DisplayClass9_0_U3CGetAwaiterU3Eb__0_m07F802B97F2C6B6034E0B4EAADE90F216BBD7E89,
	U3CU3Ec__DisplayClass10_0__ctor_mBAB6E4408F9F2A9261164AE1CF4B766D4154B752,
	U3CU3Ec__DisplayClass10_0_U3CGetAwaiterU3Eb__0_m7D52CC75C171B5618EC8C1EC5D7213F22BEBE52D,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass12_0__ctor_mBB768393642DE4653381997CE084717F40E7A3DC,
	U3CU3Ec__DisplayClass12_0_U3CGetAwaiterU3Eb__0_m1475956890798CA4D422D52E48A335B2A4814922,
	U3CU3Ec__DisplayClass13_0__ctor_mE8D93F2782E19AC5A8E6AEFD1C3882B1AAC886D8,
	U3CU3Ec__DisplayClass13_0_U3CGetAwaiterReturnVoidU3Eb__0_m87B5B15336947B87858E72181AD99E64B37D5918,
	NULL,
	NULL,
	AsyncCoroutineRunner_get_Instance_m070A37E4D0F8D16A3444BA2E1882D925D039BA50,
	AsyncCoroutineRunner_Post_mA77F8E794E3EB7BBFCC51BD4892A703DE3E48126,
	AsyncCoroutineRunner_get_IsInstanceRunning_mE9E2F0E08860AF52D490AB4ACFB7A011836B1B51,
	AsyncCoroutineRunner_Update_m21F2B8A7BEF3AFE1F291F95E42978F2EEF60384F,
	AsyncCoroutineRunner_OnDisable_m831BE7C60D098B3323D36558193EB2CA4C4E8F3C,
	AsyncCoroutineRunner_OnEnable_mAC4F7A847BA35E9E98881D17A7FB93AC36950E5E,
	AsyncCoroutineRunner__ctor_m4398F02D58DD271B5F46A4EA42CA17D3C1F4AAB9,
	AsyncCoroutineRunner__cctor_m5E7989000EF63FF6C8956D97519A10696E5E863E,
	SyncContextUtility_Initialize_m431D8FD0A793DD412FE02D417E8297CE1B96D995,
	SyncContextUtility_get_UnityThreadId_m715F42A84E74371E3F5736A6BE04319D2A165539,
	SyncContextUtility_set_UnityThreadId_mE039A497B5205A9B49019229DF15566C3139216F,
	SyncContextUtility_get_UnitySynchronizationContext_mB35C545797F2EAE60F081CD20E02A36BCA2A5521,
	SyncContextUtility_set_UnitySynchronizationContext_mF45FCEAFCB314FC48A1FD14CCBE4BFDA321FFD91,
	SyncContextUtility_get_IsMainThread_m54AC36A18A6EF0E73E512CFA96E102AE6F571961,
};
static const int32_t s_InvokerIndices[108] = 
{
	995,
	23,
	3,
	23,
	23,
	89,
	23,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	0,
	-1,
	0,
	0,
	-1,
	168,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	89,
	31,
	23,
	26,
	26,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	1,
	1,
	-1,
	1,
	1,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	23,
	23,
	23,
	23,
	-1,
	-1,
	4,
	168,
	49,
	23,
	23,
	23,
	23,
	3,
	3,
	115,
	178,
	4,
	168,
	49,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x02000006, { 10, 1 } },
	{ 0x02000008, { 11, 2 } },
	{ 0x02000009, { 13, 5 } },
	{ 0x0200000D, { 20, 2 } },
	{ 0x02000013, { 22, 3 } },
	{ 0x02000016, { 25, 1 } },
	{ 0x06000013, { 0, 5 } },
	{ 0x06000016, { 5, 5 } },
	{ 0x06000030, { 18, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[26] = 
{
	{ (Il2CppRGCTXDataType)2, 63617 },
	{ (Il2CppRGCTXDataType)3, 62359 },
	{ (Il2CppRGCTXDataType)2, 52225 },
	{ (Il2CppRGCTXDataType)3, 62360 },
	{ (Il2CppRGCTXDataType)3, 62361 },
	{ (Il2CppRGCTXDataType)2, 63618 },
	{ (Il2CppRGCTXDataType)3, 62362 },
	{ (Il2CppRGCTXDataType)2, 52227 },
	{ (Il2CppRGCTXDataType)3, 62363 },
	{ (Il2CppRGCTXDataType)3, 62364 },
	{ (Il2CppRGCTXDataType)3, 62365 },
	{ (Il2CppRGCTXDataType)2, 63619 },
	{ (Il2CppRGCTXDataType)3, 62366 },
	{ (Il2CppRGCTXDataType)3, 62367 },
	{ (Il2CppRGCTXDataType)2, 63620 },
	{ (Il2CppRGCTXDataType)3, 62368 },
	{ (Il2CppRGCTXDataType)3, 62369 },
	{ (Il2CppRGCTXDataType)2, 52243 },
	{ (Il2CppRGCTXDataType)2, 63621 },
	{ (Il2CppRGCTXDataType)3, 62370 },
	{ (Il2CppRGCTXDataType)2, 52257 },
	{ (Il2CppRGCTXDataType)3, 62371 },
	{ (Il2CppRGCTXDataType)2, 63622 },
	{ (Il2CppRGCTXDataType)3, 62372 },
	{ (Il2CppRGCTXDataType)3, 62373 },
	{ (Il2CppRGCTXDataType)3, 62374 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_AsyncCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_AsyncCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Async.dll",
	108,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	26,
	s_rgctxValues,
	NULL,
};
