﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean Klak.Spout.PluginEntry::get_IsAvailable()
extern void PluginEntry_get_IsAvailable_m03A7ABCA120E1B69BCCF7C9BD2367CD4859BC313 (void);
// 0x00000002 System.IntPtr Klak.Spout.PluginEntry::GetRenderEventFunc()
extern void PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E (void);
// 0x00000003 System.IntPtr Klak.Spout.PluginEntry::CreateSender(System.String,System.Int32,System.Int32)
extern void PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902 (void);
// 0x00000004 System.IntPtr Klak.Spout.PluginEntry::CreateReceiver(System.String)
extern void PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664 (void);
// 0x00000005 System.IntPtr Klak.Spout.PluginEntry::GetTexturePointer(System.IntPtr)
extern void PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1 (void);
// 0x00000006 System.Int32 Klak.Spout.PluginEntry::GetTextureWidth(System.IntPtr)
extern void PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF (void);
// 0x00000007 System.Int32 Klak.Spout.PluginEntry::GetTextureHeight(System.IntPtr)
extern void PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB (void);
// 0x00000008 System.Boolean Klak.Spout.PluginEntry::CheckValid(System.IntPtr)
extern void PluginEntry_CheckValid_m96F923E00F14F4F21379099E037DB35880C99F80 (void);
// 0x00000009 System.Int32 Klak.Spout.PluginEntry::ScanSharedObjects()
extern void PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2 (void);
// 0x0000000A System.String Klak.Spout.PluginEntry::GetSharedObjectNameString(System.Int32)
extern void PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF (void);
// 0x0000000B System.Void Klak.Spout.Util::Destroy(UnityEngine.Object)
extern void Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD (void);
// 0x0000000C System.Void Klak.Spout.Util::IssuePluginEvent(Klak.Spout.PluginEntry/Event,System.IntPtr)
extern void Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC (void);
// 0x0000000D System.String[] Klak.Spout.SpoutManager::GetSourceNames()
extern void SpoutManager_GetSourceNames_mBA1F5E29DB301943567FABBD29CC2E85354A6B6F (void);
// 0x0000000E System.Void Klak.Spout.SpoutManager::GetSourceNames(System.Collections.Generic.ICollection`1<System.String>)
extern void SpoutManager_GetSourceNames_mEB27875BBB4D5FFB58F883AD4BEBE975654F46D5 (void);
// 0x0000000F System.String Klak.Spout.SpoutReceiver::get_sourceName()
extern void SpoutReceiver_get_sourceName_m1BA50606AE68DEBB867C68B938639509A6720729 (void);
// 0x00000010 System.Void Klak.Spout.SpoutReceiver::set_sourceName(System.String)
extern void SpoutReceiver_set_sourceName_mACA6329C485466980A2B9E9524D940921145B99D (void);
// 0x00000011 UnityEngine.RenderTexture Klak.Spout.SpoutReceiver::get_targetTexture()
extern void SpoutReceiver_get_targetTexture_mFA6616A03DCF8820EBAE24CE91CA95052EA0B1C4 (void);
// 0x00000012 System.Void Klak.Spout.SpoutReceiver::set_targetTexture(UnityEngine.RenderTexture)
extern void SpoutReceiver_set_targetTexture_mB90906C0BE984C0BE981721C8E997CF723C0D79B (void);
// 0x00000013 UnityEngine.Renderer Klak.Spout.SpoutReceiver::get_targetRenderer()
extern void SpoutReceiver_get_targetRenderer_m640E0F5A72E8098FC77DCCFFCD1C2322A305FA22 (void);
// 0x00000014 System.Void Klak.Spout.SpoutReceiver::set_targetRenderer(UnityEngine.Renderer)
extern void SpoutReceiver_set_targetRenderer_m78C26EFC1CABFD752690836360E36C691D12CC3E (void);
// 0x00000015 System.String Klak.Spout.SpoutReceiver::get_targetMaterialProperty()
extern void SpoutReceiver_get_targetMaterialProperty_mBC272356EE58983B487ABC6865832690458286A9 (void);
// 0x00000016 System.Void Klak.Spout.SpoutReceiver::set_targetMaterialProperty(System.String)
extern void SpoutReceiver_set_targetMaterialProperty_m43FA8B94659F4408DDC08FB7386C9D3F9276E288 (void);
// 0x00000017 UnityEngine.Texture Klak.Spout.SpoutReceiver::get_receivedTexture()
extern void SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488 (void);
// 0x00000018 UnityEngine.Material Klak.Spout.SpoutReceiver::get_blitMaterial()
extern void SpoutReceiver_get_blitMaterial_m8F23BC2B3AD861CD9BE943B9986A3AD790BB81BE (void);
// 0x00000019 System.Void Klak.Spout.SpoutReceiver::set_blitMaterial(UnityEngine.Material)
extern void SpoutReceiver_set_blitMaterial_mD801D9B60C14E4626DB14427D03E4B6645A482FB (void);
// 0x0000001A System.Void Klak.Spout.SpoutReceiver::RequestReconnect()
extern void SpoutReceiver_RequestReconnect_mF1B1915CCDBEFB2DC2DDC4D96608EA926986F0C1 (void);
// 0x0000001B System.Void Klak.Spout.SpoutReceiver::OnDisable()
extern void SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8 (void);
// 0x0000001C System.Void Klak.Spout.SpoutReceiver::OnDestroy()
extern void SpoutReceiver_OnDestroy_m0A6FB3058763E2E62980F215153F143BE2DFAB4C (void);
// 0x0000001D System.Void Klak.Spout.SpoutReceiver::Update()
extern void SpoutReceiver_Update_mEBF46B4FD09CECE23A717F96322B1B1844A09A43 (void);
// 0x0000001E System.Void Klak.Spout.SpoutReceiver::.ctor()
extern void SpoutReceiver__ctor_m13B4DA3C9835B8B09B0CCDE94997813906D8D696 (void);
// 0x0000001F UnityEngine.RenderTexture Klak.Spout.SpoutSender::get_sourceTexture()
extern void SpoutSender_get_sourceTexture_m56736F05526304452202E5A2BFFE98B53F77FD60 (void);
// 0x00000020 System.Void Klak.Spout.SpoutSender::set_sourceTexture(UnityEngine.RenderTexture)
extern void SpoutSender_set_sourceTexture_m9B27154F9B1132CCD233D59EA1FF89E5AE45B703 (void);
// 0x00000021 System.Boolean Klak.Spout.SpoutSender::get_alphaSupport()
extern void SpoutSender_get_alphaSupport_mDEE51AE2D5599F42FE05B3E378DB310CF501BF3B (void);
// 0x00000022 System.Void Klak.Spout.SpoutSender::set_alphaSupport(System.Boolean)
extern void SpoutSender_set_alphaSupport_mE5A0A876DB73B29746016D56DA3912CD919A5134 (void);
// 0x00000023 System.Void Klak.Spout.SpoutSender::SendRenderTexture(UnityEngine.RenderTexture)
extern void SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D (void);
// 0x00000024 System.Void Klak.Spout.SpoutSender::OnDisable()
extern void SpoutSender_OnDisable_mA68599D3A9C29D76566081450DF5F16CFEB17575 (void);
// 0x00000025 System.Void Klak.Spout.SpoutSender::OnDestroy()
extern void SpoutSender_OnDestroy_m2DA609828A74433E07D76E33EFD8645C120F5762 (void);
// 0x00000026 System.Void Klak.Spout.SpoutSender::Update()
extern void SpoutSender_Update_mF440263DB95F7A38D4141F89203E0D72036876CB (void);
// 0x00000027 System.Void Klak.Spout.SpoutSender::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void SpoutSender_OnRenderImage_m22518EF3DF63D7D46B3F51DE4FA606D1C97DEA1E (void);
// 0x00000028 System.Void Klak.Spout.SpoutSender::.ctor()
extern void SpoutSender__ctor_mA22A9CEE5AB1261B210B426BB9116652054E55D9 (void);
static Il2CppMethodPointer s_methodPointers[40] = 
{
	PluginEntry_get_IsAvailable_m03A7ABCA120E1B69BCCF7C9BD2367CD4859BC313,
	PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E,
	PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902,
	PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664,
	PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1,
	PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF,
	PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB,
	PluginEntry_CheckValid_m96F923E00F14F4F21379099E037DB35880C99F80,
	PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2,
	PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF,
	Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD,
	Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC,
	SpoutManager_GetSourceNames_mBA1F5E29DB301943567FABBD29CC2E85354A6B6F,
	SpoutManager_GetSourceNames_mEB27875BBB4D5FFB58F883AD4BEBE975654F46D5,
	SpoutReceiver_get_sourceName_m1BA50606AE68DEBB867C68B938639509A6720729,
	SpoutReceiver_set_sourceName_mACA6329C485466980A2B9E9524D940921145B99D,
	SpoutReceiver_get_targetTexture_mFA6616A03DCF8820EBAE24CE91CA95052EA0B1C4,
	SpoutReceiver_set_targetTexture_mB90906C0BE984C0BE981721C8E997CF723C0D79B,
	SpoutReceiver_get_targetRenderer_m640E0F5A72E8098FC77DCCFFCD1C2322A305FA22,
	SpoutReceiver_set_targetRenderer_m78C26EFC1CABFD752690836360E36C691D12CC3E,
	SpoutReceiver_get_targetMaterialProperty_mBC272356EE58983B487ABC6865832690458286A9,
	SpoutReceiver_set_targetMaterialProperty_m43FA8B94659F4408DDC08FB7386C9D3F9276E288,
	SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488,
	SpoutReceiver_get_blitMaterial_m8F23BC2B3AD861CD9BE943B9986A3AD790BB81BE,
	SpoutReceiver_set_blitMaterial_mD801D9B60C14E4626DB14427D03E4B6645A482FB,
	SpoutReceiver_RequestReconnect_mF1B1915CCDBEFB2DC2DDC4D96608EA926986F0C1,
	SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8,
	SpoutReceiver_OnDestroy_m0A6FB3058763E2E62980F215153F143BE2DFAB4C,
	SpoutReceiver_Update_mEBF46B4FD09CECE23A717F96322B1B1844A09A43,
	SpoutReceiver__ctor_m13B4DA3C9835B8B09B0CCDE94997813906D8D696,
	SpoutSender_get_sourceTexture_m56736F05526304452202E5A2BFFE98B53F77FD60,
	SpoutSender_set_sourceTexture_m9B27154F9B1132CCD233D59EA1FF89E5AE45B703,
	SpoutSender_get_alphaSupport_mDEE51AE2D5599F42FE05B3E378DB310CF501BF3B,
	SpoutSender_set_alphaSupport_mE5A0A876DB73B29746016D56DA3912CD919A5134,
	SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D,
	SpoutSender_OnDisable_mA68599D3A9C29D76566081450DF5F16CFEB17575,
	SpoutSender_OnDestroy_m2DA609828A74433E07D76E33EFD8645C120F5762,
	SpoutSender_Update_mF440263DB95F7A38D4141F89203E0D72036876CB,
	SpoutSender_OnRenderImage_m22518EF3DF63D7D46B3F51DE4FA606D1C97DEA1E,
	SpoutSender__ctor_mA22A9CEE5AB1261B210B426BB9116652054E55D9,
};
static const int32_t s_InvokerIndices[40] = 
{
	49,
	767,
	1682,
	24,
	1007,
	169,
	169,
	589,
	115,
	43,
	168,
	1829,
	4,
	168,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	14,
	26,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	89,
	31,
	26,
	23,
	23,
	23,
	27,
	23,
};
extern const Il2CppCodeGenModule g_Klak_SpoutCodeGenModule;
const Il2CppCodeGenModule g_Klak_SpoutCodeGenModule = 
{
	"Klak.Spout.dll",
	40,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
