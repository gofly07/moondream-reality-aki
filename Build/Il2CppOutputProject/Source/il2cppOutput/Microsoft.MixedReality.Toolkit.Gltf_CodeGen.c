﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::get_Model()
extern void GltfAsset_get_Model_mC4DDD203AA774C00AFAD0309553C8CB012DF2019 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::set_Model(UnityEngine.GameObject)
extern void GltfAsset_set_Model_m2D4BD80878BCF1A9192F98BAF5775523E23F9E55 (void);
// 0x00000003 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::get_GltfObject()
extern void GltfAsset_get_GltfObject_mE5B97605A4371AABA76F6F1D010F67D27057E29E (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::set_GltfObject(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void GltfAsset_set_GltfObject_mD87951AAB5124005D9F59339639CA4AEB5CAB837 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.GltfAsset::.ctor()
extern void GltfAsset__ctor_m8891E2232FA64DE019096CE90109898806D86B8D (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::Construct(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void ConstructGltf_Construct_m5B3AEF104ED2D31DDBEA7DD4B9D0D6ADBEE05F01 (void);
// 0x00000007 System.Threading.Tasks.Task`1<UnityEngine.GameObject> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject)
extern void ConstructGltf_ConstructAsync_m7D8DED4997955B3C8E1270715D7566C1D23D9C0E (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructBufferView(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView)
extern void ConstructGltf_ConstructBufferView_mCEFEFF8F9D7EC38016D113FF643470E5F20DC1CB (void);
// 0x00000009 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructTextureAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture)
extern void ConstructGltf_ConstructTextureAsync_m7F928F2941F7C799FDEC6252A3AAC293FEF8C597 (void);
// 0x0000000A System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMaterialAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_ConstructMaterialAsync_m9C09E0F710CCCA9DFB0BFB1E764F8B5E1766FB11 (void);
// 0x0000000B System.Threading.Tasks.Task`1<UnityEngine.Material> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateMRTKShaderMaterial(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_CreateMRTKShaderMaterial_m670DA79EAC79B97EBB9A3609EA312574A78A9AA6 (void);
// 0x0000000C System.Threading.Tasks.Task`1<UnityEngine.Material> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateStandardShaderMaterial(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial,System.Int32)
extern void ConstructGltf_CreateStandardShaderMaterial_mB2052CF02D269739588A8FAE957688217A216CE5 (void);
// 0x0000000D System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructSceneAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfScene,UnityEngine.GameObject)
extern void ConstructGltf_ConstructSceneAsync_mE14D906EEB50971535285DBFC472F4CA05A7272B (void);
// 0x0000000E System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructNodeAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode,System.Int32,UnityEngine.Transform)
extern void ConstructGltf_ConstructNodeAsync_mEE0FE079384A22AD6DE819B1EAE9EEE9D77BD8D2 (void);
// 0x0000000F System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMeshAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,UnityEngine.GameObject,System.Int32)
extern void ConstructGltf_ConstructMeshAsync_m9589F63537AD13669E854E702AF9B974CE4A89A6 (void);
// 0x00000010 System.Threading.Tasks.Task`1<UnityEngine.Mesh> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::ConstructMeshPrimitiveAsync(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive)
extern void ConstructGltf_ConstructMeshPrimitiveAsync_mDD0A27D33D9F8DF508061D060585DA67A69C9B63 (void);
// 0x00000011 UnityEngine.BoneWeight[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::CreateBoneWeightArray(UnityEngine.Vector4[],UnityEngine.Vector4[],System.Int32)
extern void ConstructGltf_CreateBoneWeightArray_mD8E3F32D75E2F6CF7A0C85B85C19DC45A827624E (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::NormalizeBoneWeightArray(UnityEngine.Vector4[])
extern void ConstructGltf_NormalizeBoneWeightArray_mD05E0095E67C0833619E2230AEBF38249CCB0EBE (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf::.cctor()
extern void ConstructGltf__cctor_m18E7D288776076BB92C307FDC260644213AC5ECB (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<Construct>d__18::MoveNext()
extern void U3CConstructU3Ed__18_MoveNext_mE503DAF403D164611C146970C1D4CFFE5D1470C2 (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<Construct>d__18::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructU3Ed__18_SetStateMachine_m605C512A909E6395AB7757F6771A6135D24F6748 (void);
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructAsync>d__19::MoveNext()
extern void U3CConstructAsyncU3Ed__19_MoveNext_mC3CE4BCA1A662C1291DE6AAF12DDEB7B35370F37 (void);
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructAsync>d__19::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructAsyncU3Ed__19_SetStateMachine_m9338CF450CCCC5EB8383006804B2A9B621F98DE0 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructTextureAsync>d__21::MoveNext()
extern void U3CConstructTextureAsyncU3Ed__21_MoveNext_m87D7E5DA135F432F4EB478D589DFD4922B59E3C0 (void);
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructTextureAsync>d__21::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructTextureAsyncU3Ed__21_SetStateMachine_m924F856DDEE0A4645E0EE4CD224BD6B91D9D16AE (void);
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMaterialAsync>d__22::MoveNext()
extern void U3CConstructMaterialAsyncU3Ed__22_MoveNext_m80B465BDB7E9F5781750AFF4AF41452D72AFD795 (void);
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMaterialAsync>d__22::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m7EDB98E424527E860064E6F93BF30AB2CACB56D2 (void);
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateMRTKShaderMaterial>d__23::MoveNext()
extern void U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mBA59B13C04DFFE441058520303F3F9AA864A2832 (void);
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateMRTKShaderMaterial>d__23::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m6CEFEDDCE60E12D51A332EBDD9C709E93BCDA729 (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateStandardShaderMaterial>d__24::MoveNext()
extern void U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m5C49AC6B31D83B4523416DF2BE92DC5EC0BD4EF4 (void);
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<CreateStandardShaderMaterial>d__24::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mD4AA2B01181C8BE31F7DD26BEBFA2C44C2BEC2C6 (void);
// 0x00000020 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructSceneAsync>d__25::MoveNext()
extern void U3CConstructSceneAsyncU3Ed__25_MoveNext_m1A9CECE2140E4D31FAF63E72DFB5D2A6FA56481B (void);
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructSceneAsync>d__25::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructSceneAsyncU3Ed__25_SetStateMachine_m53205A9F9AE1223DE7AD03A367FAD4794904406B (void);
// 0x00000022 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructNodeAsync>d__26::MoveNext()
extern void U3CConstructNodeAsyncU3Ed__26_MoveNext_m81F42582D2EE39E5D7D35B7E822A103F527D78CE (void);
// 0x00000023 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructNodeAsync>d__26::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m4214DB18E4A84F811B66F2A3E1C2412EF452569E (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshAsync>d__27::MoveNext()
extern void U3CConstructMeshAsyncU3Ed__27_MoveNext_m90AAD4825B99652BBEB9C0ABFB92B2E55817E977 (void);
// 0x00000025 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshAsync>d__27::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMeshAsyncU3Ed__27_SetStateMachine_m74F8878283419AD77C747655139E5D5A79D84EFC (void);
// 0x00000026 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshPrimitiveAsync>d__28::MoveNext()
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m67EE15B8890FB05E7731747B81EE3A0BC79A17E4 (void);
// 0x00000027 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.ConstructGltf/<ConstructMeshPrimitiveAsync>d__28::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m31A9C930B5EC4DE05D89E61B103FAE6484875285 (void);
// 0x00000028 UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTrsProperties(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void GltfConversions_GetTrsProperties_mCAB318A57E890B44AB3A02464C9218573764C168 (void);
// 0x00000029 UnityEngine.Color Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetColorValue(System.Single[])
extern void GltfConversions_GetColorValue_mE364D536A258EBD0B22F32EEB6810A1BEC7C3724 (void);
// 0x0000002A System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetColorValue(UnityEngine.Color)
extern void GltfConversions_SetColorValue_m84F382E8520F07EBFE42CEFED8F3F1F00A0C02D1 (void);
// 0x0000002B UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector2Value(System.Single[])
extern void GltfConversions_GetVector2Value_m84288616D4A7A4912CF1CC65CEE9340447F6F0E9 (void);
// 0x0000002C System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetVector2Value(UnityEngine.Vector2)
extern void GltfConversions_SetVector2Value_m6C9E2FC060DF5E136A65386D99577FE650021B2D (void);
// 0x0000002D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector3Value(System.Single[],System.Boolean)
extern void GltfConversions_GetVector3Value_m609322FA4E8B41F53DE9B2750E13B8E5CD06736D (void);
// 0x0000002E System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetVector3Value(UnityEngine.Vector3,System.Boolean)
extern void GltfConversions_SetVector3Value_mEBD2A89D8DA5A04C93F06EC79E097CA88EAE99C7 (void);
// 0x0000002F UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetQuaternionValue(System.Single[],System.Boolean)
extern void GltfConversions_GetQuaternionValue_m67C13D9B6CE3617603D259380E44B5FC53591B49 (void);
// 0x00000030 System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetQuaternionValue(UnityEngine.Quaternion,System.Boolean)
extern void GltfConversions_SetQuaternionValue_mA66B7AC7A8E3C14081FF40B393FC3060EAC8C755 (void);
// 0x00000031 UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetMatrix4X4Value(System.Double[])
extern void GltfConversions_GetMatrix4X4Value_m7999A957D1BE22C3CEF38505814E551C7C8FE1C0 (void);
// 0x00000032 System.Single[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::SetMatrix4X4Value(UnityEngine.Matrix4x4)
extern void GltfConversions_SetMatrix4X4Value_m0D2DA8652B377B81D7356FB4DA1F67954170959B (void);
// 0x00000033 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTrsProperties(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void GltfConversions_GetTrsProperties_mED3580C4F5D67CDCF8B9C2F47A079EA84F1B2238 (void);
// 0x00000034 System.Int32[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetIntArray(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetIntArray_m9614119496482521C3CEE849688E71D224F0EFB4 (void);
// 0x00000035 UnityEngine.Vector2[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector2Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector2Array_m4F6DEB3E1B5DFF4255160F69C94600070465B686 (void);
// 0x00000036 UnityEngine.Vector3[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector3Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector3Array_mAB36A58794ED6CA09B263B207CF6C0798316D01A (void);
// 0x00000037 UnityEngine.Vector4[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetVector4Array(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor,System.Boolean)
extern void GltfConversions_GetVector4Array_m701755C81CA63D1F9D0D39D450CBC3993EF55130 (void);
// 0x00000038 UnityEngine.Color[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetColorArray(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor)
extern void GltfConversions_GetColorArray_m1FA850D9DC93F757E12B8E4B9DD198F606C60831 (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetTypeDetails(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType,System.Int32&,System.Single&)
extern void GltfConversions_GetTypeDetails_m18C98C77B7EBF5B105955A8C997B359EFEFCF7B4 (void);
// 0x0000003A System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetDiscreteElement(System.Byte[],System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfConversions_GetDiscreteElement_m0CB9BAA04E87E05DDE8BFC525092E518156ABDD3 (void);
// 0x0000003B System.UInt32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::GetDiscreteUnsignedElement(System.Byte[],System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfConversions_GetDiscreteUnsignedElement_m10789E88E9DBAA37870BFBFA9B7D383E4BC17797 (void);
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfConversions::.cctor()
extern void GltfConversions__cctor_m2AA55059FCFFFFC0CE8878447918C2F5027F41D5 (void);
// 0x0000003D System.Threading.Tasks.Task`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::ImportGltfObjectFromPathAsync(System.String)
extern void GltfUtility_ImportGltfObjectFromPathAsync_m96B0EFC1C5681224666D0960D1F2966E77DE2471 (void);
// 0x0000003E Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfObjectFromJson(System.String)
extern void GltfUtility_GetGltfObjectFromJson_m3665515DB5BF7E0AA99B9D858877EB9F935C1598 (void);
// 0x0000003F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfObjectFromGlb(System.Byte[])
extern void GltfUtility_GetGltfObjectFromGlb_m60966266F8AED6ACF3940226500645AB4C6EB87A (void);
// 0x00000040 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetJsonObject(System.String,System.String)
extern void GltfUtility_GetJsonObject_mC92D10D6FD707A5536238220D86241B831C585D1 (void);
// 0x00000041 System.Collections.Generic.List`1<System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfMeshPrimitiveAttributes(System.String)
extern void GltfUtility_GetGltfMeshPrimitiveAttributes_m97A563ED0CC82A99E1DD0905F5746CB49C52E719 (void);
// 0x00000042 System.Collections.Generic.List`1<System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfMeshPrimitiveAttributes(System.String,System.Text.RegularExpressions.Regex)
extern void GltfUtility_GetGltfMeshPrimitiveAttributes_mC5EEE7594FE78EB02AED5A50DAE7A058C49780BC (void);
// 0x00000043 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtensionObjects(System.String,System.String)
extern void GltfUtility_GetGltfExtensionObjects_m28153ABEF97016E8FCD8D44CCDEC6FBCC1C9FFBF (void);
// 0x00000044 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtraObjects(System.String,System.String)
extern void GltfUtility_GetGltfExtraObjects_m718DDBF41C65B9630B6EC752B9D4202F01C65466 (void);
// 0x00000045 System.Collections.Generic.Dictionary`2<System.String,System.String> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfExtensions(System.String,System.Text.RegularExpressions.Regex)
extern void GltfUtility_GetGltfExtensions_m93DD36CB1B67E39C8891B6C2D52CF7F84D929E7B (void);
// 0x00000046 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetJsonObject(System.String,System.Int32)
extern void GltfUtility_GetJsonObject_m6DA239E6A471AFF59C81BF80A1B57E09C3CFD5D6 (void);
// 0x00000047 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::GetGltfNodeName(System.String)
extern void GltfUtility_GetGltfNodeName_m77A1786AB9B5D7845E2AACE5DBF7CA8D72666E1D (void);
// 0x00000048 System.Collections.Generic.Dictionary`2<System.String,System.Int32> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::StringIntDictionaryFromJson(System.String)
extern void GltfUtility_StringIntDictionaryFromJson_m31EC1C06884CA0EA7A87B1B1FA827255BB924398 (void);
// 0x00000049 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::JsonDictionaryToArray(System.String)
extern void GltfUtility_JsonDictionaryToArray_m1469EDC451D697763B178167016056E5F5F7FCA5 (void);
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility::.cctor()
extern void GltfUtility__cctor_mD3C5B3F6EB17F366D3A6F2BD707A7CC0BA0E3812 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/StringKeyValue::.ctor()
extern void StringKeyValue__ctor_m52539F4A5551A4ED044941053539FB31536B910E (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/StringIntKeyValueArray::.ctor()
extern void StringIntKeyValueArray__ctor_m5913999BD5CAA024698F1A8F6076F7CC4AF77909 (void);
// 0x0000004D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/<ImportGltfObjectFromPathAsync>d__4::MoveNext()
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_m2661330414A4711134EC23DD76083108C714E06A (void);
// 0x0000004E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Serialization.GltfUtility/<ImportGltfObjectFromPathAsync>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m513F78F2BB7D20D852D5E7A3069C0D80D8EAF5A2 (void);
// 0x0000004F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::get_ComponentType()
extern void GltfAccessor_get_ComponentType_m12EB992119212899C92A281AC9EEC508DB542D21 (void);
// 0x00000050 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::set_ComponentType(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfAccessor_set_ComponentType_mEE91E5CE0C2B5C58678FB93D80C1DB37F1B071E8 (void);
// 0x00000051 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::get_BufferView()
extern void GltfAccessor_get_BufferView_m639F63CC808C1E3E0D2B9A81ADC2146370BA936B (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::set_BufferView(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView)
extern void GltfAccessor_set_BufferView_m20284A6CE969BA1A6855C8273EDEA2B3D5F596CA (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfAccessor_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2CC95F12361A397545C85EEB92FCF161430FE3C9 (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfAccessor_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDD3CCC7E4DFFF5DE4E18F8382F1E33E6AE917AC7 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor::.ctor()
extern void GltfAccessor__ctor_mF54C929003442995BBEB3978DC1CC9B0897EDDF3 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparse::.ctor()
extern void GltfAccessorSparse__ctor_m6DC3BE5C003EE4B1DBD0744D486CC1B4AA077144 (void);
// 0x00000057 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::get_ComponentType()
extern void GltfAccessorSparseIndices_get_ComponentType_mBD691A86F63A15C8DA64AD07616BD62642290DFC (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::set_ComponentType(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfComponentType)
extern void GltfAccessorSparseIndices_set_ComponentType_mB8AAB0BE602894A6E64ACD51C39A90F887C19D50 (void);
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfAccessorSparseIndices_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8F33CC9C6585B811D2784D6D219DF61BDE916D0D (void);
// 0x0000005A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfAccessorSparseIndices_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m18C5ADA34DD6C383C7EE1CA8D1B0F1D3658629AB (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseIndices::.ctor()
extern void GltfAccessorSparseIndices__ctor_m20B1261D7424B791AFE51B36BB8C5D5D28CD4084 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessorSparseValues::.ctor()
extern void GltfAccessorSparseValues__ctor_m33FB61FA3523FCEDE8C3AFFC4FBAA7C510E9D4E3 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimation::.ctor()
extern void GltfAnimation__ctor_mFEEF1FCDA26FF29EA5EE35BF03645C5E691ECB0D (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannel::.ctor()
extern void GltfAnimationChannel__ctor_m0922346D004007EAAD2E6F7C78F0A88405A5A604 (void);
// 0x0000005F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelPath Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::get_Path()
extern void GltfAnimationChannelTarget_get_Path_mE4CDC1D8E1356685D1CFBCFAD43F088891628366 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::set_Path(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelPath)
extern void GltfAnimationChannelTarget_set_Path_m99C5ECF32F9C386E74C349EC2086E840BA6C05E7 (void);
// 0x00000061 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfAnimationChannelTarget_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA7CFD42677D3FBC06A33A433C9AE6B7489C878DA (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfAnimationChannelTarget_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m0D879D7AC512F99CCBED877399725E62283DF30C (void);
// 0x00000063 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationChannelTarget::.ctor()
extern void GltfAnimationChannelTarget__ctor_mD090B9B181C6B3369AAE57E23473019BE9C96C0D (void);
// 0x00000064 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfInterpolationType Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::get_Interpolation()
extern void GltfAnimationSampler_get_Interpolation_m2E5ABD45ADA812436BEC080CC6C11AC8917BE353 (void);
// 0x00000065 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::set_Interpolation(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfInterpolationType)
extern void GltfAnimationSampler_set_Interpolation_m6B9BEDF0C47ACAF25D005CDD58285D365294E827 (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfAnimationSampler_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m7BCF4DEACF1CB08650576F1661C77A3AEC014B2B (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfAnimationSampler_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m54AC4EBF482CB7B72E37AB659DEB76373761BF17 (void);
// 0x00000068 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAnimationSampler::.ctor()
extern void GltfAnimationSampler__ctor_mCB23A17AF0BE7B430E3797C882BC3E2DD2B5EA81 (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAssetInfo::.ctor()
extern void GltfAssetInfo__ctor_m09835D55492B2C559096424DE4B5B05FAFF0139F (void);
// 0x0000006A System.Byte[] Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::get_BufferData()
extern void GltfBuffer_get_BufferData_m94C414A5C3AACDC40A6E70046136244A599C7203 (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::set_BufferData(System.Byte[])
extern void GltfBuffer_set_BufferData_mD84BD7FAA98E530FE050E11C7FB11196FDD5566D (void);
// 0x0000006C System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer::.ctor()
extern void GltfBuffer__ctor_m357B7EE058144B357DB561CA66D1E3D2F99AF59A (void);
// 0x0000006D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferViewTarget Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::get_Target()
extern void GltfBufferView_get_Target_mFCBD2F2DBE166210E82309FB905942011AB1F99D (void);
// 0x0000006E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::set_Target(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferViewTarget)
extern void GltfBufferView_set_Target_m1CBA16E6429926707B6BC7265C2B166044CA14F4 (void);
// 0x0000006F Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::get_Buffer()
extern void GltfBufferView_get_Buffer_mBEAB185FC665306121E81F91E419A4199AEBE542 (void);
// 0x00000070 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::set_Buffer(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBuffer)
extern void GltfBufferView_set_Buffer_m197BA2D950CBB1520FFC904344370F85B83833C7 (void);
// 0x00000071 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfBufferView_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mF7B727EB4E24BF9F4CB3C0086928A8BD8DFE80D8 (void);
// 0x00000072 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfBufferView_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mAD02ABE414AA157DAC026A9DFD92D7A1B5D78B40 (void);
// 0x00000073 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfBufferView::.ctor()
extern void GltfBufferView__ctor_m8014047B477FA5B076626E3B791CFB4414186F67 (void);
// 0x00000074 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraType Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::get_Type()
extern void GltfCamera_get_Type_mE5557535832093736D378168EDFBF91B8A853FDB (void);
// 0x00000075 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::set_Type(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraType)
extern void GltfCamera_set_Type_m7F7B008431CC421D01FB23229BDBD7BF3ACF81B4 (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfCamera_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m7B436028160D7DDE3FD04711141A78C78CED9F36 (void);
// 0x00000077 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfCamera_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4525FA98BAF462AFF58DFC1735A6E0DD72D556B8 (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCamera::.ctor()
extern void GltfCamera__ctor_mC3F2AC360DF306B0679AF3AE726F20B5AE9B99F1 (void);
// 0x00000079 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraOrthographic::.ctor()
extern void GltfCameraOrthographic__ctor_m3237FBAAFDD8287756E18E904C2CD9E0AB38480C (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfCameraPerspective::.ctor()
extern void GltfCameraPerspective__ctor_mA65F5936BC56E102C6BCA4FB1E9F9F3EC58C8203 (void);
// 0x0000007B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfChildOfRootProperty::.ctor()
extern void GltfChildOfRootProperty__ctor_m7B05817F21462220CD6605F7C37ACFB4E036C8D5 (void);
// 0x0000007C UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::get_Texture()
extern void GltfImage_get_Texture_m1DCC77F66B200F22E537B14689BF81DCF563F6D2 (void);
// 0x0000007D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::set_Texture(UnityEngine.Texture2D)
extern void GltfImage_set_Texture_mB861C9EC595DCC97B814CAD831F68CC1F77AAE57 (void);
// 0x0000007E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfImage::.ctor()
extern void GltfImage__ctor_mC4A61788CD4802B5CBB0EF0C4F9D68DB7F58947C (void);
// 0x0000007F UnityEngine.Material Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::get_Material()
extern void GltfMaterial_get_Material_mE46692023D0FBBC7CFC97FAFF2F1BE75ADD62569 (void);
// 0x00000080 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::set_Material(UnityEngine.Material)
extern void GltfMaterial_set_Material_m9484345E96A6B9B9E25DB50280E748B970C2DA31 (void);
// 0x00000081 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterial::.ctor()
extern void GltfMaterial__ctor_m0A21AA00FB6BE40059B21ECAA011DCB7950D612A (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMaterialCommonConstant::.ctor()
extern void GltfMaterialCommonConstant__ctor_m29F574B6B035403B9E0856A22968C89B968ABF16 (void);
// 0x00000083 UnityEngine.Mesh Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::get_Mesh()
extern void GltfMesh_get_Mesh_m002F9278D0C13783F4BD28C982E2AD0A7DF75306 (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::set_Mesh(UnityEngine.Mesh)
extern void GltfMesh_set_Mesh_mF45E21ADEF2BE4EA36B249EBD08F92D2D6891056 (void);
// 0x00000085 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMesh::.ctor()
extern void GltfMesh__ctor_m5D086289E80762A94ABC6358136F494CA93A2291 (void);
// 0x00000086 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfDrawMode Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_Mode()
extern void GltfMeshPrimitive_get_Mode_m4458F8C3DECBA5D599C5D2B03887A25EF21FF42B (void);
// 0x00000087 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_Mode(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfDrawMode)
extern void GltfMeshPrimitive_set_Mode_mBE4525832D69B275AA71800E224929D8276FDEF4 (void);
// 0x00000088 System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Int32>> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_Targets()
extern void GltfMeshPrimitive_get_Targets_m57C26C5674BBC73D791B216179B9E4A96F0540FB (void);
// 0x00000089 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_Targets(System.Collections.Generic.List`1<System.Collections.Generic.Dictionary`2<System.String,System.Int32>>)
extern void GltfMeshPrimitive_set_Targets_m50470E531DAD5365BDFA978B7D30BC78302CF1D5 (void);
// 0x0000008A Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_Attributes()
extern void GltfMeshPrimitive_get_Attributes_mB9B5BD70D80F43037C5B59B8C5A884C9C804CD77 (void);
// 0x0000008B System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_Attributes(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes)
extern void GltfMeshPrimitive_set_Attributes_mBD7D7864F65958C9758A3B0ACAEAA1EA8ADC9F83 (void);
// 0x0000008C UnityEngine.Mesh Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::get_SubMesh()
extern void GltfMeshPrimitive_get_SubMesh_m3CD3B47543CC369131B18F32C51E599D6A21BBF6 (void);
// 0x0000008D System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::set_SubMesh(UnityEngine.Mesh)
extern void GltfMeshPrimitive_set_SubMesh_mF6C07B8E99656BBCC561E05E2EDBD205E4EA8A91 (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfMeshPrimitive_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m947D2DF08371AEED0E2A5EA07F64EA9E2A771097 (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfMeshPrimitive_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDC0565EB179E061237AAEAEF3F3E7E588972D5BB (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitive::.ctor()
extern void GltfMeshPrimitive__ctor_m818C1C3900EE0C5DA1A0032AED672CD806C9209C (void);
// 0x00000091 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::.ctor(System.Collections.Generic.IDictionary`2<System.String,System.Int32>)
extern void GltfMeshPrimitiveAttributes__ctor_m2764B2BFFA305A6319BB3CD2F5AE962C4279EE3B (void);
// 0x00000092 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::TryGetDefault(System.String,System.Int32)
extern void GltfMeshPrimitiveAttributes_TryGetDefault_mB7204F3C7CDB5271EE2BEEEF3D12F7911406AE32 (void);
// 0x00000093 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_POSITION()
extern void GltfMeshPrimitiveAttributes_get_POSITION_m417AF341DE4EE8A2A518E070909AE4E440C10B98 (void);
// 0x00000094 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_NORMAL()
extern void GltfMeshPrimitiveAttributes_get_NORMAL_m7A1171E093819D113E6F880FE6EB11FF053502AB (void);
// 0x00000095 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_0()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_0_mFFC6AFB41D315C3ACEE04EEB93CC0C89711B40AC (void);
// 0x00000096 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_1()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_1_m22CA53116D75144EE9238E843FFF54CA332E96DD (void);
// 0x00000097 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_2()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_2_m2693EAAE5314DB0CAA3D2F67804CD4DB5F1C6684 (void);
// 0x00000098 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TEXCOORD_3()
extern void GltfMeshPrimitiveAttributes_get_TEXCOORD_3_m793161ED606A80CF3D62626D07CC9EA9E72F5355 (void);
// 0x00000099 System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_COLOR_0()
extern void GltfMeshPrimitiveAttributes_get_COLOR_0_m6CC8C480A82169D43091D193075837C8C4A11BD0 (void);
// 0x0000009A System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_TANGENT()
extern void GltfMeshPrimitiveAttributes_get_TANGENT_mCD594DC29AEF335F07C8E64914C9192D39739DBC (void);
// 0x0000009B System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_WEIGHTS_0()
extern void GltfMeshPrimitiveAttributes_get_WEIGHTS_0_m4C460EDBAC3F510C336CEEDC6E340C579A136E2A (void);
// 0x0000009C System.Int32 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMeshPrimitiveAttributes::get_JOINTS_0()
extern void GltfMeshPrimitiveAttributes_get_JOINTS_0_mEB2C682D7C2E92D29D8FBDA5CB3D85B8CCF3ED6F (void);
// 0x0000009D UnityEngine.Matrix4x4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::get_Matrix()
extern void GltfNode_get_Matrix_m9A1C9B949F59B70BDA37BF412B90D3F8AAB73CA9 (void);
// 0x0000009E System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::set_Matrix(UnityEngine.Matrix4x4)
extern void GltfNode_set_Matrix_mA0DE507B238B4F3C44CA455EA7794A4E6AB2EF45 (void);
// 0x0000009F System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNode::.ctor()
extern void GltfNode__ctor_mE4BAEFD8AF83FAA40193350F20EC3E6CF59C5991 (void);
// 0x000000A0 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfNormalTextureInfo::.ctor()
extern void GltfNormalTextureInfo__ctor_mDF32F13A83B3AE33180040310D974944DAAB03B9 (void);
// 0x000000A1 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_Name()
extern void GltfObject_get_Name_mD989932FBB5D60FE94494EE52FF03F6ADD1C3E9B (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_Name(System.String)
extern void GltfObject_set_Name_m942ABD66F0EBF82C86F5067BED28DE48B6448A9B (void);
// 0x000000A3 System.String Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_Uri()
extern void GltfObject_get_Uri_m59A34ED39B3C8304F0C96323A6C347874DA83C64 (void);
// 0x000000A4 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_Uri(System.String)
extern void GltfObject_set_Uri_m508D9DDA0F16B0C91AC9F502FD8A5DFD04F2FD58 (void);
// 0x000000A5 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_GameObjectReference()
extern void GltfObject_get_GameObjectReference_m77BB72793CDAB811BEC7A4EA50DE4C3927AD233A (void);
// 0x000000A6 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_GameObjectReference(UnityEngine.GameObject)
extern void GltfObject_set_GameObjectReference_m7C857AF7FE86C15A3EC317F209221AE2395E714C (void);
// 0x000000A7 System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension> Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_RegisteredExtensions()
extern void GltfObject_get_RegisteredExtensions_m1152782A75C41E55854A8C807BD89270731839DA (void);
// 0x000000A8 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_RegisteredExtensions(System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension>)
extern void GltfObject_set_RegisteredExtensions_m55444DE0E0EDF8F3BA3C2F575E5C4C89EFCEB567 (void);
// 0x000000A9 System.Boolean Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::get_UseBackgroundThread()
extern void GltfObject_get_UseBackgroundThread_mB7AC0B9C4B71C896D5422C57D1B6DF797D055AB8 (void);
// 0x000000AA System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::set_UseBackgroundThread(System.Boolean)
extern void GltfObject_set_UseBackgroundThread_m967C65C78C343544366861CE3B91D2F2BEE997EE (void);
// 0x000000AB Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfAccessor Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::GetAccessor(System.Int32)
extern void GltfObject_GetAccessor_m09D9EF065161EA49A969485E55EE56F63EEE2AFD (void);
// 0x000000AC System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfObject::.ctor()
extern void GltfObject__ctor_m75AD55E97B58A4EA7EB2D19EAC71BF1BFA1E9CA8 (void);
// 0x000000AD System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfOcclusionTextureInfo::.ctor()
extern void GltfOcclusionTextureInfo__ctor_m3BBFF83325FEC18EBB4D81A6CA56002905C4BE9C (void);
// 0x000000AE System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfPbrMetallicRoughness::.ctor()
extern void GltfPbrMetallicRoughness__ctor_m6596926FAC19DAB83A2C1D47A259EAB38FE46BB5 (void);
// 0x000000AF System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfProperty::.ctor()
extern void GltfProperty__ctor_mB5C660191EDB60772404A33CA1C36112040E27C0 (void);
// 0x000000B0 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMagnificationFilterMode Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::get_MagFilter()
extern void GltfSampler_get_MagFilter_mB146947FE52BB43EAA264A3CEDD1F25C094EBEF4 (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::set_MagFilter(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMagnificationFilterMode)
extern void GltfSampler_set_MagFilter_m0704D4FD0609B3FA97B49AC5470BCEEAEDBA66C0 (void);
// 0x000000B2 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMinFilterMode Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::get_MinFilter()
extern void GltfSampler_get_MinFilter_m78F3D0069352C0BBA7782B08AAE515235EBCB556 (void);
// 0x000000B3 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::set_MinFilter(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfMinFilterMode)
extern void GltfSampler_set_MinFilter_m10EBBE202C5B9483C2051C3725E761720FE17F22 (void);
// 0x000000B4 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfWrapMode Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::get_WrapS()
extern void GltfSampler_get_WrapS_mD5F2FE231FC7EC5D1B6E399CA89B73D6B7796E78 (void);
// 0x000000B5 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::set_WrapS(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfWrapMode)
extern void GltfSampler_set_WrapS_m744CEBACD05AB1936908B5B4F4315EB36113EBB5 (void);
// 0x000000B6 Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfWrapMode Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::get_WrapT()
extern void GltfSampler_get_WrapT_m2B2DA71D5AD8230066529AEA53B2134F32E0F586 (void);
// 0x000000B7 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::set_WrapT(Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfWrapMode)
extern void GltfSampler_set_WrapT_mD5888B5A41D37DE8420859C4D37D5C954536C159 (void);
// 0x000000B8 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::UnityEngine.ISerializationCallbackReceiver.OnAfterDeserialize()
extern void GltfSampler_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m865B89AF77AFA0E1DCCE0291F6374F8B4008B12D (void);
// 0x000000B9 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::UnityEngine.ISerializationCallbackReceiver.OnBeforeSerialize()
extern void GltfSampler_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mE8BC3523106CAB07B8CBBCA58B233584EE8044C6 (void);
// 0x000000BA System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSampler::.ctor()
extern void GltfSampler__ctor_mAEBBA1D19563C1D75B9794C80F07673AEB8347E8 (void);
// 0x000000BB System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfScene::.ctor()
extern void GltfScene__ctor_m594E6BE0B49DB6E3D76EAFB171306C33821FB279 (void);
// 0x000000BC System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfSkin::.ctor()
extern void GltfSkin__ctor_m3B3FD3DB322F396BB9BDE24F622F111C788BE530 (void);
// 0x000000BD UnityEngine.Texture2D Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::get_Texture()
extern void GltfTexture_get_Texture_mAC7497B03D6D3F0B8D8E5C2186DB6AF1EA7C649B (void);
// 0x000000BE System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::set_Texture(UnityEngine.Texture2D)
extern void GltfTexture_set_Texture_mE099B0160BE5EACD982D92E07BE7858561043483 (void);
// 0x000000BF System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTexture::.ctor()
extern void GltfTexture__ctor_m78B426F2E75CCBAC1179C59A5A0E2952E0E86176 (void);
// 0x000000C0 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.GltfTextureInfo::.ctor()
extern void GltfTextureInfo__ctor_m20B30B747F212C75CE4160D59FB51C514B711E12 (void);
// 0x000000C1 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.GltfExtension::.ctor()
extern void GltfExtension__ctor_m7A45395411B26257CC50B9CB4B804D5F593228F1 (void);
// 0x000000C2 System.Void Microsoft.MixedReality.Toolkit.Utilities.Gltf.Schema.Extensions.KHR_Materials_PbrSpecularGlossiness::.ctor()
extern void KHR_Materials_PbrSpecularGlossiness__ctor_m0EBF49E135163C5557DEDBD458724CB38E7A1B5D (void);
static Il2CppMethodPointer s_methodPointers[194] = 
{
	GltfAsset_get_Model_mC4DDD203AA774C00AFAD0309553C8CB012DF2019,
	GltfAsset_set_Model_m2D4BD80878BCF1A9192F98BAF5775523E23F9E55,
	GltfAsset_get_GltfObject_mE5B97605A4371AABA76F6F1D010F67D27057E29E,
	GltfAsset_set_GltfObject_mD87951AAB5124005D9F59339639CA4AEB5CAB837,
	GltfAsset__ctor_m8891E2232FA64DE019096CE90109898806D86B8D,
	ConstructGltf_Construct_m5B3AEF104ED2D31DDBEA7DD4B9D0D6ADBEE05F01,
	ConstructGltf_ConstructAsync_m7D8DED4997955B3C8E1270715D7566C1D23D9C0E,
	ConstructGltf_ConstructBufferView_mCEFEFF8F9D7EC38016D113FF643470E5F20DC1CB,
	ConstructGltf_ConstructTextureAsync_m7F928F2941F7C799FDEC6252A3AAC293FEF8C597,
	ConstructGltf_ConstructMaterialAsync_m9C09E0F710CCCA9DFB0BFB1E764F8B5E1766FB11,
	ConstructGltf_CreateMRTKShaderMaterial_m670DA79EAC79B97EBB9A3609EA312574A78A9AA6,
	ConstructGltf_CreateStandardShaderMaterial_mB2052CF02D269739588A8FAE957688217A216CE5,
	ConstructGltf_ConstructSceneAsync_mE14D906EEB50971535285DBFC472F4CA05A7272B,
	ConstructGltf_ConstructNodeAsync_mEE0FE079384A22AD6DE819B1EAE9EEE9D77BD8D2,
	ConstructGltf_ConstructMeshAsync_m9589F63537AD13669E854E702AF9B974CE4A89A6,
	ConstructGltf_ConstructMeshPrimitiveAsync_mDD0A27D33D9F8DF508061D060585DA67A69C9B63,
	ConstructGltf_CreateBoneWeightArray_mD8E3F32D75E2F6CF7A0C85B85C19DC45A827624E,
	ConstructGltf_NormalizeBoneWeightArray_mD05E0095E67C0833619E2230AEBF38249CCB0EBE,
	ConstructGltf__cctor_m18E7D288776076BB92C307FDC260644213AC5ECB,
	U3CConstructU3Ed__18_MoveNext_mE503DAF403D164611C146970C1D4CFFE5D1470C2,
	U3CConstructU3Ed__18_SetStateMachine_m605C512A909E6395AB7757F6771A6135D24F6748,
	U3CConstructAsyncU3Ed__19_MoveNext_mC3CE4BCA1A662C1291DE6AAF12DDEB7B35370F37,
	U3CConstructAsyncU3Ed__19_SetStateMachine_m9338CF450CCCC5EB8383006804B2A9B621F98DE0,
	U3CConstructTextureAsyncU3Ed__21_MoveNext_m87D7E5DA135F432F4EB478D589DFD4922B59E3C0,
	U3CConstructTextureAsyncU3Ed__21_SetStateMachine_m924F856DDEE0A4645E0EE4CD224BD6B91D9D16AE,
	U3CConstructMaterialAsyncU3Ed__22_MoveNext_m80B465BDB7E9F5781750AFF4AF41452D72AFD795,
	U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m7EDB98E424527E860064E6F93BF30AB2CACB56D2,
	U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mBA59B13C04DFFE441058520303F3F9AA864A2832,
	U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m6CEFEDDCE60E12D51A332EBDD9C709E93BCDA729,
	U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m5C49AC6B31D83B4523416DF2BE92DC5EC0BD4EF4,
	U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mD4AA2B01181C8BE31F7DD26BEBFA2C44C2BEC2C6,
	U3CConstructSceneAsyncU3Ed__25_MoveNext_m1A9CECE2140E4D31FAF63E72DFB5D2A6FA56481B,
	U3CConstructSceneAsyncU3Ed__25_SetStateMachine_m53205A9F9AE1223DE7AD03A367FAD4794904406B,
	U3CConstructNodeAsyncU3Ed__26_MoveNext_m81F42582D2EE39E5D7D35B7E822A103F527D78CE,
	U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m4214DB18E4A84F811B66F2A3E1C2412EF452569E,
	U3CConstructMeshAsyncU3Ed__27_MoveNext_m90AAD4825B99652BBEB9C0ABFB92B2E55817E977,
	U3CConstructMeshAsyncU3Ed__27_SetStateMachine_m74F8878283419AD77C747655139E5D5A79D84EFC,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m67EE15B8890FB05E7731747B81EE3A0BC79A17E4,
	U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m31A9C930B5EC4DE05D89E61B103FAE6484875285,
	GltfConversions_GetTrsProperties_mCAB318A57E890B44AB3A02464C9218573764C168,
	GltfConversions_GetColorValue_mE364D536A258EBD0B22F32EEB6810A1BEC7C3724,
	GltfConversions_SetColorValue_m84F382E8520F07EBFE42CEFED8F3F1F00A0C02D1,
	GltfConversions_GetVector2Value_m84288616D4A7A4912CF1CC65CEE9340447F6F0E9,
	GltfConversions_SetVector2Value_m6C9E2FC060DF5E136A65386D99577FE650021B2D,
	GltfConversions_GetVector3Value_m609322FA4E8B41F53DE9B2750E13B8E5CD06736D,
	GltfConversions_SetVector3Value_mEBD2A89D8DA5A04C93F06EC79E097CA88EAE99C7,
	GltfConversions_GetQuaternionValue_m67C13D9B6CE3617603D259380E44B5FC53591B49,
	GltfConversions_SetQuaternionValue_mA66B7AC7A8E3C14081FF40B393FC3060EAC8C755,
	GltfConversions_GetMatrix4X4Value_m7999A957D1BE22C3CEF38505814E551C7C8FE1C0,
	GltfConversions_SetMatrix4X4Value_m0D2DA8652B377B81D7356FB4DA1F67954170959B,
	GltfConversions_GetTrsProperties_mED3580C4F5D67CDCF8B9C2F47A079EA84F1B2238,
	GltfConversions_GetIntArray_m9614119496482521C3CEE849688E71D224F0EFB4,
	GltfConversions_GetVector2Array_m4F6DEB3E1B5DFF4255160F69C94600070465B686,
	GltfConversions_GetVector3Array_mAB36A58794ED6CA09B263B207CF6C0798316D01A,
	GltfConversions_GetVector4Array_m701755C81CA63D1F9D0D39D450CBC3993EF55130,
	GltfConversions_GetColorArray_m1FA850D9DC93F757E12B8E4B9DD198F606C60831,
	GltfConversions_GetTypeDetails_m18C98C77B7EBF5B105955A8C997B359EFEFCF7B4,
	GltfConversions_GetDiscreteElement_m0CB9BAA04E87E05DDE8BFC525092E518156ABDD3,
	GltfConversions_GetDiscreteUnsignedElement_m10789E88E9DBAA37870BFBFA9B7D383E4BC17797,
	GltfConversions__cctor_m2AA55059FCFFFFC0CE8878447918C2F5027F41D5,
	GltfUtility_ImportGltfObjectFromPathAsync_m96B0EFC1C5681224666D0960D1F2966E77DE2471,
	GltfUtility_GetGltfObjectFromJson_m3665515DB5BF7E0AA99B9D858877EB9F935C1598,
	GltfUtility_GetGltfObjectFromGlb_m60966266F8AED6ACF3940226500645AB4C6EB87A,
	GltfUtility_GetJsonObject_mC92D10D6FD707A5536238220D86241B831C585D1,
	GltfUtility_GetGltfMeshPrimitiveAttributes_m97A563ED0CC82A99E1DD0905F5746CB49C52E719,
	GltfUtility_GetGltfMeshPrimitiveAttributes_mC5EEE7594FE78EB02AED5A50DAE7A058C49780BC,
	GltfUtility_GetGltfExtensionObjects_m28153ABEF97016E8FCD8D44CCDEC6FBCC1C9FFBF,
	GltfUtility_GetGltfExtraObjects_m718DDBF41C65B9630B6EC752B9D4202F01C65466,
	GltfUtility_GetGltfExtensions_m93DD36CB1B67E39C8891B6C2D52CF7F84D929E7B,
	GltfUtility_GetJsonObject_m6DA239E6A471AFF59C81BF80A1B57E09C3CFD5D6,
	GltfUtility_GetGltfNodeName_m77A1786AB9B5D7845E2AACE5DBF7CA8D72666E1D,
	GltfUtility_StringIntDictionaryFromJson_m31EC1C06884CA0EA7A87B1B1FA827255BB924398,
	GltfUtility_JsonDictionaryToArray_m1469EDC451D697763B178167016056E5F5F7FCA5,
	GltfUtility__cctor_mD3C5B3F6EB17F366D3A6F2BD707A7CC0BA0E3812,
	StringKeyValue__ctor_m52539F4A5551A4ED044941053539FB31536B910E,
	StringIntKeyValueArray__ctor_m5913999BD5CAA024698F1A8F6076F7CC4AF77909,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_m2661330414A4711134EC23DD76083108C714E06A,
	U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m513F78F2BB7D20D852D5E7A3069C0D80D8EAF5A2,
	GltfAccessor_get_ComponentType_m12EB992119212899C92A281AC9EEC508DB542D21,
	GltfAccessor_set_ComponentType_mEE91E5CE0C2B5C58678FB93D80C1DB37F1B071E8,
	GltfAccessor_get_BufferView_m639F63CC808C1E3E0D2B9A81ADC2146370BA936B,
	GltfAccessor_set_BufferView_m20284A6CE969BA1A6855C8273EDEA2B3D5F596CA,
	GltfAccessor_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m2CC95F12361A397545C85EEB92FCF161430FE3C9,
	GltfAccessor_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDD3CCC7E4DFFF5DE4E18F8382F1E33E6AE917AC7,
	GltfAccessor__ctor_mF54C929003442995BBEB3978DC1CC9B0897EDDF3,
	GltfAccessorSparse__ctor_m6DC3BE5C003EE4B1DBD0744D486CC1B4AA077144,
	GltfAccessorSparseIndices_get_ComponentType_mBD691A86F63A15C8DA64AD07616BD62642290DFC,
	GltfAccessorSparseIndices_set_ComponentType_mB8AAB0BE602894A6E64ACD51C39A90F887C19D50,
	GltfAccessorSparseIndices_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m8F33CC9C6585B811D2784D6D219DF61BDE916D0D,
	GltfAccessorSparseIndices_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m18C5ADA34DD6C383C7EE1CA8D1B0F1D3658629AB,
	GltfAccessorSparseIndices__ctor_m20B1261D7424B791AFE51B36BB8C5D5D28CD4084,
	GltfAccessorSparseValues__ctor_m33FB61FA3523FCEDE8C3AFFC4FBAA7C510E9D4E3,
	GltfAnimation__ctor_mFEEF1FCDA26FF29EA5EE35BF03645C5E691ECB0D,
	GltfAnimationChannel__ctor_m0922346D004007EAAD2E6F7C78F0A88405A5A604,
	GltfAnimationChannelTarget_get_Path_mE4CDC1D8E1356685D1CFBCFAD43F088891628366,
	GltfAnimationChannelTarget_set_Path_m99C5ECF32F9C386E74C349EC2086E840BA6C05E7,
	GltfAnimationChannelTarget_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mA7CFD42677D3FBC06A33A433C9AE6B7489C878DA,
	GltfAnimationChannelTarget_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m0D879D7AC512F99CCBED877399725E62283DF30C,
	GltfAnimationChannelTarget__ctor_mD090B9B181C6B3369AAE57E23473019BE9C96C0D,
	GltfAnimationSampler_get_Interpolation_m2E5ABD45ADA812436BEC080CC6C11AC8917BE353,
	GltfAnimationSampler_set_Interpolation_m6B9BEDF0C47ACAF25D005CDD58285D365294E827,
	GltfAnimationSampler_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m7BCF4DEACF1CB08650576F1661C77A3AEC014B2B,
	GltfAnimationSampler_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m54AC4EBF482CB7B72E37AB659DEB76373761BF17,
	GltfAnimationSampler__ctor_mCB23A17AF0BE7B430E3797C882BC3E2DD2B5EA81,
	GltfAssetInfo__ctor_m09835D55492B2C559096424DE4B5B05FAFF0139F,
	GltfBuffer_get_BufferData_m94C414A5C3AACDC40A6E70046136244A599C7203,
	GltfBuffer_set_BufferData_mD84BD7FAA98E530FE050E11C7FB11196FDD5566D,
	GltfBuffer__ctor_m357B7EE058144B357DB561CA66D1E3D2F99AF59A,
	GltfBufferView_get_Target_mFCBD2F2DBE166210E82309FB905942011AB1F99D,
	GltfBufferView_set_Target_m1CBA16E6429926707B6BC7265C2B166044CA14F4,
	GltfBufferView_get_Buffer_mBEAB185FC665306121E81F91E419A4199AEBE542,
	GltfBufferView_set_Buffer_m197BA2D950CBB1520FFC904344370F85B83833C7,
	GltfBufferView_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_mF7B727EB4E24BF9F4CB3C0086928A8BD8DFE80D8,
	GltfBufferView_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mAD02ABE414AA157DAC026A9DFD92D7A1B5D78B40,
	GltfBufferView__ctor_m8014047B477FA5B076626E3B791CFB4414186F67,
	GltfCamera_get_Type_mE5557535832093736D378168EDFBF91B8A853FDB,
	GltfCamera_set_Type_m7F7B008431CC421D01FB23229BDBD7BF3ACF81B4,
	GltfCamera_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m7B436028160D7DDE3FD04711141A78C78CED9F36,
	GltfCamera_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m4525FA98BAF462AFF58DFC1735A6E0DD72D556B8,
	GltfCamera__ctor_mC3F2AC360DF306B0679AF3AE726F20B5AE9B99F1,
	GltfCameraOrthographic__ctor_m3237FBAAFDD8287756E18E904C2CD9E0AB38480C,
	GltfCameraPerspective__ctor_mA65F5936BC56E102C6BCA4FB1E9F9F3EC58C8203,
	GltfChildOfRootProperty__ctor_m7B05817F21462220CD6605F7C37ACFB4E036C8D5,
	GltfImage_get_Texture_m1DCC77F66B200F22E537B14689BF81DCF563F6D2,
	GltfImage_set_Texture_mB861C9EC595DCC97B814CAD831F68CC1F77AAE57,
	GltfImage__ctor_mC4A61788CD4802B5CBB0EF0C4F9D68DB7F58947C,
	GltfMaterial_get_Material_mE46692023D0FBBC7CFC97FAFF2F1BE75ADD62569,
	GltfMaterial_set_Material_m9484345E96A6B9B9E25DB50280E748B970C2DA31,
	GltfMaterial__ctor_m0A21AA00FB6BE40059B21ECAA011DCB7950D612A,
	GltfMaterialCommonConstant__ctor_m29F574B6B035403B9E0856A22968C89B968ABF16,
	GltfMesh_get_Mesh_m002F9278D0C13783F4BD28C982E2AD0A7DF75306,
	GltfMesh_set_Mesh_mF45E21ADEF2BE4EA36B249EBD08F92D2D6891056,
	GltfMesh__ctor_m5D086289E80762A94ABC6358136F494CA93A2291,
	GltfMeshPrimitive_get_Mode_m4458F8C3DECBA5D599C5D2B03887A25EF21FF42B,
	GltfMeshPrimitive_set_Mode_mBE4525832D69B275AA71800E224929D8276FDEF4,
	GltfMeshPrimitive_get_Targets_m57C26C5674BBC73D791B216179B9E4A96F0540FB,
	GltfMeshPrimitive_set_Targets_m50470E531DAD5365BDFA978B7D30BC78302CF1D5,
	GltfMeshPrimitive_get_Attributes_mB9B5BD70D80F43037C5B59B8C5A884C9C804CD77,
	GltfMeshPrimitive_set_Attributes_mBD7D7864F65958C9758A3B0ACAEAA1EA8ADC9F83,
	GltfMeshPrimitive_get_SubMesh_m3CD3B47543CC369131B18F32C51E599D6A21BBF6,
	GltfMeshPrimitive_set_SubMesh_mF6C07B8E99656BBCC561E05E2EDBD205E4EA8A91,
	GltfMeshPrimitive_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m947D2DF08371AEED0E2A5EA07F64EA9E2A771097,
	GltfMeshPrimitive_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mDC0565EB179E061237AAEAEF3F3E7E588972D5BB,
	GltfMeshPrimitive__ctor_m818C1C3900EE0C5DA1A0032AED672CD806C9209C,
	GltfMeshPrimitiveAttributes__ctor_m2764B2BFFA305A6319BB3CD2F5AE962C4279EE3B,
	GltfMeshPrimitiveAttributes_TryGetDefault_mB7204F3C7CDB5271EE2BEEEF3D12F7911406AE32,
	GltfMeshPrimitiveAttributes_get_POSITION_m417AF341DE4EE8A2A518E070909AE4E440C10B98,
	GltfMeshPrimitiveAttributes_get_NORMAL_m7A1171E093819D113E6F880FE6EB11FF053502AB,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_0_mFFC6AFB41D315C3ACEE04EEB93CC0C89711B40AC,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_1_m22CA53116D75144EE9238E843FFF54CA332E96DD,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_2_m2693EAAE5314DB0CAA3D2F67804CD4DB5F1C6684,
	GltfMeshPrimitiveAttributes_get_TEXCOORD_3_m793161ED606A80CF3D62626D07CC9EA9E72F5355,
	GltfMeshPrimitiveAttributes_get_COLOR_0_m6CC8C480A82169D43091D193075837C8C4A11BD0,
	GltfMeshPrimitiveAttributes_get_TANGENT_mCD594DC29AEF335F07C8E64914C9192D39739DBC,
	GltfMeshPrimitiveAttributes_get_WEIGHTS_0_m4C460EDBAC3F510C336CEEDC6E340C579A136E2A,
	GltfMeshPrimitiveAttributes_get_JOINTS_0_mEB2C682D7C2E92D29D8FBDA5CB3D85B8CCF3ED6F,
	GltfNode_get_Matrix_m9A1C9B949F59B70BDA37BF412B90D3F8AAB73CA9,
	GltfNode_set_Matrix_mA0DE507B238B4F3C44CA455EA7794A4E6AB2EF45,
	GltfNode__ctor_mE4BAEFD8AF83FAA40193350F20EC3E6CF59C5991,
	GltfNormalTextureInfo__ctor_mDF32F13A83B3AE33180040310D974944DAAB03B9,
	GltfObject_get_Name_mD989932FBB5D60FE94494EE52FF03F6ADD1C3E9B,
	GltfObject_set_Name_m942ABD66F0EBF82C86F5067BED28DE48B6448A9B,
	GltfObject_get_Uri_m59A34ED39B3C8304F0C96323A6C347874DA83C64,
	GltfObject_set_Uri_m508D9DDA0F16B0C91AC9F502FD8A5DFD04F2FD58,
	GltfObject_get_GameObjectReference_m77BB72793CDAB811BEC7A4EA50DE4C3927AD233A,
	GltfObject_set_GameObjectReference_m7C857AF7FE86C15A3EC317F209221AE2395E714C,
	GltfObject_get_RegisteredExtensions_m1152782A75C41E55854A8C807BD89270731839DA,
	GltfObject_set_RegisteredExtensions_m55444DE0E0EDF8F3BA3C2F575E5C4C89EFCEB567,
	GltfObject_get_UseBackgroundThread_mB7AC0B9C4B71C896D5422C57D1B6DF797D055AB8,
	GltfObject_set_UseBackgroundThread_m967C65C78C343544366861CE3B91D2F2BEE997EE,
	GltfObject_GetAccessor_m09D9EF065161EA49A969485E55EE56F63EEE2AFD,
	GltfObject__ctor_m75AD55E97B58A4EA7EB2D19EAC71BF1BFA1E9CA8,
	GltfOcclusionTextureInfo__ctor_m3BBFF83325FEC18EBB4D81A6CA56002905C4BE9C,
	GltfPbrMetallicRoughness__ctor_m6596926FAC19DAB83A2C1D47A259EAB38FE46BB5,
	GltfProperty__ctor_mB5C660191EDB60772404A33CA1C36112040E27C0,
	GltfSampler_get_MagFilter_mB146947FE52BB43EAA264A3CEDD1F25C094EBEF4,
	GltfSampler_set_MagFilter_m0704D4FD0609B3FA97B49AC5470BCEEAEDBA66C0,
	GltfSampler_get_MinFilter_m78F3D0069352C0BBA7782B08AAE515235EBCB556,
	GltfSampler_set_MinFilter_m10EBBE202C5B9483C2051C3725E761720FE17F22,
	GltfSampler_get_WrapS_mD5F2FE231FC7EC5D1B6E399CA89B73D6B7796E78,
	GltfSampler_set_WrapS_m744CEBACD05AB1936908B5B4F4315EB36113EBB5,
	GltfSampler_get_WrapT_m2B2DA71D5AD8230066529AEA53B2134F32E0F586,
	GltfSampler_set_WrapT_mD5888B5A41D37DE8420859C4D37D5C954536C159,
	GltfSampler_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m865B89AF77AFA0E1DCCE0291F6374F8B4008B12D,
	GltfSampler_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_mE8BC3523106CAB07B8CBBCA58B233584EE8044C6,
	GltfSampler__ctor_mAEBBA1D19563C1D75B9794C80F07673AEB8347E8,
	GltfScene__ctor_m594E6BE0B49DB6E3D76EAFB171306C33821FB279,
	GltfSkin__ctor_m3B3FD3DB322F396BB9BDE24F622F111C788BE530,
	GltfTexture_get_Texture_mAC7497B03D6D3F0B8D8E5C2186DB6AF1EA7C649B,
	GltfTexture_set_Texture_mE099B0160BE5EACD982D92E07BE7858561043483,
	GltfTexture__ctor_m78B426F2E75CCBAC1179C59A5A0E2952E0E86176,
	GltfTextureInfo__ctor_m20B30B747F212C75CE4160D59FB51C514B711E12,
	GltfExtension__ctor_m7A45395411B26257CC50B9CB4B804D5F593228F1,
	KHR_Materials_PbrSpecularGlossiness__ctor_m0EBF49E135163C5557DEDBD458724CB38E7A1B5D,
};
extern void U3CConstructU3Ed__18_MoveNext_mE503DAF403D164611C146970C1D4CFFE5D1470C2_AdjustorThunk (void);
extern void U3CConstructU3Ed__18_SetStateMachine_m605C512A909E6395AB7757F6771A6135D24F6748_AdjustorThunk (void);
extern void U3CConstructAsyncU3Ed__19_MoveNext_mC3CE4BCA1A662C1291DE6AAF12DDEB7B35370F37_AdjustorThunk (void);
extern void U3CConstructAsyncU3Ed__19_SetStateMachine_m9338CF450CCCC5EB8383006804B2A9B621F98DE0_AdjustorThunk (void);
extern void U3CConstructTextureAsyncU3Ed__21_MoveNext_m87D7E5DA135F432F4EB478D589DFD4922B59E3C0_AdjustorThunk (void);
extern void U3CConstructTextureAsyncU3Ed__21_SetStateMachine_m924F856DDEE0A4645E0EE4CD224BD6B91D9D16AE_AdjustorThunk (void);
extern void U3CConstructMaterialAsyncU3Ed__22_MoveNext_m80B465BDB7E9F5781750AFF4AF41452D72AFD795_AdjustorThunk (void);
extern void U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m7EDB98E424527E860064E6F93BF30AB2CACB56D2_AdjustorThunk (void);
extern void U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mBA59B13C04DFFE441058520303F3F9AA864A2832_AdjustorThunk (void);
extern void U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m6CEFEDDCE60E12D51A332EBDD9C709E93BCDA729_AdjustorThunk (void);
extern void U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m5C49AC6B31D83B4523416DF2BE92DC5EC0BD4EF4_AdjustorThunk (void);
extern void U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mD4AA2B01181C8BE31F7DD26BEBFA2C44C2BEC2C6_AdjustorThunk (void);
extern void U3CConstructSceneAsyncU3Ed__25_MoveNext_m1A9CECE2140E4D31FAF63E72DFB5D2A6FA56481B_AdjustorThunk (void);
extern void U3CConstructSceneAsyncU3Ed__25_SetStateMachine_m53205A9F9AE1223DE7AD03A367FAD4794904406B_AdjustorThunk (void);
extern void U3CConstructNodeAsyncU3Ed__26_MoveNext_m81F42582D2EE39E5D7D35B7E822A103F527D78CE_AdjustorThunk (void);
extern void U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m4214DB18E4A84F811B66F2A3E1C2412EF452569E_AdjustorThunk (void);
extern void U3CConstructMeshAsyncU3Ed__27_MoveNext_m90AAD4825B99652BBEB9C0ABFB92B2E55817E977_AdjustorThunk (void);
extern void U3CConstructMeshAsyncU3Ed__27_SetStateMachine_m74F8878283419AD77C747655139E5D5A79D84EFC_AdjustorThunk (void);
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m67EE15B8890FB05E7731747B81EE3A0BC79A17E4_AdjustorThunk (void);
extern void U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m31A9C930B5EC4DE05D89E61B103FAE6484875285_AdjustorThunk (void);
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_m2661330414A4711134EC23DD76083108C714E06A_AdjustorThunk (void);
extern void U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m513F78F2BB7D20D852D5E7A3069C0D80D8EAF5A2_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[22] = 
{
	{ 0x06000014, U3CConstructU3Ed__18_MoveNext_mE503DAF403D164611C146970C1D4CFFE5D1470C2_AdjustorThunk },
	{ 0x06000015, U3CConstructU3Ed__18_SetStateMachine_m605C512A909E6395AB7757F6771A6135D24F6748_AdjustorThunk },
	{ 0x06000016, U3CConstructAsyncU3Ed__19_MoveNext_mC3CE4BCA1A662C1291DE6AAF12DDEB7B35370F37_AdjustorThunk },
	{ 0x06000017, U3CConstructAsyncU3Ed__19_SetStateMachine_m9338CF450CCCC5EB8383006804B2A9B621F98DE0_AdjustorThunk },
	{ 0x06000018, U3CConstructTextureAsyncU3Ed__21_MoveNext_m87D7E5DA135F432F4EB478D589DFD4922B59E3C0_AdjustorThunk },
	{ 0x06000019, U3CConstructTextureAsyncU3Ed__21_SetStateMachine_m924F856DDEE0A4645E0EE4CD224BD6B91D9D16AE_AdjustorThunk },
	{ 0x0600001A, U3CConstructMaterialAsyncU3Ed__22_MoveNext_m80B465BDB7E9F5781750AFF4AF41452D72AFD795_AdjustorThunk },
	{ 0x0600001B, U3CConstructMaterialAsyncU3Ed__22_SetStateMachine_m7EDB98E424527E860064E6F93BF30AB2CACB56D2_AdjustorThunk },
	{ 0x0600001C, U3CCreateMRTKShaderMaterialU3Ed__23_MoveNext_mBA59B13C04DFFE441058520303F3F9AA864A2832_AdjustorThunk },
	{ 0x0600001D, U3CCreateMRTKShaderMaterialU3Ed__23_SetStateMachine_m6CEFEDDCE60E12D51A332EBDD9C709E93BCDA729_AdjustorThunk },
	{ 0x0600001E, U3CCreateStandardShaderMaterialU3Ed__24_MoveNext_m5C49AC6B31D83B4523416DF2BE92DC5EC0BD4EF4_AdjustorThunk },
	{ 0x0600001F, U3CCreateStandardShaderMaterialU3Ed__24_SetStateMachine_mD4AA2B01181C8BE31F7DD26BEBFA2C44C2BEC2C6_AdjustorThunk },
	{ 0x06000020, U3CConstructSceneAsyncU3Ed__25_MoveNext_m1A9CECE2140E4D31FAF63E72DFB5D2A6FA56481B_AdjustorThunk },
	{ 0x06000021, U3CConstructSceneAsyncU3Ed__25_SetStateMachine_m53205A9F9AE1223DE7AD03A367FAD4794904406B_AdjustorThunk },
	{ 0x06000022, U3CConstructNodeAsyncU3Ed__26_MoveNext_m81F42582D2EE39E5D7D35B7E822A103F527D78CE_AdjustorThunk },
	{ 0x06000023, U3CConstructNodeAsyncU3Ed__26_SetStateMachine_m4214DB18E4A84F811B66F2A3E1C2412EF452569E_AdjustorThunk },
	{ 0x06000024, U3CConstructMeshAsyncU3Ed__27_MoveNext_m90AAD4825B99652BBEB9C0ABFB92B2E55817E977_AdjustorThunk },
	{ 0x06000025, U3CConstructMeshAsyncU3Ed__27_SetStateMachine_m74F8878283419AD77C747655139E5D5A79D84EFC_AdjustorThunk },
	{ 0x06000026, U3CConstructMeshPrimitiveAsyncU3Ed__28_MoveNext_m67EE15B8890FB05E7731747B81EE3A0BC79A17E4_AdjustorThunk },
	{ 0x06000027, U3CConstructMeshPrimitiveAsyncU3Ed__28_SetStateMachine_m31A9C930B5EC4DE05D89E61B103FAE6484875285_AdjustorThunk },
	{ 0x0600004D, U3CImportGltfObjectFromPathAsyncU3Ed__4_MoveNext_m2661330414A4711134EC23DD76083108C714E06A_AdjustorThunk },
	{ 0x0600004E, U3CImportGltfObjectFromPathAsyncU3Ed__4_SetStateMachine_m513F78F2BB7D20D852D5E7A3069C0D80D8EAF5A2_AdjustorThunk },
};
static const int32_t s_InvokerIndices[194] = 
{
	14,
	26,
	14,
	26,
	23,
	168,
	0,
	143,
	1,
	550,
	550,
	550,
	2,
	2669,
	550,
	1,
	550,
	168,
	3,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	23,
	26,
	2670,
	2331,
	1550,
	1673,
	2671,
	2672,
	2673,
	2674,
	2675,
	2676,
	2677,
	2678,
	167,
	167,
	167,
	167,
	0,
	897,
	579,
	579,
	3,
	0,
	0,
	0,
	1,
	0,
	1,
	1,
	1,
	1,
	126,
	0,
	0,
	0,
	3,
	23,
	23,
	23,
	26,
	10,
	32,
	14,
	26,
	23,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	10,
	32,
	14,
	26,
	23,
	23,
	23,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	14,
	26,
	23,
	23,
	14,
	26,
	23,
	10,
	32,
	14,
	26,
	14,
	26,
	14,
	26,
	23,
	23,
	23,
	26,
	503,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	10,
	1358,
	1359,
	23,
	23,
	14,
	26,
	14,
	26,
	14,
	26,
	14,
	26,
	89,
	31,
	34,
	23,
	23,
	23,
	23,
	10,
	32,
	10,
	32,
	10,
	32,
	10,
	32,
	23,
	23,
	23,
	23,
	23,
	14,
	26,
	23,
	23,
	23,
	23,
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_GltfCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_GltfCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Gltf.dll",
	194,
	s_methodPointers,
	22,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
