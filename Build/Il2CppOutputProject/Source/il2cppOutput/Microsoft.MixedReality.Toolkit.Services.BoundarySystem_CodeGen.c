﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::.ctor(Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundaryVisualizationProfile,Microsoft.MixedReality.Toolkit.Utilities.ExperienceScale)
extern void MixedRealityBoundarySystem__ctor_m922EEB27125D23A3BBCBDDDEDA11C4467BD05A5A (void);
// 0x00000002 System.String Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::get_Name()
extern void MixedRealityBoundarySystem_get_Name_mDD2197D4596CE220C700D44FAC9DB3B65F416E4F (void);
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::set_Name(System.String)
extern void MixedRealityBoundarySystem_set_Name_m0E1FEFD619E054032839E272B3B3605E53BBAD68 (void);
// 0x00000004 System.Collections.Generic.List`1<UnityEngine.Vector3> Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::GetBoundaryGeometry()
extern void MixedRealityBoundarySystem_GetBoundaryGeometry_m914EA44918827829A44F6DD910394A2FB1CE023E (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::SetTrackingSpace()
extern void MixedRealityBoundarySystem_SetTrackingSpace_m63EDAA131FAB585D474E7A83F8FDF57308828D1F (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundarySystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Boundary.MixedRealityBoundaryVisualizationProfile,Microsoft.MixedReality.Toolkit.Utilities.ExperienceScale)
extern void MixedRealityBoundarySystem__ctor_mD67B98D464F42FC8732B849014B106906E7ACC3D (void);
static Il2CppMethodPointer s_methodPointers[6] = 
{
	MixedRealityBoundarySystem__ctor_m922EEB27125D23A3BBCBDDDEDA11C4467BD05A5A,
	MixedRealityBoundarySystem_get_Name_mDD2197D4596CE220C700D44FAC9DB3B65F416E4F,
	MixedRealityBoundarySystem_set_Name_m0E1FEFD619E054032839E272B3B3605E53BBAD68,
	MixedRealityBoundarySystem_GetBoundaryGeometry_m914EA44918827829A44F6DD910394A2FB1CE023E,
	MixedRealityBoundarySystem_SetTrackingSpace_m63EDAA131FAB585D474E7A83F8FDF57308828D1F,
	MixedRealityBoundarySystem__ctor_mD67B98D464F42FC8732B849014B106906E7ACC3D,
};
static const int32_t s_InvokerIndices[6] = 
{
	137,
	14,
	26,
	14,
	23,
	110,
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_BoundarySystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_BoundarySystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.BoundarySystem.dll",
	6,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
