﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
struct InterfaceActionInvoker0
{
	typedef void (*Action)(void*, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct InterfaceActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeClass* declaringInterface, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_interface_invoke_data(slot, obj, declaringInterface);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// Klak.Spout.SpoutReceiver
struct SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919;
// Klak.Spout.SpoutSender
struct SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t2E51991ADA605DB75870908AF6D7C3093DC3FCBA;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34;
// UnityEngine.Camera/CameraCallback
struct CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0;
// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621;
// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598;
// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13;
// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6;
// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4;
// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD;
// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA;
// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;

IL2CPP_EXTERN_C RuntimeClass* CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ICollection_1_t2E51991ADA605DB75870908AF6D7C3093DC3FCBA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral34DCB24BD70D26BBFE5B0BEA0B614732307E8564;
IL2CPP_EXTERN_C String_t* _stringLiteral4D0B1ECDF7F63A17B25AB0B68A8A0E8F00A3E96B;
IL2CPP_EXTERN_C const RuntimeMethod* Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664Klak_Spout_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902Klak_Spout_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1EKlak_Spout_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1Klak_Spout_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutManager_GetSourceNames_mBA1F5E29DB301943567FABBD29CC2E85354A6B6F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutManager_GetSourceNames_mEB27875BBB4D5FFB58F883AD4BEBE975654F46D5_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutReceiver_Update_mEBF46B4FD09CECE23A717F96322B1B1844A09A43_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutSender_OnDisable_mA68599D3A9C29D76566081450DF5F16CFEB17575_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutSender_OnRenderImage_m22518EF3DF63D7D46B3F51DE4FA606D1C97DEA1E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t SpoutSender_Update_mF440263DB95F7A38D4141F89203E0D72036876CB_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC_MetadataUsageId;

struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tBF15457AA295897943C0E260F941ED2F159CB816 
{
public:

public:
};


// System.Object


// Klak.Spout.PluginEntry
struct PluginEntry_t40A901D4196231A1A0D49B7E1D529A346F12A0AD  : public RuntimeObject
{
public:

public:
};


// Klak.Spout.SpoutManager
struct SpoutManager_t055F45B1509A6379BDDE842FF49A41A7A104AF67  : public RuntimeObject
{
public:

public:
};


// Klak.Spout.Util
struct Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11  : public RuntimeObject
{
public:

public:
};

struct Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields
{
public:
	// UnityEngine.Rendering.CommandBuffer Klak.Spout.Util::_commandBuffer
	CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ____commandBuffer_0;

public:
	inline static int32_t get_offset_of__commandBuffer_0() { return static_cast<int32_t>(offsetof(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields, ____commandBuffer_0)); }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * get__commandBuffer_0() const { return ____commandBuffer_0; }
	inline CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD ** get_address_of__commandBuffer_0() { return &____commandBuffer_0; }
	inline void set__commandBuffer_0(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * value)
	{
		____commandBuffer_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____commandBuffer_0), (void*)value);
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// System.Boolean
struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.Single
struct Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1 
{
public:
	// System.Single System.Single::m_value
	float ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Single_tDDDA9169C4E4E308AC6D7A824F9B28DC82204AE1, ___m_value_0)); }
	inline float get_m_value_0() const { return ___m_value_0; }
	inline float* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(float value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// Klak.Spout.PluginEntry/Event
struct Event_t48EF803E862E1762C206D5011A25DDCC2643A8FD 
{
public:
	// System.Int32 Klak.Spout.PluginEntry/Event::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Event_t48EF803E862E1762C206D5011A25DDCC2643A8FD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HideFlags
struct HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37 
{
public:
	// System.Int32 UnityEngine.HideFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(HideFlags_t30B57DC00548E963A569318C8F4A4123E7447E37, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.MaterialPropertyBlock
struct MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.MaterialPropertyBlock::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Rendering.CommandBuffer
struct CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Rendering.CommandBuffer::m_Ptr
	intptr_t ___m_Ptr_0;

public:
	inline static int32_t get_offset_of_m_Ptr_0() { return static_cast<int32_t>(offsetof(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD, ___m_Ptr_0)); }
	inline intptr_t get_m_Ptr_0() const { return ___m_Ptr_0; }
	inline intptr_t* get_address_of_m_Ptr_0() { return &___m_Ptr_0; }
	inline void set_m_Ptr_0(intptr_t value)
	{
		___m_Ptr_0 = value;
	}
};


// UnityEngine.TextureFormat
struct TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Material
struct Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Shader
struct Shader_tE2731FF351B74AB4186897484FB01E000C1160CA  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.RenderTexture
struct RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Renderer
struct Renderer_t0556D67DD582620D1F495627EDE30D03284151F4  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// UnityEngine.Camera
struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};

struct Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields
{
public:
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreCull
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreCull_4;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPreRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPreRender_5;
	// UnityEngine.Camera/CameraCallback UnityEngine.Camera::onPostRender
	CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * ___onPostRender_6;

public:
	inline static int32_t get_offset_of_onPreCull_4() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreCull_4)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreCull_4() const { return ___onPreCull_4; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreCull_4() { return &___onPreCull_4; }
	inline void set_onPreCull_4(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreCull_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreCull_4), (void*)value);
	}

	inline static int32_t get_offset_of_onPreRender_5() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPreRender_5)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPreRender_5() const { return ___onPreRender_5; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPreRender_5() { return &___onPreRender_5; }
	inline void set_onPreRender_5(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPreRender_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPreRender_5), (void*)value);
	}

	inline static int32_t get_offset_of_onPostRender_6() { return static_cast<int32_t>(offsetof(Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_StaticFields, ___onPostRender_6)); }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * get_onPostRender_6() const { return ___onPostRender_6; }
	inline CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 ** get_address_of_onPostRender_6() { return &___onPostRender_6; }
	inline void set_onPostRender_6(CameraCallback_t8BBB42AA08D7498DFC11F4128117055BC7F0B9D0 * value)
	{
		___onPostRender_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___onPostRender_6), (void*)value);
	}
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// Klak.Spout.SpoutReceiver
struct SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String Klak.Spout.SpoutReceiver::_sourceName
	String_t* ____sourceName_4;
	// UnityEngine.RenderTexture Klak.Spout.SpoutReceiver::_targetTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____targetTexture_5;
	// UnityEngine.Renderer Klak.Spout.SpoutReceiver::_targetRenderer
	Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ____targetRenderer_6;
	// System.String Klak.Spout.SpoutReceiver::_targetMaterialProperty
	String_t* ____targetMaterialProperty_7;
	// UnityEngine.RenderTexture Klak.Spout.SpoutReceiver::_receivedTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____receivedTexture_8;
	// System.IntPtr Klak.Spout.SpoutReceiver::_plugin
	intptr_t ____plugin_9;
	// UnityEngine.Texture2D Klak.Spout.SpoutReceiver::_sharedTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____sharedTexture_10;
	// UnityEngine.Material Klak.Spout.SpoutReceiver::_blitMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____blitMaterial_11;
	// UnityEngine.MaterialPropertyBlock Klak.Spout.SpoutReceiver::_propertyBlock
	MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ____propertyBlock_12;

public:
	inline static int32_t get_offset_of__sourceName_4() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____sourceName_4)); }
	inline String_t* get__sourceName_4() const { return ____sourceName_4; }
	inline String_t** get_address_of__sourceName_4() { return &____sourceName_4; }
	inline void set__sourceName_4(String_t* value)
	{
		____sourceName_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sourceName_4), (void*)value);
	}

	inline static int32_t get_offset_of__targetTexture_5() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____targetTexture_5)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__targetTexture_5() const { return ____targetTexture_5; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__targetTexture_5() { return &____targetTexture_5; }
	inline void set__targetTexture_5(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____targetTexture_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____targetTexture_5), (void*)value);
	}

	inline static int32_t get_offset_of__targetRenderer_6() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____targetRenderer_6)); }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * get__targetRenderer_6() const { return ____targetRenderer_6; }
	inline Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 ** get_address_of__targetRenderer_6() { return &____targetRenderer_6; }
	inline void set__targetRenderer_6(Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * value)
	{
		____targetRenderer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____targetRenderer_6), (void*)value);
	}

	inline static int32_t get_offset_of__targetMaterialProperty_7() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____targetMaterialProperty_7)); }
	inline String_t* get__targetMaterialProperty_7() const { return ____targetMaterialProperty_7; }
	inline String_t** get_address_of__targetMaterialProperty_7() { return &____targetMaterialProperty_7; }
	inline void set__targetMaterialProperty_7(String_t* value)
	{
		____targetMaterialProperty_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____targetMaterialProperty_7), (void*)value);
	}

	inline static int32_t get_offset_of__receivedTexture_8() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____receivedTexture_8)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__receivedTexture_8() const { return ____receivedTexture_8; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__receivedTexture_8() { return &____receivedTexture_8; }
	inline void set__receivedTexture_8(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____receivedTexture_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____receivedTexture_8), (void*)value);
	}

	inline static int32_t get_offset_of__plugin_9() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____plugin_9)); }
	inline intptr_t get__plugin_9() const { return ____plugin_9; }
	inline intptr_t* get_address_of__plugin_9() { return &____plugin_9; }
	inline void set__plugin_9(intptr_t value)
	{
		____plugin_9 = value;
	}

	inline static int32_t get_offset_of__sharedTexture_10() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____sharedTexture_10)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__sharedTexture_10() const { return ____sharedTexture_10; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__sharedTexture_10() { return &____sharedTexture_10; }
	inline void set__sharedTexture_10(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____sharedTexture_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sharedTexture_10), (void*)value);
	}

	inline static int32_t get_offset_of__blitMaterial_11() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____blitMaterial_11)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__blitMaterial_11() const { return ____blitMaterial_11; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__blitMaterial_11() { return &____blitMaterial_11; }
	inline void set__blitMaterial_11(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____blitMaterial_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____blitMaterial_11), (void*)value);
	}

	inline static int32_t get_offset_of__propertyBlock_12() { return static_cast<int32_t>(offsetof(SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919, ____propertyBlock_12)); }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * get__propertyBlock_12() const { return ____propertyBlock_12; }
	inline MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 ** get_address_of__propertyBlock_12() { return &____propertyBlock_12; }
	inline void set__propertyBlock_12(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * value)
	{
		____propertyBlock_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____propertyBlock_12), (void*)value);
	}
};


// Klak.Spout.SpoutSender
struct SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// UnityEngine.RenderTexture Klak.Spout.SpoutSender::_sourceTexture
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ____sourceTexture_4;
	// System.Boolean Klak.Spout.SpoutSender::_alphaSupport
	bool ____alphaSupport_5;
	// System.IntPtr Klak.Spout.SpoutSender::_plugin
	intptr_t ____plugin_6;
	// UnityEngine.Texture2D Klak.Spout.SpoutSender::_sharedTexture
	Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ____sharedTexture_7;
	// UnityEngine.Material Klak.Spout.SpoutSender::_blitMaterial
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ____blitMaterial_8;

public:
	inline static int32_t get_offset_of__sourceTexture_4() { return static_cast<int32_t>(offsetof(SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA, ____sourceTexture_4)); }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * get__sourceTexture_4() const { return ____sourceTexture_4; }
	inline RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 ** get_address_of__sourceTexture_4() { return &____sourceTexture_4; }
	inline void set__sourceTexture_4(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * value)
	{
		____sourceTexture_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sourceTexture_4), (void*)value);
	}

	inline static int32_t get_offset_of__alphaSupport_5() { return static_cast<int32_t>(offsetof(SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA, ____alphaSupport_5)); }
	inline bool get__alphaSupport_5() const { return ____alphaSupport_5; }
	inline bool* get_address_of__alphaSupport_5() { return &____alphaSupport_5; }
	inline void set__alphaSupport_5(bool value)
	{
		____alphaSupport_5 = value;
	}

	inline static int32_t get_offset_of__plugin_6() { return static_cast<int32_t>(offsetof(SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA, ____plugin_6)); }
	inline intptr_t get__plugin_6() const { return ____plugin_6; }
	inline intptr_t* get_address_of__plugin_6() { return &____plugin_6; }
	inline void set__plugin_6(intptr_t value)
	{
		____plugin_6 = value;
	}

	inline static int32_t get_offset_of__sharedTexture_7() { return static_cast<int32_t>(offsetof(SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA, ____sharedTexture_7)); }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * get__sharedTexture_7() const { return ____sharedTexture_7; }
	inline Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** get_address_of__sharedTexture_7() { return &____sharedTexture_7; }
	inline void set__sharedTexture_7(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * value)
	{
		____sharedTexture_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____sharedTexture_7), (void*)value);
	}

	inline static int32_t get_offset_of__blitMaterial_8() { return static_cast<int32_t>(offsetof(SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA, ____blitMaterial_8)); }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * get__blitMaterial_8() const { return ____blitMaterial_8; }
	inline Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 ** get_address_of__blitMaterial_8() { return &____blitMaterial_8; }
	inline void set__blitMaterial_8(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * value)
	{
		____blitMaterial_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____blitMaterial_8), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) String_t* m_Items[1];

public:
	inline String_t* GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline String_t** GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, String_t* value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
	inline String_t* GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline String_t** GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, String_t* value)
	{
		m_Items[index] = value;
		Il2CppCodeGenWriteBarrier((void**)m_Items + index, (void*)value);
	}
};


// !!0 UnityEngine.Component::GetComponent<System.Object>()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RuntimeObject * Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method);

// System.Int32 Klak.Spout.PluginEntry::ScanSharedObjects()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2 (const RuntimeMethod* method);
// System.String Klak.Spout.PluginEntry::GetSharedObjectNameString(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF (int32_t ___index0, const RuntimeMethod* method);
// System.Boolean System.String::op_Equality(System.String,System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE (String_t* ___a0, String_t* ___b1, const RuntimeMethod* method);
// System.Void Klak.Spout.SpoutReceiver::RequestReconnect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_RequestReconnect_mF1B1915CCDBEFB2DC2DDC4D96608EA926986F0C1 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void Klak.Spout.SpoutReceiver::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Void Klak.Spout.Util::IssuePluginEvent(Klak.Spout.PluginEntry/Event,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC (int32_t ___pluginEvent0, intptr_t ___ptr1, const RuntimeMethod* method);
// System.Void Klak.Spout.Util::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Boolean Klak.Spout.PluginEntry::CheckValid(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PluginEntry_CheckValid_m96F923E00F14F4F21379099E037DB35880C99F80 (intptr_t ___ptr0, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.IntPtr Klak.Spout.PluginEntry::CreateReceiver(System.String)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664_inline (String_t* ___name0, const RuntimeMethod* method);
// System.IntPtr Klak.Spout.PluginEntry::GetTexturePointer(System.IntPtr)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_inline (intptr_t ___ptr0, const RuntimeMethod* method);
// System.Int32 Klak.Spout.PluginEntry::GetTextureWidth(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF (intptr_t ___ptr0, const RuntimeMethod* method);
// System.Int32 Klak.Spout.PluginEntry::GetTextureHeight(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB (intptr_t ___ptr0, const RuntimeMethod* method);
// System.IntPtr UnityEngine.Texture::GetNativeTexturePtr()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t Texture_GetNativeTexturePtr_mB29F70DB525E825A4A3BB908F90C06F3171E92FD (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// UnityEngine.Texture2D UnityEngine.Texture2D::CreateExternalTexture(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean,System.Boolean,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * Texture2D_CreateExternalTexture_m292548AA116AA5176E8CA03AFBBC088E3B698DE4 (int32_t ___width0, int32_t ___height1, int32_t ___format2, bool ___mipChain3, bool ___linear4, intptr_t ___nativeTex5, const RuntimeMethod* method);
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, int32_t ___value0, const RuntimeMethod* method);
// UnityEngine.Shader UnityEngine.Shader::Find(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B (String_t* ___name0, const RuntimeMethod* method);
// System.Void UnityEngine.Material::.ctor(UnityEngine.Shader)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * ___shader0, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture,UnityEngine.Material,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___dest1, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___mat2, int32_t ___pass3, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::.ctor(System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * __this, int32_t ___width0, int32_t ___height1, int32_t ___depth2, const RuntimeMethod* method);
// UnityEngine.Texture Klak.Spout.SpoutReceiver::get_receivedTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock__ctor_m9055A333A5DA8CC70CC3D837BD59B54C313D39F3 (MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::GetPropertyBlock(UnityEngine.MaterialPropertyBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_GetPropertyBlock_mCD279F8A7CEB56ABB9EF9D150103FB1C4FB3CE8C (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___properties0, const RuntimeMethod* method);
// System.Void UnityEngine.MaterialPropertyBlock::SetTexture(System.String,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MaterialPropertyBlock_SetTexture_m1F86AED47A6CDD6390B28746DDE87E15C4FDA763 (MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * __this, String_t* ___name0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___value1, const RuntimeMethod* method);
// System.Void UnityEngine.Renderer::SetPropertyBlock(UnityEngine.MaterialPropertyBlock)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Renderer_SetPropertyBlock_m1B999AB9B425587EF44CF1CB83CDE0A191F76C40 (Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * __this, MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * ___properties0, const RuntimeMethod* method);
// System.Void UnityEngine.MonoBehaviour::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97 (MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429 * __this, const RuntimeMethod* method);
// System.String UnityEngine.Object::get_name()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * __this, const RuntimeMethod* method);
// System.IntPtr Klak.Spout.PluginEntry::CreateSender(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902_inline (String_t* ___name0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method);
// System.Void UnityEngine.Material::SetFloat(System.String,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Material_SetFloat_m4B7D3FAA00D20BCB3C487E72B7E4B2691D5ECAD2 (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * __this, String_t* ___name0, float ___value1, const RuntimeMethod* method);
// UnityEngine.RenderTexture UnityEngine.RenderTexture::GetTemporary(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * RenderTexture_GetTemporary_m02547EA84CDEB9038FCF1EEDD283AF24114C735F (int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::CopyTexture(UnityEngine.Texture,UnityEngine.Texture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_CopyTexture_m06B5C14FA9E91CC78D3901898D712ADEB0C0B8A5 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___src0, Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___dst1, const RuntimeMethod* method);
// System.Void UnityEngine.RenderTexture::ReleaseTemporary(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___temp0, const RuntimeMethod* method);
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
inline Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 * __this, const RuntimeMethod* method)
{
	return ((  Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * (*) (Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621 *, const RuntimeMethod*))Component_GetComponent_TisRuntimeObject_m15E3130603CE5400743CCCDEE7600FB9EEFAE5C0_gshared)(__this, method);
}
// System.Void Klak.Spout.SpoutSender::SendRenderTexture(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::Blit(UnityEngine.Texture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113 (Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___dest1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Application::get_isPlaying()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5 (const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer__ctor_m4394F7E41C9BB751E382A8CAFA38B19F69E03890 (CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * __this, const RuntimeMethod* method);
// System.IntPtr Klak.Spout.PluginEntry::GetRenderEventFunc()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E_inline (const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::IssuePluginEventAndData(System.IntPtr,System.Int32,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_IssuePluginEventAndData_m613D8131D999BE26CBB9D380D08EDFAEFD388673 (CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * __this, intptr_t ___callback0, int32_t ___eventID1, intptr_t ___data2, const RuntimeMethod* method);
// System.Void UnityEngine.Graphics::ExecuteCommandBuffer(UnityEngine.Rendering.CommandBuffer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Graphics_ExecuteCommandBuffer_m99851E297195473AE9C72FB2CF93DDB71E67EEC0 (CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * ___buffer0, const RuntimeMethod* method);
// System.Void UnityEngine.Rendering.CommandBuffer::Clear()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CommandBuffer_Clear_mCE65F50CF8DBEE5543EFE80CA9FEDA780F76F430 (CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * __this, const RuntimeMethod* method);
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Boolean Klak.Spout.PluginEntry::get_IsAvailable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PluginEntry_get_IsAvailable_m03A7ABCA120E1B69BCCF7C9BD2367CD4859BC313 (const RuntimeMethod* method)
{
	{
		// internal static bool IsAvailable { get { return false; } }
		return (bool)0;
	}
}
// System.IntPtr Klak.Spout.PluginEntry::GetRenderEventFunc()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
// System.IntPtr Klak.Spout.PluginEntry::CreateSender(System.String,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902 (String_t* ___name0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
// System.IntPtr Klak.Spout.PluginEntry::CreateReceiver(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664 (String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
// System.IntPtr Klak.Spout.PluginEntry::GetTexturePointer(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1 (intptr_t ___ptr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
// System.Int32 Klak.Spout.PluginEntry::GetTextureWidth(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF (intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		// { return 0; }
		return 0;
	}
}
// System.Int32 Klak.Spout.PluginEntry::GetTextureHeight(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB (intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		// { return 0; }
		return 0;
	}
}
// System.Boolean Klak.Spout.PluginEntry::CheckValid(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool PluginEntry_CheckValid_m96F923E00F14F4F21379099E037DB35880C99F80 (intptr_t ___ptr0, const RuntimeMethod* method)
{
	{
		// { return false; }
		return (bool)0;
	}
}
// System.Int32 Klak.Spout.PluginEntry::ScanSharedObjects()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2 (const RuntimeMethod* method)
{
	{
		// { return 0; }
		return 0;
	}
}
// System.String Klak.Spout.PluginEntry::GetSharedObjectNameString(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF (int32_t ___index0, const RuntimeMethod* method)
{
	{
		// { return null; }
		return (String_t*)NULL;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String[] Klak.Spout.SpoutManager::GetSourceNames()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* SpoutManager_GetSourceNames_mBA1F5E29DB301943567FABBD29CC2E85354A6B6F (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutManager_GetSourceNames_mBA1F5E29DB301943567FABBD29CC2E85354A6B6F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* V_1 = NULL;
	int32_t V_2 = 0;
	{
		// var count = PluginEntry.ScanSharedObjects();
		int32_t L_0 = PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2(/*hidden argument*/NULL);
		V_0 = L_0;
		// var names = new string [count];
		int32_t L_1 = V_0;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_2 = (StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E*)SZArrayNew(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E_il2cpp_TypeInfo_var, (uint32_t)L_1);
		V_1 = L_2;
		// for (var i = 0; i < count; i++)
		V_2 = 0;
		goto IL_001e;
	}

IL_0011:
	{
		// names[i] = PluginEntry.GetSharedObjectNameString(i);
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_3 = V_1;
		int32_t L_4 = V_2;
		int32_t L_5 = V_2;
		String_t* L_6 = PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF(L_5, /*hidden argument*/NULL);
		NullCheck(L_3);
		ArrayElementTypeCheck (L_3, L_6);
		(L_3)->SetAt(static_cast<il2cpp_array_size_t>(L_4), (String_t*)L_6);
		// for (var i = 0; i < count; i++)
		int32_t L_7 = V_2;
		V_2 = ((int32_t)il2cpp_codegen_add((int32_t)L_7, (int32_t)1));
	}

IL_001e:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_8 = V_2;
		int32_t L_9 = V_0;
		if ((((int32_t)L_8) < ((int32_t)L_9)))
		{
			goto IL_0011;
		}
	}
	{
		// return names;
		StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* L_10 = V_1;
		return L_10;
	}
}
// System.Void Klak.Spout.SpoutManager::GetSourceNames(System.Collections.Generic.ICollection`1<System.String>)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutManager_GetSourceNames_mEB27875BBB4D5FFB58F883AD4BEBE975654F46D5 (RuntimeObject* ___store0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutManager_GetSourceNames_mEB27875BBB4D5FFB58F883AD4BEBE975654F46D5_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		// store.Clear();
		RuntimeObject* L_0 = ___store0;
		NullCheck(L_0);
		InterfaceActionInvoker0::Invoke(3 /* System.Void System.Collections.Generic.ICollection`1<System.String>::Clear() */, ICollection_1_t2E51991ADA605DB75870908AF6D7C3093DC3FCBA_il2cpp_TypeInfo_var, L_0);
		// var count = PluginEntry.ScanSharedObjects();
		int32_t L_1 = PluginEntry_ScanSharedObjects_mB97699799F4C6DA5E378A057061A9850BE380EC2(/*hidden argument*/NULL);
		V_0 = L_1;
		// for (var i = 0; i < count; i++)
		V_1 = 0;
		goto IL_0020;
	}

IL_0010:
	{
		// store.Add(PluginEntry.GetSharedObjectNameString(i));
		RuntimeObject* L_2 = ___store0;
		int32_t L_3 = V_1;
		String_t* L_4 = PluginEntry_GetSharedObjectNameString_m505B7C1C0C0C379708BC9047C68D84C3E118A4DF(L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		InterfaceActionInvoker1< String_t* >::Invoke(2 /* System.Void System.Collections.Generic.ICollection`1<System.String>::Add(!0) */, ICollection_1_t2E51991ADA605DB75870908AF6D7C3093DC3FCBA_il2cpp_TypeInfo_var, L_2, L_4);
		// for (var i = 0; i < count; i++)
		int32_t L_5 = V_1;
		V_1 = ((int32_t)il2cpp_codegen_add((int32_t)L_5, (int32_t)1));
	}

IL_0020:
	{
		// for (var i = 0; i < count; i++)
		int32_t L_6 = V_1;
		int32_t L_7 = V_0;
		if ((((int32_t)L_6) < ((int32_t)L_7)))
		{
			goto IL_0010;
		}
	}
	{
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.String Klak.Spout.SpoutReceiver::get_sourceName()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SpoutReceiver_get_sourceName_m1BA50606AE68DEBB867C68B938639509A6720729 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// get { return _sourceName; }
		String_t* L_0 = __this->get__sourceName_4();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutReceiver::set_sourceName(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_set_sourceName_mACA6329C485466980A2B9E9524D940921145B99D (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// if (_sourceName == value) return;
		String_t* L_0 = __this->get__sourceName_4();
		String_t* L_1 = ___value0;
		bool L_2 = String_op_Equality_m139F0E4195AE2F856019E63B241F36F016997FCE(L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_000f;
		}
	}
	{
		// if (_sourceName == value) return;
		return;
	}

IL_000f:
	{
		// _sourceName = value;
		String_t* L_3 = ___value0;
		__this->set__sourceName_4(L_3);
		// RequestReconnect();
		SpoutReceiver_RequestReconnect_mF1B1915CCDBEFB2DC2DDC4D96608EA926986F0C1(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// UnityEngine.RenderTexture Klak.Spout.SpoutReceiver::get_targetTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * SpoutReceiver_get_targetTexture_mFA6616A03DCF8820EBAE24CE91CA95052EA0B1C4 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// get { return _targetTexture; }
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = __this->get__targetTexture_5();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutReceiver::set_targetTexture(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_set_targetTexture_mB90906C0BE984C0BE981721C8E997CF723C0D79B (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___value0, const RuntimeMethod* method)
{
	{
		// set { _targetTexture = value; }
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___value0;
		__this->set__targetTexture_5(L_0);
		// set { _targetTexture = value; }
		return;
	}
}
// UnityEngine.Renderer Klak.Spout.SpoutReceiver::get_targetRenderer()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * SpoutReceiver_get_targetRenderer_m640E0F5A72E8098FC77DCCFFCD1C2322A305FA22 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// get { return _targetRenderer; }
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_0 = __this->get__targetRenderer_6();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutReceiver::set_targetRenderer(UnityEngine.Renderer)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_set_targetRenderer_m78C26EFC1CABFD752690836360E36C691D12CC3E (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * ___value0, const RuntimeMethod* method)
{
	{
		// set { _targetRenderer = value; }
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_0 = ___value0;
		__this->set__targetRenderer_6(L_0);
		// set { _targetRenderer = value; }
		return;
	}
}
// System.String Klak.Spout.SpoutReceiver::get_targetMaterialProperty()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* SpoutReceiver_get_targetMaterialProperty_mBC272356EE58983B487ABC6865832690458286A9 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// get { return _targetMaterialProperty; }
		String_t* L_0 = __this->get__targetMaterialProperty_7();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutReceiver::set_targetMaterialProperty(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_set_targetMaterialProperty_m43FA8B94659F4408DDC08FB7386C9D3F9276E288 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, String_t* ___value0, const RuntimeMethod* method)
{
	{
		// set { _targetMaterialProperty = value; }
		String_t* L_0 = ___value0;
		__this->set__targetMaterialProperty_7(L_0);
		// set { _targetMaterialProperty = value; }
		return;
	}
}
// UnityEngine.Texture Klak.Spout.SpoutReceiver::get_receivedTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// get { return _targetTexture != null ? _targetTexture : _receivedTexture; }
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = __this->get__targetTexture_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0015;
		}
	}
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = __this->get__receivedTexture_8();
		return L_2;
	}

IL_0015:
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_3 = __this->get__targetTexture_5();
		return L_3;
	}
}
// UnityEngine.Material Klak.Spout.SpoutReceiver::get_blitMaterial()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * SpoutReceiver_get_blitMaterial_m8F23BC2B3AD861CD9BE943B9986A3AD790BB81BE (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// get { return _blitMaterial; }
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = __this->get__blitMaterial_11();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutReceiver::set_blitMaterial(UnityEngine.Material)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_set_blitMaterial_mD801D9B60C14E4626DB14427D03E4B6645A482FB (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * ___value0, const RuntimeMethod* method)
{
	{
		// set { _blitMaterial = value; }
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = ___value0;
		__this->set__blitMaterial_11(L_0);
		// set { _blitMaterial = value; }
		return;
	}
}
// System.Void Klak.Spout.SpoutReceiver::RequestReconnect()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_RequestReconnect_mF1B1915CCDBEFB2DC2DDC4D96608EA926986F0C1 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// OnDisable();
		SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8(__this, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutReceiver::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutReceiver_OnDisable_m907CDAD980CCFC4D9C275743A64FE9CD386575F8_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_plugin != System.IntPtr.Zero)
		intptr_t L_0 = __this->get__plugin_9();
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// Util.IssuePluginEvent(PluginEntry.Event.Dispose, _plugin);
		intptr_t L_2 = __this->get__plugin_9();
		Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC(1, (intptr_t)L_2, /*hidden argument*/NULL);
		// _plugin = System.IntPtr.Zero;
		__this->set__plugin_9((intptr_t)(0));
	}

IL_0029:
	{
		// Util.Destroy(_sharedTexture);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_3 = __this->get__sharedTexture_10();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutReceiver::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_OnDestroy_m0A6FB3058763E2E62980F215153F143BE2DFAB4C (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		// Util.Destroy(_blitMaterial);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = __this->get__blitMaterial_11();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_0, /*hidden argument*/NULL);
		// Util.Destroy(_receivedTexture);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_1 = __this->get__receivedTexture_8();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_1, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutReceiver::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver_Update_mEBF46B4FD09CECE23A717F96322B1B1844A09A43 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutReceiver_Update_mEBF46B4FD09CECE23A717F96322B1B1844A09A43_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		// if (_plugin != System.IntPtr.Zero && !PluginEntry.CheckValid(_plugin))
		intptr_t L_0 = __this->get__plugin_9();
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0036;
		}
	}
	{
		intptr_t L_2 = __this->get__plugin_9();
		bool L_3 = PluginEntry_CheckValid_m96F923E00F14F4F21379099E037DB35880C99F80((intptr_t)L_2, /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_0036;
		}
	}
	{
		// Util.IssuePluginEvent(PluginEntry.Event.Dispose, _plugin);
		intptr_t L_4 = __this->get__plugin_9();
		Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC(1, (intptr_t)L_4, /*hidden argument*/NULL);
		// _plugin = System.IntPtr.Zero;
		__this->set__plugin_9((intptr_t)(0));
	}

IL_0036:
	{
		// if (_plugin == System.IntPtr.Zero)
		intptr_t L_5 = __this->get__plugin_9();
		bool L_6 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_5, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006c;
		}
	}
	{
		// _plugin = PluginEntry.CreateReceiver(_sourceName);
		String_t* L_7 = __this->get__sourceName_4();
		intptr_t L_8 = PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664_inline(L_7, /*hidden argument*/NULL);
		__this->set__plugin_9((intptr_t)L_8);
		// if (_plugin == System.IntPtr.Zero) return; // Spout may not be ready.
		intptr_t L_9 = __this->get__plugin_9();
		bool L_10 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_9, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_10)
		{
			goto IL_006c;
		}
	}
	{
		// if (_plugin == System.IntPtr.Zero) return; // Spout may not be ready.
		return;
	}

IL_006c:
	{
		// Util.IssuePluginEvent(PluginEntry.Event.Update, _plugin);
		intptr_t L_11 = __this->get__plugin_9();
		Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC(0, (intptr_t)L_11, /*hidden argument*/NULL);
		// var ptr = PluginEntry.GetTexturePointer(_plugin);
		intptr_t L_12 = __this->get__plugin_9();
		intptr_t L_13 = PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_inline((intptr_t)L_12, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_13;
		// var width = PluginEntry.GetTextureWidth(_plugin);
		intptr_t L_14 = __this->get__plugin_9();
		int32_t L_15 = PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF((intptr_t)L_14, /*hidden argument*/NULL);
		V_1 = L_15;
		// var height = PluginEntry.GetTextureHeight(_plugin);
		intptr_t L_16 = __this->get__plugin_9();
		int32_t L_17 = PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB((intptr_t)L_16, /*hidden argument*/NULL);
		V_2 = L_17;
		// if (_sharedTexture != null)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_18 = __this->get__sharedTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_19 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_18, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00e4;
		}
	}
	{
		// if (ptr != _sharedTexture.GetNativeTexturePtr() ||
		//     width != _sharedTexture.width ||
		//     height != _sharedTexture.height)
		intptr_t L_20 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_21 = __this->get__sharedTexture_10();
		NullCheck(L_21);
		intptr_t L_22 = Texture_GetNativeTexturePtr_mB29F70DB525E825A4A3BB908F90C06F3171E92FD(L_21, /*hidden argument*/NULL);
		bool L_23 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_20, (intptr_t)L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_24 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_25 = __this->get__sharedTexture_10();
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_25);
		if ((!(((uint32_t)L_24) == ((uint32_t)L_26))))
		{
			goto IL_00d9;
		}
	}
	{
		int32_t L_27 = V_2;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_28 = __this->get__sharedTexture_10();
		NullCheck(L_28);
		int32_t L_29 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_28);
		if ((((int32_t)L_27) == ((int32_t)L_29)))
		{
			goto IL_00e4;
		}
	}

IL_00d9:
	{
		// Util.Destroy(_sharedTexture);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_30 = __this->get__sharedTexture_10();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_30, /*hidden argument*/NULL);
	}

IL_00e4:
	{
		// if (_sharedTexture == null && ptr != System.IntPtr.Zero)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_31 = __this->get__sharedTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_32 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_31, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0128;
		}
	}
	{
		intptr_t L_33 = V_0;
		bool L_34 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_33, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_34)
		{
			goto IL_0128;
		}
	}
	{
		// _sharedTexture = Texture2D.CreateExternalTexture(
		//     width, height, TextureFormat.ARGB32, false, false, ptr
		// );
		int32_t L_35 = V_1;
		int32_t L_36 = V_2;
		intptr_t L_37 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_38 = Texture2D_CreateExternalTexture_m292548AA116AA5176E8CA03AFBBC088E3B698DE4(L_35, L_36, 5, (bool)0, (bool)0, (intptr_t)L_37, /*hidden argument*/NULL);
		__this->set__sharedTexture_10(L_38);
		// _sharedTexture.hideFlags = HideFlags.DontSave;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_39 = __this->get__sharedTexture_10();
		NullCheck(L_39);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_39, ((int32_t)52), /*hidden argument*/NULL);
		// Util.Destroy(_receivedTexture);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_40 = __this->get__receivedTexture_8();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_40, /*hidden argument*/NULL);
	}

IL_0128:
	{
		// if (_sharedTexture != null)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_41 = __this->get__sharedTexture_10();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_42 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_41, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01e6;
		}
	}
	{
		// if (_blitMaterial == null)
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_43 = __this->get__blitMaterial_11();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_44 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_43, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_0169;
		}
	}
	{
		// _blitMaterial = new Material(Shader.Find("Hidden/Spout/Blit"));
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_45 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral34DCB24BD70D26BBFE5B0BEA0B614732307E8564, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_46 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_46, L_45, /*hidden argument*/NULL);
		__this->set__blitMaterial_11(L_46);
		// _blitMaterial.hideFlags = HideFlags.DontSave;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_47 = __this->get__blitMaterial_11();
		NullCheck(L_47);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_47, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_0169:
	{
		// if (_targetTexture != null)
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_48 = __this->get__targetTexture_5();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_49 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_48, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0191;
		}
	}
	{
		// Graphics.Blit(_sharedTexture, _targetTexture, _blitMaterial, 1);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_50 = __this->get__sharedTexture_10();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_51 = __this->get__targetTexture_5();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_52 = __this->get__blitMaterial_11();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_50, L_51, L_52, 1, /*hidden argument*/NULL);
		// }
		goto IL_01e6;
	}

IL_0191:
	{
		// if (_receivedTexture == null)
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_53 = __this->get__receivedTexture_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_54 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_53, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_54)
		{
			goto IL_01ce;
		}
	}
	{
		// _receivedTexture = new RenderTexture
		//     (_sharedTexture.width, _sharedTexture.height, 0);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_55 = __this->get__sharedTexture_10();
		NullCheck(L_55);
		int32_t L_56 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_55);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_57 = __this->get__sharedTexture_10();
		NullCheck(L_57);
		int32_t L_58 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_57);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_59 = (RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 *)il2cpp_codegen_object_new(RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6_il2cpp_TypeInfo_var);
		RenderTexture__ctor_mB54A3ABBD56D38AB762D0AB8B789E2771BC42A7D(L_59, L_56, L_58, 0, /*hidden argument*/NULL);
		__this->set__receivedTexture_8(L_59);
		// _receivedTexture.hideFlags = HideFlags.DontSave;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_60 = __this->get__receivedTexture_8();
		NullCheck(L_60);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_60, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_01ce:
	{
		// Graphics.Blit(_sharedTexture, _receivedTexture, _blitMaterial, 1);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_61 = __this->get__sharedTexture_10();
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_62 = __this->get__receivedTexture_8();
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_63 = __this->get__blitMaterial_11();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_61, L_62, L_63, 1, /*hidden argument*/NULL);
	}

IL_01e6:
	{
		// if (_targetRenderer != null && receivedTexture != null)
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_64 = __this->get__targetRenderer_6();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_65 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_64, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_65)
		{
			goto IL_024e;
		}
	}
	{
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_66 = SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_67 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_66, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_67)
		{
			goto IL_024e;
		}
	}
	{
		// if (_propertyBlock == null)
		MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * L_68 = __this->get__propertyBlock_12();
		if (L_68)
		{
			goto IL_0215;
		}
	}
	{
		// _propertyBlock = new MaterialPropertyBlock();
		MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * L_69 = (MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 *)il2cpp_codegen_object_new(MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13_il2cpp_TypeInfo_var);
		MaterialPropertyBlock__ctor_m9055A333A5DA8CC70CC3D837BD59B54C313D39F3(L_69, /*hidden argument*/NULL);
		__this->set__propertyBlock_12(L_69);
	}

IL_0215:
	{
		// _targetRenderer.GetPropertyBlock(_propertyBlock);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_70 = __this->get__targetRenderer_6();
		MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * L_71 = __this->get__propertyBlock_12();
		NullCheck(L_70);
		Renderer_GetPropertyBlock_mCD279F8A7CEB56ABB9EF9D150103FB1C4FB3CE8C(L_70, L_71, /*hidden argument*/NULL);
		// _propertyBlock.SetTexture(_targetMaterialProperty, receivedTexture);
		MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * L_72 = __this->get__propertyBlock_12();
		String_t* L_73 = __this->get__targetMaterialProperty_7();
		Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4 * L_74 = SpoutReceiver_get_receivedTexture_mA9DCA0C5FE3748FBE6F9F824921A7AE31FB50488(__this, /*hidden argument*/NULL);
		NullCheck(L_72);
		MaterialPropertyBlock_SetTexture_m1F86AED47A6CDD6390B28746DDE87E15C4FDA763(L_72, L_73, L_74, /*hidden argument*/NULL);
		// _targetRenderer.SetPropertyBlock(_propertyBlock);
		Renderer_t0556D67DD582620D1F495627EDE30D03284151F4 * L_75 = __this->get__targetRenderer_6();
		MaterialPropertyBlock_t72A481768111C6F11DCDCD44F0C7F99F1CA79E13 * L_76 = __this->get__propertyBlock_12();
		NullCheck(L_75);
		Renderer_SetPropertyBlock_m1B999AB9B425587EF44CF1CB83CDE0A191F76C40(L_75, L_76, /*hidden argument*/NULL);
	}

IL_024e:
	{
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutReceiver::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutReceiver__ctor_m13B4DA3C9835B8B09B0CCDE94997813906D8D696 (SpoutReceiver_tBBBBD330BBD2BC795F757DA455A97BA9797D9919 * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// UnityEngine.RenderTexture Klak.Spout.SpoutSender::get_sourceTexture()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * SpoutSender_get_sourceTexture_m56736F05526304452202E5A2BFFE98B53F77FD60 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	{
		// get { return _sourceTexture; }
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = __this->get__sourceTexture_4();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutSender::set_sourceTexture(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_set_sourceTexture_m9B27154F9B1132CCD233D59EA1FF89E5AE45B703 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___value0, const RuntimeMethod* method)
{
	{
		// set { _sourceTexture = value; }
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___value0;
		__this->set__sourceTexture_4(L_0);
		// set { _sourceTexture = value; }
		return;
	}
}
// System.Boolean Klak.Spout.SpoutSender::get_alphaSupport()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool SpoutSender_get_alphaSupport_mDEE51AE2D5599F42FE05B3E378DB310CF501BF3B (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	{
		// get { return _alphaSupport; }
		bool L_0 = __this->get__alphaSupport_5();
		return L_0;
	}
}
// System.Void Klak.Spout.SpoutSender::set_alphaSupport(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_set_alphaSupport_mE5A0A876DB73B29746016D56DA3912CD919A5134 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		// set { _alphaSupport = value; }
		bool L_0 = ___value0;
		__this->set__alphaSupport_5(L_0);
		// set { _alphaSupport = value; }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::SendRenderTexture(UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * V_1 = NULL;
	String_t* G_B11_0 = NULL;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * G_B11_1 = NULL;
	String_t* G_B10_0 = NULL;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * G_B10_1 = NULL;
	int32_t G_B12_0 = 0;
	String_t* G_B12_1 = NULL;
	Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * G_B12_2 = NULL;
	{
		// if (_plugin == System.IntPtr.Zero)
		intptr_t L_0 = __this->get__plugin_6();
		bool L_1 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0042;
		}
	}
	{
		// _plugin = PluginEntry.CreateSender(name, source.width, source.height);
		String_t* L_2 = Object_get_name_mA2D400141CB3C991C87A2556429781DE961A83CE(__this, /*hidden argument*/NULL);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_3 = ___source0;
		NullCheck(L_3);
		int32_t L_4 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_3);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_5 = ___source0;
		NullCheck(L_5);
		int32_t L_6 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_5);
		intptr_t L_7 = PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902_inline(L_2, L_4, L_6, /*hidden argument*/NULL);
		__this->set__plugin_6((intptr_t)L_7);
		// if (_plugin == System.IntPtr.Zero) return; // Spout may not be ready.
		intptr_t L_8 = __this->get__plugin_6();
		bool L_9 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_8, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0042;
		}
	}
	{
		// if (_plugin == System.IntPtr.Zero) return; // Spout may not be ready.
		return;
	}

IL_0042:
	{
		// if (_sharedTexture == null)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_10 = __this->get__sharedTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_11 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_10, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_009b;
		}
	}
	{
		// var ptr = PluginEntry.GetTexturePointer(_plugin);
		intptr_t L_12 = __this->get__plugin_6();
		intptr_t L_13 = PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_inline((intptr_t)L_12, /*hidden argument*/NULL);
		V_0 = (intptr_t)L_13;
		// if (ptr != System.IntPtr.Zero)
		intptr_t L_14 = V_0;
		bool L_15 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_14, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_15)
		{
			goto IL_009b;
		}
	}
	{
		// _sharedTexture = Texture2D.CreateExternalTexture(
		//     PluginEntry.GetTextureWidth(_plugin),
		//     PluginEntry.GetTextureHeight(_plugin),
		//     TextureFormat.ARGB32, false, false, ptr
		// );
		intptr_t L_16 = __this->get__plugin_6();
		int32_t L_17 = PluginEntry_GetTextureWidth_m94161E4F1BA270D1D6AA53AED3A31B6A9D40F6BF((intptr_t)L_16, /*hidden argument*/NULL);
		intptr_t L_18 = __this->get__plugin_6();
		int32_t L_19 = PluginEntry_GetTextureHeight_m50725A97F8F4658985C91D3D40326C4ABDEC8DAB((intptr_t)L_18, /*hidden argument*/NULL);
		intptr_t L_20 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_21 = Texture2D_CreateExternalTexture_m292548AA116AA5176E8CA03AFBBC088E3B698DE4(L_17, L_19, 5, (bool)0, (bool)0, (intptr_t)L_20, /*hidden argument*/NULL);
		__this->set__sharedTexture_7(L_21);
		// _sharedTexture.hideFlags = HideFlags.DontSave;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_22 = __this->get__sharedTexture_7();
		NullCheck(L_22);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_22, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_009b:
	{
		// if (_sharedTexture != null)
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_23 = __this->get__sharedTexture_7();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_24 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_23, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_24)
		{
			goto IL_0135;
		}
	}
	{
		// if (_blitMaterial == null)
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_25 = __this->get__blitMaterial_8();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_26 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_25, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_00dc;
		}
	}
	{
		// _blitMaterial = new Material(Shader.Find("Hidden/Spout/Blit"));
		Shader_tE2731FF351B74AB4186897484FB01E000C1160CA * L_27 = Shader_Find_m755654AA68D1C663A3E20A10E00CDC10F96C962B(_stringLiteral34DCB24BD70D26BBFE5B0BEA0B614732307E8564, /*hidden argument*/NULL);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_28 = (Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 *)il2cpp_codegen_object_new(Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598_il2cpp_TypeInfo_var);
		Material__ctor_m81E76B5C1316004F25D4FE9CEC0E78A7428DABA8(L_28, L_27, /*hidden argument*/NULL);
		__this->set__blitMaterial_8(L_28);
		// _blitMaterial.hideFlags = HideFlags.DontSave;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_29 = __this->get__blitMaterial_8();
		NullCheck(L_29);
		Object_set_hideFlags_mB0B45A19A5871EF407D7B09E0EB76003496BA4F0(L_29, ((int32_t)52), /*hidden argument*/NULL);
	}

IL_00dc:
	{
		// _blitMaterial.SetFloat("_ClearAlpha", _alphaSupport ? 0 : 1);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_30 = __this->get__blitMaterial_8();
		bool L_31 = __this->get__alphaSupport_5();
		G_B10_0 = _stringLiteral4D0B1ECDF7F63A17B25AB0B68A8A0E8F00A3E96B;
		G_B10_1 = L_30;
		if (L_31)
		{
			G_B11_0 = _stringLiteral4D0B1ECDF7F63A17B25AB0B68A8A0E8F00A3E96B;
			G_B11_1 = L_30;
			goto IL_00f2;
		}
	}
	{
		G_B12_0 = 1;
		G_B12_1 = G_B10_0;
		G_B12_2 = G_B10_1;
		goto IL_00f3;
	}

IL_00f2:
	{
		G_B12_0 = 0;
		G_B12_1 = G_B11_0;
		G_B12_2 = G_B11_1;
	}

IL_00f3:
	{
		NullCheck(G_B12_2);
		Material_SetFloat_m4B7D3FAA00D20BCB3C487E72B7E4B2691D5ECAD2(G_B12_2, G_B12_1, (((float)((float)G_B12_0))), /*hidden argument*/NULL);
		// var tempRT = RenderTexture.GetTemporary
		//     (_sharedTexture.width, _sharedTexture.height);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_32 = __this->get__sharedTexture_7();
		NullCheck(L_32);
		int32_t L_33 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_32);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_34 = __this->get__sharedTexture_7();
		NullCheck(L_34);
		int32_t L_35 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_34);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_36 = RenderTexture_GetTemporary_m02547EA84CDEB9038FCF1EEDD283AF24114C735F(L_33, L_35, /*hidden argument*/NULL);
		V_1 = L_36;
		// Graphics.Blit(source, tempRT, _blitMaterial, 0);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_37 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_38 = V_1;
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_39 = __this->get__blitMaterial_8();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_m9614A3F7FBE034123F456D0294946BDE9E4F78D8(L_37, L_38, L_39, 0, /*hidden argument*/NULL);
		// Graphics.CopyTexture(tempRT, _sharedTexture);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_40 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_41 = __this->get__sharedTexture_7();
		Graphics_CopyTexture_m06B5C14FA9E91CC78D3901898D712ADEB0C0B8A5(L_40, L_41, /*hidden argument*/NULL);
		// RenderTexture.ReleaseTemporary(tempRT);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_42 = V_1;
		RenderTexture_ReleaseTemporary_mFBA6F18138965049AA901D62A0080B1A087A38EA(L_42, /*hidden argument*/NULL);
	}

IL_0135:
	{
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::OnDisable()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_OnDisable_mA68599D3A9C29D76566081450DF5F16CFEB17575 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutSender_OnDisable_mA68599D3A9C29D76566081450DF5F16CFEB17575_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_plugin != System.IntPtr.Zero)
		intptr_t L_0 = __this->get__plugin_6();
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0029;
		}
	}
	{
		// Util.IssuePluginEvent(PluginEntry.Event.Dispose, _plugin);
		intptr_t L_2 = __this->get__plugin_6();
		Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC(1, (intptr_t)L_2, /*hidden argument*/NULL);
		// _plugin = System.IntPtr.Zero;
		__this->set__plugin_6((intptr_t)(0));
	}

IL_0029:
	{
		// Util.Destroy(_sharedTexture);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_3 = __this->get__sharedTexture_7();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_3, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::OnDestroy()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_OnDestroy_m2DA609828A74433E07D76E33EFD8645C120F5762 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	{
		// Util.Destroy(_blitMaterial);
		Material_tF7DB3BF0C24DEC2FE0CB51E5DF5053D5223C8598 * L_0 = __this->get__blitMaterial_8();
		Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD(L_0, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::Update()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_Update_mF440263DB95F7A38D4141F89203E0D72036876CB (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutSender_Update_mF440263DB95F7A38D4141F89203E0D72036876CB_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_plugin != System.IntPtr.Zero)
		intptr_t L_0 = __this->get__plugin_6();
		bool L_1 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_0, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001e;
		}
	}
	{
		// Util.IssuePluginEvent(PluginEntry.Event.Update, _plugin);
		intptr_t L_2 = __this->get__plugin_6();
		Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC(0, (intptr_t)L_2, /*hidden argument*/NULL);
	}

IL_001e:
	{
		// if (GetComponent<Camera>() == null && _sourceTexture != null)
		Camera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34 * L_3 = Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E(__this, /*hidden argument*/Component_GetComponent_TisCamera_t48B2B9ECB3CE6108A98BF949A1CECF0FE3421F34_mB090F51A34716700C0F4F1B08F9330C6F503DB9E_RuntimeMethod_var);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_4 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_3, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0046;
		}
	}
	{
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_5 = __this->get__sourceTexture_4();
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_6 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_5, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0046;
		}
	}
	{
		// SendRenderTexture(_sourceTexture);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_7 = __this->get__sourceTexture_4();
		SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D(__this, L_7, /*hidden argument*/NULL);
	}

IL_0046:
	{
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender_OnRenderImage_m22518EF3DF63D7D46B3F51DE4FA606D1C97DEA1E (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___source0, RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * ___destination1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (SpoutSender_OnRenderImage_m22518EF3DF63D7D46B3F51DE4FA606D1C97DEA1E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// SendRenderTexture(source);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_0 = ___source0;
		SpoutSender_SendRenderTexture_mE751AA5F5257923E3548E73BE88714473F0F168D(__this, L_0, /*hidden argument*/NULL);
		// Graphics.Blit(source, destination);
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_1 = ___source0;
		RenderTexture_tBC47D853E3DA6511CD6C49DBF78D47B890FCD2F6 * L_2 = ___destination1;
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_Blit_mB042EC04307A5617038DA4210DE7BA4B3E529113(L_1, L_2, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.SpoutSender::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SpoutSender__ctor_mA22A9CEE5AB1261B210B426BB9116652054E55D9 (SpoutSender_tACBC2BB33DDC5AF854554FCBAE7105659F34EEAA * __this, const RuntimeMethod* method)
{
	{
		MonoBehaviour__ctor_mEAEC84B222C60319D593E456D769B3311DFCEF97(__this, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void Klak.Spout.Util::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_Destroy_m2C7B56BCE4B29D5A5BF333C1EF4A1175138A06AD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (obj == null) return;
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_0 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_1 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_0, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000a;
		}
	}
	{
		// if (obj == null) return;
		return;
	}

IL_000a:
	{
		// if (Application.isPlaying)
		bool L_2 = Application_get_isPlaying_mF43B519662E7433DD90D883E5AE22EC3CFB65CA5(/*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		// Object.Destroy(obj);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_3 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_3, /*hidden argument*/NULL);
		return;
	}

IL_0018:
	{
		// Object.DestroyImmediate(obj);
		Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * L_4 = ___obj0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_DestroyImmediate_mF6F4415EF22249D6E650FAA40E403283F19B7446(L_4, /*hidden argument*/NULL);
		// }
		return;
	}
}
// System.Void Klak.Spout.Util::IssuePluginEvent(Klak.Spout.PluginEntry/Event,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC (int32_t ___pluginEvent0, intptr_t ___ptr1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (Util_IssuePluginEvent_m82EB10F343310E9043FBFE46E139748554B22EAC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// if (_commandBuffer == null) _commandBuffer = new CommandBuffer();
		CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * L_0 = ((Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields*)il2cpp_codegen_static_fields_for(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var))->get__commandBuffer_0();
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		// if (_commandBuffer == null) _commandBuffer = new CommandBuffer();
		CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * L_1 = (CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD *)il2cpp_codegen_object_new(CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD_il2cpp_TypeInfo_var);
		CommandBuffer__ctor_m4394F7E41C9BB751E382A8CAFA38B19F69E03890(L_1, /*hidden argument*/NULL);
		((Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields*)il2cpp_codegen_static_fields_for(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var))->set__commandBuffer_0(L_1);
	}

IL_0011:
	{
		// _commandBuffer.IssuePluginEventAndData(
		//     PluginEntry.GetRenderEventFunc(), (int)pluginEvent, ptr
		// );
		CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * L_2 = ((Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields*)il2cpp_codegen_static_fields_for(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var))->get__commandBuffer_0();
		intptr_t L_3 = PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E_inline(/*hidden argument*/NULL);
		int32_t L_4 = ___pluginEvent0;
		intptr_t L_5 = ___ptr1;
		NullCheck(L_2);
		CommandBuffer_IssuePluginEventAndData_m613D8131D999BE26CBB9D380D08EDFAEFD388673(L_2, (intptr_t)L_3, L_4, (intptr_t)L_5, /*hidden argument*/NULL);
		// Graphics.ExecuteCommandBuffer(_commandBuffer);
		CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * L_6 = ((Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields*)il2cpp_codegen_static_fields_for(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var))->get__commandBuffer_0();
		IL2CPP_RUNTIME_CLASS_INIT(Graphics_t6FB7A5D4561F3AB3C34BF334BB0BD8061BE763B1_il2cpp_TypeInfo_var);
		Graphics_ExecuteCommandBuffer_m99851E297195473AE9C72FB2CF93DDB71E67EEC0(L_6, /*hidden argument*/NULL);
		// _commandBuffer.Clear();
		CommandBuffer_t70BF7D9D84C2AFA83559B45FAD5BEDA73DA617DD * L_7 = ((Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_StaticFields*)il2cpp_codegen_static_fields_for(Util_tC4851016594A6DDF7E64AD9B0B1B0A9E97FC2D11_il2cpp_TypeInfo_var))->get__commandBuffer_0();
		NullCheck(L_7);
		CommandBuffer_Clear_mCE65F50CF8DBEE5543EFE80CA9FEDA780F76F430(L_7, /*hidden argument*/NULL);
		// }
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664_inline (String_t* ___name0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_CreateReceiver_mC48204A0887B4F645902FD9B5A7E3D4C4E96F664Klak_Spout_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1_inline (intptr_t ___ptr0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_GetTexturePointer_m107BEBF5EC38796DD77A553D30E3F147D20467B1Klak_Spout_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902_inline (String_t* ___name0, int32_t ___width1, int32_t ___height2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_CreateSender_m531B606D394A006A540608D4B16A8FE447D9A902Klak_Spout_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR intptr_t PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1E_inline (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (PluginEntry_GetRenderEventFunc_mE1953C2630A05DBE61B490255A12380B6CFFED1EKlak_Spout_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		// { return System.IntPtr.Zero; }
		return (intptr_t)(0);
	}
}
