﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"

template <typename R>
struct VirtFuncInvoker0
{
	typedef R (*Func)(void*, const RuntimeMethod*);

	static inline R Invoke (Il2CppMethodSlot slot, RuntimeObject* obj)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		return ((Func)invokeData.methodPtr)(obj, invokeData.method);
	}
};
template <typename T1>
struct VirtActionInvoker1
{
	typedef void (*Action)(void*, T1, const RuntimeMethod*);

	static inline void Invoke (Il2CppMethodSlot slot, RuntimeObject* obj, T1 p1)
	{
		const VirtualInvokeData& invokeData = il2cpp_codegen_get_virtual_invoke_data(slot, obj);
		((Action)invokeData.methodPtr)(obj, p1, invokeData.method);
	}
};

// FMTurboJpegWrapper.DecompressedImage
struct DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32;
// FMTurboJpegWrapper.TJCompressor
struct TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF;
// FMTurboJpegWrapper.TJDecompressor
struct TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C;
// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA;
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.Dictionary`2/Entry<FMTurboJpegWrapper.TJPixelFormat,System.Int32>[]
struct EntryU5BU5D_tBE45EB94030719BFD638516678390C648E7C617F;
// System.Collections.Generic.Dictionary`2/Entry<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>[]
struct EntryU5BU5D_t884BAB29D7328E553A2010B70FD1D7973D62BA32;
// System.Collections.Generic.Dictionary`2/KeyCollection<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct KeyCollection_tFE0A08F6E3B00BBB64C76240F1B83DCC51276047;
// System.Collections.Generic.Dictionary`2/KeyCollection<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct KeyCollection_t0C1D3BE91F3644A90384001C2525F30AD46140A8;
// System.Collections.Generic.Dictionary`2/ValueCollection<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct ValueCollection_tC1D4D2F8A3523C3F0E36AAA5E6A1EE1DC3F38F33;
// System.Collections.Generic.Dictionary`2/ValueCollection<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct ValueCollection_t7E2BE0D0A1A453097FA708E1AC528386A068B733;
// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A;
// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_t7BECB1C7491E20BC1F2B7F6269632191F986ED30;
// System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>
struct Dictionary_2_tF12CF1D3D699D81529FCF73688C5C19C0C4A8A19;
// System.Collections.Generic.IEqualityComparer`1<FMTurboJpegWrapper.TJPixelFormat>
struct IEqualityComparer_1_t76BA0A76FA76CCBC946873B0DC5FA2002685DE05;
// System.Collections.Generic.IEqualityComparer`1<FMTurboJpegWrapper.TJSubsamplingOption>
struct IEqualityComparer_1_t7CE9B32B029F5172C42CBEF92DC1D6D3CC79DC01;
// System.Collections.IDictionary
struct IDictionary_t1BD5C1546718A374EA8122FBD6C6EE45331E8CE7;
// System.Diagnostics.StackTrace[]
struct StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196;
// System.Int32[]
struct Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83;
// System.IntPtr[]
struct IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD;
// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1;
// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010;
// System.ObjectDisposedException
struct ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A;
// System.Runtime.Serialization.SafeSerializationManager
struct SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770;
// System.String
struct String_t;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0;
// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C;

IL2CPP_EXTERN_C RuntimeClass* ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* IntPtr_t_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Marshal_tC795CE9CC2FFBA41EDB1AC1C0FEC04607DFA8A40_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* RuntimeObject_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJPixelFormat_tC481543CB0361DE9AA1F0207E1B8CFEAF456CB79_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TJSubsamplingOption_tCAB3CAA7FCB54ACDC4F8BC62AA1C0655B319D4B1_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C RuntimeClass* TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var;
IL2CPP_EXTERN_C String_t* _stringLiteral0EC278C441CF646394EA7CD22D6CAADCAD2DE9CD;
IL2CPP_EXTERN_C String_t* _stringLiteral6594273630B6D2176D3E914597BC3109539BBA40;
IL2CPP_EXTERN_C String_t* _stringLiteral9237B62CFD61666EFD00565FAD7965E2FBAAE16B;
IL2CPP_EXTERN_C String_t* _stringLiteralC2543FFF3BFA6F144C2F06A7DE6CD10C0B650CAE;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m0831B7243D8A1B9DFF515B83573FB3A13C3743C4_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2__ctor_m975E6ED49D776E4C04851FA34DFABF55A1EA395F_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B_RuntimeMethod_var;
IL2CPP_EXTERN_C const RuntimeMethod* TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402_RuntimeMethod_var;
IL2CPP_EXTERN_C const uint32_t FMExtensionMethods_FMEncodeToJPG_mEA8B88031312980B401A7B162B793EB50AFE5A05_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FMExtensionMethods_FMJPGToRawTextureData_mC207C0F855721B7FCE13135C9DF893528F3FFAEC_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FMExtensionMethods_FMLoadJPG_m34CC978D5E7FA1A6E8143998C2E94C5F09982D2D_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FMExtensionMethods_FMMatchResolution_m849F96713529AB6091F6D6D6FE5F433F2070E63E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t FMExtensionMethods_FMRawTextureDataToJPG_mABD75BC62916FC0962EEAB016ABFFCEAEDCF62F1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJCompressor_Dispose_m0CD2BEEDF78D19BB1B9C9E99B474A4FAAB117913_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Dispose_m168AF7BF325AF4116571288FED73411FAC783242_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402_MetadataUsageId;
IL2CPP_EXTERN_C const uint32_t TurboJpegImport__cctor_m14EDB1B829C6218B7400940F71BD02D5323240CD_MetadataUsageId;
struct Exception_t_marshaled_com;
struct Exception_t_marshaled_pinvoke;

struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821;

IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// <Module>
struct U3CModuleU3E_tD197EFF39973682E4AB7830F11BDB975FFD9843D 
{
public:

public:
};


// System.Object


// FMExtensionMethods
struct FMExtensionMethods_t3A0D9AC762B7CE900F1AE05EF79B7EBF58948867  : public RuntimeObject
{
public:

public:
};


// FMTurboJpegWrapper.TJUtils
struct TJUtils_t0B150F308D4119F598D9DFF89E5EB9B150FE215E  : public RuntimeObject
{
public:

public:
};


// FMTurboJpegWrapper.TurboJpegImport
struct TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C  : public RuntimeObject
{
public:

public:
};

struct TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields
{
public:
	// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32> FMTurboJpegWrapper.TurboJpegImport::PixelSizes
	Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * ___PixelSizes_0;
	// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize> FMTurboJpegWrapper.TurboJpegImport::MCUSizes
	Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * ___MCUSizes_1;
	// System.Boolean FMTurboJpegWrapper.TurboJpegImport::_LibraryFound
	bool ____LibraryFound_2;

public:
	inline static int32_t get_offset_of_PixelSizes_0() { return static_cast<int32_t>(offsetof(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields, ___PixelSizes_0)); }
	inline Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * get_PixelSizes_0() const { return ___PixelSizes_0; }
	inline Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A ** get_address_of_PixelSizes_0() { return &___PixelSizes_0; }
	inline void set_PixelSizes_0(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * value)
	{
		___PixelSizes_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___PixelSizes_0), (void*)value);
	}

	inline static int32_t get_offset_of_MCUSizes_1() { return static_cast<int32_t>(offsetof(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields, ___MCUSizes_1)); }
	inline Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * get_MCUSizes_1() const { return ___MCUSizes_1; }
	inline Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 ** get_address_of_MCUSizes_1() { return &___MCUSizes_1; }
	inline void set_MCUSizes_1(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * value)
	{
		___MCUSizes_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___MCUSizes_1), (void*)value);
	}

	inline static int32_t get_offset_of__LibraryFound_2() { return static_cast<int32_t>(offsetof(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields, ____LibraryFound_2)); }
	inline bool get__LibraryFound_2() const { return ____LibraryFound_2; }
	inline bool* get_address_of__LibraryFound_2() { return &____LibraryFound_2; }
	inline void set__LibraryFound_2(bool value)
	{
		____LibraryFound_2 = value;
	}
};

struct Il2CppArrayBounds;

// System.Array


// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>
struct Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_tBE45EB94030719BFD638516678390C648E7C617F* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_tFE0A08F6E3B00BBB64C76240F1B83DCC51276047 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_tC1D4D2F8A3523C3F0E36AAA5E6A1EE1DC3F38F33 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___entries_1)); }
	inline EntryU5BU5D_tBE45EB94030719BFD638516678390C648E7C617F* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_tBE45EB94030719BFD638516678390C648E7C617F** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_tBE45EB94030719BFD638516678390C648E7C617F* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___keys_7)); }
	inline KeyCollection_tFE0A08F6E3B00BBB64C76240F1B83DCC51276047 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_tFE0A08F6E3B00BBB64C76240F1B83DCC51276047 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_tFE0A08F6E3B00BBB64C76240F1B83DCC51276047 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ___values_8)); }
	inline ValueCollection_tC1D4D2F8A3523C3F0E36AAA5E6A1EE1DC3F38F33 * get_values_8() const { return ___values_8; }
	inline ValueCollection_tC1D4D2F8A3523C3F0E36AAA5E6A1EE1DC3F38F33 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_tC1D4D2F8A3523C3F0E36AAA5E6A1EE1DC3F38F33 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>
struct Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472  : public RuntimeObject
{
public:
	// System.Int32[] System.Collections.Generic.Dictionary`2::buckets
	Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* ___buckets_0;
	// System.Collections.Generic.Dictionary`2/Entry<TKey,TValue>[] System.Collections.Generic.Dictionary`2::entries
	EntryU5BU5D_t884BAB29D7328E553A2010B70FD1D7973D62BA32* ___entries_1;
	// System.Int32 System.Collections.Generic.Dictionary`2::count
	int32_t ___count_2;
	// System.Int32 System.Collections.Generic.Dictionary`2::version
	int32_t ___version_3;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeList
	int32_t ___freeList_4;
	// System.Int32 System.Collections.Generic.Dictionary`2::freeCount
	int32_t ___freeCount_5;
	// System.Collections.Generic.IEqualityComparer`1<TKey> System.Collections.Generic.Dictionary`2::comparer
	RuntimeObject* ___comparer_6;
	// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::keys
	KeyCollection_t0C1D3BE91F3644A90384001C2525F30AD46140A8 * ___keys_7;
	// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2::values
	ValueCollection_t7E2BE0D0A1A453097FA708E1AC528386A068B733 * ___values_8;
	// System.Object System.Collections.Generic.Dictionary`2::_syncRoot
	RuntimeObject * ____syncRoot_9;

public:
	inline static int32_t get_offset_of_buckets_0() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___buckets_0)); }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* get_buckets_0() const { return ___buckets_0; }
	inline Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83** get_address_of_buckets_0() { return &___buckets_0; }
	inline void set_buckets_0(Int32U5BU5D_t2B9E4FDDDB9F0A00EC0AC631BA2DA915EB1ECF83* value)
	{
		___buckets_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___buckets_0), (void*)value);
	}

	inline static int32_t get_offset_of_entries_1() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___entries_1)); }
	inline EntryU5BU5D_t884BAB29D7328E553A2010B70FD1D7973D62BA32* get_entries_1() const { return ___entries_1; }
	inline EntryU5BU5D_t884BAB29D7328E553A2010B70FD1D7973D62BA32** get_address_of_entries_1() { return &___entries_1; }
	inline void set_entries_1(EntryU5BU5D_t884BAB29D7328E553A2010B70FD1D7973D62BA32* value)
	{
		___entries_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___entries_1), (void*)value);
	}

	inline static int32_t get_offset_of_count_2() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___count_2)); }
	inline int32_t get_count_2() const { return ___count_2; }
	inline int32_t* get_address_of_count_2() { return &___count_2; }
	inline void set_count_2(int32_t value)
	{
		___count_2 = value;
	}

	inline static int32_t get_offset_of_version_3() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___version_3)); }
	inline int32_t get_version_3() const { return ___version_3; }
	inline int32_t* get_address_of_version_3() { return &___version_3; }
	inline void set_version_3(int32_t value)
	{
		___version_3 = value;
	}

	inline static int32_t get_offset_of_freeList_4() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___freeList_4)); }
	inline int32_t get_freeList_4() const { return ___freeList_4; }
	inline int32_t* get_address_of_freeList_4() { return &___freeList_4; }
	inline void set_freeList_4(int32_t value)
	{
		___freeList_4 = value;
	}

	inline static int32_t get_offset_of_freeCount_5() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___freeCount_5)); }
	inline int32_t get_freeCount_5() const { return ___freeCount_5; }
	inline int32_t* get_address_of_freeCount_5() { return &___freeCount_5; }
	inline void set_freeCount_5(int32_t value)
	{
		___freeCount_5 = value;
	}

	inline static int32_t get_offset_of_comparer_6() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___comparer_6)); }
	inline RuntimeObject* get_comparer_6() const { return ___comparer_6; }
	inline RuntimeObject** get_address_of_comparer_6() { return &___comparer_6; }
	inline void set_comparer_6(RuntimeObject* value)
	{
		___comparer_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___comparer_6), (void*)value);
	}

	inline static int32_t get_offset_of_keys_7() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___keys_7)); }
	inline KeyCollection_t0C1D3BE91F3644A90384001C2525F30AD46140A8 * get_keys_7() const { return ___keys_7; }
	inline KeyCollection_t0C1D3BE91F3644A90384001C2525F30AD46140A8 ** get_address_of_keys_7() { return &___keys_7; }
	inline void set_keys_7(KeyCollection_t0C1D3BE91F3644A90384001C2525F30AD46140A8 * value)
	{
		___keys_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___keys_7), (void*)value);
	}

	inline static int32_t get_offset_of_values_8() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ___values_8)); }
	inline ValueCollection_t7E2BE0D0A1A453097FA708E1AC528386A068B733 * get_values_8() const { return ___values_8; }
	inline ValueCollection_t7E2BE0D0A1A453097FA708E1AC528386A068B733 ** get_address_of_values_8() { return &___values_8; }
	inline void set_values_8(ValueCollection_t7E2BE0D0A1A453097FA708E1AC528386A068B733 * value)
	{
		___values_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___values_8), (void*)value);
	}

	inline static int32_t get_offset_of__syncRoot_9() { return static_cast<int32_t>(offsetof(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472, ____syncRoot_9)); }
	inline RuntimeObject * get__syncRoot_9() const { return ____syncRoot_9; }
	inline RuntimeObject ** get_address_of__syncRoot_9() { return &____syncRoot_9; }
	inline void set__syncRoot_9(RuntimeObject * value)
	{
		____syncRoot_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____syncRoot_9), (void*)value);
	}
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// FMTurboJpegWrapper.TjSize
struct TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 
{
public:
	// System.Int32 FMTurboJpegWrapper.TjSize::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_0;
	// System.Int32 FMTurboJpegWrapper.TjSize::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_1;

public:
	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567, ___U3CWidthU3Ek__BackingField_0)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_0() const { return ___U3CWidthU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_0() { return &___U3CWidthU3Ek__BackingField_0; }
	inline void set_U3CWidthU3Ek__BackingField_0(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567, ___U3CHeightU3Ek__BackingField_1)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_1() const { return ___U3CHeightU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_1() { return &___U3CHeightU3Ek__BackingField_1; }
	inline void set_U3CHeightU3Ek__BackingField_1(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_1 = value;
	}
};


// System.Boolean
struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_tB53F6830F670160873277339AA58F15CAED4399C_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Byte
struct Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07 
{
public:
	// System.Byte System.Byte::m_value
	uint8_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Byte_tF87C579059BD4633E6840EBBBEEF899C6E33EF07, ___m_value_0)); }
	inline uint8_t get_m_value_0() const { return ___m_value_0; }
	inline uint8_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint8_t value)
	{
		___m_value_0 = value;
	}
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.Int32
struct Int32_t585191389E07734F19F3156FF88FB3EF4800D102 
{
public:
	// System.Int32 System.Int32::m_value
	int32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Int32_t585191389E07734F19F3156FF88FB3EF4800D102, ___m_value_0)); }
	inline int32_t get_m_value_0() const { return ___m_value_0; }
	inline int32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(int32_t value)
	{
		___m_value_0 = value;
	}
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// System.UInt32
struct UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B 
{
public:
	// System.UInt32 System.UInt32::m_value
	uint32_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt32_t4980FA09003AFAAB5A6E361BA2748EA9A005709B, ___m_value_0)); }
	inline uint32_t get_m_value_0() const { return ___m_value_0; }
	inline uint32_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint32_t value)
	{
		___m_value_0 = value;
	}
};


// System.UInt64
struct UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E 
{
public:
	// System.UInt64 System.UInt64::m_value
	uint64_t ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(UInt64_tA02DF3B59C8FC4A849BD207DA11038CC64E4CB4E, ___m_value_0)); }
	inline uint64_t get_m_value_0() const { return ___m_value_0; }
	inline uint64_t* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(uint64_t value)
	{
		___m_value_0 = value;
	}
};


// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017__padding[1];
	};

public:
};


// FMChromaSubsamplingOption
struct FMChromaSubsamplingOption_tB6BE264000A3EEB6CD588094846CE4200027C62C 
{
public:
	// System.Int32 FMChromaSubsamplingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FMChromaSubsamplingOption_tB6BE264000A3EEB6CD588094846CE4200027C62C, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.TJCompressor
struct TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF  : public RuntimeObject
{
public:
	// System.Object FMTurboJpegWrapper.TJCompressor::lock
	RuntimeObject * ___lock_0;
	// System.IntPtr FMTurboJpegWrapper.TJCompressor::compressorHandle
	intptr_t ___compressorHandle_1;
	// System.Boolean FMTurboJpegWrapper.TJCompressor::isDisposed
	bool ___isDisposed_2;

public:
	inline static int32_t get_offset_of_lock_0() { return static_cast<int32_t>(offsetof(TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF, ___lock_0)); }
	inline RuntimeObject * get_lock_0() const { return ___lock_0; }
	inline RuntimeObject ** get_address_of_lock_0() { return &___lock_0; }
	inline void set_lock_0(RuntimeObject * value)
	{
		___lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_compressorHandle_1() { return static_cast<int32_t>(offsetof(TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF, ___compressorHandle_1)); }
	inline intptr_t get_compressorHandle_1() const { return ___compressorHandle_1; }
	inline intptr_t* get_address_of_compressorHandle_1() { return &___compressorHandle_1; }
	inline void set_compressorHandle_1(intptr_t value)
	{
		___compressorHandle_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}
};


// FMTurboJpegWrapper.TJDecompressor
struct TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C  : public RuntimeObject
{
public:
	// System.Object FMTurboJpegWrapper.TJDecompressor::lock
	RuntimeObject * ___lock_0;
	// System.IntPtr FMTurboJpegWrapper.TJDecompressor::decompressorHandle
	intptr_t ___decompressorHandle_1;
	// System.Boolean FMTurboJpegWrapper.TJDecompressor::isDisposed
	bool ___isDisposed_2;

public:
	inline static int32_t get_offset_of_lock_0() { return static_cast<int32_t>(offsetof(TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C, ___lock_0)); }
	inline RuntimeObject * get_lock_0() const { return ___lock_0; }
	inline RuntimeObject ** get_address_of_lock_0() { return &___lock_0; }
	inline void set_lock_0(RuntimeObject * value)
	{
		___lock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___lock_0), (void*)value);
	}

	inline static int32_t get_offset_of_decompressorHandle_1() { return static_cast<int32_t>(offsetof(TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C, ___decompressorHandle_1)); }
	inline intptr_t get_decompressorHandle_1() const { return ___decompressorHandle_1; }
	inline intptr_t* get_address_of_decompressorHandle_1() { return &___decompressorHandle_1; }
	inline void set_decompressorHandle_1(intptr_t value)
	{
		___decompressorHandle_1 = value;
	}

	inline static int32_t get_offset_of_isDisposed_2() { return static_cast<int32_t>(offsetof(TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C, ___isDisposed_2)); }
	inline bool get_isDisposed_2() const { return ___isDisposed_2; }
	inline bool* get_address_of_isDisposed_2() { return &___isDisposed_2; }
	inline void set_isDisposed_2(bool value)
	{
		___isDisposed_2 = value;
	}
};


// FMTurboJpegWrapper.TJFlags
struct TJFlags_t64E245C6BE10DEADE38B123B156C5CC959E11874 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJFlags_t64E245C6BE10DEADE38B123B156C5CC959E11874, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.TJPixelFormat
struct TJPixelFormat_tC481543CB0361DE9AA1F0207E1B8CFEAF456CB79 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJPixelFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJPixelFormat_tC481543CB0361DE9AA1F0207E1B8CFEAF456CB79, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.TJSubsamplingOption
struct TJSubsamplingOption_tCAB3CAA7FCB54ACDC4F8BC62AA1C0655B319D4B1 
{
public:
	// System.Int32 FMTurboJpegWrapper.TJSubsamplingOption::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TJSubsamplingOption_tCAB3CAA7FCB54ACDC4F8BC62AA1C0655B319D4B1, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Exception
struct Exception_t  : public RuntimeObject
{
public:
	// System.String System.Exception::_className
	String_t* ____className_1;
	// System.String System.Exception::_message
	String_t* ____message_2;
	// System.Collections.IDictionary System.Exception::_data
	RuntimeObject* ____data_3;
	// System.Exception System.Exception::_innerException
	Exception_t * ____innerException_4;
	// System.String System.Exception::_helpURL
	String_t* ____helpURL_5;
	// System.Object System.Exception::_stackTrace
	RuntimeObject * ____stackTrace_6;
	// System.String System.Exception::_stackTraceString
	String_t* ____stackTraceString_7;
	// System.String System.Exception::_remoteStackTraceString
	String_t* ____remoteStackTraceString_8;
	// System.Int32 System.Exception::_remoteStackIndex
	int32_t ____remoteStackIndex_9;
	// System.Object System.Exception::_dynamicMethods
	RuntimeObject * ____dynamicMethods_10;
	// System.Int32 System.Exception::_HResult
	int32_t ____HResult_11;
	// System.String System.Exception::_source
	String_t* ____source_12;
	// System.Runtime.Serialization.SafeSerializationManager System.Exception::_safeSerializationManager
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	// System.Diagnostics.StackTrace[] System.Exception::captured_traces
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	// System.IntPtr[] System.Exception::native_trace_ips
	IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* ___native_trace_ips_15;

public:
	inline static int32_t get_offset_of__className_1() { return static_cast<int32_t>(offsetof(Exception_t, ____className_1)); }
	inline String_t* get__className_1() const { return ____className_1; }
	inline String_t** get_address_of__className_1() { return &____className_1; }
	inline void set__className_1(String_t* value)
	{
		____className_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____className_1), (void*)value);
	}

	inline static int32_t get_offset_of__message_2() { return static_cast<int32_t>(offsetof(Exception_t, ____message_2)); }
	inline String_t* get__message_2() const { return ____message_2; }
	inline String_t** get_address_of__message_2() { return &____message_2; }
	inline void set__message_2(String_t* value)
	{
		____message_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____message_2), (void*)value);
	}

	inline static int32_t get_offset_of__data_3() { return static_cast<int32_t>(offsetof(Exception_t, ____data_3)); }
	inline RuntimeObject* get__data_3() const { return ____data_3; }
	inline RuntimeObject** get_address_of__data_3() { return &____data_3; }
	inline void set__data_3(RuntimeObject* value)
	{
		____data_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____data_3), (void*)value);
	}

	inline static int32_t get_offset_of__innerException_4() { return static_cast<int32_t>(offsetof(Exception_t, ____innerException_4)); }
	inline Exception_t * get__innerException_4() const { return ____innerException_4; }
	inline Exception_t ** get_address_of__innerException_4() { return &____innerException_4; }
	inline void set__innerException_4(Exception_t * value)
	{
		____innerException_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____innerException_4), (void*)value);
	}

	inline static int32_t get_offset_of__helpURL_5() { return static_cast<int32_t>(offsetof(Exception_t, ____helpURL_5)); }
	inline String_t* get__helpURL_5() const { return ____helpURL_5; }
	inline String_t** get_address_of__helpURL_5() { return &____helpURL_5; }
	inline void set__helpURL_5(String_t* value)
	{
		____helpURL_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____helpURL_5), (void*)value);
	}

	inline static int32_t get_offset_of__stackTrace_6() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTrace_6)); }
	inline RuntimeObject * get__stackTrace_6() const { return ____stackTrace_6; }
	inline RuntimeObject ** get_address_of__stackTrace_6() { return &____stackTrace_6; }
	inline void set__stackTrace_6(RuntimeObject * value)
	{
		____stackTrace_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTrace_6), (void*)value);
	}

	inline static int32_t get_offset_of__stackTraceString_7() { return static_cast<int32_t>(offsetof(Exception_t, ____stackTraceString_7)); }
	inline String_t* get__stackTraceString_7() const { return ____stackTraceString_7; }
	inline String_t** get_address_of__stackTraceString_7() { return &____stackTraceString_7; }
	inline void set__stackTraceString_7(String_t* value)
	{
		____stackTraceString_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____stackTraceString_7), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackTraceString_8() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackTraceString_8)); }
	inline String_t* get__remoteStackTraceString_8() const { return ____remoteStackTraceString_8; }
	inline String_t** get_address_of__remoteStackTraceString_8() { return &____remoteStackTraceString_8; }
	inline void set__remoteStackTraceString_8(String_t* value)
	{
		____remoteStackTraceString_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____remoteStackTraceString_8), (void*)value);
	}

	inline static int32_t get_offset_of__remoteStackIndex_9() { return static_cast<int32_t>(offsetof(Exception_t, ____remoteStackIndex_9)); }
	inline int32_t get__remoteStackIndex_9() const { return ____remoteStackIndex_9; }
	inline int32_t* get_address_of__remoteStackIndex_9() { return &____remoteStackIndex_9; }
	inline void set__remoteStackIndex_9(int32_t value)
	{
		____remoteStackIndex_9 = value;
	}

	inline static int32_t get_offset_of__dynamicMethods_10() { return static_cast<int32_t>(offsetof(Exception_t, ____dynamicMethods_10)); }
	inline RuntimeObject * get__dynamicMethods_10() const { return ____dynamicMethods_10; }
	inline RuntimeObject ** get_address_of__dynamicMethods_10() { return &____dynamicMethods_10; }
	inline void set__dynamicMethods_10(RuntimeObject * value)
	{
		____dynamicMethods_10 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____dynamicMethods_10), (void*)value);
	}

	inline static int32_t get_offset_of__HResult_11() { return static_cast<int32_t>(offsetof(Exception_t, ____HResult_11)); }
	inline int32_t get__HResult_11() const { return ____HResult_11; }
	inline int32_t* get_address_of__HResult_11() { return &____HResult_11; }
	inline void set__HResult_11(int32_t value)
	{
		____HResult_11 = value;
	}

	inline static int32_t get_offset_of__source_12() { return static_cast<int32_t>(offsetof(Exception_t, ____source_12)); }
	inline String_t* get__source_12() const { return ____source_12; }
	inline String_t** get_address_of__source_12() { return &____source_12; }
	inline void set__source_12(String_t* value)
	{
		____source_12 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____source_12), (void*)value);
	}

	inline static int32_t get_offset_of__safeSerializationManager_13() { return static_cast<int32_t>(offsetof(Exception_t, ____safeSerializationManager_13)); }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * get__safeSerializationManager_13() const { return ____safeSerializationManager_13; }
	inline SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 ** get_address_of__safeSerializationManager_13() { return &____safeSerializationManager_13; }
	inline void set__safeSerializationManager_13(SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * value)
	{
		____safeSerializationManager_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____safeSerializationManager_13), (void*)value);
	}

	inline static int32_t get_offset_of_captured_traces_14() { return static_cast<int32_t>(offsetof(Exception_t, ___captured_traces_14)); }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* get_captured_traces_14() const { return ___captured_traces_14; }
	inline StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196** get_address_of_captured_traces_14() { return &___captured_traces_14; }
	inline void set_captured_traces_14(StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* value)
	{
		___captured_traces_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___captured_traces_14), (void*)value);
	}

	inline static int32_t get_offset_of_native_trace_ips_15() { return static_cast<int32_t>(offsetof(Exception_t, ___native_trace_ips_15)); }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* get_native_trace_ips_15() const { return ___native_trace_ips_15; }
	inline IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD** get_address_of_native_trace_ips_15() { return &___native_trace_ips_15; }
	inline void set_native_trace_ips_15(IntPtrU5BU5D_t4DC01DCB9A6DF6C9792A6513595D7A11E637DCDD* value)
	{
		___native_trace_ips_15 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___native_trace_ips_15), (void*)value);
	}
};

struct Exception_t_StaticFields
{
public:
	// System.Object System.Exception::s_EDILock
	RuntimeObject * ___s_EDILock_0;

public:
	inline static int32_t get_offset_of_s_EDILock_0() { return static_cast<int32_t>(offsetof(Exception_t_StaticFields, ___s_EDILock_0)); }
	inline RuntimeObject * get_s_EDILock_0() const { return ___s_EDILock_0; }
	inline RuntimeObject ** get_address_of_s_EDILock_0() { return &___s_EDILock_0; }
	inline void set_s_EDILock_0(RuntimeObject * value)
	{
		___s_EDILock_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___s_EDILock_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Exception
struct Exception_t_marshaled_pinvoke
{
	char* ____className_1;
	char* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_pinvoke* ____innerException_4;
	char* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	char* ____stackTraceString_7;
	char* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	char* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};
// Native definition for COM marshalling of System.Exception
struct Exception_t_marshaled_com
{
	Il2CppChar* ____className_1;
	Il2CppChar* ____message_2;
	RuntimeObject* ____data_3;
	Exception_t_marshaled_com* ____innerException_4;
	Il2CppChar* ____helpURL_5;
	Il2CppIUnknown* ____stackTrace_6;
	Il2CppChar* ____stackTraceString_7;
	Il2CppChar* ____remoteStackTraceString_8;
	int32_t ____remoteStackIndex_9;
	Il2CppIUnknown* ____dynamicMethods_10;
	int32_t ____HResult_11;
	Il2CppChar* ____source_12;
	SafeSerializationManager_t4A754D86B0F784B18CBC36C073BA564BED109770 * ____safeSerializationManager_13;
	StackTraceU5BU5D_t855F09649EA34DEE7C1B6F088E0538E3CCC3F196* ___captured_traces_14;
	Il2CppSafeArray/*NONE*/* ___native_trace_ips_15;
};

// System.Int32Enum
struct Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD 
{
public:
	// System.Int32 System.Int32Enum::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(Int32Enum_t6312CE4586C17FE2E2E513D2E7655B574F10FDCD, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.TextureFormat
struct TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE 
{
public:
	// System.Int32 UnityEngine.TextureFormat::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(TextureFormat_t7C6B5101554065C47682E592D1E26079D4EC2DCE, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// FMTurboJpegWrapper.DecompressedImage
struct DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32  : public RuntimeObject
{
public:
	// FMTurboJpegWrapper.TJPixelFormat FMTurboJpegWrapper.DecompressedImage::<PixelFormat>k__BackingField
	int32_t ___U3CPixelFormatU3Ek__BackingField_0;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Width>k__BackingField
	int32_t ___U3CWidthU3Ek__BackingField_1;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Height>k__BackingField
	int32_t ___U3CHeightU3Ek__BackingField_2;
	// System.Int32 FMTurboJpegWrapper.DecompressedImage::<Stride>k__BackingField
	int32_t ___U3CStrideU3Ek__BackingField_3;
	// System.Byte[] FMTurboJpegWrapper.DecompressedImage::<Data>k__BackingField
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___U3CDataU3Ek__BackingField_4;

public:
	inline static int32_t get_offset_of_U3CPixelFormatU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32, ___U3CPixelFormatU3Ek__BackingField_0)); }
	inline int32_t get_U3CPixelFormatU3Ek__BackingField_0() const { return ___U3CPixelFormatU3Ek__BackingField_0; }
	inline int32_t* get_address_of_U3CPixelFormatU3Ek__BackingField_0() { return &___U3CPixelFormatU3Ek__BackingField_0; }
	inline void set_U3CPixelFormatU3Ek__BackingField_0(int32_t value)
	{
		___U3CPixelFormatU3Ek__BackingField_0 = value;
	}

	inline static int32_t get_offset_of_U3CWidthU3Ek__BackingField_1() { return static_cast<int32_t>(offsetof(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32, ___U3CWidthU3Ek__BackingField_1)); }
	inline int32_t get_U3CWidthU3Ek__BackingField_1() const { return ___U3CWidthU3Ek__BackingField_1; }
	inline int32_t* get_address_of_U3CWidthU3Ek__BackingField_1() { return &___U3CWidthU3Ek__BackingField_1; }
	inline void set_U3CWidthU3Ek__BackingField_1(int32_t value)
	{
		___U3CWidthU3Ek__BackingField_1 = value;
	}

	inline static int32_t get_offset_of_U3CHeightU3Ek__BackingField_2() { return static_cast<int32_t>(offsetof(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32, ___U3CHeightU3Ek__BackingField_2)); }
	inline int32_t get_U3CHeightU3Ek__BackingField_2() const { return ___U3CHeightU3Ek__BackingField_2; }
	inline int32_t* get_address_of_U3CHeightU3Ek__BackingField_2() { return &___U3CHeightU3Ek__BackingField_2; }
	inline void set_U3CHeightU3Ek__BackingField_2(int32_t value)
	{
		___U3CHeightU3Ek__BackingField_2 = value;
	}

	inline static int32_t get_offset_of_U3CStrideU3Ek__BackingField_3() { return static_cast<int32_t>(offsetof(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32, ___U3CStrideU3Ek__BackingField_3)); }
	inline int32_t get_U3CStrideU3Ek__BackingField_3() const { return ___U3CStrideU3Ek__BackingField_3; }
	inline int32_t* get_address_of_U3CStrideU3Ek__BackingField_3() { return &___U3CStrideU3Ek__BackingField_3; }
	inline void set_U3CStrideU3Ek__BackingField_3(int32_t value)
	{
		___U3CStrideU3Ek__BackingField_3 = value;
	}

	inline static int32_t get_offset_of_U3CDataU3Ek__BackingField_4() { return static_cast<int32_t>(offsetof(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32, ___U3CDataU3Ek__BackingField_4)); }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* get_U3CDataU3Ek__BackingField_4() const { return ___U3CDataU3Ek__BackingField_4; }
	inline ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** get_address_of_U3CDataU3Ek__BackingField_4() { return &___U3CDataU3Ek__BackingField_4; }
	inline void set_U3CDataU3Ek__BackingField_4(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* value)
	{
		___U3CDataU3Ek__BackingField_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CDataU3Ek__BackingField_4), (void*)value);
	}
};


// System.SystemException
struct SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782  : public Exception_t
{
public:

public:
};


// UnityEngine.Texture
struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};

struct Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields
{
public:
	// System.Int32 UnityEngine.Texture::GenerateAllMips
	int32_t ___GenerateAllMips_4;

public:
	inline static int32_t get_offset_of_GenerateAllMips_4() { return static_cast<int32_t>(offsetof(Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4_StaticFields, ___GenerateAllMips_4)); }
	inline int32_t get_GenerateAllMips_4() const { return ___GenerateAllMips_4; }
	inline int32_t* get_address_of_GenerateAllMips_4() { return &___GenerateAllMips_4; }
	inline void set_GenerateAllMips_4(int32_t value)
	{
		___GenerateAllMips_4 = value;
	}
};


// System.ArgumentException
struct ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:
	// System.String System.ArgumentException::m_paramName
	String_t* ___m_paramName_17;

public:
	inline static int32_t get_offset_of_m_paramName_17() { return static_cast<int32_t>(offsetof(ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1, ___m_paramName_17)); }
	inline String_t* get_m_paramName_17() const { return ___m_paramName_17; }
	inline String_t** get_address_of_m_paramName_17() { return &___m_paramName_17; }
	inline void set_m_paramName_17(String_t* value)
	{
		___m_paramName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_paramName_17), (void*)value);
	}
};


// System.InvalidOperationException
struct InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// System.NotSupportedException
struct NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010  : public SystemException_t5380468142AA850BE4A341D7AF3EAB9C78746782
{
public:

public:
};


// UnityEngine.Texture2D
struct Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C  : public Texture_t387FE83BB848001FD06B14707AEA6D5A0F6A95F4
{
public:

public:
};


// System.ArgumentOutOfRangeException
struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA  : public ArgumentException_tEDCD16F20A09ECE461C3DA766C16EDA8864057D1
{
public:
	// System.Object System.ArgumentOutOfRangeException::m_actualValue
	RuntimeObject * ___m_actualValue_19;

public:
	inline static int32_t get_offset_of_m_actualValue_19() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA, ___m_actualValue_19)); }
	inline RuntimeObject * get_m_actualValue_19() const { return ___m_actualValue_19; }
	inline RuntimeObject ** get_address_of_m_actualValue_19() { return &___m_actualValue_19; }
	inline void set_m_actualValue_19(RuntimeObject * value)
	{
		___m_actualValue_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_actualValue_19), (void*)value);
	}
};

struct ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields
{
public:
	// System.String modreq(System.Runtime.CompilerServices.IsVolatile) System.ArgumentOutOfRangeException::_rangeMessage
	String_t* ____rangeMessage_18;

public:
	inline static int32_t get_offset_of__rangeMessage_18() { return static_cast<int32_t>(offsetof(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_StaticFields, ____rangeMessage_18)); }
	inline String_t* get__rangeMessage_18() const { return ____rangeMessage_18; }
	inline String_t** get_address_of__rangeMessage_18() { return &____rangeMessage_18; }
	inline void set__rangeMessage_18(String_t* value)
	{
		____rangeMessage_18 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____rangeMessage_18), (void*)value);
	}
};


// System.ObjectDisposedException
struct ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A  : public InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1
{
public:
	// System.String System.ObjectDisposedException::objectName
	String_t* ___objectName_17;

public:
	inline static int32_t get_offset_of_objectName_17() { return static_cast<int32_t>(offsetof(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A, ___objectName_17)); }
	inline String_t* get_objectName_17() const { return ___objectName_17; }
	inline String_t** get_address_of_objectName_17() { return &___objectName_17; }
	inline void set_objectName_17(String_t* value)
	{
		___objectName_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___objectName_17), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
// System.Byte[]
struct ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821  : public RuntimeArray
{
public:
	ALIGN_FIELD (8) uint8_t m_Items[1];

public:
	inline uint8_t GetAt(il2cpp_array_size_t index) const
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items[index];
	}
	inline uint8_t* GetAddressAt(il2cpp_array_size_t index)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		return m_Items + index;
	}
	inline void SetAt(il2cpp_array_size_t index, uint8_t value)
	{
		IL2CPP_ARRAY_BOUNDS_CHECK(index, (uint32_t)(this)->max_length);
		m_Items[index] = value;
	}
	inline uint8_t GetAtUnchecked(il2cpp_array_size_t index) const
	{
		return m_Items[index];
	}
	inline uint8_t* GetAddressAtUnchecked(il2cpp_array_size_t index)
	{
		return m_Items + index;
	}
	inline void SetAtUnchecked(il2cpp_array_size_t index, uint8_t value)
	{
		m_Items[index] = value;
	}
};


// !1 System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::get_Item(!0)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Dictionary_2_get_Item_mE6609AFB27A8549F0576AFA87EAF33D4EF5A5618_gshared (Dictionary_2_tF12CF1D3D699D81529FCF73688C5C19C0C4A8A19 * __this, int32_t ___key0, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mDBFF2D39C932B4BF051B092ECA2DA62FBCA66FAA_gshared (Dictionary_2_tF12CF1D3D699D81529FCF73688C5C19C0C4A8A19 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,System.Int32>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_m6A19864A2CCBBAE3D6A7FCD778C71ED2D3CFF97F_gshared (Dictionary_2_tF12CF1D3D699D81529FCF73688C5C19C0C4A8A19 * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2__ctor_mC5E1D5CAB17C6C73116472D30DBC55EE77C8F5B7_gshared (Dictionary_2_t7BECB1C7491E20BC1F2B7F6269632191F986ED30 * __this, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<System.Int32Enum,FMTurboJpegWrapper.TjSize>::Add(!0,!1)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Dictionary_2_Add_mA45589D06D940A55EAD55CA619FA939C50B614EB_gshared (Dictionary_2_t7BECB1C7491E20BC1F2B7F6269632191F986ED30 * __this, int32_t ___key0, TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  ___value1, const RuntimeMethod* method);

// System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, const RuntimeMethod* method);
// FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A (int32_t ___Subsampling0, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_EncodeFMJPG_m720CCE4185E435735EE1813878DA35631FF6F09B (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, int32_t ___Quality1, int32_t ___TJSubsampling2, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1 (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Void UnityEngine.Debug::LogError(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29 (RuntimeObject * ___message0, const RuntimeMethod* method);
// UnityEngine.TextureFormat UnityEngine.Texture2D::get_format()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___jpegBuf0, int32_t ___format1, const RuntimeMethod* method);
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___x0, Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___y1, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::.ctor(System.Int32,System.Int32,UnityEngine.TextureFormat,System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, int32_t ___width0, int32_t ___height1, int32_t ___textureFormat2, bool ___mipChain3, const RuntimeMethod* method);
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 * ___obj0, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::LoadRawTextureData(System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data0, const RuntimeMethod* method);
// System.Void UnityEngine.Texture2D::Apply()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_EncodeFMJPG_m286309881E43E4E7DD480AFD688500CA182EDA16 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ____format3, int32_t ___Quality4, int32_t ___TJSubsampling5, const RuntimeMethod* method);
// System.Void System.Object::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0 (RuntimeObject * __this, const RuntimeMethod* method);
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitCompress_m5D481C98E7233C1F96A8EBD1B887B52323D7CFE1 (const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Equality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004 (const RuntimeMethod* method);
// System.Void System.Object::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380 (RuntimeObject * __this, const RuntimeMethod* method);
// System.Void System.ObjectDisposedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9 (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * __this, String_t* ___objectName0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2 (int32_t ___subSamp0, int32_t ___srcFormat1, const RuntimeMethod* method);
// System.IntPtr System.IntPtr::op_Explicit(System.Void*)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t IntPtr_op_Explicit_m7F0C4B884FFB05BD231154CBDAEBCF1917019C21 (void* ___value0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjCompress2_mE47CB34033396A32565D6C22162B0CA15A6DC9A9 (intptr_t ___handle0, intptr_t ___srcBuf1, int32_t ___width2, int32_t ___pitch3, int32_t ___height4, int32_t ___pixelFormat5, intptr_t* ___jpegBuf6, uint64_t* ___jpegSize7, int32_t ___jpegSubsamp8, int32_t ___jpegQual9, int32_t ___flags10, const RuntimeMethod* method);
// System.Void System.Runtime.InteropServices.Marshal::Copy(System.IntPtr,System.Byte[],System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Marshal_Copy_m64744D9E23AFC00AA06CD6B057E19B7C0CE4C0C2 (intptr_t ___source0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___destination1, int32_t ___startIndex2, int32_t ___length3, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport_TjFree_m75A67BF5A45741A79826A366D0584C652D55EFF5 (intptr_t ___buffer0, const RuntimeMethod* method);
// System.Byte[] UnityEngine.Texture2D::GetRawTextureData()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* Texture2D_GetRawTextureData_m387AAB1686E27DA77F4065A2111DF18934AFB364 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * __this, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___srcBuf0, int32_t ___stride1, int32_t ___width2, int32_t ___height3, int32_t ___tjPixelFormat4, int32_t ___subSamp5, int32_t ___quality6, int32_t ___flags7, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Enter(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.GC::SuppressFinalize(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Void System.Threading.Monitor::Exit(System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2 (RuntimeObject * ___obj0, const RuntimeMethod* method);
// System.Boolean System.IntPtr::op_Inequality(System.IntPtr,System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR bool IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61 (intptr_t ___value10, intptr_t ___value21, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78 (intptr_t ___handle0, const RuntimeMethod* method);
// System.String System.String::Format(System.String,System.Object,System.Object)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A (String_t* ___format0, RuntimeObject * ___arg01, RuntimeObject * ___arg12, const RuntimeMethod* method);
// System.Void System.NotSupportedException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void NotSupportedException__ctor_mD023A89A5C1F740F43F0A9CD6C49DC21230B3CEE (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitDecompress_mFEFD01E2373F2F6F2277C1BB679B2B445C00EFCD (const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t* ___width3, int32_t* ___height4, int32_t* ___stride5, int32_t* ___bufSize6, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, intptr_t ___outBuf2, int32_t ___outBufSize3, int32_t ___destPixelFormat4, int32_t ___flags5, int32_t* ___width6, int32_t* ___height7, int32_t* ___stride8, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// !1 System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::get_Item(!0)
inline int32_t Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02 (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * __this, int32_t ___key0, const RuntimeMethod* method)
{
	return ((  int32_t (*) (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A *, int32_t, const RuntimeMethod*))Dictionary_2_get_Item_mE6609AFB27A8549F0576AFA87EAF33D4EF5A5618_gshared)(__this, ___key0, method);
}
// System.String System.Int32::ToString()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR String_t* Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02 (int32_t* __this, const RuntimeMethod* method);
// System.Void System.ArgumentOutOfRangeException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6 (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * __this, String_t* ___paramName0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, int32_t* ___width4, int32_t* ___height5, int32_t* ___stride6, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DecompressedImage__ctor_mAA66065D12D8A58743FC653804A5D9E02358F448 (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, int32_t ___width0, int32_t ___height1, int32_t ___stride2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data3, int32_t ___pixelFormat4, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, const RuntimeMethod* method);
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___jpegBuf0, int32_t ___destPixelFormat1, int32_t ___flags2, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_inline (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_inline (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method);
// System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232 (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method);
// System.Int32 System.IntPtr::get_Size()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t IntPtr_get_Size_m1342A61F11766A494F2F90D9B68CADAD62261929 (const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x86_mC406308CA370922203E3FB4464CE1DD78ACBEE3C (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x64_m8ABE9493363F1014B8119E207F22BF353500688A (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method);
// System.Void System.InvalidOperationException::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706 (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * __this, String_t* ___message0, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x86_m16C2D62F861785C4C2C3155444DCF363F1C992B1 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x64_mCF8035CD678FA2504EDD42B3C164855E1B437D0D (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method);
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::.ctor()
inline void Dictionary_2__ctor_m975E6ED49D776E4C04851FA34DFABF55A1EA395F (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A *, const RuntimeMethod*))Dictionary_2__ctor_mDBFF2D39C932B4BF051B092ECA2DA62FBCA66FAA_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJPixelFormat,System.Int32>::Add(!0,!1)
inline void Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444 (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * __this, int32_t ___key0, int32_t ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A *, int32_t, int32_t, const RuntimeMethod*))Dictionary_2_Add_m6A19864A2CCBBAE3D6A7FCD778C71ED2D3CFF97F_gshared)(__this, ___key0, ___value1, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>::.ctor()
inline void Dictionary_2__ctor_m0831B7243D8A1B9DFF515B83573FB3A13C3743C4 (Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * __this, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 *, const RuntimeMethod*))Dictionary_2__ctor_mC5E1D5CAB17C6C73116472D30DBC55EE77C8F5B7_gshared)(__this, method);
}
// System.Void System.Collections.Generic.Dictionary`2<FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TjSize>::Add(!0,!1)
inline void Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472 (Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * __this, int32_t ___key0, TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  ___value1, const RuntimeMethod* method)
{
	((  void (*) (Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 *, int32_t, TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 , const RuntimeMethod*))Dictionary_2_Add_mA45589D06D940A55EAD55CA619FA939C50B614EB_gshared)(__this, ___key0, ___value1, method);
}
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Byte[] FMExtensionMethods::FMEncodeToJPG(UnityEngine.Texture2D,System.Int32,FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* FMExtensionMethods_FMEncodeToJPG_mEA8B88031312980B401A7B162B793EB50AFE5A05 (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, int32_t ___Quality1, int32_t ___Subsampling2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FMExtensionMethods_FMEncodeToJPG_mEA8B88031312980B401A7B162B793EB50AFE5A05_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * V_0 = NULL;
	{
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_0 = (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF *)il2cpp_codegen_object_new(TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF_il2cpp_TypeInfo_var);
		TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_1 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = ___texture0;
		int32_t L_3 = ___Quality1;
		int32_t L_4 = ___Subsampling2;
		int32_t L_5 = FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A(L_4, /*hidden argument*/NULL);
		NullCheck(L_1);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = TJCompressor_EncodeFMJPG_m720CCE4185E435735EE1813878DA35631FF6F09B(L_1, L_2, L_3, L_5, /*hidden argument*/NULL);
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_7 = V_0;
		NullCheck(L_7);
		TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0(L_7, /*hidden argument*/NULL);
		return L_6;
	}
}
// System.Void FMExtensionMethods::FMLoadJPG(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Byte[])
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMLoadJPG_m34CC978D5E7FA1A6E8143998C2E94C5F09982D2D (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** ___ref_texture1, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___bytes2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FMExtensionMethods_FMLoadJPG_m34CC978D5E7FA1A6E8143998C2E94C5F09982D2D_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * V_1 = NULL;
	int32_t G_B5_0 = 0;
	TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * G_B7_0 = NULL;
	TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * G_B6_0 = NULL;
	TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * G_B10_0 = NULL;
	TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * G_B9_0 = NULL;
	TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * G_B8_0 = NULL;
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_0 = ___texture0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_1 = ___ref_texture1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = *((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral6594273630B6D2176D3E914597BC3109539BBA40, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_4, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_0027;
	}

IL_0021:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7 = Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F(L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
	}

IL_0027:
	{
		V_0 = G_B5_0;
		TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * L_8 = (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C *)il2cpp_codegen_object_new(TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C_il2cpp_TypeInfo_var);
		TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1(L_8, /*hidden argument*/NULL);
		TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * L_9 = L_8;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = ___bytes2;
		int32_t L_11 = V_0;
		NullCheck(L_9);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_12 = TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911(L_9, L_10, L_11, /*hidden argument*/NULL);
		V_1 = L_12;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_13 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_14 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_13, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		G_B6_0 = L_9;
		if (!L_14)
		{
			G_B7_0 = L_9;
			goto IL_0056;
		}
	}
	{
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_15 = V_1;
		NullCheck(L_15);
		int32_t L_16 = DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline(L_15, /*hidden argument*/NULL);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_17 = V_1;
		NullCheck(L_17);
		int32_t L_18 = DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline(L_17, /*hidden argument*/NULL);
		int32_t L_19 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_20 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_20, L_16, L_18, L_19, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_20;
		G_B10_0 = G_B6_0;
		goto IL_008d;
	}

IL_0056:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_21 = ___texture0;
		NullCheck(L_21);
		int32_t L_22 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_21);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_23 = V_1;
		NullCheck(L_23);
		int32_t L_24 = DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline(L_23, /*hidden argument*/NULL);
		G_B8_0 = G_B7_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)L_24))))
		{
			G_B9_0 = G_B7_0;
			goto IL_0072;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_25 = ___texture0;
		NullCheck(L_25);
		int32_t L_26 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_25);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_27 = V_1;
		NullCheck(L_27);
		int32_t L_28 = DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline(L_27, /*hidden argument*/NULL);
		G_B9_0 = G_B8_0;
		if ((((int32_t)L_26) == ((int32_t)L_28)))
		{
			G_B10_0 = G_B8_0;
			goto IL_008d;
		}
	}

IL_0072:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_29 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_29, /*hidden argument*/NULL);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_30 = V_1;
		NullCheck(L_30);
		int32_t L_31 = DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline(L_30, /*hidden argument*/NULL);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_32 = V_1;
		NullCheck(L_32);
		int32_t L_33 = DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline(L_32, /*hidden argument*/NULL);
		int32_t L_34 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_35 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_35, L_31, L_33, L_34, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_35;
		G_B10_0 = G_B9_0;
	}

IL_008d:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_36 = ___texture0;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_37 = V_1;
		NullCheck(L_37);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_38 = DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8_inline(L_37, /*hidden argument*/NULL);
		NullCheck(L_36);
		Texture2D_LoadRawTextureData_m0331275D060EC3521BCCF84194FA35BBEEF767CB(L_36, L_38, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_39 = ___texture0;
		NullCheck(L_39);
		Texture2D_Apply_m0F3B4A4B1B89E44E2AF60ABDEFAA18D93735B5CA(L_39, /*hidden argument*/NULL);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_40 = ___ref_texture1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_41 = ___texture0;
		*((RuntimeObject **)L_40) = (RuntimeObject *)L_41;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_40, (void*)(RuntimeObject *)L_41);
		NullCheck(G_B10_0);
		TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95(G_B10_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FMExtensionMethods::FMMatchResolution(UnityEngine.Texture2D,UnityEngine.Texture2D&,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMMatchResolution_m849F96713529AB6091F6D6D6FE5F433F2070E63E (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** ___ref_texture1, int32_t ____width2, int32_t ____height3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FMExtensionMethods_FMMatchResolution_m849F96713529AB6091F6D6D6FE5F433F2070E63E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t G_B5_0 = 0;
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_0 = ___texture0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_1 = ___ref_texture1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = *((Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C **)L_1);
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_3 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_0, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0015;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t7B5FCB117E2FD63B6838BC52821B252E2BFB61C4_il2cpp_TypeInfo_var);
		Debug_LogError_m3BCF9B78263152261565DCA9DB7D55F0C391ED29(_stringLiteral6594273630B6D2176D3E914597BC3109539BBA40, /*hidden argument*/NULL);
		return;
	}

IL_0015:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_4 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_5 = Object_op_Inequality_m31EF58E217E8F4BDD3E409DEF79E1AEE95874FC1(L_4, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (L_5)
		{
			goto IL_0021;
		}
	}
	{
		G_B5_0 = 4;
		goto IL_0027;
	}

IL_0021:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7 = Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F(L_6, /*hidden argument*/NULL);
		G_B5_0 = ((int32_t)(L_7));
	}

IL_0027:
	{
		V_0 = G_B5_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_8 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		bool L_9 = Object_op_Equality_mBC2401774F3BE33E8CF6F0A8148E66C95D6CFF1C(L_8, (Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_10 = ____width2;
		int32_t L_11 = ____height3;
		int32_t L_12 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_13 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_13, L_10, L_11, L_12, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_13;
		goto IL_0061;
	}

IL_003e:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_14 = ___texture0;
		NullCheck(L_14);
		int32_t L_15 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_14);
		int32_t L_16 = ____width2;
		if ((!(((uint32_t)L_15) == ((uint32_t)L_16))))
		{
			goto IL_0050;
		}
	}
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_17 = ___texture0;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_17);
		int32_t L_19 = ____height3;
		if ((((int32_t)L_18) == ((int32_t)L_19)))
		{
			goto IL_0061;
		}
	}

IL_0050:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_20 = ___texture0;
		IL2CPP_RUNTIME_CLASS_INIT(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_il2cpp_TypeInfo_var);
		Object_Destroy_m23B4562495BA35A74266D4372D45368F8C05109A(L_20, /*hidden argument*/NULL);
		int32_t L_21 = ____width2;
		int32_t L_22 = ____height3;
		int32_t L_23 = V_0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_24 = (Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C *)il2cpp_codegen_object_new(Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C_il2cpp_TypeInfo_var);
		Texture2D__ctor_m22561E039BC96019757E6B2427BE09734AE2C44A(L_24, L_21, L_22, L_23, (bool)0, /*hidden argument*/NULL);
		___texture0 = L_24;
	}

IL_0061:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C ** L_25 = ___ref_texture1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_26 = ___texture0;
		*((RuntimeObject **)L_25) = (RuntimeObject *)L_26;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_25, (void*)(RuntimeObject *)L_26);
		return;
	}
}
// System.Byte[] FMExtensionMethods::FMRawTextureDataToJPG(System.Byte[],System.Int32,System.Int32,System.Int32,FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* FMExtensionMethods_FMRawTextureDataToJPG_mABD75BC62916FC0962EEAB016ABFFCEAEDCF62F1 (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ___Quality3, int32_t ___Subsampling4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FMExtensionMethods_FMRawTextureDataToJPG_mABD75BC62916FC0962EEAB016ABFFCEAEDCF62F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * V_0 = NULL;
	int32_t G_B2_0 = 0;
	int32_t G_B2_1 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B2_2 = NULL;
	TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * G_B2_3 = NULL;
	int32_t G_B1_0 = 0;
	int32_t G_B1_1 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B1_2 = NULL;
	TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * G_B1_3 = NULL;
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	int32_t G_B3_2 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B3_3 = NULL;
	TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * G_B3_4 = NULL;
	{
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_0 = (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF *)il2cpp_codegen_object_new(TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF_il2cpp_TypeInfo_var);
		TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_1 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___RawTextureData0;
		int32_t L_3 = ____width1;
		int32_t L_4 = ____height2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = ___RawTextureData0;
		NullCheck(L_5);
		int32_t L_6 = ____height2;
		int32_t L_7 = ____width1;
		G_B1_0 = L_4;
		G_B1_1 = L_3;
		G_B1_2 = L_2;
		G_B1_3 = L_1;
		if ((((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length))))/(int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)L_7))))) == ((int32_t)3)))
		{
			G_B2_0 = L_4;
			G_B2_1 = L_3;
			G_B2_2 = L_2;
			G_B2_3 = L_1;
			goto IL_0017;
		}
	}
	{
		G_B3_0 = 7;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 0;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0018:
	{
		int32_t L_8 = ___Quality3;
		int32_t L_9 = ___Subsampling4;
		int32_t L_10 = FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A(L_9, /*hidden argument*/NULL);
		NullCheck(G_B3_4);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = TJCompressor_EncodeFMJPG_m286309881E43E4E7DD480AFD688500CA182EDA16(G_B3_4, G_B3_3, G_B3_2, G_B3_1, G_B3_0, L_8, L_10, /*hidden argument*/NULL);
		TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * L_12 = V_0;
		NullCheck(L_12);
		TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0(L_12, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void FMExtensionMethods::FMJPGToRawTextureData(System.Byte[],System.Byte[]&,System.Int32&,System.Int32&,UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void FMExtensionMethods_FMJPGToRawTextureData_mC207C0F855721B7FCE13135C9DF893528F3FFAEC (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___JPGData0, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** ____RawTextureData1, int32_t* ____width2, int32_t* ____height3, int32_t ____format4, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (FMExtensionMethods_FMJPGToRawTextureData_mC207C0F855721B7FCE13135C9DF893528F3FFAEC_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * V_0 = NULL;
	{
		TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * L_0 = (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C *)il2cpp_codegen_object_new(TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C_il2cpp_TypeInfo_var);
		TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1(L_0, /*hidden argument*/NULL);
		TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * L_1 = L_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___JPGData0;
		int32_t L_3 = ____format4;
		NullCheck(L_1);
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_4 = TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911(L_1, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821** L_5 = ____RawTextureData1;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_6 = V_0;
		NullCheck(L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8_inline(L_6, /*hidden argument*/NULL);
		*((RuntimeObject **)L_5) = (RuntimeObject *)L_7;
		Il2CppCodeGenWriteBarrier((void**)(RuntimeObject **)L_5, (void*)(RuntimeObject *)L_7);
		int32_t* L_8 = ____width2;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_9 = V_0;
		NullCheck(L_9);
		int32_t L_10 = DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline(L_9, /*hidden argument*/NULL);
		*((int32_t*)L_8) = (int32_t)L_10;
		int32_t* L_11 = ____height3;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_12 = V_0;
		NullCheck(L_12);
		int32_t L_13 = DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline(L_12, /*hidden argument*/NULL);
		*((int32_t*)L_11) = (int32_t)L_13;
		NullCheck(L_1);
		TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95(L_1, /*hidden argument*/NULL);
		return;
	}
}
// FMTurboJpegWrapper.TJSubsamplingOption FMExtensionMethods::GetTJSubsampligOption(FMChromaSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t FMExtensionMethods_GetTJSubsampligOption_m74EA09B7977A09C8657ED651038902293EDB129A (int32_t ___Subsampling0, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 2;
		int32_t L_0 = ___Subsampling0;
		switch (L_0)
		{
			case 0:
			{
				goto IL_001a;
			}
			case 1:
			{
				goto IL_001e;
			}
			case 2:
			{
				goto IL_0022;
			}
			case 3:
			{
				goto IL_0026;
			}
		}
	}
	{
		goto IL_0028;
	}

IL_001a:
	{
		V_0 = 0;
		goto IL_0028;
	}

IL_001e:
	{
		V_0 = 1;
		goto IL_0028;
	}

IL_0022:
	{
		V_0 = 2;
		goto IL_0028;
	}

IL_0026:
	{
		V_0 = 3;
	}

IL_0028:
	{
		int32_t L_1 = V_0;
		return L_1;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.DecompressedImage::.ctor(System.Int32,System.Int32,System.Int32,System.Byte[],FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DecompressedImage__ctor_mAA66065D12D8A58743FC653804A5D9E02358F448 (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, int32_t ___width0, int32_t ___height1, int32_t ___stride2, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___data3, int32_t ___pixelFormat4, const RuntimeMethod* method)
{
	{
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___width0;
		__this->set_U3CWidthU3Ek__BackingField_1(L_0);
		int32_t L_1 = ___height1;
		__this->set_U3CHeightU3Ek__BackingField_2(L_1);
		int32_t L_2 = ___stride2;
		__this->set_U3CStrideU3Ek__BackingField_3(L_2);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___data3;
		__this->set_U3CDataU3Ek__BackingField_4(L_3);
		int32_t L_4 = ___pixelFormat4;
		__this->set_U3CPixelFormatU3Ek__BackingField_0(L_4);
		return;
	}
}
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Width()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41 (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CWidthU3Ek__BackingField_1();
		return L_0;
	}
}
// System.Int32 FMTurboJpegWrapper.DecompressedImage::get_Height()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CHeightU3Ek__BackingField_2();
		return L_0;
	}
}
// System.Byte[] FMTurboJpegWrapper.DecompressedImage::get_Data()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8 (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_U3CDataU3Ek__BackingField_4();
		return L_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJCompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJCompressor__ctor_mC961B043AF1AF7B48B8C6F77CEFA0E83446AA3F9_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		__this->set_lock_0(L_0);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		intptr_t L_1 = TurboJpegImport_TjInitCompress_m5D481C98E7233C1F96A8EBD1B887B52323D7CFE1(/*hidden argument*/NULL);
		__this->set_compressorHandle_1((intptr_t)L_1);
		intptr_t L_2 = __this->get_compressorHandle_1();
		bool L_3 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004(/*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Finalize_mAE14595B586EDE505718E5EBFC902D745257E20E (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x10, FINALLY_0009);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0009;
	}

FINALLY_0009:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(9)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(9)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x10, IL_0010)
	}

IL_0010:
	{
		return;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::Compress(System.Byte[],System.Int32,System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJSubsamplingOption,System.Int32,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___srcBuf0, int32_t ___stride1, int32_t ___width2, int32_t ___height3, int32_t ___tjPixelFormat4, int32_t ___subSamp5, int32_t ___quality6, int32_t ___flags7, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	intptr_t V_0;
	memset((&V_0), 0, sizeof(V_0));
	uint64_t V_1 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	uint8_t* V_3 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_4 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_5 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteralC2543FFF3BFA6F144C2F06A7DE6CD10C0B650CAE, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2_RuntimeMethod_var);
	}

IL_0013:
	{
		int32_t L_2 = ___subSamp5;
		int32_t L_3 = ___tjPixelFormat4;
		TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2(L_2, L_3, /*hidden argument*/NULL);
		V_0 = (intptr_t)(0);
		V_1 = (((int64_t)((int64_t)0)));
	}

IL_0025:
	try
	{ // begin try (depth: 1)
		try
		{ // begin try (depth: 2)
			{
				ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = ___srcBuf0;
				ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = L_4;
				V_4 = L_5;
				if (!L_5)
				{
					goto IL_0031;
				}
			}

IL_002b:
			{
				ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_4;
				NullCheck(L_6);
				if ((((int32_t)((int32_t)(((RuntimeArray*)L_6)->max_length)))))
				{
					goto IL_0036;
				}
			}

IL_0031:
			{
				V_3 = (uint8_t*)(((uintptr_t)0));
				goto IL_0040;
			}

IL_0036:
			{
				ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = V_4;
				NullCheck(L_7);
				V_3 = (uint8_t*)(((uintptr_t)((L_7)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
			}

IL_0040:
			{
				intptr_t L_8 = __this->get_compressorHandle_1();
				uint8_t* L_9 = V_3;
				intptr_t L_10 = IntPtr_op_Explicit_m7F0C4B884FFB05BD231154CBDAEBCF1917019C21((void*)(void*)L_9, /*hidden argument*/NULL);
				int32_t L_11 = ___width2;
				int32_t L_12 = ___stride1;
				int32_t L_13 = ___height3;
				int32_t L_14 = ___tjPixelFormat4;
				int32_t L_15 = ___subSamp5;
				int32_t L_16 = ___quality6;
				int32_t L_17 = ___flags7;
				IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
				int32_t L_18 = TurboJpegImport_TjCompress2_mE47CB34033396A32565D6C22162B0CA15A6DC9A9((intptr_t)L_8, (intptr_t)L_10, L_11, L_12, L_13, L_14, (intptr_t*)(&V_0), (uint64_t*)(&V_1), L_15, L_16, L_17, /*hidden argument*/NULL);
				if ((!(((uint32_t)L_18) == ((uint32_t)(-1)))))
				{
					goto IL_0069;
				}
			}

IL_0064:
			{
				TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004(/*hidden argument*/NULL);
			}

IL_0069:
			{
				IL2CPP_LEAVE(0x6F, FINALLY_006b);
			}
		} // end try (depth: 2)
		catch(Il2CppExceptionWrapper& e)
		{
			__last_unhandled_exception = (Exception_t *)e.ex;
			goto FINALLY_006b;
		}

FINALLY_006b:
		{ // begin finally (depth: 2)
			V_4 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
			IL2CPP_END_FINALLY(107)
		} // end finally (depth: 2)
		IL2CPP_CLEANUP(107)
		{
			IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
			IL2CPP_JUMP_TBL(0x6F, IL_006f)
		}

IL_006f:
		{
			uint64_t L_19 = V_1;
			if ((uint64_t)(L_19) > INTPTR_MAX) IL2CPP_RAISE_MANAGED_EXCEPTION(il2cpp_codegen_get_overflow_exception(), TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2_RuntimeMethod_var);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_20 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)(((intptr_t)L_19)));
			V_2 = L_20;
			intptr_t L_21 = V_0;
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_22 = V_2;
			uint64_t L_23 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(Marshal_tC795CE9CC2FFBA41EDB1AC1C0FEC04607DFA8A40_il2cpp_TypeInfo_var);
			Marshal_Copy_m64744D9E23AFC00AA06CD6B057E19B7C0CE4C0C2((intptr_t)L_21, L_22, 0, (((int32_t)((int32_t)L_23))), /*hidden argument*/NULL);
			ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = V_2;
			V_5 = L_24;
			IL2CPP_LEAVE(0x8D, FINALLY_0086);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0086;
	}

FINALLY_0086:
	{ // begin finally (depth: 1)
		intptr_t L_25 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		TurboJpegImport_TjFree_m75A67BF5A45741A79826A366D0584C652D55EFF5((intptr_t)L_25, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(134)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(134)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x8D, IL_008d)
	}

IL_008d:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_26 = V_5;
		return L_26;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(UnityEngine.Texture2D,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_EncodeFMJPG_m720CCE4185E435735EE1813878DA35631FF6F09B (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * ___texture0, int32_t ___Quality1, int32_t ___TJSubsampling2, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	{
		V_0 = 0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_0 = ___texture0;
		NullCheck(L_0);
		int32_t L_1 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_0);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_1, (int32_t)3));
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_2 = ___texture0;
		NullCheck(L_2);
		int32_t L_3 = Texture2D_get_format_mF0EE5CEB9F84280D4E722B71546BBBA577101E9F(L_2, /*hidden argument*/NULL);
		V_2 = L_3;
		int32_t L_4 = V_2;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_4, (int32_t)3)))
		{
			case 0:
			{
				goto IL_002d;
			}
			case 1:
			{
				goto IL_003a;
			}
			case 2:
			{
				goto IL_0054;
			}
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)((int32_t)14))))
		{
			goto IL_0047;
		}
	}
	{
		goto IL_0060;
	}

IL_002d:
	{
		V_0 = 0;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_6 = ___texture0;
		NullCheck(L_6);
		int32_t L_7 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_6);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)3));
		goto IL_0060;
	}

IL_003a:
	{
		V_0 = 7;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_8 = ___texture0;
		NullCheck(L_8);
		int32_t L_9 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_8);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_9, (int32_t)4));
		goto IL_0060;
	}

IL_0047:
	{
		V_0 = 8;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_10 = ___texture0;
		NullCheck(L_10);
		int32_t L_11 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_10);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_11, (int32_t)4));
		goto IL_0060;
	}

IL_0054:
	{
		V_0 = ((int32_t)10);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_12 = ___texture0;
		NullCheck(L_12);
		int32_t L_13 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_12);
		V_1 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)4));
	}

IL_0060:
	{
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_14 = ___texture0;
		NullCheck(L_14);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_15 = Texture2D_GetRawTextureData_m387AAB1686E27DA77F4065A2111DF18934AFB364(L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_1;
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_17 = ___texture0;
		NullCheck(L_17);
		int32_t L_18 = VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_width() */, L_17);
		Texture2D_tBBF96AC337723E2EF156DF17E09D4379FD05DE1C * L_19 = ___texture0;
		NullCheck(L_19);
		int32_t L_20 = VirtFuncInvoker0< int32_t >::Invoke(7 /* System.Int32 UnityEngine.Texture::get_height() */, L_19);
		int32_t L_21 = V_0;
		int32_t L_22 = ___TJSubsampling2;
		int32_t L_23 = ___Quality1;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_24 = TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2(__this, L_15, L_16, L_18, L_20, L_21, L_22, L_23, 2, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJCompressor::EncodeFMJPG(System.Byte[],System.Int32,System.Int32,FMTurboJpegWrapper.TJPixelFormat,System.Int32,FMTurboJpegWrapper.TJSubsamplingOption)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJCompressor_EncodeFMJPG_m286309881E43E4E7DD480AFD688500CA182EDA16 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ____RawTextureData0, int32_t ____width1, int32_t ____height2, int32_t ____format3, int32_t ___Quality4, int32_t ___TJSubsampling5, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_0, (int32_t)3));
		int32_t L_1 = ____format3;
		if (!L_1)
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_2 = ____format3;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_2, (int32_t)7)))
		{
			case 0:
			{
				goto IL_0029;
			}
			case 1:
			{
				goto IL_002f;
			}
			case 2:
			{
				goto IL_0039;
			}
			case 3:
			{
				goto IL_0035;
			}
		}
	}
	{
		goto IL_0039;
	}

IL_0023:
	{
		int32_t L_3 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_3, (int32_t)3));
		goto IL_0039;
	}

IL_0029:
	{
		int32_t L_4 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_4, (int32_t)4));
		goto IL_0039;
	}

IL_002f:
	{
		int32_t L_5 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_5, (int32_t)4));
		goto IL_0039;
	}

IL_0035:
	{
		int32_t L_6 = ____width1;
		V_0 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_6, (int32_t)4));
	}

IL_0039:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = ____RawTextureData0;
		int32_t L_8 = V_0;
		int32_t L_9 = ____width1;
		int32_t L_10 = ____height2;
		int32_t L_11 = ____format3;
		int32_t L_12 = ___TJSubsampling5;
		int32_t L_13 = ___Quality4;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_14 = TJCompressor_Compress_mC6B8027F22F0093F40E468F658B0659DDE5DAEF2(__this, L_7, L_8, L_9, L_10, L_11, L_12, L_13, 2, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJCompressor_Dispose_mE9588F7C7681FE74A4E9508FCAD86F3B79F330A0_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		RuntimeObject * L_1 = __this->get_lock_0();
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_2, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = __this->get_isDisposed_2();
			if (!L_3)
			{
				goto IL_0020;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_0020:
		{
			VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean) */, __this, (bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x36, IL_0036)
	}

IL_0036:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_Dispose_m0CD2BEEDF78D19BB1B9C9E99B474A4FAAB117913 (TJCompressor_tD03C38497D02F41049735A573E8B820DEFAF3BBF * __this, bool ___callFromUserCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJCompressor_Dispose_m0CD2BEEDF78D19BB1B9C9E99B474A4FAAB117913_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___callFromUserCode0;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
	}

IL_000a:
	{
		intptr_t L_1 = __this->get_compressorHandle_1();
		bool L_2 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		intptr_t L_3 = __this->get_compressorHandle_1();
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78((intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_compressorHandle_1((intptr_t)(0));
	}

IL_0033:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJCompressor::CheckOptionsCompatibilityAndThrow(FMTurboJpegWrapper.TJSubsamplingOption,FMTurboJpegWrapper.TJPixelFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2 (int32_t ___subSamp0, int32_t ___srcFormat1, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		int32_t L_0 = ___srcFormat1;
		if ((!(((uint32_t)L_0) == ((uint32_t)6))))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_1 = ___subSamp0;
		if ((((int32_t)L_1) == ((int32_t)3)))
		{
			goto IL_0024;
		}
	}
	{
		int32_t L_2 = ((int32_t)3);
		RuntimeObject * L_3 = Box(TJSubsamplingOption_tCAB3CAA7FCB54ACDC4F8BC62AA1C0655B319D4B1_il2cpp_TypeInfo_var, &L_2);
		int32_t L_4 = ((int32_t)6);
		RuntimeObject * L_5 = Box(TJPixelFormat_tC481543CB0361DE9AA1F0207E1B8CFEAF456CB79_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Format_m19325298DBC61AAC016C16F7B3CF97A8A3DEA34A(_stringLiteral9237B62CFD61666EFD00565FAD7965E2FBAAE16B, L_3, L_5, /*hidden argument*/NULL);
		NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 * L_7 = (NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010 *)il2cpp_codegen_object_new(NotSupportedException_tE75B318D6590A02A5D9B29FD97409B1750FA0010_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_mD023A89A5C1F740F43F0A9CD6C49DC21230B3CEE(L_7, L_6, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_7, TJCompressor_CheckOptionsCompatibilityAndThrow_m97169C1ED90ED8BF1565C7EBDE6CCD8F46AB74F2_RuntimeMethod_var);
	}

IL_0024:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJDecompressor::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor__ctor_m4D8B252C5E85A808451964D637BC56DE344FE3F1_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		RuntimeObject * L_0 = (RuntimeObject *)il2cpp_codegen_object_new(RuntimeObject_il2cpp_TypeInfo_var);
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(L_0, /*hidden argument*/NULL);
		__this->set_lock_0(L_0);
		__this->set_decompressorHandle_1((intptr_t)(0));
		Object__ctor_m925ECA5E85CA100E3FB86A4F9E15C120E9A184C0(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		intptr_t L_1 = TurboJpegImport_TjInitDecompress_mFEFD01E2373F2F6F2277C1BB679B2B445C00EFCD(/*hidden argument*/NULL);
		__this->set_decompressorHandle_1((intptr_t)L_1);
		intptr_t L_2 = __this->get_decompressorHandle_1();
		bool L_3 = IntPtr_op_Equality_mEE8D9FD2DFE312BBAA8B4ED3BF7976B3142A5934((intptr_t)L_2, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_003e;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004(/*hidden argument*/NULL);
	}

IL_003e:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Finalize()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Finalize_m2CDEB66ADEECF9C2F7DD8312DC92B9D6DAF636FA (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, const RuntimeMethod* method)
{
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 1);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);

IL_0000:
	try
	{ // begin try (depth: 1)
		VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean) */, __this, (bool)0);
		IL2CPP_LEAVE(0x10, FINALLY_0009);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_0009;
	}

FINALLY_0009:
	{ // begin finally (depth: 1)
		Object_Finalize_m4015B7D3A44DE125C5FE34D7276CD4697C06F380(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(9)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(9)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x10, IL_0010)
	}

IL_0010:
	{
		return;
	}
}
// System.Byte[] FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, int32_t* ___width4, int32_t* ___height5, int32_t* ___stride6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	uint8_t* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B2_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B1_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B3_0 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* G_B4_0 = NULL;
	{
		intptr_t L_0 = ___jpegBuf0;
		uint64_t L_1 = ___jpegBufSize1;
		int32_t L_2 = ___destPixelFormat2;
		int32_t* L_3 = ___width4;
		int32_t* L_4 = ___height5;
		int32_t* L_5 = ___stride6;
		TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F(__this, (intptr_t)L_0, L_1, L_2, (int32_t*)L_3, (int32_t*)L_4, (int32_t*)L_5, (int32_t*)(&V_0), /*hidden argument*/NULL);
		int32_t L_6 = V_0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_7 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)SZArrayNew(ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821_il2cpp_TypeInfo_var, (uint32_t)L_6);
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_8 = L_7;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_9 = L_8;
		V_2 = L_9;
		G_B1_0 = L_8;
		if (!L_9)
		{
			G_B2_0 = L_8;
			goto IL_0021;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_2;
		NullCheck(L_10);
		G_B2_0 = G_B1_0;
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_10)->max_length)))))
		{
			G_B3_0 = G_B1_0;
			goto IL_0026;
		}
	}

IL_0021:
	{
		V_1 = (uint8_t*)(((uintptr_t)0));
		G_B4_0 = G_B2_0;
		goto IL_002f;
	}

IL_0026:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_11 = V_2;
		NullCheck(L_11);
		V_1 = (uint8_t*)(((uintptr_t)((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
		G_B4_0 = G_B3_0;
	}

IL_002f:
	{
		intptr_t L_12 = ___jpegBuf0;
		uint64_t L_13 = ___jpegBufSize1;
		uint8_t* L_14 = V_1;
		intptr_t L_15 = IntPtr_op_Explicit_m7F0C4B884FFB05BD231154CBDAEBCF1917019C21((void*)(void*)L_14, /*hidden argument*/NULL);
		int32_t L_16 = V_0;
		int32_t L_17 = ___destPixelFormat2;
		int32_t L_18 = ___flags3;
		int32_t* L_19 = ___width4;
		int32_t* L_20 = ___height5;
		int32_t* L_21 = ___stride6;
		TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E(__this, (intptr_t)L_12, L_13, (intptr_t)L_15, L_16, L_17, L_18, (int32_t*)L_19, (int32_t*)L_20, (int32_t*)L_21, /*hidden argument*/NULL);
		V_2 = (ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821*)NULL;
		return G_B4_0;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,System.IntPtr,System.Int32,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, intptr_t ___outBuf2, int32_t ___outBufSize3, int32_t ___destPixelFormat4, int32_t ___flags5, int32_t* ___width6, int32_t* ___height7, int32_t* ___stride8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteralC2543FFF3BFA6F144C2F06A7DE6CD10C0B650CAE, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E_RuntimeMethod_var);
	}

IL_0013:
	{
		intptr_t L_2 = __this->get_decompressorHandle_1();
		intptr_t L_3 = ___jpegBuf0;
		uint64_t L_4 = ___jpegBufSize1;
		int32_t* L_5 = ___width6;
		int32_t* L_6 = ___height7;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_7 = TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B((intptr_t)L_2, (intptr_t)L_3, L_4, (int32_t*)L_5, (int32_t*)L_6, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_7) == ((uint32_t)(-1)))))
		{
			goto IL_0030;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004(/*hidden argument*/NULL);
	}

IL_0030:
	{
		int32_t L_8 = ___destPixelFormat4;
		V_2 = L_8;
		int32_t* L_9 = ___stride8;
		int32_t* L_10 = ___width6;
		int32_t L_11 = *((int32_t*)L_10);
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_12 = ((TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var))->get_PixelSizes_0();
		int32_t L_13 = V_2;
		NullCheck(L_12);
		int32_t L_14 = Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02(L_12, L_13, /*hidden argument*/Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02_RuntimeMethod_var);
		*((int32_t*)L_9) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_11, (int32_t)L_14));
		int32_t* L_15 = ___stride8;
		int32_t L_16 = *((int32_t*)L_15);
		int32_t* L_17 = ___height7;
		int32_t L_18 = *((int32_t*)L_17);
		V_3 = ((int32_t)il2cpp_codegen_multiply((int32_t)L_16, (int32_t)L_18));
		int32_t L_19 = ___outBufSize3;
		int32_t L_20 = V_3;
		if ((((int32_t)L_19) >= ((int32_t)L_20)))
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_21 = Int32_ToString_m1863896DE712BF97C031D55B12E1583F1982DC02((int32_t*)(&___outBufSize3), /*hidden argument*/NULL);
		ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA * L_22 = (ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA *)il2cpp_codegen_object_new(ArgumentOutOfRangeException_t94D19DF918A54511AEDF4784C9A08741BAD1DEDA_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m6B36E60C989DC798A8B44556DB35960282B133A6(L_22, L_21, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_22, TJDecompressor_Decompress_mDC1642AB7440C7B1BDCEDEB76075AA2841B0091E_RuntimeMethod_var);
	}

IL_005f:
	{
		intptr_t L_23 = __this->get_decompressorHandle_1();
		intptr_t L_24 = ___jpegBuf0;
		uint64_t L_25 = ___jpegBufSize1;
		intptr_t L_26 = ___outBuf2;
		int32_t* L_27 = ___width6;
		int32_t L_28 = *((int32_t*)L_27);
		int32_t* L_29 = ___stride8;
		int32_t L_30 = *((int32_t*)L_29);
		int32_t* L_31 = ___height7;
		int32_t L_32 = *((int32_t*)L_31);
		int32_t L_33 = V_2;
		int32_t L_34 = ___flags5;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_35 = TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402((intptr_t)L_23, (intptr_t)L_24, L_25, (intptr_t)L_26, L_28, L_30, L_32, L_33, L_34, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_35) == ((uint32_t)(-1)))))
		{
			goto IL_0081;
		}
	}
	{
		TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004(/*hidden argument*/NULL);
	}

IL_0081:
	{
		return;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t ___flags3, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_3 = NULL;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteralC2543FFF3BFA6F144C2F06A7DE6CD10C0B650CAE, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A_RuntimeMethod_var);
	}

IL_0013:
	{
		intptr_t L_2 = ___jpegBuf0;
		uint64_t L_3 = ___jpegBufSize1;
		int32_t L_4 = ___destPixelFormat2;
		int32_t L_5 = ___flags3;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = TJDecompressor_Decompress_m9E6C55102D95C6AD86E62F8F42D34F15622D2B74(__this, (intptr_t)L_2, L_3, L_4, L_5, (int32_t*)(&V_0), (int32_t*)(&V_1), (int32_t*)(&V_2), /*hidden argument*/NULL);
		V_3 = L_6;
		int32_t L_7 = V_0;
		int32_t L_8 = V_1;
		int32_t L_9 = V_2;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_10 = V_3;
		int32_t L_11 = ___destPixelFormat2;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_12 = (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 *)il2cpp_codegen_object_new(DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32_il2cpp_TypeInfo_var);
		DecompressedImage__ctor_mAA66065D12D8A58743FC653804A5D9E02358F448(L_12, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::Decompress(System.Byte[],FMTurboJpegWrapper.TJPixelFormat,FMTurboJpegWrapper.TJFlags)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___jpegBuf0, int32_t ___destPixelFormat1, int32_t ___flags2, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	uint64_t V_0 = 0;
	uint8_t* V_1 = NULL;
	ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* V_2 = NULL;
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0013;
		}
	}
	{
		ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A * L_1 = (ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A *)il2cpp_codegen_object_new(ObjectDisposedException_tF68E471ECD1419AD7C51137B742837395F50B69A_il2cpp_TypeInfo_var);
		ObjectDisposedException__ctor_m8B5D23EA08E42BDE6BC5233CC666295F19BBD2F9(L_1, _stringLiteralC2543FFF3BFA6F144C2F06A7DE6CD10C0B650CAE, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_1, TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997_RuntimeMethod_var);
	}

IL_0013:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_2 = ___jpegBuf0;
		NullCheck(L_2);
		V_0 = (((int64_t)((int64_t)(((int32_t)((int32_t)(((RuntimeArray*)L_2)->max_length)))))));
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___jpegBuf0;
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_4 = L_3;
		V_2 = L_4;
		if (!L_4)
		{
			goto IL_0022;
		}
	}
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_5 = V_2;
		NullCheck(L_5);
		if ((((int32_t)((int32_t)(((RuntimeArray*)L_5)->max_length)))))
		{
			goto IL_0027;
		}
	}

IL_0022:
	{
		V_1 = (uint8_t*)(((uintptr_t)0));
		goto IL_0030;
	}

IL_0027:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_6 = V_2;
		NullCheck(L_6);
		V_1 = (uint8_t*)(((uintptr_t)((L_6)->GetAddressAt(static_cast<il2cpp_array_size_t>(0)))));
	}

IL_0030:
	{
		uint8_t* L_7 = V_1;
		intptr_t L_8 = IntPtr_op_Explicit_m7F0C4B884FFB05BD231154CBDAEBCF1917019C21((void*)(void*)L_7, /*hidden argument*/NULL);
		uint64_t L_9 = V_0;
		int32_t L_10 = ___destPixelFormat1;
		int32_t L_11 = ___flags2;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_12 = TJDecompressor_Decompress_m91D26F33076206E6D9A49F91714E93261AD9211A(__this, (intptr_t)L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// FMTurboJpegWrapper.DecompressedImage FMTurboJpegWrapper.TJDecompressor::DecodeFMJPG(System.Byte[],UnityEngine.TextureFormat)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * TJDecompressor_DecodeFMJPG_m7ED6ABC9A6096ACFF7C54535EBC9E5F3A5410911 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* ___jpegBuf0, int32_t ___format1, const RuntimeMethod* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		int32_t L_0 = ___format1;
		switch (((int32_t)il2cpp_codegen_subtract((int32_t)L_0, (int32_t)3)))
		{
			case 0:
			{
				goto IL_0022;
			}
			case 1:
			{
				goto IL_0026;
			}
			case 2:
			{
				goto IL_002e;
			}
		}
	}
	{
		int32_t L_1 = ___format1;
		if ((((int32_t)L_1) == ((int32_t)((int32_t)14))))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_2 = ___format1;
		if ((((int32_t)L_2) == ((int32_t)((int32_t)63))))
		{
			goto IL_0033;
		}
	}
	{
		goto IL_0035;
	}

IL_0022:
	{
		V_0 = 0;
		goto IL_0035;
	}

IL_0026:
	{
		V_0 = 7;
		goto IL_0035;
	}

IL_002a:
	{
		V_0 = 8;
		goto IL_0035;
	}

IL_002e:
	{
		V_0 = ((int32_t)10);
		goto IL_0035;
	}

IL_0033:
	{
		V_0 = 6;
	}

IL_0035:
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_3 = ___jpegBuf0;
		int32_t L_4 = V_0;
		DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * L_5 = TJDecompressor_Decompress_m86473DA296EDD015D2DFAC21A81FA48D9E300997(__this, L_3, L_4, 2, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::GetImageInfo(System.IntPtr,System.UInt64,FMTurboJpegWrapper.TJPixelFormat,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, intptr_t ___jpegBuf0, uint64_t ___jpegBufSize1, int32_t ___destPixelFormat2, int32_t* ___width3, int32_t* ___height4, int32_t* ___stride5, int32_t* ___bufSize6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_GetImageInfo_m8D0687EB3DEE02230CF40F16101FCFBD5A3C968F_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		intptr_t L_0 = __this->get_decompressorHandle_1();
		intptr_t L_1 = ___jpegBuf0;
		uint64_t L_2 = ___jpegBufSize1;
		int32_t* L_3 = ___width3;
		int32_t* L_4 = ___height4;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B((intptr_t)L_0, (intptr_t)L_1, L_2, (int32_t*)L_3, (int32_t*)L_4, (int32_t*)(&V_0), (int32_t*)(&V_1), /*hidden argument*/NULL);
		int32_t* L_5 = ___stride5;
		int32_t* L_6 = ___width3;
		int32_t L_7 = *((int32_t*)L_6);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_8 = ((TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var))->get_PixelSizes_0();
		int32_t L_9 = ___destPixelFormat2;
		NullCheck(L_8);
		int32_t L_10 = Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02(L_8, L_9, /*hidden argument*/Dictionary_2_get_Item_mAB9BDFBD90A4DA3FBB0D7E0D61C43F73EC069F02_RuntimeMethod_var);
		*((int32_t*)L_5) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_7, (int32_t)L_10));
		int32_t* L_11 = ___bufSize6;
		int32_t* L_12 = ___stride5;
		int32_t L_13 = *((int32_t*)L_12);
		int32_t* L_14 = ___height4;
		int32_t L_15 = *((int32_t*)L_14);
		*((int32_t*)L_11) = (int32_t)((int32_t)il2cpp_codegen_multiply((int32_t)L_13, (int32_t)L_15));
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Dispose_mFEC7D33570D2AA599792AF33E85A57BF49D33B95_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	RuntimeObject * V_0 = NULL;
	Exception_t * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	void* __leave_targets_storage = alloca(sizeof(int32_t) * 2);
	il2cpp::utils::LeaveTargetStack __leave_targets(__leave_targets_storage);
	NO_UNUSED_WARNING (__leave_targets);
	{
		bool L_0 = __this->get_isDisposed_2();
		if (!L_0)
		{
			goto IL_0009;
		}
	}
	{
		return;
	}

IL_0009:
	{
		RuntimeObject * L_1 = __this->get_lock_0();
		V_0 = L_1;
		RuntimeObject * L_2 = V_0;
		Monitor_Enter_m903755FCC479745619842CCDBF5E6355319FA102(L_2, /*hidden argument*/NULL);
	}

IL_0016:
	try
	{ // begin try (depth: 1)
		{
			bool L_3 = __this->get_isDisposed_2();
			if (!L_3)
			{
				goto IL_0020;
			}
		}

IL_001e:
		{
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}

IL_0020:
		{
			VirtActionInvoker1< bool >::Invoke(5 /* System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean) */, __this, (bool)1);
			IL2CPP_RUNTIME_CLASS_INIT(GC_tC1D7BD74E8F44ECCEF5CD2B5D84BFF9AAE02D01D_il2cpp_TypeInfo_var);
			GC_SuppressFinalize_m037319A9B95A5BA437E806DE592802225EE5B425(__this, /*hidden argument*/NULL);
			IL2CPP_LEAVE(0x36, FINALLY_002f);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t *)e.ex;
		goto FINALLY_002f;
	}

FINALLY_002f:
	{ // begin finally (depth: 1)
		RuntimeObject * L_4 = V_0;
		Monitor_Exit_m49A1E5356D984D0B934BB97A305E2E5E207225C2(L_4, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(47)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(47)
	{
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t *)
		IL2CPP_JUMP_TBL(0x36, IL_0036)
	}

IL_0036:
	{
		return;
	}
}
// System.Void FMTurboJpegWrapper.TJDecompressor::Dispose(System.Boolean)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJDecompressor_Dispose_m168AF7BF325AF4116571288FED73411FAC783242 (TJDecompressor_t11E02D983592968338AAEA674887A66D60C4B28C * __this, bool ___callFromUserCode0, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TJDecompressor_Dispose_m168AF7BF325AF4116571288FED73411FAC783242_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		bool L_0 = ___callFromUserCode0;
		if (!L_0)
		{
			goto IL_000a;
		}
	}
	{
		__this->set_isDisposed_2((bool)1);
	}

IL_000a:
	{
		intptr_t L_1 = __this->get_decompressorHandle_1();
		bool L_2 = IntPtr_op_Inequality_mB4886A806009EA825EFCC60CD2A7F6EB8E273A61((intptr_t)L_1, (intptr_t)(0), /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0033;
		}
	}
	{
		intptr_t L_3 = __this->get_decompressorHandle_1();
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78((intptr_t)L_3, /*hidden argument*/NULL);
		__this->set_decompressorHandle_1((intptr_t)(0));
	}

IL_0033:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TJUtils::GetErrorAndThrow()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TJUtils_GetErrorAndThrow_m78467D0E6A93118EA8DA242E09207F12D6F71004 (const RuntimeMethod* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void FMTurboJpegWrapper.TjSize::.ctor(System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232 (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___width0;
		TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_inline((TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 *)__this, L_0, /*hidden argument*/NULL);
		int32_t L_1 = ___height1;
		TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_inline((TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 *)__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232_AdjustorThunk (RuntimeObject * __this, int32_t ___width0, int32_t ___height1, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * _thisAdjusted = reinterpret_cast<TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 *>(__this + _offset);
	TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232(_thisAdjusted, ___width0, ___height1, method);
}
// System.Void FMTurboJpegWrapper.TjSize::set_Width(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * _thisAdjusted = reinterpret_cast<TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 *>(__this + _offset);
	TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_inline(_thisAdjusted, ___value0, method);
}
// System.Void FMTurboJpegWrapper.TjSize::set_Height(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947 (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_1(L_0);
		return;
	}
}
IL2CPP_EXTERN_C  void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_AdjustorThunk (RuntimeObject * __this, int32_t ___value0, const RuntimeMethod* method)
{
	int32_t _offset = 1;
	TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * _thisAdjusted = reinterpret_cast<TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 *>(__this + _offset);
	TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_inline(_thisAdjusted, ___value0, method);
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL tjInitCompress();
#endif
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitCompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitCompress_m5D481C98E7233C1F96A8EBD1B887B52323D7CFE1 (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjInitCompress", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(tjInitCompress)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjCompress2(intptr_t, intptr_t, int32_t, int32_t, int32_t, int32_t, intptr_t*, uint64_t*, int32_t, int32_t, int32_t);
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjCompress2(System.IntPtr,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.IntPtr&,System.UInt64&,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjCompress2_mE47CB34033396A32565D6C22162B0CA15A6DC9A9 (intptr_t ___handle0, intptr_t ___srcBuf1, int32_t ___width2, int32_t ___pitch3, int32_t ___height4, int32_t ___pixelFormat5, intptr_t* ___jpegBuf6, uint64_t* ___jpegSize7, int32_t ___jpegSubsamp8, int32_t ___jpegQual9, int32_t ___flags10, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, int32_t, int32_t, int32_t, int32_t, intptr_t*, uint64_t*, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(intptr_t*) + sizeof(uint64_t*) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjCompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjCompress2)(___handle0, ___srcBuf1, ___width2, ___pitch3, ___height4, ___pixelFormat5, ___jpegBuf6, ___jpegSize7, ___jpegSubsamp8, ___jpegQual9, ___flags10);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___srcBuf1, ___width2, ___pitch3, ___height4, ___pixelFormat5, ___jpegBuf6, ___jpegSize7, ___jpegSubsamp8, ___jpegQual9, ___flags10);
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C intptr_t DEFAULT_CALL tjInitDecompress();
#endif
// System.IntPtr FMTurboJpegWrapper.TurboJpegImport::TjInitDecompress()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR intptr_t TurboJpegImport_TjInitDecompress_mFEFD01E2373F2F6F2277C1BB679B2B445C00EFCD (const RuntimeMethod* method)
{
	typedef intptr_t (DEFAULT_CALL *PInvokeFunc) ();
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = 0;
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjInitDecompress", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	intptr_t returnValue = reinterpret_cast<PInvokeFunc>(tjInitDecompress)();
	#else
	intptr_t returnValue = il2cppPInvokeFunc();
	#endif

	return returnValue;
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = IntPtr_get_Size_m1342A61F11766A494F2F90D9B68CADAD62261929(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0021;
		}
	}
	{
		goto IL_0031;
	}

IL_0010:
	{
		intptr_t L_3 = ___handle0;
		intptr_t L_4 = ___jpegBuf1;
		uint64_t L_5 = ___jpegSize2;
		int32_t* L_6 = ___width3;
		int32_t* L_7 = ___height4;
		int32_t* L_8 = ___jpegSubsamp5;
		int32_t* L_9 = ___jpegColorspace6;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_10 = TurboJpegImport_TjDecompressHeader3_x86_mC406308CA370922203E3FB4464CE1DD78ACBEE3C((intptr_t)L_3, (intptr_t)L_4, (((int32_t)((uint32_t)L_5))), (int32_t*)L_6, (int32_t*)L_7, (int32_t*)L_8, (int32_t*)L_9, /*hidden argument*/NULL);
		return L_10;
	}

IL_0021:
	{
		intptr_t L_11 = ___handle0;
		intptr_t L_12 = ___jpegBuf1;
		uint64_t L_13 = ___jpegSize2;
		int32_t* L_14 = ___width3;
		int32_t* L_15 = ___height4;
		int32_t* L_16 = ___jpegSubsamp5;
		int32_t* L_17 = ___jpegColorspace6;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_18 = TurboJpegImport_TjDecompressHeader3_x64_m8ABE9493363F1014B8119E207F22BF353500688A((intptr_t)L_11, (intptr_t)L_12, L_13, (int32_t*)L_14, (int32_t*)L_15, (int32_t*)L_16, (int32_t*)L_17, /*hidden argument*/NULL);
		return L_18;
	}

IL_0031:
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_19 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_19, _stringLiteral0EC278C441CF646394EA7CD22D6CAADCAD2DE9CD, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_19, TurboJpegImport_TjDecompressHeader_m964326DFEB7A4291DA49245DA765DE43F95A0F9B_RuntimeMethod_var);
	}
}
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = IntPtr_get_Size_m1342A61F11766A494F2F90D9B68CADAD62261929(/*hidden argument*/NULL);
		V_0 = L_0;
		int32_t L_1 = V_0;
		if ((((int32_t)L_1) == ((int32_t)4)))
		{
			goto IL_0010;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)8)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_0039;
	}

IL_0010:
	{
		intptr_t L_3 = ___handle0;
		intptr_t L_4 = ___jpegBuf1;
		uint64_t L_5 = ___jpegSize2;
		intptr_t L_6 = ___dstBuf3;
		int32_t L_7 = ___width4;
		int32_t L_8 = ___pitch5;
		int32_t L_9 = ___height6;
		int32_t L_10 = ___pixelFormat7;
		int32_t L_11 = ___flags8;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_12 = TurboJpegImport_TjDecompress2_x86_m16C2D62F861785C4C2C3155444DCF363F1C992B1((intptr_t)L_3, (intptr_t)L_4, (((int32_t)((uint32_t)L_5))), (intptr_t)L_6, L_7, L_8, L_9, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}

IL_0025:
	{
		intptr_t L_13 = ___handle0;
		intptr_t L_14 = ___jpegBuf1;
		uint64_t L_15 = ___jpegSize2;
		intptr_t L_16 = ___dstBuf3;
		int32_t L_17 = ___width4;
		int32_t L_18 = ___pitch5;
		int32_t L_19 = ___height6;
		int32_t L_20 = ___pixelFormat7;
		int32_t L_21 = ___flags8;
		IL2CPP_RUNTIME_CLASS_INIT(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var);
		int32_t L_22 = TurboJpegImport_TjDecompress2_x64_mCF8035CD678FA2504EDD42B3C164855E1B437D0D((intptr_t)L_13, (intptr_t)L_14, L_15, (intptr_t)L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		return L_22;
	}

IL_0039:
	{
		InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 * L_23 = (InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1 *)il2cpp_codegen_object_new(InvalidOperationException_t0530E734D823F78310CAFAFA424CA5164D93A1F1_il2cpp_TypeInfo_var);
		InvalidOperationException__ctor_m72027D5F1D513C25C05137E203EEED8FD8297706(L_23, _stringLiteral0EC278C441CF646394EA7CD22D6CAADCAD2DE9CD, /*hidden argument*/NULL);
		IL2CPP_RAISE_MANAGED_EXCEPTION(L_23, TurboJpegImport_TjDecompress_mAC70114B9F98126F2523BA2DB67059E95D4EB402_RuntimeMethod_var);
	}
}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C void DEFAULT_CALL tjFree(intptr_t);
#endif
// System.Void FMTurboJpegWrapper.TurboJpegImport::TjFree(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport_TjFree_m75A67BF5A45741A79826A366D0584C652D55EFF5 (intptr_t ___buffer0, const RuntimeMethod* method)
{
	typedef void (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjFree", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	reinterpret_cast<PInvokeFunc>(tjFree)(___buffer0);
	#else
	il2cppPInvokeFunc(___buffer0);
	#endif

}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDestroy(intptr_t);
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDestroy(System.IntPtr)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDestroy_m5473094C663D24F4F510B4BEB72C069EC0AE6A78 (intptr_t ___handle0, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDestroy", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDestroy)(___handle0);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0);
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDecompressHeader3(intptr_t, intptr_t, uint32_t, int32_t*, int32_t*, int32_t*, int32_t*);
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x86(System.IntPtr,System.IntPtr,System.UInt32,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x86_mC406308CA370922203E3FB4464CE1DD78ACBEE3C (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint32_t, int32_t*, int32_t*, int32_t*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint32_t) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompressHeader3", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompressHeader3)(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompressHeader3_x64(System.IntPtr,System.IntPtr,System.UInt64,System.Int32&,System.Int32&,System.Int32&,System.Int32&)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompressHeader3_x64_m8ABE9493363F1014B8119E207F22BF353500688A (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, int32_t* ___width3, int32_t* ___height4, int32_t* ___jpegSubsamp5, int32_t* ___jpegColorspace6, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint64_t, int32_t*, int32_t*, int32_t*, int32_t*);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint64_t) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*) + sizeof(int32_t*);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompressHeader3", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompressHeader3)(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___width3, ___height4, ___jpegSubsamp5, ___jpegColorspace6);
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
IL2CPP_EXTERN_C int32_t DEFAULT_CALL tjDecompress2(intptr_t, intptr_t, uint32_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x86(System.IntPtr,System.IntPtr,System.UInt32,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x86_m16C2D62F861785C4C2C3155444DCF363F1C992B1 (intptr_t ___handle0, intptr_t ___jpegBuf1, uint32_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint32_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint32_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompress2)(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#endif

	return returnValue;
}
#if FORCE_PINVOKE_INTERNAL
#endif
// System.Int32 FMTurboJpegWrapper.TurboJpegImport::TjDecompress2_x64(System.IntPtr,System.IntPtr,System.UInt64,System.IntPtr,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR int32_t TurboJpegImport_TjDecompress2_x64_mCF8035CD678FA2504EDD42B3C164855E1B437D0D (intptr_t ___handle0, intptr_t ___jpegBuf1, uint64_t ___jpegSize2, intptr_t ___dstBuf3, int32_t ___width4, int32_t ___pitch5, int32_t ___height6, int32_t ___pixelFormat7, int32_t ___flags8, const RuntimeMethod* method)
{
	typedef int32_t (DEFAULT_CALL *PInvokeFunc) (intptr_t, intptr_t, uint64_t, intptr_t, int32_t, int32_t, int32_t, int32_t, int32_t);
	#if !FORCE_PINVOKE_INTERNAL
	static PInvokeFunc il2cppPInvokeFunc;
	if (il2cppPInvokeFunc == NULL)
	{
		int parameterSize = sizeof(intptr_t) + sizeof(intptr_t) + sizeof(uint64_t) + sizeof(intptr_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t) + sizeof(int32_t);
		il2cppPInvokeFunc = il2cpp_codegen_resolve_pinvoke<PInvokeFunc>(IL2CPP_NATIVE_STRING("turbojpeg"), "tjDecompress2", IL2CPP_CALL_DEFAULT, CHARSET_NOT_SPECIFIED, parameterSize, false);
		IL2CPP_ASSERT(il2cppPInvokeFunc != NULL);
	}
	#endif

	// Native function invocation
	#if FORCE_PINVOKE_INTERNAL
	int32_t returnValue = reinterpret_cast<PInvokeFunc>(tjDecompress2)(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#else
	int32_t returnValue = il2cppPInvokeFunc(___handle0, ___jpegBuf1, ___jpegSize2, ___dstBuf3, ___width4, ___pitch5, ___height6, ___pixelFormat7, ___flags8);
	#endif

	return returnValue;
}
// System.Void FMTurboJpegWrapper.TurboJpegImport::.cctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void TurboJpegImport__cctor_m14EDB1B829C6218B7400940F71BD02D5323240CD (const RuntimeMethod* method)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_method (TurboJpegImport__cctor_m14EDB1B829C6218B7400940F71BD02D5323240CD_MetadataUsageId);
		s_Il2CppMethodInitialized = true;
	}
	{
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_0 = (Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A *)il2cpp_codegen_object_new(Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m975E6ED49D776E4C04851FA34DFABF55A1EA395F(L_0, /*hidden argument*/Dictionary_2__ctor_m975E6ED49D776E4C04851FA34DFABF55A1EA395F_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_1 = L_0;
		NullCheck(L_1);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_1, 0, 3, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_2 = L_1;
		NullCheck(L_2);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_2, 1, 3, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_3 = L_2;
		NullCheck(L_3);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_3, 2, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_4 = L_3;
		NullCheck(L_4);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_4, 3, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_5 = L_4;
		NullCheck(L_5);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_5, 4, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_6 = L_5;
		NullCheck(L_6);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_6, 5, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_7 = L_6;
		NullCheck(L_7);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_7, 6, 1, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_8 = L_7;
		NullCheck(L_8);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_8, 7, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_9 = L_8;
		NullCheck(L_9);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_9, 8, 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_10 = L_9;
		NullCheck(L_10);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_10, ((int32_t)9), 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_11 = L_10;
		NullCheck(L_11);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_11, ((int32_t)10), 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		Dictionary_2_tAA3DC21908D0E59FCEF45434BB1C9D7E8509353A * L_12 = L_11;
		NullCheck(L_12);
		Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444(L_12, ((int32_t)11), 4, /*hidden argument*/Dictionary_2_Add_m4B3F0CE57E8BFC6257EA8BF427EF5F6E2C99F444_RuntimeMethod_var);
		((TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var))->set_PixelSizes_0(L_12);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_13 = (Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 *)il2cpp_codegen_object_new(Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m0831B7243D8A1B9DFF515B83573FB3A13C3743C4(L_13, /*hidden argument*/Dictionary_2__ctor_m0831B7243D8A1B9DFF515B83573FB3A13C3743C4_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_14 = L_13;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_15;
		memset((&L_15), 0, sizeof(L_15));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_15), 8, 8, /*hidden argument*/NULL);
		NullCheck(L_14);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_14, 3, L_15, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_16 = L_14;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_17;
		memset((&L_17), 0, sizeof(L_17));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_17), 8, 8, /*hidden argument*/NULL);
		NullCheck(L_16);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_16, 0, L_17, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_18 = L_16;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_19;
		memset((&L_19), 0, sizeof(L_19));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_19), ((int32_t)16), 8, /*hidden argument*/NULL);
		NullCheck(L_18);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_18, 1, L_19, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_20 = L_18;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_21;
		memset((&L_21), 0, sizeof(L_21));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_21), ((int32_t)16), ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_20);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_20, 2, L_21, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_22 = L_20;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_23;
		memset((&L_23), 0, sizeof(L_23));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_23), 8, ((int32_t)16), /*hidden argument*/NULL);
		NullCheck(L_22);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_22, 4, L_23, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		Dictionary_2_t30D5013F34E6B32C6E8A21704322831647FE9472 * L_24 = L_22;
		TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567  L_25;
		memset((&L_25), 0, sizeof(L_25));
		TjSize__ctor_m7B8C165C5C4D1EA1A0D620090009A108CC47E232((&L_25), ((int32_t)32), 8, /*hidden argument*/NULL);
		NullCheck(L_24);
		Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472(L_24, 5, L_25, /*hidden argument*/Dictionary_2_Add_mBB9A029CC62912F8528CBC70012C084CD1721472_RuntimeMethod_var);
		((TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var))->set_MCUSizes_1(L_24);
		((TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_StaticFields*)il2cpp_codegen_static_fields_for(TurboJpegImport_tB9B53A424E27ED7FD7AC4C2D03F66BF05A12099C_il2cpp_TypeInfo_var))->set__LibraryFound_2((bool)1);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Width_m28E97284BEE7AABEE346C37AB2B653C5DCB68F41_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CWidthU3Ek__BackingField_1();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR int32_t DecompressedImage_get_Height_m23F1B559B563E40BB5F7302B1123091BED05EC1D_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		int32_t L_0 = __this->get_U3CHeightU3Ek__BackingField_2();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* DecompressedImage_get_Data_m17C18FCD1EAEA85CE25493A5E49C4C90EF0B9BC8_inline (DecompressedImage_t863F7C25D92D5E454E5186E1E0E6606B85EDAF32 * __this, const RuntimeMethod* method)
{
	{
		ByteU5BU5D_tD06FDBE8142446525DF1C40351D523A228373821* L_0 = __this->get_U3CDataU3Ek__BackingField_4();
		return L_0;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TjSize_set_Width_mDC8AF3E56CBCEC9B883EE3328F28503DD821ADBE_inline (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CWidthU3Ek__BackingField_0(L_0);
		return;
	}
}
IL2CPP_EXTERN_C inline  IL2CPP_METHOD_ATTR void TjSize_set_Height_m5ADFF67306C13F2915A2DFE400A315598F89F947_inline (TjSize_tCA406DEBE2B726C832B90D07D2B2A7105F41E567 * __this, int32_t ___value0, const RuntimeMethod* method)
{
	{
		int32_t L_0 = ___value0;
		__this->set_U3CHeightU3Ek__BackingField_1(L_0);
		return;
	}
}
