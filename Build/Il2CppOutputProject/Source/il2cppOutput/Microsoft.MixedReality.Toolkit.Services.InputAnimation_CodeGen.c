﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::get_IsRecording()
// 0x00000002 System.Boolean Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::get_UseBufferTimeLimit()
// 0x00000003 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::set_UseBufferTimeLimit(System.Boolean)
// 0x00000004 System.Single Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::get_RecordingBufferTimeLimit()
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::set_RecordingBufferTimeLimit(System.Single)
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::StartRecording()
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::StopRecording()
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::DiscardRecordedInput()
// 0x00000009 System.String Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::SaveInputAnimation(System.String)
// 0x0000000A System.String Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::SaveInputAnimation(System.String,System.String)
// 0x0000000B System.Threading.Tasks.Task`1<System.String> Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::SaveInputAnimationAsync(System.String)
// 0x0000000C System.Threading.Tasks.Task`1<System.String> Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputRecordingService::SaveInputAnimationAsync(System.String,System.String)
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker::.ctor()
extern void InputAnimationMarker__ctor_mF807AC51C0BB945E8740E33207C67664A22ED6AB (void);
// 0x0000000E System.Single Microsoft.MixedReality.Toolkit.Input.InputAnimation::get_Duration()
extern void InputAnimation_get_Duration_m8C40D88E465307AC0D755626D11D9E25A9A8766E (void);
// 0x0000000F System.Boolean Microsoft.MixedReality.Toolkit.Input.InputAnimation::get_HasHandData()
extern void InputAnimation_get_HasHandData_m3FCF5BCBB87845D2AC6BBD284E5914B232F4ECA1 (void);
// 0x00000010 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::set_HasHandData(System.Boolean)
extern void InputAnimation_set_HasHandData_mCDD07190AAD70F78823A119F67D9F6C75FC4D2FB (void);
// 0x00000011 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputAnimation::get_HasCameraPose()
extern void InputAnimation_get_HasCameraPose_m9BAD60080B344EED0C3F4D94BD923053DB019DCC (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::set_HasCameraPose(System.Boolean)
extern void InputAnimation_set_HasCameraPose_m1FC35FA4BB999A405A966704360DEF1EAC7EA44A (void);
// 0x00000013 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputAnimation::get_HasEyeGaze()
extern void InputAnimation_get_HasEyeGaze_m7F346DF4CF54AAB582880D62198F7D2242E9BBF3 (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::set_HasEyeGaze(System.Boolean)
extern void InputAnimation_set_HasEyeGaze_m0686251C4352508F9B0343098281DCFBE3323672 (void);
// 0x00000015 System.Int32 Microsoft.MixedReality.Toolkit.Input.InputAnimation::get_markerCount()
extern void InputAnimation_get_markerCount_mD3657D6002092AA3D2F02A0A4B1E7C7B9397CF05 (void);
// 0x00000016 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::.ctor()
extern void InputAnimation__ctor_mE2E1D380E1678A03D08EA1F6D9EBA9BC233243EE (void);
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddHandStateKey(System.Single,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Boolean,System.Boolean)
extern void InputAnimation_AddHandStateKey_m89A822D4B66C0B5CB2A491F5DD59B62B00B0CAF9 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddHandJointKey(System.Single,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose,System.Single,System.Single)
extern void InputAnimation_AddHandJointKey_mD65B9BB9E7D033DBD9C16990C3B7CAAB8AE898EE (void);
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddCameraPoseKey(System.Single,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose,System.Single,System.Single)
extern void InputAnimation_AddCameraPoseKey_mAF423E423F3EA16E634A26AE7ED064081F88B111 (void);
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddMarker(Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker)
extern void InputAnimation_AddMarker_mD3EDD16333E580E2DECD8FADCC0CB4DC2E1B4389 (void);
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::RemoveMarker(System.Int32)
extern void InputAnimation_RemoveMarker_mEA66678B8DA10668D62E7045476E9CE095C60AE8 (void);
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::SetMarkerTime(System.Int32,System.Single)
extern void InputAnimation_SetMarkerTime_m0B7B8054DE56B1169F5732CF8F65EDBDE5FDA914 (void);
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::Clear()
extern void InputAnimation_Clear_m575CDC27C40E5AE3D7A1777784E799EBAE4DEB06 (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::CutoffBeforeTime(System.Single)
extern void InputAnimation_CutoffBeforeTime_m68788994A349E15878666CB8D1324195CC0F4FE4 (void);
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::ToStream(System.IO.Stream,System.Single)
extern void InputAnimation_ToStream_m05101624825098721E98B74E8B074EEC8AE6593C (void);
// 0x00000020 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Input.InputAnimation::ToStreamAsync(System.IO.Stream,System.Single,System.Action)
extern void InputAnimation_ToStreamAsync_m26B25DC51C0705E8F532EFCA8A91CB508EACA469 (void);
// 0x00000021 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateHandState(System.Single,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Boolean&,System.Boolean&)
extern void InputAnimation_EvaluateHandState_m9931497DC7A90AFAA55E62E0328593A015FCF819 (void);
// 0x00000022 System.Int32 Microsoft.MixedReality.Toolkit.Input.InputAnimation::FindMarkerInterval(System.Single)
extern void InputAnimation_FindMarkerInterval_m5749D089B81D85DDEFCF1FAE7A9022E5E0B42B4E (void);
// 0x00000023 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateCameraPose(System.Single)
extern void InputAnimation_EvaluateCameraPose_m989D4C8D79158C9CCB26265B3725ADC6E34696F1 (void);
// 0x00000024 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateHandJoint(System.Single,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint)
extern void InputAnimation_EvaluateHandJoint_m504F1E22BAF66CBE354BF9B3EF075D69D97DC5EE (void);
// 0x00000025 UnityEngine.Ray Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateEyeGaze(System.Single)
extern void InputAnimation_EvaluateEyeGaze_mCCDDDD961E4CC3B4A29A7C8D754CB2D6757A3759 (void);
// 0x00000026 Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker Microsoft.MixedReality.Toolkit.Input.InputAnimation::GetMarker(System.Int32)
extern void InputAnimation_GetMarker_m7E5DA7D82181AEC0CC258BB60ED29363B7DC9B3A (void);
// 0x00000027 Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputAnimation::FromRecordingBuffer(Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile)
extern void InputAnimation_FromRecordingBuffer_m19BFCA1505F76B57E22AA52A2C5DF54E6FE6A806 (void);
// 0x00000028 Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputAnimation::FromStream(System.IO.Stream)
extern void InputAnimation_FromStream_mFC80BA2537E503A80F5409D51EA83088CADF7BFA (void);
// 0x00000029 System.Threading.Tasks.Task`1<Microsoft.MixedReality.Toolkit.Input.InputAnimation> Microsoft.MixedReality.Toolkit.Input.InputAnimation::FromStreamAsync(System.IO.Stream,System.Action)
extern void InputAnimation_FromStreamAsync_m1E97FFB3DD96CDAA1E123EC5F11EEABFD9466597 (void);
// 0x0000002A System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddHandStateKey(System.Single,System.Boolean,System.Boolean,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve)
extern void InputAnimation_AddHandStateKey_mF6CF84C8A951807840702D312810ECDDF047F1D7 (void);
// 0x0000002B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddHandJointKey(System.Single,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose,System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves>,System.Single,System.Single)
extern void InputAnimation_AddHandJointKey_m90C0A4B915A7B72673A6CA0DB4AAF76D748B4589 (void);
// 0x0000002C System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::CutoffBeforeTime(UnityEngine.AnimationCurve,System.Single)
extern void InputAnimation_CutoffBeforeTime_mD125005B4426626752D172E8A2A57F30CD6C866A (void);
// 0x0000002D Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves Microsoft.MixedReality.Toolkit.Input.InputAnimation::CreateHandJointCurves(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint)
extern void InputAnimation_CreateHandJointCurves_m0ACC2363D019F7B27805221AD37215E5FA76DED0 (void);
// 0x0000002E System.Boolean Microsoft.MixedReality.Toolkit.Input.InputAnimation::TryGetHandJointCurves(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves&)
extern void InputAnimation_TryGetHandJointCurves_m1A2524B1008E60C54C264BE4AAD76398AB920F3F (void);
// 0x0000002F System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::ComputeDuration()
extern void InputAnimation_ComputeDuration_m434F3943A920E9716EF9EE862F9C8B64AE92591B (void);
// 0x00000030 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::Optimize(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile)
extern void InputAnimation_Optimize_m75AA1F020FF3C8F60B34E8AC8FFE205392B84009 (void);
// 0x00000031 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateHandState(System.Single,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Boolean&,System.Boolean&)
extern void InputAnimation_EvaluateHandState_m495D1638D7EADB35F6A4EA9F27F54E860C34153C (void);
// 0x00000032 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.InputAnimation::EvaluateHandJoint(System.Single,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves>)
extern void InputAnimation_EvaluateHandJoint_m53ADB7EE57B7827AF2294D80CEE0F039943F0D11 (void);
// 0x00000033 System.Collections.Generic.IEnumerable`1<UnityEngine.AnimationCurve> Microsoft.MixedReality.Toolkit.Input.InputAnimation::GetAllAnimationCurves()
extern void InputAnimation_GetAllAnimationCurves_mAF06A8AC3DB6ADC4F494E61D0EA508D2BD2E11EF (void);
// 0x00000034 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddBoolKey(UnityEngine.AnimationCurve,System.Single,System.Boolean)
extern void InputAnimation_AddBoolKey_m6BBCB120D9A5ED3380CD4F71FD73C5494D8798C6 (void);
// 0x00000035 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddFloatKey(UnityEngine.AnimationCurve,System.Single,System.Single)
extern void InputAnimation_AddFloatKey_m33EA14781AC55B0075B3B1CF392F7C88BA8A87F2 (void);
// 0x00000036 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddVectorKey(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single,UnityEngine.Vector3)
extern void InputAnimation_AddVectorKey_m78E86C018B6CC1D80FC2FDF85624328675F447E6 (void);
// 0x00000037 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddPoseKeyFiltered(Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves,System.Single,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose,System.Single,System.Single)
extern void InputAnimation_AddPoseKeyFiltered_mD85B623E76A86B37113CD77E49D1700BE3F0DA7F (void);
// 0x00000038 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddPositionKeyFiltered(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single,UnityEngine.Vector3,System.Single)
extern void InputAnimation_AddPositionKeyFiltered_m1DF9746FB7377C54ADBD5E63F2C1233E4185BC4D (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddRotationKeyFiltered(UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,UnityEngine.AnimationCurve,System.Single,UnityEngine.Quaternion,System.Single)
extern void InputAnimation_AddRotationKeyFiltered_m1E69361638E281A05E1E9E46DED931C261C746DC (void);
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::PoseCurvesToStream(System.IO.BinaryWriter,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves,System.Single)
extern void InputAnimation_PoseCurvesToStream_m7F8B246849E443B1D35B10570F3C350DAE98A3B8 (void);
// 0x0000003B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::PoseCurvesFromStream(System.IO.BinaryReader,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves,System.Boolean)
extern void InputAnimation_PoseCurvesFromStream_m19295BF02D6A974FFCCC3BAF8BBDB3838B62B153 (void);
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::RayCurvesToStream(System.IO.BinaryWriter,Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves,System.Single)
extern void InputAnimation_RayCurvesToStream_m86A3EB92357829DB729579892C2FBEC7911BD98C (void);
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::RayCurvesFromStream(System.IO.BinaryReader,Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves,System.Boolean)
extern void InputAnimation_RayCurvesFromStream_m43E8730F4D119DDF3FB2D50A9DEABA545C88BBA0 (void);
// 0x0000003E System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::OptimizePositionCurve(UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,System.Single,System.Int32)
extern void InputAnimation_OptimizePositionCurve_m8ED3FB5C05D1BFC04AD4D42D111809BACE303A3D (void);
// 0x0000003F System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::OptimizeDirectionCurve(UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,System.Single,System.Int32)
extern void InputAnimation_OptimizeDirectionCurve_mF12C7EE37C8E6368A03E3557F59FC8FB31D003E6 (void);
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::OptimizeRotationCurve(UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,UnityEngine.AnimationCurve&,System.Single,System.Int32)
extern void InputAnimation_OptimizeRotationCurve_mB685A284333B7AF6D7C44F1A31C1E1D339908E77 (void);
// 0x00000041 System.Int32 Microsoft.MixedReality.Toolkit.Input.InputAnimation::AddBoolKeyFiltered(UnityEngine.AnimationCurve,System.Single,System.Boolean)
extern void InputAnimation_AddBoolKeyFiltered_m0F6CB3CDDFC9FEF2EBE563BBA7C41E66E132377E (void);
// 0x00000042 System.Int32 Microsoft.MixedReality.Toolkit.Input.InputAnimation::FindKeyframeInterval(UnityEngine.AnimationCurve,System.Single)
extern void InputAnimation_FindKeyframeInterval_m0106B3766CF7E27D3F5741BF269BE5B79216B797 (void);
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::<FromRecordingBuffer>g__AddBoolKeyIfChanged|47_0(UnityEngine.AnimationCurve,System.Single,System.Boolean)
extern void InputAnimation_U3CFromRecordingBufferU3Eg__AddBoolKeyIfChangedU7C47_0_m0CABEB4EAFD1851F3F8B9E20FFD06E7C6B12DF6B (void);
// 0x00000044 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::<FromRecordingBuffer>g__AddJointPoseKeys|47_1(System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves>,System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,System.Single)
extern void InputAnimation_U3CFromRecordingBufferU3Eg__AddJointPoseKeysU7C47_1_mB76D0D0E1667F52BAB3689085F2E9C68A2846B97 (void);
// 0x00000045 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::<OptimizePositionCurve>g__Recurse|70_0(System.Int32,System.Int32,Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass70_0&)
extern void InputAnimation_U3COptimizePositionCurveU3Eg__RecurseU7C70_0_m14E307A0C7B5E54F45326BEC0978498A0F52E97C (void);
// 0x00000046 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::<OptimizeDirectionCurve>g__Recurse|71_0(System.Int32,System.Int32,Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass71_0&)
extern void InputAnimation_U3COptimizeDirectionCurveU3Eg__RecurseU7C71_0_mDB2F32B2E070A2CADA38FAFEEF4583ACDD16CE7F (void);
// 0x00000047 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation::<OptimizeRotationCurve>g__Recurse|72_0(System.Int32,System.Int32,Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass72_0&)
extern void InputAnimation_U3COptimizeRotationCurveU3Eg__RecurseU7C72_0_mE30798C1C8A56A776901E73D7898B4DF31F223DD (void);
// 0x00000048 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves::AddKey(System.Single,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void PoseCurves_AddKey_m7E43A52366F67705AC4C9CC9AC5B98D9C9CF4311 (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves::Optimize(System.Single,System.Single,System.Int32)
extern void PoseCurves_Optimize_mA3393A7E6CE08B159FC271B78B85F408F39AEB92 (void);
// 0x0000004A Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves::Evaluate(System.Single)
extern void PoseCurves_Evaluate_m4747FF92D87F4518F85955F7827D575686D388AC (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/PoseCurves::.ctor()
extern void PoseCurves__ctor_m8D037E3EC7E228EBC45EC01233AF9B88A9D2170F (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves::AddKey(System.Single,UnityEngine.Ray)
extern void RayCurves_AddKey_m1647C3F5656A932A7CEFED83C6ADAFE0818BA35D (void);
// 0x0000004D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves::Optimize(System.Single,System.Single,System.Int32)
extern void RayCurves_Optimize_m0B520B773CE862DD6273D43DDBF662A163BA91F7 (void);
// 0x0000004E UnityEngine.Ray Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves::Evaluate(System.Single)
extern void RayCurves_Evaluate_m4294B12FF80D867E1B5BF1115C8AF665158FB18E (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/RayCurves::.ctor()
extern void RayCurves__ctor_m5C420A45F65025B9F57F408021E5F2AD670AA3CD (void);
// 0x00000050 System.Int32 Microsoft.MixedReality.Toolkit.Input.InputAnimation/CompareMarkers::Compare(Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker,Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker)
extern void CompareMarkers_Compare_mF06A5FFAAE798B9BE763DF83DE2CD63EC10554D6 (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/CompareMarkers::.ctor()
extern void CompareMarkers__ctor_m1055675F5A3F7EA153DC3BA813DFC68FE33145E1 (void);
// 0x00000052 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_mA546F9CC0BCE849114CC121F986145BE39D34F9F (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass40_0::<ToStreamAsync>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CToStreamAsyncU3Eb__0_mC2BA9A068B980F3C0C8A3A5839596AB92279B913 (void);
// 0x00000054 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<ToStreamAsync>d__40::MoveNext()
extern void U3CToStreamAsyncU3Ed__40_MoveNext_mA51D608F4A510232FA0F29CCAD6926F053531C60 (void);
// 0x00000055 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<ToStreamAsync>d__40::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CToStreamAsyncU3Ed__40_SetStateMachine_m11A93B6443934B38F68D3BEC17DBA7AAE2C44EEA (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass49_0::.ctor()
extern void U3CU3Ec__DisplayClass49_0__ctor_m7A400702B064BDC685D4F6A323EE07CADD151E88 (void);
// 0x00000057 Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputAnimation/<>c__DisplayClass49_0::<FromStreamAsync>b__0()
extern void U3CU3Ec__DisplayClass49_0_U3CFromStreamAsyncU3Eb__0_m040DC67949EA05C16BF5DB06D9D55EEB8CFCC3BD (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<FromStreamAsync>d__49::MoveNext()
extern void U3CFromStreamAsyncU3Ed__49_MoveNext_mEC81E08BDAFA0CDAA04B7636C40817427BA48079 (void);
// 0x00000059 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<FromStreamAsync>d__49::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CFromStreamAsyncU3Ed__49_SetStateMachine_mFF1AAB8E5A75C2A22913D08FB5EAA756BA6CD27C (void);
// 0x0000005A System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::.ctor(System.Int32)
extern void U3CGetAllAnimationCurvesU3Ed__59__ctor_mC783AA7A24B44A5844BC01E131FBAE5455C170B6 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.IDisposable.Dispose()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_IDisposable_Dispose_m1B600E5D947C1B49A952CC762464EAD13A975505 (void);
// 0x0000005C System.Boolean Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::MoveNext()
extern void U3CGetAllAnimationCurvesU3Ed__59_MoveNext_m7E5F6673072E9E66B0FAA949C9B011CE9A981E41 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::<>m__Finally1()
extern void U3CGetAllAnimationCurvesU3Ed__59_U3CU3Em__Finally1_m622B59BF685BC3D3A641A44BE677953E86655515 (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::<>m__Finally2()
extern void U3CGetAllAnimationCurvesU3Ed__59_U3CU3Em__Finally2_m5498358EB3567B69E4CA6CE6B7FDC5BEFD85B944 (void);
// 0x0000005F UnityEngine.AnimationCurve Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.Collections.Generic.IEnumerator<UnityEngine.AnimationCurve>.get_Current()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_Collections_Generic_IEnumeratorU3CUnityEngine_AnimationCurveU3E_get_Current_m89FD1BDA5D4831F95D88A14B5F5A1E5B90AB1C03 (void);
// 0x00000060 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.Collections.IEnumerator.Reset()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerator_Reset_mF655B45F0166326ACC3F9E121DC2EFCDC0E7ECDF (void);
// 0x00000061 System.Object Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerator_get_Current_m65D755059E5D7A10B758E2160EC8C2A6A74756D5 (void);
// 0x00000062 System.Collections.Generic.IEnumerator`1<UnityEngine.AnimationCurve> Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.Collections.Generic.IEnumerable<UnityEngine.AnimationCurve>.GetEnumerator()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_Collections_Generic_IEnumerableU3CUnityEngine_AnimationCurveU3E_GetEnumerator_m2CBDC061F18BF96BD6F70EFA5918136AB97AAF96 (void);
// 0x00000063 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Input.InputAnimation/<GetAllAnimationCurves>d__59::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerable_GetEnumerator_m9715117C792DAC9C041BDC20A4CE09C90ACC27D7 (void);
// 0x00000064 System.String Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::GetOutputFilename(System.String,System.Boolean)
extern void InputAnimationSerializationUtils_GetOutputFilename_m93E617765D634074E4F85AF31DF93C292F74AB45 (void);
// 0x00000065 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteHeader(System.IO.BinaryWriter)
extern void InputAnimationSerializationUtils_WriteHeader_m550ED59292FC47DA2A881AF1079D272AEF3C342C (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadHeader(System.IO.BinaryReader,System.Int32&,System.Int32&)
extern void InputAnimationSerializationUtils_ReadHeader_mAFA586698BA5B9543987D5013BE9C8FBABC34F34 (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteFloatCurve(System.IO.BinaryWriter,UnityEngine.AnimationCurve,System.Single)
extern void InputAnimationSerializationUtils_WriteFloatCurve_m5C3F456A21BF1BDEFA1603E5797593ADC1AFE4D4 (void);
// 0x00000068 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadFloatCurve(System.IO.BinaryReader,UnityEngine.AnimationCurve)
extern void InputAnimationSerializationUtils_ReadFloatCurve_m9F64A31BFC3E326325A80D2F6A44CF21780937FB (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteBoolCurve(System.IO.BinaryWriter,UnityEngine.AnimationCurve,System.Single)
extern void InputAnimationSerializationUtils_WriteBoolCurve_mEDF86EAA26CC0E20A59C09939A5E9D00EB519A22 (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadBoolCurve(System.IO.BinaryReader,UnityEngine.AnimationCurve)
extern void InputAnimationSerializationUtils_ReadBoolCurve_mB5D6915255B33D37AA377FED199B3722E0F0E4E3 (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteFloatCurveSimple(System.IO.BinaryWriter,UnityEngine.AnimationCurve,System.Single)
extern void InputAnimationSerializationUtils_WriteFloatCurveSimple_m8311E7E977B11110308FF2BFB9A21344EC850FF3 (void);
// 0x0000006C System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadFloatCurveSimple(System.IO.BinaryReader,UnityEngine.AnimationCurve)
extern void InputAnimationSerializationUtils_ReadFloatCurveSimple_m527FE954754C835AD2F1FD6D98A29084771616CF (void);
// 0x0000006D System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteFloatCurveArray(System.IO.BinaryWriter,UnityEngine.AnimationCurve[],System.Single)
extern void InputAnimationSerializationUtils_WriteFloatCurveArray_m4CBEA49F792CBB02749E3C4B5F519697E08D66D3 (void);
// 0x0000006E System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadFloatCurveArray(System.IO.BinaryReader,UnityEngine.AnimationCurve[])
extern void InputAnimationSerializationUtils_ReadFloatCurveArray_mA5503F788336A6AFC99895EDA36228964C58C02F (void);
// 0x0000006F System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteBoolCurveArray(System.IO.BinaryWriter,UnityEngine.AnimationCurve[],System.Single)
extern void InputAnimationSerializationUtils_WriteBoolCurveArray_m06669EEA3F9D7BC09E828228B8DA666A9AEE8CF0 (void);
// 0x00000070 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadBoolCurveArray(System.IO.BinaryReader,UnityEngine.AnimationCurve[])
extern void InputAnimationSerializationUtils_ReadBoolCurveArray_m4BF85A1B7552C5945C193A2DD2DA429116A9610E (void);
// 0x00000071 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::WriteMarkerList(System.IO.BinaryWriter,System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker>,System.Single)
extern void InputAnimationSerializationUtils_WriteMarkerList_mA01633833DB61B8BCCC8588AE69E7E6A8DE3ECF4 (void);
// 0x00000072 System.Void Microsoft.MixedReality.Toolkit.Input.InputAnimationSerializationUtils::ReadMarkerList(System.IO.BinaryReader,System.Collections.Generic.List`1<Microsoft.MixedReality.Toolkit.Input.InputAnimationMarker>)
extern void InputAnimationSerializationUtils_ReadMarkerList_m1AF0C912535088EB5FF4BC50673681F3C97F8A84 (void);
// 0x00000073 System.Single Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::get_StartTime()
extern void InputRecordingBuffer_get_StartTime_m005F658E58475C0A8CA404652888602097220CD4 (void);
// 0x00000074 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::.ctor()
extern void InputRecordingBuffer__ctor_m1580E0519A45105DC679F3A7BE75ADD54096CDF2 (void);
// 0x00000075 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::Clear()
extern void InputRecordingBuffer_Clear_mDB828FDDA816096516E5B9938E1374D24BFB28CE (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::RemoveBeforeTime(System.Single)
extern void InputRecordingBuffer_RemoveBeforeTime_mF4052834FA65002A474F0F5E8D326EC90C8ECA6C (void);
// 0x00000077 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::SetCameraPose(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void InputRecordingBuffer_SetCameraPose_mB4F1587C14CDBD82E5A0382E3C20DEC1E44965E6 (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::SetGazeRay(UnityEngine.Ray)
extern void InputRecordingBuffer_SetGazeRay_m7A99235CCEE349C40AF521350867F0CC16BCB64D (void);
// 0x00000079 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::SetHandState(Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Boolean,System.Boolean)
extern void InputRecordingBuffer_SetHandState_m047A46C66AB4D6F1CDA40D34628F7D826BCD1BD2 (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::SetJointPose(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void InputRecordingBuffer_SetJointPose_m5976E7C130360F2108A747DE3BDD7D6D293010EA (void);
// 0x0000007B System.Int32 Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::NewKeyframe(System.Single)
extern void InputRecordingBuffer_NewKeyframe_m005894110549A9011ADEB686D5ED5A45B5B068E6 (void);
// 0x0000007C System.Collections.Generic.IEnumerator`1<Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe> Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::GetEnumerator()
extern void InputRecordingBuffer_GetEnumerator_m37691944AF3B704E3E5AD448F3DF78E06683D43F (void);
// 0x0000007D System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer::System.Collections.IEnumerable.GetEnumerator()
extern void InputRecordingBuffer_System_Collections_IEnumerable_GetEnumerator_m10534C2D27641326D613A1837D55E4844A085117 (void);
// 0x0000007E System.Single Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_Time()
extern void Keyframe_get_Time_mD7AE30DD6234278C30EB399B8A21F62CDC97C0F3 (void);
// 0x0000007F System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_Time(System.Single)
extern void Keyframe_set_Time_mE3F89DE003F57C800106894C72B009838BB34127 (void);
// 0x00000080 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_LeftTracked()
extern void Keyframe_get_LeftTracked_m8E9E9E70AFF2B8E55BA40B3E7D8EBE944201AF7D (void);
// 0x00000081 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_LeftTracked(System.Boolean)
extern void Keyframe_set_LeftTracked_m373514CAECB817996027C5A07D4906101375E304 (void);
// 0x00000082 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_RightTracked()
extern void Keyframe_get_RightTracked_mAA2263F01DC83042D8E39D9679B41E55E5632A38 (void);
// 0x00000083 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_RightTracked(System.Boolean)
extern void Keyframe_set_RightTracked_mE4DFB93601C0DA2A0078BCBB139ADD5D1E9A7220 (void);
// 0x00000084 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_LeftPinch()
extern void Keyframe_get_LeftPinch_m33DE1DC92EA44E5C736ECC19C74D2C4B29591D8A (void);
// 0x00000085 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_LeftPinch(System.Boolean)
extern void Keyframe_set_LeftPinch_mF8ED9CC91CB9FB253675F60E2E376F38C9D13B8A (void);
// 0x00000086 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_RightPinch()
extern void Keyframe_get_RightPinch_m95C0A6F5055E34A449BE4A3E14B353E7A47BEBD7 (void);
// 0x00000087 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_RightPinch(System.Boolean)
extern void Keyframe_set_RightPinch_mA0E8AAE5F6D3A068E5470446415C1CC4BC028446 (void);
// 0x00000088 Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_CameraPose()
extern void Keyframe_get_CameraPose_mFFD39193EFCF763ED366EB72504D29B6381FC488 (void);
// 0x00000089 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_CameraPose(Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void Keyframe_set_CameraPose_mCD70C223BBF2B50A39A398EA50E9A0A8C585C268 (void);
// 0x0000008A UnityEngine.Ray Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_GazeRay()
extern void Keyframe_get_GazeRay_m992F99F86D8FBE77153A0E8DD7DA10FA4F7DC0DC (void);
// 0x0000008B System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_GazeRay(UnityEngine.Ray)
extern void Keyframe_set_GazeRay_mE2F693BD3A0B51650A042716AC2EF0E019D4C06A (void);
// 0x0000008C System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose> Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_LeftJoints()
extern void Keyframe_get_LeftJoints_m112474C51AA680008C2B8A4C315FDC03811E1F12 (void);
// 0x0000008D System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_LeftJoints(System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>)
extern void Keyframe_set_LeftJoints_mE792DB8FEC61B99EC45E22B732B7F6882C196124 (void);
// 0x0000008E System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose> Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::get_RightJoints()
extern void Keyframe_get_RightJoints_mB537ACA5022E3E0E43DCBAE760F5C3504B234A11 (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::set_RightJoints(System.Collections.Generic.Dictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>)
extern void Keyframe_set_RightJoints_mE4AF646365E412A004DF7BCA42BC2349E0977573 (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingBuffer/Keyframe::.ctor(System.Single)
extern void Keyframe__ctor_m7CF2FE7CBC7834101A68634EBA8DCFEAB28038AE (void);
// 0x00000091 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::add_OnRecordingStarted(System.Action)
extern void InputRecordingService_add_OnRecordingStarted_m9FCB6C2C7241B5E66E2A77D7717EF83EA8C98C4D (void);
// 0x00000092 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::remove_OnRecordingStarted(System.Action)
extern void InputRecordingService_remove_OnRecordingStarted_m20F11FEE2D39D63BC9FFCD23A5D7FB1C90F369EC (void);
// 0x00000093 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::add_OnRecordingStopped(System.Action)
extern void InputRecordingService_add_OnRecordingStopped_m1D106560391D1DEDDDE90C1AFC74E4B58DC0DBF0 (void);
// 0x00000094 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::remove_OnRecordingStopped(System.Action)
extern void InputRecordingService_remove_OnRecordingStopped_m90D65A781CA38F74BDBE82D8183E2EA63DCFD665 (void);
// 0x00000095 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_IsRecording()
extern void InputRecordingService_get_IsRecording_mB057EA4523757C8D68B09CB9576208E0E8CA0A45 (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::set_IsRecording(System.Boolean)
extern void InputRecordingService_set_IsRecording_m843D9B99C309F3A8DAD452AEB3B3E165D6DA1FA7 (void);
// 0x00000097 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_UseBufferTimeLimit()
extern void InputRecordingService_get_UseBufferTimeLimit_mF45D3FB95AF4D0E5467AC7B8DE45875D8ECE2CAA (void);
// 0x00000098 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::set_UseBufferTimeLimit(System.Boolean)
extern void InputRecordingService_set_UseBufferTimeLimit_m945B197359948697909F46DEA5A24FBE20474C6E (void);
// 0x00000099 System.Single Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_RecordingBufferTimeLimit()
extern void InputRecordingService_get_RecordingBufferTimeLimit_mA3E33396045F5FFEB8EDAE17BDE9053E52D45C0B (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::set_RecordingBufferTimeLimit(System.Single)
extern void InputRecordingService_set_RecordingBufferTimeLimit_mC94881F0FE0F8E56D8538D1E1B5B4E746A8777AA (void);
// 0x0000009B System.Single Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_StartTime()
extern void InputRecordingService_get_StartTime_m85287E85269EA87B2AF0B540B802B63DD6C96B4C (void);
// 0x0000009C System.Single Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_EndTime()
extern void InputRecordingService_get_EndTime_mE19E70EF187D95FDCEDC6CB0009A2582D8646280 (void);
// 0x0000009D System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::set_EndTime(System.Single)
extern void InputRecordingService_set_EndTime_mABFC85BB48B12E8EE78908356EE44C7CE9FDC163 (void);
// 0x0000009E Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile Microsoft.MixedReality.Toolkit.Input.InputRecordingService::get_InputRecordingProfile()
extern void InputRecordingService_get_InputRecordingProfile_mB8BD4F1AF2E262A38DDF585AAACAEA2BEDA5AEDA (void);
// 0x0000009F System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::set_InputRecordingProfile(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile)
extern void InputRecordingService_set_InputRecordingProfile_m5EEFCD36612DBA36FA179119103B8A6F73EC1DFA (void);
// 0x000000A0 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputRecordingService__ctor_m4470F536140384D4C501AA2BC8A635719057AF46 (void);
// 0x000000A1 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSystem,System.String,System.UInt32,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void InputRecordingService__ctor_m45CC36E9594217C129440B4D533179DF25957D11 (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::Enable()
extern void InputRecordingService_Enable_m1F10DFBCCA5C5528F07FF7EF2E97CADC48AA9452 (void);
// 0x000000A3 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::Disable()
extern void InputRecordingService_Disable_mFDD2F6FF6228FB1FA586B697B8F6BEBC13E3E1AC (void);
// 0x000000A4 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::StartRecording()
extern void InputRecordingService_StartRecording_m93C047EFC0E7BA4F4A53BC5A5290B0C4F281D6CB (void);
// 0x000000A5 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::StopRecording()
extern void InputRecordingService_StopRecording_m44C56045591E4DE87869545FA125A128B98E0BD9 (void);
// 0x000000A6 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::LateUpdate()
extern void InputRecordingService_LateUpdate_m12C533C022ED796FFA5CD7FF97222A92EC884E25 (void);
// 0x000000A7 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::DiscardRecordedInput()
extern void InputRecordingService_DiscardRecordedInput_mCFB5DD4C68426909DC0F78D56C45D16D44859008 (void);
// 0x000000A8 System.String Microsoft.MixedReality.Toolkit.Input.InputRecordingService::SaveInputAnimation(System.String)
extern void InputRecordingService_SaveInputAnimation_m4F22E7353D9AA9CC159412DF85E8922851FDD1CB (void);
// 0x000000A9 System.String Microsoft.MixedReality.Toolkit.Input.InputRecordingService::SaveInputAnimation(System.String,System.String)
extern void InputRecordingService_SaveInputAnimation_mD637C6ABA7EEB9759026F3E75BC94F7136F09CF3 (void);
// 0x000000AA System.Threading.Tasks.Task`1<System.String> Microsoft.MixedReality.Toolkit.Input.InputRecordingService::SaveInputAnimationAsync(System.String)
extern void InputRecordingService_SaveInputAnimationAsync_mEEAD18B15B450158E7111AAEFD412F75605B68CE (void);
// 0x000000AB System.Threading.Tasks.Task`1<System.String> Microsoft.MixedReality.Toolkit.Input.InputRecordingService::SaveInputAnimationAsync(System.String,System.String)
extern void InputRecordingService_SaveInputAnimationAsync_mCB8DE096220B7DC9662F175A4851207CE970176D (void);
// 0x000000AC System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::ResetStartTime()
extern void InputRecordingService_ResetStartTime_m0397DE83F1512F600949CA4056DFB68FE30FA4FF (void);
// 0x000000AD System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::RecordKeyframe()
extern void InputRecordingService_RecordKeyframe_m197F92AB9FCA6907323AC5435B969B2178013B2E (void);
// 0x000000AE System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::RecordInputHandData(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void InputRecordingService_RecordInputHandData_mF7D4D53FC556DED8A364A3B580D157110EE221CC (void);
// 0x000000AF System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService::PruneBuffer()
extern void InputRecordingService_PruneBuffer_mAC5A0226F1BADFD152EAFBE1C14BF4CD380E55C6 (void);
// 0x000000B0 Microsoft.MixedReality.Toolkit.Input.InputAnimation Microsoft.MixedReality.Toolkit.Input.InputRecordingService::<SaveInputAnimationAsync>b__44_0()
extern void InputRecordingService_U3CSaveInputAnimationAsyncU3Eb__44_0_m3EB98B2F374BD5A2952D7CB757583817A6F3850A (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService/<SaveInputAnimationAsync>d__44::MoveNext()
extern void U3CSaveInputAnimationAsyncU3Ed__44_MoveNext_m4DA7E724F562C6D7B2423E448303AFA90E188519 (void);
// 0x000000B2 System.Void Microsoft.MixedReality.Toolkit.Input.InputRecordingService/<SaveInputAnimationAsync>d__44::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CSaveInputAnimationAsyncU3Ed__44_SetStateMachine_m490A41A67D294E505D9AE708E3BBE957952714BE (void);
// 0x000000B3 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_FrameRate()
extern void MixedRealityInputRecordingProfile_get_FrameRate_m0ACAA23CFE665980AD426584CCEB0F82ECD602D4 (void);
// 0x000000B4 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_RecordHandData()
extern void MixedRealityInputRecordingProfile_get_RecordHandData_m3E8F47E035ADEFA2FA41721F8555B309D98DDF6C (void);
// 0x000000B5 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_JointPositionThreshold()
extern void MixedRealityInputRecordingProfile_get_JointPositionThreshold_m4B64573233A1D7238FDE9D44D1A41DD0446690F9 (void);
// 0x000000B6 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_JointRotationThreshold()
extern void MixedRealityInputRecordingProfile_get_JointRotationThreshold_m17BAA5D4973466E0F77EAEA912C3BCB1B25A2241 (void);
// 0x000000B7 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_RecordCameraPose()
extern void MixedRealityInputRecordingProfile_get_RecordCameraPose_m114BC393084F333736F61DC1654F5BC723821D8D (void);
// 0x000000B8 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_CameraPositionThreshold()
extern void MixedRealityInputRecordingProfile_get_CameraPositionThreshold_m675A20054E46C8F32450BA01CF8C48469395CF70 (void);
// 0x000000B9 System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_CameraRotationThreshold()
extern void MixedRealityInputRecordingProfile_get_CameraRotationThreshold_mEDE1563C6B195F9FB616653AD3B1A69EBC1C8555 (void);
// 0x000000BA System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_RecordEyeGaze()
extern void MixedRealityInputRecordingProfile_get_RecordEyeGaze_m04D78057C7DF6020A8D8B57C1D5D63B08D08876C (void);
// 0x000000BB System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_EyeGazeOriginThreshold()
extern void MixedRealityInputRecordingProfile_get_EyeGazeOriginThreshold_mC80FCEC3A01A52DF1535EF2432C80CB012B23FE6 (void);
// 0x000000BC System.Single Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_EyeGazeDirectionThreshold()
extern void MixedRealityInputRecordingProfile_get_EyeGazeDirectionThreshold_m8D4F9E1B3980ED514AB5C20F7709D26CCAA8CF8F (void);
// 0x000000BD System.Int32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::get_PartitionSize()
extern void MixedRealityInputRecordingProfile_get_PartitionSize_m4305A449BAF615F6859F64AEFEB219CC8D61858C (void);
// 0x000000BE System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputRecordingProfile::.ctor()
extern void MixedRealityInputRecordingProfile__ctor_mA316C2E09FA56A17B4BFD21F50ACDA07578F52DB (void);
static Il2CppMethodPointer s_methodPointers[190] = 
{
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	InputAnimationMarker__ctor_mF807AC51C0BB945E8740E33207C67664A22ED6AB,
	InputAnimation_get_Duration_m8C40D88E465307AC0D755626D11D9E25A9A8766E,
	InputAnimation_get_HasHandData_m3FCF5BCBB87845D2AC6BBD284E5914B232F4ECA1,
	InputAnimation_set_HasHandData_mCDD07190AAD70F78823A119F67D9F6C75FC4D2FB,
	InputAnimation_get_HasCameraPose_m9BAD60080B344EED0C3F4D94BD923053DB019DCC,
	InputAnimation_set_HasCameraPose_m1FC35FA4BB999A405A966704360DEF1EAC7EA44A,
	InputAnimation_get_HasEyeGaze_m7F346DF4CF54AAB582880D62198F7D2242E9BBF3,
	InputAnimation_set_HasEyeGaze_m0686251C4352508F9B0343098281DCFBE3323672,
	InputAnimation_get_markerCount_mD3657D6002092AA3D2F02A0A4B1E7C7B9397CF05,
	InputAnimation__ctor_mE2E1D380E1678A03D08EA1F6D9EBA9BC233243EE,
	InputAnimation_AddHandStateKey_m89A822D4B66C0B5CB2A491F5DD59B62B00B0CAF9,
	InputAnimation_AddHandJointKey_mD65B9BB9E7D033DBD9C16990C3B7CAAB8AE898EE,
	InputAnimation_AddCameraPoseKey_mAF423E423F3EA16E634A26AE7ED064081F88B111,
	InputAnimation_AddMarker_mD3EDD16333E580E2DECD8FADCC0CB4DC2E1B4389,
	InputAnimation_RemoveMarker_mEA66678B8DA10668D62E7045476E9CE095C60AE8,
	InputAnimation_SetMarkerTime_m0B7B8054DE56B1169F5732CF8F65EDBDE5FDA914,
	InputAnimation_Clear_m575CDC27C40E5AE3D7A1777784E799EBAE4DEB06,
	InputAnimation_CutoffBeforeTime_m68788994A349E15878666CB8D1324195CC0F4FE4,
	InputAnimation_ToStream_m05101624825098721E98B74E8B074EEC8AE6593C,
	InputAnimation_ToStreamAsync_m26B25DC51C0705E8F532EFCA8A91CB508EACA469,
	InputAnimation_EvaluateHandState_m9931497DC7A90AFAA55E62E0328593A015FCF819,
	InputAnimation_FindMarkerInterval_m5749D089B81D85DDEFCF1FAE7A9022E5E0B42B4E,
	InputAnimation_EvaluateCameraPose_m989D4C8D79158C9CCB26265B3725ADC6E34696F1,
	InputAnimation_EvaluateHandJoint_m504F1E22BAF66CBE354BF9B3EF075D69D97DC5EE,
	InputAnimation_EvaluateEyeGaze_mCCDDDD961E4CC3B4A29A7C8D754CB2D6757A3759,
	InputAnimation_GetMarker_m7E5DA7D82181AEC0CC258BB60ED29363B7DC9B3A,
	InputAnimation_FromRecordingBuffer_m19BFCA1505F76B57E22AA52A2C5DF54E6FE6A806,
	InputAnimation_FromStream_mFC80BA2537E503A80F5409D51EA83088CADF7BFA,
	InputAnimation_FromStreamAsync_m1E97FFB3DD96CDAA1E123EC5F11EEABFD9466597,
	InputAnimation_AddHandStateKey_mF6CF84C8A951807840702D312810ECDDF047F1D7,
	InputAnimation_AddHandJointKey_m90C0A4B915A7B72673A6CA0DB4AAF76D748B4589,
	InputAnimation_CutoffBeforeTime_mD125005B4426626752D172E8A2A57F30CD6C866A,
	InputAnimation_CreateHandJointCurves_m0ACC2363D019F7B27805221AD37215E5FA76DED0,
	InputAnimation_TryGetHandJointCurves_m1A2524B1008E60C54C264BE4AAD76398AB920F3F,
	InputAnimation_ComputeDuration_m434F3943A920E9716EF9EE862F9C8B64AE92591B,
	InputAnimation_Optimize_m75AA1F020FF3C8F60B34E8AC8FFE205392B84009,
	InputAnimation_EvaluateHandState_m495D1638D7EADB35F6A4EA9F27F54E860C34153C,
	InputAnimation_EvaluateHandJoint_m53ADB7EE57B7827AF2294D80CEE0F039943F0D11,
	InputAnimation_GetAllAnimationCurves_mAF06A8AC3DB6ADC4F494E61D0EA508D2BD2E11EF,
	InputAnimation_AddBoolKey_m6BBCB120D9A5ED3380CD4F71FD73C5494D8798C6,
	InputAnimation_AddFloatKey_m33EA14781AC55B0075B3B1CF392F7C88BA8A87F2,
	InputAnimation_AddVectorKey_m78E86C018B6CC1D80FC2FDF85624328675F447E6,
	InputAnimation_AddPoseKeyFiltered_mD85B623E76A86B37113CD77E49D1700BE3F0DA7F,
	InputAnimation_AddPositionKeyFiltered_m1DF9746FB7377C54ADBD5E63F2C1233E4185BC4D,
	InputAnimation_AddRotationKeyFiltered_m1E69361638E281A05E1E9E46DED931C261C746DC,
	InputAnimation_PoseCurvesToStream_m7F8B246849E443B1D35B10570F3C350DAE98A3B8,
	InputAnimation_PoseCurvesFromStream_m19295BF02D6A974FFCCC3BAF8BBDB3838B62B153,
	InputAnimation_RayCurvesToStream_m86A3EB92357829DB729579892C2FBEC7911BD98C,
	InputAnimation_RayCurvesFromStream_m43E8730F4D119DDF3FB2D50A9DEABA545C88BBA0,
	InputAnimation_OptimizePositionCurve_m8ED3FB5C05D1BFC04AD4D42D111809BACE303A3D,
	InputAnimation_OptimizeDirectionCurve_mF12C7EE37C8E6368A03E3557F59FC8FB31D003E6,
	InputAnimation_OptimizeRotationCurve_mB685A284333B7AF6D7C44F1A31C1E1D339908E77,
	InputAnimation_AddBoolKeyFiltered_m0F6CB3CDDFC9FEF2EBE563BBA7C41E66E132377E,
	InputAnimation_FindKeyframeInterval_m0106B3766CF7E27D3F5741BF269BE5B79216B797,
	InputAnimation_U3CFromRecordingBufferU3Eg__AddBoolKeyIfChangedU7C47_0_m0CABEB4EAFD1851F3F8B9E20FFD06E7C6B12DF6B,
	InputAnimation_U3CFromRecordingBufferU3Eg__AddJointPoseKeysU7C47_1_mB76D0D0E1667F52BAB3689085F2E9C68A2846B97,
	InputAnimation_U3COptimizePositionCurveU3Eg__RecurseU7C70_0_m14E307A0C7B5E54F45326BEC0978498A0F52E97C,
	InputAnimation_U3COptimizeDirectionCurveU3Eg__RecurseU7C71_0_mDB2F32B2E070A2CADA38FAFEEF4583ACDD16CE7F,
	InputAnimation_U3COptimizeRotationCurveU3Eg__RecurseU7C72_0_mE30798C1C8A56A776901E73D7898B4DF31F223DD,
	PoseCurves_AddKey_m7E43A52366F67705AC4C9CC9AC5B98D9C9CF4311,
	PoseCurves_Optimize_mA3393A7E6CE08B159FC271B78B85F408F39AEB92,
	PoseCurves_Evaluate_m4747FF92D87F4518F85955F7827D575686D388AC,
	PoseCurves__ctor_m8D037E3EC7E228EBC45EC01233AF9B88A9D2170F,
	RayCurves_AddKey_m1647C3F5656A932A7CEFED83C6ADAFE0818BA35D,
	RayCurves_Optimize_m0B520B773CE862DD6273D43DDBF662A163BA91F7,
	RayCurves_Evaluate_m4294B12FF80D867E1B5BF1115C8AF665158FB18E,
	RayCurves__ctor_m5C420A45F65025B9F57F408021E5F2AD670AA3CD,
	CompareMarkers_Compare_mF06A5FFAAE798B9BE763DF83DE2CD63EC10554D6,
	CompareMarkers__ctor_m1055675F5A3F7EA153DC3BA813DFC68FE33145E1,
	U3CU3Ec__DisplayClass40_0__ctor_mA546F9CC0BCE849114CC121F986145BE39D34F9F,
	U3CU3Ec__DisplayClass40_0_U3CToStreamAsyncU3Eb__0_mC2BA9A068B980F3C0C8A3A5839596AB92279B913,
	U3CToStreamAsyncU3Ed__40_MoveNext_mA51D608F4A510232FA0F29CCAD6926F053531C60,
	U3CToStreamAsyncU3Ed__40_SetStateMachine_m11A93B6443934B38F68D3BEC17DBA7AAE2C44EEA,
	U3CU3Ec__DisplayClass49_0__ctor_m7A400702B064BDC685D4F6A323EE07CADD151E88,
	U3CU3Ec__DisplayClass49_0_U3CFromStreamAsyncU3Eb__0_m040DC67949EA05C16BF5DB06D9D55EEB8CFCC3BD,
	U3CFromStreamAsyncU3Ed__49_MoveNext_mEC81E08BDAFA0CDAA04B7636C40817427BA48079,
	U3CFromStreamAsyncU3Ed__49_SetStateMachine_mFF1AAB8E5A75C2A22913D08FB5EAA756BA6CD27C,
	U3CGetAllAnimationCurvesU3Ed__59__ctor_mC783AA7A24B44A5844BC01E131FBAE5455C170B6,
	U3CGetAllAnimationCurvesU3Ed__59_System_IDisposable_Dispose_m1B600E5D947C1B49A952CC762464EAD13A975505,
	U3CGetAllAnimationCurvesU3Ed__59_MoveNext_m7E5F6673072E9E66B0FAA949C9B011CE9A981E41,
	U3CGetAllAnimationCurvesU3Ed__59_U3CU3Em__Finally1_m622B59BF685BC3D3A641A44BE677953E86655515,
	U3CGetAllAnimationCurvesU3Ed__59_U3CU3Em__Finally2_m5498358EB3567B69E4CA6CE6B7FDC5BEFD85B944,
	U3CGetAllAnimationCurvesU3Ed__59_System_Collections_Generic_IEnumeratorU3CUnityEngine_AnimationCurveU3E_get_Current_m89FD1BDA5D4831F95D88A14B5F5A1E5B90AB1C03,
	U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerator_Reset_mF655B45F0166326ACC3F9E121DC2EFCDC0E7ECDF,
	U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerator_get_Current_m65D755059E5D7A10B758E2160EC8C2A6A74756D5,
	U3CGetAllAnimationCurvesU3Ed__59_System_Collections_Generic_IEnumerableU3CUnityEngine_AnimationCurveU3E_GetEnumerator_m2CBDC061F18BF96BD6F70EFA5918136AB97AAF96,
	U3CGetAllAnimationCurvesU3Ed__59_System_Collections_IEnumerable_GetEnumerator_m9715117C792DAC9C041BDC20A4CE09C90ACC27D7,
	InputAnimationSerializationUtils_GetOutputFilename_m93E617765D634074E4F85AF31DF93C292F74AB45,
	InputAnimationSerializationUtils_WriteHeader_m550ED59292FC47DA2A881AF1079D272AEF3C342C,
	InputAnimationSerializationUtils_ReadHeader_mAFA586698BA5B9543987D5013BE9C8FBABC34F34,
	InputAnimationSerializationUtils_WriteFloatCurve_m5C3F456A21BF1BDEFA1603E5797593ADC1AFE4D4,
	InputAnimationSerializationUtils_ReadFloatCurve_m9F64A31BFC3E326325A80D2F6A44CF21780937FB,
	InputAnimationSerializationUtils_WriteBoolCurve_mEDF86EAA26CC0E20A59C09939A5E9D00EB519A22,
	InputAnimationSerializationUtils_ReadBoolCurve_mB5D6915255B33D37AA377FED199B3722E0F0E4E3,
	InputAnimationSerializationUtils_WriteFloatCurveSimple_m8311E7E977B11110308FF2BFB9A21344EC850FF3,
	InputAnimationSerializationUtils_ReadFloatCurveSimple_m527FE954754C835AD2F1FD6D98A29084771616CF,
	InputAnimationSerializationUtils_WriteFloatCurveArray_m4CBEA49F792CBB02749E3C4B5F519697E08D66D3,
	InputAnimationSerializationUtils_ReadFloatCurveArray_mA5503F788336A6AFC99895EDA36228964C58C02F,
	InputAnimationSerializationUtils_WriteBoolCurveArray_m06669EEA3F9D7BC09E828228B8DA666A9AEE8CF0,
	InputAnimationSerializationUtils_ReadBoolCurveArray_m4BF85A1B7552C5945C193A2DD2DA429116A9610E,
	InputAnimationSerializationUtils_WriteMarkerList_mA01633833DB61B8BCCC8588AE69E7E6A8DE3ECF4,
	InputAnimationSerializationUtils_ReadMarkerList_m1AF0C912535088EB5FF4BC50673681F3C97F8A84,
	InputRecordingBuffer_get_StartTime_m005F658E58475C0A8CA404652888602097220CD4,
	InputRecordingBuffer__ctor_m1580E0519A45105DC679F3A7BE75ADD54096CDF2,
	InputRecordingBuffer_Clear_mDB828FDDA816096516E5B9938E1374D24BFB28CE,
	InputRecordingBuffer_RemoveBeforeTime_mF4052834FA65002A474F0F5E8D326EC90C8ECA6C,
	InputRecordingBuffer_SetCameraPose_mB4F1587C14CDBD82E5A0382E3C20DEC1E44965E6,
	InputRecordingBuffer_SetGazeRay_m7A99235CCEE349C40AF521350867F0CC16BCB64D,
	InputRecordingBuffer_SetHandState_m047A46C66AB4D6F1CDA40D34628F7D826BCD1BD2,
	InputRecordingBuffer_SetJointPose_m5976E7C130360F2108A747DE3BDD7D6D293010EA,
	InputRecordingBuffer_NewKeyframe_m005894110549A9011ADEB686D5ED5A45B5B068E6,
	InputRecordingBuffer_GetEnumerator_m37691944AF3B704E3E5AD448F3DF78E06683D43F,
	InputRecordingBuffer_System_Collections_IEnumerable_GetEnumerator_m10534C2D27641326D613A1837D55E4844A085117,
	Keyframe_get_Time_mD7AE30DD6234278C30EB399B8A21F62CDC97C0F3,
	Keyframe_set_Time_mE3F89DE003F57C800106894C72B009838BB34127,
	Keyframe_get_LeftTracked_m8E9E9E70AFF2B8E55BA40B3E7D8EBE944201AF7D,
	Keyframe_set_LeftTracked_m373514CAECB817996027C5A07D4906101375E304,
	Keyframe_get_RightTracked_mAA2263F01DC83042D8E39D9679B41E55E5632A38,
	Keyframe_set_RightTracked_mE4DFB93601C0DA2A0078BCBB139ADD5D1E9A7220,
	Keyframe_get_LeftPinch_m33DE1DC92EA44E5C736ECC19C74D2C4B29591D8A,
	Keyframe_set_LeftPinch_mF8ED9CC91CB9FB253675F60E2E376F38C9D13B8A,
	Keyframe_get_RightPinch_m95C0A6F5055E34A449BE4A3E14B353E7A47BEBD7,
	Keyframe_set_RightPinch_mA0E8AAE5F6D3A068E5470446415C1CC4BC028446,
	Keyframe_get_CameraPose_mFFD39193EFCF763ED366EB72504D29B6381FC488,
	Keyframe_set_CameraPose_mCD70C223BBF2B50A39A398EA50E9A0A8C585C268,
	Keyframe_get_GazeRay_m992F99F86D8FBE77153A0E8DD7DA10FA4F7DC0DC,
	Keyframe_set_GazeRay_mE2F693BD3A0B51650A042716AC2EF0E019D4C06A,
	Keyframe_get_LeftJoints_m112474C51AA680008C2B8A4C315FDC03811E1F12,
	Keyframe_set_LeftJoints_mE792DB8FEC61B99EC45E22B732B7F6882C196124,
	Keyframe_get_RightJoints_mB537ACA5022E3E0E43DCBAE760F5C3504B234A11,
	Keyframe_set_RightJoints_mE4AF646365E412A004DF7BCA42BC2349E0977573,
	Keyframe__ctor_m7CF2FE7CBC7834101A68634EBA8DCFEAB28038AE,
	InputRecordingService_add_OnRecordingStarted_m9FCB6C2C7241B5E66E2A77D7717EF83EA8C98C4D,
	InputRecordingService_remove_OnRecordingStarted_m20F11FEE2D39D63BC9FFCD23A5D7FB1C90F369EC,
	InputRecordingService_add_OnRecordingStopped_m1D106560391D1DEDDDE90C1AFC74E4B58DC0DBF0,
	InputRecordingService_remove_OnRecordingStopped_m90D65A781CA38F74BDBE82D8183E2EA63DCFD665,
	InputRecordingService_get_IsRecording_mB057EA4523757C8D68B09CB9576208E0E8CA0A45,
	InputRecordingService_set_IsRecording_m843D9B99C309F3A8DAD452AEB3B3E165D6DA1FA7,
	InputRecordingService_get_UseBufferTimeLimit_mF45D3FB95AF4D0E5467AC7B8DE45875D8ECE2CAA,
	InputRecordingService_set_UseBufferTimeLimit_m945B197359948697909F46DEA5A24FBE20474C6E,
	InputRecordingService_get_RecordingBufferTimeLimit_mA3E33396045F5FFEB8EDAE17BDE9053E52D45C0B,
	InputRecordingService_set_RecordingBufferTimeLimit_mC94881F0FE0F8E56D8538D1E1B5B4E746A8777AA,
	InputRecordingService_get_StartTime_m85287E85269EA87B2AF0B540B802B63DD6C96B4C,
	InputRecordingService_get_EndTime_mE19E70EF187D95FDCEDC6CB0009A2582D8646280,
	InputRecordingService_set_EndTime_mABFC85BB48B12E8EE78908356EE44C7CE9FDC163,
	InputRecordingService_get_InputRecordingProfile_mB8BD4F1AF2E262A38DDF585AAACAEA2BEDA5AEDA,
	InputRecordingService_set_InputRecordingProfile_m5EEFCD36612DBA36FA179119103B8A6F73EC1DFA,
	InputRecordingService__ctor_m4470F536140384D4C501AA2BC8A635719057AF46,
	InputRecordingService__ctor_m45CC36E9594217C129440B4D533179DF25957D11,
	InputRecordingService_Enable_m1F10DFBCCA5C5528F07FF7EF2E97CADC48AA9452,
	InputRecordingService_Disable_mFDD2F6FF6228FB1FA586B697B8F6BEBC13E3E1AC,
	InputRecordingService_StartRecording_m93C047EFC0E7BA4F4A53BC5A5290B0C4F281D6CB,
	InputRecordingService_StopRecording_m44C56045591E4DE87869545FA125A128B98E0BD9,
	InputRecordingService_LateUpdate_m12C533C022ED796FFA5CD7FF97222A92EC884E25,
	InputRecordingService_DiscardRecordedInput_mCFB5DD4C68426909DC0F78D56C45D16D44859008,
	InputRecordingService_SaveInputAnimation_m4F22E7353D9AA9CC159412DF85E8922851FDD1CB,
	InputRecordingService_SaveInputAnimation_mD637C6ABA7EEB9759026F3E75BC94F7136F09CF3,
	InputRecordingService_SaveInputAnimationAsync_mEEAD18B15B450158E7111AAEFD412F75605B68CE,
	InputRecordingService_SaveInputAnimationAsync_mCB8DE096220B7DC9662F175A4851207CE970176D,
	InputRecordingService_ResetStartTime_m0397DE83F1512F600949CA4056DFB68FE30FA4FF,
	InputRecordingService_RecordKeyframe_m197F92AB9FCA6907323AC5435B969B2178013B2E,
	InputRecordingService_RecordInputHandData_mF7D4D53FC556DED8A364A3B580D157110EE221CC,
	InputRecordingService_PruneBuffer_mAC5A0226F1BADFD152EAFBE1C14BF4CD380E55C6,
	InputRecordingService_U3CSaveInputAnimationAsyncU3Eb__44_0_m3EB98B2F374BD5A2952D7CB757583817A6F3850A,
	U3CSaveInputAnimationAsyncU3Ed__44_MoveNext_m4DA7E724F562C6D7B2423E448303AFA90E188519,
	U3CSaveInputAnimationAsyncU3Ed__44_SetStateMachine_m490A41A67D294E505D9AE708E3BBE957952714BE,
	MixedRealityInputRecordingProfile_get_FrameRate_m0ACAA23CFE665980AD426584CCEB0F82ECD602D4,
	MixedRealityInputRecordingProfile_get_RecordHandData_m3E8F47E035ADEFA2FA41721F8555B309D98DDF6C,
	MixedRealityInputRecordingProfile_get_JointPositionThreshold_m4B64573233A1D7238FDE9D44D1A41DD0446690F9,
	MixedRealityInputRecordingProfile_get_JointRotationThreshold_m17BAA5D4973466E0F77EAEA912C3BCB1B25A2241,
	MixedRealityInputRecordingProfile_get_RecordCameraPose_m114BC393084F333736F61DC1654F5BC723821D8D,
	MixedRealityInputRecordingProfile_get_CameraPositionThreshold_m675A20054E46C8F32450BA01CF8C48469395CF70,
	MixedRealityInputRecordingProfile_get_CameraRotationThreshold_mEDE1563C6B195F9FB616653AD3B1A69EBC1C8555,
	MixedRealityInputRecordingProfile_get_RecordEyeGaze_m04D78057C7DF6020A8D8B57C1D5D63B08D08876C,
	MixedRealityInputRecordingProfile_get_EyeGazeOriginThreshold_mC80FCEC3A01A52DF1535EF2432C80CB012B23FE6,
	MixedRealityInputRecordingProfile_get_EyeGazeDirectionThreshold_m8D4F9E1B3980ED514AB5C20F7709D26CCAA8CF8F,
	MixedRealityInputRecordingProfile_get_PartitionSize_m4305A449BAF615F6859F64AEFEB219CC8D61858C,
	MixedRealityInputRecordingProfile__ctor_mA316C2E09FA56A17B4BFD21F50ACDA07578F52DB,
};
extern void U3CToStreamAsyncU3Ed__40_MoveNext_mA51D608F4A510232FA0F29CCAD6926F053531C60_AdjustorThunk (void);
extern void U3CToStreamAsyncU3Ed__40_SetStateMachine_m11A93B6443934B38F68D3BEC17DBA7AAE2C44EEA_AdjustorThunk (void);
extern void U3CFromStreamAsyncU3Ed__49_MoveNext_mEC81E08BDAFA0CDAA04B7636C40817427BA48079_AdjustorThunk (void);
extern void U3CFromStreamAsyncU3Ed__49_SetStateMachine_mFF1AAB8E5A75C2A22913D08FB5EAA756BA6CD27C_AdjustorThunk (void);
extern void U3CSaveInputAnimationAsyncU3Ed__44_MoveNext_m4DA7E724F562C6D7B2423E448303AFA90E188519_AdjustorThunk (void);
extern void U3CSaveInputAnimationAsyncU3Ed__44_SetStateMachine_m490A41A67D294E505D9AE708E3BBE957952714BE_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[6] = 
{
	{ 0x06000054, U3CToStreamAsyncU3Ed__40_MoveNext_mA51D608F4A510232FA0F29CCAD6926F053531C60_AdjustorThunk },
	{ 0x06000055, U3CToStreamAsyncU3Ed__40_SetStateMachine_m11A93B6443934B38F68D3BEC17DBA7AAE2C44EEA_AdjustorThunk },
	{ 0x06000058, U3CFromStreamAsyncU3Ed__49_MoveNext_mEC81E08BDAFA0CDAA04B7636C40817427BA48079_AdjustorThunk },
	{ 0x06000059, U3CFromStreamAsyncU3Ed__49_SetStateMachine_mFF1AAB8E5A75C2A22913D08FB5EAA756BA6CD27C_AdjustorThunk },
	{ 0x060000B1, U3CSaveInputAnimationAsyncU3Ed__44_MoveNext_m4DA7E724F562C6D7B2423E448303AFA90E188519_AdjustorThunk },
	{ 0x060000B2, U3CSaveInputAnimationAsyncU3Ed__44_SetStateMachine_m490A41A67D294E505D9AE708E3BBE957952714BE_AdjustorThunk },
};
static const int32_t s_InvokerIndices[190] = 
{
	89,
	89,
	31,
	726,
	337,
	23,
	23,
	23,
	28,
	114,
	28,
	114,
	23,
	726,
	89,
	31,
	89,
	31,
	89,
	31,
	10,
	23,
	2679,
	2680,
	2681,
	26,
	32,
	1023,
	23,
	337,
	939,
	2682,
	2683,
	484,
	2684,
	2685,
	2686,
	34,
	1,
	0,
	1,
	2687,
	2688,
	939,
	2689,
	2690,
	23,
	26,
	2691,
	2692,
	14,
	2693,
	2694,
	2695,
	2696,
	2697,
	2698,
	2115,
	118,
	2115,
	118,
	2699,
	2699,
	2700,
	2701,
	2702,
	2693,
	2703,
	2704,
	2704,
	2704,
	2705,
	2706,
	2684,
	23,
	2707,
	2706,
	2686,
	23,
	41,
	23,
	23,
	23,
	23,
	26,
	23,
	14,
	23,
	26,
	32,
	23,
	89,
	23,
	23,
	14,
	23,
	14,
	14,
	14,
	167,
	168,
	459,
	2115,
	143,
	2115,
	143,
	2115,
	143,
	2115,
	143,
	2115,
	143,
	2115,
	143,
	726,
	23,
	23,
	337,
	2408,
	2432,
	671,
	2708,
	484,
	14,
	14,
	726,
	337,
	89,
	31,
	89,
	31,
	89,
	31,
	89,
	31,
	2407,
	2408,
	2473,
	2432,
	14,
	26,
	14,
	26,
	337,
	26,
	26,
	26,
	26,
	89,
	31,
	89,
	31,
	726,
	337,
	726,
	726,
	337,
	14,
	26,
	1292,
	1269,
	23,
	23,
	23,
	23,
	23,
	23,
	28,
	114,
	28,
	114,
	23,
	23,
	31,
	23,
	14,
	23,
	26,
	726,
	89,
	726,
	726,
	89,
	726,
	726,
	89,
	726,
	726,
	10,
	23,
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputAnimationCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputAnimationCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.InputAnimation.dll",
	190,
	s_methodPointers,
	6,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
