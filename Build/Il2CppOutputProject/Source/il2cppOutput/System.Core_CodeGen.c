﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String SR::GetString(System.String)
extern void SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B (void);
// 0x00000002 System.Void System.Security.Cryptography.AesManaged::.ctor()
extern void AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64 (void);
// 0x00000003 System.Int32 System.Security.Cryptography.AesManaged::get_FeedbackSize()
extern void AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712 (void);
// 0x00000004 System.Byte[] System.Security.Cryptography.AesManaged::get_IV()
extern void AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E (void);
// 0x00000005 System.Void System.Security.Cryptography.AesManaged::set_IV(System.Byte[])
extern void AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722 (void);
// 0x00000006 System.Byte[] System.Security.Cryptography.AesManaged::get_Key()
extern void AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088 (void);
// 0x00000007 System.Void System.Security.Cryptography.AesManaged::set_Key(System.Byte[])
extern void AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC (void);
// 0x00000008 System.Int32 System.Security.Cryptography.AesManaged::get_KeySize()
extern void AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6 (void);
// 0x00000009 System.Void System.Security.Cryptography.AesManaged::set_KeySize(System.Int32)
extern void AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349 (void);
// 0x0000000A System.Security.Cryptography.CipherMode System.Security.Cryptography.AesManaged::get_Mode()
extern void AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA (void);
// 0x0000000B System.Void System.Security.Cryptography.AesManaged::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8 (void);
// 0x0000000C System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesManaged::get_Padding()
extern void AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0 (void);
// 0x0000000D System.Void System.Security.Cryptography.AesManaged::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23 (void);
// 0x0000000E System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor()
extern void AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD (void);
// 0x0000000F System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1 (void);
// 0x00000010 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor()
extern void AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4 (void);
// 0x00000011 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesManaged::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91 (void);
// 0x00000012 System.Void System.Security.Cryptography.AesManaged::Dispose(System.Boolean)
extern void AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED (void);
// 0x00000013 System.Void System.Security.Cryptography.AesManaged::GenerateIV()
extern void AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC (void);
// 0x00000014 System.Void System.Security.Cryptography.AesManaged::GenerateKey()
extern void AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146 (void);
// 0x00000015 System.Void System.Security.Cryptography.AesCryptoServiceProvider::.ctor()
extern void AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E (void);
// 0x00000016 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateIV()
extern void AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC (void);
// 0x00000017 System.Void System.Security.Cryptography.AesCryptoServiceProvider::GenerateKey()
extern void AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586 (void);
// 0x00000018 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139 (void);
// 0x00000019 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor(System.Byte[],System.Byte[])
extern void AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672 (void);
// 0x0000001A System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_IV()
extern void AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F (void);
// 0x0000001B System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_IV(System.Byte[])
extern void AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C (void);
// 0x0000001C System.Byte[] System.Security.Cryptography.AesCryptoServiceProvider::get_Key()
extern void AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A (void);
// 0x0000001D System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Key(System.Byte[])
extern void AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700 (void);
// 0x0000001E System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_KeySize()
extern void AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA (void);
// 0x0000001F System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_KeySize(System.Int32)
extern void AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B (void);
// 0x00000020 System.Int32 System.Security.Cryptography.AesCryptoServiceProvider::get_FeedbackSize()
extern void AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F (void);
// 0x00000021 System.Security.Cryptography.CipherMode System.Security.Cryptography.AesCryptoServiceProvider::get_Mode()
extern void AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F (void);
// 0x00000022 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Mode(System.Security.Cryptography.CipherMode)
extern void AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA (void);
// 0x00000023 System.Security.Cryptography.PaddingMode System.Security.Cryptography.AesCryptoServiceProvider::get_Padding()
extern void AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438 (void);
// 0x00000024 System.Void System.Security.Cryptography.AesCryptoServiceProvider::set_Padding(System.Security.Cryptography.PaddingMode)
extern void AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22 (void);
// 0x00000025 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateDecryptor()
extern void AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44 (void);
// 0x00000026 System.Security.Cryptography.ICryptoTransform System.Security.Cryptography.AesCryptoServiceProvider::CreateEncryptor()
extern void AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA (void);
// 0x00000027 System.Void System.Security.Cryptography.AesCryptoServiceProvider::Dispose(System.Boolean)
extern void AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F (void);
// 0x00000028 System.Void System.Security.Cryptography.AesTransform::.ctor(System.Security.Cryptography.Aes,System.Boolean,System.Byte[],System.Byte[])
extern void AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD (void);
// 0x00000029 System.Void System.Security.Cryptography.AesTransform::ECB(System.Byte[],System.Byte[])
extern void AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA (void);
// 0x0000002A System.UInt32 System.Security.Cryptography.AesTransform::SubByte(System.UInt32)
extern void AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24 (void);
// 0x0000002B System.Void System.Security.Cryptography.AesTransform::Encrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA (void);
// 0x0000002C System.Void System.Security.Cryptography.AesTransform::Decrypt128(System.Byte[],System.Byte[],System.UInt32[])
extern void AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463 (void);
// 0x0000002D System.Void System.Security.Cryptography.AesTransform::.cctor()
extern void AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325 (void);
// 0x0000002E System.Exception System.Linq.Error::ArgumentNull(System.String)
extern void Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7 (void);
// 0x0000002F System.Exception System.Linq.Error::ArgumentOutOfRange(System.String)
extern void Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9 (void);
// 0x00000030 System.Exception System.Linq.Error::MoreThanOneMatch()
extern void Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623 (void);
// 0x00000031 System.Exception System.Linq.Error::NoElements()
extern void Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136 (void);
// 0x00000032 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Where(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000033 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Select(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TResult>)
// 0x00000034 System.Func`2<TSource,System.Boolean> System.Linq.Enumerable::CombinePredicates(System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,System.Boolean>)
// 0x00000035 System.Func`2<TSource,TResult> System.Linq.Enumerable::CombineSelectors(System.Func`2<TSource,TMiddle>,System.Func`2<TMiddle,TResult>)
// 0x00000036 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectMany(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000037 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::SelectManyIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Collections.Generic.IEnumerable`1<TResult>>)
// 0x00000038 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Take(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x00000039 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::TakeIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000003A System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderBy(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003B System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::OrderByDescending(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003C System.Linq.IOrderedEnumerable`1<TSource> System.Linq.Enumerable::ThenBy(System.Linq.IOrderedEnumerable`1<TSource>,System.Func`2<TSource,TKey>)
// 0x0000003D System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Distinct(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000003E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::DistinctIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x0000003F System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Union(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000040 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::UnionIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000041 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::Intersect(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000042 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable::IntersectIterator(System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEnumerable`1<TSource>,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000043 TSource[] System.Linq.Enumerable::ToArray(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000044 System.Collections.Generic.List`1<TSource> System.Linq.Enumerable::ToList(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000045 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>)
// 0x00000046 System.Collections.Generic.Dictionary`2<TKey,TElement> System.Linq.Enumerable::ToDictionary(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,TKey>,System.Func`2<TSource,TElement>,System.Collections.Generic.IEqualityComparer`1<TKey>)
// 0x00000047 TSource System.Linq.Enumerable::First(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000048 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000049 TSource System.Linq.Enumerable::FirstOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004A TSource System.Linq.Enumerable::Last(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x0000004B TSource System.Linq.Enumerable::SingleOrDefault(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x0000004C TSource System.Linq.Enumerable::ElementAt(System.Collections.Generic.IEnumerable`1<TSource>,System.Int32)
// 0x0000004D System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::Range(System.Int32,System.Int32)
extern void Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C (void);
// 0x0000004E System.Collections.Generic.IEnumerable`1<System.Int32> System.Linq.Enumerable::RangeIterator(System.Int32,System.Int32)
extern void Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75 (void);
// 0x0000004F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::Repeat(TResult,System.Int32)
// 0x00000050 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable::RepeatIterator(TResult,System.Int32)
// 0x00000051 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000052 System.Boolean System.Linq.Enumerable::Any(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000053 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>)
// 0x00000054 System.Int32 System.Linq.Enumerable::Count(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000055 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource)
// 0x00000056 System.Boolean System.Linq.Enumerable::Contains(System.Collections.Generic.IEnumerable`1<TSource>,TSource,System.Collections.Generic.IEqualityComparer`1<TSource>)
// 0x00000057 TAccumulate System.Linq.Enumerable::Aggregate(System.Collections.Generic.IEnumerable`1<TSource>,TAccumulate,System.Func`3<TAccumulate,TSource,TAccumulate>)
// 0x00000058 System.Int32 System.Linq.Enumerable::Sum(System.Collections.Generic.IEnumerable`1<System.Int32>)
extern void Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583 (void);
// 0x00000059 System.Void System.Linq.Enumerable/Iterator`1::.ctor()
// 0x0000005A TSource System.Linq.Enumerable/Iterator`1::get_Current()
// 0x0000005B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/Iterator`1::Clone()
// 0x0000005C System.Void System.Linq.Enumerable/Iterator`1::Dispose()
// 0x0000005D System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/Iterator`1::GetEnumerator()
// 0x0000005E System.Boolean System.Linq.Enumerable/Iterator`1::MoveNext()
// 0x0000005F System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/Iterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000060 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/Iterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000061 System.Object System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.get_Current()
// 0x00000062 System.Collections.IEnumerator System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000063 System.Void System.Linq.Enumerable/Iterator`1::System.Collections.IEnumerator.Reset()
// 0x00000064 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000065 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Clone()
// 0x00000066 System.Void System.Linq.Enumerable/WhereEnumerableIterator`1::Dispose()
// 0x00000067 System.Boolean System.Linq.Enumerable/WhereEnumerableIterator`1::MoveNext()
// 0x00000068 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereEnumerableIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000069 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereEnumerableIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006A System.Void System.Linq.Enumerable/WhereArrayIterator`1::.ctor(TSource[],System.Func`2<TSource,System.Boolean>)
// 0x0000006B System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Clone()
// 0x0000006C System.Boolean System.Linq.Enumerable/WhereArrayIterator`1::MoveNext()
// 0x0000006D System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereArrayIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x0000006E System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereArrayIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x0000006F System.Void System.Linq.Enumerable/WhereListIterator`1::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>)
// 0x00000070 System.Linq.Enumerable/Iterator`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Clone()
// 0x00000071 System.Boolean System.Linq.Enumerable/WhereListIterator`1::MoveNext()
// 0x00000072 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereListIterator`1::Select(System.Func`2<TSource,TResult>)
// 0x00000073 System.Collections.Generic.IEnumerable`1<TSource> System.Linq.Enumerable/WhereListIterator`1::Where(System.Func`2<TSource,System.Boolean>)
// 0x00000074 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::.ctor(System.Collections.Generic.IEnumerable`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000075 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Clone()
// 0x00000076 System.Void System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Dispose()
// 0x00000077 System.Boolean System.Linq.Enumerable/WhereSelectEnumerableIterator`2::MoveNext()
// 0x00000078 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000079 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectEnumerableIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007A System.Void System.Linq.Enumerable/WhereSelectArrayIterator`2::.ctor(TSource[],System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x0000007B System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Clone()
// 0x0000007C System.Boolean System.Linq.Enumerable/WhereSelectArrayIterator`2::MoveNext()
// 0x0000007D System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectArrayIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x0000007E System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectArrayIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x0000007F System.Void System.Linq.Enumerable/WhereSelectListIterator`2::.ctor(System.Collections.Generic.List`1<TSource>,System.Func`2<TSource,System.Boolean>,System.Func`2<TSource,TResult>)
// 0x00000080 System.Linq.Enumerable/Iterator`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Clone()
// 0x00000081 System.Boolean System.Linq.Enumerable/WhereSelectListIterator`2::MoveNext()
// 0x00000082 System.Collections.Generic.IEnumerable`1<TResult2> System.Linq.Enumerable/WhereSelectListIterator`2::Select(System.Func`2<TResult,TResult2>)
// 0x00000083 System.Collections.Generic.IEnumerable`1<TResult> System.Linq.Enumerable/WhereSelectListIterator`2::Where(System.Func`2<TResult,System.Boolean>)
// 0x00000084 System.Void System.Linq.Enumerable/<>c__DisplayClass6_0`1::.ctor()
// 0x00000085 System.Boolean System.Linq.Enumerable/<>c__DisplayClass6_0`1::<CombinePredicates>b__0(TSource)
// 0x00000086 System.Void System.Linq.Enumerable/<>c__DisplayClass7_0`3::.ctor()
// 0x00000087 TResult System.Linq.Enumerable/<>c__DisplayClass7_0`3::<CombineSelectors>b__0(TSource)
// 0x00000088 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::.ctor(System.Int32)
// 0x00000089 System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.IDisposable.Dispose()
// 0x0000008A System.Boolean System.Linq.Enumerable/<SelectManyIterator>d__17`2::MoveNext()
// 0x0000008B System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally1()
// 0x0000008C System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::<>m__Finally2()
// 0x0000008D TResult System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x0000008E System.Void System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.Reset()
// 0x0000008F System.Object System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerator.get_Current()
// 0x00000090 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x00000091 System.Collections.IEnumerator System.Linq.Enumerable/<SelectManyIterator>d__17`2::System.Collections.IEnumerable.GetEnumerator()
// 0x00000092 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::.ctor(System.Int32)
// 0x00000093 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.IDisposable.Dispose()
// 0x00000094 System.Boolean System.Linq.Enumerable/<TakeIterator>d__25`1::MoveNext()
// 0x00000095 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::<>m__Finally1()
// 0x00000096 TSource System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x00000097 System.Void System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.Reset()
// 0x00000098 System.Object System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerator.get_Current()
// 0x00000099 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x0000009A System.Collections.IEnumerator System.Linq.Enumerable/<TakeIterator>d__25`1::System.Collections.IEnumerable.GetEnumerator()
// 0x0000009B System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::.ctor(System.Int32)
// 0x0000009C System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.IDisposable.Dispose()
// 0x0000009D System.Boolean System.Linq.Enumerable/<DistinctIterator>d__68`1::MoveNext()
// 0x0000009E System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::<>m__Finally1()
// 0x0000009F TSource System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000A0 System.Void System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.Reset()
// 0x000000A1 System.Object System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerator.get_Current()
// 0x000000A2 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000A3 System.Collections.IEnumerator System.Linq.Enumerable/<DistinctIterator>d__68`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000A4 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::.ctor(System.Int32)
// 0x000000A5 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.IDisposable.Dispose()
// 0x000000A6 System.Boolean System.Linq.Enumerable/<UnionIterator>d__71`1::MoveNext()
// 0x000000A7 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally1()
// 0x000000A8 System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::<>m__Finally2()
// 0x000000A9 TSource System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000AA System.Void System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.Reset()
// 0x000000AB System.Object System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerator.get_Current()
// 0x000000AC System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000AD System.Collections.IEnumerator System.Linq.Enumerable/<UnionIterator>d__71`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000AE System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::.ctor(System.Int32)
// 0x000000AF System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.IDisposable.Dispose()
// 0x000000B0 System.Boolean System.Linq.Enumerable/<IntersectIterator>d__74`1::MoveNext()
// 0x000000B1 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::<>m__Finally1()
// 0x000000B2 TSource System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerator<TSource>.get_Current()
// 0x000000B3 System.Void System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.Reset()
// 0x000000B4 System.Object System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerator.get_Current()
// 0x000000B5 System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
// 0x000000B6 System.Collections.IEnumerator System.Linq.Enumerable/<IntersectIterator>d__74`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000B7 System.Void System.Linq.Enumerable/<RangeIterator>d__115::.ctor(System.Int32)
extern void U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228 (void);
// 0x000000B8 System.Void System.Linq.Enumerable/<RangeIterator>d__115::System.IDisposable.Dispose()
extern void U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032 (void);
// 0x000000B9 System.Boolean System.Linq.Enumerable/<RangeIterator>d__115::MoveNext()
extern void U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10 (void);
// 0x000000BA System.Int32 System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.Generic.IEnumerator<System.Int32>.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D (void);
// 0x000000BB System.Void System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerator.Reset()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2 (void);
// 0x000000BC System.Object System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerator.get_Current()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB (void);
// 0x000000BD System.Collections.Generic.IEnumerator`1<System.Int32> System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.Generic.IEnumerable<System.Int32>.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4 (void);
// 0x000000BE System.Collections.IEnumerator System.Linq.Enumerable/<RangeIterator>d__115::System.Collections.IEnumerable.GetEnumerator()
extern void U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880 (void);
// 0x000000BF System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::.ctor(System.Int32)
// 0x000000C0 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.IDisposable.Dispose()
// 0x000000C1 System.Boolean System.Linq.Enumerable/<RepeatIterator>d__117`1::MoveNext()
// 0x000000C2 TResult System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerator<TResult>.get_Current()
// 0x000000C3 System.Void System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.Reset()
// 0x000000C4 System.Object System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerator.get_Current()
// 0x000000C5 System.Collections.Generic.IEnumerator`1<TResult> System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.Generic.IEnumerable<TResult>.GetEnumerator()
// 0x000000C6 System.Collections.IEnumerator System.Linq.Enumerable/<RepeatIterator>d__117`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000C7 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.IOrderedEnumerable`1::CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000C8 System.Void System.Linq.Set`1::.ctor(System.Collections.Generic.IEqualityComparer`1<TElement>)
// 0x000000C9 System.Boolean System.Linq.Set`1::Add(TElement)
// 0x000000CA System.Boolean System.Linq.Set`1::Remove(TElement)
// 0x000000CB System.Boolean System.Linq.Set`1::Find(TElement,System.Boolean)
// 0x000000CC System.Void System.Linq.Set`1::Resize()
// 0x000000CD System.Int32 System.Linq.Set`1::InternalGetHashCode(TElement)
// 0x000000CE System.Collections.Generic.IEnumerator`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerator()
// 0x000000CF System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`1::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000D0 System.Collections.IEnumerator System.Linq.OrderedEnumerable`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000D1 System.Linq.IOrderedEnumerable`1<TElement> System.Linq.OrderedEnumerable`1::System.Linq.IOrderedEnumerable<TElement>.CreateOrderedEnumerable(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000D2 System.Void System.Linq.OrderedEnumerable`1::.ctor()
// 0x000000D3 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::.ctor(System.Int32)
// 0x000000D4 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.IDisposable.Dispose()
// 0x000000D5 System.Boolean System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::MoveNext()
// 0x000000D6 TElement System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.Generic.IEnumerator<TElement>.get_Current()
// 0x000000D7 System.Void System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.Reset()
// 0x000000D8 System.Object System.Linq.OrderedEnumerable`1/<GetEnumerator>d__1::System.Collections.IEnumerator.get_Current()
// 0x000000D9 System.Void System.Linq.OrderedEnumerable`2::.ctor(System.Collections.Generic.IEnumerable`1<TElement>,System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean)
// 0x000000DA System.Linq.EnumerableSorter`1<TElement> System.Linq.OrderedEnumerable`2::GetEnumerableSorter(System.Linq.EnumerableSorter`1<TElement>)
// 0x000000DB System.Void System.Linq.EnumerableSorter`1::ComputeKeys(TElement[],System.Int32)
// 0x000000DC System.Int32 System.Linq.EnumerableSorter`1::CompareKeys(System.Int32,System.Int32)
// 0x000000DD System.Int32[] System.Linq.EnumerableSorter`1::Sort(TElement[],System.Int32)
// 0x000000DE System.Void System.Linq.EnumerableSorter`1::QuickSort(System.Int32[],System.Int32,System.Int32)
// 0x000000DF System.Void System.Linq.EnumerableSorter`1::.ctor()
// 0x000000E0 System.Void System.Linq.EnumerableSorter`2::.ctor(System.Func`2<TElement,TKey>,System.Collections.Generic.IComparer`1<TKey>,System.Boolean,System.Linq.EnumerableSorter`1<TElement>)
// 0x000000E1 System.Void System.Linq.EnumerableSorter`2::ComputeKeys(TElement[],System.Int32)
// 0x000000E2 System.Int32 System.Linq.EnumerableSorter`2::CompareKeys(System.Int32,System.Int32)
// 0x000000E3 System.Void System.Linq.Buffer`1::.ctor(System.Collections.Generic.IEnumerable`1<TElement>)
// 0x000000E4 TElement[] System.Linq.Buffer`1::ToArray()
// 0x000000E5 System.Void System.Collections.Generic.HashSet`1::.ctor()
// 0x000000E6 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Collections.Generic.IEqualityComparer`1<T>)
// 0x000000E7 System.Void System.Collections.Generic.HashSet`1::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000E8 System.Void System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.Add(T)
// 0x000000E9 System.Void System.Collections.Generic.HashSet`1::Clear()
// 0x000000EA System.Boolean System.Collections.Generic.HashSet`1::Contains(T)
// 0x000000EB System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32)
// 0x000000EC System.Boolean System.Collections.Generic.HashSet`1::Remove(T)
// 0x000000ED System.Int32 System.Collections.Generic.HashSet`1::get_Count()
// 0x000000EE System.Boolean System.Collections.Generic.HashSet`1::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
// 0x000000EF System.Collections.Generic.HashSet`1/Enumerator<T> System.Collections.Generic.HashSet`1::GetEnumerator()
// 0x000000F0 System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.HashSet`1::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
// 0x000000F1 System.Collections.IEnumerator System.Collections.Generic.HashSet`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000000F2 System.Void System.Collections.Generic.HashSet`1::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
// 0x000000F3 System.Void System.Collections.Generic.HashSet`1::OnDeserialization(System.Object)
// 0x000000F4 System.Boolean System.Collections.Generic.HashSet`1::Add(T)
// 0x000000F5 System.Void System.Collections.Generic.HashSet`1::UnionWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000F6 System.Void System.Collections.Generic.HashSet`1::ExceptWith(System.Collections.Generic.IEnumerable`1<T>)
// 0x000000F7 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[])
// 0x000000F8 System.Void System.Collections.Generic.HashSet`1::CopyTo(T[],System.Int32,System.Int32)
// 0x000000F9 System.Void System.Collections.Generic.HashSet`1::Initialize(System.Int32)
// 0x000000FA System.Void System.Collections.Generic.HashSet`1::IncreaseCapacity()
// 0x000000FB System.Void System.Collections.Generic.HashSet`1::SetCapacity(System.Int32)
// 0x000000FC System.Boolean System.Collections.Generic.HashSet`1::AddIfNotPresent(T)
// 0x000000FD System.Int32 System.Collections.Generic.HashSet`1::InternalGetHashCode(T)
// 0x000000FE System.Void System.Collections.Generic.HashSet`1/Enumerator::.ctor(System.Collections.Generic.HashSet`1<T>)
// 0x000000FF System.Void System.Collections.Generic.HashSet`1/Enumerator::Dispose()
// 0x00000100 System.Boolean System.Collections.Generic.HashSet`1/Enumerator::MoveNext()
// 0x00000101 T System.Collections.Generic.HashSet`1/Enumerator::get_Current()
// 0x00000102 System.Object System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.get_Current()
// 0x00000103 System.Void System.Collections.Generic.HashSet`1/Enumerator::System.Collections.IEnumerator.Reset()
static Il2CppMethodPointer s_methodPointers[259] = 
{
	SR_GetString_m0D34A4798D653D11FFC8F27A24C741A83A3DA90B,
	AesManaged__ctor_mB2BB25E2F795428300A966DF7C4706BDDB65FB64,
	AesManaged_get_FeedbackSize_mA079406B80A8CDFB6811251C8BCE9EFE3C83A712,
	AesManaged_get_IV_mAAC08AB6D76CE29D3AEFCEF7B46F17B788B00B6E,
	AesManaged_set_IV_m6AF8905A7F0DBD20D7E059360423DB57C7DFA722,
	AesManaged_get_Key_mC3790099349E411DFBC3EB6916E31CCC1F2AC088,
	AesManaged_set_Key_m654922A858A73BC91747B52F5D8B194B1EA88ADC,
	AesManaged_get_KeySize_m5218EB6C55678DC91BDE12E4F0697B719A2C7DD6,
	AesManaged_set_KeySize_m0AF9E2BB96295D70FBADB46F8E32FB54A695C349,
	AesManaged_get_Mode_m85C722AAA2A9CF3BC012EC908CF5B3B57BAF4BDA,
	AesManaged_set_Mode_mE06717F04195261B88A558FBD08AEB847D9320D8,
	AesManaged_get_Padding_mBD0B0AA07CF0FBFDFC14458D14F058DE6DA656F0,
	AesManaged_set_Padding_m1BAC3EECEF3E2F49E4641E29169F149EDA8C5B23,
	AesManaged_CreateDecryptor_m9E9E7861138397C7A6AAF8C43C81BD4CFCB8E0BD,
	AesManaged_CreateDecryptor_mEBE041A905F0848F846901916BA23485F85C65F1,
	AesManaged_CreateEncryptor_m82CC97D7C3C330EB8F5F61B3192D65859CAA94F4,
	AesManaged_CreateEncryptor_mA914CA875EF777EDB202343570182CC0D9D89A91,
	AesManaged_Dispose_m57258CB76A9CCEF03FF4D4C5DE02E9A31056F8ED,
	AesManaged_GenerateIV_m92735378E3FB47DE1D0241A923CB4E426C702ABC,
	AesManaged_GenerateKey_m5C790BC376A3FAFF13617855FF6BFA8A57925146,
	AesCryptoServiceProvider__ctor_m8AA4C1503DBE1849070CFE727ED227BE5043373E,
	AesCryptoServiceProvider_GenerateIV_mAE25C1774AEB75702E4737808E56FD2EC8BF54CC,
	AesCryptoServiceProvider_GenerateKey_mC65CD8C14E8FD07E9469E74C641A746E52977586,
	AesCryptoServiceProvider_CreateDecryptor_m3842B2AC283063BE4D9902818C8F68CFB4100139,
	AesCryptoServiceProvider_CreateEncryptor_mACCCC00AED5CBBF5E9437BCA907DD67C6D123672,
	AesCryptoServiceProvider_get_IV_m30FBD13B702C384941FB85AD975BB3C0668F426F,
	AesCryptoServiceProvider_set_IV_m195F582AD29E4B449AFC54036AAECE0E05385C9C,
	AesCryptoServiceProvider_get_Key_m9ABC98DF0CDE8952B677538C387C66A88196786A,
	AesCryptoServiceProvider_set_Key_m4B9CE2F92E3B1BC209BFAECEACB7A976BBCDC700,
	AesCryptoServiceProvider_get_KeySize_m10BDECEC12722803F3DE5F15CD76C5BDF588D1FA,
	AesCryptoServiceProvider_set_KeySize_mA26268F7CEDA7D0A2447FC2022327E0C49C89B9B,
	AesCryptoServiceProvider_get_FeedbackSize_mB93FFC9FCB2C09EABFB13913E245A2D75491659F,
	AesCryptoServiceProvider_get_Mode_m5C09588E49787D597CF8C0CD0C74DB63BE0ACE5F,
	AesCryptoServiceProvider_set_Mode_mC7EE07E709C918D0745E5A207A66D89F08EA57EA,
	AesCryptoServiceProvider_get_Padding_mA56E045AE5CCF569C4A21C949DD4A4332E63F438,
	AesCryptoServiceProvider_set_Padding_m94A4D3BE55325036611C5015E02CB622CFCDAF22,
	AesCryptoServiceProvider_CreateDecryptor_mD858924207EA664C6E32D42408FB5C8040DD4D44,
	AesCryptoServiceProvider_CreateEncryptor_m964DD0E94A26806AB34A7A79D4E4D1539425A2EA,
	AesCryptoServiceProvider_Dispose_mCFA420F8643911F86A112F50905FCB34C4A3045F,
	AesTransform__ctor_m1BC6B0F208747D4E35A58075D74DEBD5F72DB7DD,
	AesTransform_ECB_mAFE52E4D1958026C3343F85CC950A8E24FDFBBDA,
	AesTransform_SubByte_mEDB43A2A4E83017475094E5616E7DBC56F945A24,
	AesTransform_Encrypt128_m09C945A0345FD32E8DB3F4AF4B4E184CADD754DA,
	AesTransform_Decrypt128_m1AE10B230A47A294B5B10EFD9C8243B02DBEA463,
	AesTransform__cctor_mDEA197C50BA055FF76B7ECFEB5C1FD7900CE4325,
	Error_ArgumentNull_mCA126ED8F4F3B343A70E201C44B3A509690F1EA7,
	Error_ArgumentOutOfRange_mACFCB068F4E0C4EEF9E6EDDD59E798901C32C6C9,
	Error_MoreThanOneMatch_m85C3617F782E9F2333FC1FDF42821BE069F24623,
	Error_NoElements_m17188AC2CF25EB359A4E1DDE9518A98598791136,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Range_m7D095DA6BE22CF27F2556EF0DD41FB62B454BA7C,
	Enumerable_RangeIterator_m4E1661306100838EF4523D61D1F58E1F4E935A75,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Enumerable_Sum_mA81913DBCF3086B4716F692F9DB797D7DD6B7583,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	U3CRangeIteratorU3Ed__115__ctor_m5D6B6F8CA96D435FC1C8B6E92B93CAFB45F69228,
	U3CRangeIteratorU3Ed__115_System_IDisposable_Dispose_m2CAD68A0BE64B4A83FD83AC61594B867245C3032,
	U3CRangeIteratorU3Ed__115_MoveNext_mD7DDF94B1115FD5F5CF1447CF3184E26070A0A10,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumeratorU3CSystem_Int32U3E_get_Current_m8C79E2C2C740D58C773C1E760851E13294D8BE0D,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_Reset_m040EAB9597A54F82BE40807057EC9ECE18F9D9D2,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerator_get_Current_mD80D27D2D8E8A607D471DC168F366334313DA9EB,
	U3CRangeIteratorU3Ed__115_System_Collections_Generic_IEnumerableU3CSystem_Int32U3E_GetEnumerator_m959E99ACAAE05CD3EDD16779A9A802EB52CD6FB4,
	U3CRangeIteratorU3Ed__115_System_Collections_IEnumerable_GetEnumerator_mD02DC82E366D6EFEF966D2C6F98186A08ED0E880,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
};
static const int32_t s_InvokerIndices[259] = 
{
	0,
	23,
	10,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	32,
	10,
	32,
	14,
	114,
	14,
	114,
	31,
	23,
	23,
	23,
	23,
	23,
	114,
	114,
	14,
	26,
	14,
	26,
	10,
	32,
	10,
	10,
	32,
	10,
	32,
	14,
	14,
	31,
	921,
	27,
	37,
	212,
	212,
	3,
	0,
	0,
	4,
	4,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	301,
	301,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	94,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	23,
	89,
	10,
	23,
	14,
	14,
	14,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
};
static const Il2CppTokenRangePair s_rgctxIndices[67] = 
{
	{ 0x02000008, { 106, 4 } },
	{ 0x02000009, { 110, 9 } },
	{ 0x0200000A, { 121, 7 } },
	{ 0x0200000B, { 130, 10 } },
	{ 0x0200000C, { 142, 11 } },
	{ 0x0200000D, { 156, 9 } },
	{ 0x0200000E, { 168, 12 } },
	{ 0x0200000F, { 183, 1 } },
	{ 0x02000010, { 184, 2 } },
	{ 0x02000011, { 186, 12 } },
	{ 0x02000012, { 198, 8 } },
	{ 0x02000013, { 206, 11 } },
	{ 0x02000014, { 217, 12 } },
	{ 0x02000015, { 229, 12 } },
	{ 0x02000017, { 241, 4 } },
	{ 0x02000019, { 245, 8 } },
	{ 0x0200001B, { 253, 3 } },
	{ 0x0200001C, { 258, 5 } },
	{ 0x0200001D, { 263, 7 } },
	{ 0x0200001E, { 270, 3 } },
	{ 0x0200001F, { 273, 7 } },
	{ 0x02000020, { 280, 4 } },
	{ 0x02000021, { 284, 25 } },
	{ 0x02000023, { 309, 2 } },
	{ 0x06000032, { 0, 10 } },
	{ 0x06000033, { 10, 10 } },
	{ 0x06000034, { 20, 5 } },
	{ 0x06000035, { 25, 5 } },
	{ 0x06000036, { 30, 1 } },
	{ 0x06000037, { 31, 2 } },
	{ 0x06000038, { 33, 1 } },
	{ 0x06000039, { 34, 2 } },
	{ 0x0600003A, { 36, 2 } },
	{ 0x0600003B, { 38, 2 } },
	{ 0x0600003C, { 40, 1 } },
	{ 0x0600003D, { 41, 1 } },
	{ 0x0600003E, { 42, 2 } },
	{ 0x0600003F, { 44, 1 } },
	{ 0x06000040, { 45, 2 } },
	{ 0x06000041, { 47, 1 } },
	{ 0x06000042, { 48, 2 } },
	{ 0x06000043, { 50, 3 } },
	{ 0x06000044, { 53, 2 } },
	{ 0x06000045, { 55, 1 } },
	{ 0x06000046, { 56, 7 } },
	{ 0x06000047, { 63, 4 } },
	{ 0x06000048, { 67, 4 } },
	{ 0x06000049, { 71, 3 } },
	{ 0x0600004A, { 74, 4 } },
	{ 0x0600004B, { 78, 3 } },
	{ 0x0600004C, { 81, 3 } },
	{ 0x0600004F, { 84, 1 } },
	{ 0x06000050, { 85, 2 } },
	{ 0x06000051, { 87, 1 } },
	{ 0x06000052, { 88, 3 } },
	{ 0x06000053, { 91, 2 } },
	{ 0x06000054, { 93, 3 } },
	{ 0x06000055, { 96, 2 } },
	{ 0x06000056, { 98, 5 } },
	{ 0x06000057, { 103, 3 } },
	{ 0x06000068, { 119, 2 } },
	{ 0x0600006D, { 128, 2 } },
	{ 0x06000072, { 140, 2 } },
	{ 0x06000078, { 153, 3 } },
	{ 0x0600007D, { 165, 3 } },
	{ 0x06000082, { 180, 3 } },
	{ 0x060000D1, { 256, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[311] = 
{
	{ (Il2CppRGCTXDataType)2, 63512 },
	{ (Il2CppRGCTXDataType)3, 62094 },
	{ (Il2CppRGCTXDataType)2, 63513 },
	{ (Il2CppRGCTXDataType)2, 63514 },
	{ (Il2CppRGCTXDataType)3, 62095 },
	{ (Il2CppRGCTXDataType)2, 63515 },
	{ (Il2CppRGCTXDataType)2, 63516 },
	{ (Il2CppRGCTXDataType)3, 62096 },
	{ (Il2CppRGCTXDataType)2, 63517 },
	{ (Il2CppRGCTXDataType)3, 62097 },
	{ (Il2CppRGCTXDataType)2, 63518 },
	{ (Il2CppRGCTXDataType)3, 62098 },
	{ (Il2CppRGCTXDataType)2, 63519 },
	{ (Il2CppRGCTXDataType)2, 63520 },
	{ (Il2CppRGCTXDataType)3, 62099 },
	{ (Il2CppRGCTXDataType)2, 63521 },
	{ (Il2CppRGCTXDataType)2, 63522 },
	{ (Il2CppRGCTXDataType)3, 62100 },
	{ (Il2CppRGCTXDataType)2, 63523 },
	{ (Il2CppRGCTXDataType)3, 62101 },
	{ (Il2CppRGCTXDataType)2, 63524 },
	{ (Il2CppRGCTXDataType)3, 62102 },
	{ (Il2CppRGCTXDataType)3, 62103 },
	{ (Il2CppRGCTXDataType)2, 49539 },
	{ (Il2CppRGCTXDataType)3, 62104 },
	{ (Il2CppRGCTXDataType)2, 63525 },
	{ (Il2CppRGCTXDataType)3, 62105 },
	{ (Il2CppRGCTXDataType)3, 62106 },
	{ (Il2CppRGCTXDataType)2, 49546 },
	{ (Il2CppRGCTXDataType)3, 62107 },
	{ (Il2CppRGCTXDataType)3, 62108 },
	{ (Il2CppRGCTXDataType)2, 63526 },
	{ (Il2CppRGCTXDataType)3, 62109 },
	{ (Il2CppRGCTXDataType)3, 62110 },
	{ (Il2CppRGCTXDataType)2, 63527 },
	{ (Il2CppRGCTXDataType)3, 62111 },
	{ (Il2CppRGCTXDataType)2, 63528 },
	{ (Il2CppRGCTXDataType)3, 62112 },
	{ (Il2CppRGCTXDataType)2, 63529 },
	{ (Il2CppRGCTXDataType)3, 62113 },
	{ (Il2CppRGCTXDataType)3, 62114 },
	{ (Il2CppRGCTXDataType)3, 62115 },
	{ (Il2CppRGCTXDataType)2, 63530 },
	{ (Il2CppRGCTXDataType)3, 62116 },
	{ (Il2CppRGCTXDataType)3, 62117 },
	{ (Il2CppRGCTXDataType)2, 63531 },
	{ (Il2CppRGCTXDataType)3, 62118 },
	{ (Il2CppRGCTXDataType)3, 62119 },
	{ (Il2CppRGCTXDataType)2, 63532 },
	{ (Il2CppRGCTXDataType)3, 62120 },
	{ (Il2CppRGCTXDataType)2, 63533 },
	{ (Il2CppRGCTXDataType)3, 62121 },
	{ (Il2CppRGCTXDataType)3, 62122 },
	{ (Il2CppRGCTXDataType)2, 49595 },
	{ (Il2CppRGCTXDataType)3, 62123 },
	{ (Il2CppRGCTXDataType)3, 62124 },
	{ (Il2CppRGCTXDataType)2, 49610 },
	{ (Il2CppRGCTXDataType)3, 62125 },
	{ (Il2CppRGCTXDataType)2, 49603 },
	{ (Il2CppRGCTXDataType)2, 63534 },
	{ (Il2CppRGCTXDataType)3, 62126 },
	{ (Il2CppRGCTXDataType)3, 62127 },
	{ (Il2CppRGCTXDataType)3, 62128 },
	{ (Il2CppRGCTXDataType)2, 63535 },
	{ (Il2CppRGCTXDataType)2, 63536 },
	{ (Il2CppRGCTXDataType)2, 49611 },
	{ (Il2CppRGCTXDataType)2, 63537 },
	{ (Il2CppRGCTXDataType)2, 63538 },
	{ (Il2CppRGCTXDataType)2, 63539 },
	{ (Il2CppRGCTXDataType)2, 49613 },
	{ (Il2CppRGCTXDataType)2, 63540 },
	{ (Il2CppRGCTXDataType)2, 49615 },
	{ (Il2CppRGCTXDataType)2, 63541 },
	{ (Il2CppRGCTXDataType)3, 62129 },
	{ (Il2CppRGCTXDataType)2, 63542 },
	{ (Il2CppRGCTXDataType)2, 63543 },
	{ (Il2CppRGCTXDataType)2, 49618 },
	{ (Il2CppRGCTXDataType)2, 63544 },
	{ (Il2CppRGCTXDataType)2, 49620 },
	{ (Il2CppRGCTXDataType)2, 63545 },
	{ (Il2CppRGCTXDataType)3, 62130 },
	{ (Il2CppRGCTXDataType)2, 63546 },
	{ (Il2CppRGCTXDataType)2, 49623 },
	{ (Il2CppRGCTXDataType)2, 63547 },
	{ (Il2CppRGCTXDataType)3, 62131 },
	{ (Il2CppRGCTXDataType)2, 63548 },
	{ (Il2CppRGCTXDataType)3, 62132 },
	{ (Il2CppRGCTXDataType)2, 49629 },
	{ (Il2CppRGCTXDataType)2, 49631 },
	{ (Il2CppRGCTXDataType)2, 63549 },
	{ (Il2CppRGCTXDataType)3, 62133 },
	{ (Il2CppRGCTXDataType)2, 63550 },
	{ (Il2CppRGCTXDataType)2, 49634 },
	{ (Il2CppRGCTXDataType)2, 49636 },
	{ (Il2CppRGCTXDataType)2, 63551 },
	{ (Il2CppRGCTXDataType)3, 62134 },
	{ (Il2CppRGCTXDataType)2, 63552 },
	{ (Il2CppRGCTXDataType)3, 62135 },
	{ (Il2CppRGCTXDataType)3, 62136 },
	{ (Il2CppRGCTXDataType)2, 63553 },
	{ (Il2CppRGCTXDataType)2, 49641 },
	{ (Il2CppRGCTXDataType)2, 63554 },
	{ (Il2CppRGCTXDataType)2, 49643 },
	{ (Il2CppRGCTXDataType)2, 49644 },
	{ (Il2CppRGCTXDataType)2, 63555 },
	{ (Il2CppRGCTXDataType)3, 62137 },
	{ (Il2CppRGCTXDataType)3, 62138 },
	{ (Il2CppRGCTXDataType)3, 62139 },
	{ (Il2CppRGCTXDataType)2, 49650 },
	{ (Il2CppRGCTXDataType)3, 62140 },
	{ (Il2CppRGCTXDataType)3, 62141 },
	{ (Il2CppRGCTXDataType)2, 49662 },
	{ (Il2CppRGCTXDataType)2, 63556 },
	{ (Il2CppRGCTXDataType)3, 62142 },
	{ (Il2CppRGCTXDataType)3, 62143 },
	{ (Il2CppRGCTXDataType)2, 49664 },
	{ (Il2CppRGCTXDataType)2, 63293 },
	{ (Il2CppRGCTXDataType)3, 62144 },
	{ (Il2CppRGCTXDataType)3, 62145 },
	{ (Il2CppRGCTXDataType)2, 63557 },
	{ (Il2CppRGCTXDataType)3, 62146 },
	{ (Il2CppRGCTXDataType)3, 62147 },
	{ (Il2CppRGCTXDataType)2, 49674 },
	{ (Il2CppRGCTXDataType)2, 63558 },
	{ (Il2CppRGCTXDataType)3, 62148 },
	{ (Il2CppRGCTXDataType)3, 62149 },
	{ (Il2CppRGCTXDataType)3, 60298 },
	{ (Il2CppRGCTXDataType)3, 62150 },
	{ (Il2CppRGCTXDataType)2, 63559 },
	{ (Il2CppRGCTXDataType)3, 62151 },
	{ (Il2CppRGCTXDataType)3, 62152 },
	{ (Il2CppRGCTXDataType)2, 49686 },
	{ (Il2CppRGCTXDataType)2, 63560 },
	{ (Il2CppRGCTXDataType)3, 62153 },
	{ (Il2CppRGCTXDataType)3, 62154 },
	{ (Il2CppRGCTXDataType)3, 62155 },
	{ (Il2CppRGCTXDataType)3, 62156 },
	{ (Il2CppRGCTXDataType)3, 62157 },
	{ (Il2CppRGCTXDataType)3, 60304 },
	{ (Il2CppRGCTXDataType)3, 62158 },
	{ (Il2CppRGCTXDataType)2, 63561 },
	{ (Il2CppRGCTXDataType)3, 62159 },
	{ (Il2CppRGCTXDataType)3, 62160 },
	{ (Il2CppRGCTXDataType)2, 49699 },
	{ (Il2CppRGCTXDataType)2, 63562 },
	{ (Il2CppRGCTXDataType)3, 62161 },
	{ (Il2CppRGCTXDataType)3, 62162 },
	{ (Il2CppRGCTXDataType)2, 49701 },
	{ (Il2CppRGCTXDataType)2, 63563 },
	{ (Il2CppRGCTXDataType)3, 62163 },
	{ (Il2CppRGCTXDataType)3, 62164 },
	{ (Il2CppRGCTXDataType)2, 63564 },
	{ (Il2CppRGCTXDataType)3, 62165 },
	{ (Il2CppRGCTXDataType)3, 62166 },
	{ (Il2CppRGCTXDataType)2, 63565 },
	{ (Il2CppRGCTXDataType)3, 62167 },
	{ (Il2CppRGCTXDataType)3, 62168 },
	{ (Il2CppRGCTXDataType)2, 49716 },
	{ (Il2CppRGCTXDataType)2, 63566 },
	{ (Il2CppRGCTXDataType)3, 62169 },
	{ (Il2CppRGCTXDataType)3, 62170 },
	{ (Il2CppRGCTXDataType)3, 62171 },
	{ (Il2CppRGCTXDataType)3, 60315 },
	{ (Il2CppRGCTXDataType)2, 63567 },
	{ (Il2CppRGCTXDataType)3, 62172 },
	{ (Il2CppRGCTXDataType)3, 62173 },
	{ (Il2CppRGCTXDataType)2, 63568 },
	{ (Il2CppRGCTXDataType)3, 62174 },
	{ (Il2CppRGCTXDataType)3, 62175 },
	{ (Il2CppRGCTXDataType)2, 49732 },
	{ (Il2CppRGCTXDataType)2, 63569 },
	{ (Il2CppRGCTXDataType)3, 62176 },
	{ (Il2CppRGCTXDataType)3, 62177 },
	{ (Il2CppRGCTXDataType)3, 62178 },
	{ (Il2CppRGCTXDataType)3, 62179 },
	{ (Il2CppRGCTXDataType)3, 62180 },
	{ (Il2CppRGCTXDataType)3, 62181 },
	{ (Il2CppRGCTXDataType)3, 60321 },
	{ (Il2CppRGCTXDataType)2, 63570 },
	{ (Il2CppRGCTXDataType)3, 62182 },
	{ (Il2CppRGCTXDataType)3, 62183 },
	{ (Il2CppRGCTXDataType)2, 63571 },
	{ (Il2CppRGCTXDataType)3, 62184 },
	{ (Il2CppRGCTXDataType)3, 62185 },
	{ (Il2CppRGCTXDataType)3, 62186 },
	{ (Il2CppRGCTXDataType)3, 62187 },
	{ (Il2CppRGCTXDataType)3, 62188 },
	{ (Il2CppRGCTXDataType)3, 62189 },
	{ (Il2CppRGCTXDataType)2, 63572 },
	{ (Il2CppRGCTXDataType)2, 63573 },
	{ (Il2CppRGCTXDataType)3, 62190 },
	{ (Il2CppRGCTXDataType)2, 49767 },
	{ (Il2CppRGCTXDataType)2, 49761 },
	{ (Il2CppRGCTXDataType)3, 62191 },
	{ (Il2CppRGCTXDataType)2, 49760 },
	{ (Il2CppRGCTXDataType)2, 63574 },
	{ (Il2CppRGCTXDataType)3, 62192 },
	{ (Il2CppRGCTXDataType)3, 62193 },
	{ (Il2CppRGCTXDataType)3, 62194 },
	{ (Il2CppRGCTXDataType)2, 49780 },
	{ (Il2CppRGCTXDataType)2, 49775 },
	{ (Il2CppRGCTXDataType)3, 62195 },
	{ (Il2CppRGCTXDataType)2, 49774 },
	{ (Il2CppRGCTXDataType)2, 63575 },
	{ (Il2CppRGCTXDataType)3, 62196 },
	{ (Il2CppRGCTXDataType)3, 62197 },
	{ (Il2CppRGCTXDataType)3, 62198 },
	{ (Il2CppRGCTXDataType)2, 63576 },
	{ (Il2CppRGCTXDataType)3, 62199 },
	{ (Il2CppRGCTXDataType)2, 49793 },
	{ (Il2CppRGCTXDataType)2, 49785 },
	{ (Il2CppRGCTXDataType)3, 62200 },
	{ (Il2CppRGCTXDataType)3, 62201 },
	{ (Il2CppRGCTXDataType)2, 49784 },
	{ (Il2CppRGCTXDataType)2, 63577 },
	{ (Il2CppRGCTXDataType)3, 62202 },
	{ (Il2CppRGCTXDataType)3, 62203 },
	{ (Il2CppRGCTXDataType)3, 62204 },
	{ (Il2CppRGCTXDataType)3, 62205 },
	{ (Il2CppRGCTXDataType)2, 63578 },
	{ (Il2CppRGCTXDataType)3, 62206 },
	{ (Il2CppRGCTXDataType)2, 49806 },
	{ (Il2CppRGCTXDataType)2, 49798 },
	{ (Il2CppRGCTXDataType)3, 62207 },
	{ (Il2CppRGCTXDataType)3, 62208 },
	{ (Il2CppRGCTXDataType)2, 49797 },
	{ (Il2CppRGCTXDataType)2, 63579 },
	{ (Il2CppRGCTXDataType)3, 62209 },
	{ (Il2CppRGCTXDataType)3, 62210 },
	{ (Il2CppRGCTXDataType)3, 62211 },
	{ (Il2CppRGCTXDataType)2, 63580 },
	{ (Il2CppRGCTXDataType)3, 62212 },
	{ (Il2CppRGCTXDataType)2, 49819 },
	{ (Il2CppRGCTXDataType)2, 49811 },
	{ (Il2CppRGCTXDataType)3, 62213 },
	{ (Il2CppRGCTXDataType)3, 62214 },
	{ (Il2CppRGCTXDataType)3, 62215 },
	{ (Il2CppRGCTXDataType)2, 49810 },
	{ (Il2CppRGCTXDataType)2, 63581 },
	{ (Il2CppRGCTXDataType)3, 62216 },
	{ (Il2CppRGCTXDataType)3, 62217 },
	{ (Il2CppRGCTXDataType)2, 49825 },
	{ (Il2CppRGCTXDataType)2, 63582 },
	{ (Il2CppRGCTXDataType)3, 62218 },
	{ (Il2CppRGCTXDataType)3, 62219 },
	{ (Il2CppRGCTXDataType)3, 62220 },
	{ (Il2CppRGCTXDataType)2, 63583 },
	{ (Il2CppRGCTXDataType)2, 63584 },
	{ (Il2CppRGCTXDataType)3, 62221 },
	{ (Il2CppRGCTXDataType)3, 62222 },
	{ (Il2CppRGCTXDataType)2, 49841 },
	{ (Il2CppRGCTXDataType)3, 62223 },
	{ (Il2CppRGCTXDataType)2, 49842 },
	{ (Il2CppRGCTXDataType)2, 63585 },
	{ (Il2CppRGCTXDataType)3, 62224 },
	{ (Il2CppRGCTXDataType)3, 62225 },
	{ (Il2CppRGCTXDataType)2, 63586 },
	{ (Il2CppRGCTXDataType)3, 62226 },
	{ (Il2CppRGCTXDataType)2, 63587 },
	{ (Il2CppRGCTXDataType)3, 62227 },
	{ (Il2CppRGCTXDataType)3, 62228 },
	{ (Il2CppRGCTXDataType)3, 62229 },
	{ (Il2CppRGCTXDataType)2, 49861 },
	{ (Il2CppRGCTXDataType)3, 62230 },
	{ (Il2CppRGCTXDataType)2, 49869 },
	{ (Il2CppRGCTXDataType)3, 62231 },
	{ (Il2CppRGCTXDataType)2, 63588 },
	{ (Il2CppRGCTXDataType)2, 63589 },
	{ (Il2CppRGCTXDataType)3, 62232 },
	{ (Il2CppRGCTXDataType)3, 62233 },
	{ (Il2CppRGCTXDataType)3, 62234 },
	{ (Il2CppRGCTXDataType)3, 62235 },
	{ (Il2CppRGCTXDataType)3, 62236 },
	{ (Il2CppRGCTXDataType)3, 62237 },
	{ (Il2CppRGCTXDataType)2, 49885 },
	{ (Il2CppRGCTXDataType)2, 63590 },
	{ (Il2CppRGCTXDataType)3, 62238 },
	{ (Il2CppRGCTXDataType)3, 62239 },
	{ (Il2CppRGCTXDataType)2, 49889 },
	{ (Il2CppRGCTXDataType)3, 62240 },
	{ (Il2CppRGCTXDataType)2, 63591 },
	{ (Il2CppRGCTXDataType)2, 49899 },
	{ (Il2CppRGCTXDataType)2, 49897 },
	{ (Il2CppRGCTXDataType)2, 63592 },
	{ (Il2CppRGCTXDataType)3, 62241 },
	{ (Il2CppRGCTXDataType)2, 63593 },
	{ (Il2CppRGCTXDataType)3, 62242 },
	{ (Il2CppRGCTXDataType)3, 62243 },
	{ (Il2CppRGCTXDataType)3, 62244 },
	{ (Il2CppRGCTXDataType)2, 49903 },
	{ (Il2CppRGCTXDataType)3, 62245 },
	{ (Il2CppRGCTXDataType)3, 62246 },
	{ (Il2CppRGCTXDataType)2, 49906 },
	{ (Il2CppRGCTXDataType)3, 62247 },
	{ (Il2CppRGCTXDataType)1, 63594 },
	{ (Il2CppRGCTXDataType)2, 49905 },
	{ (Il2CppRGCTXDataType)3, 62248 },
	{ (Il2CppRGCTXDataType)1, 49905 },
	{ (Il2CppRGCTXDataType)1, 49903 },
	{ (Il2CppRGCTXDataType)2, 63595 },
	{ (Il2CppRGCTXDataType)2, 49905 },
	{ (Il2CppRGCTXDataType)2, 49908 },
	{ (Il2CppRGCTXDataType)2, 49907 },
	{ (Il2CppRGCTXDataType)3, 62249 },
	{ (Il2CppRGCTXDataType)3, 62250 },
	{ (Il2CppRGCTXDataType)3, 62251 },
	{ (Il2CppRGCTXDataType)3, 62252 },
	{ (Il2CppRGCTXDataType)3, 62253 },
	{ (Il2CppRGCTXDataType)2, 49904 },
	{ (Il2CppRGCTXDataType)3, 62254 },
	{ (Il2CppRGCTXDataType)2, 49918 },
};
extern const Il2CppCodeGenModule g_System_CoreCodeGenModule;
const Il2CppCodeGenModule g_System_CoreCodeGenModule = 
{
	"System.Core.dll",
	259,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	67,
	s_rgctxIndices,
	311,
	s_rgctxValues,
	NULL,
};
