﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void MixedRealityCameraSystem__ctor_m8AD6C00917590A7E839E4D0BF05E3D66211D3285 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.ctor(Microsoft.MixedReality.Toolkit.BaseMixedRealityProfile)
extern void MixedRealityCameraSystem__ctor_mD21AE8F7C64FE42CA227F420F8EE4D170D1DA809 (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_Name()
extern void MixedRealityCameraSystem_get_Name_mFD9A5572B2828F28CC9AD2E9AE4898DD46061550 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::set_Name(System.String)
extern void MixedRealityCameraSystem_set_Name_m6C5B3B90255668F49DCAE3DA7D8F0C48F0BCF99D (void);
// 0x00000005 System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_IsOpaque()
extern void MixedRealityCameraSystem_get_IsOpaque_m41F5BCBDEA97180BA63247873C0DE79AF093392F (void);
// 0x00000006 System.UInt32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceId()
extern void MixedRealityCameraSystem_get_SourceId_m01A37E00E012A33DFC5AE95A4DB566B62ED83884 (void);
// 0x00000007 System.String Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_SourceName()
extern void MixedRealityCameraSystem_get_SourceName_mB33317E68AD27304CB78DFBBFF5B17E887A5C96E (void);
// 0x00000008 Microsoft.MixedReality.Toolkit.MixedRealityCameraProfile Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::get_CameraProfile()
extern void MixedRealityCameraSystem_get_CameraProfile_m92A48C05B067ED9B95BDDA5790D340CC1233BE42 (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Initialize()
extern void MixedRealityCameraSystem_Initialize_m9A8AEEBEC11A7775ECE3753DCF7539AE867FE448 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Enable()
extern void MixedRealityCameraSystem_Enable_m20A6E3471063834656EB99EC6C2271273A872338 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Disable()
extern void MixedRealityCameraSystem_Disable_mEC3BBD6E6E66631708ED70C550CB469EA5E6154D (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Destroy()
extern void MixedRealityCameraSystem_Destroy_mE11FFBFBCB1E22E284A69F38772440D29F9BF73A (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::Update()
extern void MixedRealityCameraSystem_Update_mED8048E5FA2D9DABC72835207421D85FCD8F4036 (void);
// 0x0000000E System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForOpaqueDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_mE07AC7DBCC158C39B29CE6FF1B0CCF0C98AF67D0 (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::ApplySettingsForTransparentDisplay()
extern void MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_m5CF2D9754D2DC5191D259E3D090E57D0A034BAB4 (void);
// 0x00000010 System.Boolean Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_m332B9D612460988097D1C0C31CED624D11D504C2 (void);
// 0x00000011 System.Int32 Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern void MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mF86A87A94317555C38FA1F06486D3819AF614E81 (void);
// 0x00000012 System.Void Microsoft.MixedReality.Toolkit.CameraSystem.MixedRealityCameraSystem::.cctor()
extern void MixedRealityCameraSystem__cctor_m073DAC6E4E6E83A6BCA58E2B54589C5DA1633CB2 (void);
static Il2CppMethodPointer s_methodPointers[18] = 
{
	MixedRealityCameraSystem__ctor_m8AD6C00917590A7E839E4D0BF05E3D66211D3285,
	MixedRealityCameraSystem__ctor_mD21AE8F7C64FE42CA227F420F8EE4D170D1DA809,
	MixedRealityCameraSystem_get_Name_mFD9A5572B2828F28CC9AD2E9AE4898DD46061550,
	MixedRealityCameraSystem_set_Name_m6C5B3B90255668F49DCAE3DA7D8F0C48F0BCF99D,
	MixedRealityCameraSystem_get_IsOpaque_m41F5BCBDEA97180BA63247873C0DE79AF093392F,
	MixedRealityCameraSystem_get_SourceId_m01A37E00E012A33DFC5AE95A4DB566B62ED83884,
	MixedRealityCameraSystem_get_SourceName_mB33317E68AD27304CB78DFBBFF5B17E887A5C96E,
	MixedRealityCameraSystem_get_CameraProfile_m92A48C05B067ED9B95BDDA5790D340CC1233BE42,
	MixedRealityCameraSystem_Initialize_m9A8AEEBEC11A7775ECE3753DCF7539AE867FE448,
	MixedRealityCameraSystem_Enable_m20A6E3471063834656EB99EC6C2271273A872338,
	MixedRealityCameraSystem_Disable_mEC3BBD6E6E66631708ED70C550CB469EA5E6154D,
	MixedRealityCameraSystem_Destroy_mE11FFBFBCB1E22E284A69F38772440D29F9BF73A,
	MixedRealityCameraSystem_Update_mED8048E5FA2D9DABC72835207421D85FCD8F4036,
	MixedRealityCameraSystem_ApplySettingsForOpaqueDisplay_mE07AC7DBCC158C39B29CE6FF1B0CCF0C98AF67D0,
	MixedRealityCameraSystem_ApplySettingsForTransparentDisplay_m5CF2D9754D2DC5191D259E3D090E57D0A034BAB4,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_Equals_m332B9D612460988097D1C0C31CED624D11D504C2,
	MixedRealityCameraSystem_System_Collections_IEqualityComparer_GetHashCode_mF86A87A94317555C38FA1F06486D3819AF614E81,
	MixedRealityCameraSystem__cctor_m073DAC6E4E6E83A6BCA58E2B54589C5DA1633CB2,
};
static const int32_t s_InvokerIndices[18] = 
{
	27,
	26,
	14,
	26,
	89,
	10,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	90,
	121,
	3,
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_CameraSystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_CameraSystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.CameraSystem.dll",
	18,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
