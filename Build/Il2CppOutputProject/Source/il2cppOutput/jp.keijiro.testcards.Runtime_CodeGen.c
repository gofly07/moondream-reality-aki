﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 TestCards.TestOverlay/Mode TestCards.TestOverlay::get_mode()
extern void TestOverlay_get_mode_m9D5B0D57FC7B3D6DCEE5A96A19582B5FD3228647 (void);
// 0x00000002 System.Void TestCards.TestOverlay::set_mode(TestCards.TestOverlay/Mode)
extern void TestOverlay_set_mode_m1D9B2C50A7401B568514F813EC41849852357CE2 (void);
// 0x00000003 UnityEngine.Color TestCards.TestOverlay::get_color()
extern void TestOverlay_get_color_mF130EF43A1D8845C493BA7ED3D3ADCC9357564A6 (void);
// 0x00000004 System.Void TestCards.TestOverlay::set_color(UnityEngine.Color)
extern void TestOverlay_set_color_mC5DAE4A036A74F9DA347B025B5C1CDBB155820FF (void);
// 0x00000005 System.Int32 TestCards.TestOverlay::get_scale()
extern void TestOverlay_get_scale_m7288D56B2EA0ED28F1BA1059F24DBEA43CC9B372 (void);
// 0x00000006 System.Void TestCards.TestOverlay::set_scale(System.Int32)
extern void TestOverlay_set_scale_m57004F892451BEC9E0AD61966DEBD55B4BFDFA2D (void);
// 0x00000007 System.Void TestCards.TestOverlay::OnDestroy()
extern void TestOverlay_OnDestroy_mC8A7FF0FB761AA4B8D58995B65A0050305D3402F (void);
// 0x00000008 System.Void TestCards.TestOverlay::OnRenderImage(UnityEngine.RenderTexture,UnityEngine.RenderTexture)
extern void TestOverlay_OnRenderImage_m040B904EF11503B2213B5845FE67658F5564D973 (void);
// 0x00000009 System.Void TestCards.TestOverlay::.ctor()
extern void TestOverlay__ctor_m6A964B36FC1CB219B05537CFB8EF55EFBA56C61B (void);
static Il2CppMethodPointer s_methodPointers[9] = 
{
	TestOverlay_get_mode_m9D5B0D57FC7B3D6DCEE5A96A19582B5FD3228647,
	TestOverlay_set_mode_m1D9B2C50A7401B568514F813EC41849852357CE2,
	TestOverlay_get_color_mF130EF43A1D8845C493BA7ED3D3ADCC9357564A6,
	TestOverlay_set_color_mC5DAE4A036A74F9DA347B025B5C1CDBB155820FF,
	TestOverlay_get_scale_m7288D56B2EA0ED28F1BA1059F24DBEA43CC9B372,
	TestOverlay_set_scale_m57004F892451BEC9E0AD61966DEBD55B4BFDFA2D,
	TestOverlay_OnDestroy_mC8A7FF0FB761AA4B8D58995B65A0050305D3402F,
	TestOverlay_OnRenderImage_m040B904EF11503B2213B5845FE67658F5564D973,
	TestOverlay__ctor_m6A964B36FC1CB219B05537CFB8EF55EFBA56C61B,
};
static const int32_t s_InvokerIndices[9] = 
{
	10,
	32,
	1354,
	1355,
	10,
	32,
	23,
	27,
	23,
};
extern const Il2CppCodeGenModule g_jp_keijiro_testcards_RuntimeCodeGenModule;
const Il2CppCodeGenModule g_jp_keijiro_testcards_RuntimeCodeGenModule = 
{
	"jp.keijiro.testcards.Runtime.dll",
	9,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
