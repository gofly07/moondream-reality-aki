﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.String UnityEngine.PhysicsScene2D::ToString()
extern void PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE (void);
// 0x00000002 System.Int32 UnityEngine.PhysicsScene2D::GetHashCode()
extern void PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467 (void);
// 0x00000003 System.Boolean UnityEngine.PhysicsScene2D::Equals(System.Object)
extern void PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8 (void);
// 0x00000004 System.Boolean UnityEngine.PhysicsScene2D::Equals(UnityEngine.PhysicsScene2D)
extern void PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293 (void);
// 0x00000005 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987 (void);
// 0x00000006 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8 (void);
// 0x00000007 UnityEngine.RaycastHit2D UnityEngine.PhysicsScene2D::Raycast_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D)
extern void PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718 (void);
// 0x00000008 System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A (void);
// 0x00000009 System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D (void);
// 0x0000000A System.Int32 UnityEngine.PhysicsScene2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24 (void);
// 0x0000000B System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector2,UnityEngine.Vector2,System.Single,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09 (void);
// 0x0000000C System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersection(UnityEngine.Ray,System.Single,UnityEngine.RaycastHit2D[],System.Int32)
extern void PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC (void);
// 0x0000000D System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5 (void);
// 0x0000000E System.Void UnityEngine.PhysicsScene2D::Raycast_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D&)
extern void PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A (void);
// 0x0000000F System.Int32 UnityEngine.PhysicsScene2D::RaycastArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143 (void);
// 0x00000010 System.Int32 UnityEngine.PhysicsScene2D::RaycastList_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,UnityEngine.ContactFilter2D&,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>)
extern void PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B (void);
// 0x00000011 System.Int32 UnityEngine.PhysicsScene2D::GetRayIntersectionArray_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32,UnityEngine.RaycastHit2D[])
extern void PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9 (void);
// 0x00000012 UnityEngine.PhysicsScene2D UnityEngine.Physics2D::get_defaultPhysicsScene()
extern void Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84 (void);
// 0x00000013 System.Boolean UnityEngine.Physics2D::get_queriesHitTriggers()
extern void Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B (void);
// 0x00000014 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2)
extern void Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC (void);
// 0x00000015 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single)
extern void Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF (void);
// 0x00000016 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern void Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD (void);
// 0x00000017 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single)
extern void Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0 (void);
// 0x00000018 UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern void Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78 (void);
// 0x00000019 System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[])
extern void Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE (void);
// 0x0000001A System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543 (void);
// 0x0000001B System.Int32 UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,UnityEngine.ContactFilter2D,System.Collections.Generic.List`1<UnityEngine.RaycastHit2D>,System.Single)
extern void Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645 (void);
// 0x0000001C UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray)
extern void Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8 (void);
// 0x0000001D UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single)
extern void Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A (void);
// 0x0000001E UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll(UnityEngine.Ray,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053 (void);
// 0x0000001F UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal(UnityEngine.PhysicsScene2D,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612 (void);
// 0x00000020 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[])
extern void Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0 (void);
// 0x00000021 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single)
extern void Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E (void);
// 0x00000022 System.Int32 UnityEngine.Physics2D::GetRayIntersectionNonAlloc(UnityEngine.Ray,UnityEngine.RaycastHit2D[],System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A (void);
// 0x00000023 System.Void UnityEngine.Physics2D::.cctor()
extern void Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC (void);
// 0x00000024 UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::GetRayIntersectionAll_Internal_Injected(UnityEngine.PhysicsScene2D&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern void Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A (void);
// 0x00000025 System.Void UnityEngine.ContactFilter2D::CheckConsistency()
extern void ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2 (void);
// 0x00000026 System.Void UnityEngine.ContactFilter2D::SetLayerMask(UnityEngine.LayerMask)
extern void ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6 (void);
// 0x00000027 System.Void UnityEngine.ContactFilter2D::SetDepth(System.Single,System.Single)
extern void ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0 (void);
// 0x00000028 UnityEngine.ContactFilter2D UnityEngine.ContactFilter2D::CreateLegacyFilter(System.Int32,System.Single,System.Single)
extern void ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51 (void);
// 0x00000029 System.Void UnityEngine.ContactFilter2D::CheckConsistency_Injected(UnityEngine.ContactFilter2D&)
extern void ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE (void);
// 0x0000002A UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern void RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098 (void);
// 0x0000002B UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern void RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4 (void);
// 0x0000002C System.Single UnityEngine.RaycastHit2D::get_distance()
extern void RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F (void);
// 0x0000002D UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern void RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976 (void);
// 0x0000002E System.Single UnityEngine.Rigidbody2D::get_rotation()
extern void Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C (void);
// 0x0000002F UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_velocity()
extern void Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1 (void);
// 0x00000030 System.Void UnityEngine.Rigidbody2D::set_velocity(UnityEngine.Vector2)
extern void Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83 (void);
// 0x00000031 System.Single UnityEngine.Rigidbody2D::get_angularVelocity()
extern void Rigidbody2D_get_angularVelocity_m98681305F4188D29DA152F508BE7B73260CCEE46 (void);
// 0x00000032 System.Void UnityEngine.Rigidbody2D::set_angularVelocity(System.Single)
extern void Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599 (void);
// 0x00000033 System.Single UnityEngine.Rigidbody2D::get_mass()
extern void Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9 (void);
// 0x00000034 UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_centerOfMass()
extern void Rigidbody2D_get_centerOfMass_mAB7A2DC45DC8100A6F8961CCA78A634F73786F54 (void);
// 0x00000035 System.Single UnityEngine.Rigidbody2D::get_inertia()
extern void Rigidbody2D_get_inertia_m628BE46BFD67F6664A06772A198D9FD37980A9F1 (void);
// 0x00000036 UnityEngine.RigidbodyType2D UnityEngine.Rigidbody2D::get_bodyType()
extern void Rigidbody2D_get_bodyType_m3C4E5E37E357E2D8AA862B6BEE14ADA4FD5BED89 (void);
// 0x00000037 System.Boolean UnityEngine.Rigidbody2D::get_isKinematic()
extern void Rigidbody2D_get_isKinematic_mAE51BC33F3534009C53385BCA47A19D27DD9DE46 (void);
// 0x00000038 UnityEngine.RigidbodyConstraints2D UnityEngine.Rigidbody2D::get_constraints()
extern void Rigidbody2D_get_constraints_m7C83CD3E42F3A9C6A1BC07526CAE4E39311F64E5 (void);
// 0x00000039 System.Void UnityEngine.Rigidbody2D::get_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C (void);
// 0x0000003A System.Void UnityEngine.Rigidbody2D::set_velocity_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E (void);
// 0x0000003B System.Void UnityEngine.Rigidbody2D::get_centerOfMass_Injected(UnityEngine.Vector2&)
extern void Rigidbody2D_get_centerOfMass_Injected_mBFDAE37580FF2E919A1E10FE42371535B017DBAE (void);
// 0x0000003C System.Boolean UnityEngine.Collider2D::get_isTrigger()
extern void Collider2D_get_isTrigger_m72C2C32959124D4FB91A83B56E5D7D5204B87E48 (void);
// 0x0000003D UnityEngine.Vector2 UnityEngine.Collider2D::get_offset()
extern void Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D (void);
// 0x0000003E UnityEngine.Bounds UnityEngine.Collider2D::get_bounds()
extern void Collider2D_get_bounds_mB13BA419529917B7F97F5EFC599D8D92B0603359 (void);
// 0x0000003F System.Void UnityEngine.Collider2D::get_offset_Injected(UnityEngine.Vector2&)
extern void Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97 (void);
// 0x00000040 System.Void UnityEngine.Collider2D::get_bounds_Injected(UnityEngine.Bounds&)
extern void Collider2D_get_bounds_Injected_mBABA06A9F645F3A24D7272B868B1782814E21A98 (void);
// 0x00000041 System.Single UnityEngine.CircleCollider2D::get_radius()
extern void CircleCollider2D_get_radius_m016333FD7A5A5FD84FEFD7D02B57D4CA728EFA27 (void);
// 0x00000042 UnityEngine.Vector2 UnityEngine.CapsuleCollider2D::get_size()
extern void CapsuleCollider2D_get_size_m2E344F2DA3881C6E8420681C40A1218DFD420A60 (void);
// 0x00000043 UnityEngine.CapsuleDirection2D UnityEngine.CapsuleCollider2D::get_direction()
extern void CapsuleCollider2D_get_direction_mD58B574E6FD8F7C7D2241FFCF837C4DD6E501EA7 (void);
// 0x00000044 System.Void UnityEngine.CapsuleCollider2D::get_size_Injected(UnityEngine.Vector2&)
extern void CapsuleCollider2D_get_size_Injected_m595751B395DB00016B61BD538285D9A8F902064A (void);
// 0x00000045 System.Single UnityEngine.EdgeCollider2D::get_edgeRadius()
extern void EdgeCollider2D_get_edgeRadius_m334999C8FC0F38B1DC2EF5D27A2F6C893075A638 (void);
// 0x00000046 System.Int32 UnityEngine.EdgeCollider2D::get_edgeCount()
extern void EdgeCollider2D_get_edgeCount_mF1E98AFD51DF7A8800E24060AA3F75499D80153C (void);
// 0x00000047 UnityEngine.Vector2[] UnityEngine.EdgeCollider2D::get_points()
extern void EdgeCollider2D_get_points_mC2D85B741B8BCF0CB05B129EE1EE4CA0C86E4291 (void);
// 0x00000048 UnityEngine.Vector2 UnityEngine.BoxCollider2D::get_size()
extern void BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1 (void);
// 0x00000049 System.Single UnityEngine.BoxCollider2D::get_edgeRadius()
extern void BoxCollider2D_get_edgeRadius_m95CF9B39CF125CBE980C5A27ABD10DAB02499145 (void);
// 0x0000004A System.Void UnityEngine.BoxCollider2D::get_size_Injected(UnityEngine.Vector2&)
extern void BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD (void);
static Il2CppMethodPointer s_methodPointers[74] = 
{
	PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE,
	PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467,
	PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8,
	PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293,
	PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987,
	PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8,
	PhysicsScene2D_Raycast_Internal_mB24F5D2B6967C70371484EA703E16346DBFD0718,
	PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A,
	PhysicsScene2D_RaycastArray_Internal_m40B8BDD4BE4D95E3826334DA2A8E31EBAD7B6E8D,
	PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24,
	PhysicsScene2D_RaycastList_Internal_m4D2446707FAC9EC36975B8119616F30BB724EA09,
	PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC,
	PhysicsScene2D_GetRayIntersectionArray_Internal_m1A9DC1520B80AF8444C38FEEDEB40EDD405805A5,
	PhysicsScene2D_Raycast_Internal_Injected_m197B563F302D9E7C336EE7BB0A356F6785F1584A,
	PhysicsScene2D_RaycastArray_Internal_Injected_mC5FDF82692390ECAB17CF821D25349A66B3C8143,
	PhysicsScene2D_RaycastList_Internal_Injected_mD8495122B2F8BD1194E83CA20DFC005D414C707B,
	PhysicsScene2D_GetRayIntersectionArray_Internal_Injected_m74194745127DA849411A2191EE7C52EB07BB21A9,
	Physics2D_get_defaultPhysicsScene_m2C9DA4DFAFB71332EC48E50CCB16275441CADE84,
	Physics2D_get_queriesHitTriggers_m8BB98B1754A86777B4D58A4F28F63E8EC77B031B,
	Physics2D_Raycast_mD22D6BC52ACAB22598A720525B3840C019842FFC,
	Physics2D_Raycast_m468BF2D74BED92728533EA2108830C44ED93A0EF,
	Physics2D_Raycast_mBEB66E9AA034BD0AE1B1C99DF872247B0131CBDD,
	Physics2D_Raycast_mB43742B1077F487D1458388C5B11EE46D73533C0,
	Physics2D_Raycast_m4803AD692674FEE7EE269A6170AD5CEFEA6D3D78,
	Physics2D_Raycast_m0C22B1CACFA7E2A16D731B6E2D9D2ABC0666CCCE,
	Physics2D_Raycast_m8678AB161A71C09D7606299D194A90BA814BA543,
	Physics2D_Raycast_m940284F559A12F0594CF6E1A20583F7EA67E8645,
	Physics2D_GetRayIntersectionAll_mBD650C3EA6E692CE3E1255B6EAADF659307012D8,
	Physics2D_GetRayIntersectionAll_mACC24DD73E1388C1DF86847390B89A5B0223F03A,
	Physics2D_GetRayIntersectionAll_m04BCAB03333B049C48BE61036E512E12A5FBD053,
	Physics2D_GetRayIntersectionAll_Internal_m4E68866CF4A79A58DBF4B8A355D3EEE62BEF6612,
	Physics2D_GetRayIntersectionNonAlloc_m5F1AF31EEB67FE97AD2C40C102914371C2E825F0,
	Physics2D_GetRayIntersectionNonAlloc_m1A638894F08E9F401C7161D02171805B4897B51E,
	Physics2D_GetRayIntersectionNonAlloc_m3817EA2CC7B95C89683ACE0E433D6D4C6735CA0A,
	Physics2D__cctor_mC0D622F2EAF13BF0513DB2969E50EEC5631CDBFC,
	Physics2D_GetRayIntersectionAll_Internal_Injected_m8B627D4448B34665FC8BCF560EE851152FE2D15A,
	ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2,
	ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6,
	ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0,
	ContactFilter2D_CreateLegacyFilter_mA52A1C54BA7C4A49094B172BE7FA6044EF346A51,
	ContactFilter2D_CheckConsistency_Injected_m4640AA8896FEFE90396C5B47C4FE07930DA918BE,
	RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098,
	RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4,
	RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F,
	RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976,
	Rigidbody2D_get_rotation_mAF8F2E151EF82D8CF48DEDC17FE6882C2A67AF5C,
	Rigidbody2D_get_velocity_m5ABF36BDF90FD7308BE608667B9E8F3DA5A207F1,
	Rigidbody2D_set_velocity_mE0DBCE5B683024B106C2AB6943BBA550B5BD0B83,
	Rigidbody2D_get_angularVelocity_m98681305F4188D29DA152F508BE7B73260CCEE46,
	Rigidbody2D_set_angularVelocity_mEA0807B27FFA3A839397575C2696F6B15693C599,
	Rigidbody2D_get_mass_mD217EC45743AB52C6555287A0FF765BC7F6002E9,
	Rigidbody2D_get_centerOfMass_mAB7A2DC45DC8100A6F8961CCA78A634F73786F54,
	Rigidbody2D_get_inertia_m628BE46BFD67F6664A06772A198D9FD37980A9F1,
	Rigidbody2D_get_bodyType_m3C4E5E37E357E2D8AA862B6BEE14ADA4FD5BED89,
	Rigidbody2D_get_isKinematic_mAE51BC33F3534009C53385BCA47A19D27DD9DE46,
	Rigidbody2D_get_constraints_m7C83CD3E42F3A9C6A1BC07526CAE4E39311F64E5,
	Rigidbody2D_get_velocity_Injected_mFB508815764F2895389C2B75002D3D7A33630B3C,
	Rigidbody2D_set_velocity_Injected_m8DCAEDCB8C0165DCEC87B70089C7B4EA41FEB73E,
	Rigidbody2D_get_centerOfMass_Injected_mBFDAE37580FF2E919A1E10FE42371535B017DBAE,
	Collider2D_get_isTrigger_m72C2C32959124D4FB91A83B56E5D7D5204B87E48,
	Collider2D_get_offset_mCB3DEFB9ACB05211320B8406B01F089EF7F8788D,
	Collider2D_get_bounds_mB13BA419529917B7F97F5EFC599D8D92B0603359,
	Collider2D_get_offset_Injected_mFBEEA538206330D2D4C62A04DF339332596C0F97,
	Collider2D_get_bounds_Injected_mBABA06A9F645F3A24D7272B868B1782814E21A98,
	CircleCollider2D_get_radius_m016333FD7A5A5FD84FEFD7D02B57D4CA728EFA27,
	CapsuleCollider2D_get_size_m2E344F2DA3881C6E8420681C40A1218DFD420A60,
	CapsuleCollider2D_get_direction_mD58B574E6FD8F7C7D2241FFCF837C4DD6E501EA7,
	CapsuleCollider2D_get_size_Injected_m595751B395DB00016B61BD538285D9A8F902064A,
	EdgeCollider2D_get_edgeRadius_m334999C8FC0F38B1DC2EF5D27A2F6C893075A638,
	EdgeCollider2D_get_edgeCount_mF1E98AFD51DF7A8800E24060AA3F75499D80153C,
	EdgeCollider2D_get_points_mC2D85B741B8BCF0CB05B129EE1EE4CA0C86E4291,
	BoxCollider2D_get_size_m6230015317115D9BED5C61A4EDAC013C8A7664E1,
	BoxCollider2D_get_edgeRadius_m95CF9B39CF125CBE980C5A27ABD10DAB02499145,
	BoxCollider2D_get_size_Injected_m6E18F627969D38CF513DB4CF680ACD51810F74CD,
};
extern void PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk (void);
extern void PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk (void);
extern void PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk (void);
extern void PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk (void);
extern void PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk (void);
extern void PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk (void);
extern void ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk (void);
extern void ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk (void);
extern void ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk (void);
extern void RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk (void);
extern void RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk (void);
extern void RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk (void);
extern void RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[16] = 
{
	{ 0x06000001, PhysicsScene2D_ToString_m6F48AC6CE0D8540FCE4914ABB78ED0BAF0D83CBE_AdjustorThunk },
	{ 0x06000002, PhysicsScene2D_GetHashCode_mB1C0E9E977ACCBF0AA0D266E5851B4D778354467_AdjustorThunk },
	{ 0x06000003, PhysicsScene2D_Equals_mA91E96FDE086CF876D4D469CBFF0D43400C834E8_AdjustorThunk },
	{ 0x06000004, PhysicsScene2D_Equals_mAA6F413AD3CDDD052496FAF69C34A45CA25D4293_AdjustorThunk },
	{ 0x06000005, PhysicsScene2D_Raycast_m8A048506EDDC5C968DB55584FBF650DAB3BCB987_AdjustorThunk },
	{ 0x06000006, PhysicsScene2D_Raycast_mFA61658E024A98E2A7BC1B6965E9E5537DCA2DB8_AdjustorThunk },
	{ 0x06000008, PhysicsScene2D_Raycast_mE0460FE0CEE7076962DC2983A7B0DBB757DB133A_AdjustorThunk },
	{ 0x0600000A, PhysicsScene2D_Raycast_mC5642256C2119435B87AD87ED30086973D9F3A24_AdjustorThunk },
	{ 0x0600000C, PhysicsScene2D_GetRayIntersection_m2DB850378F1910BFC62243A1A33D8B17738882EC_AdjustorThunk },
	{ 0x06000025, ContactFilter2D_CheckConsistency_m0E1FC7D646C418F545F778197348F97ADA5409A2_AdjustorThunk },
	{ 0x06000026, ContactFilter2D_SetLayerMask_mECEE981A09393F1097555D46449ED7CA4D8659E6_AdjustorThunk },
	{ 0x06000027, ContactFilter2D_SetDepth_mF4AB9C380EDC3726D58734010BD90AD7D36ABDB0_AdjustorThunk },
	{ 0x0600002A, RaycastHit2D_get_point_mC567E234B1B673C3A9819023C3DC97C781443098_AdjustorThunk },
	{ 0x0600002B, RaycastHit2D_get_normal_m9F0974E4514AD56C00FCF6FF4CDF10AED62FE6E4_AdjustorThunk },
	{ 0x0600002C, RaycastHit2D_get_distance_m2D9F391717ECACFDA8E01A4126E0F8F59F7E774F_AdjustorThunk },
	{ 0x0600002D, RaycastHit2D_get_collider_m6A7EC53B2E179C2EFF4F29018A132B2979CBE976_AdjustorThunk },
};
static const int32_t s_InvokerIndices[74] = 
{
	14,
	10,
	9,
	2037,
	2038,
	2039,
	2040,
	2041,
	2042,
	2041,
	2042,
	2043,
	2044,
	2045,
	2046,
	2046,
	2047,
	2048,
	49,
	2049,
	2050,
	2051,
	2052,
	2053,
	2054,
	2055,
	2055,
	1926,
	1925,
	1924,
	2056,
	1930,
	1929,
	1928,
	3,
	2057,
	23,
	2058,
	1341,
	2059,
	17,
	1395,
	1395,
	726,
	14,
	726,
	1395,
	1396,
	726,
	337,
	726,
	1395,
	726,
	10,
	89,
	10,
	6,
	6,
	6,
	89,
	1395,
	1459,
	6,
	6,
	726,
	1395,
	10,
	6,
	726,
	10,
	14,
	1395,
	726,
	6,
};
extern const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Physics2DModuleCodeGenModule = 
{
	"UnityEngine.Physics2DModule.dll",
	74,
	s_methodPointers,
	16,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
