﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile)
extern void MixedRealitySpatialAwarenessSystem__ctor_m051054A769B2603936B820E3544DE6C3A59F935A (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.ctor(Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile)
extern void MixedRealitySpatialAwarenessSystem__ctor_m5C7B4D879C2C7365C25F332CE05CACD91477E24E (void);
// 0x00000003 System.String Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_Name()
extern void MixedRealitySpatialAwarenessSystem_get_Name_m085CA5B29856F78F47CF74392FCCACDA8EC5AABB (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::set_Name(System.String)
extern void MixedRealitySpatialAwarenessSystem_set_Name_m2C3D811130B6473445E3D6309417FBA28F8C544D (void);
// 0x00000005 System.Boolean Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void MixedRealitySpatialAwarenessSystem_CheckCapability_m07E82BE974AF0E39CC83C9BE0EDDFAE4DADA41C4 (void);
// 0x00000006 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Initialize()
extern void MixedRealitySpatialAwarenessSystem_Initialize_mFFDA7A016B44A3B5E73493824C5EAF4082204DC2 (void);
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::InitializeInternal()
extern void MixedRealitySpatialAwarenessSystem_InitializeInternal_m99AD5F58C86B6C7DB7E05F773B96839E67526D51 (void);
// 0x00000008 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Disable()
extern void MixedRealitySpatialAwarenessSystem_Disable_mE719FBA4361AC84D5AB3E257948F0F3DCC7869A6 (void);
// 0x00000009 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Enable()
extern void MixedRealitySpatialAwarenessSystem_Enable_mE50D8C0A123F85C38BF363DD5BD6F9C2A86EE69E (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Reset()
extern void MixedRealitySpatialAwarenessSystem_Reset_m4682E178D675FD08641B4F969A6136AFB2759931 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::Destroy()
extern void MixedRealitySpatialAwarenessSystem_Destroy_m898E18B7921522B2EC530C3050D4A17D05DD01BC (void);
// 0x0000000C UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessObjectParent()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m147836DD925C27D7620F1300BC6995A5A1142743 (void);
// 0x0000000D UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_CreateSpatialAwarenessObjectParent()
extern void MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessObjectParent_mC15C2CFE06AC5D1F5BD87013F09A8382CED39EA7 (void);
// 0x0000000E UnityEngine.GameObject Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::CreateSpatialAwarenessObservationParent(System.String)
extern void MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObservationParent_m1A5DD9D3F299D32C8DC7755E979E0655E46C746A (void);
// 0x0000000F System.UInt32 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GenerateNewSourceId()
extern void MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_mDCAD029E643AA7A6F36DC609850B92DF863C6E77 (void);
// 0x00000010 Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystemProfile Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::get_SpatialAwarenessSystemProfile()
extern void MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m074C604BCFE03BAE8E080982EB9D65823B4D7993 (void);
// 0x00000011 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
extern void MixedRealitySpatialAwarenessSystem_GetObservers_m04F30736212062A432CDDBEB02C1E7A9490B5730 (void);
// 0x00000012 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObservers()
// 0x00000013 Microsoft.MixedReality.Toolkit.SpatialAwareness.IMixedRealitySpatialAwarenessObserver Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
extern void MixedRealitySpatialAwarenessSystem_GetObserver_m19162B704A25475158B4688F51CA5230C96FF3A4 (void);
// 0x00000014 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetObserver(System.String)
// 0x00000015 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProviders()
// 0x00000016 T Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::GetDataProvider(System.String)
// 0x00000017 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
extern void MixedRealitySpatialAwarenessSystem_ResumeObservers_m5C05F9263EDEEB6CB0090E6B2519DB06218830E0 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObservers()
// 0x00000019 System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ResumeObserver(System.String)
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
extern void MixedRealitySpatialAwarenessSystem_SuspendObservers_mD31207F9089F0FD5BD20E7D7B6A5A4AD6E350CA9 (void);
// 0x0000001B System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObservers()
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::SuspendObserver(System.String)
// 0x0000001D System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservations()
extern void MixedRealitySpatialAwarenessSystem_ClearObservations_m48251FF87DE0090610C18E02F37BBAD347CB148D (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::ClearObservations(System.String)
// 0x0000001F System.Void Microsoft.MixedReality.Toolkit.SpatialAwareness.MixedRealitySpatialAwarenessSystem::.cctor()
extern void MixedRealitySpatialAwarenessSystem__cctor_mC443E75A0BCF9CB2C7D2F7AAE39B1A9544090707 (void);
static Il2CppMethodPointer s_methodPointers[31] = 
{
	MixedRealitySpatialAwarenessSystem__ctor_m051054A769B2603936B820E3544DE6C3A59F935A,
	MixedRealitySpatialAwarenessSystem__ctor_m5C7B4D879C2C7365C25F332CE05CACD91477E24E,
	MixedRealitySpatialAwarenessSystem_get_Name_m085CA5B29856F78F47CF74392FCCACDA8EC5AABB,
	MixedRealitySpatialAwarenessSystem_set_Name_m2C3D811130B6473445E3D6309417FBA28F8C544D,
	MixedRealitySpatialAwarenessSystem_CheckCapability_m07E82BE974AF0E39CC83C9BE0EDDFAE4DADA41C4,
	MixedRealitySpatialAwarenessSystem_Initialize_mFFDA7A016B44A3B5E73493824C5EAF4082204DC2,
	MixedRealitySpatialAwarenessSystem_InitializeInternal_m99AD5F58C86B6C7DB7E05F773B96839E67526D51,
	MixedRealitySpatialAwarenessSystem_Disable_mE719FBA4361AC84D5AB3E257948F0F3DCC7869A6,
	MixedRealitySpatialAwarenessSystem_Enable_mE50D8C0A123F85C38BF363DD5BD6F9C2A86EE69E,
	MixedRealitySpatialAwarenessSystem_Reset_m4682E178D675FD08641B4F969A6136AFB2759931,
	MixedRealitySpatialAwarenessSystem_Destroy_m898E18B7921522B2EC530C3050D4A17D05DD01BC,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessObjectParent_m147836DD925C27D7620F1300BC6995A5A1142743,
	MixedRealitySpatialAwarenessSystem_get_CreateSpatialAwarenessObjectParent_mC15C2CFE06AC5D1F5BD87013F09A8382CED39EA7,
	MixedRealitySpatialAwarenessSystem_CreateSpatialAwarenessObservationParent_m1A5DD9D3F299D32C8DC7755E979E0655E46C746A,
	MixedRealitySpatialAwarenessSystem_GenerateNewSourceId_mDCAD029E643AA7A6F36DC609850B92DF863C6E77,
	MixedRealitySpatialAwarenessSystem_get_SpatialAwarenessSystemProfile_m074C604BCFE03BAE8E080982EB9D65823B4D7993,
	MixedRealitySpatialAwarenessSystem_GetObservers_m04F30736212062A432CDDBEB02C1E7A9490B5730,
	NULL,
	MixedRealitySpatialAwarenessSystem_GetObserver_m19162B704A25475158B4688F51CA5230C96FF3A4,
	NULL,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_ResumeObservers_m5C05F9263EDEEB6CB0090E6B2519DB06218830E0,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_SuspendObservers_mD31207F9089F0FD5BD20E7D7B6A5A4AD6E350CA9,
	NULL,
	NULL,
	MixedRealitySpatialAwarenessSystem_ClearObservations_m48251FF87DE0090610C18E02F37BBAD347CB148D,
	NULL,
	MixedRealitySpatialAwarenessSystem__cctor_mC443E75A0BCF9CB2C7D2F7AAE39B1A9544090707,
};
static const int32_t s_InvokerIndices[31] = 
{
	27,
	26,
	14,
	26,
	30,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	14,
	28,
	10,
	14,
	14,
	-1,
	28,
	-1,
	-1,
	-1,
	23,
	-1,
	-1,
	23,
	-1,
	-1,
	23,
	-1,
	3,
};
static const Il2CppTokenRangePair s_rgctxIndices[9] = 
{
	{ 0x06000012, { 0, 1 } },
	{ 0x06000014, { 1, 1 } },
	{ 0x06000015, { 2, 2 } },
	{ 0x06000016, { 4, 2 } },
	{ 0x06000018, { 6, 1 } },
	{ 0x06000019, { 7, 1 } },
	{ 0x0600001B, { 8, 1 } },
	{ 0x0600001C, { 9, 1 } },
	{ 0x0600001E, { 10, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[12] = 
{
	{ (Il2CppRGCTXDataType)3, 62970 },
	{ (Il2CppRGCTXDataType)3, 62971 },
	{ (Il2CppRGCTXDataType)1, 61461 },
	{ (Il2CppRGCTXDataType)3, 62972 },
	{ (Il2CppRGCTXDataType)1, 61462 },
	{ (Il2CppRGCTXDataType)3, 62973 },
	{ (Il2CppRGCTXDataType)2, 63793 },
	{ (Il2CppRGCTXDataType)2, 63794 },
	{ (Il2CppRGCTXDataType)2, 63795 },
	{ (Il2CppRGCTXDataType)2, 63796 },
	{ (Il2CppRGCTXDataType)3, 62974 },
	{ (Il2CppRGCTXDataType)2, 63797 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_SpatialAwarenessSystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.SpatialAwarenessSystem.dll",
	31,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	9,
	s_rgctxIndices,
	12,
	s_rgctxValues,
	NULL,
};
