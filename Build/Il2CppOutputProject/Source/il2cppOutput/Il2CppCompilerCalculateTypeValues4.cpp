﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>
#include <stdint.h>

#include "il2cpp-class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "il2cpp-object-internals.h"


// RenderHeads.Media.AVProVideo.ApplyToMesh
struct ApplyToMesh_t8B4E643A2D878D023489D731341AACA4B44FDF4C;
// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC;
// RenderHeads.Media.AVProVideo.DisplayIMGUI
struct DisplayIMGUI_tB907379C5DB60B5477D761E5D06DE8C60C7DEF2E;
// RenderHeads.Media.AVProVideo.DisplayUGUI
struct DisplayUGUI_tA0598D2E271F82099494F777A0E6CB025AE80743;
// RenderHeads.Media.AVProVideo.MediaPlayer
struct MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC;
// RenderHeads.Media.AVProVideo.MediaPlayer[]
struct MediaPlayerU5BU5D_tE227CBE13C940DD1422DE8F855FE717D80DD897F;
// System.Char[]
struct CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2;
// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.DisplayUGUI>
struct List_1_t9CC631B72B4B3B22612C9412746684E6122BC1D2;
// System.Collections.Generic.Queue`1<System.String>
struct Queue_1_tFA2F320547F861316BAE4BB2C5C0EE1EEB8F508E;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E;
// System.Void
struct Void_t22962CB4C05B1D89B55A6E1139F0E87A90987017;
// UnityEngine.GUISkin
struct GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7;
// UnityEngine.RectTransform
struct RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20;
// UnityEngine.UI.Image
struct Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E;
// UnityEngine.UI.Slider
struct Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09;
// UnityEngine.UI.Toggle
struct Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106;



IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23
struct U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C  : public RuntimeObject
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>1__state
	int32_t ___U3CU3E1__state_0;
	// System.Object RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>2__current
	RuntimeObject * ___U3CU3E2__current_1;
	// RenderHeads.Media.AVProVideo.Demos.SimpleController RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<>4__this
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC * ___U3CU3E4__this_2;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController/<LoadVideoWithFading>d__23::<fade>5__2
	float ___U3CfadeU3E5__2_3;

public:
	inline static int32_t get_offset_of_U3CU3E1__state_0() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C, ___U3CU3E1__state_0)); }
	inline int32_t get_U3CU3E1__state_0() const { return ___U3CU3E1__state_0; }
	inline int32_t* get_address_of_U3CU3E1__state_0() { return &___U3CU3E1__state_0; }
	inline void set_U3CU3E1__state_0(int32_t value)
	{
		___U3CU3E1__state_0 = value;
	}

	inline static int32_t get_offset_of_U3CU3E2__current_1() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C, ___U3CU3E2__current_1)); }
	inline RuntimeObject * get_U3CU3E2__current_1() const { return ___U3CU3E2__current_1; }
	inline RuntimeObject ** get_address_of_U3CU3E2__current_1() { return &___U3CU3E2__current_1; }
	inline void set_U3CU3E2__current_1(RuntimeObject * value)
	{
		___U3CU3E2__current_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E2__current_1), (void*)value);
	}

	inline static int32_t get_offset_of_U3CU3E4__this_2() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C, ___U3CU3E4__this_2)); }
	inline SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC * get_U3CU3E4__this_2() const { return ___U3CU3E4__this_2; }
	inline SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC ** get_address_of_U3CU3E4__this_2() { return &___U3CU3E4__this_2; }
	inline void set_U3CU3E4__this_2(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC * value)
	{
		___U3CU3E4__this_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CU3E4__this_2), (void*)value);
	}

	inline static int32_t get_offset_of_U3CfadeU3E5__2_3() { return static_cast<int32_t>(offsetof(U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C, ___U3CfadeU3E5__2_3)); }
	inline float get_U3CfadeU3E5__2_3() const { return ___U3CfadeU3E5__2_3; }
	inline float* get_address_of_U3CfadeU3E5__2_3() { return &___U3CfadeU3E5__2_3; }
	inline void set_U3CfadeU3E5__2_3(float value)
	{
		___U3CfadeU3E5__2_3 = value;
	}
};


// System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF_marshaled_com
{
};

// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144
struct __StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD__padding[144];
	};

public:
};


// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24
struct __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 
{
public:
	union
	{
		struct
		{
			union
			{
			};
		};
		uint8_t __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34__padding[24];
	};

public:
};


// System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521  : public ValueType_t4D0C27076F7C36E76190FB3328E232BCB1CD1FFF
{
public:

public:
};

struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t4CC6ABF0AD71BEC97E3C2F1E9C5677E46D3A75C2* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t2AF27C02B8653AE29442467390005ABC74D8F521_marshaled_com
{
};

// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// <PrivateImplementationDetails>
struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A  : public RuntimeObject
{
public:

public:
};

struct U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields
{
public:
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=144 <PrivateImplementationDetails>::63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850
	__StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD  ___63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0;
	// <PrivateImplementationDetails>/__StaticArrayInitTypeSize=24 <PrivateImplementationDetails>::6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A
	__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1;

public:
	inline static int32_t get_offset_of_U363A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0)); }
	inline __StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD  get_U363A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0() const { return ___63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0; }
	inline __StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD * get_address_of_U363A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0() { return &___63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0; }
	inline void set_U363A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0(__StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD  value)
	{
		___63A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0 = value;
	}

	inline static int32_t get_offset_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return static_cast<int32_t>(offsetof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields, ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1)); }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  get_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() const { return ___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline __StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 * get_address_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1() { return &___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1; }
	inline void set_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34  value)
	{
		___6D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State
struct State_t52CF4D67F18BE10A1891D562520BAFB60BD08B84 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(State_t52CF4D67F18BE10A1891D562520BAFB60BD08B84, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation
struct FileLocation_tE93F591DB489C835A552465434515E76B7A5D840 
{
public:
	// System.Int32 RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(FileLocation_tE93F591DB489C835A552465434515E76B7A5D840, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0  : public RuntimeObject
{
public:
	// System.IntPtr UnityEngine.Object::m_CachedPtr
	intptr_t ___m_CachedPtr_0;

public:
	inline static int32_t get_offset_of_m_CachedPtr_0() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0, ___m_CachedPtr_0)); }
	inline intptr_t get_m_CachedPtr_0() const { return ___m_CachedPtr_0; }
	inline intptr_t* get_address_of_m_CachedPtr_0() { return &___m_CachedPtr_0; }
	inline void set_m_CachedPtr_0(intptr_t value)
	{
		___m_CachedPtr_0 = value;
	}
};

struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields
{
public:
	// System.Int32 UnityEngine.Object::OffsetOfInstanceIDInCPlusPlusObject
	int32_t ___OffsetOfInstanceIDInCPlusPlusObject_1;

public:
	inline static int32_t get_offset_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return static_cast<int32_t>(offsetof(Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_StaticFields, ___OffsetOfInstanceIDInCPlusPlusObject_1)); }
	inline int32_t get_OffsetOfInstanceIDInCPlusPlusObject_1() const { return ___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline int32_t* get_address_of_OffsetOfInstanceIDInCPlusPlusObject_1() { return &___OffsetOfInstanceIDInCPlusPlusObject_1; }
	inline void set_OffsetOfInstanceIDInCPlusPlusObject_1(int32_t value)
	{
		___OffsetOfInstanceIDInCPlusPlusObject_1 = value;
	}
};

// Native definition for P/Invoke marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_pinvoke
{
	intptr_t ___m_CachedPtr_0;
};
// Native definition for COM marshalling of UnityEngine.Object
struct Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0_marshaled_com
{
	intptr_t ___m_CachedPtr_0;
};

// UnityEngine.Component
struct Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621  : public Object_tAE11E5E46CD5C37C9F3E8950C00CD8B45666A2D0
{
public:

public:
};


// UnityEngine.Behaviour
struct Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8  : public Component_t05064EF382ABCAF4B8C94F8A350EA85184C26621
{
public:

public:
};


// UnityEngine.MonoBehaviour
struct MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429  : public Behaviour_tBDC7E9C3C898AD8348891B82D3E345801D920CA8
{
public:

public:
};


// RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack
struct ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_trackIndex
	int32_t ____trackIndex_5;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.ChangeAudioTrack::_isQueued
	bool ____isQueued_6;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__trackIndex_5() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683, ____trackIndex_5)); }
	inline int32_t get__trackIndex_5() const { return ____trackIndex_5; }
	inline int32_t* get_address_of__trackIndex_5() { return &____trackIndex_5; }
	inline void set__trackIndex_5(int32_t value)
	{
		____trackIndex_5 = value;
	}

	inline static int32_t get_offset_of__isQueued_6() { return static_cast<int32_t>(offsetof(ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683, ____isQueued_6)); }
	inline bool get__isQueued_6() const { return ____isQueued_6; }
	inline bool* get_address_of__isQueued_6() { return &____isQueued_6; }
	inline void set__isQueued_6(bool value)
	{
		____isQueued_6 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode
struct ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;
	// RenderHeads.Media.AVProVideo.ApplyToMesh RenderHeads.Media.AVProVideo.Demos.ChangeStereoMode::_applyToMesh
	ApplyToMesh_t8B4E643A2D878D023489D731341AACA4B44FDF4C * ____applyToMesh_5;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__applyToMesh_5() { return static_cast<int32_t>(offsetof(ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C, ____applyToMesh_5)); }
	inline ApplyToMesh_t8B4E643A2D878D023489D731341AACA4B44FDF4C * get__applyToMesh_5() const { return ____applyToMesh_5; }
	inline ApplyToMesh_t8B4E643A2D878D023489D731341AACA4B44FDF4C ** get_address_of__applyToMesh_5() { return &____applyToMesh_5; }
	inline void set__applyToMesh_5(ApplyToMesh_t8B4E643A2D878D023489D731341AACA4B44FDF4C * value)
	{
		____applyToMesh_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____applyToMesh_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample
struct ChangeVideoExample_t0B6BC274F65F8DCC28BB8A62CB78AD4FB5BE10F4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.ChangeVideoExample::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(ChangeVideoExample_t0B6BC274F65F8DCC28BB8A62CB78AD4FB5BE10F4, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer
struct LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::_mp
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mp_4;
	// System.String RenderHeads.Media.AVProVideo.Demos.LoadFromBuffer::_filename
	String_t* ____filename_5;

public:
	inline static int32_t get_offset_of__mp_4() { return static_cast<int32_t>(offsetof(LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3, ____mp_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mp_4() const { return ____mp_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mp_4() { return &____mp_4; }
	inline void set__mp_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mp_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mp_4), (void*)value);
	}

	inline static int32_t get_offset_of__filename_5() { return static_cast<int32_t>(offsetof(LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3, ____filename_5)); }
	inline String_t* get__filename_5() const { return ____filename_5; }
	inline String_t** get_address_of__filename_5() { return &____filename_5; }
	inline void set__filename_5(String_t* value)
	{
		____filename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filename_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks
struct LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::_mp
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mp_4;
	// System.String RenderHeads.Media.AVProVideo.Demos.LoadFromBufferInChunks::_filename
	String_t* ____filename_5;

public:
	inline static int32_t get_offset_of__mp_4() { return static_cast<int32_t>(offsetof(LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4, ____mp_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mp_4() const { return ____mp_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mp_4() { return &____mp_4; }
	inline void set__mp_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mp_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mp_4), (void*)value);
	}

	inline static int32_t get_offset_of__filename_5() { return static_cast<int32_t>(offsetof(LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4, ____filename_5)); }
	inline String_t* get__filename_5() const { return ____filename_5; }
	inline String_t** get_address_of__filename_5() { return &____filename_5; }
	inline void set__filename_5(String_t* value)
	{
		____filename_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filename_5), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen
struct NativeMediaOpen_tD6DFEB5182498332604EA535015F82F670906587  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.NativeMediaOpen::_player
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____player_4;

public:
	inline static int32_t get_offset_of__player_4() { return static_cast<int32_t>(offsetof(NativeMediaOpen_tD6DFEB5182498332604EA535015F82F670906587, ____player_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__player_4() const { return ____player_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__player_4() { return &____player_4; }
	inline void set__player_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____player_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____player_4), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.PlaybackSync
struct PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_masterPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____masterPlayer_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer[] RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_slavePlayers
	MediaPlayerU5BU5D_tE227CBE13C940DD1422DE8F855FE717D80DD897F* ____slavePlayers_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_toleranceMs
	float ____toleranceMs_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_matchVideo
	bool ____matchVideo_7;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_muteSlaves
	bool ____muteSlaves_8;
	// RenderHeads.Media.AVProVideo.Demos.PlaybackSync/State RenderHeads.Media.AVProVideo.Demos.PlaybackSync::_state
	int32_t ____state_9;

public:
	inline static int32_t get_offset_of__masterPlayer_4() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____masterPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__masterPlayer_4() const { return ____masterPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__masterPlayer_4() { return &____masterPlayer_4; }
	inline void set__masterPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____masterPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____masterPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__slavePlayers_5() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____slavePlayers_5)); }
	inline MediaPlayerU5BU5D_tE227CBE13C940DD1422DE8F855FE717D80DD897F* get__slavePlayers_5() const { return ____slavePlayers_5; }
	inline MediaPlayerU5BU5D_tE227CBE13C940DD1422DE8F855FE717D80DD897F** get_address_of__slavePlayers_5() { return &____slavePlayers_5; }
	inline void set__slavePlayers_5(MediaPlayerU5BU5D_tE227CBE13C940DD1422DE8F855FE717D80DD897F* value)
	{
		____slavePlayers_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____slavePlayers_5), (void*)value);
	}

	inline static int32_t get_offset_of__toleranceMs_6() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____toleranceMs_6)); }
	inline float get__toleranceMs_6() const { return ____toleranceMs_6; }
	inline float* get_address_of__toleranceMs_6() { return &____toleranceMs_6; }
	inline void set__toleranceMs_6(float value)
	{
		____toleranceMs_6 = value;
	}

	inline static int32_t get_offset_of__matchVideo_7() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____matchVideo_7)); }
	inline bool get__matchVideo_7() const { return ____matchVideo_7; }
	inline bool* get_address_of__matchVideo_7() { return &____matchVideo_7; }
	inline void set__matchVideo_7(bool value)
	{
		____matchVideo_7 = value;
	}

	inline static int32_t get_offset_of__muteSlaves_8() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____muteSlaves_8)); }
	inline bool get__muteSlaves_8() const { return ____muteSlaves_8; }
	inline bool* get_address_of__muteSlaves_8() { return &____muteSlaves_8; }
	inline void set__muteSlaves_8(bool value)
	{
		____muteSlaves_8 = value;
	}

	inline static int32_t get_offset_of__state_9() { return static_cast<int32_t>(offsetof(PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06, ____state_9)); }
	inline int32_t get__state_9() const { return ____state_9; }
	inline int32_t* get_address_of__state_9() { return &____state_9; }
	inline void set__state_9(int32_t value)
	{
		____state_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple
struct SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_videoPath
	String_t* ___m_videoPath_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_videoLocation
	int32_t ___m_videoLocation_5;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_NumVideosAdded
	int32_t ___m_NumVideosAdded_6;
	// System.Collections.Generic.List`1<RenderHeads.Media.AVProVideo.DisplayUGUI> RenderHeads.Media.AVProVideo.Demos.SampleApp_Multiple::m_aAddedVideos
	List_1_t9CC631B72B4B3B22612C9412746684E6122BC1D2 * ___m_aAddedVideos_7;

public:
	inline static int32_t get_offset_of_m_videoPath_4() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128, ___m_videoPath_4)); }
	inline String_t* get_m_videoPath_4() const { return ___m_videoPath_4; }
	inline String_t** get_address_of_m_videoPath_4() { return &___m_videoPath_4; }
	inline void set_m_videoPath_4(String_t* value)
	{
		___m_videoPath_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_videoPath_4), (void*)value);
	}

	inline static int32_t get_offset_of_m_videoLocation_5() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128, ___m_videoLocation_5)); }
	inline int32_t get_m_videoLocation_5() const { return ___m_videoLocation_5; }
	inline int32_t* get_address_of_m_videoLocation_5() { return &___m_videoLocation_5; }
	inline void set_m_videoLocation_5(int32_t value)
	{
		___m_videoLocation_5 = value;
	}

	inline static int32_t get_offset_of_m_NumVideosAdded_6() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128, ___m_NumVideosAdded_6)); }
	inline int32_t get_m_NumVideosAdded_6() const { return ___m_NumVideosAdded_6; }
	inline int32_t* get_address_of_m_NumVideosAdded_6() { return &___m_NumVideosAdded_6; }
	inline void set_m_NumVideosAdded_6(int32_t value)
	{
		___m_NumVideosAdded_6 = value;
	}

	inline static int32_t get_offset_of_m_aAddedVideos_7() { return static_cast<int32_t>(offsetof(SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128, ___m_aAddedVideos_7)); }
	inline List_1_t9CC631B72B4B3B22612C9412746684E6122BC1D2 * get_m_aAddedVideos_7() const { return ___m_aAddedVideos_7; }
	inline List_1_t9CC631B72B4B3B22612C9412746684E6122BC1D2 ** get_address_of_m_aAddedVideos_7() { return &___m_aAddedVideos_7; }
	inline void set_m_aAddedVideos_7(List_1_t9CC631B72B4B3B22612C9412746684E6122BC1D2 * value)
	{
		___m_aAddedVideos_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_aAddedVideos_7), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.SimpleController
struct SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_folder
	String_t* ____folder_4;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_filenames
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____filenames_5;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.SimpleController::_streams
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____streams_6;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.SimpleController::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_7;
	// RenderHeads.Media.AVProVideo.DisplayIMGUI RenderHeads.Media.AVProVideo.Demos.SimpleController::_display
	DisplayIMGUI_tB907379C5DB60B5477D761E5D06DE8C60C7DEF2E * ____display_8;
	// UnityEngine.GUISkin RenderHeads.Media.AVProVideo.Demos.SimpleController::_guiSkin
	GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * ____guiSkin_9;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_width
	int32_t ____width_10;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.SimpleController::_height
	int32_t ____height_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_durationSeconds
	float ____durationSeconds_12;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SimpleController::_useFading
	bool ____useFading_13;
	// System.Collections.Generic.Queue`1<System.String> RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventLog
	Queue_1_tFA2F320547F861316BAE4BB2C5C0EE1EEB8F508E * ____eventLog_14;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SimpleController::_eventTimer
	float ____eventTimer_15;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoLocation
	int32_t ____nextVideoLocation_16;
	// System.String RenderHeads.Media.AVProVideo.Demos.SimpleController::_nextVideoPath
	String_t* ____nextVideoPath_17;

public:
	inline static int32_t get_offset_of__folder_4() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____folder_4)); }
	inline String_t* get__folder_4() const { return ____folder_4; }
	inline String_t** get_address_of__folder_4() { return &____folder_4; }
	inline void set__folder_4(String_t* value)
	{
		____folder_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_4), (void*)value);
	}

	inline static int32_t get_offset_of__filenames_5() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____filenames_5)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__filenames_5() const { return ____filenames_5; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__filenames_5() { return &____filenames_5; }
	inline void set__filenames_5(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____filenames_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____filenames_5), (void*)value);
	}

	inline static int32_t get_offset_of__streams_6() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____streams_6)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__streams_6() const { return ____streams_6; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__streams_6() { return &____streams_6; }
	inline void set__streams_6(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____streams_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____streams_6), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayer_7() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____mediaPlayer_7)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_7() const { return ____mediaPlayer_7; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_7() { return &____mediaPlayer_7; }
	inline void set__mediaPlayer_7(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_7), (void*)value);
	}

	inline static int32_t get_offset_of__display_8() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____display_8)); }
	inline DisplayIMGUI_tB907379C5DB60B5477D761E5D06DE8C60C7DEF2E * get__display_8() const { return ____display_8; }
	inline DisplayIMGUI_tB907379C5DB60B5477D761E5D06DE8C60C7DEF2E ** get_address_of__display_8() { return &____display_8; }
	inline void set__display_8(DisplayIMGUI_tB907379C5DB60B5477D761E5D06DE8C60C7DEF2E * value)
	{
		____display_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____display_8), (void*)value);
	}

	inline static int32_t get_offset_of__guiSkin_9() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____guiSkin_9)); }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * get__guiSkin_9() const { return ____guiSkin_9; }
	inline GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 ** get_address_of__guiSkin_9() { return &____guiSkin_9; }
	inline void set__guiSkin_9(GUISkin_tE22941292F37A41BE0EDF70FC3A9CD9EB02ADDB7 * value)
	{
		____guiSkin_9 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____guiSkin_9), (void*)value);
	}

	inline static int32_t get_offset_of__width_10() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____width_10)); }
	inline int32_t get__width_10() const { return ____width_10; }
	inline int32_t* get_address_of__width_10() { return &____width_10; }
	inline void set__width_10(int32_t value)
	{
		____width_10 = value;
	}

	inline static int32_t get_offset_of__height_11() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____height_11)); }
	inline int32_t get__height_11() const { return ____height_11; }
	inline int32_t* get_address_of__height_11() { return &____height_11; }
	inline void set__height_11(int32_t value)
	{
		____height_11 = value;
	}

	inline static int32_t get_offset_of__durationSeconds_12() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____durationSeconds_12)); }
	inline float get__durationSeconds_12() const { return ____durationSeconds_12; }
	inline float* get_address_of__durationSeconds_12() { return &____durationSeconds_12; }
	inline void set__durationSeconds_12(float value)
	{
		____durationSeconds_12 = value;
	}

	inline static int32_t get_offset_of__useFading_13() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____useFading_13)); }
	inline bool get__useFading_13() const { return ____useFading_13; }
	inline bool* get_address_of__useFading_13() { return &____useFading_13; }
	inline void set__useFading_13(bool value)
	{
		____useFading_13 = value;
	}

	inline static int32_t get_offset_of__eventLog_14() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____eventLog_14)); }
	inline Queue_1_tFA2F320547F861316BAE4BB2C5C0EE1EEB8F508E * get__eventLog_14() const { return ____eventLog_14; }
	inline Queue_1_tFA2F320547F861316BAE4BB2C5C0EE1EEB8F508E ** get_address_of__eventLog_14() { return &____eventLog_14; }
	inline void set__eventLog_14(Queue_1_tFA2F320547F861316BAE4BB2C5C0EE1EEB8F508E * value)
	{
		____eventLog_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____eventLog_14), (void*)value);
	}

	inline static int32_t get_offset_of__eventTimer_15() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____eventTimer_15)); }
	inline float get__eventTimer_15() const { return ____eventTimer_15; }
	inline float* get_address_of__eventTimer_15() { return &____eventTimer_15; }
	inline void set__eventTimer_15(float value)
	{
		____eventTimer_15 = value;
	}

	inline static int32_t get_offset_of__nextVideoLocation_16() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____nextVideoLocation_16)); }
	inline int32_t get__nextVideoLocation_16() const { return ____nextVideoLocation_16; }
	inline int32_t* get_address_of__nextVideoLocation_16() { return &____nextVideoLocation_16; }
	inline void set__nextVideoLocation_16(int32_t value)
	{
		____nextVideoLocation_16 = value;
	}

	inline static int32_t get_offset_of__nextVideoPath_17() { return static_cast<int32_t>(offsetof(SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC, ____nextVideoPath_17)); }
	inline String_t* get__nextVideoPath_17() const { return ____nextVideoPath_17; }
	inline String_t** get_address_of__nextVideoPath_17() { return &____nextVideoPath_17; }
	inline void set__nextVideoPath_17(String_t* value)
	{
		____nextVideoPath_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____nextVideoPath_17), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.SphereDemo
struct SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_zeroCameraPosition
	bool ____zeroCameraPosition_4;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_allowRecenter
	bool ____allowRecenter_5;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_allowVrToggle
	bool ____allowVrToggle_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.SphereDemo::_lockPitch
	bool ____lockPitch_7;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SphereDemo::_spinX
	float ____spinX_8;
	// System.Single RenderHeads.Media.AVProVideo.Demos.SphereDemo::_spinY
	float ____spinY_9;

public:
	inline static int32_t get_offset_of__zeroCameraPosition_4() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____zeroCameraPosition_4)); }
	inline bool get__zeroCameraPosition_4() const { return ____zeroCameraPosition_4; }
	inline bool* get_address_of__zeroCameraPosition_4() { return &____zeroCameraPosition_4; }
	inline void set__zeroCameraPosition_4(bool value)
	{
		____zeroCameraPosition_4 = value;
	}

	inline static int32_t get_offset_of__allowRecenter_5() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____allowRecenter_5)); }
	inline bool get__allowRecenter_5() const { return ____allowRecenter_5; }
	inline bool* get_address_of__allowRecenter_5() { return &____allowRecenter_5; }
	inline void set__allowRecenter_5(bool value)
	{
		____allowRecenter_5 = value;
	}

	inline static int32_t get_offset_of__allowVrToggle_6() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____allowVrToggle_6)); }
	inline bool get__allowVrToggle_6() const { return ____allowVrToggle_6; }
	inline bool* get_address_of__allowVrToggle_6() { return &____allowVrToggle_6; }
	inline void set__allowVrToggle_6(bool value)
	{
		____allowVrToggle_6 = value;
	}

	inline static int32_t get_offset_of__lockPitch_7() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____lockPitch_7)); }
	inline bool get__lockPitch_7() const { return ____lockPitch_7; }
	inline bool* get_address_of__lockPitch_7() { return &____lockPitch_7; }
	inline void set__lockPitch_7(bool value)
	{
		____lockPitch_7 = value;
	}

	inline static int32_t get_offset_of__spinX_8() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____spinX_8)); }
	inline float get__spinX_8() const { return ____spinX_8; }
	inline float* get_address_of__spinX_8() { return &____spinX_8; }
	inline void set__spinX_8(float value)
	{
		____spinX_8 = value;
	}

	inline static int32_t get_offset_of__spinY_9() { return static_cast<int32_t>(offsetof(SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165, ____spinY_9)); }
	inline float get__spinY_9() const { return ____spinY_9; }
	inline float* get_address_of__spinY_9() { return &____spinY_9; }
	inline void set__spinY_9(float value)
	{
		____spinY_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.StartEndPoint
struct StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_startPointSeconds
	float ____startPointSeconds_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_endPointSeconds
	float ____endPointSeconds_6;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_loop
	bool ____loop_7;
	// System.Single RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_startLoopSeconds
	float ____startLoopSeconds_8;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.StartEndPoint::_isStartQueued
	bool ____isStartQueued_9;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__startPointSeconds_5() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____startPointSeconds_5)); }
	inline float get__startPointSeconds_5() const { return ____startPointSeconds_5; }
	inline float* get_address_of__startPointSeconds_5() { return &____startPointSeconds_5; }
	inline void set__startPointSeconds_5(float value)
	{
		____startPointSeconds_5 = value;
	}

	inline static int32_t get_offset_of__endPointSeconds_6() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____endPointSeconds_6)); }
	inline float get__endPointSeconds_6() const { return ____endPointSeconds_6; }
	inline float* get_address_of__endPointSeconds_6() { return &____endPointSeconds_6; }
	inline void set__endPointSeconds_6(float value)
	{
		____endPointSeconds_6 = value;
	}

	inline static int32_t get_offset_of__loop_7() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____loop_7)); }
	inline bool get__loop_7() const { return ____loop_7; }
	inline bool* get_address_of__loop_7() { return &____loop_7; }
	inline void set__loop_7(bool value)
	{
		____loop_7 = value;
	}

	inline static int32_t get_offset_of__startLoopSeconds_8() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____startLoopSeconds_8)); }
	inline float get__startLoopSeconds_8() const { return ____startLoopSeconds_8; }
	inline float* get_address_of__startLoopSeconds_8() { return &____startLoopSeconds_8; }
	inline void set__startLoopSeconds_8(float value)
	{
		____startLoopSeconds_8 = value;
	}

	inline static int32_t get_offset_of__isStartQueued_9() { return static_cast<int32_t>(offsetof(StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165, ____isStartQueued_9)); }
	inline bool get__isStartQueued_9() const { return ____isStartQueued_9; }
	inline bool* get_address_of__isStartQueued_9() { return &____isStartQueued_9; }
	inline void set__isStartQueued_9(bool value)
	{
		____isStartQueued_9 = value;
	}
};


// RenderHeads.Media.AVProVideo.Demos.VCR
struct VCR_t0A2794732820068574A24388038F43DE7338C31B  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_mediaPlayerB
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayerB_5;
	// RenderHeads.Media.AVProVideo.DisplayUGUI RenderHeads.Media.AVProVideo.Demos.VCR::_mediaDisplay
	DisplayUGUI_tA0598D2E271F82099494F777A0E6CB025AE80743 * ____mediaDisplay_6;
	// UnityEngine.RectTransform RenderHeads.Media.AVProVideo.Demos.VCR::_bufferedSliderRect
	RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * ____bufferedSliderRect_7;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.VCR::_videoSeekSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____videoSeekSlider_8;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VCR::_setVideoSeekSliderValue
	float ____setVideoSeekSliderValue_9;
	// System.Boolean RenderHeads.Media.AVProVideo.Demos.VCR::_wasPlayingOnScrub
	bool ____wasPlayingOnScrub_10;
	// UnityEngine.UI.Slider RenderHeads.Media.AVProVideo.Demos.VCR::_audioVolumeSlider
	Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * ____audioVolumeSlider_11;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VCR::_setAudioVolumeSliderValue
	float ____setAudioVolumeSliderValue_12;
	// UnityEngine.UI.Toggle RenderHeads.Media.AVProVideo.Demos.VCR::_AutoStartToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____AutoStartToggle_13;
	// UnityEngine.UI.Toggle RenderHeads.Media.AVProVideo.Demos.VCR::_MuteToggle
	Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * ____MuteToggle_14;
	// RenderHeads.Media.AVProVideo.MediaPlayer/FileLocation RenderHeads.Media.AVProVideo.Demos.VCR::_location
	int32_t ____location_15;
	// System.String RenderHeads.Media.AVProVideo.Demos.VCR::_folder
	String_t* ____folder_16;
	// System.String[] RenderHeads.Media.AVProVideo.Demos.VCR::_videoFiles
	StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* ____videoFiles_17;
	// System.Int32 RenderHeads.Media.AVProVideo.Demos.VCR::_VideoIndex
	int32_t ____VideoIndex_18;
	// UnityEngine.UI.Image RenderHeads.Media.AVProVideo.Demos.VCR::_bufferedSliderImage
	Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * ____bufferedSliderImage_19;
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VCR::_loadingPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____loadingPlayer_20;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__mediaPlayerB_5() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____mediaPlayerB_5)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayerB_5() const { return ____mediaPlayerB_5; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayerB_5() { return &____mediaPlayerB_5; }
	inline void set__mediaPlayerB_5(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayerB_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayerB_5), (void*)value);
	}

	inline static int32_t get_offset_of__mediaDisplay_6() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____mediaDisplay_6)); }
	inline DisplayUGUI_tA0598D2E271F82099494F777A0E6CB025AE80743 * get__mediaDisplay_6() const { return ____mediaDisplay_6; }
	inline DisplayUGUI_tA0598D2E271F82099494F777A0E6CB025AE80743 ** get_address_of__mediaDisplay_6() { return &____mediaDisplay_6; }
	inline void set__mediaDisplay_6(DisplayUGUI_tA0598D2E271F82099494F777A0E6CB025AE80743 * value)
	{
		____mediaDisplay_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaDisplay_6), (void*)value);
	}

	inline static int32_t get_offset_of__bufferedSliderRect_7() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____bufferedSliderRect_7)); }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * get__bufferedSliderRect_7() const { return ____bufferedSliderRect_7; }
	inline RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 ** get_address_of__bufferedSliderRect_7() { return &____bufferedSliderRect_7; }
	inline void set__bufferedSliderRect_7(RectTransform_t285CBD8775B25174B75164F10618F8B9728E1B20 * value)
	{
		____bufferedSliderRect_7 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderRect_7), (void*)value);
	}

	inline static int32_t get_offset_of__videoSeekSlider_8() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____videoSeekSlider_8)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__videoSeekSlider_8() const { return ____videoSeekSlider_8; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__videoSeekSlider_8() { return &____videoSeekSlider_8; }
	inline void set__videoSeekSlider_8(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____videoSeekSlider_8 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoSeekSlider_8), (void*)value);
	}

	inline static int32_t get_offset_of__setVideoSeekSliderValue_9() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____setVideoSeekSliderValue_9)); }
	inline float get__setVideoSeekSliderValue_9() const { return ____setVideoSeekSliderValue_9; }
	inline float* get_address_of__setVideoSeekSliderValue_9() { return &____setVideoSeekSliderValue_9; }
	inline void set__setVideoSeekSliderValue_9(float value)
	{
		____setVideoSeekSliderValue_9 = value;
	}

	inline static int32_t get_offset_of__wasPlayingOnScrub_10() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____wasPlayingOnScrub_10)); }
	inline bool get__wasPlayingOnScrub_10() const { return ____wasPlayingOnScrub_10; }
	inline bool* get_address_of__wasPlayingOnScrub_10() { return &____wasPlayingOnScrub_10; }
	inline void set__wasPlayingOnScrub_10(bool value)
	{
		____wasPlayingOnScrub_10 = value;
	}

	inline static int32_t get_offset_of__audioVolumeSlider_11() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____audioVolumeSlider_11)); }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * get__audioVolumeSlider_11() const { return ____audioVolumeSlider_11; }
	inline Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 ** get_address_of__audioVolumeSlider_11() { return &____audioVolumeSlider_11; }
	inline void set__audioVolumeSlider_11(Slider_t0654A41304B5CE7074CA86F4E66CB681D0D52C09 * value)
	{
		____audioVolumeSlider_11 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____audioVolumeSlider_11), (void*)value);
	}

	inline static int32_t get_offset_of__setAudioVolumeSliderValue_12() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____setAudioVolumeSliderValue_12)); }
	inline float get__setAudioVolumeSliderValue_12() const { return ____setAudioVolumeSliderValue_12; }
	inline float* get_address_of__setAudioVolumeSliderValue_12() { return &____setAudioVolumeSliderValue_12; }
	inline void set__setAudioVolumeSliderValue_12(float value)
	{
		____setAudioVolumeSliderValue_12 = value;
	}

	inline static int32_t get_offset_of__AutoStartToggle_13() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____AutoStartToggle_13)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__AutoStartToggle_13() const { return ____AutoStartToggle_13; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__AutoStartToggle_13() { return &____AutoStartToggle_13; }
	inline void set__AutoStartToggle_13(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____AutoStartToggle_13 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____AutoStartToggle_13), (void*)value);
	}

	inline static int32_t get_offset_of__MuteToggle_14() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____MuteToggle_14)); }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * get__MuteToggle_14() const { return ____MuteToggle_14; }
	inline Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 ** get_address_of__MuteToggle_14() { return &____MuteToggle_14; }
	inline void set__MuteToggle_14(Toggle_t9ADD572046F831945ED0E48A01B50FEA1CA52106 * value)
	{
		____MuteToggle_14 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____MuteToggle_14), (void*)value);
	}

	inline static int32_t get_offset_of__location_15() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____location_15)); }
	inline int32_t get__location_15() const { return ____location_15; }
	inline int32_t* get_address_of__location_15() { return &____location_15; }
	inline void set__location_15(int32_t value)
	{
		____location_15 = value;
	}

	inline static int32_t get_offset_of__folder_16() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____folder_16)); }
	inline String_t* get__folder_16() const { return ____folder_16; }
	inline String_t** get_address_of__folder_16() { return &____folder_16; }
	inline void set__folder_16(String_t* value)
	{
		____folder_16 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____folder_16), (void*)value);
	}

	inline static int32_t get_offset_of__videoFiles_17() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____videoFiles_17)); }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* get__videoFiles_17() const { return ____videoFiles_17; }
	inline StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E** get_address_of__videoFiles_17() { return &____videoFiles_17; }
	inline void set__videoFiles_17(StringU5BU5D_t933FB07893230EA91C40FF900D5400665E87B14E* value)
	{
		____videoFiles_17 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____videoFiles_17), (void*)value);
	}

	inline static int32_t get_offset_of__VideoIndex_18() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____VideoIndex_18)); }
	inline int32_t get__VideoIndex_18() const { return ____VideoIndex_18; }
	inline int32_t* get_address_of__VideoIndex_18() { return &____VideoIndex_18; }
	inline void set__VideoIndex_18(int32_t value)
	{
		____VideoIndex_18 = value;
	}

	inline static int32_t get_offset_of__bufferedSliderImage_19() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____bufferedSliderImage_19)); }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * get__bufferedSliderImage_19() const { return ____bufferedSliderImage_19; }
	inline Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E ** get_address_of__bufferedSliderImage_19() { return &____bufferedSliderImage_19; }
	inline void set__bufferedSliderImage_19(Image_t18FED07D8646917E1C563745518CF3DD57FF0B3E * value)
	{
		____bufferedSliderImage_19 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____bufferedSliderImage_19), (void*)value);
	}

	inline static int32_t get_offset_of__loadingPlayer_20() { return static_cast<int32_t>(offsetof(VCR_t0A2794732820068574A24388038F43DE7338C31B, ____loadingPlayer_20)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__loadingPlayer_20() const { return ____loadingPlayer_20; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__loadingPlayer_20() { return &____loadingPlayer_20; }
	inline void set__loadingPlayer_20(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____loadingPlayer_20 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____loadingPlayer_20), (void*)value);
	}
};


// RenderHeads.Media.AVProVideo.Demos.VideoTrigger
struct VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D  : public MonoBehaviour_t4A60845CF505405AF8BE8C61CC07F75CADEF6429
{
public:
	// RenderHeads.Media.AVProVideo.MediaPlayer RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_mediaPlayer
	MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * ____mediaPlayer_4;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeTimeMs
	float ____fadeTimeMs_5;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fade
	float ____fade_6;
	// System.Single RenderHeads.Media.AVProVideo.Demos.VideoTrigger::_fadeDirection
	float ____fadeDirection_7;

public:
	inline static int32_t get_offset_of__mediaPlayer_4() { return static_cast<int32_t>(offsetof(VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D, ____mediaPlayer_4)); }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * get__mediaPlayer_4() const { return ____mediaPlayer_4; }
	inline MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC ** get_address_of__mediaPlayer_4() { return &____mediaPlayer_4; }
	inline void set__mediaPlayer_4(MediaPlayer_tF06EA584C30655D03AB578128E6A1FC321E68BBC * value)
	{
		____mediaPlayer_4 = value;
		Il2CppCodeGenWriteBarrier((void**)(&____mediaPlayer_4), (void*)value);
	}

	inline static int32_t get_offset_of__fadeTimeMs_5() { return static_cast<int32_t>(offsetof(VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D, ____fadeTimeMs_5)); }
	inline float get__fadeTimeMs_5() const { return ____fadeTimeMs_5; }
	inline float* get_address_of__fadeTimeMs_5() { return &____fadeTimeMs_5; }
	inline void set__fadeTimeMs_5(float value)
	{
		____fadeTimeMs_5 = value;
	}

	inline static int32_t get_offset_of__fade_6() { return static_cast<int32_t>(offsetof(VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D, ____fade_6)); }
	inline float get__fade_6() const { return ____fade_6; }
	inline float* get_address_of__fade_6() { return &____fade_6; }
	inline void set__fade_6(float value)
	{
		____fade_6 = value;
	}

	inline static int32_t get_offset_of__fadeDirection_7() { return static_cast<int32_t>(offsetof(VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D, ____fadeDirection_7)); }
	inline float get__fadeDirection_7() const { return ____fadeDirection_7; }
	inline float* get_address_of__fadeDirection_7() { return &____fadeDirection_7; }
	inline void set__fadeDirection_7(float value)
	{
		____fadeDirection_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif




#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7022;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7022 = { sizeof (SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7022[4] = 
{
	SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128::get_offset_of_m_videoPath_4(),
	SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128::get_offset_of_m_videoLocation_5(),
	SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128::get_offset_of_m_NumVideosAdded_6(),
	SampleApp_Multiple_tFD9D4D7FB64CC5ECDAEBA59E3871663F0F015128::get_offset_of_m_aAddedVideos_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7023;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7023 = { sizeof (ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7023[3] = 
{
	ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683::get_offset_of__mediaPlayer_4(),
	ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683::get_offset_of__trackIndex_5(),
	ChangeAudioTrack_t58251F47805F6D1060FF774E05086B12E9B29683::get_offset_of__isQueued_6(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7024;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7024 = { sizeof (ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7024[2] = 
{
	ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C::get_offset_of__mediaPlayer_4(),
	ChangeStereoMode_t206CB65786A6466E355041318EFE7DD63F847F6C::get_offset_of__applyToMesh_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7025;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7025 = { sizeof (ChangeVideoExample_t0B6BC274F65F8DCC28BB8A62CB78AD4FB5BE10F4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7025[1] = 
{
	ChangeVideoExample_t0B6BC274F65F8DCC28BB8A62CB78AD4FB5BE10F4::get_offset_of__mediaPlayer_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7026;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7026 = { sizeof (LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7026[2] = 
{
	LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3::get_offset_of__mp_4(),
	LoadFromBuffer_t836D4F01B9626D42AE59EEA8796CBFE50B98BAF3::get_offset_of__filename_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7027;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7027 = { sizeof (LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7027[2] = 
{
	LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4::get_offset_of__mp_4(),
	LoadFromBufferInChunks_t9FD65EED49DC0E0C6055572B7429F923102AA6A4::get_offset_of__filename_5(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7028;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7028 = { sizeof (NativeMediaOpen_tD6DFEB5182498332604EA535015F82F670906587), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7028[1] = 
{
	NativeMediaOpen_tD6DFEB5182498332604EA535015F82F670906587::get_offset_of__player_4(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7029;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7029 = { sizeof (PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7029[6] = 
{
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__masterPlayer_4(),
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__slavePlayers_5(),
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__toleranceMs_6(),
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__matchVideo_7(),
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__muteSlaves_8(),
	PlaybackSync_t18F4F769B8DDB8B6570BD87D11119A764BD88F06::get_offset_of__state_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7030;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7030 = { sizeof (State_t52CF4D67F18BE10A1891D562520BAFB60BD08B84)+ sizeof (RuntimeObject), sizeof(int32_t), 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7030[4] = 
{
	State_t52CF4D67F18BE10A1891D562520BAFB60BD08B84::get_offset_of_value___2() + static_cast<int32_t>(sizeof(RuntimeObject)),
	0,
	0,
	0,
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7031;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7031 = { sizeof (StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7031[6] = 
{
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__mediaPlayer_4(),
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__startPointSeconds_5(),
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__endPointSeconds_6(),
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__loop_7(),
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__startLoopSeconds_8(),
	StartEndPoint_t51F80D90F2EF416456709541DCAE113CCA98B165::get_offset_of__isStartQueued_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7032;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7032 = { sizeof (VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7032[4] = 
{
	VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D::get_offset_of__mediaPlayer_4(),
	VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D::get_offset_of__fadeTimeMs_5(),
	VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D::get_offset_of__fade_6(),
	VideoTrigger_t220D2CC56BA6BAAED742941480BCFDA030335E2D::get_offset_of__fadeDirection_7(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7033;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7033 = { sizeof (SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7033[14] = 
{
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__folder_4(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__filenames_5(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__streams_6(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__mediaPlayer_7(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__display_8(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__guiSkin_9(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__width_10(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__height_11(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__durationSeconds_12(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__useFading_13(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__eventLog_14(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__eventTimer_15(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__nextVideoLocation_16(),
	SimpleController_t7E423F00736467420B0BCB7A21365DD63CB3B4BC::get_offset_of__nextVideoPath_17(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7034;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7034 = { sizeof (U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7034[4] = 
{
	U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C::get_offset_of_U3CU3E1__state_0(),
	U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C::get_offset_of_U3CU3E2__current_1(),
	U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C::get_offset_of_U3CU3E4__this_2(),
	U3CLoadVideoWithFadingU3Ed__23_t51854F8F59B94EC04777C4CCBD9D4990FF2C4F3C::get_offset_of_U3CfadeU3E5__2_3(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7035;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7035 = { sizeof (SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7035[6] = 
{
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__zeroCameraPosition_4(),
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__allowRecenter_5(),
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__allowVrToggle_6(),
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__lockPitch_7(),
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__spinX_8(),
	SphereDemo_t110805D90740CE1FDF36063411E87CB400EDC165::get_offset_of__spinY_9(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7036;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7036 = { sizeof (VCR_t0A2794732820068574A24388038F43DE7338C31B), -1, 0, 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7036[17] = 
{
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__mediaPlayer_4(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__mediaPlayerB_5(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__mediaDisplay_6(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__bufferedSliderRect_7(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__videoSeekSlider_8(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__setVideoSeekSliderValue_9(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__wasPlayingOnScrub_10(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__audioVolumeSlider_11(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__setAudioVolumeSliderValue_12(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__AutoStartToggle_13(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__MuteToggle_14(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__location_15(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__folder_16(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__videoFiles_17(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__VideoIndex_18(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__bufferedSliderImage_19(),
	VCR_t0A2794732820068574A24388038F43DE7338C31B::get_offset_of__loadingPlayer_20(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7037;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7037 = { sizeof (U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A), -1, sizeof(U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields), 0 };
IL2CPP_EXTERN_C const int32_t g_FieldOffsetTable7037[2] = 
{
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U363A0C38EA02007CD2EBB9EE8B45CAF1F0446F850_0(),
	U3CPrivateImplementationDetailsU3E_t4846F3E0DC61BBD6AE249FF1EAAC4CB64097DE4A_StaticFields::get_offset_of_U36D78A7DEB7B1A2A73F2CDFA8EFC4FE6DDCC4E47A_1(),
};
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7038;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7038 = { sizeof (__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D24_t0186834C621050282F5EF637E836A6E1DC934A34 ), 0, 0 };
extern const Il2CppTypeDefinitionSizes g_typeDefinitionSize7039;
const Il2CppTypeDefinitionSizes g_typeDefinitionSize7039 = { sizeof (__StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD)+ sizeof (RuntimeObject), sizeof(__StaticArrayInitTypeSizeU3D144_t2F13D6DD2A348E64CBC2607693FEB407B2A543FD ), 0, 0 };
#ifdef __clang__
#pragma clang diagnostic pop
#endif
