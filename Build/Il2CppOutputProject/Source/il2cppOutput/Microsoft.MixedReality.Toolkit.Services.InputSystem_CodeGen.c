﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 Microsoft.MixedReality.Toolkit.Input.TouchableEventType Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::get_EventsToReceive()
extern void BaseNearInteractionTouchable_get_EventsToReceive_mB5B553A9DE36821014C899F43A95F0E4028EB680 (void);
// 0x00000002 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::set_EventsToReceive(Microsoft.MixedReality.Toolkit.Input.TouchableEventType)
extern void BaseNearInteractionTouchable_set_EventsToReceive_mE4D1B04CB1553F017E983603675CE032E56FEC67 (void);
// 0x00000003 System.Single Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::get_DebounceThreshold()
extern void BaseNearInteractionTouchable_get_DebounceThreshold_mE2D05599BB1C0F3F8223815D0F17DBD4EC802AB8 (void);
// 0x00000004 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::set_DebounceThreshold(System.Single)
extern void BaseNearInteractionTouchable_set_DebounceThreshold_mA98D8F0596799E0FBBCA6F69E45BF411EF45B448 (void);
// 0x00000005 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::OnValidate()
extern void BaseNearInteractionTouchable_OnValidate_m25F579431912764BCC39D904970A0A5C48E8ECEA (void);
// 0x00000006 System.Single Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
// 0x00000007 System.Void Microsoft.MixedReality.Toolkit.Input.BaseNearInteractionTouchable::.ctor()
extern void BaseNearInteractionTouchable__ctor_mD8C43CE15662BF13262557BF5B0DC7544163CAF3 (void);
// 0x00000008 System.Boolean Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::get_ColliderEnabled()
extern void ColliderNearInteractionTouchable_get_ColliderEnabled_mDFE621376A64A1352963B70EDA4C73BCC2D126D6 (void);
// 0x00000009 UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::get_TouchableCollider()
extern void ColliderNearInteractionTouchable_get_TouchableCollider_mE24DBF9F2050F8AB28021B14DC276E0BD8A7D0B0 (void);
// 0x0000000A System.Void Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::OnValidate()
extern void ColliderNearInteractionTouchable_OnValidate_m5DF1C94479D2B6D1AAEF1BE9AFD8554E26C58755 (void);
// 0x0000000B System.Void Microsoft.MixedReality.Toolkit.Input.ColliderNearInteractionTouchable::.ctor()
extern void ColliderNearInteractionTouchable__ctor_m4C3DF8380729A8D55F43C66E90743A8331B5F294 (void);
// 0x0000000C System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void DefaultRaycastProvider__ctor_mC1E50211F7AF6A6A974822B6EB186E3152C082BD (void);
// 0x0000000D System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void DefaultRaycastProvider__ctor_m553DA09CDC38B337B1742DC56786AD6A4E3B91AF (void);
// 0x0000000E System.String Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::get_Name()
extern void DefaultRaycastProvider_get_Name_m25B985271868249C0E2A576D9D747BEFE4502ECA (void);
// 0x0000000F System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::set_Name(System.String)
extern void DefaultRaycastProvider_set_Name_m60156C8D6337989DB5057226D6B9DED4DE02323D (void);
// 0x00000010 System.Boolean Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::Raycast(Microsoft.MixedReality.Toolkit.Physics.RayStep,UnityEngine.LayerMask[],System.Boolean,Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit&)
extern void DefaultRaycastProvider_Raycast_m74A8C5EC6A95621398833CC19DEF22F3AE35DB1C (void);
// 0x00000011 System.Boolean Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::SphereCast(Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Single,UnityEngine.LayerMask[],System.Boolean,Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit&)
extern void DefaultRaycastProvider_SphereCast_m1B1F964942780B34EE6A84271CCAD84149401DA4 (void);
// 0x00000012 UnityEngine.EventSystems.RaycastResult Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::GraphicsRaycast(UnityEngine.EventSystems.EventSystem,UnityEngine.EventSystems.PointerEventData,UnityEngine.LayerMask[])
extern void DefaultRaycastProvider_GraphicsRaycast_m701AC2BB5BDC5D49A15BB91786C2F59410B84E0D (void);
// 0x00000013 System.Void Microsoft.MixedReality.Toolkit.Input.DefaultRaycastProvider::.cctor()
extern void DefaultRaycastProvider__cctor_m2AD8BDD488A3C7D39BFA15FB883351E35ED374FD (void);
// 0x00000014 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void FocusProvider__ctor_mC3CA5F53AFD742D574F83834C2EE477AF37EC1DC (void);
// 0x00000015 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void FocusProvider__ctor_mF8376A75A805AD30EBE2CAC7AB344F9D378A37EE (void);
// 0x00000016 System.Collections.Generic.IReadOnlyDictionary`2<System.UInt32,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerMediator> Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_PointerMediators()
extern void FocusProvider_get_PointerMediators_m58C5197F16241C03EEE928D0903BAAF4531E3D96 (void);
// 0x00000017 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_NumNearPointersActive()
extern void FocusProvider_get_NumNearPointersActive_m5D4AF993F127A31AB6AF2A5AF5DFDB1B964A3959 (void);
// 0x00000018 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_NumNearPointersActive(System.Int32)
extern void FocusProvider_set_NumNearPointersActive_mF09BE13FA43F0C12C04664E80ABBB111EED40001 (void);
// 0x00000019 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_NumFarPointersActive()
extern void FocusProvider_get_NumFarPointersActive_m510329AF6025C9BD7FEDA408C37A1380AFD81580 (void);
// 0x0000001A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_NumFarPointersActive(System.Int32)
extern void FocusProvider_set_NumFarPointersActive_m0B4DBDBB611083D956D945543B463B69CFEC2E48 (void);
// 0x0000001B Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_PrimaryPointer()
extern void FocusProvider_get_PrimaryPointer_mCDD5E7137364EA5A7F87717B90B2DCB582EDCD74 (void);
// 0x0000001C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_PrimaryPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_set_PrimaryPointer_m024C0990196E8F300CCF6AC37B9783B7D1FB6F69 (void);
// 0x0000001D System.String Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_Name()
extern void FocusProvider_get_Name_m1F7BD4449315E2B13BB43C653319F525196F7FED (void);
// 0x0000001E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_Name(System.String)
extern void FocusProvider_set_Name_mC61D39511CFBC94599DDFD520D8F31DFE3888149 (void);
// 0x0000001F System.UInt32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_Priority()
extern void FocusProvider_get_Priority_mF6A3658270693D12C1F7DF37A8481FF7D1E3B428 (void);
// 0x00000020 System.Single Microsoft.MixedReality.Toolkit.Input.FocusProvider::Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusProvider.get_GlobalPointingExtent()
extern void FocusProvider_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusProvider_get_GlobalPointingExtent_m6E24526E9128B2D870BD72BCAFEC719A475ED45E (void);
// 0x00000021 UnityEngine.LayerMask[] Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_FocusLayerMasks()
extern void FocusProvider_get_FocusLayerMasks_m4FADE8C5F8095AB00C1E7C797D2CA11FF2069B73 (void);
// 0x00000022 UnityEngine.Camera Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_UIRaycastCamera()
extern void FocusProvider_get_UIRaycastCamera_mB9403CB90269408831F5C3585554B2A190358855 (void);
// 0x00000023 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_IsSetupValid()
extern void FocusProvider_get_IsSetupValid_m849FD0D6E39B0F2353BBEC299A1124098258FE46 (void);
// 0x00000024 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::add_PrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_add_PrimaryPointerChanged_m2A7603CE746B0BF7EEF04EAD7492F7F8506BD21A (void);
// 0x00000025 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::remove_PrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_remove_PrimaryPointerChanged_m6488B2D2062E46C386871256FA51ADA64DAFD83F (void);
// 0x00000026 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Initialize()
extern void FocusProvider_Initialize_mBFD63C9C05E39FC3AD3BEB8229FEE710E05AAFE0 (void);
// 0x00000027 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Destroy()
extern void FocusProvider_Destroy_m2DC05D105D94D8241153E20C9E0999954C057F85 (void);
// 0x00000028 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::Update()
extern void FocusProvider_Update_mA74FD1C41EC663923ACFE851BC8B23F6E73A51CD (void);
// 0x00000029 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdateGazeProvider()
extern void FocusProvider_UpdateGazeProvider_mA0ADD568FED58E52F3C151E7E3DFE5A93512B35C (void);
// 0x0000002A UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetFocusedObject(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_GetFocusedObject_m765FD16DB747BBA3A4E3E3EC50763D602BB76CEE (void);
// 0x0000002B System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryGetFocusDetails(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Physics.FocusDetails&)
extern void FocusProvider_TryGetFocusDetails_mBC5AE387E5FA255104CF464981A222BEC124E387 (void);
// 0x0000002C System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryOverrideFocusDetails(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Physics.FocusDetails)
extern void FocusProvider_TryOverrideFocusDetails_m6739BEF276FA52AE290C1C5F7BCCB8F6F0EF22E3 (void);
// 0x0000002D System.UInt32 Microsoft.MixedReality.Toolkit.Input.FocusProvider::GenerateNewPointerId()
extern void FocusProvider_GenerateNewPointerId_m3CF18510AF9D7FB092C4DDDF729DF086AD364544 (void);
// 0x0000002E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::FindOrCreateUiRaycastCamera()
extern void FocusProvider_FindOrCreateUiRaycastCamera_mFB9FBE76B5133DBB0408378BACD129DDC7FA70BD (void);
// 0x0000002F System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::CleanUpUiRaycastCamera()
extern void FocusProvider_CleanUpUiRaycastCamera_m3F102E1D0B47CF2E135AEF32CF6B6BEBDB502D30 (void);
// 0x00000030 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::IsPointerRegistered(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_IsPointerRegistered_m42374206026EFA3F51CDDBF05EAF0D222A910D18 (void);
// 0x00000031 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::RegisterPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_RegisterPointer_m2459E3903FB2A1C31A01620DB5ABB21C0194F2CB (void);
// 0x00000032 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::RegisterPointers(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void FocusProvider_RegisterPointers_m00DB699D1C7003591201A417BC9B614004A15214 (void);
// 0x00000033 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::UnregisterPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_UnregisterPointer_mF801BFB37423BB951BC8E182277CB006D45A6DB6 (void);
// 0x00000034 System.Collections.Generic.IEnumerable`1<T> Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointers()
// 0x00000035 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::SubscribeToPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler,System.Boolean)
extern void FocusProvider_SubscribeToPrimaryPointerChanged_m8E7E1578350E6239A2EDD736CC2133670D74AC8F (void);
// 0x00000036 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UnsubscribeFromPrimaryPointerChanged(Microsoft.MixedReality.Toolkit.Input.PrimaryPointerChangedHandler)
extern void FocusProvider_UnsubscribeFromPrimaryPointerChanged_m87D0BA93421DAED9704F4982617F642A6CB53544 (void);
// 0x00000037 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::TryGetPointerData(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData&)
extern void FocusProvider_TryGetPointerData_m7C7001AA325A58B037A3E422B1BF89005B100013 (void);
// 0x00000038 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdatePointers()
extern void FocusProvider_UpdatePointers_m6DE825ECB6D688860F203BC8E0BBADFF5C4DCFCE (void);
// 0x00000039 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdatePointer(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData)
extern void FocusProvider_UpdatePointer_mD8071484F6B61D6C9C9081F22B647570AE7B9BD2 (void);
// 0x0000003A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::TruncatePointerRayToHit(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void FocusProvider_TruncatePointerRayToHit_mE95478CE04B4CDEA695104C7D1C7ECFE2192CA00 (void);
// 0x0000003B Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPrioritizedHitResult(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,UnityEngine.LayerMask[])
extern void FocusProvider_GetPrioritizedHitResult_mE7582A24B96DE363C849E1CC8DC9C81EE8CE6F26 (void);
// 0x0000003C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::ReconcilePointers()
extern void FocusProvider_ReconcilePointers_m5E7FF5569B45470E54A823D9590B6A9C2CA54392 (void);
// 0x0000003D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::QueryScene(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.IMixedRealityRaycastProvider,UnityEngine.LayerMask[],Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult,System.Int32,System.Boolean)
extern void FocusProvider_QueryScene_mC3896F3D261452CD8EDB1CDDC46F6A979EDE654F (void);
// 0x0000003E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::RaycastGraphics(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.EventSystems.PointerEventData,UnityEngine.LayerMask[],Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void FocusProvider_RaycastGraphics_mB28C4CE781E332377185569C3C855268180818A3 (void);
// 0x0000003F System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider::RaycastGraphicsStep(UnityEngine.EventSystems.PointerEventData,Microsoft.MixedReality.Toolkit.Physics.RayStep,UnityEngine.LayerMask[],UnityEngine.EventSystems.RaycastResult&)
extern void FocusProvider_RaycastGraphicsStep_mC0951DDAC7638FBDBF01279B30F509380681CEFF (void);
// 0x00000040 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::UpdateFocusedObjects()
extern void FocusProvider_UpdateFocusedObjects_mBDD26DB46012EC374BED29D8580C505B9C95627D (void);
// 0x00000041 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void FocusProvider_OnSourceDetected_m095C53CCA627FA851E844C97A304D0BD49C37A16 (void);
// 0x00000042 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void FocusProvider_OnSourceLost_mC752025A98521FC73E80254EA549D13DC27666C0 (void);
// 0x00000043 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void FocusProvider_OnSpeechKeywordRecognized_mB004898A1C9F9A0A620A36DD8683D47AFBE971A8 (void);
// 0x00000044 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void FocusProvider_GetPointerBehavior_mD1FF836CC81F9C457BCBC6657F3E0CB2FF816590 (void);
// 0x00000045 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
// 0x00000046 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::GetPointerBehavior(System.Type,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void FocusProvider_GetPointerBehavior_mE1784DFDF228550A41AEB2141B76B352D152872D (void);
// 0x00000047 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider::get_GazePointerBehavior()
extern void FocusProvider_get_GazePointerBehavior_mC7F25D0CD9744A1E5EA94F78350803147C08A824 (void);
// 0x00000048 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::set_GazePointerBehavior(Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
extern void FocusProvider_set_GazePointerBehavior_m6DCEA9A6DA3A5F687F5BC498BFD860587DBA6CAC (void);
// 0x00000049 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::SetPointerBehavior(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.InputSourceType,Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
// 0x0000004A System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider::.cctor()
extern void FocusProvider__cctor_mF7C24DEBF9C9684D4D24B9BC68C0AF2D1EDB6667 (void);
// 0x0000004B System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Clear()
extern void PointerHitResult_Clear_m65E7D57BB2CC7B58E891453EA25E243F6373299C (void);
// 0x0000004C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(UnityEngine.GameObject,UnityEngine.Vector3,UnityEngine.Vector4,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single)
extern void PointerHitResult_Set_mA01A8914F89FFFBB28D942F9843FF004A8C7546D (void);
// 0x0000004D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single,System.Boolean)
extern void PointerHitResult_Set_m62A8873803D3648CDCFC76A1EE05040D5C015B1B (void);
// 0x0000004E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::Set(UnityEngine.EventSystems.RaycastResult,UnityEngine.Vector3,UnityEngine.Vector4,Microsoft.MixedReality.Toolkit.Physics.RayStep,System.Int32,System.Single)
extern void PointerHitResult_Set_mC867FA296A2A9E9BB02542D0292A74F9FDF5CB64 (void);
// 0x0000004F System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult::.ctor()
extern void PointerHitResult__ctor_m2969F05CF88559B2D790D738CAD8976FCAB3BCAC (void);
// 0x00000050 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_StartPoint()
extern void PointerData_get_StartPoint_mA79720649FBEED0E7154B2D36972956819B1FFE6 (void);
// 0x00000051 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_StartPoint(UnityEngine.Vector3)
extern void PointerData_set_StartPoint_m88D3B6E1E7F684DFC12795D8ECD04CA8CE986EF4 (void);
// 0x00000052 Microsoft.MixedReality.Toolkit.Physics.FocusDetails Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_Details()
extern void PointerData_get_Details_m274AEF840675491ACAC004258F57F182082E7EFF (void);
// 0x00000053 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_Details(Microsoft.MixedReality.Toolkit.Physics.FocusDetails)
extern void PointerData_set_Details_mFC3684738BFD1D3B884B25B0D186AB1988FA5D8B (void);
// 0x00000054 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_CurrentPointerTarget()
extern void PointerData_get_CurrentPointerTarget_mD7E9739FB5757AD9E48D8D0DCF6188187A6DFDAD (void);
// 0x00000055 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_PreviousPointerTarget()
extern void PointerData_get_PreviousPointerTarget_mCD723B519857A6578C3893061F6681F8C5688F07 (void);
// 0x00000056 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_PreviousPointerTarget(UnityEngine.GameObject)
extern void PointerData_set_PreviousPointerTarget_m1E2891C5039FEFEAEA476354667B81A57D64D4A8 (void);
// 0x00000057 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_RayStepIndex()
extern void PointerData_get_RayStepIndex_m52E3A321928E81AB7380544CB9FF4684C8F4990D (void);
// 0x00000058 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::set_RayStepIndex(System.Int32)
extern void PointerData_set_RayStepIndex_mBD478BA3D7A7B36D461659E2613F22370EBDE1D2 (void);
// 0x00000059 UnityEngine.EventSystems.PointerEventData Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_GraphicEventData()
extern void PointerData_get_GraphicEventData_mA5D09FDB5B8253D24F7D6741D82912AFD4D14417 (void);
// 0x0000005A System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::get_IsCurrentPointerTargetInvalid()
extern void PointerData_get_IsCurrentPointerTargetInvalid_m589A482DF5EE0385396CDA0D84B2EAB1A9A2F3F4 (void);
// 0x0000005B System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer)
extern void PointerData__ctor_m5D7C5088ADFC382D87DFCC7823AACE51B751CD28 (void);
// 0x0000005C System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::UpdateHit(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerHitResult)
extern void PointerData_UpdateHit_mE246A47E2825D0D4352921DD356689815D370DC2 (void);
// 0x0000005D System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::UpdateFocusLockedHit()
extern void PointerData_UpdateFocusLockedHit_m95DEA933A07095CAB7539C4C520F1F793DE93915 (void);
// 0x0000005E System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::ResetFocusedObjects(System.Boolean)
extern void PointerData_ResetFocusedObjects_mCA76F9AF47E6DD129200F7874708A80D173C090F (void);
// 0x0000005F System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::Equals(Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData)
extern void PointerData_Equals_m21A9B6F3A7531A823EB992FB051C6AB6053C201C (void);
// 0x00000060 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::Equals(System.Object)
extern void PointerData_Equals_m626680D9C107092D19059B8CCD07304F8A511EAE (void);
// 0x00000061 System.Int32 Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::GetHashCode()
extern void PointerData_GetHashCode_m57E5E175D58E8B2B1604C35DD47D95DC9EF1751F (void);
// 0x00000062 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerData::.cctor()
extern void PointerData__cctor_mA64CE47C0008B55EEC241C648A2BAF5C243B73AC (void);
// 0x00000063 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::Matches(System.Type,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void PointerPreferences_Matches_mEFA09DCD4F4C18AEA6BFEE35943880F954CE95BE (void);
// 0x00000064 System.Boolean Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::Matches(System.Type)
extern void PointerPreferences_Matches_mC3A45FEC4B516EE7A732C865E3A6B53C18BE77CF (void);
// 0x00000065 Microsoft.MixedReality.Toolkit.Input.PointerBehavior Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::GetBehaviorForHandedness(Microsoft.MixedReality.Toolkit.Utilities.Handedness)
extern void PointerPreferences_GetBehaviorForHandedness_m3CE5F919E106176E913B8949C6C18BAD705AF8BF (void);
// 0x00000066 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::SetBehaviorForHandedness(Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.PointerBehavior)
extern void PointerPreferences_SetBehaviorForHandedness_mF0171F288F892411DEF1227EA53BAE5D96334186 (void);
// 0x00000067 System.Void Microsoft.MixedReality.Toolkit.Input.FocusProvider/PointerPreferences::.ctor(System.Type,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void PointerPreferences__ctor_m87FF89FA760344D5C0708EA896E326DCB21A7DA8 (void);
// 0x00000068 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::get_IsGazePointerActive()
extern void GazePointerVisibilityStateMachine_get_IsGazePointerActive_mDB3A3A876A0A33642C642C58E50A7BDE7AD77D43 (void);
// 0x00000069 System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::UpdateState(System.Int32,System.Int32,System.Int32,System.Boolean)
extern void GazePointerVisibilityStateMachine_UpdateState_m99F62548610C88B73BB710EAF96E0FB20A752C5E (void);
// 0x0000006A System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::OnSpeechKeywordRecognized(Microsoft.MixedReality.Toolkit.Input.SpeechEventData)
extern void GazePointerVisibilityStateMachine_OnSpeechKeywordRecognized_m4636C6D52AD7B13111A2CAF63C71F5CE266B1933 (void);
// 0x0000006B System.Void Microsoft.MixedReality.Toolkit.Input.GazePointerVisibilityStateMachine::.ctor()
extern void GazePointerVisibilityStateMachine__ctor_m1E58B319AD53C938BF2DD94CC5DDBC01B37C1A04 (void);
// 0x0000006C System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_Enabled()
extern void GazeProvider_get_Enabled_m5605B7B706ECBE3F0FF575DDE89ED8DFF21CCB0F (void);
// 0x0000006D System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_Enabled(System.Boolean)
extern void GazeProvider_set_Enabled_mBD9867CD6073B7CC4708208754ED84400A3D469A (void);
// 0x0000006E Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeInputSource()
extern void GazeProvider_get_GazeInputSource_m3F65624E14FC447C228A9872193F47A770FD9F57 (void);
// 0x0000006F Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazePointer()
extern void GazeProvider_get_GazePointer_mA99FD93F398043EDCBABAFE2F7EA43701D0909F9 (void);
// 0x00000070 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeCursorPrefab()
extern void GazeProvider_get_GazeCursorPrefab_mAEBA4DBE5D658431964515890FD175247194E913 (void);
// 0x00000071 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_GazeCursorPrefab(UnityEngine.GameObject)
extern void GazeProvider_set_GazeCursorPrefab_m9D2740963E36C88C6F211B313D0DA873C19D59C0 (void);
// 0x00000072 Microsoft.MixedReality.Toolkit.Input.IMixedRealityCursor Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeCursor()
extern void GazeProvider_get_GazeCursor_mD08ED2163FDCB1DC14C1DECEFDE03682EF761676 (void);
// 0x00000073 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeTarget()
extern void GazeProvider_get_GazeTarget_mDA3F9116A842036FBE140696D5927EAE8FEEFA36 (void);
// 0x00000074 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_GazeTarget(UnityEngine.GameObject)
extern void GazeProvider_set_GazeTarget_m1C27FA43117A639DEF3D1294A442E84F4EA8A13B (void);
// 0x00000075 Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitInfo()
extern void GazeProvider_get_HitInfo_m72895D36507BD10C9C6757669DA3A563346FB810 (void);
// 0x00000076 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitInfo(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit)
extern void GazeProvider_set_HitInfo_m3B50695F2997BD48F855ADDBA28EE54C41B6D739 (void);
// 0x00000077 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitPosition()
extern void GazeProvider_get_HitPosition_mDB9E014C5EC5CCA268D03B3E21453469F47A3FA8 (void);
// 0x00000078 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitPosition(UnityEngine.Vector3)
extern void GazeProvider_set_HitPosition_mA450278AA4FA80ECD7DABADD3756B262C4F3F644 (void);
// 0x00000079 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HitNormal()
extern void GazeProvider_get_HitNormal_m49856DD31AE415BF477DDD88612E0EDFDA4021B8 (void);
// 0x0000007A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HitNormal(UnityEngine.Vector3)
extern void GazeProvider_set_HitNormal_m13E984AD955A94422CCB35A9F7CFDAE774EC08E9 (void);
// 0x0000007B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeOrigin()
extern void GazeProvider_get_GazeOrigin_m1052CFC32E815575B91513918A83FC761F68B41D (void);
// 0x0000007C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GazeDirection()
extern void GazeProvider_get_GazeDirection_m9D54A34137DB085651777DF01312858B431DAE90 (void);
// 0x0000007D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HeadVelocity()
extern void GazeProvider_get_HeadVelocity_m9DDC7BD166B5C39931CB874820178268001444F3 (void);
// 0x0000007E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HeadVelocity(UnityEngine.Vector3)
extern void GazeProvider_set_HeadVelocity_m0A0B6C15A3A3365F20BFF17D414C4525FD52CC63 (void);
// 0x0000007F UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_HeadMovementDirection()
extern void GazeProvider_get_HeadMovementDirection_m6D07D080739CA2A29DE6C9CE3DC807549EAD381C (void);
// 0x00000080 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_HeadMovementDirection(UnityEngine.Vector3)
extern void GazeProvider_set_HeadMovementDirection_mF952765DCEF4AD2F5BE4FE01C5F53F498EA87ED0 (void);
// 0x00000081 UnityEngine.GameObject Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_GameObjectReference()
extern void GazeProvider_get_GameObjectReference_m4FEFF5737F7C9E8B5C5958E4B7CD73D7C1F95980 (void);
// 0x00000082 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnValidate()
extern void GazeProvider_OnValidate_m5C6A81452FA52EE348F3082664031E37838655D0 (void);
// 0x00000083 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnEnable()
extern void GazeProvider_OnEnable_m8CD70E28586DEF71CCA41C4A87F0060CA1D9867F (void);
// 0x00000084 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::Start()
extern void GazeProvider_Start_m459314DA5DF1EBE72514BD7F22633531879AED34 (void);
// 0x00000085 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::Update()
extern void GazeProvider_Update_mD7FE6ED0C1D13EF59D1B5C11C66ED30AAE6FCF36 (void);
// 0x00000086 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::LateUpdate()
extern void GazeProvider_LateUpdate_m42197509EE8100D45DFDF4249EFA33D7EA8CE0DF (void);
// 0x00000087 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnDisable()
extern void GazeProvider_OnDisable_m0CFD6F22CEBD964DA901F040E15B8CD178870426 (void);
// 0x00000088 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnDestroy()
extern void GazeProvider_OnDestroy_m5B9B385A4330ECD44B17C2BF5B8566C00DF58C13 (void);
// 0x00000089 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::RegisterHandlers()
extern void GazeProvider_RegisterHandlers_m1623D5A98E1B56C7D259090FC8FB1BD3365882FC (void);
// 0x0000008A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UnregisterHandlers()
extern void GazeProvider_UnregisterHandlers_m09B3D5EF74D353D4330F2276F6CAF8234971DEA4 (void);
// 0x0000008B System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnInputUp(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GazeProvider_OnInputUp_m9ED2D5B11C0C99E77B8ADBAB7028C224B7126C51 (void);
// 0x0000008C System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OnInputDown(Microsoft.MixedReality.Toolkit.Input.InputEventData)
extern void GazeProvider_OnInputDown_m61523A8F155B6EFCD9BF89B481848A51F4BB4956 (void);
// 0x0000008D Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.GazeProvider::InitializeGazePointer()
extern void GazeProvider_InitializeGazePointer_m3146361F209E4ABFC64FF645E02A45E30A4F4D60 (void);
// 0x0000008E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::RaiseSourceDetected()
extern void GazeProvider_RaiseSourceDetected_mAAD73AB41F11E4A00171EA471760E22DEEDAADDF (void);
// 0x0000008F System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateGazeInfoFromHit(Microsoft.MixedReality.Toolkit.Input.MixedRealityRaycastHit)
extern void GazeProvider_UpdateGazeInfoFromHit_m78539F2571F9429FF4C28D031DE6D0165DAD62EC (void);
// 0x00000090 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::SetGazeCursor(UnityEngine.GameObject)
extern void GazeProvider_SetGazeCursor_m9B9E2EA50877B1C2C40F9E0613413B42B9EC2FA2 (void);
// 0x00000091 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingEnabledAndValid()
extern void GazeProvider_get_IsEyeTrackingEnabledAndValid_mEDD77AAF2E69D3D1A7513503B65CD3600A72BBCE (void);
// 0x00000092 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingDataValid()
extern void GazeProvider_get_IsEyeTrackingDataValid_m228B0EEA2AFC00CD38811B243986B4274FC30E12 (void);
// 0x00000093 System.Nullable`1<System.Boolean> Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeCalibrationValid()
extern void GazeProvider_get_IsEyeCalibrationValid_mD17079906D6F20A1FF09DAA4048EBAF69F4D7E12 (void);
// 0x00000094 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_IsEyeCalibrationValid(System.Nullable`1<System.Boolean>)
extern void GazeProvider_set_IsEyeCalibrationValid_mC0610D2969C94AF37C1F2D365BE5AC0D20139E9E (void);
// 0x00000095 UnityEngine.Ray Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_LatestEyeGaze()
extern void GazeProvider_get_LatestEyeGaze_mCE05B51A8BE8D33C6837D55D536E45C603E91227 (void);
// 0x00000096 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_LatestEyeGaze(UnityEngine.Ray)
extern void GazeProvider_set_LatestEyeGaze_m5DC5298D9938AB65390A73189E045872F3298A53 (void);
// 0x00000097 System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_IsEyeTrackingEnabled()
extern void GazeProvider_get_IsEyeTrackingEnabled_m22EF502BCCFC3555C14085BDFF647EF79E1697A0 (void);
// 0x00000098 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_IsEyeTrackingEnabled(System.Boolean)
extern void GazeProvider_set_IsEyeTrackingEnabled_mB46D0CD0863B4830F195F091D163B3AD913515CF (void);
// 0x00000099 System.DateTime Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_Timestamp()
extern void GazeProvider_get_Timestamp_mDD1B82FDE4B2751D9A1467C6E3AF78AC0F0FAA5C (void);
// 0x0000009A System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_Timestamp(System.DateTime)
extern void GazeProvider_set_Timestamp_mF2B93EC13DF7CB76BB027249A02520B7FA96BA9B (void);
// 0x0000009B System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateEyeGaze(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider,UnityEngine.Ray,System.DateTime)
extern void GazeProvider_UpdateEyeGaze_mB13AECA022A3D702DE8CB661E6CF0C238E2E4135 (void);
// 0x0000009C System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::UpdateEyeTrackingStatus(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeDataProvider,System.Boolean)
extern void GazeProvider_UpdateEyeTrackingStatus_m4CE440CBCB518F06D922E4AF52029828F45C6EC1 (void);
// 0x0000009D System.Boolean Microsoft.MixedReality.Toolkit.Input.GazeProvider::get_UseHeadGazeOverride()
extern void GazeProvider_get_UseHeadGazeOverride_mC27BCB7D7064EA1A37D72C8792F9BB61C9FDFEE3 (void);
// 0x0000009E System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::set_UseHeadGazeOverride(System.Boolean)
extern void GazeProvider_set_UseHeadGazeOverride_m51BADBDED84B28AE53562B63DDC66063AF779155 (void);
// 0x0000009F System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::OverrideHeadGaze(UnityEngine.Vector3,UnityEngine.Vector3)
extern void GazeProvider_OverrideHeadGaze_m7F7CBD41177338C10F105143854EB59BD6F042CC (void);
// 0x000000A0 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::.ctor()
extern void GazeProvider__ctor_mE9B3AE59852A6EBAFDB8A627642D9E5FBE3C860A (void);
// 0x000000A1 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::.cctor()
extern void GazeProvider__cctor_m94A42F863CFCB4450543B204849A07179F1C4B93 (void);
// 0x000000A2 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider::<>n__0()
extern void GazeProvider_U3CU3En__0_m34FE6CDC2E03B8CA09B19E9DDB4DDBCA99B0CECF (void);
// 0x000000A3 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::.ctor(Microsoft.MixedReality.Toolkit.Input.GazeProvider,System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,UnityEngine.LayerMask[],System.Single,UnityEngine.Transform,Microsoft.MixedReality.Toolkit.Physics.BaseRayStabilizer)
extern void InternalGazePointer__ctor_m680412F643BC782274829D6F375A2EB4CF7FAEFB (void);
// 0x000000A4 Microsoft.MixedReality.Toolkit.Input.IMixedRealityController Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Controller()
extern void InternalGazePointer_get_Controller_m78B60A083FFED497C7D0A6B7C845BF508E173B46 (void);
// 0x000000A5 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_Controller(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void InternalGazePointer_set_Controller_mC2591D136366A7EFB9395E23031EC7236BAA7C77 (void);
// 0x000000A6 Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_InputSourceParent()
extern void InternalGazePointer_get_InputSourceParent_m57739B769D5A231D9140027B75923A7003B21570 (void);
// 0x000000A7 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_InputSourceParent(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_set_InputSourceParent_mAB00A293722D0AE97A327EF54E2B641915605970 (void);
// 0x000000A8 System.Single Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_PointerExtent()
extern void InternalGazePointer_get_PointerExtent_m19521E1F33E91063E6F2B1A15FCC07B1AF1A4AA4 (void);
// 0x000000A9 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::set_PointerExtent(System.Single)
extern void InternalGazePointer_set_PointerExtent_m8F521DB5DA36F1EAF234D8E22ED233B772D03E7E (void);
// 0x000000AA System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::SetGazeInputSourceParent(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_SetGazeInputSourceParent_mCB5A8EE32EDCBFF195B3CE7FEC4DED21E67D0F81 (void);
// 0x000000AB System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPreSceneQuery()
extern void InternalGazePointer_OnPreSceneQuery_m31F124A679394D3977B3B62804C837E7A98AB34F (void);
// 0x000000AC System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPostSceneQuery()
extern void InternalGazePointer_OnPostSceneQuery_m729B66073B1009A01591BF5D234829643503753B (void);
// 0x000000AD System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::OnPreCurrentPointerTargetChange()
extern void InternalGazePointer_OnPreCurrentPointerTargetChange_m161A151572636D47E7F10C5BCCB5A030EA75B5D5 (void);
// 0x000000AE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Position()
extern void InternalGazePointer_get_Position_m6A3D45B7D8FB14224E4F1A163CFE828518F339FB (void);
// 0x000000AF UnityEngine.Quaternion Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::get_Rotation()
extern void InternalGazePointer_get_Rotation_m2550C4B0C860F40DC41AAB3D3C925C784FCB694F (void);
// 0x000000B0 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::Reset()
extern void InternalGazePointer_Reset_m03144D86503BB7EB13B7213C9A7BC50B4401E4C9 (void);
// 0x000000B1 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::RaisePointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_RaisePointerDown_m0C96AF6A0FE0984F61F700861D417E8F967991E8 (void);
// 0x000000B2 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::RaisePointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void InternalGazePointer_RaisePointerUp_m54989331B044CD68F0103C64ED7F32DB787B3AA5 (void);
// 0x000000B3 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/InternalGazePointer::.cctor()
extern void InternalGazePointer__cctor_mB66A39E776E804DD53FF5E0F78BC0DEAB9D18FFA (void);
// 0x000000B4 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<Start>d__63::MoveNext()
extern void U3CStartU3Ed__63_MoveNext_m1995BBF055EAF141D9C17374D7FBF5D9D5B55264 (void);
// 0x000000B5 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<Start>d__63::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__63_SetStateMachine_mFFE24F4AA58DA0BEDA1BFEEC5C07DAA9C8ACAF9F (void);
// 0x000000B6 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<RaiseSourceDetected>d__77::MoveNext()
extern void U3CRaiseSourceDetectedU3Ed__77_MoveNext_m3886E54E9E5F0455475EAEB0FA3D918761126E10 (void);
// 0x000000B7 System.Void Microsoft.MixedReality.Toolkit.Input.GazeProvider/<RaiseSourceDetected>d__77::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_mAD25DF053CFA407698925AC737602775ED4533DF (void);
// 0x000000B8 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::OnEnable()
extern void InputSystemGlobalHandlerListener_OnEnable_m8200CAF3CC35EF645C377C90A46B5D3017BF473F (void);
// 0x000000B9 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::Start()
extern void InputSystemGlobalHandlerListener_Start_m4CFB72FB90D531AA0A51DFC41298CD191680C323 (void);
// 0x000000BA System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::OnDisable()
extern void InputSystemGlobalHandlerListener_OnDisable_m114FE851191C66A6ADE6747FA889F9D4172692C6 (void);
// 0x000000BB System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::EnsureInputSystemValid()
extern void InputSystemGlobalHandlerListener_EnsureInputSystemValid_mC1EEA526C7E1F622598A7F24D17B3429DB8FEFB3 (void);
// 0x000000BC System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::RegisterHandlers()
// 0x000000BD System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::UnregisterHandlers()
// 0x000000BE System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener::.ctor()
extern void InputSystemGlobalHandlerListener__ctor_mBEB7175C2BC2EB90F213EA40D74DB0BFBA6FA2D8 (void);
// 0x000000BF System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_mB3603C3B8DE4E20F9719C586BA61319384ABDFDF (void);
// 0x000000C0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<Start>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__2_SetStateMachine_m962AD6827A53A07A77D01A0AB80D1C88D0FA386B (void);
// 0x000000C1 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::.cctor()
extern void U3CU3Ec__cctor_m292ABBAD09754BE86C8855C184C7ABAAF4520D3D (void);
// 0x000000C2 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::.ctor()
extern void U3CU3Ec__ctor_m5E32E12A8D7AF47F6646E6B1EE5DA01DA81B3737 (void);
// 0x000000C3 System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<>c::<EnsureInputSystemValid>b__4_0()
extern void U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_mF222F9635041A8AA0E5A52AD110BFD468C1CE7AB (void);
// 0x000000C4 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<EnsureInputSystemValid>d__4::MoveNext()
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m8A3DFB7EAF3583E06AE2B60C5C8FFF11770F0C89 (void);
// 0x000000C5 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalHandlerListener/<EnsureInputSystemValid>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m51E0D646F26D314B94CDA9CCD82EC8D2D3886A5F (void);
// 0x000000C6 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::OnEnable()
extern void InputSystemGlobalListener_OnEnable_m5E20C375720C55B61DA5501DFD4F450299CE4E97 (void);
// 0x000000C7 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::Start()
extern void InputSystemGlobalListener_Start_m2354BECA6B386CEAF9EFD9FC7B66B7CD43005E39 (void);
// 0x000000C8 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::OnDisable()
extern void InputSystemGlobalListener_OnDisable_m8F9AEFD92EA6D1E927FA83ABDB977890B3EBB2F1 (void);
// 0x000000C9 System.Threading.Tasks.Task Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::EnsureInputSystemValid()
extern void InputSystemGlobalListener_EnsureInputSystemValid_m7C298EE36539299287A924FEFF9F908479292D13 (void);
// 0x000000CA System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener::.ctor()
extern void InputSystemGlobalListener__ctor_m2C849D4D8D4AEF19D17426FE71AD24FD37A5ABD0 (void);
// 0x000000CB System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<Start>d__2::MoveNext()
extern void U3CStartU3Ed__2_MoveNext_m4E247D7B009018AEB60C476724746C6F6B919784 (void);
// 0x000000CC System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<Start>d__2::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CStartU3Ed__2_SetStateMachine_m25C1F2CBE76178EDD5E7B22BDB2F6654A9B95CCF (void);
// 0x000000CD System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::.cctor()
extern void U3CU3Ec__cctor_m39F7C09BB7286EDB8E062F761998F7414A8D9DE9 (void);
// 0x000000CE System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::.ctor()
extern void U3CU3Ec__ctor_mF10A728643D63C40A89F8CF6596DECC145E969B4 (void);
// 0x000000CF System.Boolean Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<>c::<EnsureInputSystemValid>b__4_0()
extern void U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m2FD2AE39CC084CF42E5359EB5DFA0E0DC479E8D3 (void);
// 0x000000D0 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<EnsureInputSystemValid>d__4::MoveNext()
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m7B0D5FF107CDE4A70864EA5F6D6433CB8344411E (void);
// 0x000000D1 System.Void Microsoft.MixedReality.Toolkit.Input.InputSystemGlobalListener/<EnsureInputSystemValid>d__4::SetStateMachine(System.Runtime.CompilerServices.IAsyncStateMachine)
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_mA88C5F770AE0D475E0137FC57E9CA4464D54293D (void);
// 0x000000D2 UnityEngine.Camera Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_RaycastCamera()
extern void MixedRealityInputModule_get_RaycastCamera_m329925BD02AF4BA852B7324C9F9423A23F89F634 (void);
// 0x000000D3 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::set_RaycastCamera(UnityEngine.Camera)
extern void MixedRealityInputModule_set_RaycastCamera_mCF7DFA3F997E6BC9F379ECA58285E9CD28EA19C7 (void);
// 0x000000D4 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_ManualInitializationRequired()
extern void MixedRealityInputModule_get_ManualInitializationRequired_m65BD22FAE18CAEDE539BBA6CB8B9A85B35906FEE (void);
// 0x000000D5 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::set_ManualInitializationRequired(System.Boolean)
extern void MixedRealityInputModule_set_ManualInitializationRequired_m0E808095BAE7F2F9C1C10C78349DE1724A1FEC72 (void);
// 0x000000D6 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_ProcessPaused()
extern void MixedRealityInputModule_get_ProcessPaused_m4FE434E199953C439425AAD2AB065CE835712188 (void);
// 0x000000D7 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::set_ProcessPaused(System.Boolean)
extern void MixedRealityInputModule_set_ProcessPaused_mC2EF7A900830146EFDACB2078E3DCFA082997950 (void);
// 0x000000D8 System.Collections.Generic.IEnumerable`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::get_ActiveMixedRealityPointers()
extern void MixedRealityInputModule_get_ActiveMixedRealityPointers_mFC39F2AC95C512A07F84370D4EB2FBFCD5B684C8 (void);
// 0x000000D9 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ActivateModule()
extern void MixedRealityInputModule_ActivateModule_m78E8ED9474F2626BE2F2FCCBA89583335A2E0123 (void);
// 0x000000DA System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Initialize()
extern void MixedRealityInputModule_Initialize_m5C8767E903A42AC29152699E50A524A0A726774C (void);
// 0x000000DB System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Suspend()
extern void MixedRealityInputModule_Suspend_m9F994ADB464CB0CC345C47C99ACF497A124356E5 (void);
// 0x000000DC System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::DeactivateModule()
extern void MixedRealityInputModule_DeactivateModule_m7D76EBEC365C85207AD0F8E5568662E1A71AF50F (void);
// 0x000000DD System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Process()
extern void MixedRealityInputModule_Process_m5B1DC8A36022288223BEB01770C30AEA574CF4C5 (void);
// 0x000000DE System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::IsModuleSupported()
extern void MixedRealityInputModule_IsModuleSupported_m4FDAB4F42C2DCCBF727A7DF8DAF57510AEB64703 (void);
// 0x000000DF System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ProcessMrtkPointerLost(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_ProcessMrtkPointerLost_m59084BBB2C8038D798576A03EC55AB31D49A003A (void);
// 0x000000E0 UnityEngine.EventSystems.PointerInputModule/MouseState Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::GetMousePointerEventData(System.Int32)
extern void MixedRealityInputModule_GetMousePointerEventData_m1CA4684C1CEF1BDE09AC6A2C126960EDFB706EF1 (void);
// 0x000000E1 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::UpdateMousePointerEventData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_UpdateMousePointerEventData_mAC08A45AA77331B1A15B3C5F6ADF1A017A656183 (void);
// 0x000000E2 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::ResetMousePointerEventData(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_ResetMousePointerEventData_m32136DE4736A477FEA1BC3CB50E34F1378D0EEC0 (void);
// 0x000000E3 UnityEngine.EventSystems.PointerEventData/FramePressState Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::StateForPointer(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData)
extern void MixedRealityInputModule_StateForPointer_mB186671E20373F947EF4CF31D8C0B70E2014BFE6 (void);
// 0x000000E4 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mEAC98C7AB39889F3742963903CD989ABE90DFCF8 (void);
// 0x000000E5 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m2E4EBBAF819B2B7AAF8D19342263CAE631ABE619 (void);
// 0x000000E6 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m6263D3A6BB437A5C5D81485CE86F2099AAF1688E (void);
// 0x000000E7 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler.OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m10A8EB5F0E8C630A21204F79D2BFEBDB5F395B17 (void);
// 0x000000E8 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::IsPointerIdInRemovedList(System.Int32)
extern void MixedRealityInputModule_IsPointerIdInRemovedList_m3718EECC9B7D0837C9295E1511F5AAC894B1FF4E (void);
// 0x000000E9 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m9D8A5008FC519959090EEAF1E044E247E803E2E4 (void);
// 0x000000EA System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::OnSourceDetected(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputModule_OnSourceDetected_m26841D4D953EC23155F5049B767094C4E83AF90B (void);
// 0x000000EB System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler.OnSourceLost(Microsoft.MixedReality.Toolkit.Input.SourceStateEventData)
extern void MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m07D8494F41E4B593F41B147E281771D8B0727F24 (void);
// 0x000000EC System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::.ctor()
extern void MixedRealityInputModule__ctor_m06837023269881BA9613045742AE64EA732EE581 (void);
// 0x000000ED System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule::.cctor()
extern void MixedRealityInputModule__cctor_m25126A0E52F8D95D35E4D60F5E1792185837C12C (void);
// 0x000000EE System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/PointerData::.ctor(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.EventSystems.EventSystem)
extern void PointerData__ctor_m70DABFE0F62E54BA41F768926EDDBAB2F8F4A478 (void);
// 0x000000EF System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::.ctor(System.Int32)
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16__ctor_m60718834B1CFBB90DA5865424DB7F011DA835093 (void);
// 0x000000F0 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.IDisposable.Dispose()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_IDisposable_Dispose_m560BF9972D7B8D645C0025E222616B29E024DFE8 (void);
// 0x000000F1 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::MoveNext()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_MoveNext_mA6FD4775DBB99FCA8C85EB97C5A13E8C7B62A93F (void);
// 0x000000F2 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::<>m__Finally1()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_U3CU3Em__Finally1_m5BBB5BA54298094F68689593CD03E77792F4A8DF (void);
// 0x000000F3 Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.Collections.Generic.IEnumerator<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer>.get_Current()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m40D08339CE27568F466E5C567A9C01EFDF1B6E42 (void);
// 0x000000F4 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.Collections.IEnumerator.Reset()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerator_Reset_mED23D57F0095FDDFB161CC451DA29729BB57274B (void);
// 0x000000F5 System.Object Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerator_get_Current_m9ECF592B70FF514CB8FE4C1DF2425A869EC1FAE8 (void);
// 0x000000F6 System.Collections.Generic.IEnumerator`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.Collections.Generic.IEnumerable<Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer>.GetEnumerator()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m3A18A1027315ACCF5C92F5DD16C0F30BAF0C2354 (void);
// 0x000000F7 System.Collections.IEnumerator Microsoft.MixedReality.Toolkit.Input.MixedRealityInputModule/<get_ActiveMixedRealityPointers>d__16::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerable_GetEnumerator_mD10771B7666047AC7A88E5AAD20052ECB33E4EFE (void);
// 0x000000F8 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.ctor(Microsoft.MixedReality.Toolkit.IMixedRealityServiceRegistrar,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void MixedRealityInputSystem__ctor_mA9FBEA880CC96FFEC995254FAA2236826122E495 (void);
// 0x000000F9 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.ctor(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile)
extern void MixedRealityInputSystem__ctor_m4138907A083C53861CF9D8D419162BB2B8B04A44 (void);
// 0x000000FA System.String Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_Name()
extern void MixedRealityInputSystem_get_Name_mEEA0B81ABCA81A582CE4602476A7D2FBBEAEFCA8 (void);
// 0x000000FB System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_Name(System.String)
extern void MixedRealityInputSystem_set_Name_m4C7B0922D24B9ED77D5CE11378E745F6B98F5D64 (void);
// 0x000000FC System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::add_InputEnabled(System.Action)
extern void MixedRealityInputSystem_add_InputEnabled_mA2E19024E91B7CC0AB78181CA8D6B6D647672251 (void);
// 0x000000FD System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::remove_InputEnabled(System.Action)
extern void MixedRealityInputSystem_remove_InputEnabled_mE4D786FCF1CA8B840B3A58E9167CE99E4A3F9CE0 (void);
// 0x000000FE System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::add_InputDisabled(System.Action)
extern void MixedRealityInputSystem_add_InputDisabled_mC45155FC69BB0872D73502C8CAF1CDF831B8A729 (void);
// 0x000000FF System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::remove_InputDisabled(System.Action)
extern void MixedRealityInputSystem_remove_InputDisabled_m75243AD6B6DC633FBE64F418BBCC5ECB248677EA (void);
// 0x00000100 System.Collections.Generic.HashSet`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_DetectedInputSources()
extern void MixedRealityInputSystem_get_DetectedInputSources_m1DC59E861DA122EE7A0C9556ACBA40EA9E604772 (void);
// 0x00000101 System.Collections.Generic.HashSet`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityController> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_DetectedControllers()
extern void MixedRealityInputSystem_get_DetectedControllers_m0601669AEDA32E98D1DADEC835F94572F891F9F9 (void);
// 0x00000102 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystemProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_InputSystemProfile()
extern void MixedRealityInputSystem_get_InputSystemProfile_m4792CB533E3BAA5316B08D1DA754EEDBE334E720 (void);
// 0x00000103 Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_FocusProvider()
extern void MixedRealityInputSystem_get_FocusProvider_mE26F74977DFEB000FFD50D73DA69E27E2274F2B5 (void);
// 0x00000104 Microsoft.MixedReality.Toolkit.Input.IMixedRealityRaycastProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_RaycastProvider()
extern void MixedRealityInputSystem_get_RaycastProvider_mD8B161FA98D161DD039F5EDE81CF0788EAB6B44C (void);
// 0x00000105 Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_GazeProvider()
extern void MixedRealityInputSystem_get_GazeProvider_m71E08237B2A8E86E3E0E1539155BF47952DE0845 (void);
// 0x00000106 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_GazeProvider(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGazeProvider)
extern void MixedRealityInputSystem_set_GazeProvider_mA5828B9A737DE0F03264C595F157239A5AF0EF52 (void);
// 0x00000107 Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_EyeGazeProvider()
extern void MixedRealityInputSystem_get_EyeGazeProvider_m6144F06F59B98D8C9F1717C536DF50E3187D5819 (void);
// 0x00000108 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_EyeGazeProvider(Microsoft.MixedReality.Toolkit.Input.IMixedRealityEyeGazeProvider)
extern void MixedRealityInputSystem_set_EyeGazeProvider_m7AD9EF217A6C3EEA06ACB5D1666EA2F437EDF45D (void);
// 0x00000109 System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_IsInputEnabled()
extern void MixedRealityInputSystem_get_IsInputEnabled_mD1FE96D98B73E2285FF14BA2A559E6A16F536E13 (void);
// 0x0000010A Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_CurrentInputActionRulesProfile()
extern void MixedRealityInputSystem_get_CurrentInputActionRulesProfile_m6E8C8C792FA3AA8F69C5CA6611F656E175618D56 (void);
// 0x0000010B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::set_CurrentInputActionRulesProfile(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputActionRulesProfile)
extern void MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mA4586511312369F5B5FD6317E5055DA7D4948B4B (void);
// 0x0000010C System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::CheckCapability(Microsoft.MixedReality.Toolkit.MixedRealityCapability)
extern void MixedRealityInputSystem_CheckCapability_mEE90EA9CC3B22350371ED417541E5CC128CC499D (void);
// 0x0000010D System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::get_Priority()
extern void MixedRealityInputSystem_get_Priority_m7CCCB0D8F8CAB0C65E30C271A40F0AC8F4FC1884 (void);
// 0x0000010E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Initialize()
extern void MixedRealityInputSystem_Initialize_m3645A8C7A76031E419DDFFB74F50B4B7D1701707 (void);
// 0x0000010F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Enable()
extern void MixedRealityInputSystem_Enable_m1875E5EB533B3FDC59321A436EFB8C3556E2E94E (void);
// 0x00000110 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::LateUpdate()
extern void MixedRealityInputSystem_LateUpdate_mE1326028D98A1FB9BC51012B73F63CD6E3010AD6 (void);
// 0x00000111 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::CreateDataProviders()
extern void MixedRealityInputSystem_CreateDataProviders_m88EF43BAF68E60DCD09F364C739697BE3DED3816 (void);
// 0x00000112 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::InstantiateGazeProvider(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerProfile)
extern void MixedRealityInputSystem_InstantiateGazeProvider_mC15F9EDA3C2657C8EF20FCF3B1986014BECDECA6 (void);
// 0x00000113 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Reset()
extern void MixedRealityInputSystem_Reset_m2F0F28484293548B9349FEBDE12F4EE5A8BC6027 (void);
// 0x00000114 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Disable()
extern void MixedRealityInputSystem_Disable_m032C1CFBF1301D69E154147E9798D3AB16FB2A19 (void);
// 0x00000115 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Destroy()
extern void MixedRealityInputSystem_Destroy_m3A11F3D6B173D3CD24EE54B44F3ACBF45CB67BCB (void);
// 0x00000116 System.Collections.Generic.IReadOnlyList`1<T> Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GetDataProviders()
// 0x00000117 T Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GetDataProvider(System.String)
// 0x00000118 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000119 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleFocusChangedEvents(Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler>)
extern void MixedRealityInputSystem_HandleFocusChangedEvents_m21979CE1ED5B995D665DC03F69EB37153685440C (void);
// 0x0000011A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleFocusEvent(UnityEngine.GameObject,Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler>)
extern void MixedRealityInputSystem_HandleFocusEvent_m028D05FABF5B2A3E31606F3872795F21FFCE3377 (void);
// 0x0000011B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandlePointerEvent(UnityEngine.EventSystems.BaseEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x0000011C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToGlobalListeners(Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x0000011D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToGlobalListeners(Microsoft.MixedReality.Toolkit.Input.FocusEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x0000011E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToFallbackHandlers(Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x0000011F System.Boolean Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::DispatchEventToObjectFocusedByPointer(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.BaseInputEventData,System.Boolean,UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<T>)
// 0x00000120 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Register(UnityEngine.GameObject)
extern void MixedRealityInputSystem_Register_m1632611B248DBA2B2F3735676E08F8608E38E9A0 (void);
// 0x00000121 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::Unregister(UnityEngine.GameObject)
extern void MixedRealityInputSystem_Unregister_m3990DB1C09F8EB9847C4D779E25EB922A9C161D4 (void);
// 0x00000122 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushInputDisable()
extern void MixedRealityInputSystem_PushInputDisable_m1C6134CD1138563C9588901BD28EF19686003C95 (void);
// 0x00000123 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopInputDisable()
extern void MixedRealityInputSystem_PopInputDisable_m963386716064CA88B94E668639E135D12D95CC07 (void);
// 0x00000124 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearInputDisableStack()
extern void MixedRealityInputSystem_ClearInputDisableStack_m1EBE9A31112EEC9BC1E0807EEEF7E83B4FB899BF (void);
// 0x00000125 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushModalInputHandler(UnityEngine.GameObject)
extern void MixedRealityInputSystem_PushModalInputHandler_m98058FA5209BAFD065352F38D7127584D934308E (void);
// 0x00000126 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopModalInputHandler()
extern void MixedRealityInputSystem_PopModalInputHandler_m1A3582BB12B1F77036E808166A21659C40404C66 (void);
// 0x00000127 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearModalInputStack()
extern void MixedRealityInputSystem_ClearModalInputStack_m844AC4CD1406313AFDAC98802290970D7B63D78F (void);
// 0x00000128 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PushFallbackInputHandler(UnityEngine.GameObject)
extern void MixedRealityInputSystem_PushFallbackInputHandler_m15E7071269ED94857B4F9D34C212C9948801B7DF (void);
// 0x00000129 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::PopFallbackInputHandler()
extern void MixedRealityInputSystem_PopFallbackInputHandler_mA01947304A32DC0B0955AF2ED1BEE781966909B1 (void);
// 0x0000012A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ClearFallbackInputStack()
extern void MixedRealityInputSystem_ClearFallbackInputStack_mE4122A99EEBF89B77A5924B0F22733A8A1AF0476 (void);
// 0x0000012B System.UInt32 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::GenerateNewSourceId()
extern void MixedRealityInputSystem_GenerateNewSourceId_m7E17D2A2E61BB2B82995A93437033A8EFFAC71C9 (void);
// 0x0000012C Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RequestNewGenericInputSource(System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer[],Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void MixedRealityInputSystem_RequestNewGenericInputSource_mFAEF9A86C0CF8C7309F2C2ED9B4177C0093A6338 (void);
// 0x0000012D Microsoft.MixedReality.Toolkit.Input.BaseGlobalInputSource Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RequestNewGlobalInputSource(System.String,Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusProvider,Microsoft.MixedReality.Toolkit.Input.InputSourceType)
extern void MixedRealityInputSystem_RequestNewGlobalInputSource_m3DA4A7A63A7347A5CFC16AF2C9A13D051952CB65 (void);
// 0x0000012E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceDetected(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void MixedRealityInputSystem_RaiseSourceDetected_mE938373E3F32B2DAF4A670D96024293AA5A437E9 (void);
// 0x0000012F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceLost(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController)
extern void MixedRealityInputSystem_RaiseSourceLost_mDA57604245A696ABD745933114B9851A36BF98C8 (void);
// 0x00000130 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceTrackingStateChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.TrackingState)
extern void MixedRealityInputSystem_RaiseSourceTrackingStateChanged_m05420DCBB2866C9A147E6A8E189AF6EDCEB47775 (void);
// 0x00000131 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePositionChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseSourcePositionChanged_m5AD15DE19E784BB7454063D0C36013F30DDAE21B (void);
// 0x00000132 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePositionChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseSourcePositionChanged_m967F87A91598032AA83C1DC663C674D73F185258 (void);
// 0x00000133 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourceRotationChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseSourceRotationChanged_mED6A6F5FAD4F893B0EDE6079EE581323A2C7C537 (void);
// 0x00000134 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSourcePoseChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseSourcePoseChanged_m2FCC604D4D0967D579F96352286FF5C5DE0FEE46 (void);
// 0x00000135 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePreFocusChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaisePreFocusChanged_mE9637BA4CFE4D54A51512C5C4A6122C9F8BEE6B9 (void);
// 0x00000136 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusChanged_m8B3851905AE8913362B79D4C8A27E5A120E51ED5 (void);
// 0x00000137 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusEnter(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusEnter_mEB0DE70CC3ABEE1FB82EE91A560EB503AC8336F5 (void);
// 0x00000138 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFocusExit(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,UnityEngine.GameObject)
extern void MixedRealityInputSystem_RaiseFocusExit_m5E4E22A657927C30BCF054921D4E8EA92E7F8801 (void);
// 0x00000139 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerDown_m597D81D77CADFDBEC77894A020D37A319E33E1A7 (void);
// 0x0000013A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerDragged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerDragged_m7D0D944206CD6417616CA948A9C639EB4B8CFEAA (void);
// 0x0000013B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerClicked(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Int32,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerClicked_m47DE7FDFC7075E831C6121875B09150783E3EFEE (void);
// 0x0000013C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::HandleClick()
extern void MixedRealityInputSystem_HandleClick_mCE92FA5236EB13D341FC007F9944C9B7150770E0 (void);
// 0x0000013D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePointerUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointer,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource)
extern void MixedRealityInputSystem_RaisePointerUp_mB992C214CDA63B5F725831E88F6A94B3C07FF337 (void);
// 0x0000013E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnInputDown(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseOnInputDown_mF1795FC45B5DF76FAEDA6D90BF2B482467EE0CDD (void);
// 0x0000013F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnInputUp(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseOnInputUp_m09526D3603E2B49D48473D7A30BFBE200F41488E (void);
// 0x00000140 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseFloatInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Single)
extern void MixedRealityInputSystem_RaiseFloatInputChanged_m54A789EA9530044082A5C5B2FFE3F23CAC35C189 (void);
// 0x00000141 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePositionInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaisePositionInputChanged_mA79017106D5CE5FFE9F8314FFA668F048623A3CF (void);
// 0x00000142 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePositionInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaisePositionInputChanged_m2383FE56AA9C33D740F57D84291EC5066A2BB4B4 (void);
// 0x00000143 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseRotationInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseRotationInputChanged_m063A221D98DD390CCC0B28461A617C1F5C7FC3DA (void);
// 0x00000144 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaisePoseInputChanged(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaisePoseInputChanged_mE0EFC51B4149A2D4763924990596BD866DB9049A (void);
// 0x00000145 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureStarted_m0E3A61B3FFB97136E8979DC58372A1C6539A5324 (void);
// 0x00000146 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m0D64F1FA57AC1D62996CAE3110109A3170F976DC (void);
// 0x00000147 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m3AFFB88BB46D0A4EEFC3B2B1C9D947879824E470 (void);
// 0x00000148 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m83F1E0862F16004C99CDA0A118893E017AFC6CCC (void);
// 0x00000149 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseGestureUpdated_m86D5660C5FA5D91BBEF1031E74681F055953A325 (void);
// 0x0000014A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseGestureUpdated_mCB8CB7B0C45D6F8B11C983D87A5C45EB2BD47B43 (void);
// 0x0000014B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureCompleted_mA4201D117F5B1A868F7F4EC441B14EC7646A1D3F (void);
// 0x0000014C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m9F7DAB69BC17656DA80912C21CBA0A8597F47D35 (void);
// 0x0000014D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseGestureCompleted_mBB4B1BFD92B89BD32DF394A43BDB5BAB4BF30D60 (void);
// 0x0000014E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_RaiseGestureCompleted_mEA31A162F0E5A17D5FEEBC427B110E4CAE1DE854 (void);
// 0x0000014F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_RaiseGestureCompleted_m50A19D01CD4D9EDC2F85110FFDDD4C5D2D93A4DE (void);
// 0x00000150 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseGestureCanceled(Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction)
extern void MixedRealityInputSystem_RaiseGestureCanceled_m6C78DE10C5F1820F8B40B6EC55B52B6703CFC1AD (void);
// 0x00000151 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseSpeechCommandRecognized(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.RecognitionConfidenceLevel,System.TimeSpan,System.DateTime,Microsoft.MixedReality.Toolkit.Input.SpeechCommands)
extern void MixedRealityInputSystem_RaiseSpeechCommandRecognized_mE1E55258FFD48292A92E963C34C82544B9811058 (void);
// 0x00000152 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationHypothesis(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationHypothesis_mEA20AAF662B0CDF32507E1281630620ED00290D8 (void);
// 0x00000153 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationResult(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationResult_m1CA9981073F93215DF0D0E19D8D31BE2312A8469 (void);
// 0x00000154 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationComplete(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationComplete_mB112654C78AAFD8BAA4DCB33F9A2F3230FB69B78 (void);
// 0x00000155 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseDictationError(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,System.String,UnityEngine.AudioClip)
extern void MixedRealityInputSystem_RaiseDictationError_m4D6D7D671894C45AE99A588038011C6B57ACA4E3 (void);
// 0x00000156 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseHandJointsUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,System.Collections.Generic.IDictionary`2<Microsoft.MixedReality.Toolkit.Utilities.TrackedHandJoint,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>)
extern void MixedRealityInputSystem_RaiseHandJointsUpdated_mCB70F5D5D36EACBB740A0DB7523A47CE9BFA8287 (void);
// 0x00000157 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseHandMeshUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Utilities.Handedness,Microsoft.MixedReality.Toolkit.Input.HandMeshInfo)
extern void MixedRealityInputSystem_RaiseHandMeshUpdated_mBB14BD372CACE4C3F935ADF38B094372DFF1EEFE (void);
// 0x00000158 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchStarted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchStarted_m31F8B9A6432FF7FF49BE5240F4BFEAB1119543F5 (void);
// 0x00000159 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchCompleted(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchCompleted_mF2E008087005DD633BC4DE8335AAAE2615CF463A (void);
// 0x0000015A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::RaiseOnTouchUpdated(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputSource,Microsoft.MixedReality.Toolkit.Input.IMixedRealityController,Microsoft.MixedReality.Toolkit.Utilities.Handedness,UnityEngine.Vector3)
extern void MixedRealityInputSystem_RaiseOnTouchUpdated_m21115736390B2942D6FE10D01DC14582E48B57F8 (void);
// 0x0000015B Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules_Internal(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,T1[],T2)
// 0x0000015C Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Boolean)
extern void MixedRealityInputSystem_ProcessRules_m43D47333222387477DEA69AA06A592F41A7EEDB4 (void);
// 0x0000015D Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,System.Single)
extern void MixedRealityInputSystem_ProcessRules_m10BED0AC6AA97856ED9002859195F6C9F1B194C8 (void);
// 0x0000015E Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector2)
extern void MixedRealityInputSystem_ProcessRules_m721BEDE7DFB2DC0033C2A01F130F8FC941F25FB6 (void);
// 0x0000015F Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Vector3)
extern void MixedRealityInputSystem_ProcessRules_mC29CB020934185D1443DF2A122CFDF2811E21CC1 (void);
// 0x00000160 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,UnityEngine.Quaternion)
extern void MixedRealityInputSystem_ProcessRules_m281A6B109EC76F292804334464AA8553FC050566 (void);
// 0x00000161 Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::ProcessRules(Microsoft.MixedReality.Toolkit.Input.MixedRealityInputAction,Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose)
extern void MixedRealityInputSystem_ProcessRules_mEE97C73EE0969F54B5A76704901F37D2054064F0 (void);
// 0x00000162 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem::.cctor()
extern void MixedRealityInputSystem__cctor_m7D8EDD0CB0AA272B58EFAA0C8773ADB5DFECD9C1 (void);
// 0x00000163 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::.cctor()
extern void U3CU3Ec__cctor_m771A4DA2249305F1525AE289EDBB833D2E6924F9 (void);
// 0x00000164 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::.ctor()
extern void U3CU3Ec__ctor_m74645C2E43AB86BAEC2967695905AFC4C2A36CA8 (void);
// 0x00000165 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_0(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_0_mA3755D122CEBAF0237FC1C401E30AA485BAC071C (void);
// 0x00000166 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_1(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourceStateHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_1_mA0C8143EFB1A2E9512BD6BD53ACADF546FBCB064 (void);
// 0x00000167 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_2(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_2_mF96D25A53C65D204CC6C094A8C7E11D93E086A91 (void);
// 0x00000168 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_3(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_3_m212ED947CA62C4DD3F24491CCDD79B600B89A2CB (void);
// 0x00000169 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_4(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_4_m3EB511B2695D5C3E8A668BAEF3FC968EA6297915 (void);
// 0x0000016A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_5(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_5_m5E014B2B8103BE940752E21687799357D57D838B (void);
// 0x0000016B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_6(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySourcePoseHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_6_m845EFF6E20069692C5E86562D70296BCB622535B (void);
// 0x0000016C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_7(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_7_mFC815B9D207B1128E9859800F77194E65E77D7D4 (void);
// 0x0000016D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_8(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusChangedHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_8_m07B29A98D0DE44F9B5F757CDA2B84CE5A4399D3C (void);
// 0x0000016E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_9(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_9_m7D6349DE6429105F96809D61B579442BBFEC3147 (void);
// 0x0000016F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_10(Microsoft.MixedReality.Toolkit.Input.IMixedRealityFocusHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_10_m74A454EFADB6D63825A9491A807EFBEBD7D1E8C8 (void);
// 0x00000170 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_11(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_11_m788337DD5337450C51D20ADC80518E49224AC8A7 (void);
// 0x00000171 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_12(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_12_mB110D0BBFB1369A32965E687652D5544397FCEDD (void);
// 0x00000172 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_13(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_13_mB203739B4E23E76BDC67C3C77AB13C952BB43839 (void);
// 0x00000173 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_14(Microsoft.MixedReality.Toolkit.Input.IMixedRealityPointerHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_14_m8394321DA1A17E02B57A5C50CA600C3091173E8C (void);
// 0x00000174 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_15(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_15_m88FE26E306388AF861C1D3098826E30941B4FD36 (void);
// 0x00000175 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_16(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_16_mDB42315FBCF69F464617AF212523D102A4865D1C (void);
// 0x00000176 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_17(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_17_mCE9EEBF06706049CBDB0E9747FA7BAE5EE5C6582 (void);
// 0x00000177 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_18(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_18_m92689EB8A2C94BD493F07BC7B854D7A36CC48F72 (void);
// 0x00000178 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_19(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<System.Single>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_19_mC8B9A84C7E4B9FE1F22EA3B9190D49DE5F8204B0 (void);
// 0x00000179 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_20(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_20_mD546574BD757D63F403829B3B99DAB22AC2E1B4C (void);
// 0x0000017A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_21(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_21_m7E9BBA34D0F3E7DECDB680886266675A29A10A4C (void);
// 0x0000017B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_22(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_22_m08865D0DDE03169804D72BE5D7C5CFF995CF37ED (void);
// 0x0000017C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_23(Microsoft.MixedReality.Toolkit.Input.IMixedRealityInputHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_23_mBFFB133545A720F7F4A1CD99B01D9260A985B044 (void);
// 0x0000017D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_24(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_24_m64AF0238DA3950B52D6FE26C01DF609781F6168D (void);
// 0x0000017E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_25(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_25_mA9932B8B515C02DB3A540E95859780968DE12B05 (void);
// 0x0000017F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_26(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_26_m3F544FDCB8CB7974FB180EA2D5280905AF74679A (void);
// 0x00000180 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_27(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_27_m90A1F3B54E88CBF6D1402531C368B16D3AD44572 (void);
// 0x00000181 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_28(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_28_mDFAC0BD76BD41A2ADB85387427A64CF9744761CE (void);
// 0x00000182 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_29(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_29_m87D3D0982E96F42B3020B08D9CF77793F1ADD458 (void);
// 0x00000183 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_30(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_30_mF2C37CA179EE6F1243B0B6E565B5BFFB31B9EF82 (void);
// 0x00000184 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_31(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_31_m6FCCF8DCA98F430317268D6BBAB43A09F2E7644F (void);
// 0x00000185 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_32(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_32_m2F9C64F18F1B188CDF62F38F0AB2C6B86060B643 (void);
// 0x00000186 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_33(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector2>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_33_mDA7B33F0909044F091311C83757D701E6914C5AA (void);
// 0x00000187 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_34(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Vector3>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_34_m4E0461608918A474E0BEAD2366838027CE079E07 (void);
// 0x00000188 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_35(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<UnityEngine.Quaternion>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_35_m1F417F8CCA5B63485A2738EC0B3659D9934D0CFC (void);
// 0x00000189 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_36(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler`1<Microsoft.MixedReality.Toolkit.Utilities.MixedRealityPose>,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_36_mB9B35DD6354085A44098E9BE25D0862E525D3695 (void);
// 0x0000018A System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_37(Microsoft.MixedReality.Toolkit.Input.IMixedRealityGestureHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_37_mD4240DC40D5BACC7CF433E04926963AA5AF4CCE8 (void);
// 0x0000018B System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_38(Microsoft.MixedReality.Toolkit.Input.IMixedRealitySpeechHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_38_m3BD9A0BB73A976ECE1B81A897F90E56BCB0C9001 (void);
// 0x0000018C System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_39(Microsoft.MixedReality.Toolkit.Input.IMixedRealityBaseInputHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_39_m194D80054AADB4425C49634AA62E8E1A04610316 (void);
// 0x0000018D System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_40(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_40_mE448200A7BC3E722C1B2BEF519D9B40EE252E5E6 (void);
// 0x0000018E System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_41(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_41_m262485B179BC605B22CC2D9805F8AC68A2172735 (void);
// 0x0000018F System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_42(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_42_mD25DBC098C6002B497DCB187B63FFA4175361300 (void);
// 0x00000190 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_43(Microsoft.MixedReality.Toolkit.Input.IMixedRealityDictationHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_43_m5E2134FC85DC707AC77D9725E87E57B7CDFA5423 (void);
// 0x00000191 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_44(Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandJointHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_44_m5DE8F833D52BA5E0728FC5FD6D6F8B5B4C6C032A (void);
// 0x00000192 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_45(Microsoft.MixedReality.Toolkit.Input.IMixedRealityHandMeshHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_45_mEC14C6245C596B126D0EBC69D313B9BA043A9B94 (void);
// 0x00000193 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_46(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_46_m0216C66A083BE3E34D0ADD479115C2537913CEF2 (void);
// 0x00000194 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_47(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_47_m7C30E2F8E963D23C9C688643C4C471B25D8BE677 (void);
// 0x00000195 System.Void Microsoft.MixedReality.Toolkit.Input.MixedRealityInputSystem/<>c::<.cctor>b__244_48(Microsoft.MixedReality.Toolkit.Input.IMixedRealityTouchHandler,UnityEngine.EventSystems.BaseEventData)
extern void U3CU3Ec_U3C_cctorU3Eb__244_48_m55E06A3037AB37237AF8E9D092CBC77EFD02A90A (void);
// 0x00000196 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionGrabbable::OnEnable()
extern void NearInteractionGrabbable_OnEnable_m6BD888F5740998A3DBF0D1206226312E41CC74DB (void);
// 0x00000197 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionGrabbable::.ctor()
extern void NearInteractionGrabbable__ctor_mC84A2C0C7C123225580581C4EED89CAAB036F82A (void);
// 0x00000198 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalForward()
extern void NearInteractionTouchable_get_LocalForward_m86E478F879E7599F3A1AF58541A2658C3B5B3EBF (void);
// 0x00000199 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalUp()
extern void NearInteractionTouchable_get_LocalUp_m9B7264306F50A2F0811BBDD78C2B1E63D1481AEF (void);
// 0x0000019A System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_AreLocalVectorsOrthogonal()
extern void NearInteractionTouchable_get_AreLocalVectorsOrthogonal_m6D41D1C67BC4AD6E6B47744BB5356F8F412C96F0 (void);
// 0x0000019B UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalCenter()
extern void NearInteractionTouchable_get_LocalCenter_m976B453D9975C12A49558CD22523F99799192F12 (void);
// 0x0000019C UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalRight()
extern void NearInteractionTouchable_get_LocalRight_m9CCDCFE2BDA1E347C87A3DBC137345522478A251 (void);
// 0x0000019D UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_Forward()
extern void NearInteractionTouchable_get_Forward_m985A9312873DCE273BB794E58980F11E3FB81857 (void);
// 0x0000019E UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_LocalPressDirection()
extern void NearInteractionTouchable_get_LocalPressDirection_m4923B9E3AF24F386FFAD364B9DCB162E708DDDC7 (void);
// 0x0000019F UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_Bounds()
extern void NearInteractionTouchable_get_Bounds_m541A8675CC3CE89D1FAC8982BA7D82F61F077F2A (void);
// 0x000001A0 System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_ColliderEnabled()
extern void NearInteractionTouchable_get_ColliderEnabled_m86F6D91D5FA2B2A1B3821FDAF8A4FB4CB2C22DC9 (void);
// 0x000001A1 UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::get_TouchableCollider()
extern void NearInteractionTouchable_get_TouchableCollider_m8C691DAB535F4EC8019ABEF7332E0C038C5860BE (void);
// 0x000001A2 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::OnValidate()
extern void NearInteractionTouchable_OnValidate_mE8AB69CC53E13E48CABBC84632A3BD2704144A0E (void);
// 0x000001A3 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::OnEnable()
extern void NearInteractionTouchable_OnEnable_mD1C3B884E74A9F68183E1A7888D45A993F0A4E81 (void);
// 0x000001A4 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalForward(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalForward_m86D68108392905F99DD8BCDE038A3FFC5575A0B1 (void);
// 0x000001A5 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalUp(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalUp_m14D2C4679ADB1BBBA47B25954A010D2C61A093CC (void);
// 0x000001A6 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetLocalCenter(UnityEngine.Vector3)
extern void NearInteractionTouchable_SetLocalCenter_m2B22E8B5947724B03B2D0A284A5AE86FEE88E16F (void);
// 0x000001A7 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetBounds(UnityEngine.Vector2)
extern void NearInteractionTouchable_SetBounds_m66FDC7208B00EC3BE2BCB86AF6B164E97D1E4046 (void);
// 0x000001A8 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::SetTouchableCollider(UnityEngine.BoxCollider)
extern void NearInteractionTouchable_SetTouchableCollider_m74BCAFD4DEC707BDE4AFBED7E56A00FE0FE402F4 (void);
// 0x000001A9 System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchable_DistanceToTouchable_mD0AB5721A5D2A485068C974AB964CDEDD8FDAA5A (void);
// 0x000001AA System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable::.ctor()
extern void NearInteractionTouchable__ctor_m639B63E25291306E89264D1DF898CDF21DB80354 (void);
// 0x000001AB System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::.cctor()
extern void U3CU3Ec__cctor_mC186AE7BA047B178D55FEB5EC110ABF5F6A44556 (void);
// 0x000001AC System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::.ctor()
extern void U3CU3Ec__ctor_m4A704E423DAE1FB82FE869DE25DCE8D7D085D84D (void);
// 0x000001AD System.String Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchable/<>c::<OnValidate>b__25_0(System.String,UnityEngine.Transform)
extern void U3CU3Ec_U3COnValidateU3Eb__25_0_mA4C3DAAD1DA22A984F7C83EF08B3EDD39DC9EB97 (void);
// 0x000001AE UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_LocalCenter()
// 0x000001AF UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_LocalPressDirection()
// 0x000001B0 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::get_Bounds()
// 0x000001B1 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableSurface::.ctor()
extern void NearInteractionTouchableSurface__ctor_mB50660F4487750D3B291092A908F031638D71C73 (void);
// 0x000001B2 System.Collections.Generic.IReadOnlyList`1<Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI> Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_Instances()
extern void NearInteractionTouchableUnityUI_get_Instances_mCC93A7E33281DB6A5081C52754BB1D860A9CB9BB (void);
// 0x000001B3 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_LocalCenter()
extern void NearInteractionTouchableUnityUI_get_LocalCenter_m3C322AB7048D2455E69C0B82B6F638324FA1AE78 (void);
// 0x000001B4 UnityEngine.Vector3 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_LocalPressDirection()
extern void NearInteractionTouchableUnityUI_get_LocalPressDirection_m7D1DA44E4869290C281FB34DFDCA57E497FE7113 (void);
// 0x000001B5 UnityEngine.Vector2 Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::get_Bounds()
extern void NearInteractionTouchableUnityUI_get_Bounds_m5634AF6D6BA73D4705F744B7A08F61BB316BC090 (void);
// 0x000001B6 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::.ctor()
extern void NearInteractionTouchableUnityUI__ctor_m3C4CE86E6251A4608661684052DA83E55C29EB0F (void);
// 0x000001B7 System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchableUnityUI_DistanceToTouchable_mF2FD9214210F7827DEA34E6ECE0DC95DDA85EA27 (void);
// 0x000001B8 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::OnEnable()
extern void NearInteractionTouchableUnityUI_OnEnable_mE5340149950F7D67EA64847A150AEA31FC3E3446 (void);
// 0x000001B9 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::OnDisable()
extern void NearInteractionTouchableUnityUI_OnDisable_m1C13E7C481574267A5F8F590312657EEC2AC01FD (void);
// 0x000001BA System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableUnityUI::.cctor()
extern void NearInteractionTouchableUnityUI__cctor_m62D51540081D0EA7CEECD85DBF58E8799BE8D1AE (void);
// 0x000001BB System.Boolean Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::get_ColliderEnabled()
extern void NearInteractionTouchableVolume_get_ColliderEnabled_m7DCDF2BF6352EF929D646CA2FA53ADCFE27F9CD5 (void);
// 0x000001BC UnityEngine.Collider Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::get_TouchableCollider()
extern void NearInteractionTouchableVolume_get_TouchableCollider_mBB082E327DBB122DEA40C1DB23A4D749C1B77678 (void);
// 0x000001BD System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::OnValidate()
extern void NearInteractionTouchableVolume_OnValidate_mE3FA110E040936F3D6B9463BBCD8C8BA6985FD84 (void);
// 0x000001BE System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::Awake()
extern void NearInteractionTouchableVolume_Awake_m74D580E4AC52E5BA0FAB1C57CB2FA7BFBE250031 (void);
// 0x000001BF System.Single Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::DistanceToTouchable(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void NearInteractionTouchableVolume_DistanceToTouchable_m791833773FE073D3EC94C56C50AFA98F39248752 (void);
// 0x000001C0 System.Void Microsoft.MixedReality.Toolkit.Input.NearInteractionTouchableVolume::.ctor()
extern void NearInteractionTouchableVolume__ctor_mB35E0A03F0AE9830DD2FE02A9C6C8BA4C0E36EED (void);
// 0x000001C1 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerClicked(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerClicked_m120E5ADB08750D9CB0BF284A1C6EC37CFCC00FD9 (void);
// 0x000001C2 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerDown(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerDown_m257B9AAABF3085BC2F30EF7B4B67C6B4F2E60F77 (void);
// 0x000001C3 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerDragged(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerDragged_m1DDBF479602D945609760F4BC003CE6942032973 (void);
// 0x000001C4 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::OnPointerUp(Microsoft.MixedReality.Toolkit.Input.MixedRealityPointerEventData)
extern void CanvasUtility_OnPointerUp_mFF401C7213758DEB0C181B1DBD555BAC066A0CB5 (void);
// 0x000001C5 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::Start()
extern void CanvasUtility_Start_m6EC1BD60BD2A72F07F685B69D61CF8E900855976 (void);
// 0x000001C6 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::VerifyCanvasConfiguration()
extern void CanvasUtility_VerifyCanvasConfiguration_mD596D87BE9BD562AC661DDA4E6CEE468210A5B33 (void);
// 0x000001C7 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.CanvasUtility::.ctor()
extern void CanvasUtility__ctor_m9B9F4B172EEE5CB377E0BAC98FD2C093F51C8A56 (void);
// 0x000001C8 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::Awake()
extern void ScaleMeshEffect_Awake_m762B187E4FC278DD10BE9923A4724C326E3608A5 (void);
// 0x000001C9 System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::ModifyMesh(UnityEngine.UI.VertexHelper)
extern void ScaleMeshEffect_ModifyMesh_m86443B9BE01622A6186BC8310D18F3A1A965C6DB (void);
// 0x000001CA System.Void Microsoft.MixedReality.Toolkit.Input.Utilities.ScaleMeshEffect::.ctor()
extern void ScaleMeshEffect__ctor_m82EE405A6E2CCFE66126E9FCF9E581EB78A24FE9 (void);
static Il2CppMethodPointer s_methodPointers[458] = 
{
	BaseNearInteractionTouchable_get_EventsToReceive_mB5B553A9DE36821014C899F43A95F0E4028EB680,
	BaseNearInteractionTouchable_set_EventsToReceive_mE4D1B04CB1553F017E983603675CE032E56FEC67,
	BaseNearInteractionTouchable_get_DebounceThreshold_mE2D05599BB1C0F3F8223815D0F17DBD4EC802AB8,
	BaseNearInteractionTouchable_set_DebounceThreshold_mA98D8F0596799E0FBBCA6F69E45BF411EF45B448,
	BaseNearInteractionTouchable_OnValidate_m25F579431912764BCC39D904970A0A5C48E8ECEA,
	NULL,
	BaseNearInteractionTouchable__ctor_mD8C43CE15662BF13262557BF5B0DC7544163CAF3,
	ColliderNearInteractionTouchable_get_ColliderEnabled_mDFE621376A64A1352963B70EDA4C73BCC2D126D6,
	ColliderNearInteractionTouchable_get_TouchableCollider_mE24DBF9F2050F8AB28021B14DC276E0BD8A7D0B0,
	ColliderNearInteractionTouchable_OnValidate_m5DF1C94479D2B6D1AAEF1BE9AFD8554E26C58755,
	ColliderNearInteractionTouchable__ctor_m4C3DF8380729A8D55F43C66E90743A8331B5F294,
	DefaultRaycastProvider__ctor_mC1E50211F7AF6A6A974822B6EB186E3152C082BD,
	DefaultRaycastProvider__ctor_m553DA09CDC38B337B1742DC56786AD6A4E3B91AF,
	DefaultRaycastProvider_get_Name_m25B985271868249C0E2A576D9D747BEFE4502ECA,
	DefaultRaycastProvider_set_Name_m60156C8D6337989DB5057226D6B9DED4DE02323D,
	DefaultRaycastProvider_Raycast_m74A8C5EC6A95621398833CC19DEF22F3AE35DB1C,
	DefaultRaycastProvider_SphereCast_m1B1F964942780B34EE6A84271CCAD84149401DA4,
	DefaultRaycastProvider_GraphicsRaycast_m701AC2BB5BDC5D49A15BB91786C2F59410B84E0D,
	DefaultRaycastProvider__cctor_m2AD8BDD488A3C7D39BFA15FB883351E35ED374FD,
	FocusProvider__ctor_mC3CA5F53AFD742D574F83834C2EE477AF37EC1DC,
	FocusProvider__ctor_mF8376A75A805AD30EBE2CAC7AB344F9D378A37EE,
	FocusProvider_get_PointerMediators_m58C5197F16241C03EEE928D0903BAAF4531E3D96,
	FocusProvider_get_NumNearPointersActive_m5D4AF993F127A31AB6AF2A5AF5DFDB1B964A3959,
	FocusProvider_set_NumNearPointersActive_mF09BE13FA43F0C12C04664E80ABBB111EED40001,
	FocusProvider_get_NumFarPointersActive_m510329AF6025C9BD7FEDA408C37A1380AFD81580,
	FocusProvider_set_NumFarPointersActive_m0B4DBDBB611083D956D945543B463B69CFEC2E48,
	FocusProvider_get_PrimaryPointer_mCDD5E7137364EA5A7F87717B90B2DCB582EDCD74,
	FocusProvider_set_PrimaryPointer_m024C0990196E8F300CCF6AC37B9783B7D1FB6F69,
	FocusProvider_get_Name_m1F7BD4449315E2B13BB43C653319F525196F7FED,
	FocusProvider_set_Name_mC61D39511CFBC94599DDFD520D8F31DFE3888149,
	FocusProvider_get_Priority_mF6A3658270693D12C1F7DF37A8481FF7D1E3B428,
	FocusProvider_Microsoft_MixedReality_Toolkit_Input_IMixedRealityFocusProvider_get_GlobalPointingExtent_m6E24526E9128B2D870BD72BCAFEC719A475ED45E,
	FocusProvider_get_FocusLayerMasks_m4FADE8C5F8095AB00C1E7C797D2CA11FF2069B73,
	FocusProvider_get_UIRaycastCamera_mB9403CB90269408831F5C3585554B2A190358855,
	FocusProvider_get_IsSetupValid_m849FD0D6E39B0F2353BBEC299A1124098258FE46,
	FocusProvider_add_PrimaryPointerChanged_m2A7603CE746B0BF7EEF04EAD7492F7F8506BD21A,
	FocusProvider_remove_PrimaryPointerChanged_m6488B2D2062E46C386871256FA51ADA64DAFD83F,
	FocusProvider_Initialize_mBFD63C9C05E39FC3AD3BEB8229FEE710E05AAFE0,
	FocusProvider_Destroy_m2DC05D105D94D8241153E20C9E0999954C057F85,
	FocusProvider_Update_mA74FD1C41EC663923ACFE851BC8B23F6E73A51CD,
	FocusProvider_UpdateGazeProvider_mA0ADD568FED58E52F3C151E7E3DFE5A93512B35C,
	FocusProvider_GetFocusedObject_m765FD16DB747BBA3A4E3E3EC50763D602BB76CEE,
	FocusProvider_TryGetFocusDetails_mBC5AE387E5FA255104CF464981A222BEC124E387,
	FocusProvider_TryOverrideFocusDetails_m6739BEF276FA52AE290C1C5F7BCCB8F6F0EF22E3,
	FocusProvider_GenerateNewPointerId_m3CF18510AF9D7FB092C4DDDF729DF086AD364544,
	FocusProvider_FindOrCreateUiRaycastCamera_mFB9FBE76B5133DBB0408378BACD129DDC7FA70BD,
	FocusProvider_CleanUpUiRaycastCamera_m3F102E1D0B47CF2E135AEF32CF6B6BEBDB502D30,
	FocusProvider_IsPointerRegistered_m42374206026EFA3F51CDDBF05EAF0D222A910D18,
	FocusProvider_RegisterPointer_m2459E3903FB2A1C31A01620DB5ABB21C0194F2CB,
	FocusProvider_RegisterPointers_m00DB699D1C7003591201A417BC9B614004A15214,
	FocusProvider_UnregisterPointer_mF801BFB37423BB951BC8E182277CB006D45A6DB6,
	NULL,
	FocusProvider_SubscribeToPrimaryPointerChanged_m8E7E1578350E6239A2EDD736CC2133670D74AC8F,
	FocusProvider_UnsubscribeFromPrimaryPointerChanged_m87D0BA93421DAED9704F4982617F642A6CB53544,
	FocusProvider_TryGetPointerData_m7C7001AA325A58B037A3E422B1BF89005B100013,
	FocusProvider_UpdatePointers_m6DE825ECB6D688860F203BC8E0BBADFF5C4DCFCE,
	FocusProvider_UpdatePointer_mD8071484F6B61D6C9C9081F22B647570AE7B9BD2,
	FocusProvider_TruncatePointerRayToHit_mE95478CE04B4CDEA695104C7D1C7ECFE2192CA00,
	FocusProvider_GetPrioritizedHitResult_mE7582A24B96DE363C849E1CC8DC9C81EE8CE6F26,
	FocusProvider_ReconcilePointers_m5E7FF5569B45470E54A823D9590B6A9C2CA54392,
	FocusProvider_QueryScene_mC3896F3D261452CD8EDB1CDDC46F6A979EDE654F,
	FocusProvider_RaycastGraphics_mB28C4CE781E332377185569C3C855268180818A3,
	FocusProvider_RaycastGraphicsStep_mC0951DDAC7638FBDBF01279B30F509380681CEFF,
	FocusProvider_UpdateFocusedObjects_mBDD26DB46012EC374BED29D8580C505B9C95627D,
	FocusProvider_OnSourceDetected_m095C53CCA627FA851E844C97A304D0BD49C37A16,
	FocusProvider_OnSourceLost_mC752025A98521FC73E80254EA549D13DC27666C0,
	FocusProvider_OnSpeechKeywordRecognized_mB004898A1C9F9A0A620A36DD8683D47AFBE971A8,
	FocusProvider_GetPointerBehavior_mD1FF836CC81F9C457BCBC6657F3E0CB2FF816590,
	NULL,
	FocusProvider_GetPointerBehavior_mE1784DFDF228550A41AEB2141B76B352D152872D,
	FocusProvider_get_GazePointerBehavior_mC7F25D0CD9744A1E5EA94F78350803147C08A824,
	FocusProvider_set_GazePointerBehavior_m6DCEA9A6DA3A5F687F5BC498BFD860587DBA6CAC,
	NULL,
	FocusProvider__cctor_mF7C24DEBF9C9684D4D24B9BC68C0AF2D1EDB6667,
	PointerHitResult_Clear_m65E7D57BB2CC7B58E891453EA25E243F6373299C,
	PointerHitResult_Set_mA01A8914F89FFFBB28D942F9843FF004A8C7546D,
	PointerHitResult_Set_m62A8873803D3648CDCFC76A1EE05040D5C015B1B,
	PointerHitResult_Set_mC867FA296A2A9E9BB02542D0292A74F9FDF5CB64,
	PointerHitResult__ctor_m2969F05CF88559B2D790D738CAD8976FCAB3BCAC,
	PointerData_get_StartPoint_mA79720649FBEED0E7154B2D36972956819B1FFE6,
	PointerData_set_StartPoint_m88D3B6E1E7F684DFC12795D8ECD04CA8CE986EF4,
	PointerData_get_Details_m274AEF840675491ACAC004258F57F182082E7EFF,
	PointerData_set_Details_mFC3684738BFD1D3B884B25B0D186AB1988FA5D8B,
	PointerData_get_CurrentPointerTarget_mD7E9739FB5757AD9E48D8D0DCF6188187A6DFDAD,
	PointerData_get_PreviousPointerTarget_mCD723B519857A6578C3893061F6681F8C5688F07,
	PointerData_set_PreviousPointerTarget_m1E2891C5039FEFEAEA476354667B81A57D64D4A8,
	PointerData_get_RayStepIndex_m52E3A321928E81AB7380544CB9FF4684C8F4990D,
	PointerData_set_RayStepIndex_mBD478BA3D7A7B36D461659E2613F22370EBDE1D2,
	PointerData_get_GraphicEventData_mA5D09FDB5B8253D24F7D6741D82912AFD4D14417,
	PointerData_get_IsCurrentPointerTargetInvalid_m589A482DF5EE0385396CDA0D84B2EAB1A9A2F3F4,
	PointerData__ctor_m5D7C5088ADFC382D87DFCC7823AACE51B751CD28,
	PointerData_UpdateHit_mE246A47E2825D0D4352921DD356689815D370DC2,
	PointerData_UpdateFocusLockedHit_m95DEA933A07095CAB7539C4C520F1F793DE93915,
	PointerData_ResetFocusedObjects_mCA76F9AF47E6DD129200F7874708A80D173C090F,
	PointerData_Equals_m21A9B6F3A7531A823EB992FB051C6AB6053C201C,
	PointerData_Equals_m626680D9C107092D19059B8CCD07304F8A511EAE,
	PointerData_GetHashCode_m57E5E175D58E8B2B1604C35DD47D95DC9EF1751F,
	PointerData__cctor_mA64CE47C0008B55EEC241C648A2BAF5C243B73AC,
	PointerPreferences_Matches_mEFA09DCD4F4C18AEA6BFEE35943880F954CE95BE,
	PointerPreferences_Matches_mC3A45FEC4B516EE7A732C865E3A6B53C18BE77CF,
	PointerPreferences_GetBehaviorForHandedness_m3CE5F919E106176E913B8949C6C18BAD705AF8BF,
	PointerPreferences_SetBehaviorForHandedness_mF0171F288F892411DEF1227EA53BAE5D96334186,
	PointerPreferences__ctor_m87FF89FA760344D5C0708EA896E326DCB21A7DA8,
	GazePointerVisibilityStateMachine_get_IsGazePointerActive_mDB3A3A876A0A33642C642C58E50A7BDE7AD77D43,
	GazePointerVisibilityStateMachine_UpdateState_m99F62548610C88B73BB710EAF96E0FB20A752C5E,
	GazePointerVisibilityStateMachine_OnSpeechKeywordRecognized_m4636C6D52AD7B13111A2CAF63C71F5CE266B1933,
	GazePointerVisibilityStateMachine__ctor_m1E58B319AD53C938BF2DD94CC5DDBC01B37C1A04,
	GazeProvider_get_Enabled_m5605B7B706ECBE3F0FF575DDE89ED8DFF21CCB0F,
	GazeProvider_set_Enabled_mBD9867CD6073B7CC4708208754ED84400A3D469A,
	GazeProvider_get_GazeInputSource_m3F65624E14FC447C228A9872193F47A770FD9F57,
	GazeProvider_get_GazePointer_mA99FD93F398043EDCBABAFE2F7EA43701D0909F9,
	GazeProvider_get_GazeCursorPrefab_mAEBA4DBE5D658431964515890FD175247194E913,
	GazeProvider_set_GazeCursorPrefab_m9D2740963E36C88C6F211B313D0DA873C19D59C0,
	GazeProvider_get_GazeCursor_mD08ED2163FDCB1DC14C1DECEFDE03682EF761676,
	GazeProvider_get_GazeTarget_mDA3F9116A842036FBE140696D5927EAE8FEEFA36,
	GazeProvider_set_GazeTarget_m1C27FA43117A639DEF3D1294A442E84F4EA8A13B,
	GazeProvider_get_HitInfo_m72895D36507BD10C9C6757669DA3A563346FB810,
	GazeProvider_set_HitInfo_m3B50695F2997BD48F855ADDBA28EE54C41B6D739,
	GazeProvider_get_HitPosition_mDB9E014C5EC5CCA268D03B3E21453469F47A3FA8,
	GazeProvider_set_HitPosition_mA450278AA4FA80ECD7DABADD3756B262C4F3F644,
	GazeProvider_get_HitNormal_m49856DD31AE415BF477DDD88612E0EDFDA4021B8,
	GazeProvider_set_HitNormal_m13E984AD955A94422CCB35A9F7CFDAE774EC08E9,
	GazeProvider_get_GazeOrigin_m1052CFC32E815575B91513918A83FC761F68B41D,
	GazeProvider_get_GazeDirection_m9D54A34137DB085651777DF01312858B431DAE90,
	GazeProvider_get_HeadVelocity_m9DDC7BD166B5C39931CB874820178268001444F3,
	GazeProvider_set_HeadVelocity_m0A0B6C15A3A3365F20BFF17D414C4525FD52CC63,
	GazeProvider_get_HeadMovementDirection_m6D07D080739CA2A29DE6C9CE3DC807549EAD381C,
	GazeProvider_set_HeadMovementDirection_mF952765DCEF4AD2F5BE4FE01C5F53F498EA87ED0,
	GazeProvider_get_GameObjectReference_m4FEFF5737F7C9E8B5C5958E4B7CD73D7C1F95980,
	GazeProvider_OnValidate_m5C6A81452FA52EE348F3082664031E37838655D0,
	GazeProvider_OnEnable_m8CD70E28586DEF71CCA41C4A87F0060CA1D9867F,
	GazeProvider_Start_m459314DA5DF1EBE72514BD7F22633531879AED34,
	GazeProvider_Update_mD7FE6ED0C1D13EF59D1B5C11C66ED30AAE6FCF36,
	GazeProvider_LateUpdate_m42197509EE8100D45DFDF4249EFA33D7EA8CE0DF,
	GazeProvider_OnDisable_m0CFD6F22CEBD964DA901F040E15B8CD178870426,
	GazeProvider_OnDestroy_m5B9B385A4330ECD44B17C2BF5B8566C00DF58C13,
	GazeProvider_RegisterHandlers_m1623D5A98E1B56C7D259090FC8FB1BD3365882FC,
	GazeProvider_UnregisterHandlers_m09B3D5EF74D353D4330F2276F6CAF8234971DEA4,
	GazeProvider_OnInputUp_m9ED2D5B11C0C99E77B8ADBAB7028C224B7126C51,
	GazeProvider_OnInputDown_m61523A8F155B6EFCD9BF89B481848A51F4BB4956,
	GazeProvider_InitializeGazePointer_m3146361F209E4ABFC64FF645E02A45E30A4F4D60,
	GazeProvider_RaiseSourceDetected_mAAD73AB41F11E4A00171EA471760E22DEEDAADDF,
	GazeProvider_UpdateGazeInfoFromHit_m78539F2571F9429FF4C28D031DE6D0165DAD62EC,
	GazeProvider_SetGazeCursor_m9B9E2EA50877B1C2C40F9E0613413B42B9EC2FA2,
	GazeProvider_get_IsEyeTrackingEnabledAndValid_mEDD77AAF2E69D3D1A7513503B65CD3600A72BBCE,
	GazeProvider_get_IsEyeTrackingDataValid_m228B0EEA2AFC00CD38811B243986B4274FC30E12,
	GazeProvider_get_IsEyeCalibrationValid_mD17079906D6F20A1FF09DAA4048EBAF69F4D7E12,
	GazeProvider_set_IsEyeCalibrationValid_mC0610D2969C94AF37C1F2D365BE5AC0D20139E9E,
	GazeProvider_get_LatestEyeGaze_mCE05B51A8BE8D33C6837D55D536E45C603E91227,
	GazeProvider_set_LatestEyeGaze_m5DC5298D9938AB65390A73189E045872F3298A53,
	GazeProvider_get_IsEyeTrackingEnabled_m22EF502BCCFC3555C14085BDFF647EF79E1697A0,
	GazeProvider_set_IsEyeTrackingEnabled_mB46D0CD0863B4830F195F091D163B3AD913515CF,
	GazeProvider_get_Timestamp_mDD1B82FDE4B2751D9A1467C6E3AF78AC0F0FAA5C,
	GazeProvider_set_Timestamp_mF2B93EC13DF7CB76BB027249A02520B7FA96BA9B,
	GazeProvider_UpdateEyeGaze_mB13AECA022A3D702DE8CB661E6CF0C238E2E4135,
	GazeProvider_UpdateEyeTrackingStatus_m4CE440CBCB518F06D922E4AF52029828F45C6EC1,
	GazeProvider_get_UseHeadGazeOverride_mC27BCB7D7064EA1A37D72C8792F9BB61C9FDFEE3,
	GazeProvider_set_UseHeadGazeOverride_m51BADBDED84B28AE53562B63DDC66063AF779155,
	GazeProvider_OverrideHeadGaze_m7F7CBD41177338C10F105143854EB59BD6F042CC,
	GazeProvider__ctor_mE9B3AE59852A6EBAFDB8A627642D9E5FBE3C860A,
	GazeProvider__cctor_m94A42F863CFCB4450543B204849A07179F1C4B93,
	GazeProvider_U3CU3En__0_m34FE6CDC2E03B8CA09B19E9DDB4DDBCA99B0CECF,
	InternalGazePointer__ctor_m680412F643BC782274829D6F375A2EB4CF7FAEFB,
	InternalGazePointer_get_Controller_m78B60A083FFED497C7D0A6B7C845BF508E173B46,
	InternalGazePointer_set_Controller_mC2591D136366A7EFB9395E23031EC7236BAA7C77,
	InternalGazePointer_get_InputSourceParent_m57739B769D5A231D9140027B75923A7003B21570,
	InternalGazePointer_set_InputSourceParent_mAB00A293722D0AE97A327EF54E2B641915605970,
	InternalGazePointer_get_PointerExtent_m19521E1F33E91063E6F2B1A15FCC07B1AF1A4AA4,
	InternalGazePointer_set_PointerExtent_m8F521DB5DA36F1EAF234D8E22ED233B772D03E7E,
	InternalGazePointer_SetGazeInputSourceParent_mCB5A8EE32EDCBFF195B3CE7FEC4DED21E67D0F81,
	InternalGazePointer_OnPreSceneQuery_m31F124A679394D3977B3B62804C837E7A98AB34F,
	InternalGazePointer_OnPostSceneQuery_m729B66073B1009A01591BF5D234829643503753B,
	InternalGazePointer_OnPreCurrentPointerTargetChange_m161A151572636D47E7F10C5BCCB5A030EA75B5D5,
	InternalGazePointer_get_Position_m6A3D45B7D8FB14224E4F1A163CFE828518F339FB,
	InternalGazePointer_get_Rotation_m2550C4B0C860F40DC41AAB3D3C925C784FCB694F,
	InternalGazePointer_Reset_m03144D86503BB7EB13B7213C9A7BC50B4401E4C9,
	InternalGazePointer_RaisePointerDown_m0C96AF6A0FE0984F61F700861D417E8F967991E8,
	InternalGazePointer_RaisePointerUp_m54989331B044CD68F0103C64ED7F32DB787B3AA5,
	InternalGazePointer__cctor_mB66A39E776E804DD53FF5E0F78BC0DEAB9D18FFA,
	U3CStartU3Ed__63_MoveNext_m1995BBF055EAF141D9C17374D7FBF5D9D5B55264,
	U3CStartU3Ed__63_SetStateMachine_mFFE24F4AA58DA0BEDA1BFEEC5C07DAA9C8ACAF9F,
	U3CRaiseSourceDetectedU3Ed__77_MoveNext_m3886E54E9E5F0455475EAEB0FA3D918761126E10,
	U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_mAD25DF053CFA407698925AC737602775ED4533DF,
	InputSystemGlobalHandlerListener_OnEnable_m8200CAF3CC35EF645C377C90A46B5D3017BF473F,
	InputSystemGlobalHandlerListener_Start_m4CFB72FB90D531AA0A51DFC41298CD191680C323,
	InputSystemGlobalHandlerListener_OnDisable_m114FE851191C66A6ADE6747FA889F9D4172692C6,
	InputSystemGlobalHandlerListener_EnsureInputSystemValid_mC1EEA526C7E1F622598A7F24D17B3429DB8FEFB3,
	NULL,
	NULL,
	InputSystemGlobalHandlerListener__ctor_mBEB7175C2BC2EB90F213EA40D74DB0BFBA6FA2D8,
	U3CStartU3Ed__2_MoveNext_mB3603C3B8DE4E20F9719C586BA61319384ABDFDF,
	U3CStartU3Ed__2_SetStateMachine_m962AD6827A53A07A77D01A0AB80D1C88D0FA386B,
	U3CU3Ec__cctor_m292ABBAD09754BE86C8855C184C7ABAAF4520D3D,
	U3CU3Ec__ctor_m5E32E12A8D7AF47F6646E6B1EE5DA01DA81B3737,
	U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_mF222F9635041A8AA0E5A52AD110BFD468C1CE7AB,
	U3CEnsureInputSystemValidU3Ed__4_MoveNext_m8A3DFB7EAF3583E06AE2B60C5C8FFF11770F0C89,
	U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m51E0D646F26D314B94CDA9CCD82EC8D2D3886A5F,
	InputSystemGlobalListener_OnEnable_m5E20C375720C55B61DA5501DFD4F450299CE4E97,
	InputSystemGlobalListener_Start_m2354BECA6B386CEAF9EFD9FC7B66B7CD43005E39,
	InputSystemGlobalListener_OnDisable_m8F9AEFD92EA6D1E927FA83ABDB977890B3EBB2F1,
	InputSystemGlobalListener_EnsureInputSystemValid_m7C298EE36539299287A924FEFF9F908479292D13,
	InputSystemGlobalListener__ctor_m2C849D4D8D4AEF19D17426FE71AD24FD37A5ABD0,
	U3CStartU3Ed__2_MoveNext_m4E247D7B009018AEB60C476724746C6F6B919784,
	U3CStartU3Ed__2_SetStateMachine_m25C1F2CBE76178EDD5E7B22BDB2F6654A9B95CCF,
	U3CU3Ec__cctor_m39F7C09BB7286EDB8E062F761998F7414A8D9DE9,
	U3CU3Ec__ctor_mF10A728643D63C40A89F8CF6596DECC145E969B4,
	U3CU3Ec_U3CEnsureInputSystemValidU3Eb__4_0_m2FD2AE39CC084CF42E5359EB5DFA0E0DC479E8D3,
	U3CEnsureInputSystemValidU3Ed__4_MoveNext_m7B0D5FF107CDE4A70864EA5F6D6433CB8344411E,
	U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_mA88C5F770AE0D475E0137FC57E9CA4464D54293D,
	MixedRealityInputModule_get_RaycastCamera_m329925BD02AF4BA852B7324C9F9423A23F89F634,
	MixedRealityInputModule_set_RaycastCamera_mCF7DFA3F997E6BC9F379ECA58285E9CD28EA19C7,
	MixedRealityInputModule_get_ManualInitializationRequired_m65BD22FAE18CAEDE539BBA6CB8B9A85B35906FEE,
	MixedRealityInputModule_set_ManualInitializationRequired_m0E808095BAE7F2F9C1C10C78349DE1724A1FEC72,
	MixedRealityInputModule_get_ProcessPaused_m4FE434E199953C439425AAD2AB065CE835712188,
	MixedRealityInputModule_set_ProcessPaused_mC2EF7A900830146EFDACB2078E3DCFA082997950,
	MixedRealityInputModule_get_ActiveMixedRealityPointers_mFC39F2AC95C512A07F84370D4EB2FBFCD5B684C8,
	MixedRealityInputModule_ActivateModule_m78E8ED9474F2626BE2F2FCCBA89583335A2E0123,
	MixedRealityInputModule_Initialize_m5C8767E903A42AC29152699E50A524A0A726774C,
	MixedRealityInputModule_Suspend_m9F994ADB464CB0CC345C47C99ACF497A124356E5,
	MixedRealityInputModule_DeactivateModule_m7D76EBEC365C85207AD0F8E5568662E1A71AF50F,
	MixedRealityInputModule_Process_m5B1DC8A36022288223BEB01770C30AEA574CF4C5,
	MixedRealityInputModule_IsModuleSupported_m4FDAB4F42C2DCCBF727A7DF8DAF57510AEB64703,
	MixedRealityInputModule_ProcessMrtkPointerLost_m59084BBB2C8038D798576A03EC55AB31D49A003A,
	MixedRealityInputModule_GetMousePointerEventData_m1CA4684C1CEF1BDE09AC6A2C126960EDFB706EF1,
	MixedRealityInputModule_UpdateMousePointerEventData_mAC08A45AA77331B1A15B3C5F6ADF1A017A656183,
	MixedRealityInputModule_ResetMousePointerEventData_m32136DE4736A477FEA1BC3CB50E34F1378D0EEC0,
	MixedRealityInputModule_StateForPointer_mB186671E20373F947EF4CF31D8C0B70E2014BFE6,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerUp_mEAC98C7AB39889F3742963903CD989ABE90DFCF8,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDown_m2E4EBBAF819B2B7AAF8D19342263CAE631ABE619,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerDragged_m6263D3A6BB437A5C5D81485CE86F2099AAF1688E,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealityPointerHandler_OnPointerClicked_m10A8EB5F0E8C630A21204F79D2BFEBDB5F395B17,
	MixedRealityInputModule_IsPointerIdInRemovedList_m3718EECC9B7D0837C9295E1511F5AAC894B1FF4E,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceDetected_m9D8A5008FC519959090EEAF1E044E247E803E2E4,
	MixedRealityInputModule_OnSourceDetected_m26841D4D953EC23155F5049B767094C4E83AF90B,
	MixedRealityInputModule_Microsoft_MixedReality_Toolkit_Input_IMixedRealitySourceStateHandler_OnSourceLost_m07D8494F41E4B593F41B147E281771D8B0727F24,
	MixedRealityInputModule__ctor_m06837023269881BA9613045742AE64EA732EE581,
	MixedRealityInputModule__cctor_m25126A0E52F8D95D35E4D60F5E1792185837C12C,
	PointerData__ctor_m70DABFE0F62E54BA41F768926EDDBAB2F8F4A478,
	U3Cget_ActiveMixedRealityPointersU3Ed__16__ctor_m60718834B1CFBB90DA5865424DB7F011DA835093,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_IDisposable_Dispose_m560BF9972D7B8D645C0025E222616B29E024DFE8,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_MoveNext_mA6FD4775DBB99FCA8C85EB97C5A13E8C7B62A93F,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_U3CU3Em__Finally1_m5BBB5BA54298094F68689593CD03E77792F4A8DF,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_Generic_IEnumeratorU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_get_Current_m40D08339CE27568F466E5C567A9C01EFDF1B6E42,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerator_Reset_mED23D57F0095FDDFB161CC451DA29729BB57274B,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerator_get_Current_m9ECF592B70FF514CB8FE4C1DF2425A869EC1FAE8,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_Generic_IEnumerableU3CMicrosoft_MixedReality_Toolkit_Input_IMixedRealityPointerU3E_GetEnumerator_m3A18A1027315ACCF5C92F5DD16C0F30BAF0C2354,
	U3Cget_ActiveMixedRealityPointersU3Ed__16_System_Collections_IEnumerable_GetEnumerator_mD10771B7666047AC7A88E5AAD20052ECB33E4EFE,
	MixedRealityInputSystem__ctor_mA9FBEA880CC96FFEC995254FAA2236826122E495,
	MixedRealityInputSystem__ctor_m4138907A083C53861CF9D8D419162BB2B8B04A44,
	MixedRealityInputSystem_get_Name_mEEA0B81ABCA81A582CE4602476A7D2FBBEAEFCA8,
	MixedRealityInputSystem_set_Name_m4C7B0922D24B9ED77D5CE11378E745F6B98F5D64,
	MixedRealityInputSystem_add_InputEnabled_mA2E19024E91B7CC0AB78181CA8D6B6D647672251,
	MixedRealityInputSystem_remove_InputEnabled_mE4D786FCF1CA8B840B3A58E9167CE99E4A3F9CE0,
	MixedRealityInputSystem_add_InputDisabled_mC45155FC69BB0872D73502C8CAF1CDF831B8A729,
	MixedRealityInputSystem_remove_InputDisabled_m75243AD6B6DC633FBE64F418BBCC5ECB248677EA,
	MixedRealityInputSystem_get_DetectedInputSources_m1DC59E861DA122EE7A0C9556ACBA40EA9E604772,
	MixedRealityInputSystem_get_DetectedControllers_m0601669AEDA32E98D1DADEC835F94572F891F9F9,
	MixedRealityInputSystem_get_InputSystemProfile_m4792CB533E3BAA5316B08D1DA754EEDBE334E720,
	MixedRealityInputSystem_get_FocusProvider_mE26F74977DFEB000FFD50D73DA69E27E2274F2B5,
	MixedRealityInputSystem_get_RaycastProvider_mD8B161FA98D161DD039F5EDE81CF0788EAB6B44C,
	MixedRealityInputSystem_get_GazeProvider_m71E08237B2A8E86E3E0E1539155BF47952DE0845,
	MixedRealityInputSystem_set_GazeProvider_mA5828B9A737DE0F03264C595F157239A5AF0EF52,
	MixedRealityInputSystem_get_EyeGazeProvider_m6144F06F59B98D8C9F1717C536DF50E3187D5819,
	MixedRealityInputSystem_set_EyeGazeProvider_m7AD9EF217A6C3EEA06ACB5D1666EA2F437EDF45D,
	MixedRealityInputSystem_get_IsInputEnabled_mD1FE96D98B73E2285FF14BA2A559E6A16F536E13,
	MixedRealityInputSystem_get_CurrentInputActionRulesProfile_m6E8C8C792FA3AA8F69C5CA6611F656E175618D56,
	MixedRealityInputSystem_set_CurrentInputActionRulesProfile_mA4586511312369F5B5FD6317E5055DA7D4948B4B,
	MixedRealityInputSystem_CheckCapability_mEE90EA9CC3B22350371ED417541E5CC128CC499D,
	MixedRealityInputSystem_get_Priority_m7CCCB0D8F8CAB0C65E30C271A40F0AC8F4FC1884,
	MixedRealityInputSystem_Initialize_m3645A8C7A76031E419DDFFB74F50B4B7D1701707,
	MixedRealityInputSystem_Enable_m1875E5EB533B3FDC59321A436EFB8C3556E2E94E,
	MixedRealityInputSystem_LateUpdate_mE1326028D98A1FB9BC51012B73F63CD6E3010AD6,
	MixedRealityInputSystem_CreateDataProviders_m88EF43BAF68E60DCD09F364C739697BE3DED3816,
	MixedRealityInputSystem_InstantiateGazeProvider_mC15F9EDA3C2657C8EF20FCF3B1986014BECDECA6,
	MixedRealityInputSystem_Reset_m2F0F28484293548B9349FEBDE12F4EE5A8BC6027,
	MixedRealityInputSystem_Disable_m032C1CFBF1301D69E154147E9798D3AB16FB2A19,
	MixedRealityInputSystem_Destroy_m3A11F3D6B173D3CD24EE54B44F3ACBF45CB67BCB,
	NULL,
	NULL,
	NULL,
	MixedRealityInputSystem_HandleFocusChangedEvents_m21979CE1ED5B995D665DC03F69EB37153685440C,
	MixedRealityInputSystem_HandleFocusEvent_m028D05FABF5B2A3E31606F3872795F21FFCE3377,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MixedRealityInputSystem_Register_m1632611B248DBA2B2F3735676E08F8608E38E9A0,
	MixedRealityInputSystem_Unregister_m3990DB1C09F8EB9847C4D779E25EB922A9C161D4,
	MixedRealityInputSystem_PushInputDisable_m1C6134CD1138563C9588901BD28EF19686003C95,
	MixedRealityInputSystem_PopInputDisable_m963386716064CA88B94E668639E135D12D95CC07,
	MixedRealityInputSystem_ClearInputDisableStack_m1EBE9A31112EEC9BC1E0807EEEF7E83B4FB899BF,
	MixedRealityInputSystem_PushModalInputHandler_m98058FA5209BAFD065352F38D7127584D934308E,
	MixedRealityInputSystem_PopModalInputHandler_m1A3582BB12B1F77036E808166A21659C40404C66,
	MixedRealityInputSystem_ClearModalInputStack_m844AC4CD1406313AFDAC98802290970D7B63D78F,
	MixedRealityInputSystem_PushFallbackInputHandler_m15E7071269ED94857B4F9D34C212C9948801B7DF,
	MixedRealityInputSystem_PopFallbackInputHandler_mA01947304A32DC0B0955AF2ED1BEE781966909B1,
	MixedRealityInputSystem_ClearFallbackInputStack_mE4122A99EEBF89B77A5924B0F22733A8A1AF0476,
	MixedRealityInputSystem_GenerateNewSourceId_m7E17D2A2E61BB2B82995A93437033A8EFFAC71C9,
	MixedRealityInputSystem_RequestNewGenericInputSource_mFAEF9A86C0CF8C7309F2C2ED9B4177C0093A6338,
	MixedRealityInputSystem_RequestNewGlobalInputSource_m3DA4A7A63A7347A5CFC16AF2C9A13D051952CB65,
	MixedRealityInputSystem_RaiseSourceDetected_mE938373E3F32B2DAF4A670D96024293AA5A437E9,
	MixedRealityInputSystem_RaiseSourceLost_mDA57604245A696ABD745933114B9851A36BF98C8,
	MixedRealityInputSystem_RaiseSourceTrackingStateChanged_m05420DCBB2866C9A147E6A8E189AF6EDCEB47775,
	MixedRealityInputSystem_RaiseSourcePositionChanged_m5AD15DE19E784BB7454063D0C36013F30DDAE21B,
	MixedRealityInputSystem_RaiseSourcePositionChanged_m967F87A91598032AA83C1DC663C674D73F185258,
	MixedRealityInputSystem_RaiseSourceRotationChanged_mED6A6F5FAD4F893B0EDE6079EE581323A2C7C537,
	MixedRealityInputSystem_RaiseSourcePoseChanged_m2FCC604D4D0967D579F96352286FF5C5DE0FEE46,
	MixedRealityInputSystem_RaisePreFocusChanged_mE9637BA4CFE4D54A51512C5C4A6122C9F8BEE6B9,
	MixedRealityInputSystem_RaiseFocusChanged_m8B3851905AE8913362B79D4C8A27E5A120E51ED5,
	MixedRealityInputSystem_RaiseFocusEnter_mEB0DE70CC3ABEE1FB82EE91A560EB503AC8336F5,
	MixedRealityInputSystem_RaiseFocusExit_m5E4E22A657927C30BCF054921D4E8EA92E7F8801,
	MixedRealityInputSystem_RaisePointerDown_m597D81D77CADFDBEC77894A020D37A319E33E1A7,
	MixedRealityInputSystem_RaisePointerDragged_m7D0D944206CD6417616CA948A9C639EB4B8CFEAA,
	MixedRealityInputSystem_RaisePointerClicked_m47DE7FDFC7075E831C6121875B09150783E3EFEE,
	MixedRealityInputSystem_HandleClick_mCE92FA5236EB13D341FC007F9944C9B7150770E0,
	MixedRealityInputSystem_RaisePointerUp_mB992C214CDA63B5F725831E88F6A94B3C07FF337,
	MixedRealityInputSystem_RaiseOnInputDown_mF1795FC45B5DF76FAEDA6D90BF2B482467EE0CDD,
	MixedRealityInputSystem_RaiseOnInputUp_m09526D3603E2B49D48473D7A30BFBE200F41488E,
	MixedRealityInputSystem_RaiseFloatInputChanged_m54A789EA9530044082A5C5B2FFE3F23CAC35C189,
	MixedRealityInputSystem_RaisePositionInputChanged_mA79017106D5CE5FFE9F8314FFA668F048623A3CF,
	MixedRealityInputSystem_RaisePositionInputChanged_m2383FE56AA9C33D740F57D84291EC5066A2BB4B4,
	MixedRealityInputSystem_RaiseRotationInputChanged_m063A221D98DD390CCC0B28461A617C1F5C7FC3DA,
	MixedRealityInputSystem_RaisePoseInputChanged_mE0EFC51B4149A2D4763924990596BD866DB9049A,
	MixedRealityInputSystem_RaiseGestureStarted_m0E3A61B3FFB97136E8979DC58372A1C6539A5324,
	MixedRealityInputSystem_RaiseGestureUpdated_m0D64F1FA57AC1D62996CAE3110109A3170F976DC,
	MixedRealityInputSystem_RaiseGestureUpdated_m3AFFB88BB46D0A4EEFC3B2B1C9D947879824E470,
	MixedRealityInputSystem_RaiseGestureUpdated_m83F1E0862F16004C99CDA0A118893E017AFC6CCC,
	MixedRealityInputSystem_RaiseGestureUpdated_m86D5660C5FA5D91BBEF1031E74681F055953A325,
	MixedRealityInputSystem_RaiseGestureUpdated_mCB8CB7B0C45D6F8B11C983D87A5C45EB2BD47B43,
	MixedRealityInputSystem_RaiseGestureCompleted_mA4201D117F5B1A868F7F4EC441B14EC7646A1D3F,
	MixedRealityInputSystem_RaiseGestureCompleted_m9F7DAB69BC17656DA80912C21CBA0A8597F47D35,
	MixedRealityInputSystem_RaiseGestureCompleted_mBB4B1BFD92B89BD32DF394A43BDB5BAB4BF30D60,
	MixedRealityInputSystem_RaiseGestureCompleted_mEA31A162F0E5A17D5FEEBC427B110E4CAE1DE854,
	MixedRealityInputSystem_RaiseGestureCompleted_m50A19D01CD4D9EDC2F85110FFDDD4C5D2D93A4DE,
	MixedRealityInputSystem_RaiseGestureCanceled_m6C78DE10C5F1820F8B40B6EC55B52B6703CFC1AD,
	MixedRealityInputSystem_RaiseSpeechCommandRecognized_mE1E55258FFD48292A92E963C34C82544B9811058,
	MixedRealityInputSystem_RaiseDictationHypothesis_mEA20AAF662B0CDF32507E1281630620ED00290D8,
	MixedRealityInputSystem_RaiseDictationResult_m1CA9981073F93215DF0D0E19D8D31BE2312A8469,
	MixedRealityInputSystem_RaiseDictationComplete_mB112654C78AAFD8BAA4DCB33F9A2F3230FB69B78,
	MixedRealityInputSystem_RaiseDictationError_m4D6D7D671894C45AE99A588038011C6B57ACA4E3,
	MixedRealityInputSystem_RaiseHandJointsUpdated_mCB70F5D5D36EACBB740A0DB7523A47CE9BFA8287,
	MixedRealityInputSystem_RaiseHandMeshUpdated_mBB14BD372CACE4C3F935ADF38B094372DFF1EEFE,
	MixedRealityInputSystem_RaiseOnTouchStarted_m31F8B9A6432FF7FF49BE5240F4BFEAB1119543F5,
	MixedRealityInputSystem_RaiseOnTouchCompleted_mF2E008087005DD633BC4DE8335AAAE2615CF463A,
	MixedRealityInputSystem_RaiseOnTouchUpdated_m21115736390B2942D6FE10D01DC14582E48B57F8,
	NULL,
	MixedRealityInputSystem_ProcessRules_m43D47333222387477DEA69AA06A592F41A7EEDB4,
	MixedRealityInputSystem_ProcessRules_m10BED0AC6AA97856ED9002859195F6C9F1B194C8,
	MixedRealityInputSystem_ProcessRules_m721BEDE7DFB2DC0033C2A01F130F8FC941F25FB6,
	MixedRealityInputSystem_ProcessRules_mC29CB020934185D1443DF2A122CFDF2811E21CC1,
	MixedRealityInputSystem_ProcessRules_m281A6B109EC76F292804334464AA8553FC050566,
	MixedRealityInputSystem_ProcessRules_mEE97C73EE0969F54B5A76704901F37D2054064F0,
	MixedRealityInputSystem__cctor_m7D8EDD0CB0AA272B58EFAA0C8773ADB5DFECD9C1,
	U3CU3Ec__cctor_m771A4DA2249305F1525AE289EDBB833D2E6924F9,
	U3CU3Ec__ctor_m74645C2E43AB86BAEC2967695905AFC4C2A36CA8,
	U3CU3Ec_U3C_cctorU3Eb__244_0_mA3755D122CEBAF0237FC1C401E30AA485BAC071C,
	U3CU3Ec_U3C_cctorU3Eb__244_1_mA0C8143EFB1A2E9512BD6BD53ACADF546FBCB064,
	U3CU3Ec_U3C_cctorU3Eb__244_2_mF96D25A53C65D204CC6C094A8C7E11D93E086A91,
	U3CU3Ec_U3C_cctorU3Eb__244_3_m212ED947CA62C4DD3F24491CCDD79B600B89A2CB,
	U3CU3Ec_U3C_cctorU3Eb__244_4_m3EB511B2695D5C3E8A668BAEF3FC968EA6297915,
	U3CU3Ec_U3C_cctorU3Eb__244_5_m5E014B2B8103BE940752E21687799357D57D838B,
	U3CU3Ec_U3C_cctorU3Eb__244_6_m845EFF6E20069692C5E86562D70296BCB622535B,
	U3CU3Ec_U3C_cctorU3Eb__244_7_mFC815B9D207B1128E9859800F77194E65E77D7D4,
	U3CU3Ec_U3C_cctorU3Eb__244_8_m07B29A98D0DE44F9B5F757CDA2B84CE5A4399D3C,
	U3CU3Ec_U3C_cctorU3Eb__244_9_m7D6349DE6429105F96809D61B579442BBFEC3147,
	U3CU3Ec_U3C_cctorU3Eb__244_10_m74A454EFADB6D63825A9491A807EFBEBD7D1E8C8,
	U3CU3Ec_U3C_cctorU3Eb__244_11_m788337DD5337450C51D20ADC80518E49224AC8A7,
	U3CU3Ec_U3C_cctorU3Eb__244_12_mB110D0BBFB1369A32965E687652D5544397FCEDD,
	U3CU3Ec_U3C_cctorU3Eb__244_13_mB203739B4E23E76BDC67C3C77AB13C952BB43839,
	U3CU3Ec_U3C_cctorU3Eb__244_14_m8394321DA1A17E02B57A5C50CA600C3091173E8C,
	U3CU3Ec_U3C_cctorU3Eb__244_15_m88FE26E306388AF861C1D3098826E30941B4FD36,
	U3CU3Ec_U3C_cctorU3Eb__244_16_mDB42315FBCF69F464617AF212523D102A4865D1C,
	U3CU3Ec_U3C_cctorU3Eb__244_17_mCE9EEBF06706049CBDB0E9747FA7BAE5EE5C6582,
	U3CU3Ec_U3C_cctorU3Eb__244_18_m92689EB8A2C94BD493F07BC7B854D7A36CC48F72,
	U3CU3Ec_U3C_cctorU3Eb__244_19_mC8B9A84C7E4B9FE1F22EA3B9190D49DE5F8204B0,
	U3CU3Ec_U3C_cctorU3Eb__244_20_mD546574BD757D63F403829B3B99DAB22AC2E1B4C,
	U3CU3Ec_U3C_cctorU3Eb__244_21_m7E9BBA34D0F3E7DECDB680886266675A29A10A4C,
	U3CU3Ec_U3C_cctorU3Eb__244_22_m08865D0DDE03169804D72BE5D7C5CFF995CF37ED,
	U3CU3Ec_U3C_cctorU3Eb__244_23_mBFFB133545A720F7F4A1CD99B01D9260A985B044,
	U3CU3Ec_U3C_cctorU3Eb__244_24_m64AF0238DA3950B52D6FE26C01DF609781F6168D,
	U3CU3Ec_U3C_cctorU3Eb__244_25_mA9932B8B515C02DB3A540E95859780968DE12B05,
	U3CU3Ec_U3C_cctorU3Eb__244_26_m3F544FDCB8CB7974FB180EA2D5280905AF74679A,
	U3CU3Ec_U3C_cctorU3Eb__244_27_m90A1F3B54E88CBF6D1402531C368B16D3AD44572,
	U3CU3Ec_U3C_cctorU3Eb__244_28_mDFAC0BD76BD41A2ADB85387427A64CF9744761CE,
	U3CU3Ec_U3C_cctorU3Eb__244_29_m87D3D0982E96F42B3020B08D9CF77793F1ADD458,
	U3CU3Ec_U3C_cctorU3Eb__244_30_mF2C37CA179EE6F1243B0B6E565B5BFFB31B9EF82,
	U3CU3Ec_U3C_cctorU3Eb__244_31_m6FCCF8DCA98F430317268D6BBAB43A09F2E7644F,
	U3CU3Ec_U3C_cctorU3Eb__244_32_m2F9C64F18F1B188CDF62F38F0AB2C6B86060B643,
	U3CU3Ec_U3C_cctorU3Eb__244_33_mDA7B33F0909044F091311C83757D701E6914C5AA,
	U3CU3Ec_U3C_cctorU3Eb__244_34_m4E0461608918A474E0BEAD2366838027CE079E07,
	U3CU3Ec_U3C_cctorU3Eb__244_35_m1F417F8CCA5B63485A2738EC0B3659D9934D0CFC,
	U3CU3Ec_U3C_cctorU3Eb__244_36_mB9B35DD6354085A44098E9BE25D0862E525D3695,
	U3CU3Ec_U3C_cctorU3Eb__244_37_mD4240DC40D5BACC7CF433E04926963AA5AF4CCE8,
	U3CU3Ec_U3C_cctorU3Eb__244_38_m3BD9A0BB73A976ECE1B81A897F90E56BCB0C9001,
	U3CU3Ec_U3C_cctorU3Eb__244_39_m194D80054AADB4425C49634AA62E8E1A04610316,
	U3CU3Ec_U3C_cctorU3Eb__244_40_mE448200A7BC3E722C1B2BEF519D9B40EE252E5E6,
	U3CU3Ec_U3C_cctorU3Eb__244_41_m262485B179BC605B22CC2D9805F8AC68A2172735,
	U3CU3Ec_U3C_cctorU3Eb__244_42_mD25DBC098C6002B497DCB187B63FFA4175361300,
	U3CU3Ec_U3C_cctorU3Eb__244_43_m5E2134FC85DC707AC77D9725E87E57B7CDFA5423,
	U3CU3Ec_U3C_cctorU3Eb__244_44_m5DE8F833D52BA5E0728FC5FD6D6F8B5B4C6C032A,
	U3CU3Ec_U3C_cctorU3Eb__244_45_mEC14C6245C596B126D0EBC69D313B9BA043A9B94,
	U3CU3Ec_U3C_cctorU3Eb__244_46_m0216C66A083BE3E34D0ADD479115C2537913CEF2,
	U3CU3Ec_U3C_cctorU3Eb__244_47_m7C30E2F8E963D23C9C688643C4C471B25D8BE677,
	U3CU3Ec_U3C_cctorU3Eb__244_48_m55E06A3037AB37237AF8E9D092CBC77EFD02A90A,
	NearInteractionGrabbable_OnEnable_m6BD888F5740998A3DBF0D1206226312E41CC74DB,
	NearInteractionGrabbable__ctor_mC84A2C0C7C123225580581C4EED89CAAB036F82A,
	NearInteractionTouchable_get_LocalForward_m86E478F879E7599F3A1AF58541A2658C3B5B3EBF,
	NearInteractionTouchable_get_LocalUp_m9B7264306F50A2F0811BBDD78C2B1E63D1481AEF,
	NearInteractionTouchable_get_AreLocalVectorsOrthogonal_m6D41D1C67BC4AD6E6B47744BB5356F8F412C96F0,
	NearInteractionTouchable_get_LocalCenter_m976B453D9975C12A49558CD22523F99799192F12,
	NearInteractionTouchable_get_LocalRight_m9CCDCFE2BDA1E347C87A3DBC137345522478A251,
	NearInteractionTouchable_get_Forward_m985A9312873DCE273BB794E58980F11E3FB81857,
	NearInteractionTouchable_get_LocalPressDirection_m4923B9E3AF24F386FFAD364B9DCB162E708DDDC7,
	NearInteractionTouchable_get_Bounds_m541A8675CC3CE89D1FAC8982BA7D82F61F077F2A,
	NearInteractionTouchable_get_ColliderEnabled_m86F6D91D5FA2B2A1B3821FDAF8A4FB4CB2C22DC9,
	NearInteractionTouchable_get_TouchableCollider_m8C691DAB535F4EC8019ABEF7332E0C038C5860BE,
	NearInteractionTouchable_OnValidate_mE8AB69CC53E13E48CABBC84632A3BD2704144A0E,
	NearInteractionTouchable_OnEnable_mD1C3B884E74A9F68183E1A7888D45A993F0A4E81,
	NearInteractionTouchable_SetLocalForward_m86D68108392905F99DD8BCDE038A3FFC5575A0B1,
	NearInteractionTouchable_SetLocalUp_m14D2C4679ADB1BBBA47B25954A010D2C61A093CC,
	NearInteractionTouchable_SetLocalCenter_m2B22E8B5947724B03B2D0A284A5AE86FEE88E16F,
	NearInteractionTouchable_SetBounds_m66FDC7208B00EC3BE2BCB86AF6B164E97D1E4046,
	NearInteractionTouchable_SetTouchableCollider_m74BCAFD4DEC707BDE4AFBED7E56A00FE0FE402F4,
	NearInteractionTouchable_DistanceToTouchable_mD0AB5721A5D2A485068C974AB964CDEDD8FDAA5A,
	NearInteractionTouchable__ctor_m639B63E25291306E89264D1DF898CDF21DB80354,
	U3CU3Ec__cctor_mC186AE7BA047B178D55FEB5EC110ABF5F6A44556,
	U3CU3Ec__ctor_m4A704E423DAE1FB82FE869DE25DCE8D7D085D84D,
	U3CU3Ec_U3COnValidateU3Eb__25_0_mA4C3DAAD1DA22A984F7C83EF08B3EDD39DC9EB97,
	NULL,
	NULL,
	NULL,
	NearInteractionTouchableSurface__ctor_mB50660F4487750D3B291092A908F031638D71C73,
	NearInteractionTouchableUnityUI_get_Instances_mCC93A7E33281DB6A5081C52754BB1D860A9CB9BB,
	NearInteractionTouchableUnityUI_get_LocalCenter_m3C322AB7048D2455E69C0B82B6F638324FA1AE78,
	NearInteractionTouchableUnityUI_get_LocalPressDirection_m7D1DA44E4869290C281FB34DFDCA57E497FE7113,
	NearInteractionTouchableUnityUI_get_Bounds_m5634AF6D6BA73D4705F744B7A08F61BB316BC090,
	NearInteractionTouchableUnityUI__ctor_m3C4CE86E6251A4608661684052DA83E55C29EB0F,
	NearInteractionTouchableUnityUI_DistanceToTouchable_mF2FD9214210F7827DEA34E6ECE0DC95DDA85EA27,
	NearInteractionTouchableUnityUI_OnEnable_mE5340149950F7D67EA64847A150AEA31FC3E3446,
	NearInteractionTouchableUnityUI_OnDisable_m1C13E7C481574267A5F8F590312657EEC2AC01FD,
	NearInteractionTouchableUnityUI__cctor_m62D51540081D0EA7CEECD85DBF58E8799BE8D1AE,
	NearInteractionTouchableVolume_get_ColliderEnabled_m7DCDF2BF6352EF929D646CA2FA53ADCFE27F9CD5,
	NearInteractionTouchableVolume_get_TouchableCollider_mBB082E327DBB122DEA40C1DB23A4D749C1B77678,
	NearInteractionTouchableVolume_OnValidate_mE3FA110E040936F3D6B9463BBCD8C8BA6985FD84,
	NearInteractionTouchableVolume_Awake_m74D580E4AC52E5BA0FAB1C57CB2FA7BFBE250031,
	NearInteractionTouchableVolume_DistanceToTouchable_m791833773FE073D3EC94C56C50AFA98F39248752,
	NearInteractionTouchableVolume__ctor_mB35E0A03F0AE9830DD2FE02A9C6C8BA4C0E36EED,
	CanvasUtility_OnPointerClicked_m120E5ADB08750D9CB0BF284A1C6EC37CFCC00FD9,
	CanvasUtility_OnPointerDown_m257B9AAABF3085BC2F30EF7B4B67C6B4F2E60F77,
	CanvasUtility_OnPointerDragged_m1DDBF479602D945609760F4BC003CE6942032973,
	CanvasUtility_OnPointerUp_mFF401C7213758DEB0C181B1DBD555BAC066A0CB5,
	CanvasUtility_Start_m6EC1BD60BD2A72F07F685B69D61CF8E900855976,
	CanvasUtility_VerifyCanvasConfiguration_mD596D87BE9BD562AC661DDA4E6CEE468210A5B33,
	CanvasUtility__ctor_m9B9F4B172EEE5CB377E0BAC98FD2C093F51C8A56,
	ScaleMeshEffect_Awake_m762B187E4FC278DD10BE9923A4724C326E3608A5,
	ScaleMeshEffect_ModifyMesh_m86443B9BE01622A6186BC8310D18F3A1A965C6DB,
	ScaleMeshEffect__ctor_m82EE405A6E2CCFE66126E9FCF9E581EB78A24FE9,
};
extern void U3CStartU3Ed__63_MoveNext_m1995BBF055EAF141D9C17374D7FBF5D9D5B55264_AdjustorThunk (void);
extern void U3CStartU3Ed__63_SetStateMachine_mFFE24F4AA58DA0BEDA1BFEEC5C07DAA9C8ACAF9F_AdjustorThunk (void);
extern void U3CRaiseSourceDetectedU3Ed__77_MoveNext_m3886E54E9E5F0455475EAEB0FA3D918761126E10_AdjustorThunk (void);
extern void U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_mAD25DF053CFA407698925AC737602775ED4533DF_AdjustorThunk (void);
extern void U3CStartU3Ed__2_MoveNext_mB3603C3B8DE4E20F9719C586BA61319384ABDFDF_AdjustorThunk (void);
extern void U3CStartU3Ed__2_SetStateMachine_m962AD6827A53A07A77D01A0AB80D1C88D0FA386B_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m8A3DFB7EAF3583E06AE2B60C5C8FFF11770F0C89_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m51E0D646F26D314B94CDA9CCD82EC8D2D3886A5F_AdjustorThunk (void);
extern void U3CStartU3Ed__2_MoveNext_m4E247D7B009018AEB60C476724746C6F6B919784_AdjustorThunk (void);
extern void U3CStartU3Ed__2_SetStateMachine_m25C1F2CBE76178EDD5E7B22BDB2F6654A9B95CCF_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_MoveNext_m7B0D5FF107CDE4A70864EA5F6D6433CB8344411E_AdjustorThunk (void);
extern void U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_mA88C5F770AE0D475E0137FC57E9CA4464D54293D_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[12] = 
{
	{ 0x060000B4, U3CStartU3Ed__63_MoveNext_m1995BBF055EAF141D9C17374D7FBF5D9D5B55264_AdjustorThunk },
	{ 0x060000B5, U3CStartU3Ed__63_SetStateMachine_mFFE24F4AA58DA0BEDA1BFEEC5C07DAA9C8ACAF9F_AdjustorThunk },
	{ 0x060000B6, U3CRaiseSourceDetectedU3Ed__77_MoveNext_m3886E54E9E5F0455475EAEB0FA3D918761126E10_AdjustorThunk },
	{ 0x060000B7, U3CRaiseSourceDetectedU3Ed__77_SetStateMachine_mAD25DF053CFA407698925AC737602775ED4533DF_AdjustorThunk },
	{ 0x060000BF, U3CStartU3Ed__2_MoveNext_mB3603C3B8DE4E20F9719C586BA61319384ABDFDF_AdjustorThunk },
	{ 0x060000C0, U3CStartU3Ed__2_SetStateMachine_m962AD6827A53A07A77D01A0AB80D1C88D0FA386B_AdjustorThunk },
	{ 0x060000C4, U3CEnsureInputSystemValidU3Ed__4_MoveNext_m8A3DFB7EAF3583E06AE2B60C5C8FFF11770F0C89_AdjustorThunk },
	{ 0x060000C5, U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_m51E0D646F26D314B94CDA9CCD82EC8D2D3886A5F_AdjustorThunk },
	{ 0x060000CB, U3CStartU3Ed__2_MoveNext_m4E247D7B009018AEB60C476724746C6F6B919784_AdjustorThunk },
	{ 0x060000CC, U3CStartU3Ed__2_SetStateMachine_m25C1F2CBE76178EDD5E7B22BDB2F6654A9B95CCF_AdjustorThunk },
	{ 0x060000D0, U3CEnsureInputSystemValidU3Ed__4_MoveNext_m7B0D5FF107CDE4A70864EA5F6D6433CB8344411E_AdjustorThunk },
	{ 0x060000D1, U3CEnsureInputSystemValidU3Ed__4_SetStateMachine_mA88C5F770AE0D475E0137FC57E9CA4464D54293D_AdjustorThunk },
};
static const int32_t s_InvokerIndices[458] = 
{
	10,
	32,
	726,
	337,
	23,
	2709,
	23,
	89,
	14,
	23,
	23,
	27,
	26,
	14,
	26,
	2546,
	2547,
	2548,
	3,
	27,
	26,
	14,
	10,
	32,
	10,
	32,
	14,
	26,
	14,
	26,
	10,
	726,
	14,
	14,
	89,
	26,
	26,
	23,
	23,
	23,
	23,
	28,
	924,
	2530,
	10,
	23,
	23,
	9,
	9,
	26,
	9,
	-1,
	102,
	26,
	924,
	23,
	26,
	27,
	218,
	23,
	2710,
	442,
	2711,
	23,
	26,
	26,
	26,
	121,
	-1,
	2712,
	10,
	32,
	-1,
	3,
	23,
	2713,
	2714,
	2715,
	23,
	1379,
	1380,
	2549,
	2716,
	14,
	14,
	26,
	10,
	32,
	14,
	89,
	26,
	26,
	23,
	31,
	9,
	9,
	10,
	3,
	147,
	9,
	228,
	819,
	137,
	89,
	39,
	26,
	23,
	89,
	31,
	14,
	14,
	14,
	26,
	14,
	14,
	26,
	2468,
	2469,
	1379,
	1380,
	1379,
	1380,
	1379,
	1379,
	1379,
	1380,
	1379,
	1380,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	14,
	23,
	2469,
	26,
	89,
	89,
	1109,
	1110,
	2473,
	2432,
	89,
	31,
	119,
	331,
	2529,
	102,
	89,
	31,
	1377,
	23,
	3,
	23,
	2717,
	14,
	26,
	14,
	26,
	726,
	337,
	26,
	23,
	23,
	23,
	1379,
	1552,
	23,
	2718,
	2718,
	3,
	23,
	26,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	23,
	26,
	3,
	23,
	89,
	23,
	26,
	23,
	23,
	23,
	14,
	23,
	23,
	26,
	3,
	23,
	89,
	23,
	26,
	14,
	26,
	89,
	31,
	89,
	31,
	14,
	23,
	23,
	23,
	23,
	23,
	89,
	26,
	34,
	26,
	26,
	121,
	26,
	26,
	26,
	26,
	30,
	26,
	26,
	26,
	23,
	3,
	27,
	32,
	23,
	89,
	23,
	14,
	23,
	14,
	14,
	14,
	27,
	26,
	14,
	26,
	26,
	26,
	26,
	26,
	14,
	14,
	14,
	14,
	14,
	14,
	26,
	14,
	26,
	89,
	14,
	26,
	30,
	10,
	23,
	23,
	23,
	23,
	26,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	27,
	212,
	-1,
	-1,
	-1,
	-1,
	-1,
	26,
	26,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	23,
	23,
	10,
	111,
	111,
	27,
	27,
	110,
	2531,
	2532,
	2533,
	2534,
	212,
	212,
	27,
	27,
	2535,
	2535,
	2536,
	23,
	2535,
	2517,
	2517,
	2537,
	2538,
	2539,
	2540,
	2541,
	2515,
	2515,
	2542,
	2543,
	2544,
	2545,
	2515,
	2542,
	2543,
	2544,
	2545,
	2515,
	2522,
	212,
	212,
	212,
	212,
	133,
	133,
	2516,
	2516,
	2516,
	-1,
	2719,
	2720,
	2721,
	2722,
	2723,
	2724,
	3,
	3,
	23,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	27,
	23,
	23,
	1379,
	1379,
	89,
	1379,
	1379,
	1379,
	1379,
	1395,
	89,
	14,
	23,
	23,
	1380,
	1380,
	1380,
	1396,
	26,
	2709,
	23,
	3,
	23,
	114,
	1379,
	1379,
	1395,
	23,
	4,
	1379,
	1379,
	1395,
	23,
	2709,
	23,
	23,
	3,
	89,
	14,
	23,
	23,
	2709,
	23,
	26,
	26,
	26,
	26,
	23,
	23,
	23,
	23,
	26,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[12] = 
{
	{ 0x06000034, { 0, 5 } },
	{ 0x06000045, { 5, 1 } },
	{ 0x06000049, { 6, 1 } },
	{ 0x06000116, { 7, 2 } },
	{ 0x06000117, { 9, 2 } },
	{ 0x06000118, { 11, 3 } },
	{ 0x0600011B, { 14, 3 } },
	{ 0x0600011C, { 17, 1 } },
	{ 0x0600011D, { 18, 1 } },
	{ 0x0600011E, { 19, 1 } },
	{ 0x0600011F, { 20, 1 } },
	{ 0x0600015B, { 21, 6 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[27] = 
{
	{ (Il2CppRGCTXDataType)2, 63691 },
	{ (Il2CppRGCTXDataType)3, 62661 },
	{ (Il2CppRGCTXDataType)2, 56414 },
	{ (Il2CppRGCTXDataType)3, 62662 },
	{ (Il2CppRGCTXDataType)3, 62663 },
	{ (Il2CppRGCTXDataType)1, 63692 },
	{ (Il2CppRGCTXDataType)1, 63693 },
	{ (Il2CppRGCTXDataType)1, 56501 },
	{ (Il2CppRGCTXDataType)3, 62664 },
	{ (Il2CppRGCTXDataType)1, 56502 },
	{ (Il2CppRGCTXDataType)3, 62665 },
	{ (Il2CppRGCTXDataType)3, 62666 },
	{ (Il2CppRGCTXDataType)3, 62667 },
	{ (Il2CppRGCTXDataType)3, 62668 },
	{ (Il2CppRGCTXDataType)3, 62669 },
	{ (Il2CppRGCTXDataType)3, 62670 },
	{ (Il2CppRGCTXDataType)3, 62671 },
	{ (Il2CppRGCTXDataType)3, 62672 },
	{ (Il2CppRGCTXDataType)3, 62673 },
	{ (Il2CppRGCTXDataType)3, 62674 },
	{ (Il2CppRGCTXDataType)3, 62675 },
	{ (Il2CppRGCTXDataType)2, 56516 },
	{ (Il2CppRGCTXDataType)2, 56518 },
	{ (Il2CppRGCTXDataType)3, 62676 },
	{ (Il2CppRGCTXDataType)3, 62677 },
	{ (Il2CppRGCTXDataType)2, 56517 },
	{ (Il2CppRGCTXDataType)3, 62678 },
};
extern const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSystemCodeGenModule;
const Il2CppCodeGenModule g_Microsoft_MixedReality_Toolkit_Services_InputSystemCodeGenModule = 
{
	"Microsoft.MixedReality.Toolkit.Services.InputSystem.dll",
	458,
	s_methodPointers,
	12,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	12,
	s_rgctxIndices,
	27,
	s_rgctxValues,
	NULL,
};
