﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 UnityEngine.TerrainData UnityEngine.TerrainCollider::get_terrainData()
extern void TerrainCollider_get_terrainData_mDE79198C47B923C972E75EE4DAC24D0117DB9F8B (void);
static Il2CppMethodPointer s_methodPointers[1] = 
{
	TerrainCollider_get_terrainData_mDE79198C47B923C972E75EE4DAC24D0117DB9F8B,
};
static const int32_t s_InvokerIndices[1] = 
{
	14,
};
extern const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_TerrainPhysicsModuleCodeGenModule = 
{
	"UnityEngine.TerrainPhysicsModule.dll",
	1,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
};
