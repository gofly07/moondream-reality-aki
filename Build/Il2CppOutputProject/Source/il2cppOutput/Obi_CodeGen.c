﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "codegen/il2cpp-codegen-metadata.h"





IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END




// 0x00000001 System.Boolean ObiContactGrabber::get_grabbed()
extern void ObiContactGrabber_get_grabbed_mEF424D81FBD0D9839F90266534D206B390F6F38F (void);
// 0x00000002 System.Void ObiContactGrabber::Awake()
extern void ObiContactGrabber_Awake_m07C98A107436EF9EFCD1E5341A2C8796EDA3C58C (void);
// 0x00000003 System.Void ObiContactGrabber::OnEnable()
extern void ObiContactGrabber_OnEnable_m0959DDCD6A5135831E5720DBD972C4EA53C9608C (void);
// 0x00000004 System.Void ObiContactGrabber::OnDisable()
extern void ObiContactGrabber_OnDisable_m4B3259ADF9B791EEABC4BDFA9F96119392F3B3C5 (void);
// 0x00000005 System.Void ObiContactGrabber::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactGrabber_Solver_OnCollision_m8B1B245C128EB18660570B462D7B66932F4A3085 (void);
// 0x00000006 System.Void ObiContactGrabber::UpdateParticleProperties()
extern void ObiContactGrabber_UpdateParticleProperties_mD0CAA571E80537B84ABD4138622385BA55217A6B (void);
// 0x00000007 System.Boolean ObiContactGrabber::GrabParticle(Obi.ObiSolver,System.Int32)
extern void ObiContactGrabber_GrabParticle_m3C970F6A4A69179D921F847759F8C1F52E1573FA (void);
// 0x00000008 System.Void ObiContactGrabber::Grab()
extern void ObiContactGrabber_Grab_mCB77408F7BA33558DD0191B9D72140396324D733 (void);
// 0x00000009 System.Void ObiContactGrabber::Release()
extern void ObiContactGrabber_Release_m0E714310CE0CF9DB94B9D88B0B582A03B457F291 (void);
// 0x0000000A System.Void ObiContactGrabber::FixedUpdate()
extern void ObiContactGrabber_FixedUpdate_m4E9292EC27FC0EC618D28A9F3F4F3797E3AA81E3 (void);
// 0x0000000B System.Void ObiContactGrabber::.ctor()
extern void ObiContactGrabber__ctor_m24D31D38118E238C23651BF57F769774F8A06C04 (void);
// 0x0000000C System.Void ObiContactGrabber/GrabbedParticle::.ctor(Obi.ObiSolver,System.Int32,System.Single)
extern void GrabbedParticle__ctor_mA98E7DE9626624773F115AF2D24BE6CE04845413 (void);
// 0x0000000D System.Boolean ObiContactGrabber/GrabbedParticle::Equals(ObiContactGrabber/GrabbedParticle,ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_Equals_m1148839162B76102DED390B449BA60AD9DA9C270 (void);
// 0x0000000E System.Int32 ObiContactGrabber/GrabbedParticle::GetHashCode(ObiContactGrabber/GrabbedParticle)
extern void GrabbedParticle_GetHashCode_m2020F55DEDD9FCE304D1309AD6BB38C882DCDC94 (void);
// 0x0000000F System.Runtime.InteropServices.GCHandle Oni::PinMemory(System.Object)
extern void Oni_PinMemory_m208E84B3AB58A0BD994F116B056A727208F14F8C (void);
// 0x00000010 System.Void Oni::UnpinMemory(System.Runtime.InteropServices.GCHandle)
extern void Oni_UnpinMemory_mE894DF46823B95677702B2FD9E29004650D4B88F (void);
// 0x00000011 System.Void Oni/SolverParameters::.ctor(Oni/SolverParameters/Interpolation,UnityEngine.Vector4)
extern void SolverParameters__ctor_mCE0C179B96FED563AC3E2E669026A579BE013660 (void);
// 0x00000012 System.Void Oni/ConstraintParameters::.ctor(System.Boolean,Oni/ConstraintParameters/EvaluationOrder,System.Int32)
extern void ConstraintParameters__ctor_m863469DE4E22DA6D6EA4B50AB79348BFEB13BEB1 (void);
// 0x00000013 Obi.ObiActorBlueprint Obi.ObiCloth::get_sourceBlueprint()
extern void ObiCloth_get_sourceBlueprint_m13D370945E6DD6D5837A57CA793F3197746365AD (void);
// 0x00000014 Obi.ObiClothBlueprintBase Obi.ObiCloth::get_clothBlueprintBase()
extern void ObiCloth_get_clothBlueprintBase_m16A8EC579D53A04F4A4576A5DB9692D18F910E86 (void);
// 0x00000015 System.Boolean Obi.ObiCloth::get_volumeConstraintsEnabled()
extern void ObiCloth_get_volumeConstraintsEnabled_m76C7309B3D7CE7C7A77BDC1E8C7F408ABED47EF6 (void);
// 0x00000016 System.Void Obi.ObiCloth::set_volumeConstraintsEnabled(System.Boolean)
extern void ObiCloth_set_volumeConstraintsEnabled_mB365237EF02AE01A846963DE762CA1E801EA58AE (void);
// 0x00000017 System.Single Obi.ObiCloth::get_compressionCompliance()
extern void ObiCloth_get_compressionCompliance_m292433C738BF79F774AC2EF60F26566D66BE306A (void);
// 0x00000018 System.Void Obi.ObiCloth::set_compressionCompliance(System.Single)
extern void ObiCloth_set_compressionCompliance_m5C8A760E0ADFA6C3E271151210CAE8D42A048FD9 (void);
// 0x00000019 System.Single Obi.ObiCloth::get_pressure()
extern void ObiCloth_get_pressure_m7D48FEBE744CCF221A0DDB6D052616D6CBD8C6E0 (void);
// 0x0000001A System.Void Obi.ObiCloth::set_pressure(System.Single)
extern void ObiCloth_set_pressure_m553BA785F9FFF92F84E0DC27E326EE12623B950F (void);
// 0x0000001B System.Boolean Obi.ObiCloth::get_tetherConstraintsEnabled()
extern void ObiCloth_get_tetherConstraintsEnabled_m290EA6F9508D712BF2AFDC6912E5C55E162A7F4A (void);
// 0x0000001C System.Void Obi.ObiCloth::set_tetherConstraintsEnabled(System.Boolean)
extern void ObiCloth_set_tetherConstraintsEnabled_mB844EAF783BCD82FEF172E9F62B9C024D222F0AD (void);
// 0x0000001D System.Single Obi.ObiCloth::get_tetherCompliance()
extern void ObiCloth_get_tetherCompliance_m21174A2978D30A95E942FD59A95650450AB38808 (void);
// 0x0000001E System.Void Obi.ObiCloth::set_tetherCompliance(System.Single)
extern void ObiCloth_set_tetherCompliance_mA53C03148A4ED6B7A12E4DA5BBC0DF3F0E3B2F44 (void);
// 0x0000001F System.Single Obi.ObiCloth::get_tetherScale()
extern void ObiCloth_get_tetherScale_m75316B57FFA3CB02039DF6DF263C937E90692371 (void);
// 0x00000020 System.Void Obi.ObiCloth::set_tetherScale(System.Single)
extern void ObiCloth_set_tetherScale_m24D8EA690CF70CE4A4FF10C16D190CB890D43BB4 (void);
// 0x00000021 Obi.ObiClothBlueprint Obi.ObiCloth::get_clothBlueprint()
extern void ObiCloth_get_clothBlueprint_m22D8CA7D61C5673611EA6B69A1E3EF73775F747D (void);
// 0x00000022 System.Void Obi.ObiCloth::set_clothBlueprint(Obi.ObiClothBlueprint)
extern void ObiCloth_set_clothBlueprint_mF20722E74218D592AD1BD5B615A1AF313A5D7BB3 (void);
// 0x00000023 System.Void Obi.ObiCloth::OnValidate()
extern void ObiCloth_OnValidate_m3B202D1B0868CEB000DF9CB98FFE2D774AD9292F (void);
// 0x00000024 System.Void Obi.ObiCloth::SetupRuntimeConstraints()
extern void ObiCloth_SetupRuntimeConstraints_m9CF1D0CE9EBC404E1B3BDE39409A1442BE2FC12B (void);
// 0x00000025 System.Void Obi.ObiCloth::.ctor()
extern void ObiCloth__ctor_m59769A9BE3F80E01A7E331C399FA4B87107157D8 (void);
// 0x00000026 Obi.ObiClothBlueprintBase Obi.ObiClothBase::get_clothBlueprintBase()
// 0x00000027 System.Boolean Obi.ObiClothBase::get_oneSided()
extern void ObiClothBase_get_oneSided_mD70AEC182F1158BD94AE795304C61F3FF624B22B (void);
// 0x00000028 System.Void Obi.ObiClothBase::set_oneSided(System.Boolean)
extern void ObiClothBase_set_oneSided_m35A3CBFA50E8BE8C57D0C9FD76D3E09F354ABC45 (void);
// 0x00000029 System.Boolean Obi.ObiClothBase::get_selfCollisions()
extern void ObiClothBase_get_selfCollisions_m3316717111097A8312FD5C906902D32B68709B3F (void);
// 0x0000002A System.Void Obi.ObiClothBase::set_selfCollisions(System.Boolean)
extern void ObiClothBase_set_selfCollisions_m8A370E52621E4302CC5E5E690FA69B98E20F8507 (void);
// 0x0000002B System.Boolean Obi.ObiClothBase::get_distanceConstraintsEnabled()
extern void ObiClothBase_get_distanceConstraintsEnabled_m4601252C285EB72E25A0CE60A0A36A24EFA65CD9 (void);
// 0x0000002C System.Void Obi.ObiClothBase::set_distanceConstraintsEnabled(System.Boolean)
extern void ObiClothBase_set_distanceConstraintsEnabled_mED671045E4ABE4D641713A4AE48C0EF46D8D1BF5 (void);
// 0x0000002D System.Single Obi.ObiClothBase::get_stretchingScale()
extern void ObiClothBase_get_stretchingScale_m3728F960D7C92A00FBC730E4189845867C7FE488 (void);
// 0x0000002E System.Void Obi.ObiClothBase::set_stretchingScale(System.Single)
extern void ObiClothBase_set_stretchingScale_mFAD7D559898240B97A4102EC8707680EFC5555E2 (void);
// 0x0000002F System.Single Obi.ObiClothBase::get_stretchCompliance()
extern void ObiClothBase_get_stretchCompliance_mBD676958E67DA78D3C0E792D0756D5FBB787DC3E (void);
// 0x00000030 System.Void Obi.ObiClothBase::set_stretchCompliance(System.Single)
extern void ObiClothBase_set_stretchCompliance_mF14EFFC283A72116B88CDB0EDB50480B8CE4DA36 (void);
// 0x00000031 System.Single Obi.ObiClothBase::get_maxCompression()
extern void ObiClothBase_get_maxCompression_mFB1C3072962E4E09DC50E9B025AF9BB8879D4651 (void);
// 0x00000032 System.Void Obi.ObiClothBase::set_maxCompression(System.Single)
extern void ObiClothBase_set_maxCompression_m143FFD92728A58676A95FD6E58B9110708BC0C62 (void);
// 0x00000033 System.Boolean Obi.ObiClothBase::get_bendConstraintsEnabled()
extern void ObiClothBase_get_bendConstraintsEnabled_m337B0D155CE52522C5B0E674FA4B127947D9D79C (void);
// 0x00000034 System.Void Obi.ObiClothBase::set_bendConstraintsEnabled(System.Boolean)
extern void ObiClothBase_set_bendConstraintsEnabled_mB5D4BF79F2749688D1FAC07F5638B69B801EEB6D (void);
// 0x00000035 System.Single Obi.ObiClothBase::get_bendCompliance()
extern void ObiClothBase_get_bendCompliance_m6A24CEECE44271FE829FDFFAC8727B7F81471646 (void);
// 0x00000036 System.Void Obi.ObiClothBase::set_bendCompliance(System.Single)
extern void ObiClothBase_set_bendCompliance_mF61722E51AA54B49A0859B909A533F940E9F3C84 (void);
// 0x00000037 System.Single Obi.ObiClothBase::get_maxBending()
extern void ObiClothBase_get_maxBending_mE6CFA04ED2D8B3D677D3E91B350F3A0D3EC95AC7 (void);
// 0x00000038 System.Void Obi.ObiClothBase::set_maxBending(System.Single)
extern void ObiClothBase_set_maxBending_mF3317AB601BA6892AFBD3C59BC3B5A34BE2941AE (void);
// 0x00000039 System.Single Obi.ObiClothBase::get_plasticYield()
extern void ObiClothBase_get_plasticYield_m994319A5C5E6F6948B692E49515541685C8DEBCE (void);
// 0x0000003A System.Void Obi.ObiClothBase::set_plasticYield(System.Single)
extern void ObiClothBase_set_plasticYield_mD095BACD7A69F02685492595BE21FCE3C027379C (void);
// 0x0000003B System.Single Obi.ObiClothBase::get_plasticCreep()
extern void ObiClothBase_get_plasticCreep_m6CCCC3185278C02F21103D0EE75965BA8211E984 (void);
// 0x0000003C System.Void Obi.ObiClothBase::set_plasticCreep(System.Single)
extern void ObiClothBase_set_plasticCreep_mC6A1D231779CAFBB2CD4ADDADC809CE906B1697F (void);
// 0x0000003D System.Boolean Obi.ObiClothBase::get_aerodynamicsEnabled()
extern void ObiClothBase_get_aerodynamicsEnabled_m724A14548F69698E0BFFF0AAFC7D4B411ABF945D (void);
// 0x0000003E System.Void Obi.ObiClothBase::set_aerodynamicsEnabled(System.Boolean)
extern void ObiClothBase_set_aerodynamicsEnabled_mAB56C3284C124AD9FB53324C0213C15CBB3A9BAC (void);
// 0x0000003F System.Single Obi.ObiClothBase::get_drag()
extern void ObiClothBase_get_drag_m6EB40AA1CD217C1A045B7F971E43F1FFCA77DFEE (void);
// 0x00000040 System.Void Obi.ObiClothBase::set_drag(System.Single)
extern void ObiClothBase_set_drag_m3A94A2857A88C693D44C9A98EDA52D84576D73FA (void);
// 0x00000041 System.Single Obi.ObiClothBase::get_lift()
extern void ObiClothBase_get_lift_mA8E8E40DB7ED5E81CCA5708BE1DB7E4DD659FC2C (void);
// 0x00000042 System.Void Obi.ObiClothBase::set_lift(System.Single)
extern void ObiClothBase_set_lift_mD9F8E4B6C202CFEAE78037A4D94A9780996460CC (void);
// 0x00000043 System.Boolean Obi.ObiClothBase::get_usesCustomExternalForces()
extern void ObiClothBase_get_usesCustomExternalForces_m753BC6385F9CD3EE74C1C1A6310E88B2B3C8CF5B (void);
// 0x00000044 System.Void Obi.ObiClothBase::LoadBlueprint(Obi.ObiSolver)
extern void ObiClothBase_LoadBlueprint_m3E9338E2C81996AF06788691FFFD6C8CD5408ED9 (void);
// 0x00000045 System.Void Obi.ObiClothBase::UnloadBlueprint(Obi.ObiSolver)
extern void ObiClothBase_UnloadBlueprint_mB0BF0CCDC63B0BE15DDED6E289496145E3845945 (void);
// 0x00000046 System.Void Obi.ObiClothBase::UpdateDeformableTriangles()
extern void ObiClothBase_UpdateDeformableTriangles_m3B671D3EA48D5EBD4912BA193E66E4CA3002F4B2 (void);
// 0x00000047 System.Void Obi.ObiClothBase::.ctor()
extern void ObiClothBase__ctor_mFBFF1E5D726CB95C999773DDC4612FCA25CFE720 (void);
// 0x00000048 Obi.ObiActorBlueprint Obi.ObiSkinnedCloth::get_sourceBlueprint()
extern void ObiSkinnedCloth_get_sourceBlueprint_mEA27126BCC672129A3B7766332D29093D6175A0C (void);
// 0x00000049 Obi.ObiClothBlueprintBase Obi.ObiSkinnedCloth::get_clothBlueprintBase()
extern void ObiSkinnedCloth_get_clothBlueprintBase_m1529D289F4BA3FE8764D347287236A95554CDD7C (void);
// 0x0000004A System.Boolean Obi.ObiSkinnedCloth::get_tetherConstraintsEnabled()
extern void ObiSkinnedCloth_get_tetherConstraintsEnabled_mD12EB38B8977C736CE07460F124A196C36F5DB14 (void);
// 0x0000004B System.Void Obi.ObiSkinnedCloth::set_tetherConstraintsEnabled(System.Boolean)
extern void ObiSkinnedCloth_set_tetherConstraintsEnabled_m3CCDAA09DC9CC778AC07FE673FD77F317B4B501F (void);
// 0x0000004C System.Single Obi.ObiSkinnedCloth::get_tetherCompliance()
extern void ObiSkinnedCloth_get_tetherCompliance_mD2021535E9B9A78E884EDB0946714D91F3187568 (void);
// 0x0000004D System.Void Obi.ObiSkinnedCloth::set_tetherCompliance(System.Single)
extern void ObiSkinnedCloth_set_tetherCompliance_mCCB51CFECD12168274126EFACA6AAFEFC7A8AFDC (void);
// 0x0000004E System.Single Obi.ObiSkinnedCloth::get_tetherScale()
extern void ObiSkinnedCloth_get_tetherScale_mFB6726B6BBE3DE8FABC0B2B7F3AADFAED14D2114 (void);
// 0x0000004F System.Void Obi.ObiSkinnedCloth::set_tetherScale(System.Single)
extern void ObiSkinnedCloth_set_tetherScale_mFE25D56BB1ED73746407D1D6D6692EE85B663E94 (void);
// 0x00000050 Obi.ObiSkinnedClothBlueprint Obi.ObiSkinnedCloth::get_skinnedClothBlueprint()
extern void ObiSkinnedCloth_get_skinnedClothBlueprint_mA690E5883D4C2B37BAB96C95503A3EE6637982B0 (void);
// 0x00000051 System.Void Obi.ObiSkinnedCloth::set_skinnedClothBlueprint(Obi.ObiSkinnedClothBlueprint)
extern void ObiSkinnedCloth_set_skinnedClothBlueprint_m5195445F8A791353581BED6D6E9EDA7A13E2665A (void);
// 0x00000052 System.Void Obi.ObiSkinnedCloth::LoadBlueprint(Obi.ObiSolver)
extern void ObiSkinnedCloth_LoadBlueprint_mAC4B5A67AB01888FEEC4332E55A189DE06E87AB7 (void);
// 0x00000053 System.Void Obi.ObiSkinnedCloth::OnValidate()
extern void ObiSkinnedCloth_OnValidate_m0B636B2CB110096F30FBA06ACB8E969EABC5DE0A (void);
// 0x00000054 System.Void Obi.ObiSkinnedCloth::SetupRuntimeConstraints()
extern void ObiSkinnedCloth_SetupRuntimeConstraints_m091CE68392FD98A827C1C59C4DCA50561E004829 (void);
// 0x00000055 System.Void Obi.ObiSkinnedCloth::OnEnable()
extern void ObiSkinnedCloth_OnEnable_m595FAA6BF4FEC864A174F2A575F973DC0D381B6A (void);
// 0x00000056 System.Void Obi.ObiSkinnedCloth::OnDisable()
extern void ObiSkinnedCloth_OnDisable_m7F2D77F05ACBCFB4AC6DA60D063FE3B81C3FD95B (void);
// 0x00000057 System.Void Obi.ObiSkinnedCloth::CreateBakedMeshIfNeeded()
extern void ObiSkinnedCloth_CreateBakedMeshIfNeeded_m929E3794A473BD8A2B921C78A62080A4611D690C (void);
// 0x00000058 System.Void Obi.ObiSkinnedCloth::SetTargetSkin()
extern void ObiSkinnedCloth_SetTargetSkin_mF7DFD21C1B3AB7F6DA8E89C7F62F420DDFDCBFC0 (void);
// 0x00000059 System.Void Obi.ObiSkinnedCloth::BeginStep(System.Single)
extern void ObiSkinnedCloth_BeginStep_m814BE1E13A2F650E6C27469A2B40424E12C5DE03 (void);
// 0x0000005A System.Void Obi.ObiSkinnedCloth::.ctor()
extern void ObiSkinnedCloth__ctor_m057BEF1BBD87BD492324A35C2AA3357D0EE2B9A4 (void);
// 0x0000005B System.Void Obi.ObiSkinnedCloth::.cctor()
extern void ObiSkinnedCloth__cctor_m39C48061A17FE019D7650FFF7E64535F32706BA3 (void);
// 0x0000005C Obi.ObiActorBlueprint Obi.ObiTearableCloth::get_sourceBlueprint()
extern void ObiTearableCloth_get_sourceBlueprint_m6CB5BE13DF36F76CEEFE42C7C4C8BDF5BC4AD117 (void);
// 0x0000005D Obi.ObiClothBlueprintBase Obi.ObiTearableCloth::get_clothBlueprintBase()
extern void ObiTearableCloth_get_clothBlueprintBase_m595CBA788EF9AC2D4859366FD4CF1BB4D507A355 (void);
// 0x0000005E Obi.ObiTearableClothBlueprint Obi.ObiTearableCloth::get_clothBlueprint()
extern void ObiTearableCloth_get_clothBlueprint_m193D8050E3EB29ECEBCCC13800A4A65A3E74DEF8 (void);
// 0x0000005F System.Void Obi.ObiTearableCloth::set_clothBlueprint(Obi.ObiTearableClothBlueprint)
extern void ObiTearableCloth_set_clothBlueprint_m00BF6A7834CEF4D3CD1DB79859D2B630D31B6EA1 (void);
// 0x00000060 System.Void Obi.ObiTearableCloth::add_OnClothTorn(Obi.ObiTearableCloth/ClothTornCallback)
extern void ObiTearableCloth_add_OnClothTorn_mEC85AE271BAB9CB16C6BC740BE02F2CCD1389C60 (void);
// 0x00000061 System.Void Obi.ObiTearableCloth::remove_OnClothTorn(Obi.ObiTearableCloth/ClothTornCallback)
extern void ObiTearableCloth_remove_OnClothTorn_m17F5D5C94C1B7F8324CD95EFF0B3F1430A7D3412 (void);
// 0x00000062 System.Void Obi.ObiTearableCloth::LoadBlueprint(Obi.ObiSolver)
extern void ObiTearableCloth_LoadBlueprint_m01F84142D7B5FB8F36CAD4764F8E6550F4050D03 (void);
// 0x00000063 System.Void Obi.ObiTearableCloth::UnloadBlueprint(Obi.ObiSolver)
extern void ObiTearableCloth_UnloadBlueprint_m1D1CB5E27C2344920DA9E8C18AF56A9305F540C6 (void);
// 0x00000064 System.Void Obi.ObiTearableCloth::SetupRuntimeConstraints()
extern void ObiTearableCloth_SetupRuntimeConstraints_m6949CC983A52F1087A8C712DE0D90DC56DCAE2A4 (void);
// 0x00000065 System.Void Obi.ObiTearableCloth::OnValidate()
extern void ObiTearableCloth_OnValidate_m0317CACD2EFF69F63731391B3C415EBC2EEF2310 (void);
// 0x00000066 System.Void Obi.ObiTearableCloth::Substep(System.Single)
extern void ObiTearableCloth_Substep_m1D8B9E930B2F6EA0EE13FE954E97DF40E71A17AB (void);
// 0x00000067 System.Void Obi.ObiTearableCloth::ApplyTearing(System.Single)
extern void ObiTearableCloth_ApplyTearing_mACF70C409F46C950A27B82B444F069AC300735B7 (void);
// 0x00000068 System.Boolean Obi.ObiTearableCloth::Tear(Obi.StructuralConstraint)
extern void ObiTearableCloth_Tear_m289AE8187944FCF0C03BF505839AA7CE8B8914C6 (void);
// 0x00000069 System.Boolean Obi.ObiTearableCloth::TopologySplitAttempt(System.Int32&,System.Int32&,UnityEngine.Vector3&,UnityEngine.Vector3&,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_TopologySplitAttempt_m4A20D931C00BBE2E52327AC19AE5B67F6E7983DA (void);
// 0x0000006A System.Void Obi.ObiTearableCloth::SplitParticle(System.Int32)
extern void ObiTearableCloth_SplitParticle_mD2C7D1B54B81088B454D028CA36258217934B276 (void);
// 0x0000006B System.Void Obi.ObiTearableCloth::WeakenCutPoint(System.Int32,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiTearableCloth_WeakenCutPoint_m96AA5929E23DDA50FD537F34C063075229677E83 (void);
// 0x0000006C System.Void Obi.ObiTearableCloth::ClassifyFaces(Obi.HalfEdgeMesh/Vertex,UnityEngine.Plane,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableCloth_ClassifyFaces_mA5E6121C7F2E7899FBF785747F3A76F7F28440C7 (void);
// 0x0000006D System.Boolean Obi.ObiTearableCloth::SplitTopologyAtVertex(System.Int32,UnityEngine.Plane,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>,System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_SplitTopologyAtVertex_m8D9CF0809D8C17C08D9E62602F91B2A6A48F88C4 (void);
// 0x0000006E System.Void Obi.ObiTearableCloth::UpdateTornDistanceConstraints(System.Collections.Generic.HashSet`1<System.Int32>)
extern void ObiTearableCloth_UpdateTornDistanceConstraints_m33C4A08366B00828983FA36C27B1B5889224A4A0 (void);
// 0x0000006F System.Void Obi.ObiTearableCloth::UpdateTornBendConstraints(System.Int32)
extern void ObiTearableCloth_UpdateTornBendConstraints_m90DEF8C2E331F3E980AD2011D6557CB4D893FEBB (void);
// 0x00000070 System.Void Obi.ObiTearableCloth::UpdateDeformableTriangles()
extern void ObiTearableCloth_UpdateDeformableTriangles_m295EE9DFB4BB9EA8BB2FCD068BDC3C8355D503FB (void);
// 0x00000071 System.Void Obi.ObiTearableCloth::OnDrawGizmosSelected()
extern void ObiTearableCloth_OnDrawGizmosSelected_mDE475E90A8FB156D41ABE5EEED44802420EDE9E8 (void);
// 0x00000072 System.Void Obi.ObiTearableCloth::Update()
extern void ObiTearableCloth_Update_m4D345AF05C968480C4DA0AEAB8692DA006AD2F36 (void);
// 0x00000073 System.Void Obi.ObiTearableCloth::.ctor()
extern void ObiTearableCloth__ctor_mDA99DC00FDF966DA559BE6F1FD3FB84E41B25B15 (void);
// 0x00000074 System.Void Obi.ObiTearableCloth::.cctor()
extern void ObiTearableCloth__cctor_mD504737A0AAECBAD232B27500F7C483E3345F930 (void);
// 0x00000075 System.Void Obi.ObiTearableCloth/ClothTornCallback::.ctor(System.Object,System.IntPtr)
extern void ClothTornCallback__ctor_mE57B91D6E263EA221779102651926B607AD29B76 (void);
// 0x00000076 System.Void Obi.ObiTearableCloth/ClothTornCallback::Invoke(Obi.ObiTearableCloth,Obi.ObiTearableCloth/ObiClothTornEventArgs)
extern void ClothTornCallback_Invoke_mAC61E111032A0591234652E701263A8EF39427F3 (void);
// 0x00000077 System.IAsyncResult Obi.ObiTearableCloth/ClothTornCallback::BeginInvoke(Obi.ObiTearableCloth,Obi.ObiTearableCloth/ObiClothTornEventArgs,System.AsyncCallback,System.Object)
extern void ClothTornCallback_BeginInvoke_mFB0788D7797D354E91A6283F4881539D4AF7609F (void);
// 0x00000078 System.Void Obi.ObiTearableCloth/ClothTornCallback::EndInvoke(System.IAsyncResult)
extern void ClothTornCallback_EndInvoke_m109D873CDD373C69D32A0CC8FE4931B030581F85 (void);
// 0x00000079 System.Void Obi.ObiTearableCloth/ObiClothTornEventArgs::.ctor(Obi.StructuralConstraint,System.Int32,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiClothTornEventArgs__ctor_m709EC7E4FD2C7B0270B4D796A041BD58600E837E (void);
// 0x0000007A System.Void Obi.ObiTearableCloth/<>c::.cctor()
extern void U3CU3Ec__cctor_mDAC97FBBE782103672E33A1391E0ED9F7378FA58 (void);
// 0x0000007B System.Void Obi.ObiTearableCloth/<>c::.ctor()
extern void U3CU3Ec__ctor_m338AE04294412318948CB35D87CB59FF12519F2B (void);
// 0x0000007C System.Int32 Obi.ObiTearableCloth/<>c::<ApplyTearing>b__24_0(Obi.StructuralConstraint,Obi.StructuralConstraint)
extern void U3CU3Ec_U3CApplyTearingU3Eb__24_0_m44431CF342F4B5F112B7814C9FED0466FAB2BCB7 (void);
// 0x0000007D System.Boolean Obi.ObiClothBlueprint::get_usesTethers()
extern void ObiClothBlueprint_get_usesTethers_m36D5FA3046CEFA83F43D10DBCCEE291D95504E31 (void);
// 0x0000007E System.Collections.IEnumerator Obi.ObiClothBlueprint::Initialize()
extern void ObiClothBlueprint_Initialize_mC821201D6C73FD74172917F4B47D86E247436637 (void);
// 0x0000007F System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateDistanceConstraints()
extern void ObiClothBlueprint_CreateDistanceConstraints_m2BD2777172DB698116B46D988858D97A785DB7E5 (void);
// 0x00000080 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateAerodynamicConstraints()
extern void ObiClothBlueprint_CreateAerodynamicConstraints_mA4996455BBEC0570529FF2ADE79B56A7EFA29FC9 (void);
// 0x00000081 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateBendingConstraints()
extern void ObiClothBlueprint_CreateBendingConstraints_m7D28254A6C5BDF54AAEA2C1324D5A702CF2982D6 (void);
// 0x00000082 System.Collections.IEnumerator Obi.ObiClothBlueprint::CreateVolumeConstraints()
extern void ObiClothBlueprint_CreateVolumeConstraints_mCAD1E4ED571A93814AF8AD17D926B0065C30E6E1 (void);
// 0x00000083 System.Void Obi.ObiClothBlueprint::ClearTethers()
extern void ObiClothBlueprint_ClearTethers_mC2770ED44BD0538B446CBE7E0BED8C06E723FA62 (void);
// 0x00000084 System.Collections.Generic.List`1<System.Collections.Generic.HashSet`1<System.Int32>> Obi.ObiClothBlueprint::GenerateIslands(System.Collections.Generic.IEnumerable`1<System.Int32>,System.Func`2<System.Int32,System.Boolean>)
extern void ObiClothBlueprint_GenerateIslands_mC84BD0A563E23FC794DD5D874217965A852FFEB0 (void);
// 0x00000085 System.Void Obi.ObiClothBlueprint::GenerateTethers(System.Boolean[])
extern void ObiClothBlueprint_GenerateTethers_m19035D0B19AA667D7F72DFCB325808F08A9F5379 (void);
// 0x00000086 System.Void Obi.ObiClothBlueprint::GenerateTethersForIsland(System.Collections.Generic.HashSet`1<System.Int32>,System.Collections.Generic.List`1<System.Int32>,System.Boolean[],System.Int32)
extern void ObiClothBlueprint_GenerateTethersForIsland_mB667EEE465392D42D60EDAA37251B1D0B7347C00 (void);
// 0x00000087 System.Void Obi.ObiClothBlueprint::.ctor()
extern void ObiClothBlueprint__ctor_m330710D1D475B3F870467803C92D8EB11FFCD945 (void);
// 0x00000088 System.Void Obi.ObiClothBlueprint/<Initialize>d__2::.ctor(System.Int32)
extern void U3CInitializeU3Ed__2__ctor_m03020FC6FD2B9D269917BB6F7C9C0DCDDD6E2561 (void);
// 0x00000089 System.Void Obi.ObiClothBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__2_System_IDisposable_Dispose_mB05A3DA48C46FB54386164416A68FF35C8BA81E4 (void);
// 0x0000008A System.Boolean Obi.ObiClothBlueprint/<Initialize>d__2::MoveNext()
extern void U3CInitializeU3Ed__2_MoveNext_m302A69E08AE7BAFFD3224ABE919F80A158AAFF51 (void);
// 0x0000008B System.Object Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE086F200365532FA29B83AD2B3595ADFA22EE6F (void);
// 0x0000008C System.Void Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m58F645AA9A0B4F194919051D97FF764435010ACE (void);
// 0x0000008D System.Object Obi.ObiClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m03BD8D7ED8FEA24F9BF18D57D1C26C463A2C80E6 (void);
// 0x0000008E System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::.ctor(System.Int32)
extern void U3CCreateDistanceConstraintsU3Ed__3__ctor_m2165A7905B75F686B6EB79C8360F82705A43F002 (void);
// 0x0000008F System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.IDisposable.Dispose()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m2EA9DF5AFFD56BB4A01414F8033C8552E0A17F63 (void);
// 0x00000090 System.Boolean Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::MoveNext()
extern void U3CCreateDistanceConstraintsU3Ed__3_MoveNext_mEFD23D0412B26FF3359D8B28631C6776A544E5CC (void);
// 0x00000091 System.Object Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FFABC8D386190AD1252F8010CA37A1129988477 (void);
// 0x00000092 System.Void Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m9A9A3D28EE82C1C9CC440756268313182A28769C (void);
// 0x00000093 System.Object Obi.ObiClothBlueprint/<CreateDistanceConstraints>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_mE7958E068EB74DFDA3BA778B8905AE86BF1F5D01 (void);
// 0x00000094 System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::.ctor(System.Int32)
extern void U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m31B15C5B17B8C34F9C4714EAA95B881AC32C8504 (void);
// 0x00000095 System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.IDisposable.Dispose()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_mE2C279ABF51A608163FDBBE4930C00D8A661F3FF (void);
// 0x00000096 System.Boolean Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::MoveNext()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_MoveNext_m05541FD655F08F60A1A857669DFAEFE0B2D3CBFF (void);
// 0x00000097 System.Object Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF2E2D9440AA0D26FAD832543DE1FBBAF22D475E (void);
// 0x00000098 System.Void Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.IEnumerator.Reset()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8DB140EE89987CF9D9975939A2B10C4D5771B8D7 (void);
// 0x00000099 System.Object Obi.ObiClothBlueprint/<CreateAerodynamicConstraints>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m64BFC0A639CF8F425C2E6E6EAC54E60AE3B8ACD1 (void);
// 0x0000009A System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::.ctor(System.Int32)
extern void U3CCreateBendingConstraintsU3Ed__5__ctor_m7A3788831C438FF52B984C65426E7055DCEA2F20 (void);
// 0x0000009B System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.IDisposable.Dispose()
extern void U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_mABA1606677C79BDD2FD9556D6963A1CF4C71299A (void);
// 0x0000009C System.Boolean Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::MoveNext()
extern void U3CCreateBendingConstraintsU3Ed__5_MoveNext_m19B7E25316061D964D46A367D68C16625B7778BC (void);
// 0x0000009D System.Object Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6DA62A242C5BB4F7999881ACA8384398B2E643D (void);
// 0x0000009E System.Void Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.IEnumerator.Reset()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m99008933F0F99D282B7DD6010E088384DE087F4F (void);
// 0x0000009F System.Object Obi.ObiClothBlueprint/<CreateBendingConstraints>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m630DC74A3B6FCD1C00ACE9CDB5D9C339BE81CBF3 (void);
// 0x000000A0 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::.ctor(System.Int32)
extern void U3CCreateVolumeConstraintsU3Ed__6__ctor_m726B8020E13DD7A4EBE916CFFD8292CA4472C3EA (void);
// 0x000000A1 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.IDisposable.Dispose()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m1E0A92935E3227A8A889157CD421FEA0379E0E39 (void);
// 0x000000A2 System.Boolean Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::MoveNext()
extern void U3CCreateVolumeConstraintsU3Ed__6_MoveNext_m801293BF179BA8640C7868766412FA4A5810AFBC (void);
// 0x000000A3 System.Object Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE255CE246691B00B58B7C27281A458B24CD10D1 (void);
// 0x000000A4 System.Void Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.IEnumerator.Reset()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m5F9AAB4548AF92F079FF16DEB9DCCFEEB1FB9817 (void);
// 0x000000A5 System.Object Obi.ObiClothBlueprint/<CreateVolumeConstraints>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m779B3E79795A7A851B5F2080D095ADC06C5A6F3B (void);
// 0x000000A6 System.Void Obi.ObiClothBlueprint/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_mCA4BB7EFB4202D511569ED75001DA257335DC03F (void);
// 0x000000A7 System.Boolean Obi.ObiClothBlueprint/<>c__DisplayClass10_0::<GenerateTethersForIsland>b__0(System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CGenerateTethersForIslandU3Eb__0_m228EA147201D2F54ADE2A350E7D361AB7A692DAC (void);
// 0x000000A8 System.Void Obi.ObiClothBlueprint/<>c::.cctor()
extern void U3CU3Ec__cctor_m81E0D9C2C844297DC2CC6D386D6D18574FE7A25B (void);
// 0x000000A9 System.Void Obi.ObiClothBlueprint/<>c::.ctor()
extern void U3CU3Ec__ctor_m44DBDA334931038409B37CCDEE60D49A314A030F (void);
// 0x000000AA System.Int32 Obi.ObiClothBlueprint/<>c::<GenerateTethersForIsland>b__10_1(System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>,System.Collections.Generic.KeyValuePair`2<System.Single,System.Int32>)
extern void U3CU3Ec_U3CGenerateTethersForIslandU3Eb__10_1_m9204F6299DB15BA2066909BE3A5992FF811516FD (void);
// 0x000000AB Obi.HalfEdgeMesh Obi.ObiClothBlueprintBase::get_Topology()
extern void ObiClothBlueprintBase_get_Topology_mE35630E1A8F1D4FF4FDFBD1B55B0FD135296FD81 (void);
// 0x000000AC System.Void Obi.ObiClothBlueprintBase::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiClothBlueprintBase_SwapWithFirstInactiveParticle_m7487298098E3AB9C23B657865FF874134445D407 (void);
// 0x000000AD System.Collections.IEnumerator Obi.ObiClothBlueprintBase::GenerateDeformableTriangles()
extern void ObiClothBlueprintBase_GenerateDeformableTriangles_m57C68962E620B96908388CAC9A952DCA3D3B11C4 (void);
// 0x000000AE System.Collections.IEnumerator Obi.ObiClothBlueprintBase::CreateSimplices()
extern void ObiClothBlueprintBase_CreateSimplices_m4B89261638E31CE9D9046BE2D1C3DB23D31671BF (void);
// 0x000000AF System.Void Obi.ObiClothBlueprintBase::.ctor()
extern void ObiClothBlueprintBase__ctor_m448379F1F34C2F69E0B2EA0D90177AD5B683682B (void);
// 0x000000B0 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::.ctor(System.Int32)
extern void U3CGenerateDeformableTrianglesU3Ed__8__ctor_m8139DC43596BD55214B1F9CBF79B61334C4FB303 (void);
// 0x000000B1 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.IDisposable.Dispose()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m74C4415080168350829FE8A5DD9AF9A5405F3A27 (void);
// 0x000000B2 System.Boolean Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::MoveNext()
extern void U3CGenerateDeformableTrianglesU3Ed__8_MoveNext_mF5112455130FD61016C1CBF03E44AC08829DDEF6 (void);
// 0x000000B3 System.Object Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5408ABF70D4F1A55E05421689FBAB42C98EE617C (void);
// 0x000000B4 System.Void Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.IEnumerator.Reset()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mA8110BD6BE76597A39B0A88E3711CC5489FEC896 (void);
// 0x000000B5 System.Object Obi.ObiClothBlueprintBase/<GenerateDeformableTriangles>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mBB5E21B2BE3291406AE669215C5192ECAFDA9059 (void);
// 0x000000B6 System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::.ctor(System.Int32)
extern void U3CCreateSimplicesU3Ed__9__ctor_m6AF7C34D51F0C9027A94D895000B742F12D6D2DE (void);
// 0x000000B7 System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.IDisposable.Dispose()
extern void U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_mD8BA44B90AAC0DE11F3A0431ED1ACE334A04BBC5 (void);
// 0x000000B8 System.Boolean Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::MoveNext()
extern void U3CCreateSimplicesU3Ed__9_MoveNext_mC9D009E251058DE1A68616F58685DF9095E69957 (void);
// 0x000000B9 System.Object Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5ADA0BBC36D0A236F6D0091810A0FE9E92A1370A (void);
// 0x000000BA System.Void Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.IEnumerator.Reset()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_mD668CF36AEB83620A2FE2A8155CA813A29421AAF (void);
// 0x000000BB System.Object Obi.ObiClothBlueprintBase/<CreateSimplices>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_m46A43033D3499E23FCD045DE9D84E32018AE2A31 (void);
// 0x000000BC System.Boolean Obi.ObiSkinnedClothBlueprint::get_usesTethers()
extern void ObiSkinnedClothBlueprint_get_usesTethers_mFB6A1F8DB9337CD20D7A3A45EE2F6A4378095FAF (void);
// 0x000000BD System.Collections.IEnumerator Obi.ObiSkinnedClothBlueprint::Initialize()
extern void ObiSkinnedClothBlueprint_Initialize_m9C44FD17E8A31CB5BC36A27812FBB9B88EB4D271 (void);
// 0x000000BE System.Collections.IEnumerator Obi.ObiSkinnedClothBlueprint::CreateSkinConstraints()
extern void ObiSkinnedClothBlueprint_CreateSkinConstraints_m301723D10057502260D046132DEE8E69A67B435B (void);
// 0x000000BF System.Void Obi.ObiSkinnedClothBlueprint::.ctor()
extern void ObiSkinnedClothBlueprint__ctor_m057C902844020886240D9E8330F28EE4114EB6BE (void);
// 0x000000C0 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::.ctor(System.Int32)
extern void U3CInitializeU3Ed__2__ctor_m74C4F5F3BD150B334802008C977E713FBD745FB5 (void);
// 0x000000C1 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__2_System_IDisposable_Dispose_m5D2AC4D1DBA6F2CD98525165EA40923897FC26AD (void);
// 0x000000C2 System.Boolean Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::MoveNext()
extern void U3CInitializeU3Ed__2_MoveNext_m1C75D9114926E161FCE046178551F95199924AC5 (void);
// 0x000000C3 System.Object Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m060C078F5C58E548AE053F47B2665FA769D33F47 (void);
// 0x000000C4 System.Void Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m1DB56CACC6E4B0C01726F74F013589216D1735DE (void);
// 0x000000C5 System.Object Obi.ObiSkinnedClothBlueprint/<Initialize>d__2::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m18B2AB3F28D9FDDBCE4C5942EC35F5369CC76ED9 (void);
// 0x000000C6 System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::.ctor(System.Int32)
extern void U3CCreateSkinConstraintsU3Ed__3__ctor_mB82120AAC82D7FF18CAB6F1AE91B332B82248CDA (void);
// 0x000000C7 System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.IDisposable.Dispose()
extern void U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_mD6F28A76C96F00D36EDEAC9C4BC802EE5ECD61AB (void);
// 0x000000C8 System.Boolean Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::MoveNext()
extern void U3CCreateSkinConstraintsU3Ed__3_MoveNext_mD55B1CB130A747A517D392E7732C7D61E3FDF5DD (void);
// 0x000000C9 System.Object Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30F2FCAEF12F2DF55352FFC3801D6B17C37AF429 (void);
// 0x000000CA System.Void Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.IEnumerator.Reset()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m20863CA27EA5B0FE586DF972B4E33F93B44A94BB (void);
// 0x000000CB System.Object Obi.ObiSkinnedClothBlueprint/<CreateSkinConstraints>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m19EBD8D91F9C3AC56ABF53DCDA01F272833C0A39 (void);
// 0x000000CC System.Int32 Obi.ObiTearableClothBlueprint::get_PooledParticles()
extern void ObiTearableClothBlueprint_get_PooledParticles_m60E85C577CBB70DB1188ACADA0039ECA86E2A7B4 (void);
// 0x000000CD System.Boolean Obi.ObiTearableClothBlueprint::get_usesTethers()
extern void ObiTearableClothBlueprint_get_usesTethers_mB1DAB44D3B6E31310FDA017A73E952C65EA3617B (void);
// 0x000000CE System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::Initialize()
extern void ObiTearableClothBlueprint_Initialize_mAB87D7E2D2169C9E3A687285521300D251C5F9AE (void);
// 0x000000CF System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreateDistanceConstraints()
extern void ObiTearableClothBlueprint_CreateDistanceConstraints_m210056ABF39610D9AB58CD6EE2BB63B996BCB5EC (void);
// 0x000000D0 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreateInitialDistanceConstraints(System.Collections.Generic.List`1<System.Int32>)
extern void ObiTearableClothBlueprint_CreateInitialDistanceConstraints_m6454656AD1D745A2F16F5096035092B3FE4F0EEA (void);
// 0x000000D1 System.Collections.IEnumerator Obi.ObiTearableClothBlueprint::CreatePooledDistanceConstraints(System.Collections.Generic.List`1<System.Int32>)
extern void ObiTearableClothBlueprint_CreatePooledDistanceConstraints_m2CF68D6EB12EC0288BF7CF8114ED8A76475DF6B2 (void);
// 0x000000D2 System.Void Obi.ObiTearableClothBlueprint::.ctor()
extern void ObiTearableClothBlueprint__ctor_mA00BAF480E94328209D56224AF9ED316629EFF67 (void);
// 0x000000D3 System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::.ctor(System.Int32)
extern void U3CInitializeU3Ed__8__ctor_mC2EAE7F3E298D30D7B40374ACC5F94CFE70748A0 (void);
// 0x000000D4 System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.IDisposable.Dispose()
extern void U3CInitializeU3Ed__8_System_IDisposable_Dispose_m7EB8A87FD770A0114AC120446F9C7DEA7985EC63 (void);
// 0x000000D5 System.Boolean Obi.ObiTearableClothBlueprint/<Initialize>d__8::MoveNext()
extern void U3CInitializeU3Ed__8_MoveNext_m8B656167A50E5CEECCFBBF56B8DF0D5392E86206 (void);
// 0x000000D6 System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8CDEC853C25012FD10F0BC2D855DED5869E4C2A (void);
// 0x000000D7 System.Void Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.Reset()
extern void U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_m6D8AC93EB36AA485AC8BB64E73247E39A9DE163C (void);
// 0x000000D8 System.Object Obi.ObiTearableClothBlueprint/<Initialize>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m772F66F32F759B17DCE6424C75A648CEDADAF472 (void);
// 0x000000D9 System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::.ctor(System.Int32)
extern void U3CCreateDistanceConstraintsU3Ed__9__ctor_m10D767164F94A8F05622946A56AC89F60794383B (void);
// 0x000000DA System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.IDisposable.Dispose()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_m3BB519B14C38C38594DD385A14E70752FD80E054 (void);
// 0x000000DB System.Boolean Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::MoveNext()
extern void U3CCreateDistanceConstraintsU3Ed__9_MoveNext_m7AF80F3E34CC978EAFCBD9520147C772E0FB6FFA (void);
// 0x000000DC System.Object Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF05CDB7B6EB27DCFF38D57CFD55F7939B395E67 (void);
// 0x000000DD System.Void Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.IEnumerator.Reset()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mCB067718FF90B57B51B005763022474362D159A4 (void);
// 0x000000DE System.Object Obi.ObiTearableClothBlueprint/<CreateDistanceConstraints>d__9::System.Collections.IEnumerator.get_Current()
extern void U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_m069492497AABB68DD787B9B9485AAE79C7E7C62C (void);
// 0x000000DF System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::.ctor(System.Int32)
extern void U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_m860DEC39A10B619CB02D9A03F2DF1CC314CDD120 (void);
// 0x000000E0 System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.IDisposable.Dispose()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_mFD42077906845A3AC141AB186973761625D388E7 (void);
// 0x000000E1 System.Boolean Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::MoveNext()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_MoveNext_m82AC28B0879B660B4DD72663041CF314A0BDF1E5 (void);
// 0x000000E2 System.Object Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m720DB2F410849D5B3237A1C26AE0C4AB8445265C (void);
// 0x000000E3 System.Void Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.IEnumerator.Reset()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mFFCDAB2F7A98ED448B887DD0325488F2C7127922 (void);
// 0x000000E4 System.Object Obi.ObiTearableClothBlueprint/<CreateInitialDistanceConstraints>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9E5CEEF3F29FC86CEFACCA48DEAF6F99628CB4B (void);
// 0x000000E5 System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::.ctor(System.Int32)
extern void U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_mE5011620ABBE52261DCDF900C25BC60D46190AA2 (void);
// 0x000000E6 System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.IDisposable.Dispose()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m24E179D2396555CBC7931B006DCF382ED737882B (void);
// 0x000000E7 System.Boolean Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::MoveNext()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_MoveNext_mD5D31320D8EC8575810E179C3C84A3A3A3B13FB5 (void);
// 0x000000E8 System.Object Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2025D23C395F363B5CAD10BF8CF249A045569B39 (void);
// 0x000000E9 System.Void Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.IEnumerator.Reset()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_mA1DEFAA6A2DAE90CEA3649CFAD079C301E945D7E (void);
// 0x000000EA System.Object Obi.ObiTearableClothBlueprint/<CreatePooledDistanceConstraints>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_mD019F838649C96EFDFD97DB933FA80B0EDB22ED8 (void);
// 0x000000EB System.Boolean Obi.HalfEdgeMesh::get_ContainsData()
extern void HalfEdgeMesh_get_ContainsData_m67132D1DA820B0B33491E4AC1D297E286AD89EF8 (void);
// 0x000000EC System.Boolean Obi.HalfEdgeMesh::get_closed()
extern void HalfEdgeMesh_get_closed_m720512F243C8837E8D88E9968BF2CD3821DF1537 (void);
// 0x000000ED System.Single Obi.HalfEdgeMesh::get_area()
extern void HalfEdgeMesh_get_area_m1C5411A5FB6217ABBEE0BB0D797100389BEA5B19 (void);
// 0x000000EE System.Single Obi.HalfEdgeMesh::get_volume()
extern void HalfEdgeMesh_get_volume_m66F3CFA6E325A41041FF97835648CCE49E060118 (void);
// 0x000000EF System.Void Obi.HalfEdgeMesh::.ctor()
extern void HalfEdgeMesh__ctor_m2409753E6C585A36D56C0672DEC1E55A69F6D398 (void);
// 0x000000F0 System.Void Obi.HalfEdgeMesh::.ctor(Obi.HalfEdgeMesh)
extern void HalfEdgeMesh__ctor_mD56BB0A58E07D152D1DCC51258DFB64D49004AC9 (void);
// 0x000000F1 System.Void Obi.HalfEdgeMesh::Generate()
extern void HalfEdgeMesh_Generate_mAF8BDD70FF4421E7B85F14008044D837B6D4F7A6 (void);
// 0x000000F2 System.Void Obi.HalfEdgeMesh::CalculateRestNormals()
extern void HalfEdgeMesh_CalculateRestNormals_m06A0D9D64077F707BC0B122D9F9CC8AAAB524D4E (void);
// 0x000000F3 System.Void Obi.HalfEdgeMesh::CalculateRestOrientations()
extern void HalfEdgeMesh_CalculateRestOrientations_m441FCEE91D0C56AF7794397B519BF954EE3EDA88 (void);
// 0x000000F4 System.Void Obi.HalfEdgeMesh::SwapVertices(System.Int32,System.Int32)
extern void HalfEdgeMesh_SwapVertices_m8D108B9B2095522E59905652B47AFA3D67C891F5 (void);
// 0x000000F5 System.Int32 Obi.HalfEdgeMesh::GetHalfEdgeStartVertex(Obi.HalfEdgeMesh/HalfEdge)
extern void HalfEdgeMesh_GetHalfEdgeStartVertex_m1309E7CFD17A07D6C157110091E16E8292D8AC5F (void);
// 0x000000F6 System.Single Obi.HalfEdgeMesh::GetFaceArea(Obi.HalfEdgeMesh/Face)
extern void HalfEdgeMesh_GetFaceArea_m1D79061E5B7045231801CFA296B50DD764BFB9A1 (void);
// 0x000000F7 System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Vertex> Obi.HalfEdgeMesh::GetNeighbourVerticesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourVerticesEnumerator_m427D20E191E32FD38225DF431B627FC8A3B19AEE (void);
// 0x000000F8 System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh::GetNeighbourEdgesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourEdgesEnumerator_mAF5EEF10069CFFA9448C6F6D1623CFCCA46AA929 (void);
// 0x000000F9 System.Collections.Generic.IEnumerable`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh::GetNeighbourFacesEnumerator(Obi.HalfEdgeMesh/Vertex)
extern void HalfEdgeMesh_GetNeighbourFacesEnumerator_mB0158BBB50C411485D5C26138FF73FD4810552F4 (void);
// 0x000000FA System.Collections.Generic.List`1<System.Int32> Obi.HalfEdgeMesh::GetEdgeList()
extern void HalfEdgeMesh_GetEdgeList_m24EE36AC0B04D4371BA908FC9B54BD96800A75BC (void);
// 0x000000FB System.Boolean Obi.HalfEdgeMesh::IsSplit(System.Int32)
extern void HalfEdgeMesh_IsSplit_m14E0D14E21DACB497E5F0DBF78029E3C58707795 (void);
// 0x000000FC System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::.ctor(System.Int32)
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_m8156A28051972177616DED3CA97F451E45DC7185 (void);
// 0x000000FD System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.IDisposable.Dispose()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_m56584522A7EF9CD15577DCCE9BE90084A16CD5A0 (void);
// 0x000000FE System.Boolean Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::MoveNext()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_MoveNext_m02FE74929ECBFFAEE5E86605EC2D267D7C3965D3 (void);
// 0x000000FF Obi.HalfEdgeMesh/Vertex Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.Vertex>.get_Current()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_m27FD1A775C4A3F297B2C58E2092662AF69DC9148 (void);
// 0x00000100 System.Void Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_m55F703C661A4F8DFB933F0551AFF04714664738D (void);
// 0x00000101 System.Object Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_m8081115EDDE5A8D021B2AA5CBCAFE065EF0BA8FC (void);
// 0x00000102 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/Vertex> Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.Vertex>.GetEnumerator()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mE6B4FD37BF23C1F08AAD8FD8B9A5BF8A6326F21D (void);
// 0x00000103 System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourVerticesEnumerator>d__31::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_mE276303D862883120B0D06103592A23968708ED8 (void);
// 0x00000104 System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::.ctor(System.Int32)
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_m08CE33742096F1FDCCE51C2263BAE629222DFCE3 (void);
// 0x00000105 System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.IDisposable.Dispose()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m245661F2D84CE211EA54BB0B31B23356F18EC04C (void);
// 0x00000106 System.Boolean Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::MoveNext()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_MoveNext_m1B1E0CAEA67208793D9364DCA59781EF81C72FD2 (void);
// 0x00000107 Obi.HalfEdgeMesh/HalfEdge Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.HalfEdge>.get_Current()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_m841F1A67A81AF1D1DB8CC7819124DF002CC7CC74 (void);
// 0x00000108 System.Void Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_m6AF7B85B8F67F4D7CDF328F73FB9CEEC9E732040 (void);
// 0x00000109 System.Object Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_m2EB25C26C43A80B57D481AEA9F2E312B23627894 (void);
// 0x0000010A System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/HalfEdge> Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.HalfEdge>.GetEnumerator()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mBCAC9207E22122BBA14009BAB3F1CE81B49ECD51 (void);
// 0x0000010B System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourEdgesEnumerator>d__32::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m1A70C81B7E5B8F71ED2B8A19814DA990FD781B16 (void);
// 0x0000010C System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::.ctor(System.Int32)
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_mDE9CCAE52922A0A8B7CFA8EB1BAAAA1E1ED2CA3F (void);
// 0x0000010D System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.IDisposable.Dispose()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m54C2538A85F8AAD146B2AB888A05F5F4BCA70994 (void);
// 0x0000010E System.Boolean Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::MoveNext()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_MoveNext_mED91E0FBF0F8E0F417A1906E31CECB7ADC443773 (void);
// 0x0000010F Obi.HalfEdgeMesh/Face Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.Generic.IEnumerator<Obi.HalfEdgeMesh.Face>.get_Current()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_mFD88B3D02BA9FD24F49609448D6354DA0E2FD9FB (void);
// 0x00000110 System.Void Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerator.Reset()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_mB07B17F0F16CCF05606C34FDFCFBCBA9FDB398F0 (void);
// 0x00000111 System.Object Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_m7B5E8F3270ADED930E203C7F1DB3236D2E172478 (void);
// 0x00000112 System.Collections.Generic.IEnumerator`1<Obi.HalfEdgeMesh/Face> Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.Generic.IEnumerable<Obi.HalfEdgeMesh.Face>.GetEnumerator()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF33CEE5DFCEB560159726F7CE362A1E8AADA3982 (void);
// 0x00000113 System.Collections.IEnumerator Obi.HalfEdgeMesh/<GetNeighbourFacesEnumerator>d__33::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m07C6A65A9082750EDFCDB8D0EFC4DFDEB45826DD (void);
// 0x00000114 System.Void Obi.ObiTriangleSkinMap::set_master(Obi.ObiClothBlueprintBase)
extern void ObiTriangleSkinMap_set_master_m5E115D7718A8617482E1701CA18D20E3BE30FB6E (void);
// 0x00000115 Obi.ObiClothBlueprintBase Obi.ObiTriangleSkinMap::get_master()
extern void ObiTriangleSkinMap_get_master_mF49863E288BA9C99D1CEFDC9DF99F9A2623D82A7 (void);
// 0x00000116 System.Void Obi.ObiTriangleSkinMap::set_slave(UnityEngine.Mesh)
extern void ObiTriangleSkinMap_set_slave_mF120662565C78B207B2EE4AC41EB86AC7A4BF0D5 (void);
// 0x00000117 UnityEngine.Mesh Obi.ObiTriangleSkinMap::get_slave()
extern void ObiTriangleSkinMap_get_slave_mE8F13D556281070BEB26DFEF4969665CC4B78395 (void);
// 0x00000118 System.Void Obi.ObiTriangleSkinMap::OnEnable()
extern void ObiTriangleSkinMap_OnEnable_mF3970C48AE8A8C6F700F24F1878D1EBFC5CDE404 (void);
// 0x00000119 System.Void Obi.ObiTriangleSkinMap::Clear()
extern void ObiTriangleSkinMap_Clear_mF0B3406C85EC6D32B3CB0EAA0F802C3BAC2F4FA2 (void);
// 0x0000011A System.Void Obi.ObiTriangleSkinMap::ValidateMasterChannels(System.Boolean)
extern void ObiTriangleSkinMap_ValidateMasterChannels_m6954F42A5BD3621ABBF98CBC2E4D90F6B7DA3E02 (void);
// 0x0000011B System.Void Obi.ObiTriangleSkinMap::ValidateSlaveChannels(System.Boolean)
extern void ObiTriangleSkinMap_ValidateSlaveChannels_mBFD5DED1F3D3218BFA963001A324C21146C6C2D4 (void);
// 0x0000011C System.Void Obi.ObiTriangleSkinMap::CopyChannel(System.UInt32[],System.Int32,System.Int32)
extern void ObiTriangleSkinMap_CopyChannel_m2A08612D0ADBC3326473C33CF5E19029E8859B8F (void);
// 0x0000011D System.Void Obi.ObiTriangleSkinMap::FillChannel(System.UInt32[],System.Int32)
extern void ObiTriangleSkinMap_FillChannel_m8453C63E562D501363823D6162036F20B75B5651 (void);
// 0x0000011E System.Void Obi.ObiTriangleSkinMap::ClearChannel(System.UInt32[],System.Int32)
extern void ObiTriangleSkinMap_ClearChannel_m62E892A9892F3528AD225CCAEF08C2D368A59C62 (void);
// 0x0000011F System.Boolean Obi.ObiTriangleSkinMap::BindToFace(System.Int32,Obi.ObiTriangleSkinMap/MasterFace,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,Obi.ObiTriangleSkinMap/SlaveVertex&)
extern void ObiTriangleSkinMap_BindToFace_m904B69AD7BE3769CC10E5A400CC614EEA7AE91A6 (void);
// 0x00000120 System.Single Obi.ObiTriangleSkinMap::GetBarycentricError(UnityEngine.Vector3)
extern void ObiTriangleSkinMap_GetBarycentricError_mA4BF58342EFEDF9C71615E3DC9E043CBD77A1779 (void);
// 0x00000121 System.Single Obi.ObiTriangleSkinMap::GetFaceMappingError(Obi.ObiTriangleSkinMap/MasterFace,Obi.ObiTriangleSkinMap/SlaveVertex,UnityEngine.Vector3)
extern void ObiTriangleSkinMap_GetFaceMappingError_m83D60544D64792010A0362A30A8E989747EA76AF (void);
// 0x00000122 System.Boolean Obi.ObiTriangleSkinMap::FindSkinBarycentricCoords(Obi.ObiTriangleSkinMap/MasterFace,UnityEngine.Vector3,System.Int32,System.Single,Obi.ObiTriangleSkinMap/BarycentricPoint&)
extern void ObiTriangleSkinMap_FindSkinBarycentricCoords_m7881E5CB808F7D7B82B1B4F86252C272F5922557 (void);
// 0x00000123 System.Collections.IEnumerator Obi.ObiTriangleSkinMap::Bind()
extern void ObiTriangleSkinMap_Bind_m62980026E2CF756492E6BDF0FF5F32D462169522 (void);
// 0x00000124 System.Void Obi.ObiTriangleSkinMap::.ctor()
extern void ObiTriangleSkinMap__ctor_mF084C886D75F78D14C9CD798F2959ADC8964EE86 (void);
// 0x00000125 System.Void Obi.ObiTriangleSkinMap/MasterFace::CacheBarycentricData()
extern void MasterFace_CacheBarycentricData_m4CE92EC9AB2150F5D057B915E4A3E8C370EDAF28 (void);
// 0x00000126 System.Boolean Obi.ObiTriangleSkinMap/MasterFace::BarycentricCoords(UnityEngine.Vector3,UnityEngine.Vector3&)
extern void MasterFace_BarycentricCoords_m8A2510BFBFE31ABD8623CF7B03E2F520B9934717 (void);
// 0x00000127 System.Void Obi.ObiTriangleSkinMap/MasterFace::.ctor()
extern void MasterFace__ctor_m5F8FF47924C0460E115B7CB1E1FEBB99CDC1FED0 (void);
// 0x00000128 System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Vector3,UnityEngine.Quaternion,UnityEngine.Vector3)
extern void SkinTransform__ctor_mABDA5BC834E71D0637F68EC987DE11419C85364A (void);
// 0x00000129 System.Void Obi.ObiTriangleSkinMap/SkinTransform::.ctor(UnityEngine.Transform)
extern void SkinTransform__ctor_m04F80634057138EDFCD0E9AF5883073EEE5D2D90 (void);
// 0x0000012A System.Void Obi.ObiTriangleSkinMap/SkinTransform::Apply(UnityEngine.Transform)
extern void SkinTransform_Apply_m60D31FA6B0918902E7F39C888B680AA41022BAE2 (void);
// 0x0000012B UnityEngine.Matrix4x4 Obi.ObiTriangleSkinMap/SkinTransform::GetMatrix4X4()
extern void SkinTransform_GetMatrix4X4_mD5A708E729C95FE3D38DD26D16C7E7BE94421D1B (void);
// 0x0000012C System.Void Obi.ObiTriangleSkinMap/SkinTransform::Reset()
extern void SkinTransform_Reset_m3FD007CF70C5C4ED52952EF1CA7F95B333BD6C1D (void);
// 0x0000012D Obi.ObiTriangleSkinMap/BarycentricPoint Obi.ObiTriangleSkinMap/BarycentricPoint::get_zero()
extern void BarycentricPoint_get_zero_mD4FD59E5FE3F7B61E1D7EF67D1F641B4B54FA87A (void);
// 0x0000012E System.Void Obi.ObiTriangleSkinMap/BarycentricPoint::.ctor(UnityEngine.Vector3,System.Single)
extern void BarycentricPoint__ctor_m3EEE56C6E6CB000D8224ADF9173123AC1FF91836 (void);
// 0x0000012F Obi.ObiTriangleSkinMap/SlaveVertex Obi.ObiTriangleSkinMap/SlaveVertex::get_empty()
extern void SlaveVertex_get_empty_m936ADF657206971C4F23C7E5B3621324BA04F263 (void);
// 0x00000130 System.Boolean Obi.ObiTriangleSkinMap/SlaveVertex::get_isEmpty()
extern void SlaveVertex_get_isEmpty_m63953EAD27B6F66BEA7A28B77BC2253CA453101E (void);
// 0x00000131 System.Void Obi.ObiTriangleSkinMap/SlaveVertex::.ctor(System.Int32,System.Int32,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint,Obi.ObiTriangleSkinMap/BarycentricPoint)
extern void SlaveVertex__ctor_m12A785158C13405D27F9C736FAE1350516EDF56C (void);
// 0x00000132 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::.ctor(System.Int32)
extern void U3CBindU3Ed__31__ctor_m345D3284C09C114F18927E7F8EDF672BF64F6109 (void);
// 0x00000133 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.IDisposable.Dispose()
extern void U3CBindU3Ed__31_System_IDisposable_Dispose_mEC8D340299496DC39BF57BBA1F907DBE3DB91E94 (void);
// 0x00000134 System.Boolean Obi.ObiTriangleSkinMap/<Bind>d__31::MoveNext()
extern void U3CBindU3Ed__31_MoveNext_m25AD32F7C23158AD745F537AE6FADF9C324F47F7 (void);
// 0x00000135 System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09716DC077042BA4DB1A735CDDACE6F3A85F0113 (void);
// 0x00000136 System.Void Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.Reset()
extern void U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m2886755E5DB4DCB7C2D3A1DDB54E7890E5D3255A (void);
// 0x00000137 System.Object Obi.ObiTriangleSkinMap/<Bind>d__31::System.Collections.IEnumerator.get_Current()
extern void U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_mD9523C5684A83C45D0E592E81502428E1B74A8EA (void);
// 0x00000138 System.Void Obi.ObiClothProxy::set_master(Obi.ObiClothRendererBase)
extern void ObiClothProxy_set_master_mA10B6136180C3F8883EB77467D05296EF6958B39 (void);
// 0x00000139 Obi.ObiClothRendererBase Obi.ObiClothProxy::get_master()
extern void ObiClothProxy_get_master_m9097A98051AA22E455E9ECDC1D17A7D908722854 (void);
// 0x0000013A System.Void Obi.ObiClothProxy::OnEnable()
extern void ObiClothProxy_OnEnable_m2A0AEAB28A75A63571D506B4A422D435D1B8435E (void);
// 0x0000013B System.Void Obi.ObiClothProxy::OnDisable()
extern void ObiClothProxy_OnDisable_mEED97A31EA1FDE139CFBC479EEF4E14A5FD2B6F6 (void);
// 0x0000013C System.Void Obi.ObiClothProxy::GetSlaveMeshIfNeeded()
extern void ObiClothProxy_GetSlaveMeshIfNeeded_mA2213C1F12E371A1169B5D2FBDD89BF1DDAAA9D5 (void);
// 0x0000013D System.Void Obi.ObiClothProxy::UpdateSkinning(Obi.ObiActor)
extern void ObiClothProxy_UpdateSkinning_mD5BF40D50893B4AA5B9CDF59DBFE2F3110804C2B (void);
// 0x0000013E System.Void Obi.ObiClothProxy::.ctor()
extern void ObiClothProxy__ctor_mED67798944E1C23E820295A5BD35F4FBE951F3F5 (void);
// 0x0000013F System.Void Obi.ObiClothRenderer::.ctor()
extern void ObiClothRenderer__ctor_mBAC561B2AF103D9C49C73F9B0FD02E3952783B07 (void);
// 0x00000140 System.Void Obi.ObiClothRendererBase::add_OnRendererUpdated(Obi.ObiActor/ActorCallback)
extern void ObiClothRendererBase_add_OnRendererUpdated_m005A5B4813F40AD9030908BA9864FDDB27D07A51 (void);
// 0x00000141 System.Void Obi.ObiClothRendererBase::remove_OnRendererUpdated(Obi.ObiActor/ActorCallback)
extern void ObiClothRendererBase_remove_OnRendererUpdated_m3FBF2A5A61D8A2112913C3186813A87667103128 (void);
// 0x00000142 Obi.HalfEdgeMesh Obi.ObiClothRendererBase::get_topology()
extern void ObiClothRendererBase_get_topology_mAFB5108E3117AEEFDF4396DD54647E17508A6A94 (void);
// 0x00000143 UnityEngine.Matrix4x4 Obi.ObiClothRendererBase::get_renderMatrix()
extern void ObiClothRendererBase_get_renderMatrix_m63D33076CEFAB7804FCB8D5A54357AA5FE36EE10 (void);
// 0x00000144 System.Void Obi.ObiClothRendererBase::Awake()
extern void ObiClothRendererBase_Awake_m1C28F746382661CA24D4554A21B3012631662C36 (void);
// 0x00000145 System.Void Obi.ObiClothRendererBase::OnDestroy()
extern void ObiClothRendererBase_OnDestroy_m5957DCAE1C8EBDE3262BEE1FDEADC392D3D4021C (void);
// 0x00000146 System.Void Obi.ObiClothRendererBase::OnEnable()
extern void ObiClothRendererBase_OnEnable_mBB699002D10B17911DD5F99EC919124A5890D7D9 (void);
// 0x00000147 System.Void Obi.ObiClothRendererBase::OnDisable()
extern void ObiClothRendererBase_OnDisable_mBA13C1413C262DC40E6136549D6AD84260C31560 (void);
// 0x00000148 System.Void Obi.ObiClothRendererBase::GetClothMeshData()
extern void ObiClothRendererBase_GetClothMeshData_m1EBCC10924D369303C788603E1992A2714B68755 (void);
// 0x00000149 System.Void Obi.ObiClothRendererBase::SetClothMeshData()
extern void ObiClothRendererBase_SetClothMeshData_mD1F2FD48D7C6203837DE02654FD74E5C2B78DD2F (void);
// 0x0000014A System.Void Obi.ObiClothRendererBase::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererBase_OnBlueprintLoaded_m06BF18FA13DEBC1E7964370725DEC3CEFFC6E86C (void);
// 0x0000014B System.Void Obi.ObiClothRendererBase::OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererBase_OnBlueprintUnloaded_mEC61E0663591DFB8DB508A0465EFDABFF264D7A1 (void);
// 0x0000014C System.Void Obi.ObiClothRendererBase::SetupUpdate()
extern void ObiClothRendererBase_SetupUpdate_m0FA9C1879DA717926B4365EFA388407C0E573425 (void);
// 0x0000014D System.Void Obi.ObiClothRendererBase::UpdateActiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiClothRendererBase_UpdateActiveVertex_mD0211CB2FA72279FECFB83A24331E53D2606303D (void);
// 0x0000014E System.Void Obi.ObiClothRendererBase::UpdateInactiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiClothRendererBase_UpdateInactiveVertex_mFD3D31244DAF9ECF879D4AA94CC59A76AF831635 (void);
// 0x0000014F System.Void Obi.ObiClothRendererBase::UpdateRenderer(Obi.ObiActor)
extern void ObiClothRendererBase_UpdateRenderer_m67C5D60FA907DA341E7DE852502879D360B4A72A (void);
// 0x00000150 System.Void Obi.ObiClothRendererBase::.ctor()
extern void ObiClothRendererBase__ctor_m14E952FA423389FF8A9FFBB4DEDB3AD45F89A5B1 (void);
// 0x00000151 System.Void Obi.ObiClothRendererBase::.cctor()
extern void ObiClothRendererBase__cctor_m123E98B445499062FB7277932205609170F7478C (void);
// 0x00000152 System.Void Obi.ObiClothRendererMeshFilter::Awake()
extern void ObiClothRendererMeshFilter_Awake_m03D6844E7C94DAFE7FFA9E0DA6EFEB6EB0B02EB1 (void);
// 0x00000153 System.Void Obi.ObiClothRendererMeshFilter::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererMeshFilter_OnBlueprintLoaded_m7DFD7D44A5C55E53D7CBF48402AE0D1A867F1F2B (void);
// 0x00000154 System.Void Obi.ObiClothRendererMeshFilter::OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiClothRendererMeshFilter_OnBlueprintUnloaded_m233825C2AA0FBA4C3262E542CB63D088796E9E06 (void);
// 0x00000155 System.Void Obi.ObiClothRendererMeshFilter::.ctor()
extern void ObiClothRendererMeshFilter__ctor_m282A70D2A0FDA19306CE4E901C1EC6C4D30D3666 (void);
// 0x00000156 UnityEngine.Matrix4x4 Obi.ObiSkinnedClothRenderer::get_renderMatrix()
extern void ObiSkinnedClothRenderer_get_renderMatrix_m4BAC1EC550B0D3F29950CCF556674FBD2AA31313 (void);
// 0x00000157 System.Void Obi.ObiSkinnedClothRenderer::Awake()
extern void ObiSkinnedClothRenderer_Awake_m244A0D9B2968131DFF23CA5018502CEB4B472A5B (void);
// 0x00000158 System.Void Obi.ObiSkinnedClothRenderer::OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiSkinnedClothRenderer_OnBlueprintLoaded_m5FA4BBB248FFF2CE1555B76D4F6031D7C2D3FCD3 (void);
// 0x00000159 System.Void Obi.ObiSkinnedClothRenderer::SetupUpdate()
extern void ObiSkinnedClothRenderer_SetupUpdate_m1A5EB436AAB7518E6C7D204BB7713608903853C5 (void);
// 0x0000015A System.Void Obi.ObiSkinnedClothRenderer::UpdateInactiveVertex(Obi.ObiSolver,System.Int32,System.Int32)
extern void ObiSkinnedClothRenderer_UpdateInactiveVertex_m3A7BEFD80868F949B67E1E13FCF395BA6027EF38 (void);
// 0x0000015B System.Void Obi.ObiSkinnedClothRenderer::UpdateRenderer(Obi.ObiActor)
extern void ObiSkinnedClothRenderer_UpdateRenderer_m0B034CC6342C1E8F8630069500EA34C4579CB173 (void);
// 0x0000015C System.Void Obi.ObiSkinnedClothRenderer::.ctor()
extern void ObiSkinnedClothRenderer__ctor_mD0D8AA6357C14C2FF206DE916914F8A60EFF63B7 (void);
// 0x0000015D Obi.HalfEdgeMesh Obi.ObiTearableClothRenderer::get_topology()
extern void ObiTearableClothRenderer_get_topology_m1DC9B503255163BD330C4C0EE154A4B9696A8F29 (void);
// 0x0000015E System.Void Obi.ObiTearableClothRenderer::OnEnable()
extern void ObiTearableClothRenderer_OnEnable_mA0FD0FE15AE90519159E649741E2D877E3F6F43A (void);
// 0x0000015F System.Void Obi.ObiTearableClothRenderer::OnDisable()
extern void ObiTearableClothRenderer_OnDisable_mC7499A98C4DAA4A486C8523C1C97EC521811822E (void);
// 0x00000160 System.Void Obi.ObiTearableClothRenderer::GetClothMeshData()
extern void ObiTearableClothRenderer_GetClothMeshData_mC99E90FEB3AB5A9BD9DBB5185F8D37D6ACEB524C (void);
// 0x00000161 System.Void Obi.ObiTearableClothRenderer::SetClothMeshData()
extern void ObiTearableClothRenderer_SetClothMeshData_mE9F6592A5C6657B2FAD491825ED80D69B5463706 (void);
// 0x00000162 System.Void Obi.ObiTearableClothRenderer::UpdateMesh(System.Object,Obi.ObiTearableCloth/ObiClothTornEventArgs)
extern void ObiTearableClothRenderer_UpdateMesh_m98ABCF7C8E42CC5CC650EC7E45D1341E507C32B3 (void);
// 0x00000163 System.Collections.Generic.HashSet`1<System.Int32> Obi.ObiTearableClothRenderer::GetTornMeshVertices(System.Int32,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableClothRenderer_GetTornMeshVertices_mD7A8D7E293DB0815B2A060456ECA62EE826972C1 (void);
// 0x00000164 System.Void Obi.ObiTearableClothRenderer::UpdateTornMeshVertices(System.Collections.Generic.HashSet`1<System.Int32>,System.Collections.Generic.List`1<Obi.HalfEdgeMesh/Face>)
extern void ObiTearableClothRenderer_UpdateTornMeshVertices_m2E42006A49946BE6E1624CF6B8962C4071841EA8 (void);
// 0x00000165 System.Void Obi.ObiTearableClothRenderer::.ctor()
extern void ObiTearableClothRenderer__ctor_m3884A3BA9B31A6B4786F79FCFF7A20F50F446572 (void);
// 0x00000166 System.Int32 Obi.IObiParticleCollection::get_particleCount()
// 0x00000167 System.Int32 Obi.IObiParticleCollection::get_activeParticleCount()
// 0x00000168 System.Boolean Obi.IObiParticleCollection::get_usesOrientedParticles()
// 0x00000169 System.Int32 Obi.IObiParticleCollection::GetParticleRuntimeIndex(System.Int32)
// 0x0000016A UnityEngine.Vector3 Obi.IObiParticleCollection::GetParticlePosition(System.Int32)
// 0x0000016B UnityEngine.Quaternion Obi.IObiParticleCollection::GetParticleOrientation(System.Int32)
// 0x0000016C System.Void Obi.IObiParticleCollection::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
// 0x0000016D System.Single Obi.IObiParticleCollection::GetParticleMaxRadius(System.Int32)
// 0x0000016E UnityEngine.Color Obi.IObiParticleCollection::GetParticleColor(System.Int32)
// 0x0000016F System.Void Obi.ObiActor::add_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintLoaded_m750BDCFAAC9B9FC2B13473DD6142256B87AEB9AD (void);
// 0x00000170 System.Void Obi.ObiActor::remove_OnBlueprintLoaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintLoaded_m7DCDC0933CAE4EEDD66341CF218C7C52AEB5EA05 (void);
// 0x00000171 System.Void Obi.ObiActor::add_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_add_OnBlueprintUnloaded_mAF07AF4D755E892627202E5D841606AD45504DE8 (void);
// 0x00000172 System.Void Obi.ObiActor::remove_OnBlueprintUnloaded(Obi.ObiActor/ActorBlueprintCallback)
extern void ObiActor_remove_OnBlueprintUnloaded_m1BBCF889DC5E7CF33AD01B111E5983EB29BEEC5D (void);
// 0x00000173 System.Void Obi.ObiActor::add_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnPrepareStep_m02E85D770109757133A0D4E7218B5414022C096D (void);
// 0x00000174 System.Void Obi.ObiActor::remove_OnPrepareStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnPrepareStep_m609001C918634ABA520EE383AEA2A0EA6B40B70F (void);
// 0x00000175 System.Void Obi.ObiActor::add_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnBeginStep_mEADF45AAECC5E2C0930CCBD3D14CDDD65E1526F1 (void);
// 0x00000176 System.Void Obi.ObiActor::remove_OnBeginStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnBeginStep_m56B61A09CB8B0156101BE3B673A3ECACB9C8B0F8 (void);
// 0x00000177 System.Void Obi.ObiActor::add_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnSubstep_mFCF2FF0A7B81870BB43E6651F53C2C037D67CC03 (void);
// 0x00000178 System.Void Obi.ObiActor::remove_OnSubstep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnSubstep_mAE61ECC9F9F42DD0EEC897EE73596D5B83C3C8AD (void);
// 0x00000179 System.Void Obi.ObiActor::add_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_add_OnEndStep_mD7307710B86E3884F47561B432963D3EF1F49847 (void);
// 0x0000017A System.Void Obi.ObiActor::remove_OnEndStep(Obi.ObiActor/ActorStepCallback)
extern void ObiActor_remove_OnEndStep_mA7612D69ED92FE16899F8292AB9274047EA20A95 (void);
// 0x0000017B System.Void Obi.ObiActor::add_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_add_OnInterpolate_mF5BEDDED56D77A9F3F126B8A0C75213F796F5237 (void);
// 0x0000017C System.Void Obi.ObiActor::remove_OnInterpolate(Obi.ObiActor/ActorCallback)
extern void ObiActor_remove_OnInterpolate_m8262597E47B9200EC9D36E3BB132C79BAC0B8403 (void);
// 0x0000017D Obi.ObiSolver Obi.ObiActor::get_solver()
extern void ObiActor_get_solver_mA2AA7D75716CC2D3B192EF6660A742CD7BA6537A (void);
// 0x0000017E System.Boolean Obi.ObiActor::get_isLoaded()
extern void ObiActor_get_isLoaded_m44EB6CF5F3A3141E1FCA029F5CA8E0F06DFF52C4 (void);
// 0x0000017F Obi.ObiCollisionMaterial Obi.ObiActor::get_collisionMaterial()
extern void ObiActor_get_collisionMaterial_mA0CAB573F50DB23136C92682BBD42AFB2B660D79 (void);
// 0x00000180 System.Void Obi.ObiActor::set_collisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiActor_set_collisionMaterial_m319EB84B9963B7DD51AB003D867735A2D29BAE3B (void);
// 0x00000181 System.Boolean Obi.ObiActor::get_surfaceCollisions()
extern void ObiActor_get_surfaceCollisions_mB88273F10504E327D0392CB09A87EFFDF45E5051 (void);
// 0x00000182 System.Void Obi.ObiActor::set_surfaceCollisions(System.Boolean)
extern void ObiActor_set_surfaceCollisions_m86016E44CFFEE5DAB0C139C8AC9CD7F7ABD28ACB (void);
// 0x00000183 System.Int32 Obi.ObiActor::get_particleCount()
extern void ObiActor_get_particleCount_mB975A4D367302FF8F7DB9AB1841C9EFE14AB2B5A (void);
// 0x00000184 System.Int32 Obi.ObiActor::get_activeParticleCount()
extern void ObiActor_get_activeParticleCount_mB9A86C48B1F1B31DF5FA15A2E9F29550079B99AC (void);
// 0x00000185 System.Boolean Obi.ObiActor::get_usesOrientedParticles()
extern void ObiActor_get_usesOrientedParticles_mEAD00F29B6E3C3E6B1D4F5D0115F620E8C540026 (void);
// 0x00000186 System.Boolean Obi.ObiActor::get_usesAnisotropicParticles()
extern void ObiActor_get_usesAnisotropicParticles_mAA20FA6263D07BDF0ED730CB0509B0DFCDC5B6A5 (void);
// 0x00000187 System.Boolean Obi.ObiActor::get_usesCustomExternalForces()
extern void ObiActor_get_usesCustomExternalForces_mB2DB2A7D050E13F6E2DA2C0AEE02FDFC8F183812 (void);
// 0x00000188 UnityEngine.Matrix4x4 Obi.ObiActor::get_actorLocalToSolverMatrix()
extern void ObiActor_get_actorLocalToSolverMatrix_mEEA426364EA846E7545862692992B75B9541CA86 (void);
// 0x00000189 UnityEngine.Matrix4x4 Obi.ObiActor::get_actorSolverToLocalMatrix()
extern void ObiActor_get_actorSolverToLocalMatrix_m44B0C2AA60494EE7FD815816C1D0ABE7E304E142 (void);
// 0x0000018A Obi.ObiActorBlueprint Obi.ObiActor::get_sourceBlueprint()
// 0x0000018B Obi.ObiActorBlueprint Obi.ObiActor::get_sharedBlueprint()
extern void ObiActor_get_sharedBlueprint_m6A88C094D65EB05975F045937BB5A9CDF3C8322A (void);
// 0x0000018C Obi.ObiActorBlueprint Obi.ObiActor::get_blueprint()
extern void ObiActor_get_blueprint_mFB44880BA798FFBC94A11ABC8ACFE87077CE1C3A (void);
// 0x0000018D System.Void Obi.ObiActor::Awake()
extern void ObiActor_Awake_mCFA26716113346786A47362EE4399C8135328F45 (void);
// 0x0000018E System.Void Obi.ObiActor::OnDestroy()
extern void ObiActor_OnDestroy_m9C8BA7B78B8E469F0A7A78416F230315E7A4186A (void);
// 0x0000018F System.Void Obi.ObiActor::OnEnable()
extern void ObiActor_OnEnable_mD66D61D2BBC655028D8208422AEA64786381F6BB (void);
// 0x00000190 System.Void Obi.ObiActor::OnDisable()
extern void ObiActor_OnDisable_m13C6D2A5949762C0718A506CE0B4DD73D63B03C3 (void);
// 0x00000191 System.Void Obi.ObiActor::OnValidate()
extern void ObiActor_OnValidate_mD2AB1820B6297779092DA1B2D29768467BA82DE0 (void);
// 0x00000192 System.Void Obi.ObiActor::OnTransformParentChanged()
extern void ObiActor_OnTransformParentChanged_m28F61AD8B165E2AA050346781678DC540D63E720 (void);
// 0x00000193 System.Void Obi.ObiActor::AddToSolver()
extern void ObiActor_AddToSolver_mE5B9CEC1E1A9357E520418FD64B5DDC4D2FC180F (void);
// 0x00000194 System.Void Obi.ObiActor::RemoveFromSolver()
extern void ObiActor_RemoveFromSolver_m0D4589A5675024C9D21FFA3AB415A1D42DEFE1EA (void);
// 0x00000195 System.Void Obi.ObiActor::SetSolver(Obi.ObiSolver)
extern void ObiActor_SetSolver_m4F478A600EFE01614A0E28DBBAC90EC4D56DBF36 (void);
// 0x00000196 System.Void Obi.ObiActor::OnBlueprintRegenerate(Obi.ObiActorBlueprint)
extern void ObiActor_OnBlueprintRegenerate_mCFD8B8A478A5B731AA8385F26810EA7AE7D6D25D (void);
// 0x00000197 System.Void Obi.ObiActor::UpdateCollisionMaterials()
extern void ObiActor_UpdateCollisionMaterials_m61579860AE22EB454F1BE424E5E2224AD74F95EB (void);
// 0x00000198 System.Boolean Obi.ObiActor::CopyParticle(System.Int32,System.Int32)
extern void ObiActor_CopyParticle_m51D676D131EBDF4C8685DCA1549AA5DAECFF5B99 (void);
// 0x00000199 System.Void Obi.ObiActor::TeleportParticle(System.Int32,UnityEngine.Vector3)
extern void ObiActor_TeleportParticle_m69B27BC2A0FD53826FE5B14A46A4951BA7B52578 (void);
// 0x0000019A System.Void Obi.ObiActor::Teleport(UnityEngine.Vector3,UnityEngine.Quaternion)
extern void ObiActor_Teleport_mF8C7FDC53D774D61FC7BE6E72B009803A909CCD9 (void);
// 0x0000019B System.Void Obi.ObiActor::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActor_SwapWithFirstInactiveParticle_m78A822EA1147B2CE69C32B89DB48D562BC0D9E26 (void);
// 0x0000019C System.Boolean Obi.ObiActor::ActivateParticle(System.Int32)
extern void ObiActor_ActivateParticle_m008AED1F23E74B2C4C977706708D3E547B0AA3C3 (void);
// 0x0000019D System.Boolean Obi.ObiActor::DeactivateParticle(System.Int32)
extern void ObiActor_DeactivateParticle_m8A106245C1657AA6CF2A81468CCE4A7A3032C5E1 (void);
// 0x0000019E System.Boolean Obi.ObiActor::IsParticleActive(System.Int32)
extern void ObiActor_IsParticleActive_m892F91908AE43D40E0117C826A2ADE2E70579B37 (void);
// 0x0000019F System.Void Obi.ObiActor::SetSelfCollisions(System.Boolean)
extern void ObiActor_SetSelfCollisions_mBA196DE918E283D18B104E44E25A369BF57DF48B (void);
// 0x000001A0 System.Void Obi.ObiActor::SetOneSided(System.Boolean)
extern void ObiActor_SetOneSided_mF6F09D5CDD6FC7E382379F12EDAFC69B2D3B5BDE (void);
// 0x000001A1 System.Void Obi.ObiActor::SetSimplicesDirty()
extern void ObiActor_SetSimplicesDirty_m99F0CAFF371F338400E593B556E7BA6B0825E779 (void);
// 0x000001A2 System.Void Obi.ObiActor::SetConstraintsDirty(Oni/ConstraintType)
extern void ObiActor_SetConstraintsDirty_mC937119D5E6ADCD2674A1204EBD3E2E2B6559607 (void);
// 0x000001A3 Obi.IObiConstraints Obi.ObiActor::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActor_GetConstraintsByType_mC947F9F5CF01DCF737BBBA300D32B9C0D6E52284 (void);
// 0x000001A4 System.Void Obi.ObiActor::UpdateParticleProperties()
extern void ObiActor_UpdateParticleProperties_m79AA51543AD70E1CD57B8ED6DBF7C57BC61BF316 (void);
// 0x000001A5 System.Int32 Obi.ObiActor::GetParticleRuntimeIndex(System.Int32)
extern void ObiActor_GetParticleRuntimeIndex_m4EED333775B6B67B33CB0C2B200C344D3B375E77 (void);
// 0x000001A6 UnityEngine.Vector3 Obi.ObiActor::GetParticlePosition(System.Int32)
extern void ObiActor_GetParticlePosition_m09A6B4BFB585627E2ED09E93DD4094F333A14C1C (void);
// 0x000001A7 UnityEngine.Quaternion Obi.ObiActor::GetParticleOrientation(System.Int32)
extern void ObiActor_GetParticleOrientation_m128E5C81A8611D1B2D56241A7EF0070C1662E15A (void);
// 0x000001A8 System.Void Obi.ObiActor::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActor_GetParticleAnisotropy_m9CEDBC119B69CD6B157807D2E66DDDB3D06A13EA (void);
// 0x000001A9 System.Single Obi.ObiActor::GetParticleMaxRadius(System.Int32)
extern void ObiActor_GetParticleMaxRadius_m87144AB3ADB33846F86CEB4B6939ACE92917DF77 (void);
// 0x000001AA UnityEngine.Color Obi.ObiActor::GetParticleColor(System.Int32)
extern void ObiActor_GetParticleColor_m7F66CE36A055FDAC277D9C6D4216DA4B992A4659 (void);
// 0x000001AB System.Void Obi.ObiActor::SetFilterCategory(System.Int32)
extern void ObiActor_SetFilterCategory_mB1E5312397CA790EB18CAA64BD73E57FC396EE4F (void);
// 0x000001AC System.Void Obi.ObiActor::SetFilterMask(System.Int32)
extern void ObiActor_SetFilterMask_m6D2AF9BE496199753111190398B8804EC830DA9D (void);
// 0x000001AD System.Void Obi.ObiActor::SetMass(System.Single)
extern void ObiActor_SetMass_mA784144BCC7ADF1985E58AA5F5569307AE2FF4E5 (void);
// 0x000001AE System.Single Obi.ObiActor::GetMass(UnityEngine.Vector3&)
extern void ObiActor_GetMass_m19BAD33875847C12BD7622D6DFAB5F84D6F0D964 (void);
// 0x000001AF System.Void Obi.ObiActor::AddForce(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddForce_m0E8B211FB33780E00CC1B22831B95BB37953EBD6 (void);
// 0x000001B0 System.Void Obi.ObiActor::AddTorque(UnityEngine.Vector3,UnityEngine.ForceMode)
extern void ObiActor_AddTorque_m7582153CD8CEB690DC0B9BCAFE57F7BAFEEB74E2 (void);
// 0x000001B1 System.Void Obi.ObiActor::LoadBlueprintParticles(Obi.ObiActorBlueprint,System.Int32)
extern void ObiActor_LoadBlueprintParticles_m8FE3AC1F4DFC662D6973E269A51C55740E909826 (void);
// 0x000001B2 System.Void Obi.ObiActor::UnloadBlueprintParticles()
extern void ObiActor_UnloadBlueprintParticles_mD50370C19E1AD0FE3A6C88233E35609F6932B869 (void);
// 0x000001B3 System.Void Obi.ObiActor::ResetParticles()
extern void ObiActor_ResetParticles_mBBB929ADB2C470ABE6093BE4D4647E2C0A2A79BA (void);
// 0x000001B4 System.Void Obi.ObiActor::SaveStateToBlueprint(Obi.ObiActorBlueprint)
extern void ObiActor_SaveStateToBlueprint_m6E90039A5DCC6C4F2FCCF6A2C9AEEB0836D333ED (void);
// 0x000001B5 System.Void Obi.ObiActor::StoreState()
extern void ObiActor_StoreState_m6D5D8B5823CC011DC9F97D34E27798D1E5F8722F (void);
// 0x000001B6 System.Void Obi.ObiActor::ClearState()
extern void ObiActor_ClearState_m848A4E35DF8DD37649408E58E0FAAC93C9442954 (void);
// 0x000001B7 System.Void Obi.ObiActor::LoadBlueprint(Obi.ObiSolver)
extern void ObiActor_LoadBlueprint_mD01EB4D136F4CAB3CC10456A1597D702A3CF1A97 (void);
// 0x000001B8 System.Void Obi.ObiActor::UnloadBlueprint(Obi.ObiSolver)
extern void ObiActor_UnloadBlueprint_m969E960679F2D21349E837CBA33D18EC1C876622 (void);
// 0x000001B9 System.Void Obi.ObiActor::PrepareStep(System.Single)
extern void ObiActor_PrepareStep_mFEFDD4015BE99D742E4EC4562961CD4BCD19ACFC (void);
// 0x000001BA System.Void Obi.ObiActor::BeginStep(System.Single)
extern void ObiActor_BeginStep_m7C98EE23CE2E3BEBF930FDF68375D072ECB5DFCF (void);
// 0x000001BB System.Void Obi.ObiActor::Substep(System.Single)
extern void ObiActor_Substep_mDF70B27421BA323B7EB47AC78E825DD43398A6B5 (void);
// 0x000001BC System.Void Obi.ObiActor::EndStep(System.Single)
extern void ObiActor_EndStep_m733FF165CEB956C00689DC6B2FEE46DA209B5EAD (void);
// 0x000001BD System.Void Obi.ObiActor::Interpolate()
extern void ObiActor_Interpolate_m782ABB607378E722CA9AC44D3D4AC05E214471FD (void);
// 0x000001BE System.Void Obi.ObiActor::OnSolverVisibilityChanged(System.Boolean)
extern void ObiActor_OnSolverVisibilityChanged_m5582DD6825DE4B3C555502ABC646BC685CA4AA02 (void);
// 0x000001BF System.Void Obi.ObiActor::.ctor()
extern void ObiActor__ctor_mC9E385064DB6C39DC089855ED3E35750E80E2D65 (void);
// 0x000001C0 Obi.ObiSolver Obi.ObiActor/ObiActorSolverArgs::get_solver()
extern void ObiActorSolverArgs_get_solver_m22E029AF61259687CD6FFB9D6AB6EA20F86E3743 (void);
// 0x000001C1 System.Void Obi.ObiActor/ObiActorSolverArgs::.ctor(Obi.ObiSolver)
extern void ObiActorSolverArgs__ctor_m474F3748E48B55A7E6CFD3DB247238416FFE5B22 (void);
// 0x000001C2 System.Void Obi.ObiActor/ActorCallback::.ctor(System.Object,System.IntPtr)
extern void ActorCallback__ctor_m957117094F0A77A5D1206A50B8E10A9EE85E7E42 (void);
// 0x000001C3 System.Void Obi.ObiActor/ActorCallback::Invoke(Obi.ObiActor)
extern void ActorCallback_Invoke_m4E2F7079DDF8806942FFBC3744E8C2110A0B7475 (void);
// 0x000001C4 System.IAsyncResult Obi.ObiActor/ActorCallback::BeginInvoke(Obi.ObiActor,System.AsyncCallback,System.Object)
extern void ActorCallback_BeginInvoke_m154251BB5F9A6216F2FCB124267F1495C75E54C1 (void);
// 0x000001C5 System.Void Obi.ObiActor/ActorCallback::EndInvoke(System.IAsyncResult)
extern void ActorCallback_EndInvoke_m1EABC102C44DDE64098209CAE75266FA74904663 (void);
// 0x000001C6 System.Void Obi.ObiActor/ActorStepCallback::.ctor(System.Object,System.IntPtr)
extern void ActorStepCallback__ctor_m52D4618B6F4F29C2395853E2D28C2C68FFB17DD6 (void);
// 0x000001C7 System.Void Obi.ObiActor/ActorStepCallback::Invoke(Obi.ObiActor,System.Single)
extern void ActorStepCallback_Invoke_m9014EEDF9817269F00FA4DD0F7610FE9900B1E7B (void);
// 0x000001C8 System.IAsyncResult Obi.ObiActor/ActorStepCallback::BeginInvoke(Obi.ObiActor,System.Single,System.AsyncCallback,System.Object)
extern void ActorStepCallback_BeginInvoke_m0846053D3A742F989F829061A3F87354E803190A (void);
// 0x000001C9 System.Void Obi.ObiActor/ActorStepCallback::EndInvoke(System.IAsyncResult)
extern void ActorStepCallback_EndInvoke_m37F5088435801E61A62095769073F7F38EEF5128 (void);
// 0x000001CA System.Void Obi.ObiActor/ActorBlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void ActorBlueprintCallback__ctor_m747C9BC1FC06162D4E272EAAA503ED2CAE4A5D6E (void);
// 0x000001CB System.Void Obi.ObiActor/ActorBlueprintCallback::Invoke(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ActorBlueprintCallback_Invoke_mE9F4E664478CACFABCFDE8B2560F776DA4FDE48A (void);
// 0x000001CC System.IAsyncResult Obi.ObiActor/ActorBlueprintCallback::BeginInvoke(Obi.ObiActor,Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void ActorBlueprintCallback_BeginInvoke_m5A217A82EBC784D65EF72AF750C5A82D21A7AF2A (void);
// 0x000001CD System.Void Obi.ObiActor/ActorBlueprintCallback::EndInvoke(System.IAsyncResult)
extern void ActorBlueprintCallback_EndInvoke_m455198981D76FC45C680B1295D54B2DDF03E1961 (void);
// 0x000001CE System.Int32 Obi.IColliderWorldImpl::get_referenceCount()
// 0x000001CF System.Void Obi.IColliderWorldImpl::UpdateWorld(System.Single)
// 0x000001D0 System.Void Obi.IColliderWorldImpl::SetColliders(Obi.ObiNativeColliderShapeList,Obi.ObiNativeAabbList,Obi.ObiNativeAffineTransformList,System.Int32)
// 0x000001D1 System.Void Obi.IColliderWorldImpl::SetRigidbodies(Obi.ObiNativeRigidbodyList)
// 0x000001D2 System.Void Obi.IColliderWorldImpl::SetCollisionMaterials(Obi.ObiNativeCollisionMaterialList)
// 0x000001D3 System.Void Obi.IColliderWorldImpl::SetTriangleMeshData(Obi.ObiNativeTriangleMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeTriangleList,Obi.ObiNativeVector3List)
// 0x000001D4 System.Void Obi.IColliderWorldImpl::SetEdgeMeshData(Obi.ObiNativeEdgeMeshHeaderList,Obi.ObiNativeBIHNodeList,Obi.ObiNativeEdgeList,Obi.ObiNativeVector2List)
// 0x000001D5 System.Void Obi.IColliderWorldImpl::SetDistanceFieldData(Obi.ObiNativeDistanceFieldHeaderList,Obi.ObiNativeDFNodeList)
// 0x000001D6 System.Void Obi.IColliderWorldImpl::SetHeightFieldData(Obi.ObiNativeHeightFieldHeaderList,Obi.ObiNativeFloatList)
// 0x000001D7 System.Void Obi.IAerodynamicConstraintsBatchImpl::SetAerodynamicConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,System.Int32)
// 0x000001D8 System.Void Obi.IBendConstraintsBatchImpl::SetBendConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001D9 System.Void Obi.IBendTwistConstraintsBatchImpl::SetBendTwistConstraints(Obi.ObiNativeIntList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001DA System.Void Obi.IChainConstraintsBatchImpl::SetChainConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeIntList,Obi.ObiNativeIntList,System.Int32)
// 0x000001DB Oni/ConstraintType Obi.IConstraintsBatchImpl::get_constraintType()
// 0x000001DC Obi.IConstraints Obi.IConstraintsBatchImpl::get_constraints()
// 0x000001DD System.Void Obi.IConstraintsBatchImpl::set_enabled(System.Boolean)
// 0x000001DE System.Boolean Obi.IConstraintsBatchImpl::get_enabled()
// 0x000001DF System.Void Obi.IConstraintsBatchImpl::Destroy()
// 0x000001E0 System.Void Obi.IConstraintsBatchImpl::SetConstraintCount(System.Int32)
// 0x000001E1 System.Int32 Obi.IConstraintsBatchImpl::GetConstraintCount()
// 0x000001E2 Oni/ConstraintType Obi.IConstraints::get_constraintType()
// 0x000001E3 Obi.ISolverImpl Obi.IConstraints::get_solver()
// 0x000001E4 System.Int32 Obi.IConstraints::GetConstraintCount()
// 0x000001E5 System.Void Obi.IDistanceConstraintsBatchImpl::SetDistanceConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001E6 System.Void Obi.IPinConstraintsBatchImpl::SetPinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x000001E7 System.Void Obi.IShapeMatchingConstraintsBatchImpl::SetShapeMatchingConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeMatrix4x4List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001E8 System.Void Obi.IShapeMatchingConstraintsBatchImpl::CalculateRestShapeMatching()
// 0x000001E9 System.Void Obi.ISkinConstraintsBatchImpl::SetSkinConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x000001EA System.Void Obi.IStitchConstraintsBatchImpl::SetStitchConstraints(Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x000001EB System.Void Obi.IStretchShearConstraintsBatchImpl::SetStretchShearConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeQuaternionList,Obi.ObiNativeVector3List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001EC System.Void Obi.ITetherConstraintsBatchImpl::SetTetherConstraints(Obi.ObiNativeIntList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,Obi.ObiNativeFloatList,System.Int32)
// 0x000001ED System.Void Obi.IVolumeConstraintsBatchImpl::SetVolumeConstraints(Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeIntList,Obi.ObiNativeFloatList,Obi.ObiNativeVector2List,Obi.ObiNativeFloatList,System.Int32)
// 0x000001EE Obi.ISolverImpl Obi.IObiBackend::CreateSolver(Obi.ObiSolver,System.Int32)
// 0x000001EF System.Void Obi.IObiBackend::DestroySolver(Obi.ISolverImpl)
// 0x000001F0 System.Void Obi.IObiJobHandle::Complete()
// 0x000001F1 System.Void Obi.ISolverImpl::Destroy()
// 0x000001F2 System.Void Obi.ISolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
// 0x000001F3 System.Void Obi.ISolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
// 0x000001F4 System.Void Obi.ISolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
// 0x000001F5 System.Void Obi.ISolverImpl::ParticleCountChanged(Obi.ObiSolver)
// 0x000001F6 System.Void Obi.ISolverImpl::SetActiveParticles(System.Int32[],System.Int32)
// 0x000001F7 System.Void Obi.ISolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
// 0x000001F8 System.Void Obi.ISolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
// 0x000001F9 Obi.IConstraintsBatchImpl Obi.ISolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
// 0x000001FA System.Void Obi.ISolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
// 0x000001FB System.Int32 Obi.ISolverImpl::GetConstraintCount(Oni/ConstraintType)
// 0x000001FC System.Void Obi.ISolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
// 0x000001FD System.Void Obi.ISolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
// 0x000001FE System.Void Obi.ISolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
// 0x000001FF Obi.IObiJobHandle Obi.ISolverImpl::CollisionDetection(System.Single)
// 0x00000200 Obi.IObiJobHandle Obi.ISolverImpl::Substep(System.Single,System.Single,System.Int32)
// 0x00000201 System.Void Obi.ISolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
// 0x00000202 System.Int32 Obi.ISolverImpl::GetDeformableTriangleCount()
// 0x00000203 System.Void Obi.ISolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
// 0x00000204 System.Int32 Obi.ISolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
// 0x00000205 System.Void Obi.ISolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
// 0x00000206 System.Void Obi.ISolverImpl::SetParameters(Oni/SolverParameters)
// 0x00000207 System.Void Obi.ISolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
// 0x00000208 System.Void Obi.ISolverImpl::ResetForces()
// 0x00000209 System.Int32 Obi.ISolverImpl::GetParticleGridSize()
// 0x0000020A System.Void Obi.ISolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
// 0x0000020B System.Void Obi.ISolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
// 0x0000020C Obi.ISolverImpl Obi.NullBackend::CreateSolver(Obi.ObiSolver,System.Int32)
extern void NullBackend_CreateSolver_mDDC610D9B5DFD34CAA18BF33EA17AE191CD68804 (void);
// 0x0000020D System.Void Obi.NullBackend::DestroySolver(Obi.ISolverImpl)
extern void NullBackend_DestroySolver_mCD787B52B296982C9D2438E1CF9A7242CE3B18B5 (void);
// 0x0000020E System.Void Obi.NullBackend::.ctor()
extern void NullBackend__ctor_m15F612C86E6CBFD78C9C747B2DBB5B0A026E473F (void);
// 0x0000020F System.Void Obi.NullSolverImpl::Destroy()
extern void NullSolverImpl_Destroy_mB9D748974AB49AD42F59C4F7537E14735BA925AB (void);
// 0x00000210 System.Void Obi.NullSolverImpl::InitializeFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion)
extern void NullSolverImpl_InitializeFrame_mB4EED2EC4F57413F0093AFB60566DF7D6E1003B0 (void);
// 0x00000211 System.Void Obi.NullSolverImpl::UpdateFrame(UnityEngine.Vector4,UnityEngine.Vector4,UnityEngine.Quaternion,System.Single)
extern void NullSolverImpl_UpdateFrame_m41A43DA4CEDB5C86E3B61C1B648A2C49DA2045BB (void);
// 0x00000212 System.Void Obi.NullSolverImpl::ApplyFrame(System.Single,System.Single,System.Single)
extern void NullSolverImpl_ApplyFrame_mA1F9133DA49AC1F08FF0FEC050E5DEF10EC88E21 (void);
// 0x00000213 System.Int32 Obi.NullSolverImpl::GetDeformableTriangleCount()
extern void NullSolverImpl_GetDeformableTriangleCount_m3A8AE1DF269BBFCFF0C48419C2ED8B90CFB5A95B (void);
// 0x00000214 System.Void Obi.NullSolverImpl::SetDeformableTriangles(System.Int32[],System.Int32,System.Int32)
extern void NullSolverImpl_SetDeformableTriangles_mCFA6D651F8606212CF3158B1229B16A2079B90D0 (void);
// 0x00000215 System.Int32 Obi.NullSolverImpl::RemoveDeformableTriangles(System.Int32,System.Int32)
extern void NullSolverImpl_RemoveDeformableTriangles_mD24BAEB276DDCE364A13F89AD935A175696319E8 (void);
// 0x00000216 System.Void Obi.NullSolverImpl::SetSimplices(System.Int32[],Obi.SimplexCounts)
extern void NullSolverImpl_SetSimplices_m63A96660EBEC7FDF104C4121A099C7503D68D23A (void);
// 0x00000217 System.Void Obi.NullSolverImpl::ParticleCountChanged(Obi.ObiSolver)
extern void NullSolverImpl_ParticleCountChanged_m8B9FED4B7B5A6C6014F41D48B396A53CDA40568C (void);
// 0x00000218 System.Void Obi.NullSolverImpl::SetRigidbodyArrays(Obi.ObiSolver)
extern void NullSolverImpl_SetRigidbodyArrays_m1447ED0E67413D3CC414D66A687B6AA804D5A7D2 (void);
// 0x00000219 System.Void Obi.NullSolverImpl::SetActiveParticles(System.Int32[],System.Int32)
extern void NullSolverImpl_SetActiveParticles_m8E95A62DFCFAACFCCFD71C5D88C1D2D89B4B34FD (void);
// 0x0000021A System.Void Obi.NullSolverImpl::ResetForces()
extern void NullSolverImpl_ResetForces_m82E047EACB6F9E608555DF3BED74AC126A09AD69 (void);
// 0x0000021B System.Void Obi.NullSolverImpl::GetBounds(UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void NullSolverImpl_GetBounds_mAC172D98AE3D9510345D850A6CAE7BED572AD658 (void);
// 0x0000021C System.Void Obi.NullSolverImpl::SetParameters(Oni/SolverParameters)
extern void NullSolverImpl_SetParameters_m87C1A9AF268024C0A098129A7032E9A6321D13AE (void);
// 0x0000021D System.Int32 Obi.NullSolverImpl::GetConstraintCount(Oni/ConstraintType)
extern void NullSolverImpl_GetConstraintCount_mE99B745191C0B68347534F69BCBDCD3FA738DF63 (void);
// 0x0000021E System.Void Obi.NullSolverImpl::GetCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetCollisionContacts_mD5BF1363A042CB174A8D0FA9794DEC8DF3958D31 (void);
// 0x0000021F System.Void Obi.NullSolverImpl::GetParticleCollisionContacts(Oni/Contact[],System.Int32)
extern void NullSolverImpl_GetParticleCollisionContacts_m3A5C1741A0C6B26B27DB246311246DBD9596F7DC (void);
// 0x00000220 System.Void Obi.NullSolverImpl::SetConstraintGroupParameters(Oni/ConstraintType,Oni/ConstraintParameters&)
extern void NullSolverImpl_SetConstraintGroupParameters_m1424D00634EE6B6E70DFF8EA8EF0B96F215B9B33 (void);
// 0x00000221 Obi.IConstraintsBatchImpl Obi.NullSolverImpl::CreateConstraintsBatch(Oni/ConstraintType)
extern void NullSolverImpl_CreateConstraintsBatch_mCD99CB5676EB5E4BA217C5CEDBE962E293B2714D (void);
// 0x00000222 System.Void Obi.NullSolverImpl::DestroyConstraintsBatch(Obi.IConstraintsBatchImpl)
extern void NullSolverImpl_DestroyConstraintsBatch_mBBEEC8E9CDAA7A0EFB1CDF86008679CB710376B2 (void);
// 0x00000223 Obi.IObiJobHandle Obi.NullSolverImpl::CollisionDetection(System.Single)
extern void NullSolverImpl_CollisionDetection_mF6EA4B141873E6B181C3BE308E244BE9BC032615 (void);
// 0x00000224 Obi.IObiJobHandle Obi.NullSolverImpl::Substep(System.Single,System.Single,System.Int32)
extern void NullSolverImpl_Substep_m8A8BEE938250D29A032AFE3B78C66371DBD9CD01 (void);
// 0x00000225 System.Void Obi.NullSolverImpl::ApplyInterpolation(Obi.ObiNativeVector4List,Obi.ObiNativeQuaternionList,System.Single,System.Single)
extern void NullSolverImpl_ApplyInterpolation_m798CEE15B0E0B41CECC3F50AD8026B2DD2F6ED2D (void);
// 0x00000226 System.Void Obi.NullSolverImpl::InterpolateDiffuseProperties(Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeVector4List,Obi.ObiNativeIntList,System.Int32)
extern void NullSolverImpl_InterpolateDiffuseProperties_m0B56D3EA46E77669D02A80AF84E432CE4DC36516 (void);
// 0x00000227 System.Int32 Obi.NullSolverImpl::GetParticleGridSize()
extern void NullSolverImpl_GetParticleGridSize_mAB2387DF4946BD28660B4B9C462C238D59AC90DE (void);
// 0x00000228 System.Void Obi.NullSolverImpl::GetParticleGrid(Obi.ObiNativeAabbList)
extern void NullSolverImpl_GetParticleGrid_mAA02219DE1D1E8AF85ABA638C446EC4646A67F0D (void);
// 0x00000229 System.Void Obi.NullSolverImpl::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void NullSolverImpl_SpatialQuery_m0148C0348711AD8CE00B61647ED4D9BDBD212BB0 (void);
// 0x0000022A System.Void Obi.NullSolverImpl::.ctor()
extern void NullSolverImpl__ctor_m4867A668003F79D63D4C15BB6DE916CAAEF9C858 (void);
// 0x0000022B System.Single Obi.IStructuralConstraintBatch::GetRestLength(System.Int32)
// 0x0000022C System.Void Obi.IStructuralConstraintBatch::SetRestLength(System.Int32,System.Single)
// 0x0000022D Obi.ParticlePair Obi.IStructuralConstraintBatch::GetParticleIndices(System.Int32)
// 0x0000022E Oni/ConstraintType Obi.ObiAerodynamicConstraintsBatch::get_constraintType()
extern void ObiAerodynamicConstraintsBatch_get_constraintType_m6040BFC92ACB997BF45396F214CD149999EAD65C (void);
// 0x0000022F Obi.IConstraintsBatchImpl Obi.ObiAerodynamicConstraintsBatch::get_implementation()
extern void ObiAerodynamicConstraintsBatch_get_implementation_m0ABDE56F2D9D55089C4844ACBEB750EDF2F2891B (void);
// 0x00000230 System.Void Obi.ObiAerodynamicConstraintsBatch::.ctor(Obi.ObiAerodynamicConstraintsData)
extern void ObiAerodynamicConstraintsBatch__ctor_m25D792BB54A6216ED777E0024C3F3A0DE44B988F (void);
// 0x00000231 System.Void Obi.ObiAerodynamicConstraintsBatch::AddConstraint(System.Int32,System.Single,System.Single,System.Single)
extern void ObiAerodynamicConstraintsBatch_AddConstraint_mCC6E68CC3D308B67C289888736696E150AF8EA2A (void);
// 0x00000232 System.Void Obi.ObiAerodynamicConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m027B6DE66398E0F8471AA932CB4AE4FB89B72C7B (void);
// 0x00000233 System.Void Obi.ObiAerodynamicConstraintsBatch::Clear()
extern void ObiAerodynamicConstraintsBatch_Clear_m10E7BF1CCDCA05D61B7E0E9189F51A3663537A03 (void);
// 0x00000234 System.Void Obi.ObiAerodynamicConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiAerodynamicConstraintsBatch_SwapConstraints_mFD22DF1620944363A9323610E7465F71F4471A92 (void);
// 0x00000235 System.Void Obi.ObiAerodynamicConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiAerodynamicConstraintsBatch_Merge_m9308B92A6AF8D02A36217C18C6CAE958DA55D829 (void);
// 0x00000236 System.Void Obi.ObiAerodynamicConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_AddToSolver_m9443F9131AAFFD918FA8CE86F7BEF600EEE5906A (void);
// 0x00000237 System.Void Obi.ObiAerodynamicConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiAerodynamicConstraintsBatch_RemoveFromSolver_mE7D0473018702606B66D30240083EF253AE9B1C3 (void);
// 0x00000238 Oni/ConstraintType Obi.ObiBendConstraintsBatch::get_constraintType()
extern void ObiBendConstraintsBatch_get_constraintType_mC9A58918A83E7B2FBA6DC1DADAEAD4A8BEC787D5 (void);
// 0x00000239 Obi.IConstraintsBatchImpl Obi.ObiBendConstraintsBatch::get_implementation()
extern void ObiBendConstraintsBatch_get_implementation_m18F9C4C0D6D548BDB54280777BAD07CDEBA8BBB4 (void);
// 0x0000023A System.Void Obi.ObiBendConstraintsBatch::.ctor(Obi.ObiBendConstraintsData)
extern void ObiBendConstraintsBatch__ctor_m7177EA44789A81DD8E136F47A1B93FA577265FEB (void);
// 0x0000023B System.Void Obi.ObiBendConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendConstraintsBatch_Merge_mD52244354E9F60085E0A515A3878673195133FA7 (void);
// 0x0000023C System.Void Obi.ObiBendConstraintsBatch::AddConstraint(UnityEngine.Vector3Int,System.Single)
extern void ObiBendConstraintsBatch_AddConstraint_m91A746106C49A8D164BCB77F1AD08B7E21186E62 (void);
// 0x0000023D System.Void Obi.ObiBendConstraintsBatch::Clear()
extern void ObiBendConstraintsBatch_Clear_m8071A4A1941BBD6947334864521EF1AD9F26821D (void);
// 0x0000023E System.Void Obi.ObiBendConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendConstraintsBatch_GetParticlesInvolved_mA10C16017FC5ED1C4B0ACD68F562D6F2D0BA2BFF (void);
// 0x0000023F System.Void Obi.ObiBendConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendConstraintsBatch_SwapConstraints_m5DBFB004A248585886F229CD4659BA997C2CB916 (void);
// 0x00000240 System.Void Obi.ObiBendConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_AddToSolver_mFED1BD899374F8D91B54DC1740A5408AA695251F (void);
// 0x00000241 System.Void Obi.ObiBendConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendConstraintsBatch_RemoveFromSolver_mE430D71519380719B08014FE7A4BA992CD45DCD9 (void);
// 0x00000242 Oni/ConstraintType Obi.ObiBendTwistConstraintsBatch::get_constraintType()
extern void ObiBendTwistConstraintsBatch_get_constraintType_m87C6A4463F0BD0C9B47DC213C9BB56847790B79F (void);
// 0x00000243 Obi.IConstraintsBatchImpl Obi.ObiBendTwistConstraintsBatch::get_implementation()
extern void ObiBendTwistConstraintsBatch_get_implementation_m7797868D0B35B4B8A7AFF96506FF43526AE6A702 (void);
// 0x00000244 System.Void Obi.ObiBendTwistConstraintsBatch::.ctor(Obi.ObiBendTwistConstraintsData)
extern void ObiBendTwistConstraintsBatch__ctor_m41B5BBAB31EFB0541152B01E186B5C39FF26A9E0 (void);
// 0x00000245 System.Void Obi.ObiBendTwistConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,UnityEngine.Quaternion)
extern void ObiBendTwistConstraintsBatch_AddConstraint_m41ED663245750ADB2C9EC752E27CC828F5E63A0B (void);
// 0x00000246 System.Void Obi.ObiBendTwistConstraintsBatch::Clear()
extern void ObiBendTwistConstraintsBatch_Clear_mD3AECC462BA3746C51058B0C02A853340048AC3C (void);
// 0x00000247 System.Void Obi.ObiBendTwistConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiBendTwistConstraintsBatch_GetParticlesInvolved_m6C5D97C54F11F31FD1A01289455E8FD60A7A63A1 (void);
// 0x00000248 System.Void Obi.ObiBendTwistConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiBendTwistConstraintsBatch_SwapConstraints_mFC1004BCF2D2A2EC8EC3C522D0CA241C0971A50C (void);
// 0x00000249 System.Void Obi.ObiBendTwistConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiBendTwistConstraintsBatch_Merge_m2DE903B520FEE9A093E986C89300AF27E10E82CB (void);
// 0x0000024A System.Void Obi.ObiBendTwistConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_AddToSolver_m53FB4DC851D4EAB187A7694BFC924617655E4722 (void);
// 0x0000024B System.Void Obi.ObiBendTwistConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiBendTwistConstraintsBatch_RemoveFromSolver_m71A2C2D8C1361AAEA0626C63DC30A32984992C96 (void);
// 0x0000024C Oni/ConstraintType Obi.ObiChainConstraintsBatch::get_constraintType()
extern void ObiChainConstraintsBatch_get_constraintType_m32BA3CE0851520BB2428F43686BE579E2263780C (void);
// 0x0000024D Obi.IConstraintsBatchImpl Obi.ObiChainConstraintsBatch::get_implementation()
extern void ObiChainConstraintsBatch_get_implementation_m06CB0C729ED6E6A5C6DAE81F07966875C590C46D (void);
// 0x0000024E System.Void Obi.ObiChainConstraintsBatch::.ctor(Obi.ObiChainConstraintsData)
extern void ObiChainConstraintsBatch__ctor_m29D8DFF83BDC43705D07512AB3C4E4310D40847E (void);
// 0x0000024F System.Void Obi.ObiChainConstraintsBatch::AddConstraint(System.Int32[],System.Single,System.Single,System.Single)
extern void ObiChainConstraintsBatch_AddConstraint_mCC67D71A5D82D67E8A58D3AAFCAC57EEAE88251A (void);
// 0x00000250 System.Void Obi.ObiChainConstraintsBatch::Clear()
extern void ObiChainConstraintsBatch_Clear_mEE2C2127119C0BC02DA141E3CE6A87E0123B7CC0 (void);
// 0x00000251 System.Void Obi.ObiChainConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiChainConstraintsBatch_GetParticlesInvolved_m347FE60058ADC07D507DC3759DBC7AD776BFE24A (void);
// 0x00000252 System.Void Obi.ObiChainConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiChainConstraintsBatch_SwapConstraints_m2BF8A9FE3C1FD267B2DD0A293E9581D3321FD6E6 (void);
// 0x00000253 System.Void Obi.ObiChainConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiChainConstraintsBatch_Merge_m3FEF585F1BD6CA3549BAFEF02F801B05487F974C (void);
// 0x00000254 System.Void Obi.ObiChainConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_AddToSolver_m3FFDF9CE593E40DADE1FDE3CFE2D551F877E542F (void);
// 0x00000255 System.Void Obi.ObiChainConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiChainConstraintsBatch_RemoveFromSolver_mD5349882C9454FD14040CF4B4B01DFF2956981D4 (void);
// 0x00000256 System.Int32 Obi.IObiConstraintsBatch::get_constraintCount()
// 0x00000257 System.Int32 Obi.IObiConstraintsBatch::get_activeConstraintCount()
// 0x00000258 System.Void Obi.IObiConstraintsBatch::set_activeConstraintCount(System.Int32)
// 0x00000259 System.Int32 Obi.IObiConstraintsBatch::get_initialActiveConstraintCount()
// 0x0000025A System.Void Obi.IObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
// 0x0000025B Oni/ConstraintType Obi.IObiConstraintsBatch::get_constraintType()
// 0x0000025C Obi.IConstraintsBatchImpl Obi.IObiConstraintsBatch::get_implementation()
// 0x0000025D System.Void Obi.IObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x0000025E System.Void Obi.IObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x0000025F System.Void Obi.IObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
// 0x00000260 System.Boolean Obi.IObiConstraintsBatch::DeactivateConstraint(System.Int32)
// 0x00000261 System.Boolean Obi.IObiConstraintsBatch::ActivateConstraint(System.Int32)
// 0x00000262 System.Void Obi.IObiConstraintsBatch::DeactivateAllConstraints()
// 0x00000263 System.Void Obi.IObiConstraintsBatch::Clear()
// 0x00000264 System.Void Obi.IObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000265 System.Void Obi.IObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
// 0x00000266 System.Int32 Obi.ObiConstraintsBatch::get_constraintCount()
extern void ObiConstraintsBatch_get_constraintCount_m4E00272D7170BBFC571EE715EEE8828D96D02E52 (void);
// 0x00000267 System.Int32 Obi.ObiConstraintsBatch::get_activeConstraintCount()
extern void ObiConstraintsBatch_get_activeConstraintCount_m6E6D5CDF52509352BF2355216995F8398BED7180 (void);
// 0x00000268 System.Void Obi.ObiConstraintsBatch::set_activeConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_activeConstraintCount_m47CB4B1E0D9BB5763BF4E3B76A1ABC409B8827B5 (void);
// 0x00000269 System.Int32 Obi.ObiConstraintsBatch::get_initialActiveConstraintCount()
extern void ObiConstraintsBatch_get_initialActiveConstraintCount_mEE507FB39B4E352F6FA6FD13D3861A21242FC560 (void);
// 0x0000026A System.Void Obi.ObiConstraintsBatch::set_initialActiveConstraintCount(System.Int32)
extern void ObiConstraintsBatch_set_initialActiveConstraintCount_mBD3AF514882972EE52BF3A223559393BFEDBD657 (void);
// 0x0000026B Oni/ConstraintType Obi.ObiConstraintsBatch::get_constraintType()
// 0x0000026C Obi.IConstraintsBatchImpl Obi.ObiConstraintsBatch::get_implementation()
// 0x0000026D System.Void Obi.ObiConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiConstraintsBatch_Merge_m1D91B98297B3622D69B5D72E659770E3CFC8BE58 (void);
// 0x0000026E System.Void Obi.ObiConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
// 0x0000026F System.Void Obi.ObiConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
// 0x00000270 System.Void Obi.ObiConstraintsBatch::AddToSolver(Obi.ObiSolver)
// 0x00000271 System.Void Obi.ObiConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
// 0x00000272 System.Void Obi.ObiConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiConstraintsBatch_CopyConstraint_mED5F0FA303BD8AE027DEDC7EA7FAA7CA85993222 (void);
// 0x00000273 System.Void Obi.ObiConstraintsBatch::InnerSwapConstraints(System.Int32,System.Int32)
extern void ObiConstraintsBatch_InnerSwapConstraints_m96D1004604F1B82F62C5497409485E7E3CA44E3E (void);
// 0x00000274 System.Void Obi.ObiConstraintsBatch::RegisterConstraint()
extern void ObiConstraintsBatch_RegisterConstraint_mE486157FBC98DDE17CD1F1EA22063D7BD8A115BD (void);
// 0x00000275 System.Void Obi.ObiConstraintsBatch::Clear()
extern void ObiConstraintsBatch_Clear_mDDF32C36C5A9723AD134D7D49BCE5ED0A2628A3C (void);
// 0x00000276 System.Int32 Obi.ObiConstraintsBatch::GetConstraintIndex(System.Int32)
extern void ObiConstraintsBatch_GetConstraintIndex_mB953302720E7FA7A466C98A24CA47752CCA318D4 (void);
// 0x00000277 System.Boolean Obi.ObiConstraintsBatch::IsConstraintActive(System.Int32)
extern void ObiConstraintsBatch_IsConstraintActive_m22A8784C1241B0AF1FA125755BA86A83139EA33F (void);
// 0x00000278 System.Boolean Obi.ObiConstraintsBatch::ActivateConstraint(System.Int32)
extern void ObiConstraintsBatch_ActivateConstraint_m9CBA21CDC67196756D939CB2AF5A3AEE4C875670 (void);
// 0x00000279 System.Boolean Obi.ObiConstraintsBatch::DeactivateConstraint(System.Int32)
extern void ObiConstraintsBatch_DeactivateConstraint_m1B5361FE327AF1B74F28928B2820F43377C990DC (void);
// 0x0000027A System.Void Obi.ObiConstraintsBatch::DeactivateAllConstraints()
extern void ObiConstraintsBatch_DeactivateAllConstraints_m464A79016D1A1BFED227F7578BCBDCD5A42557FA (void);
// 0x0000027B System.Void Obi.ObiConstraintsBatch::RemoveConstraint(System.Int32)
extern void ObiConstraintsBatch_RemoveConstraint_m8189637BE0D2051E091ECB7715D8E0285D84DEC6 (void);
// 0x0000027C System.Void Obi.ObiConstraintsBatch::ParticlesSwapped(System.Int32,System.Int32)
extern void ObiConstraintsBatch_ParticlesSwapped_m69696C36C7D9ED549CBA88325807B8E267B6BCCD (void);
// 0x0000027D System.Void Obi.ObiConstraintsBatch::.ctor()
extern void ObiConstraintsBatch__ctor_m193FBCE314B9D8E37B9994E58BCD42C4CA54ED47 (void);
// 0x0000027E Oni/ConstraintType Obi.ObiDistanceConstraintsBatch::get_constraintType()
extern void ObiDistanceConstraintsBatch_get_constraintType_m190A5F03319B7CD354FA70476ECDDB0CF497E977 (void);
// 0x0000027F Obi.IConstraintsBatchImpl Obi.ObiDistanceConstraintsBatch::get_implementation()
extern void ObiDistanceConstraintsBatch_get_implementation_m4BEB1A164478614379046670E0D995C1C7973B0F (void);
// 0x00000280 System.Void Obi.ObiDistanceConstraintsBatch::.ctor(System.Int32)
extern void ObiDistanceConstraintsBatch__ctor_m790FDB511304C5885D8839BFE82C87017E8EE617 (void);
// 0x00000281 System.Void Obi.ObiDistanceConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single)
extern void ObiDistanceConstraintsBatch_AddConstraint_mCFA5F34C60D27357731AF57A53F1BE4FFC4A9331 (void);
// 0x00000282 System.Void Obi.ObiDistanceConstraintsBatch::Clear()
extern void ObiDistanceConstraintsBatch_Clear_mECADC8966C7B33BE6DBAC7EEA077852C4894759B (void);
// 0x00000283 System.Single Obi.ObiDistanceConstraintsBatch::GetRestLength(System.Int32)
extern void ObiDistanceConstraintsBatch_GetRestLength_mF522E6802E0FF6E2AAC75B8DA1E2D39ACD16C8FA (void);
// 0x00000284 System.Void Obi.ObiDistanceConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiDistanceConstraintsBatch_SetRestLength_m19C64337495E2F77549651EFFD7EE9C9AEFE4A7A (void);
// 0x00000285 Obi.ParticlePair Obi.ObiDistanceConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiDistanceConstraintsBatch_GetParticleIndices_mBAFEA360DCBD24C1CA6F11D1DF3A2103E8AAD5E8 (void);
// 0x00000286 System.Void Obi.ObiDistanceConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiDistanceConstraintsBatch_GetParticlesInvolved_m96004703CA8F6B2E355FEC2DB57FE8752936AE85 (void);
// 0x00000287 System.Void Obi.ObiDistanceConstraintsBatch::CopyConstraint(Obi.ObiConstraintsBatch,System.Int32)
extern void ObiDistanceConstraintsBatch_CopyConstraint_m770BEC33F923510422483EA234C6A555C5E87E8F (void);
// 0x00000288 System.Void Obi.ObiDistanceConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiDistanceConstraintsBatch_SwapConstraints_mFAA5EFF7DE1FDEB90650770DD4AE8F2AA55F9311 (void);
// 0x00000289 System.Void Obi.ObiDistanceConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiDistanceConstraintsBatch_Merge_m18C8BB0A4EA64EBE8519D3337EEEB0C66F31751B (void);
// 0x0000028A System.Void Obi.ObiDistanceConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_AddToSolver_mCD30473D6B3DBCCA9DF557F59796050E36C0CE19 (void);
// 0x0000028B System.Void Obi.ObiDistanceConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiDistanceConstraintsBatch_RemoveFromSolver_m3DD0B0F8DC45B9CC4219B6D371552416AA07DF13 (void);
// 0x0000028C Oni/ConstraintType Obi.ObiPinConstraintsBatch::get_constraintType()
extern void ObiPinConstraintsBatch_get_constraintType_m9DEFC47C40861F453CDDA876B210345477BF4DD1 (void);
// 0x0000028D Obi.IConstraintsBatchImpl Obi.ObiPinConstraintsBatch::get_implementation()
extern void ObiPinConstraintsBatch_get_implementation_mF812C515ADEF9F33AB0FABC5EBD6A259C16525AE (void);
// 0x0000028E System.Void Obi.ObiPinConstraintsBatch::.ctor(Obi.ObiPinConstraintsData)
extern void ObiPinConstraintsBatch__ctor_m4C81948872E9934B4AA9056CD5C739D0D96ACB22 (void);
// 0x0000028F System.Void Obi.ObiPinConstraintsBatch::AddConstraint(System.Int32,Obi.ObiColliderBase,UnityEngine.Vector3,UnityEngine.Quaternion,System.Single,System.Single,System.Single)
extern void ObiPinConstraintsBatch_AddConstraint_m0838F4170FC5AB9D0B133800A0E1C6AC52D4D13B (void);
// 0x00000290 System.Void Obi.ObiPinConstraintsBatch::Clear()
extern void ObiPinConstraintsBatch_Clear_m7E365B1B1213EDE1E31991C106106D149D700279 (void);
// 0x00000291 System.Void Obi.ObiPinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiPinConstraintsBatch_GetParticlesInvolved_mED7DDAB3F66241C5614E05BFBA6F30F92F8AA889 (void);
// 0x00000292 System.Void Obi.ObiPinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiPinConstraintsBatch_SwapConstraints_m8385C5A9537EC815E9A66D83C11ABB5C974B503A (void);
// 0x00000293 System.Void Obi.ObiPinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiPinConstraintsBatch_Merge_mCD532E7A783752197FF842B6EBB0265F279B5996 (void);
// 0x00000294 System.Void Obi.ObiPinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_AddToSolver_mF594ECA9A0CD268EC8333EC6184F84FD776D6C40 (void);
// 0x00000295 System.Void Obi.ObiPinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiPinConstraintsBatch_RemoveFromSolver_m0D8B9453D75CB94EB1B3B318C14D65B86AC5C754 (void);
// 0x00000296 Oni/ConstraintType Obi.ObiShapeMatchingConstraintsBatch::get_constraintType()
extern void ObiShapeMatchingConstraintsBatch_get_constraintType_mB50B35888E0F97B1AF0C7BC4D8CDE190231AF4F6 (void);
// 0x00000297 Obi.IConstraintsBatchImpl Obi.ObiShapeMatchingConstraintsBatch::get_implementation()
extern void ObiShapeMatchingConstraintsBatch_get_implementation_m4BA8C5859DCF47C334FE7E797B5A3A806591E326 (void);
// 0x00000298 System.Void Obi.ObiShapeMatchingConstraintsBatch::.ctor(Obi.ObiShapeMatchingConstraintsData)
extern void ObiShapeMatchingConstraintsBatch__ctor_m5ACB157EB4F8783A8FAABF7B6C42728F889579A3 (void);
// 0x00000299 System.Void Obi.ObiShapeMatchingConstraintsBatch::AddConstraint(System.Int32[],System.Boolean)
extern void ObiShapeMatchingConstraintsBatch_AddConstraint_m0ACEA7D8A66234BD089034C904A43CF43E02A822 (void);
// 0x0000029A System.Void Obi.ObiShapeMatchingConstraintsBatch::Clear()
extern void ObiShapeMatchingConstraintsBatch_Clear_m6E50E2ECDCE04F6CA6E42EAC6807257A6EF328E3 (void);
// 0x0000029B System.Void Obi.ObiShapeMatchingConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m7FAC15E7522407CDA84BCCB98697A421079258DA (void);
// 0x0000029C System.Void Obi.ObiShapeMatchingConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiShapeMatchingConstraintsBatch_SwapConstraints_m6857C3148FDB065E38C43D5FF42D2AF39E35E8AF (void);
// 0x0000029D System.Void Obi.ObiShapeMatchingConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiShapeMatchingConstraintsBatch_Merge_m437985B208162205896195E94DDE754E63F8CACF (void);
// 0x0000029E System.Void Obi.ObiShapeMatchingConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_AddToSolver_m96316AF9F461D3C2629064427C5BF983B377732D (void);
// 0x0000029F System.Void Obi.ObiShapeMatchingConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiShapeMatchingConstraintsBatch_RemoveFromSolver_mFFCCC95021893AE8406B3CF572CFCD5A004D4D77 (void);
// 0x000002A0 System.Void Obi.ObiShapeMatchingConstraintsBatch::RecalculateRestShapeMatching()
extern void ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_mF7F1083A3082F08437767C334AF98F6C6836ADB8 (void);
// 0x000002A1 Oni/ConstraintType Obi.ObiSkinConstraintsBatch::get_constraintType()
extern void ObiSkinConstraintsBatch_get_constraintType_m43617264E9A74606E427CF12AD73FC33A1C3EDB0 (void);
// 0x000002A2 Obi.IConstraintsBatchImpl Obi.ObiSkinConstraintsBatch::get_implementation()
extern void ObiSkinConstraintsBatch_get_implementation_mBCCA6519112E7BC4CE35826BE6AD268605C4F7A1 (void);
// 0x000002A3 System.Void Obi.ObiSkinConstraintsBatch::.ctor(Obi.ObiSkinConstraintsData)
extern void ObiSkinConstraintsBatch__ctor_mD3168603B7448A20C3A01B84E78CF90C6FA32315 (void);
// 0x000002A4 System.Void Obi.ObiSkinConstraintsBatch::AddConstraint(System.Int32,UnityEngine.Vector4,UnityEngine.Vector4,System.Single,System.Single,System.Single,System.Single)
extern void ObiSkinConstraintsBatch_AddConstraint_mD298AA59592F96CC6666FFB209AA56313D77AC04 (void);
// 0x000002A5 System.Void Obi.ObiSkinConstraintsBatch::Clear()
extern void ObiSkinConstraintsBatch_Clear_m4429DE2477EE5B0C4F853F13924BAFB15D36471B (void);
// 0x000002A6 System.Void Obi.ObiSkinConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiSkinConstraintsBatch_GetParticlesInvolved_m2BB59D7EC8570B6299C571DA5B06200F7002BB41 (void);
// 0x000002A7 System.Void Obi.ObiSkinConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiSkinConstraintsBatch_SwapConstraints_m273EBC39910668056AC0104BBA6B41206A2D862A (void);
// 0x000002A8 System.Void Obi.ObiSkinConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiSkinConstraintsBatch_Merge_m027573A98590755002B0A485032729914654A0A6 (void);
// 0x000002A9 System.Void Obi.ObiSkinConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_AddToSolver_m4AA52CBF9768D0D7FEEB5C51738879C37617D1B7 (void);
// 0x000002AA System.Void Obi.ObiSkinConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiSkinConstraintsBatch_RemoveFromSolver_mF2E5D7885E0EFCA7A9C8E9BF4AA9D034091420BD (void);
// 0x000002AB Oni/ConstraintType Obi.ObiStretchShearConstraintsBatch::get_constraintType()
extern void ObiStretchShearConstraintsBatch_get_constraintType_m948CCFB2E6F31C2D7F55199F2264FFEA244B8A6A (void);
// 0x000002AC Obi.IConstraintsBatchImpl Obi.ObiStretchShearConstraintsBatch::get_implementation()
extern void ObiStretchShearConstraintsBatch_get_implementation_m8FD53A9BC99C7336EAA10901E94F223AAD993DB5 (void);
// 0x000002AD System.Void Obi.ObiStretchShearConstraintsBatch::.ctor(Obi.ObiStretchShearConstraintsData)
extern void ObiStretchShearConstraintsBatch__ctor_mAEF7386671FDAE31146C46155938EA24FB4BB0CF (void);
// 0x000002AE System.Void Obi.ObiStretchShearConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Int32,System.Single,UnityEngine.Quaternion)
extern void ObiStretchShearConstraintsBatch_AddConstraint_mA7EC001A3C4C7029737F902ECB04D90590480E43 (void);
// 0x000002AF System.Void Obi.ObiStretchShearConstraintsBatch::Clear()
extern void ObiStretchShearConstraintsBatch_Clear_m15F14739C62DA79AC798C77123CD119B4AC80199 (void);
// 0x000002B0 System.Single Obi.ObiStretchShearConstraintsBatch::GetRestLength(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetRestLength_m2DBA5798B72F9AC73E56F86741494A6F04BC81C8 (void);
// 0x000002B1 System.Void Obi.ObiStretchShearConstraintsBatch::SetRestLength(System.Int32,System.Single)
extern void ObiStretchShearConstraintsBatch_SetRestLength_mC4634A4AB60B8B44CC7F2121AC3ECD94F686DC2D (void);
// 0x000002B2 Obi.ParticlePair Obi.ObiStretchShearConstraintsBatch::GetParticleIndices(System.Int32)
extern void ObiStretchShearConstraintsBatch_GetParticleIndices_m700F60CDF847F7BD660D81A3C486224CEF926298 (void);
// 0x000002B3 System.Void Obi.ObiStretchShearConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiStretchShearConstraintsBatch_GetParticlesInvolved_m20F312A352681BC3FC07FA6FDC894502B225AD7F (void);
// 0x000002B4 System.Void Obi.ObiStretchShearConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiStretchShearConstraintsBatch_SwapConstraints_m33C0BEA7DDEFC13F89F85C5681EC57D291643A92 (void);
// 0x000002B5 System.Void Obi.ObiStretchShearConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiStretchShearConstraintsBatch_Merge_m48F422BC5E5CF3FC834F41A6E167DC9F12A3498C (void);
// 0x000002B6 System.Void Obi.ObiStretchShearConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_AddToSolver_mFA26026F08631A8DA6DA75493B3D76F5138579A6 (void);
// 0x000002B7 System.Void Obi.ObiStretchShearConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStretchShearConstraintsBatch_RemoveFromSolver_mBA6C140C81E8490A1BEE1EB1A7FB31E225B91793 (void);
// 0x000002B8 Oni/ConstraintType Obi.ObiTetherConstraintsBatch::get_constraintType()
extern void ObiTetherConstraintsBatch_get_constraintType_mB5C8D6C8977031D53A14EF35674B3BA7FB18E98C (void);
// 0x000002B9 Obi.IConstraintsBatchImpl Obi.ObiTetherConstraintsBatch::get_implementation()
extern void ObiTetherConstraintsBatch_get_implementation_mF4449DF39A0BE8629FB4FEE849CADD130D088E02 (void);
// 0x000002BA System.Void Obi.ObiTetherConstraintsBatch::.ctor(Obi.ObiTetherConstraintsData)
extern void ObiTetherConstraintsBatch__ctor_m1ECCE4C0E33C5E6FC0D4837CD7E3E7A8A1C11843 (void);
// 0x000002BB System.Void Obi.ObiTetherConstraintsBatch::AddConstraint(UnityEngine.Vector2Int,System.Single,System.Single)
extern void ObiTetherConstraintsBatch_AddConstraint_m00340D7296944D9AF2C498F2EA8CC2185572BB44 (void);
// 0x000002BC System.Void Obi.ObiTetherConstraintsBatch::Clear()
extern void ObiTetherConstraintsBatch_Clear_mE9AF6F043D828602C4CEEBAF7AB000FAD9A5C0AE (void);
// 0x000002BD System.Void Obi.ObiTetherConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiTetherConstraintsBatch_GetParticlesInvolved_m7AEC2CCE6926DC1CBEB75D7464FBAE2EC2F143C3 (void);
// 0x000002BE System.Void Obi.ObiTetherConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiTetherConstraintsBatch_SwapConstraints_m559BE7B7AE7337697B4A1A0DD7E2BAEF9C981589 (void);
// 0x000002BF System.Void Obi.ObiTetherConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiTetherConstraintsBatch_Merge_m0EE3CD63815A39D7F257DC36E9A9804CE2AF4A10 (void);
// 0x000002C0 System.Void Obi.ObiTetherConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_AddToSolver_mDD0E12C205B28409E7C98D707C4B8CDF918B540C (void);
// 0x000002C1 System.Void Obi.ObiTetherConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiTetherConstraintsBatch_RemoveFromSolver_m74CADEE6D4CF79CCF69FCA20F7ACD8C47EC59C7E (void);
// 0x000002C2 System.Void Obi.ObiTetherConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiTetherConstraintsBatch_SetParameters_m62F2FAB128BB8A7AD07564AB8C575C059B13CD22 (void);
// 0x000002C3 Oni/ConstraintType Obi.ObiVolumeConstraintsBatch::get_constraintType()
extern void ObiVolumeConstraintsBatch_get_constraintType_m9D81BF01ED857E956C5DE50E9F994D9C86E53660 (void);
// 0x000002C4 Obi.IConstraintsBatchImpl Obi.ObiVolumeConstraintsBatch::get_implementation()
extern void ObiVolumeConstraintsBatch_get_implementation_m946F7C41F8CE2ACD0D7615FD5548CD9F530EF41D (void);
// 0x000002C5 System.Void Obi.ObiVolumeConstraintsBatch::.ctor(Obi.ObiVolumeConstraintsData)
extern void ObiVolumeConstraintsBatch__ctor_mB2647CF2902FB0A0CA1A69FF19FC239B4830DC94 (void);
// 0x000002C6 System.Void Obi.ObiVolumeConstraintsBatch::AddConstraint(System.Int32[],System.Single)
extern void ObiVolumeConstraintsBatch_AddConstraint_mBA70D54B618AB3B3EAEBC07556F8A84E6007610A (void);
// 0x000002C7 System.Void Obi.ObiVolumeConstraintsBatch::Clear()
extern void ObiVolumeConstraintsBatch_Clear_m48637D46B4B6523DAF0CFEAA333282C1B5E4E292 (void);
// 0x000002C8 System.Void Obi.ObiVolumeConstraintsBatch::GetParticlesInvolved(System.Int32,System.Collections.Generic.List`1<System.Int32>)
extern void ObiVolumeConstraintsBatch_GetParticlesInvolved_m2F02EF9CE76285F168C567DD1F7BB646D8604352 (void);
// 0x000002C9 System.Void Obi.ObiVolumeConstraintsBatch::SwapConstraints(System.Int32,System.Int32)
extern void ObiVolumeConstraintsBatch_SwapConstraints_m29154135DDF6A4013A74CE93533E4DC6CBEF8875 (void);
// 0x000002CA System.Void Obi.ObiVolumeConstraintsBatch::Merge(Obi.ObiActor,Obi.IObiConstraintsBatch)
extern void ObiVolumeConstraintsBatch_Merge_m83877E6ED340BC6DDBA77555354DE250F775B2A7 (void);
// 0x000002CB System.Void Obi.ObiVolumeConstraintsBatch::AddToSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_AddToSolver_m28665035A10D5B25A542D1021FEC8FB8433FBCAC (void);
// 0x000002CC System.Void Obi.ObiVolumeConstraintsBatch::RemoveFromSolver(Obi.ObiSolver)
extern void ObiVolumeConstraintsBatch_RemoveFromSolver_m4613CB7776E67CF56AF07D61F6FF848351542945 (void);
// 0x000002CD System.Void Obi.ObiVolumeConstraintsBatch::SetParameters(System.Single,System.Single)
extern void ObiVolumeConstraintsBatch_SetParameters_m5242E860F0532140C4D70AFFBEC136C06C863112 (void);
// 0x000002CE System.Boolean Obi.IAerodynamicConstraintsUser::get_aerodynamicsEnabled()
// 0x000002CF System.Void Obi.IAerodynamicConstraintsUser::set_aerodynamicsEnabled(System.Boolean)
// 0x000002D0 System.Single Obi.IAerodynamicConstraintsUser::get_drag()
// 0x000002D1 System.Void Obi.IAerodynamicConstraintsUser::set_drag(System.Single)
// 0x000002D2 System.Single Obi.IAerodynamicConstraintsUser::get_lift()
// 0x000002D3 System.Void Obi.IAerodynamicConstraintsUser::set_lift(System.Single)
// 0x000002D4 Obi.ObiAerodynamicConstraintsBatch Obi.ObiAerodynamicConstraintsData::CreateBatch(Obi.ObiAerodynamicConstraintsBatch)
extern void ObiAerodynamicConstraintsData_CreateBatch_m9F8687477420F05FA5DAF93D6517BE199B9758AE (void);
// 0x000002D5 System.Void Obi.ObiAerodynamicConstraintsData::.ctor()
extern void ObiAerodynamicConstraintsData__ctor_m49F24DDC021A94D8F243AB94D1612D18D7D893A7 (void);
// 0x000002D6 System.Boolean Obi.IBendConstraintsUser::get_bendConstraintsEnabled()
// 0x000002D7 System.Void Obi.IBendConstraintsUser::set_bendConstraintsEnabled(System.Boolean)
// 0x000002D8 System.Single Obi.IBendConstraintsUser::get_bendCompliance()
// 0x000002D9 System.Void Obi.IBendConstraintsUser::set_bendCompliance(System.Single)
// 0x000002DA System.Single Obi.IBendConstraintsUser::get_maxBending()
// 0x000002DB System.Void Obi.IBendConstraintsUser::set_maxBending(System.Single)
// 0x000002DC System.Single Obi.IBendConstraintsUser::get_plasticYield()
// 0x000002DD System.Void Obi.IBendConstraintsUser::set_plasticYield(System.Single)
// 0x000002DE System.Single Obi.IBendConstraintsUser::get_plasticCreep()
// 0x000002DF System.Void Obi.IBendConstraintsUser::set_plasticCreep(System.Single)
// 0x000002E0 Obi.ObiBendConstraintsBatch Obi.ObiBendConstraintsData::CreateBatch(Obi.ObiBendConstraintsBatch)
extern void ObiBendConstraintsData_CreateBatch_m1E5674147A953CADEB7335E6A8EAD6B94B82FE96 (void);
// 0x000002E1 System.Void Obi.ObiBendConstraintsData::.ctor()
extern void ObiBendConstraintsData__ctor_m5CE912D237E59AC2459A0044914C0ADF679D5664 (void);
// 0x000002E2 System.Boolean Obi.IBendTwistConstraintsUser::get_bendTwistConstraintsEnabled()
// 0x000002E3 System.Void Obi.IBendTwistConstraintsUser::set_bendTwistConstraintsEnabled(System.Boolean)
// 0x000002E4 System.Single Obi.IBendTwistConstraintsUser::get_torsionCompliance()
// 0x000002E5 System.Void Obi.IBendTwistConstraintsUser::set_torsionCompliance(System.Single)
// 0x000002E6 System.Single Obi.IBendTwistConstraintsUser::get_bend1Compliance()
// 0x000002E7 System.Void Obi.IBendTwistConstraintsUser::set_bend1Compliance(System.Single)
// 0x000002E8 System.Single Obi.IBendTwistConstraintsUser::get_bend2Compliance()
// 0x000002E9 System.Void Obi.IBendTwistConstraintsUser::set_bend2Compliance(System.Single)
// 0x000002EA System.Single Obi.IBendTwistConstraintsUser::get_plasticYield()
// 0x000002EB System.Void Obi.IBendTwistConstraintsUser::set_plasticYield(System.Single)
// 0x000002EC System.Single Obi.IBendTwistConstraintsUser::get_plasticCreep()
// 0x000002ED System.Void Obi.IBendTwistConstraintsUser::set_plasticCreep(System.Single)
// 0x000002EE Obi.ObiBendTwistConstraintsBatch Obi.ObiBendTwistConstraintsData::CreateBatch(Obi.ObiBendTwistConstraintsBatch)
extern void ObiBendTwistConstraintsData_CreateBatch_m0709FD77E20752F45BA776DC05A130AAD04D7688 (void);
// 0x000002EF System.Void Obi.ObiBendTwistConstraintsData::.ctor()
extern void ObiBendTwistConstraintsData__ctor_mEAD7C51796794836ACBF367D4D41D013A6E4D799 (void);
// 0x000002F0 System.Boolean Obi.IChainConstraintsUser::get_chainConstraintsEnabled()
// 0x000002F1 System.Void Obi.IChainConstraintsUser::set_chainConstraintsEnabled(System.Boolean)
// 0x000002F2 System.Single Obi.IChainConstraintsUser::get_tightness()
// 0x000002F3 System.Void Obi.IChainConstraintsUser::set_tightness(System.Single)
// 0x000002F4 Obi.ObiChainConstraintsBatch Obi.ObiChainConstraintsData::CreateBatch(Obi.ObiChainConstraintsBatch)
extern void ObiChainConstraintsData_CreateBatch_m0BB75C6811F727D1A180B77248740B890BF4864A (void);
// 0x000002F5 System.Void Obi.ObiChainConstraintsData::.ctor()
extern void ObiChainConstraintsData__ctor_m3BA306403D04BAD27188B520142EBA6940392C02 (void);
// 0x000002F6 System.Boolean Obi.IDistanceConstraintsUser::get_distanceConstraintsEnabled()
// 0x000002F7 System.Void Obi.IDistanceConstraintsUser::set_distanceConstraintsEnabled(System.Boolean)
// 0x000002F8 System.Single Obi.IDistanceConstraintsUser::get_stretchingScale()
// 0x000002F9 System.Void Obi.IDistanceConstraintsUser::set_stretchingScale(System.Single)
// 0x000002FA System.Single Obi.IDistanceConstraintsUser::get_stretchCompliance()
// 0x000002FB System.Void Obi.IDistanceConstraintsUser::set_stretchCompliance(System.Single)
// 0x000002FC System.Single Obi.IDistanceConstraintsUser::get_maxCompression()
// 0x000002FD System.Void Obi.IDistanceConstraintsUser::set_maxCompression(System.Single)
// 0x000002FE Obi.ObiDistanceConstraintsBatch Obi.ObiDistanceConstraintsData::CreateBatch(Obi.ObiDistanceConstraintsBatch)
extern void ObiDistanceConstraintsData_CreateBatch_mE76E6B2D1A7CC775E2DCA8BCFE61B4058F7EC8E5 (void);
// 0x000002FF System.Void Obi.ObiDistanceConstraintsData::.ctor()
extern void ObiDistanceConstraintsData__ctor_mD731E72CD8800C60BB8876179E36211C1C7F04E9 (void);
// 0x00000300 Obi.ObiPinConstraintsBatch Obi.ObiPinConstraintsData::CreateBatch(Obi.ObiPinConstraintsBatch)
extern void ObiPinConstraintsData_CreateBatch_m0E2C40753FD7A119CE5A7FD392C9F7BDA0E671CF (void);
// 0x00000301 System.Void Obi.ObiPinConstraintsData::.ctor()
extern void ObiPinConstraintsData__ctor_m744846C675FE4FCBABCB7FC988693E4B5F751157 (void);
// 0x00000302 System.Boolean Obi.IShapeMatchingConstraintsUser::get_shapeMatchingConstraintsEnabled()
// 0x00000303 System.Void Obi.IShapeMatchingConstraintsUser::set_shapeMatchingConstraintsEnabled(System.Boolean)
// 0x00000304 System.Single Obi.IShapeMatchingConstraintsUser::get_deformationResistance()
// 0x00000305 System.Void Obi.IShapeMatchingConstraintsUser::set_deformationResistance(System.Single)
// 0x00000306 System.Single Obi.IShapeMatchingConstraintsUser::get_maxDeformation()
// 0x00000307 System.Void Obi.IShapeMatchingConstraintsUser::set_maxDeformation(System.Single)
// 0x00000308 System.Single Obi.IShapeMatchingConstraintsUser::get_plasticYield()
// 0x00000309 System.Void Obi.IShapeMatchingConstraintsUser::set_plasticYield(System.Single)
// 0x0000030A System.Single Obi.IShapeMatchingConstraintsUser::get_plasticCreep()
// 0x0000030B System.Void Obi.IShapeMatchingConstraintsUser::set_plasticCreep(System.Single)
// 0x0000030C System.Single Obi.IShapeMatchingConstraintsUser::get_plasticRecovery()
// 0x0000030D System.Void Obi.IShapeMatchingConstraintsUser::set_plasticRecovery(System.Single)
// 0x0000030E Obi.ObiShapeMatchingConstraintsBatch Obi.ObiShapeMatchingConstraintsData::CreateBatch(Obi.ObiShapeMatchingConstraintsBatch)
extern void ObiShapeMatchingConstraintsData_CreateBatch_m47612CB07D694D4AC0AE0530497FE3A2A9391573 (void);
// 0x0000030F System.Void Obi.ObiShapeMatchingConstraintsData::.ctor()
extern void ObiShapeMatchingConstraintsData__ctor_m0B38950E582515B04C26082701C793A096B4D592 (void);
// 0x00000310 Obi.ObiSkinConstraintsBatch Obi.ObiSkinConstraintsData::CreateBatch(Obi.ObiSkinConstraintsBatch)
extern void ObiSkinConstraintsData_CreateBatch_m6718125284AD1F35943AA449A41D2202999BFE23 (void);
// 0x00000311 System.Void Obi.ObiSkinConstraintsData::.ctor()
extern void ObiSkinConstraintsData__ctor_m24190FDC7EF8ABA6B97B657E1F6E2CEDE955FE67 (void);
// 0x00000312 System.Boolean Obi.IStretchShearConstraintsUser::get_stretchShearConstraintsEnabled()
// 0x00000313 System.Void Obi.IStretchShearConstraintsUser::set_stretchShearConstraintsEnabled(System.Boolean)
// 0x00000314 System.Single Obi.IStretchShearConstraintsUser::get_stretchCompliance()
// 0x00000315 System.Void Obi.IStretchShearConstraintsUser::set_stretchCompliance(System.Single)
// 0x00000316 System.Single Obi.IStretchShearConstraintsUser::get_shear1Compliance()
// 0x00000317 System.Void Obi.IStretchShearConstraintsUser::set_shear1Compliance(System.Single)
// 0x00000318 System.Single Obi.IStretchShearConstraintsUser::get_shear2Compliance()
// 0x00000319 System.Void Obi.IStretchShearConstraintsUser::set_shear2Compliance(System.Single)
// 0x0000031A Obi.ObiStretchShearConstraintsBatch Obi.ObiStretchShearConstraintsData::CreateBatch(Obi.ObiStretchShearConstraintsBatch)
extern void ObiStretchShearConstraintsData_CreateBatch_m0EF5F33043FD16160BC1BA7732E74CF90196617A (void);
// 0x0000031B System.Void Obi.ObiStretchShearConstraintsData::.ctor()
extern void ObiStretchShearConstraintsData__ctor_mE207C4FC05457C2259CBA1ADD59F6BA3953ABCD8 (void);
// 0x0000031C System.Boolean Obi.ITetherConstraintsUser::get_tetherConstraintsEnabled()
// 0x0000031D System.Void Obi.ITetherConstraintsUser::set_tetherConstraintsEnabled(System.Boolean)
// 0x0000031E System.Single Obi.ITetherConstraintsUser::get_tetherCompliance()
// 0x0000031F System.Void Obi.ITetherConstraintsUser::set_tetherCompliance(System.Single)
// 0x00000320 System.Single Obi.ITetherConstraintsUser::get_tetherScale()
// 0x00000321 System.Void Obi.ITetherConstraintsUser::set_tetherScale(System.Single)
// 0x00000322 Obi.ObiTetherConstraintsBatch Obi.ObiTetherConstraintsData::CreateBatch(Obi.ObiTetherConstraintsBatch)
extern void ObiTetherConstraintsData_CreateBatch_m74B1890FE3281245BF97E41FBDFCA4252FEB1470 (void);
// 0x00000323 System.Void Obi.ObiTetherConstraintsData::.ctor()
extern void ObiTetherConstraintsData__ctor_mEF28EB23B58ECF1EE9DA005B34A316A567588846 (void);
// 0x00000324 System.Boolean Obi.IVolumeConstraintsUser::get_volumeConstraintsEnabled()
// 0x00000325 System.Void Obi.IVolumeConstraintsUser::set_volumeConstraintsEnabled(System.Boolean)
// 0x00000326 System.Single Obi.IVolumeConstraintsUser::get_compressionCompliance()
// 0x00000327 System.Void Obi.IVolumeConstraintsUser::set_compressionCompliance(System.Single)
// 0x00000328 System.Single Obi.IVolumeConstraintsUser::get_pressure()
// 0x00000329 System.Void Obi.IVolumeConstraintsUser::set_pressure(System.Single)
// 0x0000032A Obi.ObiVolumeConstraintsBatch Obi.ObiVolumeConstraintsData::CreateBatch(Obi.ObiVolumeConstraintsBatch)
extern void ObiVolumeConstraintsData_CreateBatch_mC832E9336237193B4075DFF88DAF9F3D19BC6C95 (void);
// 0x0000032B System.Void Obi.ObiVolumeConstraintsData::.ctor()
extern void ObiVolumeConstraintsData__ctor_mC8750405A88786FFF03C3ACBB102CF73DAD1C8EE (void);
// 0x0000032C System.Nullable`1<Oni/ConstraintType> Obi.IObiConstraints::GetConstraintType()
// 0x0000032D Obi.IObiConstraintsBatch Obi.IObiConstraints::GetBatch(System.Int32)
// 0x0000032E System.Int32 Obi.IObiConstraints::GetBatchCount()
// 0x0000032F System.Void Obi.IObiConstraints::Clear()
// 0x00000330 System.Boolean Obi.IObiConstraints::AddToSolver(Obi.ObiSolver)
// 0x00000331 System.Boolean Obi.IObiConstraints::RemoveFromSolver()
// 0x00000332 System.Int32 Obi.IObiConstraints::GetConstraintCount()
// 0x00000333 System.Int32 Obi.IObiConstraints::GetActiveConstraintCount()
// 0x00000334 System.Void Obi.IObiConstraints::DeactivateAllConstraints()
// 0x00000335 System.Void Obi.IObiConstraints::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x00000336 System.Void Obi.ObiConstraints`1::Merge(Obi.ObiActor,Obi.IObiConstraints)
// 0x00000337 Obi.IObiConstraintsBatch Obi.ObiConstraints`1::GetBatch(System.Int32)
// 0x00000338 System.Int32 Obi.ObiConstraints`1::GetBatchCount()
// 0x00000339 System.Int32 Obi.ObiConstraints`1::GetConstraintCount()
// 0x0000033A System.Int32 Obi.ObiConstraints`1::GetActiveConstraintCount()
// 0x0000033B System.Void Obi.ObiConstraints`1::DeactivateAllConstraints()
// 0x0000033C T Obi.ObiConstraints`1::GetFirstBatch()
// 0x0000033D System.Nullable`1<Oni/ConstraintType> Obi.ObiConstraints`1::GetConstraintType()
// 0x0000033E System.Void Obi.ObiConstraints`1::Clear()
// 0x0000033F T Obi.ObiConstraints`1::CreateBatch(T)
// 0x00000340 System.Void Obi.ObiConstraints`1::AddBatch(T)
// 0x00000341 System.Boolean Obi.ObiConstraints`1::RemoveBatch(T)
// 0x00000342 System.Boolean Obi.ObiConstraints`1::AddToSolver(Obi.ObiSolver)
// 0x00000343 System.Boolean Obi.ObiConstraints`1::RemoveFromSolver()
// 0x00000344 System.Void Obi.ObiConstraints`1::.ctor()
// 0x00000345 System.Single Obi.StructuralConstraint::get_restLength()
extern void StructuralConstraint_get_restLength_m4DF4D85631CA2689452264F43D092A2FF9FAD9B7 (void);
// 0x00000346 System.Void Obi.StructuralConstraint::set_restLength(System.Single)
extern void StructuralConstraint_set_restLength_m41F5560600620E38F9CB5122513336B879D36763 (void);
// 0x00000347 System.Void Obi.StructuralConstraint::.ctor(Obi.IStructuralConstraintBatch,System.Int32,System.Single)
extern void StructuralConstraint__ctor_m02AB4F391F2914D0C63E12EA1ABB8A8AB98B00B4 (void);
// 0x00000348 System.Int32[] Obi.GraphColoring::Colorize(System.Int32[],System.Int32[])
extern void GraphColoring_Colorize_m7D42EFE9A1DA9F0D34C1B582B560CF34820CCCDB (void);
// 0x00000349 System.Void Obi.ObiActorBlueprint::add_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_add_OnBlueprintGenerate_m722E32133C4AF676EC31317FC1629275A6D37C3E (void);
// 0x0000034A System.Void Obi.ObiActorBlueprint::remove_OnBlueprintGenerate(Obi.ObiActorBlueprint/BlueprintCallback)
extern void ObiActorBlueprint_remove_OnBlueprintGenerate_mDB764D279BF95D0F705FFC6541C8601C97AEBC0B (void);
// 0x0000034B System.Int32 Obi.ObiActorBlueprint::get_particleCount()
extern void ObiActorBlueprint_get_particleCount_m32DECEC061D75DAD97AA6110A1F22F45EA9B1D30 (void);
// 0x0000034C System.Int32 Obi.ObiActorBlueprint::get_activeParticleCount()
extern void ObiActorBlueprint_get_activeParticleCount_mF61F0B48324015C5FE0946358D3A8205074A442D (void);
// 0x0000034D System.Boolean Obi.ObiActorBlueprint::get_usesOrientedParticles()
extern void ObiActorBlueprint_get_usesOrientedParticles_m28DE6E8AA98625BB7B2BECE4F1A1FC4C8AC589D3 (void);
// 0x0000034E System.Boolean Obi.ObiActorBlueprint::get_usesTethers()
extern void ObiActorBlueprint_get_usesTethers_mA9B61493ACD210D9A6CDF2AC50AE543F9527AD20 (void);
// 0x0000034F System.Boolean Obi.ObiActorBlueprint::IsParticleActive(System.Int32)
extern void ObiActorBlueprint_IsParticleActive_m75AE6706A7726FB3608CF435316F87BA49E44208 (void);
// 0x00000350 System.Void Obi.ObiActorBlueprint::SwapWithFirstInactiveParticle(System.Int32)
extern void ObiActorBlueprint_SwapWithFirstInactiveParticle_m9FFCA89E796C59CABBCDE588A65F29995235478E (void);
// 0x00000351 System.Boolean Obi.ObiActorBlueprint::ActivateParticle(System.Int32)
extern void ObiActorBlueprint_ActivateParticle_m0002FC5E69FD374AA5EFB9BC1F53F279310CDE80 (void);
// 0x00000352 System.Boolean Obi.ObiActorBlueprint::DeactivateParticle(System.Int32)
extern void ObiActorBlueprint_DeactivateParticle_m292A9950AC1F69E56B479D8525405BE5A336DCC9 (void);
// 0x00000353 System.Boolean Obi.ObiActorBlueprint::get_empty()
extern void ObiActorBlueprint_get_empty_m533A4D139D3EE6F026BF2A67B562EDF9C15E2C19 (void);
// 0x00000354 System.Void Obi.ObiActorBlueprint::RecalculateBounds()
extern void ObiActorBlueprint_RecalculateBounds_mC8B3BEAAE2219E8B400E2A9E2B85AC60EC256FCF (void);
// 0x00000355 UnityEngine.Bounds Obi.ObiActorBlueprint::get_bounds()
extern void ObiActorBlueprint_get_bounds_m9A2C87A35CF971985D2A017E65D0F4E83F2465ED (void);
// 0x00000356 System.Collections.Generic.IEnumerable`1<Obi.IObiConstraints> Obi.ObiActorBlueprint::GetConstraints()
extern void ObiActorBlueprint_GetConstraints_mE9073C983B0C46F75C8597D50F54F6E4BD2976CD (void);
// 0x00000357 Obi.IObiConstraints Obi.ObiActorBlueprint::GetConstraintsByType(Oni/ConstraintType)
extern void ObiActorBlueprint_GetConstraintsByType_m850F04FFF6503102CC1285A4A293DF519D94D03C (void);
// 0x00000358 System.Int32 Obi.ObiActorBlueprint::GetParticleRuntimeIndex(System.Int32)
extern void ObiActorBlueprint_GetParticleRuntimeIndex_m338D08056C596909B71962BD41D90A35778185D8 (void);
// 0x00000359 UnityEngine.Vector3 Obi.ObiActorBlueprint::GetParticlePosition(System.Int32)
extern void ObiActorBlueprint_GetParticlePosition_m809557B9E54C966416666946A0116E9B4FE3273A (void);
// 0x0000035A UnityEngine.Quaternion Obi.ObiActorBlueprint::GetParticleOrientation(System.Int32)
extern void ObiActorBlueprint_GetParticleOrientation_mEB166212FF89B1F6B2D0E006F64D480CC474E399 (void);
// 0x0000035B System.Void Obi.ObiActorBlueprint::GetParticleAnisotropy(System.Int32,UnityEngine.Vector4&,UnityEngine.Vector4&,UnityEngine.Vector4&)
extern void ObiActorBlueprint_GetParticleAnisotropy_m29F45586B637695FF214FF3970167149CF4D1C8B (void);
// 0x0000035C System.Single Obi.ObiActorBlueprint::GetParticleMaxRadius(System.Int32)
extern void ObiActorBlueprint_GetParticleMaxRadius_m0930B36B9734F68963DA78EE73868C6F2648783D (void);
// 0x0000035D UnityEngine.Color Obi.ObiActorBlueprint::GetParticleColor(System.Int32)
extern void ObiActorBlueprint_GetParticleColor_m679E28CCF4AE45CD284C1F6998C48BE4E3726756 (void);
// 0x0000035E System.Void Obi.ObiActorBlueprint::GenerateImmediate()
extern void ObiActorBlueprint_GenerateImmediate_m6400F73812A2A099A121C9FD603D956E0CD1E032 (void);
// 0x0000035F System.Collections.IEnumerator Obi.ObiActorBlueprint::Generate()
extern void ObiActorBlueprint_Generate_m5080D99D3401C5397E728FE6384DAAC0E43E8F1F (void);
// 0x00000360 Obi.ObiParticleGroup Obi.ObiActorBlueprint::InsertNewParticleGroup(System.String,System.Int32)
extern void ObiActorBlueprint_InsertNewParticleGroup_m627C6C0E3C99092D87981BA9F372CB364A73A069 (void);
// 0x00000361 Obi.ObiParticleGroup Obi.ObiActorBlueprint::AppendNewParticleGroup(System.String)
extern void ObiActorBlueprint_AppendNewParticleGroup_m0E0CC2FF6766EB812B4C33F4F9FB65491206A0F5 (void);
// 0x00000362 System.Boolean Obi.ObiActorBlueprint::RemoveParticleGroupAt(System.Int32)
extern void ObiActorBlueprint_RemoveParticleGroupAt_m44C677861ADADBA8160EA3346B9352A3DBBE087A (void);
// 0x00000363 System.Boolean Obi.ObiActorBlueprint::SetParticleGroupName(System.Int32,System.String)
extern void ObiActorBlueprint_SetParticleGroupName_m8972A24BFC1B01B329F6D302A550FFD5FC1BD958 (void);
// 0x00000364 System.Void Obi.ObiActorBlueprint::ClearParticleGroups()
extern void ObiActorBlueprint_ClearParticleGroups_mBD9DEF28A12E32A8FE58BD535DBE4A64FC1ACCA1 (void);
// 0x00000365 System.Boolean Obi.ObiActorBlueprint::IsParticleSharedInConstraint(System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_IsParticleSharedInConstraint_m65F2A72F6ADA8A0103B4D954B76202AFF032DAD1 (void);
// 0x00000366 System.Boolean Obi.ObiActorBlueprint::DoesParticleShareConstraints(Obi.IObiConstraints,System.Int32,System.Collections.Generic.List`1<System.Int32>,System.Boolean[])
extern void ObiActorBlueprint_DoesParticleShareConstraints_m58578F416B7A5F855D954FB941CBF5F1E077A0DD (void);
// 0x00000367 System.Void Obi.ObiActorBlueprint::DeactivateConstraintsWithInactiveParticles(Obi.IObiConstraints,System.Collections.Generic.List`1<System.Int32>)
extern void ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_mE8ADC81B4819AF3F669A012D70018743A61C9191 (void);
// 0x00000368 System.Void Obi.ObiActorBlueprint::ParticlesSwappedInGroups(System.Int32,System.Int32)
extern void ObiActorBlueprint_ParticlesSwappedInGroups_mC8E1F44F0E7018E98D78F31AB71B4BB2FA630967 (void);
// 0x00000369 System.Void Obi.ObiActorBlueprint::RemoveSelectedParticles(System.Boolean[]&,System.Boolean)
extern void ObiActorBlueprint_RemoveSelectedParticles_m38C23BA2058BDC49997BFC139F89DAB8A1E05FB5 (void);
// 0x0000036A System.Void Obi.ObiActorBlueprint::RestoreRemovedParticles()
extern void ObiActorBlueprint_RestoreRemovedParticles_m583966D5F1B5F57A8F9F085870D1CD7181902AE9 (void);
// 0x0000036B System.Void Obi.ObiActorBlueprint::GenerateTethers(System.Boolean[])
extern void ObiActorBlueprint_GenerateTethers_m9EE5A9F23039CDC0802E991B78CE7BD0C0CA4CF3 (void);
// 0x0000036C System.Void Obi.ObiActorBlueprint::ClearTethers()
extern void ObiActorBlueprint_ClearTethers_m6542371776C70D0EE153EEBF0B89555400C214D0 (void);
// 0x0000036D System.Collections.IEnumerator Obi.ObiActorBlueprint::Initialize()
// 0x0000036E System.Void Obi.ObiActorBlueprint::.ctor()
extern void ObiActorBlueprint__ctor_mCF461CF7ADF91137650AC6A2F0201895FB871139 (void);
// 0x0000036F System.Void Obi.ObiActorBlueprint/BlueprintCallback::.ctor(System.Object,System.IntPtr)
extern void BlueprintCallback__ctor_m1C05FA54C75C74D2FE31BEFA3CD5DD5A48A537A2 (void);
// 0x00000370 System.Void Obi.ObiActorBlueprint/BlueprintCallback::Invoke(Obi.ObiActorBlueprint)
extern void BlueprintCallback_Invoke_m0DAA83F8560B7A01E02BE1BEA342B8FF282AA690 (void);
// 0x00000371 System.IAsyncResult Obi.ObiActorBlueprint/BlueprintCallback::BeginInvoke(Obi.ObiActorBlueprint,System.AsyncCallback,System.Object)
extern void BlueprintCallback_BeginInvoke_m68AB7718861BED5EA793C69612787216135F5A65 (void);
// 0x00000372 System.Void Obi.ObiActorBlueprint/BlueprintCallback::EndInvoke(System.IAsyncResult)
extern void BlueprintCallback_EndInvoke_mC1B9B01B080421E8F4040DD9783C8B7E067ED9C7 (void);
// 0x00000373 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::.ctor(System.Int32)
extern void U3CGetConstraintsU3Ed__50__ctor_mD6AF1993959B36E1A204AB0AFC94D2FBB8CDB038 (void);
// 0x00000374 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.IDisposable.Dispose()
extern void U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m2B66041A7F60DD9BC93703E04F250E7F0613BE2A (void);
// 0x00000375 System.Boolean Obi.ObiActorBlueprint/<GetConstraints>d__50::MoveNext()
extern void U3CGetConstraintsU3Ed__50_MoveNext_m2BB3F4BDE3CF53D8691ED4A48BC062995BA6D05E (void);
// 0x00000376 Obi.IObiConstraints Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerator<Obi.IObiConstraints>.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m9A79580894AEB14A2B0BA62AAE39A1B392E9F4FC (void);
// 0x00000377 System.Void Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.Reset()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m458EABAF66430791D0C1CFE4A20D27E310B7591F (void);
// 0x00000378 System.Object Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mCEEDBE01723DA154C8685D2823E481C78769A50B (void);
// 0x00000379 System.Collections.Generic.IEnumerator`1<Obi.IObiConstraints> Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.Generic.IEnumerable<Obi.IObiConstraints>.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_m20BA04CF2264F78BAD7EBD50B5933438AA818484 (void);
// 0x0000037A System.Collections.IEnumerator Obi.ObiActorBlueprint/<GetConstraints>d__50::System.Collections.IEnumerable.GetEnumerator()
extern void U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m26791F98C8A1420900D98771D4DEDC57601B029E (void);
// 0x0000037B System.Void Obi.ObiActorBlueprint/<Generate>d__59::.ctor(System.Int32)
extern void U3CGenerateU3Ed__59__ctor_m54244DFE624F4AEAF10B69E73905B536E557C111 (void);
// 0x0000037C System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__59_System_IDisposable_Dispose_mD3BCFB34C57742D183318F9CA268A1EEFB0F8899 (void);
// 0x0000037D System.Boolean Obi.ObiActorBlueprint/<Generate>d__59::MoveNext()
extern void U3CGenerateU3Ed__59_MoveNext_m86CB03056D8522556011343C4E12C569B6E7C306 (void);
// 0x0000037E System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B2BA23F5A3087A28988199727976C63C17E8A3B (void);
// 0x0000037F System.Void Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_mA330FB66D7965D067D607B9B53521BB6B00458D8 (void);
// 0x00000380 System.Object Obi.ObiActorBlueprint/<Generate>d__59::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_m597FA762BE3018F55A1C2EA2F7B92C67CE67F269 (void);
// 0x00000381 System.Void Obi.ObiMeshBasedActorBlueprint::.ctor()
extern void ObiMeshBasedActorBlueprint__ctor_mD1CCC19A27CB4507721539C71BFDC95377B9CCDA (void);
// 0x00000382 Obi.ObiActorBlueprint Obi.ObiParticleGroup::get_blueprint()
extern void ObiParticleGroup_get_blueprint_m9FD4C7214E3E9510B1425BB7896AAAC3ED4F2BBB (void);
// 0x00000383 System.Void Obi.ObiParticleGroup::SetSourceBlueprint(Obi.ObiActorBlueprint)
extern void ObiParticleGroup_SetSourceBlueprint_mA2170558232F906D564DD68BADD9ED61B3751C4E (void);
// 0x00000384 System.Int32 Obi.ObiParticleGroup::get_Count()
extern void ObiParticleGroup_get_Count_m23DCE79FDBAF746E6A907045E50752C42B7CAB2A (void);
// 0x00000385 System.Boolean Obi.ObiParticleGroup::ContainsParticle(System.Int32)
extern void ObiParticleGroup_ContainsParticle_mB11DBD3561F7B98112ADD73A99F65243EBA726A2 (void);
// 0x00000386 System.Void Obi.ObiParticleGroup::.ctor()
extern void ObiParticleGroup__ctor_m885AA5EBCA717A818B14D2033E9782BA50219AF7 (void);
// 0x00000387 System.Void Obi.ObiBoxShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.BoxCollider2D)
extern void ObiBoxShapeTracker2D__ctor_m72A5FABFA97DA009F34441602ECB2E947A9A739F (void);
// 0x00000388 System.Boolean Obi.ObiBoxShapeTracker2D::UpdateIfNeeded()
extern void ObiBoxShapeTracker2D_UpdateIfNeeded_m91193CF6269361586C9008A77A700345F5974D47 (void);
// 0x00000389 System.Void Obi.ObiCapsuleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CapsuleCollider2D)
extern void ObiCapsuleShapeTracker2D__ctor_m4F5BEDEF53623CFA6AB8C0D60297592E45BE9903 (void);
// 0x0000038A System.Boolean Obi.ObiCapsuleShapeTracker2D::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker2D_UpdateIfNeeded_mD0C44C3584228599A8BCEC562B5480C014E1FA95 (void);
// 0x0000038B System.Void Obi.ObiCircleShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.CircleCollider2D)
extern void ObiCircleShapeTracker2D__ctor_mE3F6870036F6B3A95FC6B26DB7A0ECA9B3075730 (void);
// 0x0000038C System.Boolean Obi.ObiCircleShapeTracker2D::UpdateIfNeeded()
extern void ObiCircleShapeTracker2D_UpdateIfNeeded_mB8126562470730326CCD154127C7D30DCDCFF6F9 (void);
// 0x0000038D System.Void Obi.ObiEdgeShapeTracker2D::.ctor(Obi.ObiCollider2D,UnityEngine.EdgeCollider2D)
extern void ObiEdgeShapeTracker2D__ctor_m69E472C578DE3CB917AA570D6991D20C299CC629 (void);
// 0x0000038E System.Void Obi.ObiEdgeShapeTracker2D::UpdateEdgeData()
extern void ObiEdgeShapeTracker2D_UpdateEdgeData_m4FB6C746BBAF32C2DC265793C876F903701F7E2B (void);
// 0x0000038F System.Boolean Obi.ObiEdgeShapeTracker2D::UpdateIfNeeded()
extern void ObiEdgeShapeTracker2D_UpdateIfNeeded_m1DDA9C78DA820F4FE320E1597DFCAC96636B253F (void);
// 0x00000390 System.Void Obi.ObiEdgeShapeTracker2D::Destroy()
extern void ObiEdgeShapeTracker2D_Destroy_mA11D3038473BCD30A48AA1EB8CFDB3F0E22C27F3 (void);
// 0x00000391 System.Void Obi.ObiBoxShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.BoxCollider)
extern void ObiBoxShapeTracker__ctor_mC46FA6587648E8086544D41DBDBA1CFC61748265 (void);
// 0x00000392 System.Boolean Obi.ObiBoxShapeTracker::UpdateIfNeeded()
extern void ObiBoxShapeTracker_UpdateIfNeeded_mDCCB3495573A0CC2B10B8519403C33741F34E86B (void);
// 0x00000393 System.Void Obi.ObiCapsuleShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CapsuleCollider)
extern void ObiCapsuleShapeTracker__ctor_mEACB22598959563B00E783F743C23D9BC38342AE (void);
// 0x00000394 System.Boolean Obi.ObiCapsuleShapeTracker::UpdateIfNeeded()
extern void ObiCapsuleShapeTracker_UpdateIfNeeded_m9E0EB1D7DD68733F0435FC11FFF9BEA4ABC8F809 (void);
// 0x00000395 System.Void Obi.ObiCharacterControllerShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.CharacterController)
extern void ObiCharacterControllerShapeTracker__ctor_m469B48FB58AD955867A5FC3D8336A9F46AA3E3CC (void);
// 0x00000396 System.Boolean Obi.ObiCharacterControllerShapeTracker::UpdateIfNeeded()
extern void ObiCharacterControllerShapeTracker_UpdateIfNeeded_mD88444722EEF14B729AF17E13B360FDA8A31A820 (void);
// 0x00000397 System.Void Obi.ObiDistanceFieldShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.Component,Obi.ObiDistanceField)
extern void ObiDistanceFieldShapeTracker__ctor_mC6E24376FC68B21C5B4B12AA59389FEDECAB9EEB (void);
// 0x00000398 System.Void Obi.ObiDistanceFieldShapeTracker::UpdateDistanceFieldData()
extern void ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_m6E0A0F0E873F5DF290DB3E88063DDD5E0C192EFD (void);
// 0x00000399 System.Boolean Obi.ObiDistanceFieldShapeTracker::UpdateIfNeeded()
extern void ObiDistanceFieldShapeTracker_UpdateIfNeeded_m01F9C5D5243F44C9DB90C9F58A6B450D171895C4 (void);
// 0x0000039A System.Void Obi.ObiDistanceFieldShapeTracker::Destroy()
extern void ObiDistanceFieldShapeTracker_Destroy_m67B3FB02F03A6A627CA88C745D5EE8945D9CE25D (void);
// 0x0000039B System.Void Obi.ObiMeshShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.MeshCollider)
extern void ObiMeshShapeTracker__ctor_m84B37C3D6C5AF9057683315A9695879E8146F5EF (void);
// 0x0000039C System.Void Obi.ObiMeshShapeTracker::UpdateMeshData()
extern void ObiMeshShapeTracker_UpdateMeshData_m792F335D36DDBF67036960222AD503052BD4B441 (void);
// 0x0000039D System.Boolean Obi.ObiMeshShapeTracker::UpdateIfNeeded()
extern void ObiMeshShapeTracker_UpdateIfNeeded_m9750519C394EBEAEAF2D8FF95E52E1CEDC088EB8 (void);
// 0x0000039E System.Void Obi.ObiMeshShapeTracker::Destroy()
extern void ObiMeshShapeTracker_Destroy_m2E0D92720E52771FCCB8FF89561142E9C4375F6B (void);
// 0x0000039F System.Void Obi.ObiShapeTracker::Destroy()
extern void ObiShapeTracker_Destroy_m3D78B0BF6919363219DFC18EEC33E9D5FE51E6E2 (void);
// 0x000003A0 System.Boolean Obi.ObiShapeTracker::UpdateIfNeeded()
// 0x000003A1 System.Void Obi.ObiShapeTracker::.ctor()
extern void ObiShapeTracker__ctor_m6593D9CB64011F3C87EB568E8D4A856A55E4119A (void);
// 0x000003A2 System.Void Obi.ObiSphereShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.SphereCollider)
extern void ObiSphereShapeTracker__ctor_m3283B209BAFB408E7B51A1076A700D57AF9BEA03 (void);
// 0x000003A3 System.Boolean Obi.ObiSphereShapeTracker::UpdateIfNeeded()
extern void ObiSphereShapeTracker_UpdateIfNeeded_mEF48A8DEB3C0291FA68CE7C055F937D84ECB358E (void);
// 0x000003A4 System.Void Obi.ObiTerrainShapeTracker::.ctor(Obi.ObiCollider,UnityEngine.TerrainCollider)
extern void ObiTerrainShapeTracker__ctor_m5BE8BB4A00AB9B0F79345BC17293B181C055C396 (void);
// 0x000003A5 System.Void Obi.ObiTerrainShapeTracker::UpdateHeightData()
extern void ObiTerrainShapeTracker_UpdateHeightData_m1B60D70BF4C97F8FAB0111534209E5CD81187D52 (void);
// 0x000003A6 System.Boolean Obi.ObiTerrainShapeTracker::UpdateIfNeeded()
extern void ObiTerrainShapeTracker_UpdateIfNeeded_m1386544C05562CA1EFD02614875AA5E35D8FEFA9 (void);
// 0x000003A7 System.Void Obi.ObiTerrainShapeTracker::Destroy()
extern void ObiTerrainShapeTracker_Destroy_m8447999AC2ABB7D960382E2389A70435614339E4 (void);
// 0x000003A8 System.Void Obi.ObiCollider::set_sourceCollider(UnityEngine.Collider)
extern void ObiCollider_set_sourceCollider_mB6E0C4C02CFDB78A515BCFD65B36AC736CECCCBD (void);
// 0x000003A9 UnityEngine.Collider Obi.ObiCollider::get_sourceCollider()
extern void ObiCollider_get_sourceCollider_mE7A6AFE352693E039E4A2193E85A89DFAA477D7E (void);
// 0x000003AA System.Void Obi.ObiCollider::set_distanceField(Obi.ObiDistanceField)
extern void ObiCollider_set_distanceField_m9F40D8CBE94B9CF4D7EC3C03ACBF4E64BA18F116 (void);
// 0x000003AB Obi.ObiDistanceField Obi.ObiCollider::get_distanceField()
extern void ObiCollider_get_distanceField_m5B1AAC65676531B209F9AEE01A7AB88B8279AADE (void);
// 0x000003AC System.Void Obi.ObiCollider::CreateTracker()
extern void ObiCollider_CreateTracker_mB6CE9368E416DE23BF14AFA8940D5FAB5F6C8851 (void);
// 0x000003AD UnityEngine.Component Obi.ObiCollider::GetUnityCollider(System.Boolean&)
extern void ObiCollider_GetUnityCollider_mCBAEB65DB24CAD8A6307DF33E3B1478EC9289627 (void);
// 0x000003AE System.Void Obi.ObiCollider::FindSourceCollider()
extern void ObiCollider_FindSourceCollider_m9EA19CA58432DEA55023A603332EBE1D7B38A7A4 (void);
// 0x000003AF System.Void Obi.ObiCollider::.ctor()
extern void ObiCollider__ctor_mA7A19D3C50B8FEF3314DF23590E0960A7B51BABC (void);
// 0x000003B0 System.Void Obi.ObiCollider2D::set_SourceCollider(UnityEngine.Collider2D)
extern void ObiCollider2D_set_SourceCollider_mB7D2E58B7533B92207B297E43D4558226A350051 (void);
// 0x000003B1 UnityEngine.Collider2D Obi.ObiCollider2D::get_SourceCollider()
extern void ObiCollider2D_get_SourceCollider_mFFEEF7B7C189C2DB7F7EAFA845940F272E60141C (void);
// 0x000003B2 System.Void Obi.ObiCollider2D::CreateTracker()
extern void ObiCollider2D_CreateTracker_m7C18F60D923D099896AA79AF0CD0B8377E17B326 (void);
// 0x000003B3 UnityEngine.Component Obi.ObiCollider2D::GetUnityCollider(System.Boolean&)
extern void ObiCollider2D_GetUnityCollider_mF64F76245333A4C93F5EE038EAEE37E9993CFD77 (void);
// 0x000003B4 System.Void Obi.ObiCollider2D::FindSourceCollider()
extern void ObiCollider2D_FindSourceCollider_m8CFB1A9581B3FB148BCF0D4646413EBD22B0927D (void);
// 0x000003B5 System.Void Obi.ObiCollider2D::.ctor()
extern void ObiCollider2D__ctor_mC0D8A72F2DC693E755D6EB44CB6ADF22545CAD7A (void);
// 0x000003B6 System.Void Obi.ObiColliderBase::set_CollisionMaterial(Obi.ObiCollisionMaterial)
extern void ObiColliderBase_set_CollisionMaterial_mB448948B824EA9DBB3A06893CF9C63807BAFD896 (void);
// 0x000003B7 Obi.ObiCollisionMaterial Obi.ObiColliderBase::get_CollisionMaterial()
extern void ObiColliderBase_get_CollisionMaterial_m5A023C1F180D1BE92233EFE9979F8130F481CF34 (void);
// 0x000003B8 System.Void Obi.ObiColliderBase::set_Filter(System.Int32)
extern void ObiColliderBase_set_Filter_m8079D68C0979B0E1EDC41BC0B5C7FB83D3419CF3 (void);
// 0x000003B9 System.Int32 Obi.ObiColliderBase::get_Filter()
extern void ObiColliderBase_get_Filter_m9DDD807986F545F75BCE481F647CAD927CD24E7B (void);
// 0x000003BA System.Void Obi.ObiColliderBase::set_Thickness(System.Single)
extern void ObiColliderBase_set_Thickness_mC71CB40F6FD36ADAC380E0DCBBAAB927517B6815 (void);
// 0x000003BB System.Single Obi.ObiColliderBase::get_Thickness()
extern void ObiColliderBase_get_Thickness_mE2D9C1A79A127C7DA4F463B1A797EEEEA4061ABE (void);
// 0x000003BC Obi.ObiShapeTracker Obi.ObiColliderBase::get_Tracker()
extern void ObiColliderBase_get_Tracker_m417E63972B37E4BFBB287B2145D22936C835FEEC (void);
// 0x000003BD Obi.ObiColliderHandle Obi.ObiColliderBase::get_Handle()
extern void ObiColliderBase_get_Handle_m0FF935FECD67E0572FD33B7A263B4D6BBD6CD55B (void);
// 0x000003BE System.IntPtr Obi.ObiColliderBase::get_OniCollider()
extern void ObiColliderBase_get_OniCollider_m3EE3917FF4C2E9AB8484FFFAA94DE99AB24ED1E9 (void);
// 0x000003BF Obi.ObiRigidbodyBase Obi.ObiColliderBase::get_Rigidbody()
extern void ObiColliderBase_get_Rigidbody_m3259D0421BBC02338A3674ECDB56ED3326E39AD5 (void);
// 0x000003C0 System.Void Obi.ObiColliderBase::CreateTracker()
// 0x000003C1 UnityEngine.Component Obi.ObiColliderBase::GetUnityCollider(System.Boolean&)
// 0x000003C2 System.Void Obi.ObiColliderBase::FindSourceCollider()
// 0x000003C3 System.Void Obi.ObiColliderBase::CreateRigidbody()
extern void ObiColliderBase_CreateRigidbody_mDD5A737E895830ACED373DCF669C9B77D7972760 (void);
// 0x000003C4 System.Void Obi.ObiColliderBase::OnTransformParentChanged()
extern void ObiColliderBase_OnTransformParentChanged_m2A45BC7EAAB8EB8FDD52BD73BD293DBD90FC1535 (void);
// 0x000003C5 System.Void Obi.ObiColliderBase::AddCollider()
extern void ObiColliderBase_AddCollider_m87D30B63CDCD12DCFA5B99726C40AAE9E183B3FF (void);
// 0x000003C6 System.Void Obi.ObiColliderBase::RemoveCollider()
extern void ObiColliderBase_RemoveCollider_m85CED7A46AE82E36B0A57AC4D966CB0F81A0964C (void);
// 0x000003C7 System.Void Obi.ObiColliderBase::UpdateIfNeeded()
extern void ObiColliderBase_UpdateIfNeeded_mC6C31FE4ACDD3FF7ADDC7522DDF89E87449CA699 (void);
// 0x000003C8 System.Void Obi.ObiColliderBase::OnEnable()
extern void ObiColliderBase_OnEnable_mB8C6F0690630D0EF5A6ECBAB6831420C039A1407 (void);
// 0x000003C9 System.Void Obi.ObiColliderBase::OnDisable()
extern void ObiColliderBase_OnDisable_m70147B944C32560377A84346CA8AE096DEFEDCD0 (void);
// 0x000003CA System.Void Obi.ObiColliderBase::.ctor()
extern void ObiColliderBase__ctor_m2EA5331A79C272EB6F66DCAD84236E4D2A973F4D (void);
// 0x000003CB System.Boolean Obi.ObiResourceHandle`1::get_isValid()
// 0x000003CC System.Void Obi.ObiResourceHandle`1::Invalidate()
// 0x000003CD System.Void Obi.ObiResourceHandle`1::Reference()
// 0x000003CE System.Boolean Obi.ObiResourceHandle`1::Dereference()
// 0x000003CF System.Void Obi.ObiResourceHandle`1::.ctor(System.Int32)
// 0x000003D0 System.Void Obi.ObiColliderHandle::.ctor(System.Int32)
extern void ObiColliderHandle__ctor_m17783224C43CE7F08E87C29A8008D9B84237E88F (void);
// 0x000003D1 System.Void Obi.ObiCollisionMaterialHandle::.ctor(System.Int32)
extern void ObiCollisionMaterialHandle__ctor_m9E87A9AFA47FC9170EF75424886914F685C752AB (void);
// 0x000003D2 System.Void Obi.ObiRigidbodyHandle::.ctor(System.Int32)
extern void ObiRigidbodyHandle__ctor_m070BDDB71AB612702373D37CC0286BCBFC13B958 (void);
// 0x000003D3 Obi.ObiColliderWorld Obi.ObiColliderWorld::GetInstance()
extern void ObiColliderWorld_GetInstance_mCD5E33B37AD66058E1F0A61B7CDAD98CFC13896B (void);
// 0x000003D4 System.Void Obi.ObiColliderWorld::Initialize()
extern void ObiColliderWorld_Initialize_m1622C98EC99C31AC52BFA11404D9A4E6341FA168 (void);
// 0x000003D5 System.Void Obi.ObiColliderWorld::Destroy()
extern void ObiColliderWorld_Destroy_m4CB7C99F9039703E1C97CFE18C4E9D03CD8CC7D0 (void);
// 0x000003D6 System.Void Obi.ObiColliderWorld::DestroyIfUnused()
extern void ObiColliderWorld_DestroyIfUnused_m451242730F7B0F766F09EF65DDF77C39F18032AB (void);
// 0x000003D7 System.Void Obi.ObiColliderWorld::RegisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_RegisterImplementation_mF4B7F94F3C9C0DBAD95A575C3C9158F7F5C30847 (void);
// 0x000003D8 System.Void Obi.ObiColliderWorld::UnregisterImplementation(Obi.IColliderWorldImpl)
extern void ObiColliderWorld_UnregisterImplementation_m0982CC62EC1588205C9B4F5473F8D30CF8C9AE6B (void);
// 0x000003D9 Obi.ObiColliderHandle Obi.ObiColliderWorld::CreateCollider()
extern void ObiColliderWorld_CreateCollider_m963AE24C299ECEB00697F39F517C045D28B42352 (void);
// 0x000003DA Obi.ObiRigidbodyHandle Obi.ObiColliderWorld::CreateRigidbody()
extern void ObiColliderWorld_CreateRigidbody_m30D27D11F8AC419B9DA2716357E9C22F68E4CF21 (void);
// 0x000003DB Obi.ObiCollisionMaterialHandle Obi.ObiColliderWorld::CreateCollisionMaterial()
extern void ObiColliderWorld_CreateCollisionMaterial_mFDEF23E92D27AA612EAE0878A4856D0D5CF44B0E (void);
// 0x000003DC Obi.ObiTriangleMeshHandle Obi.ObiColliderWorld::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiColliderWorld_GetOrCreateTriangleMesh_mAE3E43A75C4023F4D8DE9ADA5194B98EC0332337 (void);
// 0x000003DD System.Void Obi.ObiColliderWorld::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiColliderWorld_DestroyTriangleMesh_m9EA39BD3167CC3ABD3B04AA47DC7E369E9A93DEF (void);
// 0x000003DE Obi.ObiEdgeMeshHandle Obi.ObiColliderWorld::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiColliderWorld_GetOrCreateEdgeMesh_m63794D698A5B13DEF65533803BEAF7B31E12B070 (void);
// 0x000003DF System.Void Obi.ObiColliderWorld::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiColliderWorld_DestroyEdgeMesh_m1489982CEFFF55EC6E86B89632F9C2186915180C (void);
// 0x000003E0 Obi.ObiDistanceFieldHandle Obi.ObiColliderWorld::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiColliderWorld_GetOrCreateDistanceField_mF0D24E094B57DC3B6D41D8FA8CAA50F6EC092E2D (void);
// 0x000003E1 System.Void Obi.ObiColliderWorld::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiColliderWorld_DestroyDistanceField_mB6EBF7DC9BF9BA21B8E3D1C338D24CC0B2CC2151 (void);
// 0x000003E2 Obi.ObiHeightFieldHandle Obi.ObiColliderWorld::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiColliderWorld_GetOrCreateHeightField_m885D1297E85C101D8A258C4A5FEAEBE5D314CDE4 (void);
// 0x000003E3 System.Void Obi.ObiColliderWorld::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiColliderWorld_DestroyHeightField_mBDFA8AC5F63970485468C0B9CC355477CE28920E (void);
// 0x000003E4 System.Void Obi.ObiColliderWorld::DestroyCollider(Obi.ObiColliderHandle)
extern void ObiColliderWorld_DestroyCollider_m1C78622703C9D5D9F921C2BBF1CDF52FEC09D648 (void);
// 0x000003E5 System.Void Obi.ObiColliderWorld::DestroyRigidbody(Obi.ObiRigidbodyHandle)
extern void ObiColliderWorld_DestroyRigidbody_mE027D141E82F35FC032E341CA060DC356F5AE85C (void);
// 0x000003E6 System.Void Obi.ObiColliderWorld::DestroyCollisionMaterial(Obi.ObiCollisionMaterialHandle)
extern void ObiColliderWorld_DestroyCollisionMaterial_m80E9D5CE744153FE3A1B78C3CC336CA1D18CFDF2 (void);
// 0x000003E7 System.Void Obi.ObiColliderWorld::UpdateColliders()
extern void ObiColliderWorld_UpdateColliders_mE22BD1C71C6CE311AABEB3EBE5D5D89499040594 (void);
// 0x000003E8 System.Void Obi.ObiColliderWorld::UpdateRigidbodies(System.Collections.Generic.List`1<Obi.ObiSolver>,System.Single)
extern void ObiColliderWorld_UpdateRigidbodies_m879FDF5DF7AC2C45A626701DC6C4476A7FCBB2C4 (void);
// 0x000003E9 System.Void Obi.ObiColliderWorld::UpdateWorld(System.Single)
extern void ObiColliderWorld_UpdateWorld_m0A22F19C866A4E18E5FE92590F12C269D9B5F6FC (void);
// 0x000003EA System.Void Obi.ObiColliderWorld::UpdateRigidbodyVelocities(System.Collections.Generic.List`1<Obi.ObiSolver>)
extern void ObiColliderWorld_UpdateRigidbodyVelocities_m8EB632E1D181278AB86439D2735C4C1EDA2AD58E (void);
// 0x000003EB System.Void Obi.ObiColliderWorld::.ctor()
extern void ObiColliderWorld__ctor_m56C3C728C87F653030D89BFB4712AE3A2004838B (void);
// 0x000003EC Obi.ObiCollisionMaterialHandle Obi.ObiCollisionMaterial::get_handle()
extern void ObiCollisionMaterial_get_handle_m438ED1A35E14A7DFB14C7837753BC8A264222FD7 (void);
// 0x000003ED System.Void Obi.ObiCollisionMaterial::OnEnable()
extern void ObiCollisionMaterial_OnEnable_m360351035E55863B4483333BAD71D081E7D81ADE (void);
// 0x000003EE System.Void Obi.ObiCollisionMaterial::OnDisable()
extern void ObiCollisionMaterial_OnDisable_m2E9697F842115512DB5B61E12D6C37E4EAE973B1 (void);
// 0x000003EF System.Void Obi.ObiCollisionMaterial::OnValidate()
extern void ObiCollisionMaterial_OnValidate_mC643AB6D6EF7EE34A3564F5F8B5A5819E4818B08 (void);
// 0x000003F0 System.Void Obi.ObiCollisionMaterial::UpdateMaterial()
extern void ObiCollisionMaterial_UpdateMaterial_m9BD91C9E69976192F9188E6C5D7118F013C511CD (void);
// 0x000003F1 System.Void Obi.ObiCollisionMaterial::CreateMaterialIfNeeded()
extern void ObiCollisionMaterial_CreateMaterialIfNeeded_m26C56B4E0DEA4EE4FF06556BC0084F9EA68A512E (void);
// 0x000003F2 System.Void Obi.ObiCollisionMaterial::.ctor()
extern void ObiCollisionMaterial__ctor_m1C64ACA96EB44DE0F823DB8B910A82BCDC1A92FF (void);
// 0x000003F3 System.Boolean Obi.ObiDistanceField::get_Initialized()
extern void ObiDistanceField_get_Initialized_m51FA1E219A856ED9DF246BCD9951CC1948605DA3 (void);
// 0x000003F4 UnityEngine.Bounds Obi.ObiDistanceField::get_FieldBounds()
extern void ObiDistanceField_get_FieldBounds_m4BC90C7A8CF1B7CA63BB9C99FC7F6093C0D29B3E (void);
// 0x000003F5 System.Single Obi.ObiDistanceField::get_EffectiveSampleSize()
extern void ObiDistanceField_get_EffectiveSampleSize_m37382132C97730D8B5824BA8A84B4510DEDBF958 (void);
// 0x000003F6 System.Void Obi.ObiDistanceField::set_InputMesh(UnityEngine.Mesh)
extern void ObiDistanceField_set_InputMesh_mD156CA7F4C12480B991AB72C9F0218EE9022F0ED (void);
// 0x000003F7 UnityEngine.Mesh Obi.ObiDistanceField::get_InputMesh()
extern void ObiDistanceField_get_InputMesh_m2C0F67AF519997D7CBD4E8626636DB1DCABB728C (void);
// 0x000003F8 System.Void Obi.ObiDistanceField::Reset()
extern void ObiDistanceField_Reset_m14516F7F128BCD2234B72205927D9A7EDFA42DB8 (void);
// 0x000003F9 System.Collections.IEnumerator Obi.ObiDistanceField::Generate()
extern void ObiDistanceField_Generate_m5BBC55C56A02B95B313221B58F95004033901A71 (void);
// 0x000003FA UnityEngine.Texture3D Obi.ObiDistanceField::GetVolumeTexture(System.Int32)
extern void ObiDistanceField_GetVolumeTexture_m36F78A8377730BFAEDCAFF68B750F005AC197D67 (void);
// 0x000003FB System.Void Obi.ObiDistanceField::.ctor()
extern void ObiDistanceField__ctor_m188C8102DB79494D3F9FE0F286652323A6FB6B0A (void);
// 0x000003FC System.Void Obi.ObiDistanceField/<Generate>d__16::.ctor(System.Int32)
extern void U3CGenerateU3Ed__16__ctor_mC63A126BE81683635ACE94161C2093665F30E781 (void);
// 0x000003FD System.Void Obi.ObiDistanceField/<Generate>d__16::System.IDisposable.Dispose()
extern void U3CGenerateU3Ed__16_System_IDisposable_Dispose_mFE056A6ADA7A7B1DF5166A9BBCBA238839740669 (void);
// 0x000003FE System.Boolean Obi.ObiDistanceField/<Generate>d__16::MoveNext()
extern void U3CGenerateU3Ed__16_MoveNext_m9B9BA3E313245C4EF7468A5498E71CD80110FD0F (void);
// 0x000003FF System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7BA86A13141744082109A0FE7E962CB2B6ECE44 (void);
// 0x00000400 System.Void Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.Reset()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m78EBB3B25EFC2EBCFB06421B5A63B8AFC2EE16E8 (void);
// 0x00000401 System.Object Obi.ObiDistanceField/<Generate>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mA78DA3FA3FABDAD4670B5666DD18AEEF89278B5B (void);
// 0x00000402 System.Void Obi.ObiDistanceFieldHandle::.ctor(Obi.ObiDistanceField,System.Int32)
extern void ObiDistanceFieldHandle__ctor_m8142EF48648C875A3C61BD530D34FDC7F53D123C (void);
// 0x00000403 System.Void Obi.DistanceFieldHeader::.ctor(System.Int32,System.Int32)
extern void DistanceFieldHeader__ctor_m251B2C29894323C5CEED156849B443EA8067C679 (void);
// 0x00000404 System.Void Obi.ObiDistanceFieldContainer::.ctor()
extern void ObiDistanceFieldContainer__ctor_mF7C40EEFA07738812C54174ACB370AA3CC0AF125 (void);
// 0x00000405 Obi.ObiDistanceFieldHandle Obi.ObiDistanceFieldContainer::GetOrCreateDistanceField(Obi.ObiDistanceField)
extern void ObiDistanceFieldContainer_GetOrCreateDistanceField_m1375B8E8147209EC9172CAC53AC13876980B0C9E (void);
// 0x00000406 System.Void Obi.ObiDistanceFieldContainer::DestroyDistanceField(Obi.ObiDistanceFieldHandle)
extern void ObiDistanceFieldContainer_DestroyDistanceField_mACE5BE92EF022F795071C3BA7A9B07FCFB7A4513 (void);
// 0x00000407 System.Void Obi.ObiDistanceFieldContainer::Dispose()
extern void ObiDistanceFieldContainer_Dispose_m24F3E963532F4E2929E09ACD6BE70C93DED0E4A7 (void);
// 0x00000408 System.Void Obi.Edge::.ctor(System.Int32,System.Int32,UnityEngine.Vector2,UnityEngine.Vector2)
extern void Edge__ctor_m77B0841455A2B88452B465EFBC8FFEEC0A67E648 (void);
// 0x00000409 Obi.Aabb Obi.Edge::GetBounds()
extern void Edge_GetBounds_m644FB98AE4FEA8DC1BCEEFF06588BEEBF7F7261C (void);
// 0x0000040A System.Void Obi.ObiEdgeMeshHandle::.ctor(UnityEngine.EdgeCollider2D,System.Int32)
extern void ObiEdgeMeshHandle__ctor_m3540F335B471820DA6FDEE0DB8F18C59818C13E3 (void);
// 0x0000040B System.Void Obi.EdgeMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void EdgeMeshHeader__ctor_m877A47F9943627D383EFDCEE3A19AE256A9B55A2 (void);
// 0x0000040C System.Void Obi.ObiEdgeMeshContainer::.ctor()
extern void ObiEdgeMeshContainer__ctor_mF79F760DC56967863D567A80D6AE3D1EF932377A (void);
// 0x0000040D Obi.ObiEdgeMeshHandle Obi.ObiEdgeMeshContainer::GetOrCreateEdgeMesh(UnityEngine.EdgeCollider2D)
extern void ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m467071EAE6703BDE659CBD0A9D2902445B25EC25 (void);
// 0x0000040E System.Void Obi.ObiEdgeMeshContainer::DestroyEdgeMesh(Obi.ObiEdgeMeshHandle)
extern void ObiEdgeMeshContainer_DestroyEdgeMesh_m0B9DF3A0F9643DEDF1E83A849EA8F71245F80460 (void);
// 0x0000040F System.Void Obi.ObiEdgeMeshContainer::Dispose()
extern void ObiEdgeMeshContainer_Dispose_m5F57B4FA53FCDFFF2734E2A3700B0DB3634DFBE8 (void);
// 0x00000410 System.Void Obi.ObiEdgeMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_mB91D7183ED2FF1510CA841B903C11A8884B5AF12 (void);
// 0x00000411 System.Void Obi.ObiEdgeMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m9A67F61F2CACE6809F15597CC0C6B5E4393B32CA (void);
// 0x00000412 Obi.Edge Obi.ObiEdgeMeshContainer/<>c::<GetOrCreateEdgeMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_m5F0AC709621AA79274676593791DE6D2BE3B3F10 (void);
// 0x00000413 System.Void Obi.ObiHeightFieldHandle::.ctor(UnityEngine.TerrainData,System.Int32)
extern void ObiHeightFieldHandle__ctor_m1D23D11A632B7ACE4BA79A5EDA634BE30B8D9064 (void);
// 0x00000414 System.Void Obi.HeightFieldHeader::.ctor(System.Int32,System.Int32)
extern void HeightFieldHeader__ctor_m262BFF2169E5E485BD00BFACB85132FFED62CDDA (void);
// 0x00000415 System.Void Obi.ObiHeightFieldContainer::.ctor()
extern void ObiHeightFieldContainer__ctor_mBAF588A2F72128D62B8DA5E88B0F4BD459CA67F7 (void);
// 0x00000416 Obi.ObiHeightFieldHandle Obi.ObiHeightFieldContainer::GetOrCreateHeightField(UnityEngine.TerrainData)
extern void ObiHeightFieldContainer_GetOrCreateHeightField_m8A84C54CDEA3B9D05CB127B05F3B222B7A3A5517 (void);
// 0x00000417 System.Void Obi.ObiHeightFieldContainer::DestroyHeightField(Obi.ObiHeightFieldHandle)
extern void ObiHeightFieldContainer_DestroyHeightField_mE07C682D4706ED975AFD022D4185DDB9FF7E0F19 (void);
// 0x00000418 System.Void Obi.ObiHeightFieldContainer::Dispose()
extern void ObiHeightFieldContainer_Dispose_mF2DFE76857E3BC440CCA67D730DA733BF6417DE1 (void);
// 0x00000419 System.Void Obi.ObiRigidbody::Awake()
extern void ObiRigidbody_Awake_m2500E98F62ECFB5EAB8FBD4DA3BD7B8BADFC38AF (void);
// 0x0000041A System.Void Obi.ObiRigidbody::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody_UpdateKinematicVelocities_m35263D021F8ABCD1E6D9365099AD62F704F760BE (void);
// 0x0000041B System.Void Obi.ObiRigidbody::UpdateIfNeeded(System.Single)
extern void ObiRigidbody_UpdateIfNeeded_mCCA1526686277BDC8BEF5ABDE78418FEF58FF502 (void);
// 0x0000041C System.Void Obi.ObiRigidbody::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody_UpdateVelocities_m63DF8D11F83CC06ED311E367C2AAF3BF1CEFC765 (void);
// 0x0000041D System.Void Obi.ObiRigidbody::.ctor()
extern void ObiRigidbody__ctor_m31B6718C1536A75766D906B61A2ED338CA2F6DA0 (void);
// 0x0000041E System.Void Obi.ObiRigidbody2D::Awake()
extern void ObiRigidbody2D_Awake_mF371E9238C48582C8CD00102B0A376D19D1DA41C (void);
// 0x0000041F System.Void Obi.ObiRigidbody2D::UpdateKinematicVelocities(System.Single)
extern void ObiRigidbody2D_UpdateKinematicVelocities_m4D4BFB3E9233E2077E20FA5BBAC7013825C7DE2C (void);
// 0x00000420 System.Void Obi.ObiRigidbody2D::UpdateIfNeeded(System.Single)
extern void ObiRigidbody2D_UpdateIfNeeded_mE7950E3FDE20A2F31C580E7E97650C53CFB23B49 (void);
// 0x00000421 System.Void Obi.ObiRigidbody2D::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiRigidbody2D_UpdateVelocities_m074D762E5B5A124E43C3F484599BBFC7E520FABA (void);
// 0x00000422 System.Void Obi.ObiRigidbody2D::.ctor()
extern void ObiRigidbody2D__ctor_m56721BA63D2702EA5238878A7F92EA70AE0043D2 (void);
// 0x00000423 System.Void Obi.ObiRigidbodyBase::Awake()
extern void ObiRigidbodyBase_Awake_mC07E66761C016AB8FDF7AB08D512879662497090 (void);
// 0x00000424 System.Void Obi.ObiRigidbodyBase::OnDestroy()
extern void ObiRigidbodyBase_OnDestroy_m7E9F48EE65481EB05B9ECCC137F7C2614B96BDDD (void);
// 0x00000425 System.Void Obi.ObiRigidbodyBase::UpdateIfNeeded(System.Single)
// 0x00000426 System.Void Obi.ObiRigidbodyBase::UpdateVelocities(UnityEngine.Vector3,UnityEngine.Vector3)
// 0x00000427 System.Void Obi.ObiRigidbodyBase::.ctor()
extern void ObiRigidbodyBase__ctor_mF6C7EADE7F12CAB86D1127BF465CE41DA8A6823A (void);
// 0x00000428 System.Void Obi.Triangle::.ctor(System.Int32,System.Int32,System.Int32,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void Triangle__ctor_m15AEE3A9EEE96FA1698DED44AB98949A1E11CF62 (void);
// 0x00000429 Obi.Aabb Obi.Triangle::GetBounds()
extern void Triangle_GetBounds_m56E0F7268C436A9036CF11094F585C3F1AF0F998 (void);
// 0x0000042A System.Void Obi.ObiTriangleMeshHandle::.ctor(UnityEngine.Mesh,System.Int32)
extern void ObiTriangleMeshHandle__ctor_mA53756E4207412B3797147B9FEAE74EEABEA2B94 (void);
// 0x0000042B System.Void Obi.TriangleMeshHeader::.ctor(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern void TriangleMeshHeader__ctor_m8EF03D0DD1903087DE66A92EC3433EF5C7CF9B91 (void);
// 0x0000042C System.Void Obi.ObiTriangleMeshContainer::.ctor()
extern void ObiTriangleMeshContainer__ctor_mA876587074B4D103DC187C5C58717CC31779D6F5 (void);
// 0x0000042D Obi.ObiTriangleMeshHandle Obi.ObiTriangleMeshContainer::GetOrCreateTriangleMesh(UnityEngine.Mesh)
extern void ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m3C4CE0D177EFFC4DAA15DEC342AAB8A920AA98C0 (void);
// 0x0000042E System.Void Obi.ObiTriangleMeshContainer::DestroyTriangleMesh(Obi.ObiTriangleMeshHandle)
extern void ObiTriangleMeshContainer_DestroyTriangleMesh_m9D3FE0FAA7920CFC9867069366D32D99FD9419E4 (void);
// 0x0000042F System.Void Obi.ObiTriangleMeshContainer::Dispose()
extern void ObiTriangleMeshContainer_Dispose_m1DBC5B421ADAA3FC63B436085ADAE7E58A3E5ABF (void);
// 0x00000430 System.Void Obi.ObiTriangleMeshContainer/<>c::.cctor()
extern void U3CU3Ec__cctor_mE53EE44077253A5B7024F9C66840122C2A6A5F00 (void);
// 0x00000431 System.Void Obi.ObiTriangleMeshContainer/<>c::.ctor()
extern void U3CU3Ec__ctor_m2319BE74D6B1AE87A476A702034B945AFFA77134 (void);
// 0x00000432 Obi.Triangle Obi.ObiTriangleMeshContainer/<>c::<GetOrCreateTriangleMesh>b__6_0(Obi.IBounded)
extern void U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m7C65E4FD87AABC0680E624DDEFA344EC37F00399 (void);
// 0x00000433 System.Collections.IEnumerator Obi.ASDF::Build(System.Single,System.Int32,UnityEngine.Vector3[],System.Int32[],System.Collections.Generic.List`1<Obi.DFNode>,System.Int32)
extern void ASDF_Build_m599FAE6C66EEBD1725DF976C19BE85252B84AAB1 (void);
// 0x00000434 System.Single Obi.ASDF::Sample(System.Collections.Generic.List`1<Obi.DFNode>,UnityEngine.Vector3)
extern void ASDF_Sample_mB51F949A6EBC7CDD272A95D701A03D3C9EAF214B (void);
// 0x00000435 System.Void Obi.ASDF::.ctor()
extern void ASDF__ctor_mEEE94E12CC5E1D95F89C5A5764324DD19C15B430 (void);
// 0x00000436 System.Void Obi.ASDF::.cctor()
extern void ASDF__cctor_m908672B45D21B9190763820B7A92E7A06F7D8C86 (void);
// 0x00000437 System.Void Obi.ASDF/<>c::.cctor()
extern void U3CU3Ec__cctor_mE615EAAC566DCA8C8BF282F8A2426476AD3B933C (void);
// 0x00000438 System.Void Obi.ASDF/<>c::.ctor()
extern void U3CU3Ec__ctor_m6A067ACABF8B8DE4140F3E690C0C2D203F80C085 (void);
// 0x00000439 Obi.Triangle Obi.ASDF/<>c::<Build>b__3_0(Obi.IBounded)
extern void U3CU3Ec_U3CBuildU3Eb__3_0_mB8DADCF561B221B1D942CA05BFC2AAC959193F92 (void);
// 0x0000043A System.Void Obi.ASDF/<Build>d__3::.ctor(System.Int32)
extern void U3CBuildU3Ed__3__ctor_m4016CC8A07B3D07FA4B6170E7F0D91FC5D9477C7 (void);
// 0x0000043B System.Void Obi.ASDF/<Build>d__3::System.IDisposable.Dispose()
extern void U3CBuildU3Ed__3_System_IDisposable_Dispose_m0ED848778C1B1DA878C592D33556DC186F97875F (void);
// 0x0000043C System.Boolean Obi.ASDF/<Build>d__3::MoveNext()
extern void U3CBuildU3Ed__3_MoveNext_m174CB894CC91416CBE97B9F030A5F17A7E2BE964 (void);
// 0x0000043D System.Object Obi.ASDF/<Build>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m718A3D5304A2D96072BC702908CAD7DB869413DB (void);
// 0x0000043E System.Void Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.Reset()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m529D739355FAD14F1C306A09AEAB1C3E80918F21 (void);
// 0x0000043F System.Object Obi.ASDF/<Build>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_mDC66AA1DCDC338CCBEA02F0929A28562BBBEFBB2 (void);
// 0x00000440 System.Void Obi.DFNode::.ctor(UnityEngine.Vector4)
extern void DFNode__ctor_mB94060EAF88B6233E60AB935634F6716E502547E (void);
// 0x00000441 System.Single Obi.DFNode::Sample(UnityEngine.Vector3)
extern void DFNode_Sample_m14246F91889EC3E920F108B2B406FBC4E44CF124 (void);
// 0x00000442 UnityEngine.Vector3 Obi.DFNode::GetNormalizedPos(UnityEngine.Vector3)
extern void DFNode_GetNormalizedPos_m335E725D7443558FD8C47CFA490D1BC29034917E (void);
// 0x00000443 System.Int32 Obi.DFNode::GetOctant(UnityEngine.Vector3)
extern void DFNode_GetOctant_m5808A88E16C5E0EEFF5E1CF2E66040C92961E884 (void);
// 0x00000444 UnityEngine.Vector4 Obi.Aabb::get_center()
extern void Aabb_get_center_m34EBD23AF65E6E8B49FECCE4F4C285610D38F823 (void);
// 0x00000445 UnityEngine.Vector4 Obi.Aabb::get_size()
extern void Aabb_get_size_mC66C740C1B769CEA883531AC10D85CAD0D4E5657 (void);
// 0x00000446 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4,UnityEngine.Vector4)
extern void Aabb__ctor_m4465FBAA62A29E90A0BBEC56EF534EA9582A8AC4 (void);
// 0x00000447 System.Void Obi.Aabb::.ctor(UnityEngine.Vector4)
extern void Aabb__ctor_m59BB63BC91E612A9C3ADDB5106EAE996A0BD4465 (void);
// 0x00000448 System.Void Obi.Aabb::Encapsulate(UnityEngine.Vector4)
extern void Aabb_Encapsulate_mAEA769FBEDA585C1186F2D1CB4BD06A880EB8209 (void);
// 0x00000449 System.Void Obi.Aabb::Encapsulate(Obi.Aabb)
extern void Aabb_Encapsulate_m832BC3BAF98AD3CE9B8F36C191777681E6F8FD6A (void);
// 0x0000044A System.Void Obi.Aabb::FromBounds(UnityEngine.Bounds,System.Single,System.Boolean)
extern void Aabb_FromBounds_m27239365E8E05E7F09B43FD655D220D115610763 (void);
// 0x0000044B System.Void Obi.AffineTransform::.ctor(UnityEngine.Vector4,UnityEngine.Quaternion,UnityEngine.Vector4)
extern void AffineTransform__ctor_mCA54E543BC65B018A5C94C2CE89E325ECA1BB6DC (void);
// 0x0000044C System.Void Obi.AffineTransform::FromTransform(UnityEngine.Transform,System.Boolean)
extern void AffineTransform_FromTransform_m82766F8868CBFE2E7E74A50669374B8987C8BAD9 (void);
// 0x0000044D Obi.BIHNode[] Obi.BIH::Build(Obi.IBounded[]&,System.Int32,System.Single)
extern void BIH_Build_m5055AB9B932CC1969508C1B60B55F02612CA1E4F (void);
// 0x0000044E System.Int32 Obi.BIH::HoarePartition(Obi.IBounded[],System.Int32,System.Int32,System.Single,Obi.BIHNode&,System.Int32)
extern void BIH_HoarePartition_mDD8939FC005C1FE69F393A0EEB98B7C3BB34521D (void);
// 0x0000044F System.Single Obi.BIH::DistanceToSurface(Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_m9BE974D87EC72703A9FF6273F4BF3F8D56769889 (void);
// 0x00000450 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_m2D97F2A06A1887C561BB87392DDD40ED1EC69033 (void);
// 0x00000451 System.Single Obi.BIH::DistanceToSurface(Obi.BIHNode[],Obi.Triangle[],UnityEngine.Vector3[],UnityEngine.Vector3[],Obi.BIHNode&,UnityEngine.Vector3&)
extern void BIH_DistanceToSurface_mA5C9FE2858F60370476EDC20BD72DC1B91432FB5 (void);
// 0x00000452 System.Void Obi.BIH::.ctor()
extern void BIH__ctor_mAB45D6F1B6D5CB3FA62A019FEE91C03E1B09255B (void);
// 0x00000453 System.Single Obi.BIH::<DistanceToSurface>g__MinSignedDistance|4_0(System.Single,System.Single)
extern void BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_m9A0320882A54C615D81A3CB5E680A67D1D0B35B6 (void);
// 0x00000454 System.Void Obi.BIHNode::.ctor(System.Int32,System.Int32)
extern void BIHNode__ctor_m311091C6FC6855D66C9272ACBEC22AFEEF02745A (void);
// 0x00000455 Obi.Aabb Obi.IBounded::GetBounds()
// 0x00000456 System.Void Obi.CellSpan::.ctor(Obi.VInt4,Obi.VInt4)
extern void CellSpan__ctor_mBAA579B4157A8ECC1A3AF1945366566B354D74E1 (void);
// 0x00000457 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m8950C5EDEC5F5FCB2450A939D11E1B7F8F51CA51 (void);
// 0x00000458 System.Void Obi.ColliderRigidbody::FromRigidbody(UnityEngine.Rigidbody2D,System.Boolean)
extern void ColliderRigidbody_FromRigidbody_m634E6B8C72367A4FE8781687FDA571001810F64B (void);
// 0x00000459 System.Void Obi.CollisionMaterial::FromObiCollisionMaterial(Obi.ObiCollisionMaterial)
extern void CollisionMaterial_FromObiCollisionMaterial_m57CF8FE723A8624F55070A837BB0ABEF6A196750 (void);
// 0x0000045A System.Void Obi.ObiNativeAabbList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAabbList__ctor_mE40545EA5EFFF80CA76DAAF3D3571267F9EE89F9 (void);
// 0x0000045B System.Void Obi.ObiNativeAffineTransformList::.ctor(System.Int32,System.Int32)
extern void ObiNativeAffineTransformList__ctor_mA8B296672E8B0C15E7BFB40F7381B9613623F4BB (void);
// 0x0000045C System.Void Obi.ObiNativeBIHNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeBIHNodeList__ctor_m430D2EB3BAA8014894A0F18122946D3C85C73509 (void);
// 0x0000045D System.Void Obi.ObiNativeCellSpanList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCellSpanList__ctor_mB716A81D32E22A0F55B6088B67A82EFBE1BE307E (void);
// 0x0000045E System.Void Obi.ObiNativeColliderShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeColliderShapeList__ctor_mE2DC8FC7669A1C8BD97FDC6FD9DED79524D1F533 (void);
// 0x0000045F System.Void Obi.ObiNativeCollisionMaterialList::.ctor(System.Int32,System.Int32)
extern void ObiNativeCollisionMaterialList__ctor_m358B6ECD8E212C156724B2D23371158C6C279316 (void);
// 0x00000460 System.Void Obi.ObiNativeContactShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeContactShapeList__ctor_m513DC1E745FB1ADBE279FD5807DE6EDD6DCE9A50 (void);
// 0x00000461 System.Void Obi.ObiNativeDFNodeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDFNodeList__ctor_m87B93C4B0D173C7DE21A5E337D9538C1AB6D6022 (void);
// 0x00000462 System.Void Obi.ObiNativeDistanceFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeDistanceFieldHeaderList__ctor_m7068242C8C2A4311C6E7BB10EDDB4076C985BEE6 (void);
// 0x00000463 System.Void Obi.ObiNativeEdgeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeList__ctor_m871E11B58D2BCB410CEA186C77C8B728AB09FB71 (void);
// 0x00000464 System.Void Obi.ObiNativeEdgeMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeEdgeMeshHeaderList__ctor_m495EB672AB7FBD542D61549A6DB8CD21DC474216 (void);
// 0x00000465 System.Void Obi.ObiNativeFloatList::.ctor(System.Int32,System.Int32)
extern void ObiNativeFloatList__ctor_m084E2652786A6504303E0DCC85A406961F7AAF79 (void);
// 0x00000466 System.Void Obi.ObiNativeHeightFieldHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeHeightFieldHeaderList__ctor_mF46A488EFD6B00C4EBD9AAD9676B4AB1FF06BD8F (void);
// 0x00000467 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeInt4List__ctor_m65C32CF777FA034666EFC366B12EA10C45F2F759 (void);
// 0x00000468 System.Void Obi.ObiNativeInt4List::.ctor(System.Int32,System.Int32,Obi.VInt4)
extern void ObiNativeInt4List__ctor_m4995E584FAE3543E4F5A4EF1C53759C19545A1BC (void);
// 0x00000469 System.Void Obi.ObiNativeIntList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntList__ctor_m1EEAF238780D9B1B982D5E6B3BEF28F7395D5D3A (void);
// 0x0000046A System.Void Obi.ObiNativeIntPtrList::.ctor(System.Int32,System.Int32)
extern void ObiNativeIntPtrList__ctor_m8BDDFDE672234C458B67EBB6E0E752C1476BED6F (void);
// 0x0000046B System.Void Obi.ObiNativeList`1::set_count(System.Int32)
// 0x0000046C System.Int32 Obi.ObiNativeList`1::get_count()
// 0x0000046D System.Void Obi.ObiNativeList`1::set_capacity(System.Int32)
// 0x0000046E System.Int32 Obi.ObiNativeList`1::get_capacity()
// 0x0000046F System.Boolean Obi.ObiNativeList`1::get_isCreated()
// 0x00000470 T Obi.ObiNativeList`1::get_Item(System.Int32)
// 0x00000471 System.Void Obi.ObiNativeList`1::set_Item(System.Int32,T)
// 0x00000472 System.Void Obi.ObiNativeList`1::.ctor(System.Int32,System.Int32)
// 0x00000473 System.Void Obi.ObiNativeList`1::Finalize()
// 0x00000474 System.Void Obi.ObiNativeList`1::Dispose(System.Boolean)
// 0x00000475 System.Void Obi.ObiNativeList`1::Dispose()
// 0x00000476 System.Void Obi.ObiNativeList`1::OnBeforeSerialize()
// 0x00000477 System.Void Obi.ObiNativeList`1::OnAfterDeserialize()
// 0x00000478 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray()
// 0x00000479 Unity.Collections.NativeArray`1<U> Obi.ObiNativeList`1::AsNativeArray(System.Int32)
// 0x0000047A UnityEngine.ComputeBuffer Obi.ObiNativeList`1::AsComputeBuffer()
// 0x0000047B System.Void Obi.ObiNativeList`1::ChangeCapacity(System.Int32,System.Int32)
// 0x0000047C System.Boolean Obi.ObiNativeList`1::Compare(Obi.ObiNativeList`1<T>)
// 0x0000047D System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>)
// 0x0000047E System.Void Obi.ObiNativeList`1::CopyFrom(Obi.ObiNativeList`1<T>,System.Int32,System.Int32,System.Int32)
// 0x0000047F System.Void Obi.ObiNativeList`1::CopyReplicate(T,System.Int32,System.Int32)
// 0x00000480 System.Void Obi.ObiNativeList`1::CopyTo(T[],System.Int32,System.Int32)
// 0x00000481 System.Void Obi.ObiNativeList`1::Clear()
// 0x00000482 System.Void Obi.ObiNativeList`1::Add(T)
// 0x00000483 System.Void Obi.ObiNativeList`1::AddRange(System.Collections.Generic.IEnumerable`1<T>)
// 0x00000484 System.Void Obi.ObiNativeList`1::RemoveRange(System.Int32,System.Int32)
// 0x00000485 System.Void Obi.ObiNativeList`1::RemoveAt(System.Int32)
// 0x00000486 System.Boolean Obi.ObiNativeList`1::ResizeUninitialized(System.Int32)
// 0x00000487 System.Boolean Obi.ObiNativeList`1::ResizeInitialized(System.Int32,T)
// 0x00000488 System.Boolean Obi.ObiNativeList`1::EnsureCapacity(System.Int32)
// 0x00000489 System.Void Obi.ObiNativeList`1::WipeToZero()
// 0x0000048A System.String Obi.ObiNativeList`1::ToString()
// 0x0000048B System.Void* Obi.ObiNativeList`1::AddressOfElement(System.Int32)
// 0x0000048C System.IntPtr Obi.ObiNativeList`1::GetIntPtr()
// 0x0000048D System.Void Obi.ObiNativeList`1::Swap(System.Int32,System.Int32)
// 0x0000048E System.Collections.Generic.IEnumerator`1<T> Obi.ObiNativeList`1::GetEnumerator()
// 0x0000048F System.Collections.IEnumerator Obi.ObiNativeList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x00000490 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__46::.ctor(System.Int32)
// 0x00000491 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__46::System.IDisposable.Dispose()
// 0x00000492 System.Boolean Obi.ObiNativeList`1/<GetEnumerator>d__46::MoveNext()
// 0x00000493 T Obi.ObiNativeList`1/<GetEnumerator>d__46::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x00000494 System.Void Obi.ObiNativeList`1/<GetEnumerator>d__46::System.Collections.IEnumerator.Reset()
// 0x00000495 System.Object Obi.ObiNativeList`1/<GetEnumerator>d__46::System.Collections.IEnumerator.get_Current()
// 0x00000496 System.Void Obi.ObiNativeMatrix4x4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeMatrix4x4List__ctor_m28F259B479F3B9E6F07ADB7914EEBDEFAD7490D9 (void);
// 0x00000497 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQuaternionList__ctor_mEB7CF253CBE003F21042E1CB01294E88EB8C12CC (void);
// 0x00000498 System.Void Obi.ObiNativeQuaternionList::.ctor(System.Int32,System.Int32,UnityEngine.Quaternion)
extern void ObiNativeQuaternionList__ctor_m9E3FA5D8D6E2731ABE98E70F678A8D4230C9EC20 (void);
// 0x00000499 System.Void Obi.ObiNativeQueryResultList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryResultList__ctor_m0897E63A9D46652B53DD4D6B7092312182C9B35F (void);
// 0x0000049A System.Void Obi.ObiNativeQueryShapeList::.ctor(System.Int32,System.Int32)
extern void ObiNativeQueryShapeList__ctor_m00DAD9B29DDD69C6463C76B0341C690EFC689191 (void);
// 0x0000049B System.Void Obi.ObiNativeRigidbodyList::.ctor(System.Int32,System.Int32)
extern void ObiNativeRigidbodyList__ctor_mE735CB74C4A310ECCB3AA2C7BBBB2555D0E160D6 (void);
// 0x0000049C System.Void Obi.ObiNativeTriangleList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleList__ctor_m5688D7FB37A7FDC199F3874A3C77DBE36B72D599 (void);
// 0x0000049D System.Void Obi.ObiNativeTriangleMeshHeaderList::.ctor(System.Int32,System.Int32)
extern void ObiNativeTriangleMeshHeaderList__ctor_m7D18947097366047DD11CD50EF5061F0143B52F5 (void);
// 0x0000049E System.Void Obi.ObiNativeVector2List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector2List__ctor_m21C34792CECE609072CF39C13D56C19028D75266 (void);
// 0x0000049F System.Void Obi.ObiNativeVector3List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector3List__ctor_mBC0740F244904D22E754FD2D230A756FDBD4ED15 (void);
// 0x000004A0 System.Void Obi.ObiNativeVector4List::.ctor(System.Int32,System.Int32)
extern void ObiNativeVector4List__ctor_m5D4C8EFE0AFB31C8D76FF9D6BDBFEEE7C9A19A7F (void);
// 0x000004A1 UnityEngine.Vector3 Obi.ObiNativeVector4List::GetVector3(System.Int32)
extern void ObiNativeVector4List_GetVector3_mB077F2EA7AE13E15FD28521E63F39CB1C03392DF (void);
// 0x000004A2 System.Void Obi.ObiNativeVector4List::SetVector3(System.Int32,UnityEngine.Vector3)
extern void ObiNativeVector4List_SetVector3_mB2128317C9A45705A27D05B4F496DAA1B5557409 (void);
// 0x000004A3 System.Collections.Generic.IEnumerator`1<T> Obi.ObiList`1::GetEnumerator()
// 0x000004A4 System.Collections.IEnumerator Obi.ObiList`1::System.Collections.IEnumerable.GetEnumerator()
// 0x000004A5 System.Void Obi.ObiList`1::Add(T)
// 0x000004A6 System.Void Obi.ObiList`1::Clear()
// 0x000004A7 System.Boolean Obi.ObiList`1::Contains(T)
// 0x000004A8 System.Void Obi.ObiList`1::CopyTo(T[],System.Int32)
// 0x000004A9 System.Boolean Obi.ObiList`1::Remove(T)
// 0x000004AA System.Int32 Obi.ObiList`1::get_Count()
// 0x000004AB System.Boolean Obi.ObiList`1::get_IsReadOnly()
// 0x000004AC System.Int32 Obi.ObiList`1::IndexOf(T)
// 0x000004AD System.Void Obi.ObiList`1::Insert(System.Int32,T)
// 0x000004AE System.Void Obi.ObiList`1::RemoveAt(System.Int32)
// 0x000004AF T Obi.ObiList`1::get_Item(System.Int32)
// 0x000004B0 System.Void Obi.ObiList`1::set_Item(System.Int32,T)
// 0x000004B1 T[] Obi.ObiList`1::get_Data()
// 0x000004B2 System.Void Obi.ObiList`1::SetCount(System.Int32)
// 0x000004B3 System.Void Obi.ObiList`1::EnsureCapacity(System.Int32)
// 0x000004B4 System.Void Obi.ObiList`1::.ctor()
// 0x000004B5 System.Void Obi.ObiList`1/<GetEnumerator>d__2::.ctor(System.Int32)
// 0x000004B6 System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.IDisposable.Dispose()
// 0x000004B7 System.Boolean Obi.ObiList`1/<GetEnumerator>d__2::MoveNext()
// 0x000004B8 T Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.Generic.IEnumerator<T>.get_Current()
// 0x000004B9 System.Void Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.Reset()
// 0x000004BA System.Object Obi.ObiList`1/<GetEnumerator>d__2::System.Collections.IEnumerator.get_Current()
// 0x000004BB System.Void Obi.ParticlePair::.ctor(System.Int32,System.Int32)
extern void ParticlePair__ctor_m1ABA5CF889DBC5A410CE57D2A9E6F1F7D7536257 (void);
// 0x000004BC System.Int32 Obi.ParticlePair::get_Item(System.Int32)
extern void ParticlePair_get_Item_m9D26C2D704AA4EE04E5B20ED21AE193CD4A4530A (void);
// 0x000004BD System.Void Obi.ParticlePair::set_Item(System.Int32,System.Int32)
extern void ParticlePair_set_Item_m9AAF33F1EC57D519A5C6267044F2A569B0E2E156 (void);
// 0x000004BE System.Void Obi.QueryShape::.ctor(Obi.QueryShape/QueryType,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Single,System.Int32)
extern void QueryShape__ctor_mAB2B92EF1A8A1E13D04424FC3483D6990E64C959 (void);
// 0x000004BF System.Int32 Obi.SimplexCounts::get_simplexCount()
extern void SimplexCounts_get_simplexCount_m71F95AE072FC89EBD43364AD9F5D99B8A4AB4C3C (void);
// 0x000004C0 System.Void Obi.SimplexCounts::.ctor(System.Int32,System.Int32,System.Int32)
extern void SimplexCounts__ctor_m87B2910E8FA9E6B152BF231CF0B85B8B1BC144AD (void);
// 0x000004C1 System.Int32 Obi.SimplexCounts::GetSimplexStartAndSize(System.Int32,System.Int32&)
extern void SimplexCounts_GetSimplexStartAndSize_m1C63A710053493E24CDBEF324C8A66B27F871641 (void);
// 0x000004C2 System.Void Obi.VInt4::.ctor(System.Int32,System.Int32,System.Int32,System.Int32)
extern void VInt4__ctor_mAB09F0C216D6336E655285359F99CAC21749BD51 (void);
// 0x000004C3 System.Void Obi.VInt4::.ctor(System.Int32)
extern void VInt4__ctor_mBDE8910E0F589EDC34EF21F9A4DDC0D3C1B4551E (void);
// 0x000004C4 UnityEngine.Vector3Int Obi.MeshVoxelizer::get_Origin()
extern void MeshVoxelizer_get_Origin_mD71BC7FD4959DA95E367367BDCF4AC2B1418C7FE (void);
// 0x000004C5 System.Void Obi.MeshVoxelizer::.ctor(UnityEngine.Mesh,System.Single)
extern void MeshVoxelizer__ctor_m98FE768E9523B1EA295179545E63C4C60DB1B0B4 (void);
// 0x000004C6 UnityEngine.Bounds Obi.MeshVoxelizer::GetTriangleBounds(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_GetTriangleBounds_m68E1A2C5887C890FEF553730AF918C67242E0548 (void);
// 0x000004C7 UnityEngine.Vector3Int Obi.MeshVoxelizer::GetPointVoxel(UnityEngine.Vector3)
extern void MeshVoxelizer_GetPointVoxel_m6916482E899ECA9A98C999F42595AF4CFC67BC87 (void);
// 0x000004C8 System.Boolean Obi.MeshVoxelizer::VoxelExists(UnityEngine.Vector3Int)
extern void MeshVoxelizer_VoxelExists_m359D018EAFEF8C1C155B4CFBB0B24DC7E96DEF48 (void);
// 0x000004C9 System.Void Obi.MeshVoxelizer::AppendOverlappingVoxels(UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_AppendOverlappingVoxels_m2F726421AFF148505220F11E52121A4D8F693C21 (void);
// 0x000004CA System.Void Obi.MeshVoxelizer::Voxelize(UnityEngine.Vector3)
extern void MeshVoxelizer_Voxelize_m420C1E5B81A86A0044AB58EF7A066DE30BF01F83 (void);
// 0x000004CB System.Void Obi.MeshVoxelizer::FloodFill()
extern void MeshVoxelizer_FloodFill_mE886DAC7929077EC07AA10A1115C5B6EC0A161D7 (void);
// 0x000004CC System.Boolean Obi.MeshVoxelizer::IsIntersecting(UnityEngine.Bounds,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void MeshVoxelizer_IsIntersecting_mF672978BF03383F3B19AEFA50BAF0F12B7F1842B (void);
// 0x000004CD System.Void Obi.MeshVoxelizer::Project(System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>,UnityEngine.Vector3,System.Double&,System.Double&)
extern void MeshVoxelizer_Project_m2E3D7D935B5BF56A41D0E4160082C6CB6DF62C99 (void);
// 0x000004CE System.Void Obi.VoxelDistanceField::.ctor(Obi.MeshVoxelizer)
extern void VoxelDistanceField__ctor_mCD081DCBC2E4D47C4D58542400087DE4A5833695 (void);
// 0x000004CF System.Boolean Obi.VoxelDistanceField::VoxelExists(UnityEngine.Vector3Int)
extern void VoxelDistanceField_VoxelExists_m91BEDE07CEAD13732C899075999B86F992E4BF54 (void);
// 0x000004D0 System.Void Obi.VoxelDistanceField::JumpFlood()
extern void VoxelDistanceField_JumpFlood_mDF5484ED7C02D14395FD1089D1011F1420ECFD75 (void);
// 0x000004D1 System.Void Obi.VoxelDistanceField::JumpFloodPass(System.Int32,UnityEngine.Vector3Int[0...,0...,0...],UnityEngine.Vector3Int[0...,0...,0...])
extern void VoxelDistanceField_JumpFloodPass_m93C4304B47AF0177E35140AA583AC97546FA8A35 (void);
// 0x000004D2 System.Void Obi.ObiDistanceFieldRenderer::Awake()
extern void ObiDistanceFieldRenderer_Awake_m3E4426F1E774910E865BC75833FF9E613D55DBE6 (void);
// 0x000004D3 System.Void Obi.ObiDistanceFieldRenderer::OnEnable()
extern void ObiDistanceFieldRenderer_OnEnable_mB1D255D6F0A3B18AE7DE6C236343276F6030A442 (void);
// 0x000004D4 System.Void Obi.ObiDistanceFieldRenderer::OnDisable()
extern void ObiDistanceFieldRenderer_OnDisable_m7A0F0988A96F5AEE90512F9732E241F0D85A7A56 (void);
// 0x000004D5 System.Void Obi.ObiDistanceFieldRenderer::Cleanup()
extern void ObiDistanceFieldRenderer_Cleanup_m106CC736B24C9ACF8FB69F069D368BC31A9CD1D0 (void);
// 0x000004D6 System.Void Obi.ObiDistanceFieldRenderer::ResizeTexture()
extern void ObiDistanceFieldRenderer_ResizeTexture_mC5521E1F05E4AF5B98F10CC88C1E20E39859F500 (void);
// 0x000004D7 System.Void Obi.ObiDistanceFieldRenderer::CreatePlaneMesh(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_CreatePlaneMesh_m736BE4B002234C1577D6A02B93E3481B19F9A017 (void);
// 0x000004D8 System.Void Obi.ObiDistanceFieldRenderer::RefreshCutawayTexture(Obi.ObiDistanceField)
extern void ObiDistanceFieldRenderer_RefreshCutawayTexture_mACD4B3F520E069C362B3D1E2A52284E7EC11A02D (void);
// 0x000004D9 System.Void Obi.ObiDistanceFieldRenderer::DrawCutawayPlane(Obi.ObiDistanceField,UnityEngine.Matrix4x4)
extern void ObiDistanceFieldRenderer_DrawCutawayPlane_m9CF65A4FC7CAF54DCCC57AB59D55F6BCC4F322C9 (void);
// 0x000004DA System.Void Obi.ObiDistanceFieldRenderer::OnDrawGizmos()
extern void ObiDistanceFieldRenderer_OnDrawGizmos_m7C4F4718288BF910BCF17DD09AAB6FDA9D544B10 (void);
// 0x000004DB System.Void Obi.ObiDistanceFieldRenderer::.ctor()
extern void ObiDistanceFieldRenderer__ctor_m191C4F3AA8C9AB2EE1037FD40CF6A000D9E05E5F (void);
// 0x000004DC System.Void Obi.ObiInstancedParticleRenderer::OnEnable()
extern void ObiInstancedParticleRenderer_OnEnable_mDC5A6A92920AABB2E5781EED72D375EA23A69F5A (void);
// 0x000004DD System.Void Obi.ObiInstancedParticleRenderer::OnDisable()
extern void ObiInstancedParticleRenderer_OnDisable_mBDCC90E4447ECA01685F85BDCECD0E32E54433DF (void);
// 0x000004DE System.Void Obi.ObiInstancedParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiInstancedParticleRenderer_DrawParticles_m465A516F2A7C36BB1267C92E076C7402498D508E (void);
// 0x000004DF System.Void Obi.ObiInstancedParticleRenderer::.ctor()
extern void ObiInstancedParticleRenderer__ctor_mF3C04E4B127A3068AC43A816933CD2706642DA2D (void);
// 0x000004E0 System.Void Obi.ObiInstancedParticleRenderer::.cctor()
extern void ObiInstancedParticleRenderer__cctor_m21AC0645F738EE4A24E85330E5A4A9BF8CDA5668 (void);
// 0x000004E1 System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ObiParticleRenderer::get_ParticleMeshes()
extern void ObiParticleRenderer_get_ParticleMeshes_m923C2E62304362F78C82CFCCE4EC65162E760E85 (void);
// 0x000004E2 UnityEngine.Material Obi.ObiParticleRenderer::get_ParticleMaterial()
extern void ObiParticleRenderer_get_ParticleMaterial_mC7BED89475A9E3469A2CC58DA36D29A3F9DD57C2 (void);
// 0x000004E3 System.Void Obi.ObiParticleRenderer::OnEnable()
extern void ObiParticleRenderer_OnEnable_mB97E2BDC1615330D8FAAA9BAA48AD4A0C99A4A48 (void);
// 0x000004E4 System.Void Obi.ObiParticleRenderer::OnDisable()
extern void ObiParticleRenderer_OnDisable_mEDED7F7CD25B727F135E73A8ED72C21969BAD494 (void);
// 0x000004E5 System.Void Obi.ObiParticleRenderer::CreateMaterialIfNeeded()
extern void ObiParticleRenderer_CreateMaterialIfNeeded_mA44AC8DFCA92BFE9AE11422167F33B0D7D901C8A (void);
// 0x000004E6 System.Void Obi.ObiParticleRenderer::DrawParticles(Obi.ObiActor)
extern void ObiParticleRenderer_DrawParticles_mFCAAF65EFBA92B57A9D7A4375BBDF9445DE9C6B7 (void);
// 0x000004E7 System.Void Obi.ObiParticleRenderer::DrawParticles()
extern void ObiParticleRenderer_DrawParticles_m856963BC279329E37190C6CF90F63C8E8C92AC8E (void);
// 0x000004E8 System.Void Obi.ObiParticleRenderer::.ctor()
extern void ObiParticleRenderer__ctor_m49B47769D78FB2D77D03F05A8704FE6D708BF304 (void);
// 0x000004E9 System.Void Obi.ObiParticleRenderer::.cctor()
extern void ObiParticleRenderer__cctor_m2E9ED3F12F056C26A171CCCBE28AEA535AA87E1C (void);
// 0x000004EA System.Collections.Generic.IEnumerable`1<UnityEngine.Mesh> Obi.ParticleImpostorRendering::get_Meshes()
extern void ParticleImpostorRendering_get_Meshes_m8D60CAAFF281436EE387DBE3D60FC565B33ABE5E (void);
// 0x000004EB System.Void Obi.ParticleImpostorRendering::Apply(UnityEngine.Mesh)
extern void ParticleImpostorRendering_Apply_mC2D0ACEA779553E33C7B278035CE12772FB302ED (void);
// 0x000004EC System.Void Obi.ParticleImpostorRendering::ClearMeshes()
extern void ParticleImpostorRendering_ClearMeshes_m1AD7671FAF0826450931CB9A632E387989730CEA (void);
// 0x000004ED System.Void Obi.ParticleImpostorRendering::UpdateMeshes(Obi.IObiParticleCollection)
extern void ParticleImpostorRendering_UpdateMeshes_m9140980F5151F0C30AB2FCACDD1BB92153F38292 (void);
// 0x000004EE System.Void Obi.ParticleImpostorRendering::.ctor()
extern void ParticleImpostorRendering__ctor_m5BD794C9146FE8A187E9934E6F381942DF58C71C (void);
// 0x000004EF System.Void Obi.ParticleImpostorRendering::.cctor()
extern void ParticleImpostorRendering__cctor_mFFCA0EE4575E08A0FF122713FE109BBFAC41295D (void);
// 0x000004F0 System.Void Obi.ShadowmapExposer::Awake()
extern void ShadowmapExposer_Awake_m7F124F583C2E1C1069ADF356564DC613D3118E9F (void);
// 0x000004F1 System.Void Obi.ShadowmapExposer::OnEnable()
extern void ShadowmapExposer_OnEnable_m6289A9CF5D1278997659358163B5C00676BDD883 (void);
// 0x000004F2 System.Void Obi.ShadowmapExposer::OnDisable()
extern void ShadowmapExposer_OnDisable_m41264EFB804DB02AFC4B866A3FD564A627328B27 (void);
// 0x000004F3 System.Void Obi.ShadowmapExposer::Cleanup()
extern void ShadowmapExposer_Cleanup_m08B86D9F51C0B21DC5417AE818143FA0DB4C0001 (void);
// 0x000004F4 System.Void Obi.ShadowmapExposer::SetupFluidShadowsCommandBuffer()
extern void ShadowmapExposer_SetupFluidShadowsCommandBuffer_m9722E09E25C549C260E10A9C5659136B0C1CA73B (void);
// 0x000004F5 System.Void Obi.ShadowmapExposer::Update()
extern void ShadowmapExposer_Update_m3A8254DAF98E6E2E432000106C3E533A447A8F1D (void);
// 0x000004F6 System.Void Obi.ShadowmapExposer::.ctor()
extern void ShadowmapExposer__ctor_mAC792FC6B235D517AE0D56D4FD986CEE4552FAA3 (void);
// 0x000004F7 System.Void Obi.ObiSolver::add_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnCollision_mF3DE133AD6D5E0BBBCCEC7EA3E66D929F8F3633B (void);
// 0x000004F8 System.Void Obi.ObiSolver::remove_OnCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnCollision_mB635DB605B5D19D3EA177631CA5439BFF1A8D1B5 (void);
// 0x000004F9 System.Void Obi.ObiSolver::add_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_add_OnParticleCollision_m31CEDEA9AA2D8A506A00A2D2D86E8DCE16014AB8 (void);
// 0x000004FA System.Void Obi.ObiSolver::remove_OnParticleCollision(Obi.ObiSolver/CollisionCallback)
extern void ObiSolver_remove_OnParticleCollision_mDC61C20C6D02E36B0B42E57203F932DFD3E6A74C (void);
// 0x000004FB System.Void Obi.ObiSolver::add_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnUpdateParameters_mF5F6CDCBE2937711ED779A336A41E4A9F09BFAFF (void);
// 0x000004FC System.Void Obi.ObiSolver::remove_OnUpdateParameters(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnUpdateParameters_m794890967633B6ABC38A06BCEBDA388A95FFF02B (void);
// 0x000004FD System.Void Obi.ObiSolver::add_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnPrepareStep_m5547752A41B84A5A2E7633F714D487836B587CE8 (void);
// 0x000004FE System.Void Obi.ObiSolver::remove_OnPrepareStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnPrepareStep_m67744EB2A8D5537C0C395623CD9D5B0F50E74B6E (void);
// 0x000004FF System.Void Obi.ObiSolver::add_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnBeginStep_m697F810061AEDD54F6C00780617C93DAB9ACE577 (void);
// 0x00000500 System.Void Obi.ObiSolver::remove_OnBeginStep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnBeginStep_m245376053A288393AC90A4C87D7735A775669F3A (void);
// 0x00000501 System.Void Obi.ObiSolver::add_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_add_OnSubstep_m74A3A296E17B831E20B660AB20E5A5C93E7AD793 (void);
// 0x00000502 System.Void Obi.ObiSolver::remove_OnSubstep(Obi.ObiSolver/SolverStepCallback)
extern void ObiSolver_remove_OnSubstep_mE68833452CD48337CF57F728508464BFF40A2BFD (void);
// 0x00000503 System.Void Obi.ObiSolver::add_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnEndStep_m32A01A15E29E0FD7197A3274B16B06627D5B97AB (void);
// 0x00000504 System.Void Obi.ObiSolver::remove_OnEndStep(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnEndStep_m4256367002E41AAFF8F8E1A77E899047CC753C03 (void);
// 0x00000505 System.Void Obi.ObiSolver::add_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_add_OnInterpolate_mB678D7773AE4C54FF3B95B037B4E1A2B62E93FDD (void);
// 0x00000506 System.Void Obi.ObiSolver::remove_OnInterpolate(Obi.ObiSolver/SolverCallback)
extern void ObiSolver_remove_OnInterpolate_mCD70D73217DE553A814A4DDF15344D28FC6A4D8C (void);
// 0x00000507 Obi.ISolverImpl Obi.ObiSolver::get_implementation()
extern void ObiSolver_get_implementation_m9F2CDFEB83D2A934ACB9BB40DC1AFD915B18A19E (void);
// 0x00000508 System.Boolean Obi.ObiSolver::get_initialized()
extern void ObiSolver_get_initialized_m586F23FD6F4749B03B0079D1B2A9D33165C798F5 (void);
// 0x00000509 Obi.IObiBackend Obi.ObiSolver::get_simulationBackend()
extern void ObiSolver_get_simulationBackend_m216408A308F32F19ADC5AB6618FBACADB1437A71 (void);
// 0x0000050A System.Void Obi.ObiSolver::set_backendType(Obi.ObiSolver/BackendType)
extern void ObiSolver_set_backendType_mA247F3405FC0E70B541DA283DC816A39F2B70516 (void);
// 0x0000050B Obi.ObiSolver/BackendType Obi.ObiSolver::get_backendType()
extern void ObiSolver_get_backendType_mCEAC2DE80955390DC501F700FA3AD5F28CD593B4 (void);
// 0x0000050C Obi.SimplexCounts Obi.ObiSolver::get_simplexCounts()
extern void ObiSolver_get_simplexCounts_m991E746582686190DB673974A06446DDD330CB27 (void);
// 0x0000050D UnityEngine.Bounds Obi.ObiSolver::get_Bounds()
extern void ObiSolver_get_Bounds_m44EA3D10C353AC78AC16FBA599540D93859BDCB8 (void);
// 0x0000050E System.Boolean Obi.ObiSolver::get_IsVisible()
extern void ObiSolver_get_IsVisible_m45379E9399A54D9D79F0984367438EDDC145FA17 (void);
// 0x0000050F System.Single Obi.ObiSolver::get_maxScale()
extern void ObiSolver_get_maxScale_mE496805FAB7D4C7273D7F06A1E9E074DA574AED4 (void);
// 0x00000510 System.Int32 Obi.ObiSolver::get_allocParticleCount()
extern void ObiSolver_get_allocParticleCount_m54EE5AF265571EBA043DA09233C41D9F9E22910E (void);
// 0x00000511 System.Int32 Obi.ObiSolver::get_pointCount()
extern void ObiSolver_get_pointCount_m2138390024C8A2E8F087D1411C198623BE88D84C (void);
// 0x00000512 System.Int32 Obi.ObiSolver::get_edgeCount()
extern void ObiSolver_get_edgeCount_mA206ADAEEF71C25CD106E2B45CD68A64E3BAA1D0 (void);
// 0x00000513 System.Int32 Obi.ObiSolver::get_triCount()
extern void ObiSolver_get_triCount_mB99127D38C39BB12017009B3BFE5CE3F285A59C1 (void);
// 0x00000514 System.Int32 Obi.ObiSolver::get_contactCount()
extern void ObiSolver_get_contactCount_mA1423B73BAE2F730068A141340319262DB0F8D5A (void);
// 0x00000515 System.Int32 Obi.ObiSolver::get_particleContactCount()
extern void ObiSolver_get_particleContactCount_m4D7418A6ADED5531781C2C97E3F9E93F1ED78751 (void);
// 0x00000516 Obi.ObiSolver/ParticleInActor[] Obi.ObiSolver::get_particleToActor()
extern void ObiSolver_get_particleToActor_m8C24527E4AF167BF38FBAA41CBEF35C7E918002C (void);
// 0x00000517 Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyLinearDeltas()
extern void ObiSolver_get_rigidbodyLinearDeltas_m245881A5A99F88E1F6F0D3E7F470F8EF446EC078 (void);
// 0x00000518 Obi.ObiNativeVector4List Obi.ObiSolver::get_rigidbodyAngularDeltas()
extern void ObiSolver_get_rigidbodyAngularDeltas_m44468761D636C511AEEB712F8074643A29D92060 (void);
// 0x00000519 UnityEngine.Color[] Obi.ObiSolver::get_colors()
extern void ObiSolver_get_colors_mD936B7EDEBB6324E6EEEEA7B8BA34F9E4C99FCDF (void);
// 0x0000051A Obi.ObiNativeInt4List Obi.ObiSolver::get_cellCoords()
extern void ObiSolver_get_cellCoords_mEAB574A55A49F74BEE1BC9461EDAEFE971AD6456 (void);
// 0x0000051B Obi.ObiNativeVector4List Obi.ObiSolver::get_positions()
extern void ObiSolver_get_positions_m0C88959F7FD733D60EE1E08074ACA6B2DC6EC35D (void);
// 0x0000051C Obi.ObiNativeVector4List Obi.ObiSolver::get_restPositions()
extern void ObiSolver_get_restPositions_mD6D8BC6C5ED0DC8B6DC1D8F7FEBAC5D2EFD9AE68 (void);
// 0x0000051D Obi.ObiNativeVector4List Obi.ObiSolver::get_prevPositions()
extern void ObiSolver_get_prevPositions_mBBD0165222ED8AD2211A668F58CD0B1DFE984920 (void);
// 0x0000051E Obi.ObiNativeVector4List Obi.ObiSolver::get_startPositions()
extern void ObiSolver_get_startPositions_m9BF78FEAA535DA234D4785F2C7BFABD11BFE3279 (void);
// 0x0000051F Obi.ObiNativeVector4List Obi.ObiSolver::get_renderablePositions()
extern void ObiSolver_get_renderablePositions_m7807CBAD597B97FCF5FC1C7184D168F01C97E8D8 (void);
// 0x00000520 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientations()
extern void ObiSolver_get_orientations_mDD97AAB61F0DE2E6156B576946EE85482A36D495 (void);
// 0x00000521 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_restOrientations()
extern void ObiSolver_get_restOrientations_m9AC87DDE8ED8D54823805ACB1848782C09ED92C1 (void);
// 0x00000522 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_prevOrientations()
extern void ObiSolver_get_prevOrientations_m41AF1E74113EB276342CCBBE27A067506F8AC0FF (void);
// 0x00000523 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_startOrientations()
extern void ObiSolver_get_startOrientations_m14958738016263B46143EB1AB51385579C93504F (void);
// 0x00000524 Obi.ObiNativeQuaternionList Obi.ObiSolver::get_renderableOrientations()
extern void ObiSolver_get_renderableOrientations_m6315C54D34044153479A72F89B7F0E6AEB074E46 (void);
// 0x00000525 Obi.ObiNativeVector4List Obi.ObiSolver::get_velocities()
extern void ObiSolver_get_velocities_m6EADFFB2BA3663150EA1AE4BAFEB3502914D4817 (void);
// 0x00000526 Obi.ObiNativeVector4List Obi.ObiSolver::get_angularVelocities()
extern void ObiSolver_get_angularVelocities_m2B5062FB47C4F47ED9D6DB85BFDAF12E06201568 (void);
// 0x00000527 Obi.ObiNativeFloatList Obi.ObiSolver::get_invMasses()
extern void ObiSolver_get_invMasses_mEAD2D3480FDDF75E32C5951A998CBCECBD24D3B3 (void);
// 0x00000528 Obi.ObiNativeFloatList Obi.ObiSolver::get_invRotationalMasses()
extern void ObiSolver_get_invRotationalMasses_m07B879855F0838D371BC5F64EE79C722E6EC75C4 (void);
// 0x00000529 Obi.ObiNativeVector4List Obi.ObiSolver::get_invInertiaTensors()
extern void ObiSolver_get_invInertiaTensors_m10FFFA6753076888A60BB29B828E6DD2AE39C858 (void);
// 0x0000052A Obi.ObiNativeVector4List Obi.ObiSolver::get_externalForces()
extern void ObiSolver_get_externalForces_m8C5E7D9722D7535C890003050858B2D4BA7634A7 (void);
// 0x0000052B Obi.ObiNativeVector4List Obi.ObiSolver::get_externalTorques()
extern void ObiSolver_get_externalTorques_m4BE93F20AAD6068A04FED46FE15720B2B4668B70 (void);
// 0x0000052C Obi.ObiNativeVector4List Obi.ObiSolver::get_wind()
extern void ObiSolver_get_wind_m794C8FCFFF227D75C5E064EB592F995D99D668B4 (void);
// 0x0000052D Obi.ObiNativeVector4List Obi.ObiSolver::get_positionDeltas()
extern void ObiSolver_get_positionDeltas_mEDABA21B58E0ED5F314302BC505578EEFFDEAF65 (void);
// 0x0000052E Obi.ObiNativeQuaternionList Obi.ObiSolver::get_orientationDeltas()
extern void ObiSolver_get_orientationDeltas_m7553005E502BE4300578DD6B42B9327DBB359D55 (void);
// 0x0000052F Obi.ObiNativeIntList Obi.ObiSolver::get_positionConstraintCounts()
extern void ObiSolver_get_positionConstraintCounts_mBEB32A98E26252D0AF555A30FC233EEA12D03746 (void);
// 0x00000530 Obi.ObiNativeIntList Obi.ObiSolver::get_orientationConstraintCounts()
extern void ObiSolver_get_orientationConstraintCounts_mE778057F1D89CB8AFFD4F0E1B47D6C59CA51AE98 (void);
// 0x00000531 Obi.ObiNativeIntList Obi.ObiSolver::get_collisionMaterials()
extern void ObiSolver_get_collisionMaterials_m763C9617995F1AA97F44A98AC8C4E6451D79F437 (void);
// 0x00000532 Obi.ObiNativeIntList Obi.ObiSolver::get_phases()
extern void ObiSolver_get_phases_m710DD563003F4AAA696450EC266F2CEF699DA71B (void);
// 0x00000533 Obi.ObiNativeIntList Obi.ObiSolver::get_filters()
extern void ObiSolver_get_filters_m802746B5BFEAF09C501A5DA28057F6698FBC745D (void);
// 0x00000534 Obi.ObiNativeVector4List Obi.ObiSolver::get_anisotropies()
extern void ObiSolver_get_anisotropies_m417C6BCD4180A9A33D325D8A56BC3982EB0524A9 (void);
// 0x00000535 Obi.ObiNativeVector4List Obi.ObiSolver::get_principalRadii()
extern void ObiSolver_get_principalRadii_mFD708006AF8833856FC8A3388B3DAC453CD0CC92 (void);
// 0x00000536 Obi.ObiNativeVector4List Obi.ObiSolver::get_normals()
extern void ObiSolver_get_normals_mF5D5F729DD254D5C4589439BF80094E99A6BB073 (void);
// 0x00000537 Obi.ObiNativeVector4List Obi.ObiSolver::get_vorticities()
extern void ObiSolver_get_vorticities_mDBB35B67BDF862A0EBDAB5B03FBD44FB16B61A8E (void);
// 0x00000538 Obi.ObiNativeVector4List Obi.ObiSolver::get_fluidData()
extern void ObiSolver_get_fluidData_m15AA21C3B2A390CD1EB4D1F9447A9A91E0ECAFA4 (void);
// 0x00000539 Obi.ObiNativeVector4List Obi.ObiSolver::get_userData()
extern void ObiSolver_get_userData_m1EE0A46AB278CD90EE43984496EB7C47ACF0F5D5 (void);
// 0x0000053A Obi.ObiNativeFloatList Obi.ObiSolver::get_smoothingRadii()
extern void ObiSolver_get_smoothingRadii_mB786B31388B6800F59F07607B77CBF076599B422 (void);
// 0x0000053B Obi.ObiNativeFloatList Obi.ObiSolver::get_buoyancies()
extern void ObiSolver_get_buoyancies_mFEA15451B676D494D1BDE5679D03C17124C1697A (void);
// 0x0000053C Obi.ObiNativeFloatList Obi.ObiSolver::get_restDensities()
extern void ObiSolver_get_restDensities_m8D042F798C98339CE085260D8F81269CAD122172 (void);
// 0x0000053D Obi.ObiNativeFloatList Obi.ObiSolver::get_viscosities()
extern void ObiSolver_get_viscosities_m11E76D9C7E1BCCD395F024C7F0D9C0A2F3965E23 (void);
// 0x0000053E Obi.ObiNativeFloatList Obi.ObiSolver::get_surfaceTension()
extern void ObiSolver_get_surfaceTension_m96FBC03646C165F1CF91A0D80E6A506B0C571358 (void);
// 0x0000053F Obi.ObiNativeFloatList Obi.ObiSolver::get_vortConfinement()
extern void ObiSolver_get_vortConfinement_m28757BBCC63B9D797D78F9394D9FB6F36073087D (void);
// 0x00000540 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericDrag()
extern void ObiSolver_get_atmosphericDrag_m46B1B3645BB3F3AC6920E53F49032EF9D7A2930F (void);
// 0x00000541 Obi.ObiNativeFloatList Obi.ObiSolver::get_atmosphericPressure()
extern void ObiSolver_get_atmosphericPressure_m97A89CCC57CD237DDE740ABA272874AA1F9D5AA4 (void);
// 0x00000542 Obi.ObiNativeFloatList Obi.ObiSolver::get_diffusion()
extern void ObiSolver_get_diffusion_mBB44EC5D7C1ECC9FD2994FC121F9161750F240F4 (void);
// 0x00000543 System.Void Obi.ObiSolver::Update()
extern void ObiSolver_Update_m9ED3E3A02C9973F36132781D6AEAEB20CE5061DE (void);
// 0x00000544 System.Void Obi.ObiSolver::OnDestroy()
extern void ObiSolver_OnDestroy_mA8F7B6138D59E738D0883567701A1664FD187DF0 (void);
// 0x00000545 System.Void Obi.ObiSolver::CreateBackend()
extern void ObiSolver_CreateBackend_mD8BE03B36506903B1F388B0DC214DD817FD1B798 (void);
// 0x00000546 System.Void Obi.ObiSolver::Initialize()
extern void ObiSolver_Initialize_m89B29A40A3418F087F30611AFAD542DD1AFC16FA (void);
// 0x00000547 System.Void Obi.ObiSolver::Teardown()
extern void ObiSolver_Teardown_m0FF521346E884F8AE239BAC4A1835722A2550A7B (void);
// 0x00000548 System.Void Obi.ObiSolver::UpdateBackend()
extern void ObiSolver_UpdateBackend_m027C131E805EC2A9A36FBFE54B3D95E5A8F76BB0 (void);
// 0x00000549 System.Void Obi.ObiSolver::FreeRigidbodyArrays()
extern void ObiSolver_FreeRigidbodyArrays_m7D6EEFD9184E2E8767D47FF4882AA01FB6DBAC71 (void);
// 0x0000054A System.Void Obi.ObiSolver::EnsureRigidbodyArraysCapacity(System.Int32)
extern void ObiSolver_EnsureRigidbodyArraysCapacity_m821AF4A6183257D4C939236A5D4EF8938259BB59 (void);
// 0x0000054B System.Void Obi.ObiSolver::FreeParticleArrays()
extern void ObiSolver_FreeParticleArrays_m761E1045358FB28F88DD93AF49A9755C49E1CEE1 (void);
// 0x0000054C System.Void Obi.ObiSolver::EnsureParticleArraysCapacity(System.Int32)
extern void ObiSolver_EnsureParticleArraysCapacity_mB711BB3673CA3C04BBD363EFC78EE5FB29AA0278 (void);
// 0x0000054D System.Void Obi.ObiSolver::AllocateParticles(System.Int32[])
extern void ObiSolver_AllocateParticles_m2E904400BE8CD3551AD991B7BE20F49B3D8A99B9 (void);
// 0x0000054E System.Void Obi.ObiSolver::FreeParticles(System.Int32[])
extern void ObiSolver_FreeParticles_m58E563D22C726EB41700F77AB8D0DD429111AD62 (void);
// 0x0000054F System.Boolean Obi.ObiSolver::AddActor(Obi.ObiActor)
extern void ObiSolver_AddActor_m659F33D07DFB3765DC89C6BE5618CDC8B3F3CE62 (void);
// 0x00000550 System.Boolean Obi.ObiSolver::RemoveActor(Obi.ObiActor)
extern void ObiSolver_RemoveActor_mF70200B7D46C8BB2F6488A15A429F99CD25B669E (void);
// 0x00000551 System.Void Obi.ObiSolver::PushSolverParameters()
extern void ObiSolver_PushSolverParameters_m9B8F26085CB523DACA1DBE26C1BE132E92291AE5 (void);
// 0x00000552 Oni/ConstraintParameters Obi.ObiSolver::GetConstraintParameters(Oni/ConstraintType)
extern void ObiSolver_GetConstraintParameters_mFC75278F5EB861E2A14855A7FCB6B31B1C68BDD0 (void);
// 0x00000553 Obi.IObiConstraints Obi.ObiSolver::GetConstraintsByType(Oni/ConstraintType)
extern void ObiSolver_GetConstraintsByType_mAF65FA4A57FF5231A7F958468B08919364BBF14B (void);
// 0x00000554 System.Void Obi.ObiSolver::PushActiveParticles()
extern void ObiSolver_PushActiveParticles_m711160E900FAB1C9612ABE8E64DF3580D298B18F (void);
// 0x00000555 System.Void Obi.ObiSolver::PushSimplices()
extern void ObiSolver_PushSimplices_m26F6066BBC71182C229F61991BEEB12D7C455A8A (void);
// 0x00000556 System.Void Obi.ObiSolver::PushConstraints()
extern void ObiSolver_PushConstraints_mD5C51F2BE7021B9BA8C71BDF142D485693228C56 (void);
// 0x00000557 System.Void Obi.ObiSolver::UpdateVisibility()
extern void ObiSolver_UpdateVisibility_mE67BDBAD691D6C9E4163D5239B7B203E1F78FC46 (void);
// 0x00000558 System.Void Obi.ObiSolver::InitializeTransformFrame()
extern void ObiSolver_InitializeTransformFrame_mB5F807A577821103ED00EC2F04D06EC2F130665D (void);
// 0x00000559 System.Void Obi.ObiSolver::UpdateTransformFrame(System.Single)
extern void ObiSolver_UpdateTransformFrame_m149D2F3207C6955651F175B9FF035EA5B6CACB99 (void);
// 0x0000055A Obi.IObiJobHandle Obi.ObiSolver::BeginStep(System.Single)
extern void ObiSolver_BeginStep_m0209B4FE6F55F359D380B81628BFB93DD7171E4B (void);
// 0x0000055B Obi.IObiJobHandle Obi.ObiSolver::Substep(System.Single,System.Single,System.Int32)
extern void ObiSolver_Substep_mC87665C9B26AE616BE2846C00294D9656FBE4AB9 (void);
// 0x0000055C System.Void Obi.ObiSolver::EndStep(System.Single)
extern void ObiSolver_EndStep_mB69D4B5498573F533F766596BDA938AAB4121B51 (void);
// 0x0000055D System.Void Obi.ObiSolver::Interpolate(System.Single,System.Single)
extern void ObiSolver_Interpolate_m76EE8F8FF90774103389F56796B5158394B9FCBA (void);
// 0x0000055E System.Void Obi.ObiSolver::SpatialQuery(Obi.ObiNativeQueryShapeList,Obi.ObiNativeAffineTransformList,Obi.ObiNativeQueryResultList)
extern void ObiSolver_SpatialQuery_mAC8DE6B38410845DA38B8415E8A9A7591F45938F (void);
// 0x0000055F Obi.QueryResult[] Obi.ObiSolver::SpatialQuery(Obi.QueryShape,Obi.AffineTransform)
extern void ObiSolver_SpatialQuery_m97851526FF0B3A4451EAE3C6B65F79A40B6DABA3 (void);
// 0x00000560 System.Boolean Obi.ObiSolver::Raycast(UnityEngine.Ray,Obi.QueryResult&,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_m295E64295FFE106BCE8A4C8AF731175FFBD53F4E (void);
// 0x00000561 Obi.QueryResult[] Obi.ObiSolver::Raycast(System.Collections.Generic.List`1<UnityEngine.Ray>,System.Int32,System.Single,System.Single)
extern void ObiSolver_Raycast_m1BF0ECF62E59E8E53B478E8E28E332DDDB6B7B07 (void);
// 0x00000562 System.Void Obi.ObiSolver::.ctor()
extern void ObiSolver__ctor_m62424792417D25BB315A0F76088BA278489E19D6 (void);
// 0x00000563 System.Void Obi.ObiSolver::.cctor()
extern void ObiSolver__cctor_mAB381E213872C557C12FC3F05519FF556F1B5E28 (void);
// 0x00000564 System.Void Obi.ObiSolver/ObiCollisionEventArgs::.ctor()
extern void ObiCollisionEventArgs__ctor_m7C09249A2F3E995C9BEB7D796FCFDFDF5CFDE12E (void);
// 0x00000565 System.Void Obi.ObiSolver/ParticleInActor::.ctor()
extern void ParticleInActor__ctor_mB6476E7B7DCC76BF8E7067F3C93981363C01F038 (void);
// 0x00000566 System.Void Obi.ObiSolver/ParticleInActor::.ctor(Obi.ObiActor,System.Int32)
extern void ParticleInActor__ctor_mFB6EC1CA7434441ECBE52C550353458005AF52C7 (void);
// 0x00000567 System.Void Obi.ObiSolver/SolverCallback::.ctor(System.Object,System.IntPtr)
extern void SolverCallback__ctor_m251483D210E63FBB0C36F821A4629020D9E749C5 (void);
// 0x00000568 System.Void Obi.ObiSolver/SolverCallback::Invoke(Obi.ObiSolver)
extern void SolverCallback_Invoke_m60FA315704A9C77F3EC00614322BFB0D0BBE59A1 (void);
// 0x00000569 System.IAsyncResult Obi.ObiSolver/SolverCallback::BeginInvoke(Obi.ObiSolver,System.AsyncCallback,System.Object)
extern void SolverCallback_BeginInvoke_m9D4170E99C634E41DCB9A39E5EC8FFA97EC265FF (void);
// 0x0000056A System.Void Obi.ObiSolver/SolverCallback::EndInvoke(System.IAsyncResult)
extern void SolverCallback_EndInvoke_mE89E09F4CCF079537C3C0CB34CA11F3A654383D2 (void);
// 0x0000056B System.Void Obi.ObiSolver/SolverStepCallback::.ctor(System.Object,System.IntPtr)
extern void SolverStepCallback__ctor_mE4A7F99972297FEF75B4B25D998F4496E5890E18 (void);
// 0x0000056C System.Void Obi.ObiSolver/SolverStepCallback::Invoke(Obi.ObiSolver,System.Single)
extern void SolverStepCallback_Invoke_m040E6103366E4451B8161A2E24A53FFCB2AF36C5 (void);
// 0x0000056D System.IAsyncResult Obi.ObiSolver/SolverStepCallback::BeginInvoke(Obi.ObiSolver,System.Single,System.AsyncCallback,System.Object)
extern void SolverStepCallback_BeginInvoke_mFD6E4177C6426D5562D15B58F3C6C0A71774F775 (void);
// 0x0000056E System.Void Obi.ObiSolver/SolverStepCallback::EndInvoke(System.IAsyncResult)
extern void SolverStepCallback_EndInvoke_mF2ECB6CED6D79AD61904B933E40EEFFA74332BBE (void);
// 0x0000056F System.Void Obi.ObiSolver/CollisionCallback::.ctor(System.Object,System.IntPtr)
extern void CollisionCallback__ctor_m89C24EA62FB9D7815E190FB50B37031520F79A07 (void);
// 0x00000570 System.Void Obi.ObiSolver/CollisionCallback::Invoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs)
extern void CollisionCallback_Invoke_m00A77BDBCF16A248EBAEC8B3D30FCC86AA91BF48 (void);
// 0x00000571 System.IAsyncResult Obi.ObiSolver/CollisionCallback::BeginInvoke(Obi.ObiSolver,Obi.ObiSolver/ObiCollisionEventArgs,System.AsyncCallback,System.Object)
extern void CollisionCallback_BeginInvoke_mC23F3ADD17827DB14FB0436FCC49CFFD8B58E9D6 (void);
// 0x00000572 System.Void Obi.ObiSolver/CollisionCallback::EndInvoke(System.IAsyncResult)
extern void CollisionCallback_EndInvoke_m592B0D186416285FBABBB542CB3D51DFFAE5331E (void);
// 0x00000573 System.Void Obi.ObiSolver/<>c::.cctor()
extern void U3CU3Ec__cctor_m52C5CB1B3097B0072B0905815FA7FCB4E4CBC437 (void);
// 0x00000574 System.Void Obi.ObiSolver/<>c::.ctor()
extern void U3CU3Ec__ctor_m20A3AE22CB86D091916952C584CB8FE816F13064 (void);
// 0x00000575 System.Boolean Obi.ObiSolver/<>c::<get_allocParticleCount>b__143_0(Obi.ObiSolver/ParticleInActor)
extern void U3CU3Ec_U3Cget_allocParticleCountU3Eb__143_0_m1CBFBBEBF59C2BB9DBAA1254BB36CB4DA2BA086A (void);
// 0x00000576 System.Void Obi.ObiFixedUpdater::OnValidate()
extern void ObiFixedUpdater_OnValidate_m76229031B40F92A4301EEC77378FE22C1585EF75 (void);
// 0x00000577 System.Void Obi.ObiFixedUpdater::Awake()
extern void ObiFixedUpdater_Awake_m05F83D529770320298C9C4C6915A8F91719F1302 (void);
// 0x00000578 System.Void Obi.ObiFixedUpdater::OnDisable()
extern void ObiFixedUpdater_OnDisable_m9F686B2F0BC11763672725B37057F421B46DC477 (void);
// 0x00000579 System.Void Obi.ObiFixedUpdater::FixedUpdate()
extern void ObiFixedUpdater_FixedUpdate_m6960C109F19FEBC5A547AF798DEABAB519EBBE06 (void);
// 0x0000057A System.Void Obi.ObiFixedUpdater::Update()
extern void ObiFixedUpdater_Update_m91AC799CBA5CC912B6029EB6329A7B5E5E6E9498 (void);
// 0x0000057B System.Void Obi.ObiFixedUpdater::.ctor()
extern void ObiFixedUpdater__ctor_m6990831EFBDD958CA0F347B5EA7F3DEA852589C8 (void);
// 0x0000057C System.Void Obi.ObiLateFixedUpdater::OnValidate()
extern void ObiLateFixedUpdater_OnValidate_m213475905C35111F196843770C5E36BF654EA143 (void);
// 0x0000057D System.Void Obi.ObiLateFixedUpdater::Awake()
extern void ObiLateFixedUpdater_Awake_mE1C1C00A4FC7D3BCDFFCA0F70E84E51F5ADC8438 (void);
// 0x0000057E System.Void Obi.ObiLateFixedUpdater::OnEnable()
extern void ObiLateFixedUpdater_OnEnable_m908C01CE84D9447C31A99BAE4A09D7FFB0D88FDB (void);
// 0x0000057F System.Void Obi.ObiLateFixedUpdater::OnDisable()
extern void ObiLateFixedUpdater_OnDisable_mA3804203725D3B8E9112D94FF6F877432FA31AFF (void);
// 0x00000580 System.Collections.IEnumerator Obi.ObiLateFixedUpdater::RunLateFixedUpdate()
extern void ObiLateFixedUpdater_RunLateFixedUpdate_m0CDA0F9B4B1972B0C449BC2FB04DE440C02AF7CD (void);
// 0x00000581 System.Void Obi.ObiLateFixedUpdater::LateFixedUpdate()
extern void ObiLateFixedUpdater_LateFixedUpdate_mA4CD86E0316FF07E3290E5C9684FF6167CC4761C (void);
// 0x00000582 System.Void Obi.ObiLateFixedUpdater::Update()
extern void ObiLateFixedUpdater_Update_m18A4EDFF29C841B340689E1C7D9DAC5454F4B0D2 (void);
// 0x00000583 System.Void Obi.ObiLateFixedUpdater::.ctor()
extern void ObiLateFixedUpdater__ctor_m5198FFBED9AAE55028D7D8BD3C86961A54CFEE30 (void);
// 0x00000584 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::.ctor(System.Int32)
extern void U3CRunLateFixedUpdateU3Ed__6__ctor_m1C39A2F60B53405954DCD5ED9FE325876FE5CDC7 (void);
// 0x00000585 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.IDisposable.Dispose()
extern void U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_m5EF7D405A0731CF20480B0E79335798E70CCAAC4 (void);
// 0x00000586 System.Boolean Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::MoveNext()
extern void U3CRunLateFixedUpdateU3Ed__6_MoveNext_mF569121D9A2070DDB78D5F28B7A198528F949DF2 (void);
// 0x00000587 System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B8AA764EE69F1B4CD885E26C2D7331DEEA888A3 (void);
// 0x00000588 System.Void Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.Reset()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_mEFE1C118BDB12FAF1140DDFE8C67855C34061EF2 (void);
// 0x00000589 System.Object Obi.ObiLateFixedUpdater/<RunLateFixedUpdate>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_mC15BD77B70D9ADFFC5B97FA09E8DC226360E37C4 (void);
// 0x0000058A System.Void Obi.ObiLateUpdater::OnValidate()
extern void ObiLateUpdater_OnValidate_m6A70608502C5AC388A21B4A40E60A1F726539846 (void);
// 0x0000058B System.Void Obi.ObiLateUpdater::LateUpdate()
extern void ObiLateUpdater_LateUpdate_mFCFCD6F9F1BA8745F36429DDE808D093F581903A (void);
// 0x0000058C System.Void Obi.ObiLateUpdater::.ctor()
extern void ObiLateUpdater__ctor_mCBEB8B90CE4D6853A1FCD9606E7D1B40C5C78F53 (void);
// 0x0000058D System.Void Obi.ObiUpdater::BeginStep(System.Single)
extern void ObiUpdater_BeginStep_m3AA829EBD617621E4C0470786FB8A83B5B170B98 (void);
// 0x0000058E System.Void Obi.ObiUpdater::Substep(System.Single,System.Single,System.Int32)
extern void ObiUpdater_Substep_mE0E36E8748E6F8C4FFD03D53AF5AB8D8E176D006 (void);
// 0x0000058F System.Void Obi.ObiUpdater::EndStep(System.Single)
extern void ObiUpdater_EndStep_mD6541C67A8513F342A8E91D557C6D19ADE67ED2E (void);
// 0x00000590 System.Void Obi.ObiUpdater::Interpolate(System.Single,System.Single)
extern void ObiUpdater_Interpolate_m6EB1759D2C8325F5DE48A578DF71A5A1DD548382 (void);
// 0x00000591 System.Void Obi.ObiUpdater::.ctor()
extern void ObiUpdater__ctor_mF81A4C5CD5B546F86F6AB868A63AFB947CFD59EC (void);
// 0x00000592 System.Void Obi.ObiUpdater::.cctor()
extern void ObiUpdater__cctor_m107D423EE0493C47465FCC5662C2D9BC850F9946 (void);
// 0x00000593 System.Void Obi.ChildrenOnly::.ctor()
extern void ChildrenOnly__ctor_m048197E7104356A10634C5B8B583E1010E74538A (void);
// 0x00000594 System.Void Obi.Indent::.ctor()
extern void Indent__ctor_mECAFC7D3C134B49D1C1A0DF0F973A9454722793A (void);
// 0x00000595 System.Single Obi.InspectorButtonAttribute::get_ButtonWidth()
extern void InspectorButtonAttribute_get_ButtonWidth_m8D91F1A836B21EA9065F75CD372433EAB304C3BA (void);
// 0x00000596 System.Void Obi.InspectorButtonAttribute::set_ButtonWidth(System.Single)
extern void InspectorButtonAttribute_set_ButtonWidth_m43CB0912FC4563956094937D35F1A71C0C07A6C0 (void);
// 0x00000597 System.Void Obi.InspectorButtonAttribute::.ctor(System.String)
extern void InspectorButtonAttribute__ctor_m1055891570899590DA6F5A5A4134C4156D341934 (void);
// 0x00000598 System.Void Obi.InspectorButtonAttribute::.cctor()
extern void InspectorButtonAttribute__cctor_m94D32958C7FF15E09C795B7727387C628A286A58 (void);
// 0x00000599 System.Void Obi.MinMaxAttribute::.ctor(System.Single,System.Single)
extern void MinMaxAttribute__ctor_mC312C7DFAB309656D26818D5AF3BD99E77195C51 (void);
// 0x0000059A System.Void Obi.MultiDelayed::.ctor()
extern void MultiDelayed__ctor_mA3652B4326B3EC475333F955D325DA6991030CFC (void);
// 0x0000059B System.Void Obi.MultiPropertyAttribute::.ctor()
extern void MultiPropertyAttribute__ctor_mB2FEE6D7DD030ACDDDBE66B7D81C7A207AFEC921 (void);
// 0x0000059C System.Void Obi.MultiRange::.ctor(System.Single,System.Single)
extern void MultiRange__ctor_mD5F08C882D531555721C836FCDFA4803B84CBABA (void);
// 0x0000059D System.String Obi.SerializeProperty::get_PropertyName()
extern void SerializeProperty_get_PropertyName_m8DB9FE2FCDAB970A4738FB21D3EBEDEC6A6C583F (void);
// 0x0000059E System.Void Obi.SerializeProperty::set_PropertyName(System.String)
extern void SerializeProperty_set_PropertyName_m5875C7F872DCB76DB77EE11805CCA13341D20EA2 (void);
// 0x0000059F System.Void Obi.SerializeProperty::.ctor(System.String)
extern void SerializeProperty__ctor_m63D37746946CC59110BBAFDD2ABB6E9B6A7D14D9 (void);
// 0x000005A0 System.String Obi.VisibleIf::get_MethodName()
extern void VisibleIf_get_MethodName_m1A4B9E57BD386B6B40C18154B22C972502F236C1 (void);
// 0x000005A1 System.Void Obi.VisibleIf::set_MethodName(System.String)
extern void VisibleIf_set_MethodName_mD526342BE1632E2ED1BEC686C11C8B38957ECE12 (void);
// 0x000005A2 System.Boolean Obi.VisibleIf::get_Negate()
extern void VisibleIf_get_Negate_m7A90AD5D89223445CB93EB298618495AAA7FD991 (void);
// 0x000005A3 System.Void Obi.VisibleIf::set_Negate(System.Boolean)
extern void VisibleIf_set_Negate_mA288AECADE78CF5998F29A41D1ADBEAB830677B6 (void);
// 0x000005A4 System.Void Obi.VisibleIf::.ctor(System.String,System.Boolean)
extern void VisibleIf__ctor_m93166DBD8925F3F77763F63933E2F7645312953C (void);
// 0x000005A5 System.Object Obi.CoroutineJob::get_Result()
extern void CoroutineJob_get_Result_m005D2CAD9ED068AB5AF3EA3A23C24111EC4975E2 (void);
// 0x000005A6 System.Boolean Obi.CoroutineJob::get_IsDone()
extern void CoroutineJob_get_IsDone_mFF5515EC07F513B5BF06254C73DDEC42567EE798 (void);
// 0x000005A7 System.Boolean Obi.CoroutineJob::get_RaisedException()
extern void CoroutineJob_get_RaisedException_m1B59EFB8ED5C17EFA9F1AF027BC10A1ED263D4AB (void);
// 0x000005A8 System.Void Obi.CoroutineJob::Init()
extern void CoroutineJob_Init_m39C95915EEEFA70D389ADBBF37D655C876186E52 (void);
// 0x000005A9 System.Object Obi.CoroutineJob::RunSynchronously(System.Collections.IEnumerator)
extern void CoroutineJob_RunSynchronously_mD27E02C233BE1EF2F248BEBE9ED29B3488AC39A2 (void);
// 0x000005AA System.Collections.IEnumerator Obi.CoroutineJob::Start(System.Collections.IEnumerator)
extern void CoroutineJob_Start_m375FD45CCADE19D80E522854D0FB39CDDBEE1FE3 (void);
// 0x000005AB System.Void Obi.CoroutineJob::Stop()
extern void CoroutineJob_Stop_m9CABBC6A46ABBBE31C12D0C89D8F8EC430B475DC (void);
// 0x000005AC System.Void Obi.CoroutineJob::.ctor()
extern void CoroutineJob__ctor_m54279053114D120A1A69B77CD846D98BB00E2538 (void);
// 0x000005AD System.Void Obi.CoroutineJob/ProgressInfo::.ctor(System.String,System.Single)
extern void ProgressInfo__ctor_mE34603E06F6E11AA1D4902057779C81537F1BEC0 (void);
// 0x000005AE System.Void Obi.CoroutineJob/<Start>d__15::.ctor(System.Int32)
extern void U3CStartU3Ed__15__ctor_m8940EBFDCAB29E979DEFB2E00C0C0EE3B8B327F2 (void);
// 0x000005AF System.Void Obi.CoroutineJob/<Start>d__15::System.IDisposable.Dispose()
extern void U3CStartU3Ed__15_System_IDisposable_Dispose_mD1B1A2865792F4F722584801277F43A9A9AD7135 (void);
// 0x000005B0 System.Boolean Obi.CoroutineJob/<Start>d__15::MoveNext()
extern void U3CStartU3Ed__15_MoveNext_mA51F84B63ACC0A785E9F7BA3345BBE4EFB0488FB (void);
// 0x000005B1 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E05A85EE72A4632DBE0E3418F0EE07485FFC0E (void);
// 0x000005B2 System.Void Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_m68D0EF2950D90BADEFB1CA3A1A2BC46D5E581FB3 (void);
// 0x000005B3 System.Object Obi.CoroutineJob/<Start>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mDB081BB96A0648750720D9D315A832F618E04ED2 (void);
// 0x000005B4 System.Void Obi.EditorCoroutine::ShowCoroutineProgressBar(System.String,System.Collections.IEnumerator&)
extern void EditorCoroutine_ShowCoroutineProgressBar_m6ADC4C30916CF6733EB5660CC91BD233C4E1B611 (void);
// 0x000005B5 System.Void Obi.EditorCoroutine::.ctor()
extern void EditorCoroutine__ctor_mF7ADB1EF57F9E6E6F047E840CB54D4D269427BC0 (void);
// 0x000005B6 System.Void Obi.ObiAmbientForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiAmbientForceZone_ApplyForcesToActor_mD7F1FF1CA51BC4D30064221DCA818E5BBEE39F90 (void);
// 0x000005B7 System.Void Obi.ObiAmbientForceZone::OnDrawGizmosSelected()
extern void ObiAmbientForceZone_OnDrawGizmosSelected_m6247B9D0C476691F2087960CB116F2F291734833 (void);
// 0x000005B8 System.Void Obi.ObiAmbientForceZone::.ctor()
extern void ObiAmbientForceZone__ctor_mD5A328F41FE94DB134B362A688C33EFB40B0336D (void);
// 0x000005B9 System.Void Obi.ObiExternalForce::OnEnable()
extern void ObiExternalForce_OnEnable_m9C8027C03DB5299C5CEFD71970D715838FA20F40 (void);
// 0x000005BA System.Void Obi.ObiExternalForce::OnDisable()
extern void ObiExternalForce_OnDisable_m99DD20E326728D253D38776120151C4F78B29B90 (void);
// 0x000005BB System.Void Obi.ObiExternalForce::Solver_OnStepBegin(Obi.ObiSolver,System.Single)
extern void ObiExternalForce_Solver_OnStepBegin_m83BA11CBBDEB98596786149E6B19B9AD07672D06 (void);
// 0x000005BC System.Single Obi.ObiExternalForce::GetTurbulence(System.Single)
extern void ObiExternalForce_GetTurbulence_m52C1C609B68FEBBEA5CC6A02CCCABFF0F168397B (void);
// 0x000005BD System.Void Obi.ObiExternalForce::ApplyForcesToActor(Obi.ObiActor)
// 0x000005BE System.Void Obi.ObiExternalForce::.ctor()
extern void ObiExternalForce__ctor_m1A216AE34D90460D03E46EFE5BE490C7BCA0647E (void);
// 0x000005BF System.Void Obi.ObiSphericalForceZone::ApplyForcesToActor(Obi.ObiActor)
extern void ObiSphericalForceZone_ApplyForcesToActor_m0C3D245E0CABB0E33F06DF470BF31EC361DCDC11 (void);
// 0x000005C0 System.Void Obi.ObiSphericalForceZone::OnDrawGizmosSelected()
extern void ObiSphericalForceZone_OnDrawGizmosSelected_mD53969A56A126FEF59332B15ECAFECCE039BD318 (void);
// 0x000005C1 System.Void Obi.ObiSphericalForceZone::.ctor()
extern void ObiSphericalForceZone__ctor_mE4B89FE34EA98D83198B1AC6BD3E0B659E42F82E (void);
// 0x000005C2 System.Int32 Obi.ObiContactEventDispatcher::CompareByRef(Oni/Contact&,Oni/Contact&,Obi.ObiSolver)
extern void ObiContactEventDispatcher_CompareByRef_mDC7ED257C6F1AE367534B057503BDD3FF0A079C5 (void);
// 0x000005C3 System.Void Obi.ObiContactEventDispatcher::Awake()
extern void ObiContactEventDispatcher_Awake_m07A3EDEAA04DF5FE5C5E681957AD224CBB54E554 (void);
// 0x000005C4 System.Void Obi.ObiContactEventDispatcher::OnEnable()
extern void ObiContactEventDispatcher_OnEnable_m245E24D968CB80FDEA0E644B8666CC7D10AECF83 (void);
// 0x000005C5 System.Void Obi.ObiContactEventDispatcher::OnDisable()
extern void ObiContactEventDispatcher_OnDisable_mF486551C68967776E0F8F27588BC417F47F171A7 (void);
// 0x000005C6 System.Int32 Obi.ObiContactEventDispatcher::FilterOutDistantContacts(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_FilterOutDistantContacts_m5D131E57296726442BEF2B4E59396989C10BF642 (void);
// 0x000005C7 System.Int32 Obi.ObiContactEventDispatcher::RemoveDuplicates(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_RemoveDuplicates_m6A50787B4E23FD447ACCD8A8DE0995CE74E085FD (void);
// 0x000005C8 System.Void Obi.ObiContactEventDispatcher::InvokeCallbacks(Oni/Contact[],System.Int32)
extern void ObiContactEventDispatcher_InvokeCallbacks_m692BC361454FE9230E82B07824B0D0D3A9E0EB1B (void);
// 0x000005C9 System.Void Obi.ObiContactEventDispatcher::Solver_OnCollision(System.Object,Obi.ObiSolver/ObiCollisionEventArgs)
extern void ObiContactEventDispatcher_Solver_OnCollision_mBE446B9F7E91796B2A0CAE41FB666011D342B865 (void);
// 0x000005CA System.Void Obi.ObiContactEventDispatcher::.ctor()
extern void ObiContactEventDispatcher__ctor_m1A61C75E933604EDC536AAFB53F4F126C96D1475 (void);
// 0x000005CB System.Void Obi.ObiContactEventDispatcher/ContactComparer::.ctor(Obi.ObiSolver)
extern void ContactComparer__ctor_m4F8B5DB8A25B890F0E456DB3B8C932EA1869C2EB (void);
// 0x000005CC System.Int32 Obi.ObiContactEventDispatcher/ContactComparer::Compare(Oni/Contact,Oni/Contact)
extern void ContactComparer_Compare_m430A55340AD018C0DC9AEA1CD28B35517B246AA7 (void);
// 0x000005CD System.Void Obi.ObiContactEventDispatcher/ContactCallback::.ctor()
extern void ContactCallback__ctor_m380D27F1E006FE623593F2C3D10A1263C2E12C13 (void);
// 0x000005CE Obi.ObiActor Obi.ObiParticleAttachment::get_actor()
extern void ObiParticleAttachment_get_actor_m27851E500622AFD123A35F0A983417D87C4C6F15 (void);
// 0x000005CF UnityEngine.Transform Obi.ObiParticleAttachment::get_target()
extern void ObiParticleAttachment_get_target_m3792C1936854391A4B809902BEDAB269F30BA98A (void);
// 0x000005D0 System.Void Obi.ObiParticleAttachment::set_target(UnityEngine.Transform)
extern void ObiParticleAttachment_set_target_m7C3933233706C549785454D2E774BAC0411F15B1 (void);
// 0x000005D1 Obi.ObiParticleGroup Obi.ObiParticleAttachment::get_particleGroup()
extern void ObiParticleAttachment_get_particleGroup_m8AAF20A83123429F9009F71AA70DC7FE94403D53 (void);
// 0x000005D2 System.Void Obi.ObiParticleAttachment::set_particleGroup(Obi.ObiParticleGroup)
extern void ObiParticleAttachment_set_particleGroup_m0F43704FD2C3A792860CE707C8A7CCFADEA15695 (void);
// 0x000005D3 System.Boolean Obi.ObiParticleAttachment::get_isBound()
extern void ObiParticleAttachment_get_isBound_m1876F5B16BCD7528475C74D74A79768CC4641B77 (void);
// 0x000005D4 Obi.ObiParticleAttachment/AttachmentType Obi.ObiParticleAttachment::get_attachmentType()
extern void ObiParticleAttachment_get_attachmentType_m3C6DC87422FA425FBF438A8A7CA8FB5D443EB168 (void);
// 0x000005D5 System.Void Obi.ObiParticleAttachment::set_attachmentType(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_set_attachmentType_mBCA200E1E3C33103DDC3A3B15F12CFABA97BF09B (void);
// 0x000005D6 System.Boolean Obi.ObiParticleAttachment::get_constrainOrientation()
extern void ObiParticleAttachment_get_constrainOrientation_m08C5A79FCD2BFF79078F88EB6FA2BA72FFC4E28F (void);
// 0x000005D7 System.Void Obi.ObiParticleAttachment::set_constrainOrientation(System.Boolean)
extern void ObiParticleAttachment_set_constrainOrientation_m247F0066BF99CBC983FE9387278D1B4C7F56DA5D (void);
// 0x000005D8 System.Single Obi.ObiParticleAttachment::get_compliance()
extern void ObiParticleAttachment_get_compliance_m4558C4878270146EF8D49DF5D34FB8AC0D4CCA06 (void);
// 0x000005D9 System.Void Obi.ObiParticleAttachment::set_compliance(System.Single)
extern void ObiParticleAttachment_set_compliance_m8198F8613B2AED92CB67C4F2625F2C0C9AA64D7A (void);
// 0x000005DA System.Single Obi.ObiParticleAttachment::get_breakThreshold()
extern void ObiParticleAttachment_get_breakThreshold_mCC7D45869EB44AB95B7180D2C15E5ED944541458 (void);
// 0x000005DB System.Void Obi.ObiParticleAttachment::set_breakThreshold(System.Single)
extern void ObiParticleAttachment_set_breakThreshold_m31EB650F5F8C8054AC5753C115B1E479541BC3B0 (void);
// 0x000005DC System.Void Obi.ObiParticleAttachment::Awake()
extern void ObiParticleAttachment_Awake_m23446C5C74A5E7B08FFAA9747F56CE720F87F336 (void);
// 0x000005DD System.Void Obi.ObiParticleAttachment::OnDestroy()
extern void ObiParticleAttachment_OnDestroy_m52134FFBF2E5CF631EA7F1292FBAC1A254C7BDE3 (void);
// 0x000005DE System.Void Obi.ObiParticleAttachment::OnEnable()
extern void ObiParticleAttachment_OnEnable_mF34A9D513CF9D52B9A9E0759CC6A83C5176FD22F (void);
// 0x000005DF System.Void Obi.ObiParticleAttachment::OnDisable()
extern void ObiParticleAttachment_OnDisable_mF8C641CB0E3C1822D1484B333C1FA2AB240AA843 (void);
// 0x000005E0 System.Void Obi.ObiParticleAttachment::OnValidate()
extern void ObiParticleAttachment_OnValidate_m74E16BB78FFA79728F39C1946AE3DF5810C28EB4 (void);
// 0x000005E1 System.Void Obi.ObiParticleAttachment::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiParticleAttachment_Actor_OnBlueprintLoaded_m9BDF9F756C2220716C7B7FF09A2638EC2622ED6C (void);
// 0x000005E2 System.Void Obi.ObiParticleAttachment::Actor_OnPrepareStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnPrepareStep_m4ADFFF1468F95B8D80DFD601CDC5281CEEABB476 (void);
// 0x000005E3 System.Void Obi.ObiParticleAttachment::Actor_OnEndStep(Obi.ObiActor,System.Single)
extern void ObiParticleAttachment_Actor_OnEndStep_m17EEBDC31AE3E698DB7EDFDC55A91A46E8CE3B65 (void);
// 0x000005E4 System.Void Obi.ObiParticleAttachment::Bind()
extern void ObiParticleAttachment_Bind_m0F6831D2367C630B8A813F432781CEB86C372711 (void);
// 0x000005E5 System.Void Obi.ObiParticleAttachment::EnableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_EnableAttachment_mCD920F3627A2454FD1024B3F9B55ADFD3496A830 (void);
// 0x000005E6 System.Void Obi.ObiParticleAttachment::DisableAttachment(Obi.ObiParticleAttachment/AttachmentType)
extern void ObiParticleAttachment_DisableAttachment_mFAF630DD4461A1E43AF953FC4C3FC104B98E7AF5 (void);
// 0x000005E7 System.Void Obi.ObiParticleAttachment::UpdateAttachment()
extern void ObiParticleAttachment_UpdateAttachment_m7F47F2E7D6E3CA43436CDCE92AE6C07907EA38AD (void);
// 0x000005E8 System.Void Obi.ObiParticleAttachment::BreakDynamicAttachment(System.Single)
extern void ObiParticleAttachment_BreakDynamicAttachment_mA604FCC48F74F62022DDF42E48A721F28631456C (void);
// 0x000005E9 System.Void Obi.ObiParticleAttachment::.ctor()
extern void ObiParticleAttachment__ctor_m3FED1D679DE4AE1FE898CA20D26ED611DAD16557 (void);
// 0x000005EA System.Void Obi.ObiParticleDragger::OnEnable()
extern void ObiParticleDragger_OnEnable_m3A773530120C1DFA785C937A4CF70CE4E1769A34 (void);
// 0x000005EB System.Void Obi.ObiParticleDragger::OnDisable()
extern void ObiParticleDragger_OnDisable_m98EB30439859B2308D9E1CB5F391B42ED3534582 (void);
// 0x000005EC System.Void Obi.ObiParticleDragger::FixedUpdate()
extern void ObiParticleDragger_FixedUpdate_mCD0D51639FBBD4CE43E7AF99B63D2C8ACAA99A2D (void);
// 0x000005ED System.Void Obi.ObiParticleDragger::Picker_OnParticleDragged(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleDragged_m5AA44A3876EAF6D23656F2906CA70DC45EF0449B (void);
// 0x000005EE System.Void Obi.ObiParticleDragger::Picker_OnParticleReleased(Obi.ObiParticlePicker/ParticlePickEventArgs)
extern void ObiParticleDragger_Picker_OnParticleReleased_mCE191F876EB8019B642F2FB993EB80AEE4EE8412 (void);
// 0x000005EF System.Void Obi.ObiParticleDragger::.ctor()
extern void ObiParticleDragger__ctor_mFE08058F6A328DAE0BBFA840DF0EE70A4A59750E (void);
// 0x000005F0 System.Void Obi.ObiParticleGridDebugger::OnEnable()
extern void ObiParticleGridDebugger_OnEnable_m22D8D7E3A0ADF2C746157244B50A0865933C4EC3 (void);
// 0x000005F1 System.Void Obi.ObiParticleGridDebugger::OnDisable()
extern void ObiParticleGridDebugger_OnDisable_m99AFE2A426EEF0783EB1BD8EBEEF4E8F7BBF586B (void);
// 0x000005F2 System.Void Obi.ObiParticleGridDebugger::LateUpdate()
extern void ObiParticleGridDebugger_LateUpdate_m0688BD60CAC637CE3B6378C4A213F544ADAD5285 (void);
// 0x000005F3 System.Void Obi.ObiParticleGridDebugger::OnDrawGizmos()
extern void ObiParticleGridDebugger_OnDrawGizmos_m2C22C11A75725EC6F00CF9877B8EF38C17E4C397 (void);
// 0x000005F4 System.Void Obi.ObiParticleGridDebugger::.ctor()
extern void ObiParticleGridDebugger__ctor_m4C3CDFAAE5C907D9C952386717E4A8FFD3F2EFAE (void);
// 0x000005F5 System.Void Obi.ObiParticlePicker::Awake()
extern void ObiParticlePicker_Awake_m7DA63AB0BE2A8142527070A871933B66E02D7A4D (void);
// 0x000005F6 System.Void Obi.ObiParticlePicker::LateUpdate()
extern void ObiParticlePicker_LateUpdate_m361F701E0BBC294D1C8E01F3EDF0FEF602EB20B8 (void);
// 0x000005F7 System.Void Obi.ObiParticlePicker::.ctor()
extern void ObiParticlePicker__ctor_m2C886F2EBE8835F3AAD24DBFEFA5521847AE5215 (void);
// 0x000005F8 System.Void Obi.ObiParticlePicker/ParticlePickEventArgs::.ctor(System.Int32,UnityEngine.Vector3)
extern void ParticlePickEventArgs__ctor_m8E207C6EE600418DE884E613C5E3EBEDF9C09E7C (void);
// 0x000005F9 System.Void Obi.ObiParticlePicker/ParticlePickUnityEvent::.ctor()
extern void ParticlePickUnityEvent__ctor_m373BF8C05CFB614C704B30975C5D87E01CBFF86B (void);
// 0x000005FA System.Void Obi.ObiProfiler::Awake()
extern void ObiProfiler_Awake_mBAFD910D188643E946BE591D620A0C08E68DA4DD (void);
// 0x000005FB System.Void Obi.ObiProfiler::OnDestroy()
extern void ObiProfiler_OnDestroy_mD81D7084FB7A54922C8839632A0DE626D5302AAC (void);
// 0x000005FC System.Void Obi.ObiProfiler::OnEnable()
extern void ObiProfiler_OnEnable_m926D972796DF7CA64AEF295E2CB449E18C51CF13 (void);
// 0x000005FD System.Void Obi.ObiProfiler::OnDisable()
extern void ObiProfiler_OnDisable_mA82560267B0124673B8948B116500A7EDA5CF6FD (void);
// 0x000005FE System.Void Obi.ObiProfiler::EnableProfiler()
extern void ObiProfiler_EnableProfiler_mB24613A79E73316CF4BF538282767044F78496D5 (void);
// 0x000005FF System.Void Obi.ObiProfiler::DisableProfiler()
extern void ObiProfiler_DisableProfiler_m81DCFE0E380D85DD402AFD692F8E2D97F79DF5E2 (void);
// 0x00000600 System.Void Obi.ObiProfiler::BeginSample(System.String,System.Byte)
extern void ObiProfiler_BeginSample_m1AE47D3A47A61B7AF518318647274933D6B3DBFF (void);
// 0x00000601 System.Void Obi.ObiProfiler::EndSample()
extern void ObiProfiler_EndSample_mD326A70EB8BE0A7D5BC9C693C361E5EE155F8A75 (void);
// 0x00000602 System.Void Obi.ObiProfiler::UpdateProfilerInfo()
extern void ObiProfiler_UpdateProfilerInfo_mD4D343B23B55CAF9B4CFF4ED94F0A4EF4C45D109 (void);
// 0x00000603 System.Void Obi.ObiProfiler::OnGUI()
extern void ObiProfiler_OnGUI_m3806190AA0605A63FFF39A275ED86B7114117BB0 (void);
// 0x00000604 System.Void Obi.ObiProfiler::.ctor()
extern void ObiProfiler__ctor_m9F295F887DA4D4BE379F61D5409B3B4A2974F191 (void);
// 0x00000605 System.Void Obi.ObiStitcher::set_Actor1(Obi.ObiActor)
extern void ObiStitcher_set_Actor1_mC6598CF544A62DB96DABC5CAEA7BD4779E17EEAD (void);
// 0x00000606 Obi.ObiActor Obi.ObiStitcher::get_Actor1()
extern void ObiStitcher_get_Actor1_m356C884A3809D178ABBA1DEE765C9EDA6ECFDC60 (void);
// 0x00000607 System.Void Obi.ObiStitcher::set_Actor2(Obi.ObiActor)
extern void ObiStitcher_set_Actor2_mCF698C1E37A95BBFAA70C2C42BBDEB1BEF628B55 (void);
// 0x00000608 Obi.ObiActor Obi.ObiStitcher::get_Actor2()
extern void ObiStitcher_get_Actor2_mBA875D475DB2DA7069888EBF0E24F2F51ADD15A4 (void);
// 0x00000609 System.Int32 Obi.ObiStitcher::get_StitchCount()
extern void ObiStitcher_get_StitchCount_m68C67B275943FE86E6A1809AA71A7F29828C5D08 (void);
// 0x0000060A System.Collections.Generic.IEnumerable`1<Obi.ObiStitcher/Stitch> Obi.ObiStitcher::get_Stitches()
extern void ObiStitcher_get_Stitches_m8516F8D87128E8BBF4172365FEEB4758F5450A83 (void);
// 0x0000060B System.Void Obi.ObiStitcher::RegisterActor(Obi.ObiActor)
extern void ObiStitcher_RegisterActor_m724A0B01EA95508502CD44192272FDC35C2DD21B (void);
// 0x0000060C System.Void Obi.ObiStitcher::UnregisterActor(Obi.ObiActor)
extern void ObiStitcher_UnregisterActor_mF6B1662A4D928E4E65792DBD0D23198EA3C2562E (void);
// 0x0000060D System.Void Obi.ObiStitcher::OnEnable()
extern void ObiStitcher_OnEnable_m36A5797014051C2083B66D6A5576446A3344CF11 (void);
// 0x0000060E System.Void Obi.ObiStitcher::OnDisable()
extern void ObiStitcher_OnDisable_m1023484912F786E1F9741D48EC1DF3B64E76EBD6 (void);
// 0x0000060F System.Int32 Obi.ObiStitcher::AddStitch(System.Int32,System.Int32)
extern void ObiStitcher_AddStitch_mAD511CFD835FFC50F4ACC3326E071703E50495BB (void);
// 0x00000610 System.Void Obi.ObiStitcher::RemoveStitch(System.Int32)
extern void ObiStitcher_RemoveStitch_m18CD6F1050DACAAE2390B3F4C4E7BE53207503A7 (void);
// 0x00000611 System.Void Obi.ObiStitcher::Clear()
extern void ObiStitcher_Clear_m69516351DDF1114F9E6A43448E9F3FF2D13F8A16 (void);
// 0x00000612 System.Void Obi.ObiStitcher::Actor_OnBlueprintUnloaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintUnloaded_m72783F365B1F53FB341AB89987A93C5FF02EB371 (void);
// 0x00000613 System.Void Obi.ObiStitcher::Actor_OnBlueprintLoaded(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void ObiStitcher_Actor_OnBlueprintLoaded_m8E291D7C45A91930606F35AD7DA0FEE3F1EBB13A (void);
// 0x00000614 System.Void Obi.ObiStitcher::AddToSolver(Obi.ObiSolver)
extern void ObiStitcher_AddToSolver_m2D51C19A3C3A694337A6CA34C2AB14F1CB280633 (void);
// 0x00000615 System.Void Obi.ObiStitcher::RemoveFromSolver(Obi.ObiSolver)
extern void ObiStitcher_RemoveFromSolver_mF0ED8CCA4D5FAEA60D348F8AF912E12339662081 (void);
// 0x00000616 System.Void Obi.ObiStitcher::PushDataToSolver()
extern void ObiStitcher_PushDataToSolver_mB26ABC18EBC9D1EBEB808EA46345C1543C81370F (void);
// 0x00000617 System.Void Obi.ObiStitcher::.ctor()
extern void ObiStitcher__ctor_mE621FF9A178F97484204039FE8A0E3C685D3831D (void);
// 0x00000618 System.Void Obi.ObiStitcher/Stitch::.ctor(System.Int32,System.Int32)
extern void Stitch__ctor_m77F4E2459E65A256C836292F31CA58DC586A7E9F (void);
// 0x00000619 System.Void Obi.ObiUtils::DrawArrowGizmo(System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_DrawArrowGizmo_mF3CFC39F67B29D29480C7892940CA723BDDBAC17 (void);
// 0x0000061A System.Void Obi.ObiUtils::DebugDrawCross(UnityEngine.Vector3,System.Single,UnityEngine.Color)
extern void ObiUtils_DebugDrawCross_m45C81519A41F225746ACC8A364D5D095643F40C7 (void);
// 0x0000061B System.Void Obi.ObiUtils::Swap(T&,T&)
// 0x0000061C System.Void Obi.ObiUtils::Swap(T[],System.Int32,System.Int32)
// 0x0000061D System.Void Obi.ObiUtils::Swap(System.Collections.Generic.IList`1<T>,System.Int32,System.Int32)
// 0x0000061E System.Void Obi.ObiUtils::ShiftLeft(T[],System.Int32,System.Int32,System.Int32)
// 0x0000061F System.Void Obi.ObiUtils::ShiftRight(T[],System.Int32,System.Int32,System.Int32)
// 0x00000620 System.Boolean Obi.ObiUtils::AreValid(UnityEngine.Bounds)
extern void ObiUtils_AreValid_mAD1A13E1BF4949DD9F7602194B329532DF969179 (void);
// 0x00000621 UnityEngine.Bounds Obi.ObiUtils::Transform(UnityEngine.Bounds,UnityEngine.Matrix4x4)
extern void ObiUtils_Transform_m878DAF7F94B524911097CB33F9CF3AFE61C8092D (void);
// 0x00000622 System.Int32 Obi.ObiUtils::CountTrailingZeroes(System.Int32)
extern void ObiUtils_CountTrailingZeroes_mA673FEC05FCA8F37DFAD005DBD0636FE69A26192 (void);
// 0x00000623 System.Void Obi.ObiUtils::Add(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_Add_mED6C728427474910D9E3A107F7C89578D51E9EA7 (void);
// 0x00000624 System.Single Obi.ObiUtils::Remap(System.Single,System.Single,System.Single,System.Single,System.Single)
extern void ObiUtils_Remap_m7928236905391D25424A3810053A001388670968 (void);
// 0x00000625 System.Single Obi.ObiUtils::Mod(System.Single,System.Single)
extern void ObiUtils_Mod_mA7B12E7E20B64E93A98DE55FB942B330EAB868EA (void);
// 0x00000626 UnityEngine.Matrix4x4 Obi.ObiUtils::Add(UnityEngine.Matrix4x4,UnityEngine.Matrix4x4)
extern void ObiUtils_Add_m6B22531DB64E242CC083C4F1AC3E573D3FC8CDBA (void);
// 0x00000627 System.Single Obi.ObiUtils::FrobeniusNorm(UnityEngine.Matrix4x4)
extern void ObiUtils_FrobeniusNorm_mCE8806A43515214E6EA2739EF5B1609D8410662B (void);
// 0x00000628 UnityEngine.Matrix4x4 Obi.ObiUtils::ScalarMultiply(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_ScalarMultiply_m241041DF6C04AC91ED22CD7636DAF832C4CA5E5C (void);
// 0x00000629 UnityEngine.Vector3 Obi.ObiUtils::ProjectPointLine(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Boolean)
extern void ObiUtils_ProjectPointLine_m684F6C03FE7958BC77E6301F602C60B4E0D2ABF7 (void);
// 0x0000062A System.Boolean Obi.ObiUtils::LinePlaneIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiUtils_LinePlaneIntersection_mE1B2F9FBA229B00CC48AD42DB6A64A9557C074BA (void);
// 0x0000062B System.Single Obi.ObiUtils::RaySphereIntersection(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void ObiUtils_RaySphereIntersection_m7405A53E33BA95DEEFE619690364CB5602FB65EA (void);
// 0x0000062C System.Single Obi.ObiUtils::InvMassToMass(System.Single)
extern void ObiUtils_InvMassToMass_mDEADF97DA93B534C172F5C8D827408F3810D9088 (void);
// 0x0000062D System.Single Obi.ObiUtils::MassToInvMass(System.Single)
extern void ObiUtils_MassToInvMass_m8F9E7029E98E977A3CBBD522B526147B17A03772 (void);
// 0x0000062E System.Int32 Obi.ObiUtils::PureSign(System.Single)
extern void ObiUtils_PureSign_m1E9060FDD0112F738B37BD15C6F0DABF5C89E6FB (void);
// 0x0000062F System.Void Obi.ObiUtils::NearestPointOnTri(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_NearestPointOnTri_m4E23152DD3A4BF065FB6B8CE6777BDB1E8FBF1BE (void);
// 0x00000630 System.Single Obi.ObiUtils::TriangleArea(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_TriangleArea_mE27F37009D9B943502B2957C73B2A1B46973F7C2 (void);
// 0x00000631 System.Single Obi.ObiUtils::EllipsoidVolume(UnityEngine.Vector3)
extern void ObiUtils_EllipsoidVolume_mE344833B89A9825ACBC2472A6981B11BCE8A21F5 (void);
// 0x00000632 UnityEngine.Quaternion Obi.ObiUtils::RestDarboux(UnityEngine.Quaternion,UnityEngine.Quaternion)
extern void ObiUtils_RestDarboux_m33CE5F79C43DBB7E49E448044E502678377F4F53 (void);
// 0x00000633 System.Single Obi.ObiUtils::RestBendingConstraint(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern void ObiUtils_RestBendingConstraint_m7E1AAC42BED920DA069CCEAC27CE2672E9A8279A (void);
// 0x00000634 System.Collections.IEnumerable Obi.ObiUtils::BilateralInterleaved(System.Int32)
extern void ObiUtils_BilateralInterleaved_m0AD7642462AC6B8EB970124C25A5DBE379A042C3 (void);
// 0x00000635 System.Void Obi.ObiUtils::BarycentricCoordinates(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricCoordinates_m69ADA96835525D413DA970DB5F58D1A796B78C66 (void);
// 0x00000636 System.Void Obi.ObiUtils::BarycentricInterpolation(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Vector3&)
extern void ObiUtils_BarycentricInterpolation_mD6C458ECFDAF54343A22A4B9AD0EED2D914A1F57 (void);
// 0x00000637 System.Single Obi.ObiUtils::BarycentricInterpolation(System.Single,System.Single,System.Single,UnityEngine.Vector3)
extern void ObiUtils_BarycentricInterpolation_m2662CCFC3CC272FA40F515F441172976D695C481 (void);
// 0x00000638 System.Single Obi.ObiUtils::BarycentricExtrapolationScale(UnityEngine.Vector3)
extern void ObiUtils_BarycentricExtrapolationScale_mD546BF291A085DFB8421BF498430A56BE55E3E70 (void);
// 0x00000639 UnityEngine.Vector3[] Obi.ObiUtils::CalculateAngleWeightedNormals(UnityEngine.Vector3[],System.Int32[])
extern void ObiUtils_CalculateAngleWeightedNormals_mFDE12B5D04049A2A619A43079141E88463B76E85 (void);
// 0x0000063A System.Int32 Obi.ObiUtils::MakePhase(System.Int32,Obi.ObiUtils/ParticleFlags)
extern void ObiUtils_MakePhase_m7E62F413AD3E7A431DFC67BA06B4257021417A4E (void);
// 0x0000063B System.Int32 Obi.ObiUtils::GetGroupFromPhase(System.Int32)
extern void ObiUtils_GetGroupFromPhase_m1CDFCCEE425C8D288D514B3450EC7BEC27C3B1AB (void);
// 0x0000063C Obi.ObiUtils/ParticleFlags Obi.ObiUtils::GetFlagsFromPhase(System.Int32)
extern void ObiUtils_GetFlagsFromPhase_m4B508FE5CB15493204397F4AE79EF83C2613B22D (void);
// 0x0000063D System.Int32 Obi.ObiUtils::MakeFilter(System.Int32,System.Int32)
extern void ObiUtils_MakeFilter_m602A612ED46165F004BB5706BAA4EF62B2BAE4CB (void);
// 0x0000063E System.Int32 Obi.ObiUtils::GetCategoryFromFilter(System.Int32)
extern void ObiUtils_GetCategoryFromFilter_m17910EB5F69DE5F0D1BA9EE0EE2EC7AE81B7AB6B (void);
// 0x0000063F System.Int32 Obi.ObiUtils::GetMaskFromFilter(System.Int32)
extern void ObiUtils_GetMaskFromFilter_m257B2F84FBFC454B5369C96DCBE59D79F090778F (void);
// 0x00000640 System.Void Obi.ObiUtils::EigenSolve(UnityEngine.Matrix4x4,UnityEngine.Vector3&,UnityEngine.Matrix4x4&)
extern void ObiUtils_EigenSolve_m78B129A7153A92C83DC119D143580E211348C7A3 (void);
// 0x00000641 UnityEngine.Vector3 Obi.ObiUtils::unitOrthogonal(UnityEngine.Vector3)
extern void ObiUtils_unitOrthogonal_m32F7323AD50B11C90A771E97782078839D00FE10 (void);
// 0x00000642 UnityEngine.Vector3 Obi.ObiUtils::EigenVector(UnityEngine.Matrix4x4,System.Single)
extern void ObiUtils_EigenVector_m8C746635E6407BC6951F13A9132ADA38918CCA0B (void);
// 0x00000643 UnityEngine.Vector3 Obi.ObiUtils::EigenValues(UnityEngine.Matrix4x4)
extern void ObiUtils_EigenValues_mC5D4BA6FD1D9695FB3DD025B6CB28E534CDBE928 (void);
// 0x00000644 UnityEngine.Vector3 Obi.ObiUtils::GetPointCloudCentroid(System.Collections.Generic.List`1<UnityEngine.Vector3>)
extern void ObiUtils_GetPointCloudCentroid_m36E46B062321B39ABBCDCD8750D9D54367296230 (void);
// 0x00000645 System.Void Obi.ObiUtils::GetPointCloudAnisotropy(System.Collections.Generic.List`1<UnityEngine.Vector3>,System.Single,System.Single,UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.Quaternion&,UnityEngine.Vector3&)
extern void ObiUtils_GetPointCloudAnisotropy_mAD946D1DB7C96EFC405617E8D7AAF27FBC686FB1 (void);
// 0x00000646 System.Void Obi.ObiUtils::.cctor()
extern void ObiUtils__cctor_m785D5918F523383039FF034ED4BEC80433E0FAB6 (void);
// 0x00000647 System.Void Obi.ObiUtils/<BilateralInterleaved>d__38::.ctor(System.Int32)
extern void U3CBilateralInterleavedU3Ed__38__ctor_m636AFC81E56CD82E2A1AE8DAE6C190B020EE51AD (void);
// 0x00000648 System.Void Obi.ObiUtils/<BilateralInterleaved>d__38::System.IDisposable.Dispose()
extern void U3CBilateralInterleavedU3Ed__38_System_IDisposable_Dispose_m073BA4D7FD1C1FA46850B336E8A3ABA8C4165985 (void);
// 0x00000649 System.Boolean Obi.ObiUtils/<BilateralInterleaved>d__38::MoveNext()
extern void U3CBilateralInterleavedU3Ed__38_MoveNext_mFDF261F7B732B98CCD34D5C718D947908D024DBE (void);
// 0x0000064A System.Object Obi.ObiUtils/<BilateralInterleaved>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m209A3C2B372F3100AF57BA69AA6484694C6D861D (void);
// 0x0000064B System.Void Obi.ObiUtils/<BilateralInterleaved>d__38::System.Collections.IEnumerator.Reset()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_Reset_m75BA15CF4816AF7B31D0F2B7B2E2C901246F361A (void);
// 0x0000064C System.Object Obi.ObiUtils/<BilateralInterleaved>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_get_Current_m2D2F9CEEEC53323CD3E18E948B3132B37316504C (void);
// 0x0000064D System.Collections.Generic.IEnumerator`1<System.Object> Obi.ObiUtils/<BilateralInterleaved>d__38::System.Collections.Generic.IEnumerable<System.Object>.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_mC949AADB0472E40C59322D457C4D712A6550F7C7 (void);
// 0x0000064E System.Collections.IEnumerator Obi.ObiUtils/<BilateralInterleaved>d__38::System.Collections.IEnumerable.GetEnumerator()
extern void U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerable_GetEnumerator_m72DAC0FFCCEAB4F52B0E4EEE587674A0E9D9212E (void);
// 0x0000064F System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_mFFDD7EEEB73CB7550F46DCF22305E54C9B3B26D3 (void);
// 0x00000650 System.Void Obi.ObiVectorMath::Cross(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Cross_mF02960A84CD632E5A4DAA4C9358B0048D3574193 (void);
// 0x00000651 System.Void Obi.ObiVectorMath::Cross(System.Single,System.Single,System.Single,System.Single,System.Single,System.Single,System.Single&,System.Single&,System.Single&)
extern void ObiVectorMath_Cross_mE9D0BC9214974A5FEE80CE8F9D2C1E8559FC0FBE (void);
// 0x00000652 System.Void Obi.ObiVectorMath::Subtract(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3&)
extern void ObiVectorMath_Subtract_m2FD84948274EB3DDAA212F4FE0C75B11595704DE (void);
// 0x00000653 System.Void Obi.ObiVectorMath::BarycentricInterpolation(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3,System.Single,UnityEngine.Vector3&)
extern void ObiVectorMath_BarycentricInterpolation_mC150F53002C484C2C818980620D8EFCE37AE8BCA (void);
// 0x00000654 System.Void Obi.SetCategory::Awake()
extern void SetCategory_Awake_m935169A493ED4AB9747EC5F1FA1659E3CA1A13F7 (void);
// 0x00000655 System.Void Obi.SetCategory::OnDestroy()
extern void SetCategory_OnDestroy_mF5C3FA224A0A3304F35E5B040D5BB99108A52307 (void);
// 0x00000656 System.Void Obi.SetCategory::OnValidate()
extern void SetCategory_OnValidate_mBCFAC4034E7C6DB4DA31ABED278E44B4209E709A (void);
// 0x00000657 System.Void Obi.SetCategory::OnLoad(Obi.ObiActor,Obi.ObiActorBlueprint)
extern void SetCategory_OnLoad_mD070DB78A32168F801F7C7E5D26FED0D0E608D6B (void);
// 0x00000658 System.Void Obi.SetCategory::.ctor()
extern void SetCategory__ctor_m41A0F8EEF71EA38B13F698D252E76FDED327848D (void);
static Il2CppMethodPointer s_methodPointers[1624] = 
{
	ObiContactGrabber_get_grabbed_mEF424D81FBD0D9839F90266534D206B390F6F38F,
	ObiContactGrabber_Awake_m07C98A107436EF9EFCD1E5341A2C8796EDA3C58C,
	ObiContactGrabber_OnEnable_m0959DDCD6A5135831E5720DBD972C4EA53C9608C,
	ObiContactGrabber_OnDisable_m4B3259ADF9B791EEABC4BDFA9F96119392F3B3C5,
	ObiContactGrabber_Solver_OnCollision_m8B1B245C128EB18660570B462D7B66932F4A3085,
	ObiContactGrabber_UpdateParticleProperties_mD0CAA571E80537B84ABD4138622385BA55217A6B,
	ObiContactGrabber_GrabParticle_m3C970F6A4A69179D921F847759F8C1F52E1573FA,
	ObiContactGrabber_Grab_mCB77408F7BA33558DD0191B9D72140396324D733,
	ObiContactGrabber_Release_m0E714310CE0CF9DB94B9D88B0B582A03B457F291,
	ObiContactGrabber_FixedUpdate_m4E9292EC27FC0EC618D28A9F3F4F3797E3AA81E3,
	ObiContactGrabber__ctor_m24D31D38118E238C23651BF57F769774F8A06C04,
	GrabbedParticle__ctor_mA98E7DE9626624773F115AF2D24BE6CE04845413,
	GrabbedParticle_Equals_m1148839162B76102DED390B449BA60AD9DA9C270,
	GrabbedParticle_GetHashCode_m2020F55DEDD9FCE304D1309AD6BB38C882DCDC94,
	Oni_PinMemory_m208E84B3AB58A0BD994F116B056A727208F14F8C,
	Oni_UnpinMemory_mE894DF46823B95677702B2FD9E29004650D4B88F,
	SolverParameters__ctor_mCE0C179B96FED563AC3E2E669026A579BE013660,
	ConstraintParameters__ctor_m863469DE4E22DA6D6EA4B50AB79348BFEB13BEB1,
	ObiCloth_get_sourceBlueprint_m13D370945E6DD6D5837A57CA793F3197746365AD,
	ObiCloth_get_clothBlueprintBase_m16A8EC579D53A04F4A4576A5DB9692D18F910E86,
	ObiCloth_get_volumeConstraintsEnabled_m76C7309B3D7CE7C7A77BDC1E8C7F408ABED47EF6,
	ObiCloth_set_volumeConstraintsEnabled_mB365237EF02AE01A846963DE762CA1E801EA58AE,
	ObiCloth_get_compressionCompliance_m292433C738BF79F774AC2EF60F26566D66BE306A,
	ObiCloth_set_compressionCompliance_m5C8A760E0ADFA6C3E271151210CAE8D42A048FD9,
	ObiCloth_get_pressure_m7D48FEBE744CCF221A0DDB6D052616D6CBD8C6E0,
	ObiCloth_set_pressure_m553BA785F9FFF92F84E0DC27E326EE12623B950F,
	ObiCloth_get_tetherConstraintsEnabled_m290EA6F9508D712BF2AFDC6912E5C55E162A7F4A,
	ObiCloth_set_tetherConstraintsEnabled_mB844EAF783BCD82FEF172E9F62B9C024D222F0AD,
	ObiCloth_get_tetherCompliance_m21174A2978D30A95E942FD59A95650450AB38808,
	ObiCloth_set_tetherCompliance_mA53C03148A4ED6B7A12E4DA5BBC0DF3F0E3B2F44,
	ObiCloth_get_tetherScale_m75316B57FFA3CB02039DF6DF263C937E90692371,
	ObiCloth_set_tetherScale_m24D8EA690CF70CE4A4FF10C16D190CB890D43BB4,
	ObiCloth_get_clothBlueprint_m22D8CA7D61C5673611EA6B69A1E3EF73775F747D,
	ObiCloth_set_clothBlueprint_mF20722E74218D592AD1BD5B615A1AF313A5D7BB3,
	ObiCloth_OnValidate_m3B202D1B0868CEB000DF9CB98FFE2D774AD9292F,
	ObiCloth_SetupRuntimeConstraints_m9CF1D0CE9EBC404E1B3BDE39409A1442BE2FC12B,
	ObiCloth__ctor_m59769A9BE3F80E01A7E331C399FA4B87107157D8,
	NULL,
	ObiClothBase_get_oneSided_mD70AEC182F1158BD94AE795304C61F3FF624B22B,
	ObiClothBase_set_oneSided_m35A3CBFA50E8BE8C57D0C9FD76D3E09F354ABC45,
	ObiClothBase_get_selfCollisions_m3316717111097A8312FD5C906902D32B68709B3F,
	ObiClothBase_set_selfCollisions_m8A370E52621E4302CC5E5E690FA69B98E20F8507,
	ObiClothBase_get_distanceConstraintsEnabled_m4601252C285EB72E25A0CE60A0A36A24EFA65CD9,
	ObiClothBase_set_distanceConstraintsEnabled_mED671045E4ABE4D641713A4AE48C0EF46D8D1BF5,
	ObiClothBase_get_stretchingScale_m3728F960D7C92A00FBC730E4189845867C7FE488,
	ObiClothBase_set_stretchingScale_mFAD7D559898240B97A4102EC8707680EFC5555E2,
	ObiClothBase_get_stretchCompliance_mBD676958E67DA78D3C0E792D0756D5FBB787DC3E,
	ObiClothBase_set_stretchCompliance_mF14EFFC283A72116B88CDB0EDB50480B8CE4DA36,
	ObiClothBase_get_maxCompression_mFB1C3072962E4E09DC50E9B025AF9BB8879D4651,
	ObiClothBase_set_maxCompression_m143FFD92728A58676A95FD6E58B9110708BC0C62,
	ObiClothBase_get_bendConstraintsEnabled_m337B0D155CE52522C5B0E674FA4B127947D9D79C,
	ObiClothBase_set_bendConstraintsEnabled_mB5D4BF79F2749688D1FAC07F5638B69B801EEB6D,
	ObiClothBase_get_bendCompliance_m6A24CEECE44271FE829FDFFAC8727B7F81471646,
	ObiClothBase_set_bendCompliance_mF61722E51AA54B49A0859B909A533F940E9F3C84,
	ObiClothBase_get_maxBending_mE6CFA04ED2D8B3D677D3E91B350F3A0D3EC95AC7,
	ObiClothBase_set_maxBending_mF3317AB601BA6892AFBD3C59BC3B5A34BE2941AE,
	ObiClothBase_get_plasticYield_m994319A5C5E6F6948B692E49515541685C8DEBCE,
	ObiClothBase_set_plasticYield_mD095BACD7A69F02685492595BE21FCE3C027379C,
	ObiClothBase_get_plasticCreep_m6CCCC3185278C02F21103D0EE75965BA8211E984,
	ObiClothBase_set_plasticCreep_mC6A1D231779CAFBB2CD4ADDADC809CE906B1697F,
	ObiClothBase_get_aerodynamicsEnabled_m724A14548F69698E0BFFF0AAFC7D4B411ABF945D,
	ObiClothBase_set_aerodynamicsEnabled_mAB56C3284C124AD9FB53324C0213C15CBB3A9BAC,
	ObiClothBase_get_drag_m6EB40AA1CD217C1A045B7F971E43F1FFCA77DFEE,
	ObiClothBase_set_drag_m3A94A2857A88C693D44C9A98EDA52D84576D73FA,
	ObiClothBase_get_lift_mA8E8E40DB7ED5E81CCA5708BE1DB7E4DD659FC2C,
	ObiClothBase_set_lift_mD9F8E4B6C202CFEAE78037A4D94A9780996460CC,
	ObiClothBase_get_usesCustomExternalForces_m753BC6385F9CD3EE74C1C1A6310E88B2B3C8CF5B,
	ObiClothBase_LoadBlueprint_m3E9338E2C81996AF06788691FFFD6C8CD5408ED9,
	ObiClothBase_UnloadBlueprint_mB0BF0CCDC63B0BE15DDED6E289496145E3845945,
	ObiClothBase_UpdateDeformableTriangles_m3B671D3EA48D5EBD4912BA193E66E4CA3002F4B2,
	ObiClothBase__ctor_mFBFF1E5D726CB95C999773DDC4612FCA25CFE720,
	ObiSkinnedCloth_get_sourceBlueprint_mEA27126BCC672129A3B7766332D29093D6175A0C,
	ObiSkinnedCloth_get_clothBlueprintBase_m1529D289F4BA3FE8764D347287236A95554CDD7C,
	ObiSkinnedCloth_get_tetherConstraintsEnabled_mD12EB38B8977C736CE07460F124A196C36F5DB14,
	ObiSkinnedCloth_set_tetherConstraintsEnabled_m3CCDAA09DC9CC778AC07FE673FD77F317B4B501F,
	ObiSkinnedCloth_get_tetherCompliance_mD2021535E9B9A78E884EDB0946714D91F3187568,
	ObiSkinnedCloth_set_tetherCompliance_mCCB51CFECD12168274126EFACA6AAFEFC7A8AFDC,
	ObiSkinnedCloth_get_tetherScale_mFB6726B6BBE3DE8FABC0B2B7F3AADFAED14D2114,
	ObiSkinnedCloth_set_tetherScale_mFE25D56BB1ED73746407D1D6D6692EE85B663E94,
	ObiSkinnedCloth_get_skinnedClothBlueprint_mA690E5883D4C2B37BAB96C95503A3EE6637982B0,
	ObiSkinnedCloth_set_skinnedClothBlueprint_m5195445F8A791353581BED6D6E9EDA7A13E2665A,
	ObiSkinnedCloth_LoadBlueprint_mAC4B5A67AB01888FEEC4332E55A189DE06E87AB7,
	ObiSkinnedCloth_OnValidate_m0B636B2CB110096F30FBA06ACB8E969EABC5DE0A,
	ObiSkinnedCloth_SetupRuntimeConstraints_m091CE68392FD98A827C1C59C4DCA50561E004829,
	ObiSkinnedCloth_OnEnable_m595FAA6BF4FEC864A174F2A575F973DC0D381B6A,
	ObiSkinnedCloth_OnDisable_m7F2D77F05ACBCFB4AC6DA60D063FE3B81C3FD95B,
	ObiSkinnedCloth_CreateBakedMeshIfNeeded_m929E3794A473BD8A2B921C78A62080A4611D690C,
	ObiSkinnedCloth_SetTargetSkin_mF7DFD21C1B3AB7F6DA8E89C7F62F420DDFDCBFC0,
	ObiSkinnedCloth_BeginStep_m814BE1E13A2F650E6C27469A2B40424E12C5DE03,
	ObiSkinnedCloth__ctor_m057BEF1BBD87BD492324A35C2AA3357D0EE2B9A4,
	ObiSkinnedCloth__cctor_m39C48061A17FE019D7650FFF7E64535F32706BA3,
	ObiTearableCloth_get_sourceBlueprint_m6CB5BE13DF36F76CEEFE42C7C4C8BDF5BC4AD117,
	ObiTearableCloth_get_clothBlueprintBase_m595CBA788EF9AC2D4859366FD4CF1BB4D507A355,
	ObiTearableCloth_get_clothBlueprint_m193D8050E3EB29ECEBCCC13800A4A65A3E74DEF8,
	ObiTearableCloth_set_clothBlueprint_m00BF6A7834CEF4D3CD1DB79859D2B630D31B6EA1,
	ObiTearableCloth_add_OnClothTorn_mEC85AE271BAB9CB16C6BC740BE02F2CCD1389C60,
	ObiTearableCloth_remove_OnClothTorn_m17F5D5C94C1B7F8324CD95EFF0B3F1430A7D3412,
	ObiTearableCloth_LoadBlueprint_m01F84142D7B5FB8F36CAD4764F8E6550F4050D03,
	ObiTearableCloth_UnloadBlueprint_m1D1CB5E27C2344920DA9E8C18AF56A9305F540C6,
	ObiTearableCloth_SetupRuntimeConstraints_m6949CC983A52F1087A8C712DE0D90DC56DCAE2A4,
	ObiTearableCloth_OnValidate_m0317CACD2EFF69F63731391B3C415EBC2EEF2310,
	ObiTearableCloth_Substep_m1D8B9E930B2F6EA0EE13FE954E97DF40E71A17AB,
	ObiTearableCloth_ApplyTearing_mACF70C409F46C950A27B82B444F069AC300735B7,
	ObiTearableCloth_Tear_m289AE8187944FCF0C03BF505839AA7CE8B8914C6,
	ObiTearableCloth_TopologySplitAttempt_m4A20D931C00BBE2E52327AC19AE5B67F6E7983DA,
	ObiTearableCloth_SplitParticle_mD2C7D1B54B81088B454D028CA36258217934B276,
	ObiTearableCloth_WeakenCutPoint_m96AA5929E23DDA50FD537F34C063075229677E83,
	ObiTearableCloth_ClassifyFaces_mA5E6121C7F2E7899FBF785747F3A76F7F28440C7,
	ObiTearableCloth_SplitTopologyAtVertex_m8D9CF0809D8C17C08D9E62602F91B2A6A48F88C4,
	ObiTearableCloth_UpdateTornDistanceConstraints_m33C4A08366B00828983FA36C27B1B5889224A4A0,
	ObiTearableCloth_UpdateTornBendConstraints_m90DEF8C2E331F3E980AD2011D6557CB4D893FEBB,
	ObiTearableCloth_UpdateDeformableTriangles_m295EE9DFB4BB9EA8BB2FCD068BDC3C8355D503FB,
	ObiTearableCloth_OnDrawGizmosSelected_mDE475E90A8FB156D41ABE5EEED44802420EDE9E8,
	ObiTearableCloth_Update_m4D345AF05C968480C4DA0AEAB8692DA006AD2F36,
	ObiTearableCloth__ctor_mDA99DC00FDF966DA559BE6F1FD3FB84E41B25B15,
	ObiTearableCloth__cctor_mD504737A0AAECBAD232B27500F7C483E3345F930,
	ClothTornCallback__ctor_mE57B91D6E263EA221779102651926B607AD29B76,
	ClothTornCallback_Invoke_mAC61E111032A0591234652E701263A8EF39427F3,
	ClothTornCallback_BeginInvoke_mFB0788D7797D354E91A6283F4881539D4AF7609F,
	ClothTornCallback_EndInvoke_m109D873CDD373C69D32A0CC8FE4931B030581F85,
	ObiClothTornEventArgs__ctor_m709EC7E4FD2C7B0270B4D796A041BD58600E837E,
	U3CU3Ec__cctor_mDAC97FBBE782103672E33A1391E0ED9F7378FA58,
	U3CU3Ec__ctor_m338AE04294412318948CB35D87CB59FF12519F2B,
	U3CU3Ec_U3CApplyTearingU3Eb__24_0_m44431CF342F4B5F112B7814C9FED0466FAB2BCB7,
	ObiClothBlueprint_get_usesTethers_m36D5FA3046CEFA83F43D10DBCCEE291D95504E31,
	ObiClothBlueprint_Initialize_mC821201D6C73FD74172917F4B47D86E247436637,
	ObiClothBlueprint_CreateDistanceConstraints_m2BD2777172DB698116B46D988858D97A785DB7E5,
	ObiClothBlueprint_CreateAerodynamicConstraints_mA4996455BBEC0570529FF2ADE79B56A7EFA29FC9,
	ObiClothBlueprint_CreateBendingConstraints_m7D28254A6C5BDF54AAEA2C1324D5A702CF2982D6,
	ObiClothBlueprint_CreateVolumeConstraints_mCAD1E4ED571A93814AF8AD17D926B0065C30E6E1,
	ObiClothBlueprint_ClearTethers_mC2770ED44BD0538B446CBE7E0BED8C06E723FA62,
	ObiClothBlueprint_GenerateIslands_mC84BD0A563E23FC794DD5D874217965A852FFEB0,
	ObiClothBlueprint_GenerateTethers_m19035D0B19AA667D7F72DFCB325808F08A9F5379,
	ObiClothBlueprint_GenerateTethersForIsland_mB667EEE465392D42D60EDAA37251B1D0B7347C00,
	ObiClothBlueprint__ctor_m330710D1D475B3F870467803C92D8EB11FFCD945,
	U3CInitializeU3Ed__2__ctor_m03020FC6FD2B9D269917BB6F7C9C0DCDDD6E2561,
	U3CInitializeU3Ed__2_System_IDisposable_Dispose_mB05A3DA48C46FB54386164416A68FF35C8BA81E4,
	U3CInitializeU3Ed__2_MoveNext_m302A69E08AE7BAFFD3224ABE919F80A158AAFF51,
	U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEE086F200365532FA29B83AD2B3595ADFA22EE6F,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m58F645AA9A0B4F194919051D97FF764435010ACE,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m03BD8D7ED8FEA24F9BF18D57D1C26C463A2C80E6,
	U3CCreateDistanceConstraintsU3Ed__3__ctor_m2165A7905B75F686B6EB79C8360F82705A43F002,
	U3CCreateDistanceConstraintsU3Ed__3_System_IDisposable_Dispose_m2EA9DF5AFFD56BB4A01414F8033C8552E0A17F63,
	U3CCreateDistanceConstraintsU3Ed__3_MoveNext_mEFD23D0412B26FF3359D8B28631C6776A544E5CC,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8FFABC8D386190AD1252F8010CA37A1129988477,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m9A9A3D28EE82C1C9CC440756268313182A28769C,
	U3CCreateDistanceConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_mE7958E068EB74DFDA3BA778B8905AE86BF1F5D01,
	U3CCreateAerodynamicConstraintsU3Ed__4__ctor_m31B15C5B17B8C34F9C4714EAA95B881AC32C8504,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_IDisposable_Dispose_mE2C279ABF51A608163FDBBE4930C00D8A661F3FF,
	U3CCreateAerodynamicConstraintsU3Ed__4_MoveNext_m05541FD655F08F60A1A857669DFAEFE0B2D3CBFF,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDF2E2D9440AA0D26FAD832543DE1FBBAF22D475E,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_Reset_m8DB140EE89987CF9D9975939A2B10C4D5771B8D7,
	U3CCreateAerodynamicConstraintsU3Ed__4_System_Collections_IEnumerator_get_Current_m64BFC0A639CF8F425C2E6E6EAC54E60AE3B8ACD1,
	U3CCreateBendingConstraintsU3Ed__5__ctor_m7A3788831C438FF52B984C65426E7055DCEA2F20,
	U3CCreateBendingConstraintsU3Ed__5_System_IDisposable_Dispose_mABA1606677C79BDD2FD9556D6963A1CF4C71299A,
	U3CCreateBendingConstraintsU3Ed__5_MoveNext_m19B7E25316061D964D46A367D68C16625B7778BC,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB6DA62A242C5BB4F7999881ACA8384398B2E643D,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_Reset_m99008933F0F99D282B7DD6010E088384DE087F4F,
	U3CCreateBendingConstraintsU3Ed__5_System_Collections_IEnumerator_get_Current_m630DC74A3B6FCD1C00ACE9CDB5D9C339BE81CBF3,
	U3CCreateVolumeConstraintsU3Ed__6__ctor_m726B8020E13DD7A4EBE916CFFD8292CA4472C3EA,
	U3CCreateVolumeConstraintsU3Ed__6_System_IDisposable_Dispose_m1E0A92935E3227A8A889157CD421FEA0379E0E39,
	U3CCreateVolumeConstraintsU3Ed__6_MoveNext_m801293BF179BA8640C7868766412FA4A5810AFBC,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE255CE246691B00B58B7C27281A458B24CD10D1,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_Reset_m5F9AAB4548AF92F079FF16DEB9DCCFEEB1FB9817,
	U3CCreateVolumeConstraintsU3Ed__6_System_Collections_IEnumerator_get_Current_m779B3E79795A7A851B5F2080D095ADC06C5A6F3B,
	U3CU3Ec__DisplayClass10_0__ctor_mCA4BB7EFB4202D511569ED75001DA257335DC03F,
	U3CU3Ec__DisplayClass10_0_U3CGenerateTethersForIslandU3Eb__0_m228EA147201D2F54ADE2A350E7D361AB7A692DAC,
	U3CU3Ec__cctor_m81E0D9C2C844297DC2CC6D386D6D18574FE7A25B,
	U3CU3Ec__ctor_m44DBDA334931038409B37CCDEE60D49A314A030F,
	U3CU3Ec_U3CGenerateTethersForIslandU3Eb__10_1_m9204F6299DB15BA2066909BE3A5992FF811516FD,
	ObiClothBlueprintBase_get_Topology_mE35630E1A8F1D4FF4FDFBD1B55B0FD135296FD81,
	ObiClothBlueprintBase_SwapWithFirstInactiveParticle_m7487298098E3AB9C23B657865FF874134445D407,
	ObiClothBlueprintBase_GenerateDeformableTriangles_m57C68962E620B96908388CAC9A952DCA3D3B11C4,
	ObiClothBlueprintBase_CreateSimplices_m4B89261638E31CE9D9046BE2D1C3DB23D31671BF,
	ObiClothBlueprintBase__ctor_m448379F1F34C2F69E0B2EA0D90177AD5B683682B,
	U3CGenerateDeformableTrianglesU3Ed__8__ctor_m8139DC43596BD55214B1F9CBF79B61334C4FB303,
	U3CGenerateDeformableTrianglesU3Ed__8_System_IDisposable_Dispose_m74C4415080168350829FE8A5DD9AF9A5405F3A27,
	U3CGenerateDeformableTrianglesU3Ed__8_MoveNext_mF5112455130FD61016C1CBF03E44AC08829DDEF6,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5408ABF70D4F1A55E05421689FBAB42C98EE617C,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_Reset_mA8110BD6BE76597A39B0A88E3711CC5489FEC896,
	U3CGenerateDeformableTrianglesU3Ed__8_System_Collections_IEnumerator_get_Current_mBB5E21B2BE3291406AE669215C5192ECAFDA9059,
	U3CCreateSimplicesU3Ed__9__ctor_m6AF7C34D51F0C9027A94D895000B742F12D6D2DE,
	U3CCreateSimplicesU3Ed__9_System_IDisposable_Dispose_mD8BA44B90AAC0DE11F3A0431ED1ACE334A04BBC5,
	U3CCreateSimplicesU3Ed__9_MoveNext_mC9D009E251058DE1A68616F58685DF9095E69957,
	U3CCreateSimplicesU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5ADA0BBC36D0A236F6D0091810A0FE9E92A1370A,
	U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_Reset_mD668CF36AEB83620A2FE2A8155CA813A29421AAF,
	U3CCreateSimplicesU3Ed__9_System_Collections_IEnumerator_get_Current_m46A43033D3499E23FCD045DE9D84E32018AE2A31,
	ObiSkinnedClothBlueprint_get_usesTethers_mFB6A1F8DB9337CD20D7A3A45EE2F6A4378095FAF,
	ObiSkinnedClothBlueprint_Initialize_m9C44FD17E8A31CB5BC36A27812FBB9B88EB4D271,
	ObiSkinnedClothBlueprint_CreateSkinConstraints_m301723D10057502260D046132DEE8E69A67B435B,
	ObiSkinnedClothBlueprint__ctor_m057C902844020886240D9E8330F28EE4114EB6BE,
	U3CInitializeU3Ed__2__ctor_m74C4F5F3BD150B334802008C977E713FBD745FB5,
	U3CInitializeU3Ed__2_System_IDisposable_Dispose_m5D2AC4D1DBA6F2CD98525165EA40923897FC26AD,
	U3CInitializeU3Ed__2_MoveNext_m1C75D9114926E161FCE046178551F95199924AC5,
	U3CInitializeU3Ed__2_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m060C078F5C58E548AE053F47B2665FA769D33F47,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_Reset_m1DB56CACC6E4B0C01726F74F013589216D1735DE,
	U3CInitializeU3Ed__2_System_Collections_IEnumerator_get_Current_m18B2AB3F28D9FDDBCE4C5942EC35F5369CC76ED9,
	U3CCreateSkinConstraintsU3Ed__3__ctor_mB82120AAC82D7FF18CAB6F1AE91B332B82248CDA,
	U3CCreateSkinConstraintsU3Ed__3_System_IDisposable_Dispose_mD6F28A76C96F00D36EDEAC9C4BC802EE5ECD61AB,
	U3CCreateSkinConstraintsU3Ed__3_MoveNext_mD55B1CB130A747A517D392E7732C7D61E3FDF5DD,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m30F2FCAEF12F2DF55352FFC3801D6B17C37AF429,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_Reset_m20863CA27EA5B0FE586DF972B4E33F93B44A94BB,
	U3CCreateSkinConstraintsU3Ed__3_System_Collections_IEnumerator_get_Current_m19EBD8D91F9C3AC56ABF53DCDA01F272833C0A39,
	ObiTearableClothBlueprint_get_PooledParticles_m60E85C577CBB70DB1188ACADA0039ECA86E2A7B4,
	ObiTearableClothBlueprint_get_usesTethers_mB1DAB44D3B6E31310FDA017A73E952C65EA3617B,
	ObiTearableClothBlueprint_Initialize_mAB87D7E2D2169C9E3A687285521300D251C5F9AE,
	ObiTearableClothBlueprint_CreateDistanceConstraints_m210056ABF39610D9AB58CD6EE2BB63B996BCB5EC,
	ObiTearableClothBlueprint_CreateInitialDistanceConstraints_m6454656AD1D745A2F16F5096035092B3FE4F0EEA,
	ObiTearableClothBlueprint_CreatePooledDistanceConstraints_m2CF68D6EB12EC0288BF7CF8114ED8A76475DF6B2,
	ObiTearableClothBlueprint__ctor_mA00BAF480E94328209D56224AF9ED316629EFF67,
	U3CInitializeU3Ed__8__ctor_mC2EAE7F3E298D30D7B40374ACC5F94CFE70748A0,
	U3CInitializeU3Ed__8_System_IDisposable_Dispose_m7EB8A87FD770A0114AC120446F9C7DEA7985EC63,
	U3CInitializeU3Ed__8_MoveNext_m8B656167A50E5CEECCFBBF56B8DF0D5392E86206,
	U3CInitializeU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA8CDEC853C25012FD10F0BC2D855DED5869E4C2A,
	U3CInitializeU3Ed__8_System_Collections_IEnumerator_Reset_m6D8AC93EB36AA485AC8BB64E73247E39A9DE163C,
	U3CInitializeU3Ed__8_System_Collections_IEnumerator_get_Current_m772F66F32F759B17DCE6424C75A648CEDADAF472,
	U3CCreateDistanceConstraintsU3Ed__9__ctor_m10D767164F94A8F05622946A56AC89F60794383B,
	U3CCreateDistanceConstraintsU3Ed__9_System_IDisposable_Dispose_m3BB519B14C38C38594DD385A14E70752FD80E054,
	U3CCreateDistanceConstraintsU3Ed__9_MoveNext_m7AF80F3E34CC978EAFCBD9520147C772E0FB6FFA,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF05CDB7B6EB27DCFF38D57CFD55F7939B395E67,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_Reset_mCB067718FF90B57B51B005763022474362D159A4,
	U3CCreateDistanceConstraintsU3Ed__9_System_Collections_IEnumerator_get_Current_m069492497AABB68DD787B9B9485AAE79C7E7C62C,
	U3CCreateInitialDistanceConstraintsU3Ed__10__ctor_m860DEC39A10B619CB02D9A03F2DF1CC314CDD120,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_IDisposable_Dispose_mFD42077906845A3AC141AB186973761625D388E7,
	U3CCreateInitialDistanceConstraintsU3Ed__10_MoveNext_m82AC28B0879B660B4DD72663041CF314A0BDF1E5,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m720DB2F410849D5B3237A1C26AE0C4AB8445265C,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_Reset_mFFCDAB2F7A98ED448B887DD0325488F2C7127922,
	U3CCreateInitialDistanceConstraintsU3Ed__10_System_Collections_IEnumerator_get_Current_mE9E5CEEF3F29FC86CEFACCA48DEAF6F99628CB4B,
	U3CCreatePooledDistanceConstraintsU3Ed__11__ctor_mE5011620ABBE52261DCDF900C25BC60D46190AA2,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_IDisposable_Dispose_m24E179D2396555CBC7931B006DCF382ED737882B,
	U3CCreatePooledDistanceConstraintsU3Ed__11_MoveNext_mD5D31320D8EC8575810E179C3C84A3A3A3B13FB5,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2025D23C395F363B5CAD10BF8CF249A045569B39,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_Reset_mA1DEFAA6A2DAE90CEA3649CFAD079C301E945D7E,
	U3CCreatePooledDistanceConstraintsU3Ed__11_System_Collections_IEnumerator_get_Current_mD019F838649C96EFDFD97DB933FA80B0EDB22ED8,
	HalfEdgeMesh_get_ContainsData_m67132D1DA820B0B33491E4AC1D297E286AD89EF8,
	HalfEdgeMesh_get_closed_m720512F243C8837E8D88E9968BF2CD3821DF1537,
	HalfEdgeMesh_get_area_m1C5411A5FB6217ABBEE0BB0D797100389BEA5B19,
	HalfEdgeMesh_get_volume_m66F3CFA6E325A41041FF97835648CCE49E060118,
	HalfEdgeMesh__ctor_m2409753E6C585A36D56C0672DEC1E55A69F6D398,
	HalfEdgeMesh__ctor_mD56BB0A58E07D152D1DCC51258DFB64D49004AC9,
	HalfEdgeMesh_Generate_mAF8BDD70FF4421E7B85F14008044D837B6D4F7A6,
	HalfEdgeMesh_CalculateRestNormals_m06A0D9D64077F707BC0B122D9F9CC8AAAB524D4E,
	HalfEdgeMesh_CalculateRestOrientations_m441FCEE91D0C56AF7794397B519BF954EE3EDA88,
	HalfEdgeMesh_SwapVertices_m8D108B9B2095522E59905652B47AFA3D67C891F5,
	HalfEdgeMesh_GetHalfEdgeStartVertex_m1309E7CFD17A07D6C157110091E16E8292D8AC5F,
	HalfEdgeMesh_GetFaceArea_m1D79061E5B7045231801CFA296B50DD764BFB9A1,
	HalfEdgeMesh_GetNeighbourVerticesEnumerator_m427D20E191E32FD38225DF431B627FC8A3B19AEE,
	HalfEdgeMesh_GetNeighbourEdgesEnumerator_mAF5EEF10069CFFA9448C6F6D1623CFCCA46AA929,
	HalfEdgeMesh_GetNeighbourFacesEnumerator_mB0158BBB50C411485D5C26138FF73FD4810552F4,
	HalfEdgeMesh_GetEdgeList_m24EE36AC0B04D4371BA908FC9B54BD96800A75BC,
	HalfEdgeMesh_IsSplit_m14E0D14E21DACB497E5F0DBF78029E3C58707795,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31__ctor_m8156A28051972177616DED3CA97F451E45DC7185,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_IDisposable_Dispose_m56584522A7EF9CD15577DCCE9BE90084A16CD5A0,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_MoveNext_m02FE74929ECBFFAEE5E86605EC2D267D7C3965D3,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_VertexU3E_get_Current_m27FD1A775C4A3F297B2C58E2092662AF69DC9148,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_Reset_m55F703C661A4F8DFB933F0551AFF04714664738D,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerator_get_Current_m8081115EDDE5A8D021B2AA5CBCAFE065EF0BA8FC,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_VertexU3E_GetEnumerator_mE6B4FD37BF23C1F08AAD8FD8B9A5BF8A6326F21D,
	U3CGetNeighbourVerticesEnumeratorU3Ed__31_System_Collections_IEnumerable_GetEnumerator_mE276303D862883120B0D06103592A23968708ED8,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32__ctor_m08CE33742096F1FDCCE51C2263BAE629222DFCE3,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_IDisposable_Dispose_m245661F2D84CE211EA54BB0B31B23356F18EC04C,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_MoveNext_m1B1E0CAEA67208793D9364DCA59781EF81C72FD2,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_HalfEdgeU3E_get_Current_m841F1A67A81AF1D1DB8CC7819124DF002CC7CC74,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_Reset_m6AF7B85B8F67F4D7CDF328F73FB9CEEC9E732040,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerator_get_Current_m2EB25C26C43A80B57D481AEA9F2E312B23627894,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_HalfEdgeU3E_GetEnumerator_mBCAC9207E22122BBA14009BAB3F1CE81B49ECD51,
	U3CGetNeighbourEdgesEnumeratorU3Ed__32_System_Collections_IEnumerable_GetEnumerator_m1A70C81B7E5B8F71ED2B8A19814DA990FD781B16,
	U3CGetNeighbourFacesEnumeratorU3Ed__33__ctor_mDE9CCAE52922A0A8B7CFA8EB1BAAAA1E1ED2CA3F,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_IDisposable_Dispose_m54C2538A85F8AAD146B2AB888A05F5F4BCA70994,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_MoveNext_mED91E0FBF0F8E0F417A1906E31CECB7ADC443773,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumeratorU3CObi_HalfEdgeMesh_FaceU3E_get_Current_mFD88B3D02BA9FD24F49609448D6354DA0E2FD9FB,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_Reset_mB07B17F0F16CCF05606C34FDFCFBCBA9FDB398F0,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerator_get_Current_m7B5E8F3270ADED930E203C7F1DB3236D2E172478,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_Generic_IEnumerableU3CObi_HalfEdgeMesh_FaceU3E_GetEnumerator_mF33CEE5DFCEB560159726F7CE362A1E8AADA3982,
	U3CGetNeighbourFacesEnumeratorU3Ed__33_System_Collections_IEnumerable_GetEnumerator_m07C6A65A9082750EDFCDB8D0EFC4DFDEB45826DD,
	ObiTriangleSkinMap_set_master_m5E115D7718A8617482E1701CA18D20E3BE30FB6E,
	ObiTriangleSkinMap_get_master_mF49863E288BA9C99D1CEFDC9DF99F9A2623D82A7,
	ObiTriangleSkinMap_set_slave_mF120662565C78B207B2EE4AC41EB86AC7A4BF0D5,
	ObiTriangleSkinMap_get_slave_mE8F13D556281070BEB26DFEF4969665CC4B78395,
	ObiTriangleSkinMap_OnEnable_mF3970C48AE8A8C6F700F24F1878D1EBFC5CDE404,
	ObiTriangleSkinMap_Clear_mF0B3406C85EC6D32B3CB0EAA0F802C3BAC2F4FA2,
	ObiTriangleSkinMap_ValidateMasterChannels_m6954F42A5BD3621ABBF98CBC2E4D90F6B7DA3E02,
	ObiTriangleSkinMap_ValidateSlaveChannels_mBFD5DED1F3D3218BFA963001A324C21146C6C2D4,
	ObiTriangleSkinMap_CopyChannel_m2A08612D0ADBC3326473C33CF5E19029E8859B8F,
	ObiTriangleSkinMap_FillChannel_m8453C63E562D501363823D6162036F20B75B5651,
	ObiTriangleSkinMap_ClearChannel_m62E892A9892F3528AD225CCAEF08C2D368A59C62,
	ObiTriangleSkinMap_BindToFace_m904B69AD7BE3769CC10E5A400CC614EEA7AE91A6,
	ObiTriangleSkinMap_GetBarycentricError_mA4BF58342EFEDF9C71615E3DC9E043CBD77A1779,
	ObiTriangleSkinMap_GetFaceMappingError_m83D60544D64792010A0362A30A8E989747EA76AF,
	ObiTriangleSkinMap_FindSkinBarycentricCoords_m7881E5CB808F7D7B82B1B4F86252C272F5922557,
	ObiTriangleSkinMap_Bind_m62980026E2CF756492E6BDF0FF5F32D462169522,
	ObiTriangleSkinMap__ctor_mF084C886D75F78D14C9CD798F2959ADC8964EE86,
	MasterFace_CacheBarycentricData_m4CE92EC9AB2150F5D057B915E4A3E8C370EDAF28,
	MasterFace_BarycentricCoords_m8A2510BFBFE31ABD8623CF7B03E2F520B9934717,
	MasterFace__ctor_m5F8FF47924C0460E115B7CB1E1FEBB99CDC1FED0,
	SkinTransform__ctor_mABDA5BC834E71D0637F68EC987DE11419C85364A,
	SkinTransform__ctor_m04F80634057138EDFCD0E9AF5883073EEE5D2D90,
	SkinTransform_Apply_m60D31FA6B0918902E7F39C888B680AA41022BAE2,
	SkinTransform_GetMatrix4X4_mD5A708E729C95FE3D38DD26D16C7E7BE94421D1B,
	SkinTransform_Reset_m3FD007CF70C5C4ED52952EF1CA7F95B333BD6C1D,
	BarycentricPoint_get_zero_mD4FD59E5FE3F7B61E1D7EF67D1F641B4B54FA87A,
	BarycentricPoint__ctor_m3EEE56C6E6CB000D8224ADF9173123AC1FF91836,
	SlaveVertex_get_empty_m936ADF657206971C4F23C7E5B3621324BA04F263,
	SlaveVertex_get_isEmpty_m63953EAD27B6F66BEA7A28B77BC2253CA453101E,
	SlaveVertex__ctor_m12A785158C13405D27F9C736FAE1350516EDF56C,
	U3CBindU3Ed__31__ctor_m345D3284C09C114F18927E7F8EDF672BF64F6109,
	U3CBindU3Ed__31_System_IDisposable_Dispose_mEC8D340299496DC39BF57BBA1F907DBE3DB91E94,
	U3CBindU3Ed__31_MoveNext_m25AD32F7C23158AD745F537AE6FADF9C324F47F7,
	U3CBindU3Ed__31_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m09716DC077042BA4DB1A735CDDACE6F3A85F0113,
	U3CBindU3Ed__31_System_Collections_IEnumerator_Reset_m2886755E5DB4DCB7C2D3A1DDB54E7890E5D3255A,
	U3CBindU3Ed__31_System_Collections_IEnumerator_get_Current_mD9523C5684A83C45D0E592E81502428E1B74A8EA,
	ObiClothProxy_set_master_mA10B6136180C3F8883EB77467D05296EF6958B39,
	ObiClothProxy_get_master_m9097A98051AA22E455E9ECDC1D17A7D908722854,
	ObiClothProxy_OnEnable_m2A0AEAB28A75A63571D506B4A422D435D1B8435E,
	ObiClothProxy_OnDisable_mEED97A31EA1FDE139CFBC479EEF4E14A5FD2B6F6,
	ObiClothProxy_GetSlaveMeshIfNeeded_mA2213C1F12E371A1169B5D2FBDD89BF1DDAAA9D5,
	ObiClothProxy_UpdateSkinning_mD5BF40D50893B4AA5B9CDF59DBFE2F3110804C2B,
	ObiClothProxy__ctor_mED67798944E1C23E820295A5BD35F4FBE951F3F5,
	ObiClothRenderer__ctor_mBAC561B2AF103D9C49C73F9B0FD02E3952783B07,
	ObiClothRendererBase_add_OnRendererUpdated_m005A5B4813F40AD9030908BA9864FDDB27D07A51,
	ObiClothRendererBase_remove_OnRendererUpdated_m3FBF2A5A61D8A2112913C3186813A87667103128,
	ObiClothRendererBase_get_topology_mAFB5108E3117AEEFDF4396DD54647E17508A6A94,
	ObiClothRendererBase_get_renderMatrix_m63D33076CEFAB7804FCB8D5A54357AA5FE36EE10,
	ObiClothRendererBase_Awake_m1C28F746382661CA24D4554A21B3012631662C36,
	ObiClothRendererBase_OnDestroy_m5957DCAE1C8EBDE3262BEE1FDEADC392D3D4021C,
	ObiClothRendererBase_OnEnable_mBB699002D10B17911DD5F99EC919124A5890D7D9,
	ObiClothRendererBase_OnDisable_mBA13C1413C262DC40E6136549D6AD84260C31560,
	ObiClothRendererBase_GetClothMeshData_m1EBCC10924D369303C788603E1992A2714B68755,
	ObiClothRendererBase_SetClothMeshData_mD1F2FD48D7C6203837DE02654FD74E5C2B78DD2F,
	ObiClothRendererBase_OnBlueprintLoaded_m06BF18FA13DEBC1E7964370725DEC3CEFFC6E86C,
	ObiClothRendererBase_OnBlueprintUnloaded_mEC61E0663591DFB8DB508A0465EFDABFF264D7A1,
	ObiClothRendererBase_SetupUpdate_m0FA9C1879DA717926B4365EFA388407C0E573425,
	ObiClothRendererBase_UpdateActiveVertex_mD0211CB2FA72279FECFB83A24331E53D2606303D,
	ObiClothRendererBase_UpdateInactiveVertex_mFD3D31244DAF9ECF879D4AA94CC59A76AF831635,
	ObiClothRendererBase_UpdateRenderer_m67C5D60FA907DA341E7DE852502879D360B4A72A,
	ObiClothRendererBase__ctor_m14E952FA423389FF8A9FFBB4DEDB3AD45F89A5B1,
	ObiClothRendererBase__cctor_m123E98B445499062FB7277932205609170F7478C,
	ObiClothRendererMeshFilter_Awake_m03D6844E7C94DAFE7FFA9E0DA6EFEB6EB0B02EB1,
	ObiClothRendererMeshFilter_OnBlueprintLoaded_m7DFD7D44A5C55E53D7CBF48402AE0D1A867F1F2B,
	ObiClothRendererMeshFilter_OnBlueprintUnloaded_m233825C2AA0FBA4C3262E542CB63D088796E9E06,
	ObiClothRendererMeshFilter__ctor_m282A70D2A0FDA19306CE4E901C1EC6C4D30D3666,
	ObiSkinnedClothRenderer_get_renderMatrix_m4BAC1EC550B0D3F29950CCF556674FBD2AA31313,
	ObiSkinnedClothRenderer_Awake_m244A0D9B2968131DFF23CA5018502CEB4B472A5B,
	ObiSkinnedClothRenderer_OnBlueprintLoaded_m5FA4BBB248FFF2CE1555B76D4F6031D7C2D3FCD3,
	ObiSkinnedClothRenderer_SetupUpdate_m1A5EB436AAB7518E6C7D204BB7713608903853C5,
	ObiSkinnedClothRenderer_UpdateInactiveVertex_m3A7BEFD80868F949B67E1E13FCF395BA6027EF38,
	ObiSkinnedClothRenderer_UpdateRenderer_m0B034CC6342C1E8F8630069500EA34C4579CB173,
	ObiSkinnedClothRenderer__ctor_mD0D8AA6357C14C2FF206DE916914F8A60EFF63B7,
	ObiTearableClothRenderer_get_topology_m1DC9B503255163BD330C4C0EE154A4B9696A8F29,
	ObiTearableClothRenderer_OnEnable_mA0FD0FE15AE90519159E649741E2D877E3F6F43A,
	ObiTearableClothRenderer_OnDisable_mC7499A98C4DAA4A486C8523C1C97EC521811822E,
	ObiTearableClothRenderer_GetClothMeshData_mC99E90FEB3AB5A9BD9DBB5185F8D37D6ACEB524C,
	ObiTearableClothRenderer_SetClothMeshData_mE9F6592A5C6657B2FAD491825ED80D69B5463706,
	ObiTearableClothRenderer_UpdateMesh_m98ABCF7C8E42CC5CC650EC7E45D1341E507C32B3,
	ObiTearableClothRenderer_GetTornMeshVertices_mD7A8D7E293DB0815B2A060456ECA62EE826972C1,
	ObiTearableClothRenderer_UpdateTornMeshVertices_m2E42006A49946BE6E1624CF6B8962C4071841EA8,
	ObiTearableClothRenderer__ctor_m3884A3BA9B31A6B4786F79FCFF7A20F50F446572,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiActor_add_OnBlueprintLoaded_m750BDCFAAC9B9FC2B13473DD6142256B87AEB9AD,
	ObiActor_remove_OnBlueprintLoaded_m7DCDC0933CAE4EEDD66341CF218C7C52AEB5EA05,
	ObiActor_add_OnBlueprintUnloaded_mAF07AF4D755E892627202E5D841606AD45504DE8,
	ObiActor_remove_OnBlueprintUnloaded_m1BBCF889DC5E7CF33AD01B111E5983EB29BEEC5D,
	ObiActor_add_OnPrepareStep_m02E85D770109757133A0D4E7218B5414022C096D,
	ObiActor_remove_OnPrepareStep_m609001C918634ABA520EE383AEA2A0EA6B40B70F,
	ObiActor_add_OnBeginStep_mEADF45AAECC5E2C0930CCBD3D14CDDD65E1526F1,
	ObiActor_remove_OnBeginStep_m56B61A09CB8B0156101BE3B673A3ECACB9C8B0F8,
	ObiActor_add_OnSubstep_mFCF2FF0A7B81870BB43E6651F53C2C037D67CC03,
	ObiActor_remove_OnSubstep_mAE61ECC9F9F42DD0EEC897EE73596D5B83C3C8AD,
	ObiActor_add_OnEndStep_mD7307710B86E3884F47561B432963D3EF1F49847,
	ObiActor_remove_OnEndStep_mA7612D69ED92FE16899F8292AB9274047EA20A95,
	ObiActor_add_OnInterpolate_mF5BEDDED56D77A9F3F126B8A0C75213F796F5237,
	ObiActor_remove_OnInterpolate_m8262597E47B9200EC9D36E3BB132C79BAC0B8403,
	ObiActor_get_solver_mA2AA7D75716CC2D3B192EF6660A742CD7BA6537A,
	ObiActor_get_isLoaded_m44EB6CF5F3A3141E1FCA029F5CA8E0F06DFF52C4,
	ObiActor_get_collisionMaterial_mA0CAB573F50DB23136C92682BBD42AFB2B660D79,
	ObiActor_set_collisionMaterial_m319EB84B9963B7DD51AB003D867735A2D29BAE3B,
	ObiActor_get_surfaceCollisions_mB88273F10504E327D0392CB09A87EFFDF45E5051,
	ObiActor_set_surfaceCollisions_m86016E44CFFEE5DAB0C139C8AC9CD7F7ABD28ACB,
	ObiActor_get_particleCount_mB975A4D367302FF8F7DB9AB1841C9EFE14AB2B5A,
	ObiActor_get_activeParticleCount_mB9A86C48B1F1B31DF5FA15A2E9F29550079B99AC,
	ObiActor_get_usesOrientedParticles_mEAD00F29B6E3C3E6B1D4F5D0115F620E8C540026,
	ObiActor_get_usesAnisotropicParticles_mAA20FA6263D07BDF0ED730CB0509B0DFCDC5B6A5,
	ObiActor_get_usesCustomExternalForces_mB2DB2A7D050E13F6E2DA2C0AEE02FDFC8F183812,
	ObiActor_get_actorLocalToSolverMatrix_mEEA426364EA846E7545862692992B75B9541CA86,
	ObiActor_get_actorSolverToLocalMatrix_m44B0C2AA60494EE7FD815816C1D0ABE7E304E142,
	NULL,
	ObiActor_get_sharedBlueprint_m6A88C094D65EB05975F045937BB5A9CDF3C8322A,
	ObiActor_get_blueprint_mFB44880BA798FFBC94A11ABC8ACFE87077CE1C3A,
	ObiActor_Awake_mCFA26716113346786A47362EE4399C8135328F45,
	ObiActor_OnDestroy_m9C8BA7B78B8E469F0A7A78416F230315E7A4186A,
	ObiActor_OnEnable_mD66D61D2BBC655028D8208422AEA64786381F6BB,
	ObiActor_OnDisable_m13C6D2A5949762C0718A506CE0B4DD73D63B03C3,
	ObiActor_OnValidate_mD2AB1820B6297779092DA1B2D29768467BA82DE0,
	ObiActor_OnTransformParentChanged_m28F61AD8B165E2AA050346781678DC540D63E720,
	ObiActor_AddToSolver_mE5B9CEC1E1A9357E520418FD64B5DDC4D2FC180F,
	ObiActor_RemoveFromSolver_m0D4589A5675024C9D21FFA3AB415A1D42DEFE1EA,
	ObiActor_SetSolver_m4F478A600EFE01614A0E28DBBAC90EC4D56DBF36,
	ObiActor_OnBlueprintRegenerate_mCFD8B8A478A5B731AA8385F26810EA7AE7D6D25D,
	ObiActor_UpdateCollisionMaterials_m61579860AE22EB454F1BE424E5E2224AD74F95EB,
	ObiActor_CopyParticle_m51D676D131EBDF4C8685DCA1549AA5DAECFF5B99,
	ObiActor_TeleportParticle_m69B27BC2A0FD53826FE5B14A46A4951BA7B52578,
	ObiActor_Teleport_mF8C7FDC53D774D61FC7BE6E72B009803A909CCD9,
	ObiActor_SwapWithFirstInactiveParticle_m78A822EA1147B2CE69C32B89DB48D562BC0D9E26,
	ObiActor_ActivateParticle_m008AED1F23E74B2C4C977706708D3E547B0AA3C3,
	ObiActor_DeactivateParticle_m8A106245C1657AA6CF2A81468CCE4A7A3032C5E1,
	ObiActor_IsParticleActive_m892F91908AE43D40E0117C826A2ADE2E70579B37,
	ObiActor_SetSelfCollisions_mBA196DE918E283D18B104E44E25A369BF57DF48B,
	ObiActor_SetOneSided_mF6F09D5CDD6FC7E382379F12EDAFC69B2D3B5BDE,
	ObiActor_SetSimplicesDirty_m99F0CAFF371F338400E593B556E7BA6B0825E779,
	ObiActor_SetConstraintsDirty_mC937119D5E6ADCD2674A1204EBD3E2E2B6559607,
	ObiActor_GetConstraintsByType_mC947F9F5CF01DCF737BBBA300D32B9C0D6E52284,
	ObiActor_UpdateParticleProperties_m79AA51543AD70E1CD57B8ED6DBF7C57BC61BF316,
	ObiActor_GetParticleRuntimeIndex_m4EED333775B6B67B33CB0C2B200C344D3B375E77,
	ObiActor_GetParticlePosition_m09A6B4BFB585627E2ED09E93DD4094F333A14C1C,
	ObiActor_GetParticleOrientation_m128E5C81A8611D1B2D56241A7EF0070C1662E15A,
	ObiActor_GetParticleAnisotropy_m9CEDBC119B69CD6B157807D2E66DDDB3D06A13EA,
	ObiActor_GetParticleMaxRadius_m87144AB3ADB33846F86CEB4B6939ACE92917DF77,
	ObiActor_GetParticleColor_m7F66CE36A055FDAC277D9C6D4216DA4B992A4659,
	ObiActor_SetFilterCategory_mB1E5312397CA790EB18CAA64BD73E57FC396EE4F,
	ObiActor_SetFilterMask_m6D2AF9BE496199753111190398B8804EC830DA9D,
	ObiActor_SetMass_mA784144BCC7ADF1985E58AA5F5569307AE2FF4E5,
	ObiActor_GetMass_m19BAD33875847C12BD7622D6DFAB5F84D6F0D964,
	ObiActor_AddForce_m0E8B211FB33780E00CC1B22831B95BB37953EBD6,
	ObiActor_AddTorque_m7582153CD8CEB690DC0B9BCAFE57F7BAFEEB74E2,
	ObiActor_LoadBlueprintParticles_m8FE3AC1F4DFC662D6973E269A51C55740E909826,
	ObiActor_UnloadBlueprintParticles_mD50370C19E1AD0FE3A6C88233E35609F6932B869,
	ObiActor_ResetParticles_mBBB929ADB2C470ABE6093BE4D4647E2C0A2A79BA,
	ObiActor_SaveStateToBlueprint_m6E90039A5DCC6C4F2FCCF6A2C9AEEB0836D333ED,
	ObiActor_StoreState_m6D5D8B5823CC011DC9F97D34E27798D1E5F8722F,
	ObiActor_ClearState_m848A4E35DF8DD37649408E58E0FAAC93C9442954,
	ObiActor_LoadBlueprint_mD01EB4D136F4CAB3CC10456A1597D702A3CF1A97,
	ObiActor_UnloadBlueprint_m969E960679F2D21349E837CBA33D18EC1C876622,
	ObiActor_PrepareStep_mFEFDD4015BE99D742E4EC4562961CD4BCD19ACFC,
	ObiActor_BeginStep_m7C98EE23CE2E3BEBF930FDF68375D072ECB5DFCF,
	ObiActor_Substep_mDF70B27421BA323B7EB47AC78E825DD43398A6B5,
	ObiActor_EndStep_m733FF165CEB956C00689DC6B2FEE46DA209B5EAD,
	ObiActor_Interpolate_m782ABB607378E722CA9AC44D3D4AC05E214471FD,
	ObiActor_OnSolverVisibilityChanged_m5582DD6825DE4B3C555502ABC646BC685CA4AA02,
	ObiActor__ctor_mC9E385064DB6C39DC089855ED3E35750E80E2D65,
	ObiActorSolverArgs_get_solver_m22E029AF61259687CD6FFB9D6AB6EA20F86E3743,
	ObiActorSolverArgs__ctor_m474F3748E48B55A7E6CFD3DB247238416FFE5B22,
	ActorCallback__ctor_m957117094F0A77A5D1206A50B8E10A9EE85E7E42,
	ActorCallback_Invoke_m4E2F7079DDF8806942FFBC3744E8C2110A0B7475,
	ActorCallback_BeginInvoke_m154251BB5F9A6216F2FCB124267F1495C75E54C1,
	ActorCallback_EndInvoke_m1EABC102C44DDE64098209CAE75266FA74904663,
	ActorStepCallback__ctor_m52D4618B6F4F29C2395853E2D28C2C68FFB17DD6,
	ActorStepCallback_Invoke_m9014EEDF9817269F00FA4DD0F7610FE9900B1E7B,
	ActorStepCallback_BeginInvoke_m0846053D3A742F989F829061A3F87354E803190A,
	ActorStepCallback_EndInvoke_m37F5088435801E61A62095769073F7F38EEF5128,
	ActorBlueprintCallback__ctor_m747C9BC1FC06162D4E272EAAA503ED2CAE4A5D6E,
	ActorBlueprintCallback_Invoke_mE9F4E664478CACFABCFDE8B2560F776DA4FDE48A,
	ActorBlueprintCallback_BeginInvoke_m5A217A82EBC784D65EF72AF750C5A82D21A7AF2A,
	ActorBlueprintCallback_EndInvoke_m455198981D76FC45C680B1295D54B2DDF03E1961,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NullBackend_CreateSolver_mDDC610D9B5DFD34CAA18BF33EA17AE191CD68804,
	NullBackend_DestroySolver_mCD787B52B296982C9D2438E1CF9A7242CE3B18B5,
	NullBackend__ctor_m15F612C86E6CBFD78C9C747B2DBB5B0A026E473F,
	NullSolverImpl_Destroy_mB9D748974AB49AD42F59C4F7537E14735BA925AB,
	NullSolverImpl_InitializeFrame_mB4EED2EC4F57413F0093AFB60566DF7D6E1003B0,
	NullSolverImpl_UpdateFrame_m41A43DA4CEDB5C86E3B61C1B648A2C49DA2045BB,
	NullSolverImpl_ApplyFrame_mA1F9133DA49AC1F08FF0FEC050E5DEF10EC88E21,
	NullSolverImpl_GetDeformableTriangleCount_m3A8AE1DF269BBFCFF0C48419C2ED8B90CFB5A95B,
	NullSolverImpl_SetDeformableTriangles_mCFA6D651F8606212CF3158B1229B16A2079B90D0,
	NullSolverImpl_RemoveDeformableTriangles_mD24BAEB276DDCE364A13F89AD935A175696319E8,
	NullSolverImpl_SetSimplices_m63A96660EBEC7FDF104C4121A099C7503D68D23A,
	NullSolverImpl_ParticleCountChanged_m8B9FED4B7B5A6C6014F41D48B396A53CDA40568C,
	NullSolverImpl_SetRigidbodyArrays_m1447ED0E67413D3CC414D66A687B6AA804D5A7D2,
	NullSolverImpl_SetActiveParticles_m8E95A62DFCFAACFCCFD71C5D88C1D2D89B4B34FD,
	NullSolverImpl_ResetForces_m82E047EACB6F9E608555DF3BED74AC126A09AD69,
	NullSolverImpl_GetBounds_mAC172D98AE3D9510345D850A6CAE7BED572AD658,
	NullSolverImpl_SetParameters_m87C1A9AF268024C0A098129A7032E9A6321D13AE,
	NullSolverImpl_GetConstraintCount_mE99B745191C0B68347534F69BCBDCD3FA738DF63,
	NullSolverImpl_GetCollisionContacts_mD5BF1363A042CB174A8D0FA9794DEC8DF3958D31,
	NullSolverImpl_GetParticleCollisionContacts_m3A5C1741A0C6B26B27DB246311246DBD9596F7DC,
	NullSolverImpl_SetConstraintGroupParameters_m1424D00634EE6B6E70DFF8EA8EF0B96F215B9B33,
	NullSolverImpl_CreateConstraintsBatch_mCD99CB5676EB5E4BA217C5CEDBE962E293B2714D,
	NullSolverImpl_DestroyConstraintsBatch_mBBEEC8E9CDAA7A0EFB1CDF86008679CB710376B2,
	NullSolverImpl_CollisionDetection_mF6EA4B141873E6B181C3BE308E244BE9BC032615,
	NullSolverImpl_Substep_m8A8BEE938250D29A032AFE3B78C66371DBD9CD01,
	NullSolverImpl_ApplyInterpolation_m798CEE15B0E0B41CECC3F50AD8026B2DD2F6ED2D,
	NullSolverImpl_InterpolateDiffuseProperties_m0B56D3EA46E77669D02A80AF84E432CE4DC36516,
	NullSolverImpl_GetParticleGridSize_mAB2387DF4946BD28660B4B9C462C238D59AC90DE,
	NullSolverImpl_GetParticleGrid_mAA02219DE1D1E8AF85ABA638C446EC4646A67F0D,
	NullSolverImpl_SpatialQuery_m0148C0348711AD8CE00B61647ED4D9BDBD212BB0,
	NullSolverImpl__ctor_m4867A668003F79D63D4C15BB6DE916CAAEF9C858,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsBatch_get_constraintType_m6040BFC92ACB997BF45396F214CD149999EAD65C,
	ObiAerodynamicConstraintsBatch_get_implementation_m0ABDE56F2D9D55089C4844ACBEB750EDF2F2891B,
	ObiAerodynamicConstraintsBatch__ctor_m25D792BB54A6216ED777E0024C3F3A0DE44B988F,
	ObiAerodynamicConstraintsBatch_AddConstraint_mCC6E68CC3D308B67C289888736696E150AF8EA2A,
	ObiAerodynamicConstraintsBatch_GetParticlesInvolved_m027B6DE66398E0F8471AA932CB4AE4FB89B72C7B,
	ObiAerodynamicConstraintsBatch_Clear_m10E7BF1CCDCA05D61B7E0E9189F51A3663537A03,
	ObiAerodynamicConstraintsBatch_SwapConstraints_mFD22DF1620944363A9323610E7465F71F4471A92,
	ObiAerodynamicConstraintsBatch_Merge_m9308B92A6AF8D02A36217C18C6CAE958DA55D829,
	ObiAerodynamicConstraintsBatch_AddToSolver_m9443F9131AAFFD918FA8CE86F7BEF600EEE5906A,
	ObiAerodynamicConstraintsBatch_RemoveFromSolver_mE7D0473018702606B66D30240083EF253AE9B1C3,
	ObiBendConstraintsBatch_get_constraintType_mC9A58918A83E7B2FBA6DC1DADAEAD4A8BEC787D5,
	ObiBendConstraintsBatch_get_implementation_m18F9C4C0D6D548BDB54280777BAD07CDEBA8BBB4,
	ObiBendConstraintsBatch__ctor_m7177EA44789A81DD8E136F47A1B93FA577265FEB,
	ObiBendConstraintsBatch_Merge_mD52244354E9F60085E0A515A3878673195133FA7,
	ObiBendConstraintsBatch_AddConstraint_m91A746106C49A8D164BCB77F1AD08B7E21186E62,
	ObiBendConstraintsBatch_Clear_m8071A4A1941BBD6947334864521EF1AD9F26821D,
	ObiBendConstraintsBatch_GetParticlesInvolved_mA10C16017FC5ED1C4B0ACD68F562D6F2D0BA2BFF,
	ObiBendConstraintsBatch_SwapConstraints_m5DBFB004A248585886F229CD4659BA997C2CB916,
	ObiBendConstraintsBatch_AddToSolver_mFED1BD899374F8D91B54DC1740A5408AA695251F,
	ObiBendConstraintsBatch_RemoveFromSolver_mE430D71519380719B08014FE7A4BA992CD45DCD9,
	ObiBendTwistConstraintsBatch_get_constraintType_m87C6A4463F0BD0C9B47DC213C9BB56847790B79F,
	ObiBendTwistConstraintsBatch_get_implementation_m7797868D0B35B4B8A7AFF96506FF43526AE6A702,
	ObiBendTwistConstraintsBatch__ctor_m41B5BBAB31EFB0541152B01E186B5C39FF26A9E0,
	ObiBendTwistConstraintsBatch_AddConstraint_m41ED663245750ADB2C9EC752E27CC828F5E63A0B,
	ObiBendTwistConstraintsBatch_Clear_mD3AECC462BA3746C51058B0C02A853340048AC3C,
	ObiBendTwistConstraintsBatch_GetParticlesInvolved_m6C5D97C54F11F31FD1A01289455E8FD60A7A63A1,
	ObiBendTwistConstraintsBatch_SwapConstraints_mFC1004BCF2D2A2EC8EC3C522D0CA241C0971A50C,
	ObiBendTwistConstraintsBatch_Merge_m2DE903B520FEE9A093E986C89300AF27E10E82CB,
	ObiBendTwistConstraintsBatch_AddToSolver_m53FB4DC851D4EAB187A7694BFC924617655E4722,
	ObiBendTwistConstraintsBatch_RemoveFromSolver_m71A2C2D8C1361AAEA0626C63DC30A32984992C96,
	ObiChainConstraintsBatch_get_constraintType_m32BA3CE0851520BB2428F43686BE579E2263780C,
	ObiChainConstraintsBatch_get_implementation_m06CB0C729ED6E6A5C6DAE81F07966875C590C46D,
	ObiChainConstraintsBatch__ctor_m29D8DFF83BDC43705D07512AB3C4E4310D40847E,
	ObiChainConstraintsBatch_AddConstraint_mCC67D71A5D82D67E8A58D3AAFCAC57EEAE88251A,
	ObiChainConstraintsBatch_Clear_mEE2C2127119C0BC02DA141E3CE6A87E0123B7CC0,
	ObiChainConstraintsBatch_GetParticlesInvolved_m347FE60058ADC07D507DC3759DBC7AD776BFE24A,
	ObiChainConstraintsBatch_SwapConstraints_m2BF8A9FE3C1FD267B2DD0A293E9581D3321FD6E6,
	ObiChainConstraintsBatch_Merge_m3FEF585F1BD6CA3549BAFEF02F801B05487F974C,
	ObiChainConstraintsBatch_AddToSolver_m3FFDF9CE593E40DADE1FDE3CFE2D551F877E542F,
	ObiChainConstraintsBatch_RemoveFromSolver_mD5349882C9454FD14040CF4B4B01DFF2956981D4,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_get_constraintCount_m4E00272D7170BBFC571EE715EEE8828D96D02E52,
	ObiConstraintsBatch_get_activeConstraintCount_m6E6D5CDF52509352BF2355216995F8398BED7180,
	ObiConstraintsBatch_set_activeConstraintCount_m47CB4B1E0D9BB5763BF4E3B76A1ABC409B8827B5,
	ObiConstraintsBatch_get_initialActiveConstraintCount_mEE507FB39B4E352F6FA6FD13D3861A21242FC560,
	ObiConstraintsBatch_set_initialActiveConstraintCount_mBD3AF514882972EE52BF3A223559393BFEDBD657,
	NULL,
	NULL,
	ObiConstraintsBatch_Merge_m1D91B98297B3622D69B5D72E659770E3CFC8BE58,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiConstraintsBatch_CopyConstraint_mED5F0FA303BD8AE027DEDC7EA7FAA7CA85993222,
	ObiConstraintsBatch_InnerSwapConstraints_m96D1004604F1B82F62C5497409485E7E3CA44E3E,
	ObiConstraintsBatch_RegisterConstraint_mE486157FBC98DDE17CD1F1EA22063D7BD8A115BD,
	ObiConstraintsBatch_Clear_mDDF32C36C5A9723AD134D7D49BCE5ED0A2628A3C,
	ObiConstraintsBatch_GetConstraintIndex_mB953302720E7FA7A466C98A24CA47752CCA318D4,
	ObiConstraintsBatch_IsConstraintActive_m22A8784C1241B0AF1FA125755BA86A83139EA33F,
	ObiConstraintsBatch_ActivateConstraint_m9CBA21CDC67196756D939CB2AF5A3AEE4C875670,
	ObiConstraintsBatch_DeactivateConstraint_m1B5361FE327AF1B74F28928B2820F43377C990DC,
	ObiConstraintsBatch_DeactivateAllConstraints_m464A79016D1A1BFED227F7578BCBDCD5A42557FA,
	ObiConstraintsBatch_RemoveConstraint_m8189637BE0D2051E091ECB7715D8E0285D84DEC6,
	ObiConstraintsBatch_ParticlesSwapped_m69696C36C7D9ED549CBA88325807B8E267B6BCCD,
	ObiConstraintsBatch__ctor_m193FBCE314B9D8E37B9994E58BCD42C4CA54ED47,
	ObiDistanceConstraintsBatch_get_constraintType_m190A5F03319B7CD354FA70476ECDDB0CF497E977,
	ObiDistanceConstraintsBatch_get_implementation_m4BEB1A164478614379046670E0D995C1C7973B0F,
	ObiDistanceConstraintsBatch__ctor_m790FDB511304C5885D8839BFE82C87017E8EE617,
	ObiDistanceConstraintsBatch_AddConstraint_mCFA5F34C60D27357731AF57A53F1BE4FFC4A9331,
	ObiDistanceConstraintsBatch_Clear_mECADC8966C7B33BE6DBAC7EEA077852C4894759B,
	ObiDistanceConstraintsBatch_GetRestLength_mF522E6802E0FF6E2AAC75B8DA1E2D39ACD16C8FA,
	ObiDistanceConstraintsBatch_SetRestLength_m19C64337495E2F77549651EFFD7EE9C9AEFE4A7A,
	ObiDistanceConstraintsBatch_GetParticleIndices_mBAFEA360DCBD24C1CA6F11D1DF3A2103E8AAD5E8,
	ObiDistanceConstraintsBatch_GetParticlesInvolved_m96004703CA8F6B2E355FEC2DB57FE8752936AE85,
	ObiDistanceConstraintsBatch_CopyConstraint_m770BEC33F923510422483EA234C6A555C5E87E8F,
	ObiDistanceConstraintsBatch_SwapConstraints_mFAA5EFF7DE1FDEB90650770DD4AE8F2AA55F9311,
	ObiDistanceConstraintsBatch_Merge_m18C8BB0A4EA64EBE8519D3337EEEB0C66F31751B,
	ObiDistanceConstraintsBatch_AddToSolver_mCD30473D6B3DBCCA9DF557F59796050E36C0CE19,
	ObiDistanceConstraintsBatch_RemoveFromSolver_m3DD0B0F8DC45B9CC4219B6D371552416AA07DF13,
	ObiPinConstraintsBatch_get_constraintType_m9DEFC47C40861F453CDDA876B210345477BF4DD1,
	ObiPinConstraintsBatch_get_implementation_mF812C515ADEF9F33AB0FABC5EBD6A259C16525AE,
	ObiPinConstraintsBatch__ctor_m4C81948872E9934B4AA9056CD5C739D0D96ACB22,
	ObiPinConstraintsBatch_AddConstraint_m0838F4170FC5AB9D0B133800A0E1C6AC52D4D13B,
	ObiPinConstraintsBatch_Clear_m7E365B1B1213EDE1E31991C106106D149D700279,
	ObiPinConstraintsBatch_GetParticlesInvolved_mED7DDAB3F66241C5614E05BFBA6F30F92F8AA889,
	ObiPinConstraintsBatch_SwapConstraints_m8385C5A9537EC815E9A66D83C11ABB5C974B503A,
	ObiPinConstraintsBatch_Merge_mCD532E7A783752197FF842B6EBB0265F279B5996,
	ObiPinConstraintsBatch_AddToSolver_mF594ECA9A0CD268EC8333EC6184F84FD776D6C40,
	ObiPinConstraintsBatch_RemoveFromSolver_m0D8B9453D75CB94EB1B3B318C14D65B86AC5C754,
	ObiShapeMatchingConstraintsBatch_get_constraintType_mB50B35888E0F97B1AF0C7BC4D8CDE190231AF4F6,
	ObiShapeMatchingConstraintsBatch_get_implementation_m4BA8C5859DCF47C334FE7E797B5A3A806591E326,
	ObiShapeMatchingConstraintsBatch__ctor_m5ACB157EB4F8783A8FAABF7B6C42728F889579A3,
	ObiShapeMatchingConstraintsBatch_AddConstraint_m0ACEA7D8A66234BD089034C904A43CF43E02A822,
	ObiShapeMatchingConstraintsBatch_Clear_m6E50E2ECDCE04F6CA6E42EAC6807257A6EF328E3,
	ObiShapeMatchingConstraintsBatch_GetParticlesInvolved_m7FAC15E7522407CDA84BCCB98697A421079258DA,
	ObiShapeMatchingConstraintsBatch_SwapConstraints_m6857C3148FDB065E38C43D5FF42D2AF39E35E8AF,
	ObiShapeMatchingConstraintsBatch_Merge_m437985B208162205896195E94DDE754E63F8CACF,
	ObiShapeMatchingConstraintsBatch_AddToSolver_m96316AF9F461D3C2629064427C5BF983B377732D,
	ObiShapeMatchingConstraintsBatch_RemoveFromSolver_mFFCCC95021893AE8406B3CF572CFCD5A004D4D77,
	ObiShapeMatchingConstraintsBatch_RecalculateRestShapeMatching_mF7F1083A3082F08437767C334AF98F6C6836ADB8,
	ObiSkinConstraintsBatch_get_constraintType_m43617264E9A74606E427CF12AD73FC33A1C3EDB0,
	ObiSkinConstraintsBatch_get_implementation_mBCCA6519112E7BC4CE35826BE6AD268605C4F7A1,
	ObiSkinConstraintsBatch__ctor_mD3168603B7448A20C3A01B84E78CF90C6FA32315,
	ObiSkinConstraintsBatch_AddConstraint_mD298AA59592F96CC6666FFB209AA56313D77AC04,
	ObiSkinConstraintsBatch_Clear_m4429DE2477EE5B0C4F853F13924BAFB15D36471B,
	ObiSkinConstraintsBatch_GetParticlesInvolved_m2BB59D7EC8570B6299C571DA5B06200F7002BB41,
	ObiSkinConstraintsBatch_SwapConstraints_m273EBC39910668056AC0104BBA6B41206A2D862A,
	ObiSkinConstraintsBatch_Merge_m027573A98590755002B0A485032729914654A0A6,
	ObiSkinConstraintsBatch_AddToSolver_m4AA52CBF9768D0D7FEEB5C51738879C37617D1B7,
	ObiSkinConstraintsBatch_RemoveFromSolver_mF2E5D7885E0EFCA7A9C8E9BF4AA9D034091420BD,
	ObiStretchShearConstraintsBatch_get_constraintType_m948CCFB2E6F31C2D7F55199F2264FFEA244B8A6A,
	ObiStretchShearConstraintsBatch_get_implementation_m8FD53A9BC99C7336EAA10901E94F223AAD993DB5,
	ObiStretchShearConstraintsBatch__ctor_mAEF7386671FDAE31146C46155938EA24FB4BB0CF,
	ObiStretchShearConstraintsBatch_AddConstraint_mA7EC001A3C4C7029737F902ECB04D90590480E43,
	ObiStretchShearConstraintsBatch_Clear_m15F14739C62DA79AC798C77123CD119B4AC80199,
	ObiStretchShearConstraintsBatch_GetRestLength_m2DBA5798B72F9AC73E56F86741494A6F04BC81C8,
	ObiStretchShearConstraintsBatch_SetRestLength_mC4634A4AB60B8B44CC7F2121AC3ECD94F686DC2D,
	ObiStretchShearConstraintsBatch_GetParticleIndices_m700F60CDF847F7BD660D81A3C486224CEF926298,
	ObiStretchShearConstraintsBatch_GetParticlesInvolved_m20F312A352681BC3FC07FA6FDC894502B225AD7F,
	ObiStretchShearConstraintsBatch_SwapConstraints_m33C0BEA7DDEFC13F89F85C5681EC57D291643A92,
	ObiStretchShearConstraintsBatch_Merge_m48F422BC5E5CF3FC834F41A6E167DC9F12A3498C,
	ObiStretchShearConstraintsBatch_AddToSolver_mFA26026F08631A8DA6DA75493B3D76F5138579A6,
	ObiStretchShearConstraintsBatch_RemoveFromSolver_mBA6C140C81E8490A1BEE1EB1A7FB31E225B91793,
	ObiTetherConstraintsBatch_get_constraintType_mB5C8D6C8977031D53A14EF35674B3BA7FB18E98C,
	ObiTetherConstraintsBatch_get_implementation_mF4449DF39A0BE8629FB4FEE849CADD130D088E02,
	ObiTetherConstraintsBatch__ctor_m1ECCE4C0E33C5E6FC0D4837CD7E3E7A8A1C11843,
	ObiTetherConstraintsBatch_AddConstraint_m00340D7296944D9AF2C498F2EA8CC2185572BB44,
	ObiTetherConstraintsBatch_Clear_mE9AF6F043D828602C4CEEBAF7AB000FAD9A5C0AE,
	ObiTetherConstraintsBatch_GetParticlesInvolved_m7AEC2CCE6926DC1CBEB75D7464FBAE2EC2F143C3,
	ObiTetherConstraintsBatch_SwapConstraints_m559BE7B7AE7337697B4A1A0DD7E2BAEF9C981589,
	ObiTetherConstraintsBatch_Merge_m0EE3CD63815A39D7F257DC36E9A9804CE2AF4A10,
	ObiTetherConstraintsBatch_AddToSolver_mDD0E12C205B28409E7C98D707C4B8CDF918B540C,
	ObiTetherConstraintsBatch_RemoveFromSolver_m74CADEE6D4CF79CCF69FCA20F7ACD8C47EC59C7E,
	ObiTetherConstraintsBatch_SetParameters_m62F2FAB128BB8A7AD07564AB8C575C059B13CD22,
	ObiVolumeConstraintsBatch_get_constraintType_m9D81BF01ED857E956C5DE50E9F994D9C86E53660,
	ObiVolumeConstraintsBatch_get_implementation_m946F7C41F8CE2ACD0D7615FD5548CD9F530EF41D,
	ObiVolumeConstraintsBatch__ctor_mB2647CF2902FB0A0CA1A69FF19FC239B4830DC94,
	ObiVolumeConstraintsBatch_AddConstraint_mBA70D54B618AB3B3EAEBC07556F8A84E6007610A,
	ObiVolumeConstraintsBatch_Clear_m48637D46B4B6523DAF0CFEAA333282C1B5E4E292,
	ObiVolumeConstraintsBatch_GetParticlesInvolved_m2F02EF9CE76285F168C567DD1F7BB646D8604352,
	ObiVolumeConstraintsBatch_SwapConstraints_m29154135DDF6A4013A74CE93533E4DC6CBEF8875,
	ObiVolumeConstraintsBatch_Merge_m83877E6ED340BC6DDBA77555354DE250F775B2A7,
	ObiVolumeConstraintsBatch_AddToSolver_m28665035A10D5B25A542D1021FEC8FB8433FBCAC,
	ObiVolumeConstraintsBatch_RemoveFromSolver_m4613CB7776E67CF56AF07D61F6FF848351542945,
	ObiVolumeConstraintsBatch_SetParameters_m5242E860F0532140C4D70AFFBEC136C06C863112,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiAerodynamicConstraintsData_CreateBatch_m9F8687477420F05FA5DAF93D6517BE199B9758AE,
	ObiAerodynamicConstraintsData__ctor_m49F24DDC021A94D8F243AB94D1612D18D7D893A7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendConstraintsData_CreateBatch_m1E5674147A953CADEB7335E6A8EAD6B94B82FE96,
	ObiBendConstraintsData__ctor_m5CE912D237E59AC2459A0044914C0ADF679D5664,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiBendTwistConstraintsData_CreateBatch_m0709FD77E20752F45BA776DC05A130AAD04D7688,
	ObiBendTwistConstraintsData__ctor_mEAD7C51796794836ACBF367D4D41D013A6E4D799,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiChainConstraintsData_CreateBatch_m0BB75C6811F727D1A180B77248740B890BF4864A,
	ObiChainConstraintsData__ctor_m3BA306403D04BAD27188B520142EBA6940392C02,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiDistanceConstraintsData_CreateBatch_mE76E6B2D1A7CC775E2DCA8BCFE61B4058F7EC8E5,
	ObiDistanceConstraintsData__ctor_mD731E72CD8800C60BB8876179E36211C1C7F04E9,
	ObiPinConstraintsData_CreateBatch_m0E2C40753FD7A119CE5A7FD392C9F7BDA0E671CF,
	ObiPinConstraintsData__ctor_m744846C675FE4FCBABCB7FC988693E4B5F751157,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiShapeMatchingConstraintsData_CreateBatch_m47612CB07D694D4AC0AE0530497FE3A2A9391573,
	ObiShapeMatchingConstraintsData__ctor_m0B38950E582515B04C26082701C793A096B4D592,
	ObiSkinConstraintsData_CreateBatch_m6718125284AD1F35943AA449A41D2202999BFE23,
	ObiSkinConstraintsData__ctor_m24190FDC7EF8ABA6B97B657E1F6E2CEDE955FE67,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiStretchShearConstraintsData_CreateBatch_m0EF5F33043FD16160BC1BA7732E74CF90196617A,
	ObiStretchShearConstraintsData__ctor_mE207C4FC05457C2259CBA1ADD59F6BA3953ABCD8,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiTetherConstraintsData_CreateBatch_m74B1890FE3281245BF97E41FBDFCA4252FEB1470,
	ObiTetherConstraintsData__ctor_mEF28EB23B58ECF1EE9DA005B34A316A567588846,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiVolumeConstraintsData_CreateBatch_mC832E9336237193B4075DFF88DAF9F3D19BC6C95,
	ObiVolumeConstraintsData__ctor_mC8750405A88786FFF03C3ACBB102CF73DAD1C8EE,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	StructuralConstraint_get_restLength_m4DF4D85631CA2689452264F43D092A2FF9FAD9B7,
	StructuralConstraint_set_restLength_m41F5560600620E38F9CB5122513336B879D36763,
	StructuralConstraint__ctor_m02AB4F391F2914D0C63E12EA1ABB8A8AB98B00B4,
	GraphColoring_Colorize_m7D42EFE9A1DA9F0D34C1B582B560CF34820CCCDB,
	ObiActorBlueprint_add_OnBlueprintGenerate_m722E32133C4AF676EC31317FC1629275A6D37C3E,
	ObiActorBlueprint_remove_OnBlueprintGenerate_mDB764D279BF95D0F705FFC6541C8601C97AEBC0B,
	ObiActorBlueprint_get_particleCount_m32DECEC061D75DAD97AA6110A1F22F45EA9B1D30,
	ObiActorBlueprint_get_activeParticleCount_mF61F0B48324015C5FE0946358D3A8205074A442D,
	ObiActorBlueprint_get_usesOrientedParticles_m28DE6E8AA98625BB7B2BECE4F1A1FC4C8AC589D3,
	ObiActorBlueprint_get_usesTethers_mA9B61493ACD210D9A6CDF2AC50AE543F9527AD20,
	ObiActorBlueprint_IsParticleActive_m75AE6706A7726FB3608CF435316F87BA49E44208,
	ObiActorBlueprint_SwapWithFirstInactiveParticle_m9FFCA89E796C59CABBCDE588A65F29995235478E,
	ObiActorBlueprint_ActivateParticle_m0002FC5E69FD374AA5EFB9BC1F53F279310CDE80,
	ObiActorBlueprint_DeactivateParticle_m292A9950AC1F69E56B479D8525405BE5A336DCC9,
	ObiActorBlueprint_get_empty_m533A4D139D3EE6F026BF2A67B562EDF9C15E2C19,
	ObiActorBlueprint_RecalculateBounds_mC8B3BEAAE2219E8B400E2A9E2B85AC60EC256FCF,
	ObiActorBlueprint_get_bounds_m9A2C87A35CF971985D2A017E65D0F4E83F2465ED,
	ObiActorBlueprint_GetConstraints_mE9073C983B0C46F75C8597D50F54F6E4BD2976CD,
	ObiActorBlueprint_GetConstraintsByType_m850F04FFF6503102CC1285A4A293DF519D94D03C,
	ObiActorBlueprint_GetParticleRuntimeIndex_m338D08056C596909B71962BD41D90A35778185D8,
	ObiActorBlueprint_GetParticlePosition_m809557B9E54C966416666946A0116E9B4FE3273A,
	ObiActorBlueprint_GetParticleOrientation_mEB166212FF89B1F6B2D0E006F64D480CC474E399,
	ObiActorBlueprint_GetParticleAnisotropy_m29F45586B637695FF214FF3970167149CF4D1C8B,
	ObiActorBlueprint_GetParticleMaxRadius_m0930B36B9734F68963DA78EE73868C6F2648783D,
	ObiActorBlueprint_GetParticleColor_m679E28CCF4AE45CD284C1F6998C48BE4E3726756,
	ObiActorBlueprint_GenerateImmediate_m6400F73812A2A099A121C9FD603D956E0CD1E032,
	ObiActorBlueprint_Generate_m5080D99D3401C5397E728FE6384DAAC0E43E8F1F,
	ObiActorBlueprint_InsertNewParticleGroup_m627C6C0E3C99092D87981BA9F372CB364A73A069,
	ObiActorBlueprint_AppendNewParticleGroup_m0E0CC2FF6766EB812B4C33F4F9FB65491206A0F5,
	ObiActorBlueprint_RemoveParticleGroupAt_m44C677861ADADBA8160EA3346B9352A3DBBE087A,
	ObiActorBlueprint_SetParticleGroupName_m8972A24BFC1B01B329F6D302A550FFD5FC1BD958,
	ObiActorBlueprint_ClearParticleGroups_mBD9DEF28A12E32A8FE58BD535DBE4A64FC1ACCA1,
	ObiActorBlueprint_IsParticleSharedInConstraint_m65F2A72F6ADA8A0103B4D954B76202AFF032DAD1,
	ObiActorBlueprint_DoesParticleShareConstraints_m58578F416B7A5F855D954FB941CBF5F1E077A0DD,
	ObiActorBlueprint_DeactivateConstraintsWithInactiveParticles_mE8ADC81B4819AF3F669A012D70018743A61C9191,
	ObiActorBlueprint_ParticlesSwappedInGroups_mC8E1F44F0E7018E98D78F31AB71B4BB2FA630967,
	ObiActorBlueprint_RemoveSelectedParticles_m38C23BA2058BDC49997BFC139F89DAB8A1E05FB5,
	ObiActorBlueprint_RestoreRemovedParticles_m583966D5F1B5F57A8F9F085870D1CD7181902AE9,
	ObiActorBlueprint_GenerateTethers_m9EE5A9F23039CDC0802E991B78CE7BD0C0CA4CF3,
	ObiActorBlueprint_ClearTethers_m6542371776C70D0EE153EEBF0B89555400C214D0,
	NULL,
	ObiActorBlueprint__ctor_mCF461CF7ADF91137650AC6A2F0201895FB871139,
	BlueprintCallback__ctor_m1C05FA54C75C74D2FE31BEFA3CD5DD5A48A537A2,
	BlueprintCallback_Invoke_m0DAA83F8560B7A01E02BE1BEA342B8FF282AA690,
	BlueprintCallback_BeginInvoke_m68AB7718861BED5EA793C69612787216135F5A65,
	BlueprintCallback_EndInvoke_mC1B9B01B080421E8F4040DD9783C8B7E067ED9C7,
	U3CGetConstraintsU3Ed__50__ctor_mD6AF1993959B36E1A204AB0AFC94D2FBB8CDB038,
	U3CGetConstraintsU3Ed__50_System_IDisposable_Dispose_m2B66041A7F60DD9BC93703E04F250E7F0613BE2A,
	U3CGetConstraintsU3Ed__50_MoveNext_m2BB3F4BDE3CF53D8691ED4A48BC062995BA6D05E,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumeratorU3CObi_IObiConstraintsU3E_get_Current_m9A79580894AEB14A2B0BA62AAE39A1B392E9F4FC,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_Reset_m458EABAF66430791D0C1CFE4A20D27E310B7591F,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerator_get_Current_mCEEDBE01723DA154C8685D2823E481C78769A50B,
	U3CGetConstraintsU3Ed__50_System_Collections_Generic_IEnumerableU3CObi_IObiConstraintsU3E_GetEnumerator_m20BA04CF2264F78BAD7EBD50B5933438AA818484,
	U3CGetConstraintsU3Ed__50_System_Collections_IEnumerable_GetEnumerator_m26791F98C8A1420900D98771D4DEDC57601B029E,
	U3CGenerateU3Ed__59__ctor_m54244DFE624F4AEAF10B69E73905B536E557C111,
	U3CGenerateU3Ed__59_System_IDisposable_Dispose_mD3BCFB34C57742D183318F9CA268A1EEFB0F8899,
	U3CGenerateU3Ed__59_MoveNext_m86CB03056D8522556011343C4E12C569B6E7C306,
	U3CGenerateU3Ed__59_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2B2BA23F5A3087A28988199727976C63C17E8A3B,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_Reset_mA330FB66D7965D067D607B9B53521BB6B00458D8,
	U3CGenerateU3Ed__59_System_Collections_IEnumerator_get_Current_m597FA762BE3018F55A1C2EA2F7B92C67CE67F269,
	ObiMeshBasedActorBlueprint__ctor_mD1CCC19A27CB4507721539C71BFDC95377B9CCDA,
	ObiParticleGroup_get_blueprint_m9FD4C7214E3E9510B1425BB7896AAAC3ED4F2BBB,
	ObiParticleGroup_SetSourceBlueprint_mA2170558232F906D564DD68BADD9ED61B3751C4E,
	ObiParticleGroup_get_Count_m23DCE79FDBAF746E6A907045E50752C42B7CAB2A,
	ObiParticleGroup_ContainsParticle_mB11DBD3561F7B98112ADD73A99F65243EBA726A2,
	ObiParticleGroup__ctor_m885AA5EBCA717A818B14D2033E9782BA50219AF7,
	ObiBoxShapeTracker2D__ctor_m72A5FABFA97DA009F34441602ECB2E947A9A739F,
	ObiBoxShapeTracker2D_UpdateIfNeeded_m91193CF6269361586C9008A77A700345F5974D47,
	ObiCapsuleShapeTracker2D__ctor_m4F5BEDEF53623CFA6AB8C0D60297592E45BE9903,
	ObiCapsuleShapeTracker2D_UpdateIfNeeded_mD0C44C3584228599A8BCEC562B5480C014E1FA95,
	ObiCircleShapeTracker2D__ctor_mE3F6870036F6B3A95FC6B26DB7A0ECA9B3075730,
	ObiCircleShapeTracker2D_UpdateIfNeeded_mB8126562470730326CCD154127C7D30DCDCFF6F9,
	ObiEdgeShapeTracker2D__ctor_m69E472C578DE3CB917AA570D6991D20C299CC629,
	ObiEdgeShapeTracker2D_UpdateEdgeData_m4FB6C746BBAF32C2DC265793C876F903701F7E2B,
	ObiEdgeShapeTracker2D_UpdateIfNeeded_m1DDA9C78DA820F4FE320E1597DFCAC96636B253F,
	ObiEdgeShapeTracker2D_Destroy_mA11D3038473BCD30A48AA1EB8CFDB3F0E22C27F3,
	ObiBoxShapeTracker__ctor_mC46FA6587648E8086544D41DBDBA1CFC61748265,
	ObiBoxShapeTracker_UpdateIfNeeded_mDCCB3495573A0CC2B10B8519403C33741F34E86B,
	ObiCapsuleShapeTracker__ctor_mEACB22598959563B00E783F743C23D9BC38342AE,
	ObiCapsuleShapeTracker_UpdateIfNeeded_m9E0EB1D7DD68733F0435FC11FFF9BEA4ABC8F809,
	ObiCharacterControllerShapeTracker__ctor_m469B48FB58AD955867A5FC3D8336A9F46AA3E3CC,
	ObiCharacterControllerShapeTracker_UpdateIfNeeded_mD88444722EEF14B729AF17E13B360FDA8A31A820,
	ObiDistanceFieldShapeTracker__ctor_mC6E24376FC68B21C5B4B12AA59389FEDECAB9EEB,
	ObiDistanceFieldShapeTracker_UpdateDistanceFieldData_m6E0A0F0E873F5DF290DB3E88063DDD5E0C192EFD,
	ObiDistanceFieldShapeTracker_UpdateIfNeeded_m01F9C5D5243F44C9DB90C9F58A6B450D171895C4,
	ObiDistanceFieldShapeTracker_Destroy_m67B3FB02F03A6A627CA88C745D5EE8945D9CE25D,
	ObiMeshShapeTracker__ctor_m84B37C3D6C5AF9057683315A9695879E8146F5EF,
	ObiMeshShapeTracker_UpdateMeshData_m792F335D36DDBF67036960222AD503052BD4B441,
	ObiMeshShapeTracker_UpdateIfNeeded_m9750519C394EBEAEAF2D8FF95E52E1CEDC088EB8,
	ObiMeshShapeTracker_Destroy_m2E0D92720E52771FCCB8FF89561142E9C4375F6B,
	ObiShapeTracker_Destroy_m3D78B0BF6919363219DFC18EEC33E9D5FE51E6E2,
	NULL,
	ObiShapeTracker__ctor_m6593D9CB64011F3C87EB568E8D4A856A55E4119A,
	ObiSphereShapeTracker__ctor_m3283B209BAFB408E7B51A1076A700D57AF9BEA03,
	ObiSphereShapeTracker_UpdateIfNeeded_mEF48A8DEB3C0291FA68CE7C055F937D84ECB358E,
	ObiTerrainShapeTracker__ctor_m5BE8BB4A00AB9B0F79345BC17293B181C055C396,
	ObiTerrainShapeTracker_UpdateHeightData_m1B60D70BF4C97F8FAB0111534209E5CD81187D52,
	ObiTerrainShapeTracker_UpdateIfNeeded_m1386544C05562CA1EFD02614875AA5E35D8FEFA9,
	ObiTerrainShapeTracker_Destroy_m8447999AC2ABB7D960382E2389A70435614339E4,
	ObiCollider_set_sourceCollider_mB6E0C4C02CFDB78A515BCFD65B36AC736CECCCBD,
	ObiCollider_get_sourceCollider_mE7A6AFE352693E039E4A2193E85A89DFAA477D7E,
	ObiCollider_set_distanceField_m9F40D8CBE94B9CF4D7EC3C03ACBF4E64BA18F116,
	ObiCollider_get_distanceField_m5B1AAC65676531B209F9AEE01A7AB88B8279AADE,
	ObiCollider_CreateTracker_mB6CE9368E416DE23BF14AFA8940D5FAB5F6C8851,
	ObiCollider_GetUnityCollider_mCBAEB65DB24CAD8A6307DF33E3B1478EC9289627,
	ObiCollider_FindSourceCollider_m9EA19CA58432DEA55023A603332EBE1D7B38A7A4,
	ObiCollider__ctor_mA7A19D3C50B8FEF3314DF23590E0960A7B51BABC,
	ObiCollider2D_set_SourceCollider_mB7D2E58B7533B92207B297E43D4558226A350051,
	ObiCollider2D_get_SourceCollider_mFFEEF7B7C189C2DB7F7EAFA845940F272E60141C,
	ObiCollider2D_CreateTracker_m7C18F60D923D099896AA79AF0CD0B8377E17B326,
	ObiCollider2D_GetUnityCollider_mF64F76245333A4C93F5EE038EAEE37E9993CFD77,
	ObiCollider2D_FindSourceCollider_m8CFB1A9581B3FB148BCF0D4646413EBD22B0927D,
	ObiCollider2D__ctor_mC0D8A72F2DC693E755D6EB44CB6ADF22545CAD7A,
	ObiColliderBase_set_CollisionMaterial_mB448948B824EA9DBB3A06893CF9C63807BAFD896,
	ObiColliderBase_get_CollisionMaterial_m5A023C1F180D1BE92233EFE9979F8130F481CF34,
	ObiColliderBase_set_Filter_m8079D68C0979B0E1EDC41BC0B5C7FB83D3419CF3,
	ObiColliderBase_get_Filter_m9DDD807986F545F75BCE481F647CAD927CD24E7B,
	ObiColliderBase_set_Thickness_mC71CB40F6FD36ADAC380E0DCBBAAB927517B6815,
	ObiColliderBase_get_Thickness_mE2D9C1A79A127C7DA4F463B1A797EEEEA4061ABE,
	ObiColliderBase_get_Tracker_m417E63972B37E4BFBB287B2145D22936C835FEEC,
	ObiColliderBase_get_Handle_m0FF935FECD67E0572FD33B7A263B4D6BBD6CD55B,
	ObiColliderBase_get_OniCollider_m3EE3917FF4C2E9AB8484FFFAA94DE99AB24ED1E9,
	ObiColliderBase_get_Rigidbody_m3259D0421BBC02338A3674ECDB56ED3326E39AD5,
	NULL,
	NULL,
	NULL,
	ObiColliderBase_CreateRigidbody_mDD5A737E895830ACED373DCF669C9B77D7972760,
	ObiColliderBase_OnTransformParentChanged_m2A45BC7EAAB8EB8FDD52BD73BD293DBD90FC1535,
	ObiColliderBase_AddCollider_m87D30B63CDCD12DCFA5B99726C40AAE9E183B3FF,
	ObiColliderBase_RemoveCollider_m85CED7A46AE82E36B0A57AC4D966CB0F81A0964C,
	ObiColliderBase_UpdateIfNeeded_mC6C31FE4ACDD3FF7ADDC7522DDF89E87449CA699,
	ObiColliderBase_OnEnable_mB8C6F0690630D0EF5A6ECBAB6831420C039A1407,
	ObiColliderBase_OnDisable_m70147B944C32560377A84346CA8AE096DEFEDCD0,
	ObiColliderBase__ctor_m2EA5331A79C272EB6F66DCAD84236E4D2A973F4D,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiColliderHandle__ctor_m17783224C43CE7F08E87C29A8008D9B84237E88F,
	ObiCollisionMaterialHandle__ctor_m9E87A9AFA47FC9170EF75424886914F685C752AB,
	ObiRigidbodyHandle__ctor_m070BDDB71AB612702373D37CC0286BCBFC13B958,
	ObiColliderWorld_GetInstance_mCD5E33B37AD66058E1F0A61B7CDAD98CFC13896B,
	ObiColliderWorld_Initialize_m1622C98EC99C31AC52BFA11404D9A4E6341FA168,
	ObiColliderWorld_Destroy_m4CB7C99F9039703E1C97CFE18C4E9D03CD8CC7D0,
	ObiColliderWorld_DestroyIfUnused_m451242730F7B0F766F09EF65DDF77C39F18032AB,
	ObiColliderWorld_RegisterImplementation_mF4B7F94F3C9C0DBAD95A575C3C9158F7F5C30847,
	ObiColliderWorld_UnregisterImplementation_m0982CC62EC1588205C9B4F5473F8D30CF8C9AE6B,
	ObiColliderWorld_CreateCollider_m963AE24C299ECEB00697F39F517C045D28B42352,
	ObiColliderWorld_CreateRigidbody_m30D27D11F8AC419B9DA2716357E9C22F68E4CF21,
	ObiColliderWorld_CreateCollisionMaterial_mFDEF23E92D27AA612EAE0878A4856D0D5CF44B0E,
	ObiColliderWorld_GetOrCreateTriangleMesh_mAE3E43A75C4023F4D8DE9ADA5194B98EC0332337,
	ObiColliderWorld_DestroyTriangleMesh_m9EA39BD3167CC3ABD3B04AA47DC7E369E9A93DEF,
	ObiColliderWorld_GetOrCreateEdgeMesh_m63794D698A5B13DEF65533803BEAF7B31E12B070,
	ObiColliderWorld_DestroyEdgeMesh_m1489982CEFFF55EC6E86B89632F9C2186915180C,
	ObiColliderWorld_GetOrCreateDistanceField_mF0D24E094B57DC3B6D41D8FA8CAA50F6EC092E2D,
	ObiColliderWorld_DestroyDistanceField_mB6EBF7DC9BF9BA21B8E3D1C338D24CC0B2CC2151,
	ObiColliderWorld_GetOrCreateHeightField_m885D1297E85C101D8A258C4A5FEAEBE5D314CDE4,
	ObiColliderWorld_DestroyHeightField_mBDFA8AC5F63970485468C0B9CC355477CE28920E,
	ObiColliderWorld_DestroyCollider_m1C78622703C9D5D9F921C2BBF1CDF52FEC09D648,
	ObiColliderWorld_DestroyRigidbody_mE027D141E82F35FC032E341CA060DC356F5AE85C,
	ObiColliderWorld_DestroyCollisionMaterial_m80E9D5CE744153FE3A1B78C3CC336CA1D18CFDF2,
	ObiColliderWorld_UpdateColliders_mE22BD1C71C6CE311AABEB3EBE5D5D89499040594,
	ObiColliderWorld_UpdateRigidbodies_m879FDF5DF7AC2C45A626701DC6C4476A7FCBB2C4,
	ObiColliderWorld_UpdateWorld_m0A22F19C866A4E18E5FE92590F12C269D9B5F6FC,
	ObiColliderWorld_UpdateRigidbodyVelocities_m8EB632E1D181278AB86439D2735C4C1EDA2AD58E,
	ObiColliderWorld__ctor_m56C3C728C87F653030D89BFB4712AE3A2004838B,
	ObiCollisionMaterial_get_handle_m438ED1A35E14A7DFB14C7837753BC8A264222FD7,
	ObiCollisionMaterial_OnEnable_m360351035E55863B4483333BAD71D081E7D81ADE,
	ObiCollisionMaterial_OnDisable_m2E9697F842115512DB5B61E12D6C37E4EAE973B1,
	ObiCollisionMaterial_OnValidate_mC643AB6D6EF7EE34A3564F5F8B5A5819E4818B08,
	ObiCollisionMaterial_UpdateMaterial_m9BD91C9E69976192F9188E6C5D7118F013C511CD,
	ObiCollisionMaterial_CreateMaterialIfNeeded_m26C56B4E0DEA4EE4FF06556BC0084F9EA68A512E,
	ObiCollisionMaterial__ctor_m1C64ACA96EB44DE0F823DB8B910A82BCDC1A92FF,
	ObiDistanceField_get_Initialized_m51FA1E219A856ED9DF246BCD9951CC1948605DA3,
	ObiDistanceField_get_FieldBounds_m4BC90C7A8CF1B7CA63BB9C99FC7F6093C0D29B3E,
	ObiDistanceField_get_EffectiveSampleSize_m37382132C97730D8B5824BA8A84B4510DEDBF958,
	ObiDistanceField_set_InputMesh_mD156CA7F4C12480B991AB72C9F0218EE9022F0ED,
	ObiDistanceField_get_InputMesh_m2C0F67AF519997D7CBD4E8626636DB1DCABB728C,
	ObiDistanceField_Reset_m14516F7F128BCD2234B72205927D9A7EDFA42DB8,
	ObiDistanceField_Generate_m5BBC55C56A02B95B313221B58F95004033901A71,
	ObiDistanceField_GetVolumeTexture_m36F78A8377730BFAEDCAFF68B750F005AC197D67,
	ObiDistanceField__ctor_m188C8102DB79494D3F9FE0F286652323A6FB6B0A,
	U3CGenerateU3Ed__16__ctor_mC63A126BE81683635ACE94161C2093665F30E781,
	U3CGenerateU3Ed__16_System_IDisposable_Dispose_mFE056A6ADA7A7B1DF5166A9BBCBA238839740669,
	U3CGenerateU3Ed__16_MoveNext_m9B9BA3E313245C4EF7468A5498E71CD80110FD0F,
	U3CGenerateU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE7BA86A13141744082109A0FE7E962CB2B6ECE44,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_Reset_m78EBB3B25EFC2EBCFB06421B5A63B8AFC2EE16E8,
	U3CGenerateU3Ed__16_System_Collections_IEnumerator_get_Current_mA78DA3FA3FABDAD4670B5666DD18AEEF89278B5B,
	ObiDistanceFieldHandle__ctor_m8142EF48648C875A3C61BD530D34FDC7F53D123C,
	DistanceFieldHeader__ctor_m251B2C29894323C5CEED156849B443EA8067C679,
	ObiDistanceFieldContainer__ctor_mF7C40EEFA07738812C54174ACB370AA3CC0AF125,
	ObiDistanceFieldContainer_GetOrCreateDistanceField_m1375B8E8147209EC9172CAC53AC13876980B0C9E,
	ObiDistanceFieldContainer_DestroyDistanceField_mACE5BE92EF022F795071C3BA7A9B07FCFB7A4513,
	ObiDistanceFieldContainer_Dispose_m24F3E963532F4E2929E09ACD6BE70C93DED0E4A7,
	Edge__ctor_m77B0841455A2B88452B465EFBC8FFEEC0A67E648,
	Edge_GetBounds_m644FB98AE4FEA8DC1BCEEFF06588BEEBF7F7261C,
	ObiEdgeMeshHandle__ctor_m3540F335B471820DA6FDEE0DB8F18C59818C13E3,
	EdgeMeshHeader__ctor_m877A47F9943627D383EFDCEE3A19AE256A9B55A2,
	ObiEdgeMeshContainer__ctor_mF79F760DC56967863D567A80D6AE3D1EF932377A,
	ObiEdgeMeshContainer_GetOrCreateEdgeMesh_m467071EAE6703BDE659CBD0A9D2902445B25EC25,
	ObiEdgeMeshContainer_DestroyEdgeMesh_m0B9DF3A0F9643DEDF1E83A849EA8F71245F80460,
	ObiEdgeMeshContainer_Dispose_m5F57B4FA53FCDFFF2734E2A3700B0DB3634DFBE8,
	U3CU3Ec__cctor_mB91D7183ED2FF1510CA841B903C11A8884B5AF12,
	U3CU3Ec__ctor_m9A67F61F2CACE6809F15597CC0C6B5E4393B32CA,
	U3CU3Ec_U3CGetOrCreateEdgeMeshU3Eb__6_0_m5F0AC709621AA79274676593791DE6D2BE3B3F10,
	ObiHeightFieldHandle__ctor_m1D23D11A632B7ACE4BA79A5EDA634BE30B8D9064,
	HeightFieldHeader__ctor_m262BFF2169E5E485BD00BFACB85132FFED62CDDA,
	ObiHeightFieldContainer__ctor_mBAF588A2F72128D62B8DA5E88B0F4BD459CA67F7,
	ObiHeightFieldContainer_GetOrCreateHeightField_m8A84C54CDEA3B9D05CB127B05F3B222B7A3A5517,
	ObiHeightFieldContainer_DestroyHeightField_mE07C682D4706ED975AFD022D4185DDB9FF7E0F19,
	ObiHeightFieldContainer_Dispose_mF2DFE76857E3BC440CCA67D730DA733BF6417DE1,
	ObiRigidbody_Awake_m2500E98F62ECFB5EAB8FBD4DA3BD7B8BADFC38AF,
	ObiRigidbody_UpdateKinematicVelocities_m35263D021F8ABCD1E6D9365099AD62F704F760BE,
	ObiRigidbody_UpdateIfNeeded_mCCA1526686277BDC8BEF5ABDE78418FEF58FF502,
	ObiRigidbody_UpdateVelocities_m63DF8D11F83CC06ED311E367C2AAF3BF1CEFC765,
	ObiRigidbody__ctor_m31B6718C1536A75766D906B61A2ED338CA2F6DA0,
	ObiRigidbody2D_Awake_mF371E9238C48582C8CD00102B0A376D19D1DA41C,
	ObiRigidbody2D_UpdateKinematicVelocities_m4D4BFB3E9233E2077E20FA5BBAC7013825C7DE2C,
	ObiRigidbody2D_UpdateIfNeeded_mE7950E3FDE20A2F31C580E7E97650C53CFB23B49,
	ObiRigidbody2D_UpdateVelocities_m074D762E5B5A124E43C3F484599BBFC7E520FABA,
	ObiRigidbody2D__ctor_m56721BA63D2702EA5238878A7F92EA70AE0043D2,
	ObiRigidbodyBase_Awake_mC07E66761C016AB8FDF7AB08D512879662497090,
	ObiRigidbodyBase_OnDestroy_m7E9F48EE65481EB05B9ECCC137F7C2614B96BDDD,
	NULL,
	NULL,
	ObiRigidbodyBase__ctor_mF6C7EADE7F12CAB86D1127BF465CE41DA8A6823A,
	Triangle__ctor_m15AEE3A9EEE96FA1698DED44AB98949A1E11CF62,
	Triangle_GetBounds_m56E0F7268C436A9036CF11094F585C3F1AF0F998,
	ObiTriangleMeshHandle__ctor_mA53756E4207412B3797147B9FEAE74EEABEA2B94,
	TriangleMeshHeader__ctor_m8EF03D0DD1903087DE66A92EC3433EF5C7CF9B91,
	ObiTriangleMeshContainer__ctor_mA876587074B4D103DC187C5C58717CC31779D6F5,
	ObiTriangleMeshContainer_GetOrCreateTriangleMesh_m3C4CE0D177EFFC4DAA15DEC342AAB8A920AA98C0,
	ObiTriangleMeshContainer_DestroyTriangleMesh_m9D3FE0FAA7920CFC9867069366D32D99FD9419E4,
	ObiTriangleMeshContainer_Dispose_m1DBC5B421ADAA3FC63B436085ADAE7E58A3E5ABF,
	U3CU3Ec__cctor_mE53EE44077253A5B7024F9C66840122C2A6A5F00,
	U3CU3Ec__ctor_m2319BE74D6B1AE87A476A702034B945AFFA77134,
	U3CU3Ec_U3CGetOrCreateTriangleMeshU3Eb__6_0_m7C65E4FD87AABC0680E624DDEFA344EC37F00399,
	ASDF_Build_m599FAE6C66EEBD1725DF976C19BE85252B84AAB1,
	ASDF_Sample_mB51F949A6EBC7CDD272A95D701A03D3C9EAF214B,
	ASDF__ctor_mEEE94E12CC5E1D95F89C5A5764324DD19C15B430,
	ASDF__cctor_m908672B45D21B9190763820B7A92E7A06F7D8C86,
	U3CU3Ec__cctor_mE615EAAC566DCA8C8BF282F8A2426476AD3B933C,
	U3CU3Ec__ctor_m6A067ACABF8B8DE4140F3E690C0C2D203F80C085,
	U3CU3Ec_U3CBuildU3Eb__3_0_mB8DADCF561B221B1D942CA05BFC2AAC959193F92,
	U3CBuildU3Ed__3__ctor_m4016CC8A07B3D07FA4B6170E7F0D91FC5D9477C7,
	U3CBuildU3Ed__3_System_IDisposable_Dispose_m0ED848778C1B1DA878C592D33556DC186F97875F,
	U3CBuildU3Ed__3_MoveNext_m174CB894CC91416CBE97B9F030A5F17A7E2BE964,
	U3CBuildU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m718A3D5304A2D96072BC702908CAD7DB869413DB,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_Reset_m529D739355FAD14F1C306A09AEAB1C3E80918F21,
	U3CBuildU3Ed__3_System_Collections_IEnumerator_get_Current_mDC66AA1DCDC338CCBEA02F0929A28562BBBEFBB2,
	DFNode__ctor_mB94060EAF88B6233E60AB935634F6716E502547E,
	DFNode_Sample_m14246F91889EC3E920F108B2B406FBC4E44CF124,
	DFNode_GetNormalizedPos_m335E725D7443558FD8C47CFA490D1BC29034917E,
	DFNode_GetOctant_m5808A88E16C5E0EEFF5E1CF2E66040C92961E884,
	Aabb_get_center_m34EBD23AF65E6E8B49FECCE4F4C285610D38F823,
	Aabb_get_size_mC66C740C1B769CEA883531AC10D85CAD0D4E5657,
	Aabb__ctor_m4465FBAA62A29E90A0BBEC56EF534EA9582A8AC4,
	Aabb__ctor_m59BB63BC91E612A9C3ADDB5106EAE996A0BD4465,
	Aabb_Encapsulate_mAEA769FBEDA585C1186F2D1CB4BD06A880EB8209,
	Aabb_Encapsulate_m832BC3BAF98AD3CE9B8F36C191777681E6F8FD6A,
	Aabb_FromBounds_m27239365E8E05E7F09B43FD655D220D115610763,
	AffineTransform__ctor_mCA54E543BC65B018A5C94C2CE89E325ECA1BB6DC,
	AffineTransform_FromTransform_m82766F8868CBFE2E7E74A50669374B8987C8BAD9,
	BIH_Build_m5055AB9B932CC1969508C1B60B55F02612CA1E4F,
	BIH_HoarePartition_mDD8939FC005C1FE69F393A0EEB98B7C3BB34521D,
	BIH_DistanceToSurface_m9BE974D87EC72703A9FF6273F4BF3F8D56769889,
	BIH_DistanceToSurface_m2D97F2A06A1887C561BB87392DDD40ED1EC69033,
	BIH_DistanceToSurface_mA5C9FE2858F60370476EDC20BD72DC1B91432FB5,
	BIH__ctor_mAB45D6F1B6D5CB3FA62A019FEE91C03E1B09255B,
	BIH_U3CDistanceToSurfaceU3Eg__MinSignedDistanceU7C4_0_m9A0320882A54C615D81A3CB5E680A67D1D0B35B6,
	BIHNode__ctor_m311091C6FC6855D66C9272ACBEC22AFEEF02745A,
	NULL,
	CellSpan__ctor_mBAA579B4157A8ECC1A3AF1945366566B354D74E1,
	ColliderRigidbody_FromRigidbody_m8950C5EDEC5F5FCB2450A939D11E1B7F8F51CA51,
	ColliderRigidbody_FromRigidbody_m634E6B8C72367A4FE8781687FDA571001810F64B,
	CollisionMaterial_FromObiCollisionMaterial_m57CF8FE723A8624F55070A837BB0ABEF6A196750,
	ObiNativeAabbList__ctor_mE40545EA5EFFF80CA76DAAF3D3571267F9EE89F9,
	ObiNativeAffineTransformList__ctor_mA8B296672E8B0C15E7BFB40F7381B9613623F4BB,
	ObiNativeBIHNodeList__ctor_m430D2EB3BAA8014894A0F18122946D3C85C73509,
	ObiNativeCellSpanList__ctor_mB716A81D32E22A0F55B6088B67A82EFBE1BE307E,
	ObiNativeColliderShapeList__ctor_mE2DC8FC7669A1C8BD97FDC6FD9DED79524D1F533,
	ObiNativeCollisionMaterialList__ctor_m358B6ECD8E212C156724B2D23371158C6C279316,
	ObiNativeContactShapeList__ctor_m513DC1E745FB1ADBE279FD5807DE6EDD6DCE9A50,
	ObiNativeDFNodeList__ctor_m87B93C4B0D173C7DE21A5E337D9538C1AB6D6022,
	ObiNativeDistanceFieldHeaderList__ctor_m7068242C8C2A4311C6E7BB10EDDB4076C985BEE6,
	ObiNativeEdgeList__ctor_m871E11B58D2BCB410CEA186C77C8B728AB09FB71,
	ObiNativeEdgeMeshHeaderList__ctor_m495EB672AB7FBD542D61549A6DB8CD21DC474216,
	ObiNativeFloatList__ctor_m084E2652786A6504303E0DCC85A406961F7AAF79,
	ObiNativeHeightFieldHeaderList__ctor_mF46A488EFD6B00C4EBD9AAD9676B4AB1FF06BD8F,
	ObiNativeInt4List__ctor_m65C32CF777FA034666EFC366B12EA10C45F2F759,
	ObiNativeInt4List__ctor_m4995E584FAE3543E4F5A4EF1C53759C19545A1BC,
	ObiNativeIntList__ctor_m1EEAF238780D9B1B982D5E6B3BEF28F7395D5D3A,
	ObiNativeIntPtrList__ctor_m8BDDFDE672234C458B67EBB6E0E752C1476BED6F,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiNativeMatrix4x4List__ctor_m28F259B479F3B9E6F07ADB7914EEBDEFAD7490D9,
	ObiNativeQuaternionList__ctor_mEB7CF253CBE003F21042E1CB01294E88EB8C12CC,
	ObiNativeQuaternionList__ctor_m9E3FA5D8D6E2731ABE98E70F678A8D4230C9EC20,
	ObiNativeQueryResultList__ctor_m0897E63A9D46652B53DD4D6B7092312182C9B35F,
	ObiNativeQueryShapeList__ctor_m00DAD9B29DDD69C6463C76B0341C690EFC689191,
	ObiNativeRigidbodyList__ctor_mE735CB74C4A310ECCB3AA2C7BBBB2555D0E160D6,
	ObiNativeTriangleList__ctor_m5688D7FB37A7FDC199F3874A3C77DBE36B72D599,
	ObiNativeTriangleMeshHeaderList__ctor_m7D18947097366047DD11CD50EF5061F0143B52F5,
	ObiNativeVector2List__ctor_m21C34792CECE609072CF39C13D56C19028D75266,
	ObiNativeVector3List__ctor_mBC0740F244904D22E754FD2D230A756FDBD4ED15,
	ObiNativeVector4List__ctor_m5D4C8EFE0AFB31C8D76FF9D6BDBFEEE7C9A19A7F,
	ObiNativeVector4List_GetVector3_mB077F2EA7AE13E15FD28521E63F39CB1C03392DF,
	ObiNativeVector4List_SetVector3_mB2128317C9A45705A27D05B4F496DAA1B5557409,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ParticlePair__ctor_m1ABA5CF889DBC5A410CE57D2A9E6F1F7D7536257,
	ParticlePair_get_Item_m9D26C2D704AA4EE04E5B20ED21AE193CD4A4530A,
	ParticlePair_set_Item_m9AAF33F1EC57D519A5C6267044F2A569B0E2E156,
	QueryShape__ctor_mAB2B92EF1A8A1E13D04424FC3483D6990E64C959,
	SimplexCounts_get_simplexCount_m71F95AE072FC89EBD43364AD9F5D99B8A4AB4C3C,
	SimplexCounts__ctor_m87B2910E8FA9E6B152BF231CF0B85B8B1BC144AD,
	SimplexCounts_GetSimplexStartAndSize_m1C63A710053493E24CDBEF324C8A66B27F871641,
	VInt4__ctor_mAB09F0C216D6336E655285359F99CAC21749BD51,
	VInt4__ctor_mBDE8910E0F589EDC34EF21F9A4DDC0D3C1B4551E,
	MeshVoxelizer_get_Origin_mD71BC7FD4959DA95E367367BDCF4AC2B1418C7FE,
	MeshVoxelizer__ctor_m98FE768E9523B1EA295179545E63C4C60DB1B0B4,
	MeshVoxelizer_GetTriangleBounds_m68E1A2C5887C890FEF553730AF918C67242E0548,
	MeshVoxelizer_GetPointVoxel_m6916482E899ECA9A98C999F42595AF4CFC67BC87,
	MeshVoxelizer_VoxelExists_m359D018EAFEF8C1C155B4CFBB0B24DC7E96DEF48,
	MeshVoxelizer_AppendOverlappingVoxels_m2F726421AFF148505220F11E52121A4D8F693C21,
	MeshVoxelizer_Voxelize_m420C1E5B81A86A0044AB58EF7A066DE30BF01F83,
	MeshVoxelizer_FloodFill_mE886DAC7929077EC07AA10A1115C5B6EC0A161D7,
	MeshVoxelizer_IsIntersecting_mF672978BF03383F3B19AEFA50BAF0F12B7F1842B,
	MeshVoxelizer_Project_m2E3D7D935B5BF56A41D0E4160082C6CB6DF62C99,
	VoxelDistanceField__ctor_mCD081DCBC2E4D47C4D58542400087DE4A5833695,
	VoxelDistanceField_VoxelExists_m91BEDE07CEAD13732C899075999B86F992E4BF54,
	VoxelDistanceField_JumpFlood_mDF5484ED7C02D14395FD1089D1011F1420ECFD75,
	VoxelDistanceField_JumpFloodPass_m93C4304B47AF0177E35140AA583AC97546FA8A35,
	ObiDistanceFieldRenderer_Awake_m3E4426F1E774910E865BC75833FF9E613D55DBE6,
	ObiDistanceFieldRenderer_OnEnable_mB1D255D6F0A3B18AE7DE6C236343276F6030A442,
	ObiDistanceFieldRenderer_OnDisable_m7A0F0988A96F5AEE90512F9732E241F0D85A7A56,
	ObiDistanceFieldRenderer_Cleanup_m106CC736B24C9ACF8FB69F069D368BC31A9CD1D0,
	ObiDistanceFieldRenderer_ResizeTexture_mC5521E1F05E4AF5B98F10CC88C1E20E39859F500,
	ObiDistanceFieldRenderer_CreatePlaneMesh_m736BE4B002234C1577D6A02B93E3481B19F9A017,
	ObiDistanceFieldRenderer_RefreshCutawayTexture_mACD4B3F520E069C362B3D1E2A52284E7EC11A02D,
	ObiDistanceFieldRenderer_DrawCutawayPlane_m9CF65A4FC7CAF54DCCC57AB59D55F6BCC4F322C9,
	ObiDistanceFieldRenderer_OnDrawGizmos_m7C4F4718288BF910BCF17DD09AAB6FDA9D544B10,
	ObiDistanceFieldRenderer__ctor_m191C4F3AA8C9AB2EE1037FD40CF6A000D9E05E5F,
	ObiInstancedParticleRenderer_OnEnable_mDC5A6A92920AABB2E5781EED72D375EA23A69F5A,
	ObiInstancedParticleRenderer_OnDisable_mBDCC90E4447ECA01685F85BDCECD0E32E54433DF,
	ObiInstancedParticleRenderer_DrawParticles_m465A516F2A7C36BB1267C92E076C7402498D508E,
	ObiInstancedParticleRenderer__ctor_mF3C04E4B127A3068AC43A816933CD2706642DA2D,
	ObiInstancedParticleRenderer__cctor_m21AC0645F738EE4A24E85330E5A4A9BF8CDA5668,
	ObiParticleRenderer_get_ParticleMeshes_m923C2E62304362F78C82CFCCE4EC65162E760E85,
	ObiParticleRenderer_get_ParticleMaterial_mC7BED89475A9E3469A2CC58DA36D29A3F9DD57C2,
	ObiParticleRenderer_OnEnable_mB97E2BDC1615330D8FAAA9BAA48AD4A0C99A4A48,
	ObiParticleRenderer_OnDisable_mEDED7F7CD25B727F135E73A8ED72C21969BAD494,
	ObiParticleRenderer_CreateMaterialIfNeeded_mA44AC8DFCA92BFE9AE11422167F33B0D7D901C8A,
	ObiParticleRenderer_DrawParticles_mFCAAF65EFBA92B57A9D7A4375BBDF9445DE9C6B7,
	ObiParticleRenderer_DrawParticles_m856963BC279329E37190C6CF90F63C8E8C92AC8E,
	ObiParticleRenderer__ctor_m49B47769D78FB2D77D03F05A8704FE6D708BF304,
	ObiParticleRenderer__cctor_m2E9ED3F12F056C26A171CCCBE28AEA535AA87E1C,
	ParticleImpostorRendering_get_Meshes_m8D60CAAFF281436EE387DBE3D60FC565B33ABE5E,
	ParticleImpostorRendering_Apply_mC2D0ACEA779553E33C7B278035CE12772FB302ED,
	ParticleImpostorRendering_ClearMeshes_m1AD7671FAF0826450931CB9A632E387989730CEA,
	ParticleImpostorRendering_UpdateMeshes_m9140980F5151F0C30AB2FCACDD1BB92153F38292,
	ParticleImpostorRendering__ctor_m5BD794C9146FE8A187E9934E6F381942DF58C71C,
	ParticleImpostorRendering__cctor_mFFCA0EE4575E08A0FF122713FE109BBFAC41295D,
	ShadowmapExposer_Awake_m7F124F583C2E1C1069ADF356564DC613D3118E9F,
	ShadowmapExposer_OnEnable_m6289A9CF5D1278997659358163B5C00676BDD883,
	ShadowmapExposer_OnDisable_m41264EFB804DB02AFC4B866A3FD564A627328B27,
	ShadowmapExposer_Cleanup_m08B86D9F51C0B21DC5417AE818143FA0DB4C0001,
	ShadowmapExposer_SetupFluidShadowsCommandBuffer_m9722E09E25C549C260E10A9C5659136B0C1CA73B,
	ShadowmapExposer_Update_m3A8254DAF98E6E2E432000106C3E533A447A8F1D,
	ShadowmapExposer__ctor_mAC792FC6B235D517AE0D56D4FD986CEE4552FAA3,
	ObiSolver_add_OnCollision_mF3DE133AD6D5E0BBBCCEC7EA3E66D929F8F3633B,
	ObiSolver_remove_OnCollision_mB635DB605B5D19D3EA177631CA5439BFF1A8D1B5,
	ObiSolver_add_OnParticleCollision_m31CEDEA9AA2D8A506A00A2D2D86E8DCE16014AB8,
	ObiSolver_remove_OnParticleCollision_mDC61C20C6D02E36B0B42E57203F932DFD3E6A74C,
	ObiSolver_add_OnUpdateParameters_mF5F6CDCBE2937711ED779A336A41E4A9F09BFAFF,
	ObiSolver_remove_OnUpdateParameters_m794890967633B6ABC38A06BCEBDA388A95FFF02B,
	ObiSolver_add_OnPrepareStep_m5547752A41B84A5A2E7633F714D487836B587CE8,
	ObiSolver_remove_OnPrepareStep_m67744EB2A8D5537C0C395623CD9D5B0F50E74B6E,
	ObiSolver_add_OnBeginStep_m697F810061AEDD54F6C00780617C93DAB9ACE577,
	ObiSolver_remove_OnBeginStep_m245376053A288393AC90A4C87D7735A775669F3A,
	ObiSolver_add_OnSubstep_m74A3A296E17B831E20B660AB20E5A5C93E7AD793,
	ObiSolver_remove_OnSubstep_mE68833452CD48337CF57F728508464BFF40A2BFD,
	ObiSolver_add_OnEndStep_m32A01A15E29E0FD7197A3274B16B06627D5B97AB,
	ObiSolver_remove_OnEndStep_m4256367002E41AAFF8F8E1A77E899047CC753C03,
	ObiSolver_add_OnInterpolate_mB678D7773AE4C54FF3B95B037B4E1A2B62E93FDD,
	ObiSolver_remove_OnInterpolate_mCD70D73217DE553A814A4DDF15344D28FC6A4D8C,
	ObiSolver_get_implementation_m9F2CDFEB83D2A934ACB9BB40DC1AFD915B18A19E,
	ObiSolver_get_initialized_m586F23FD6F4749B03B0079D1B2A9D33165C798F5,
	ObiSolver_get_simulationBackend_m216408A308F32F19ADC5AB6618FBACADB1437A71,
	ObiSolver_set_backendType_mA247F3405FC0E70B541DA283DC816A39F2B70516,
	ObiSolver_get_backendType_mCEAC2DE80955390DC501F700FA3AD5F28CD593B4,
	ObiSolver_get_simplexCounts_m991E746582686190DB673974A06446DDD330CB27,
	ObiSolver_get_Bounds_m44EA3D10C353AC78AC16FBA599540D93859BDCB8,
	ObiSolver_get_IsVisible_m45379E9399A54D9D79F0984367438EDDC145FA17,
	ObiSolver_get_maxScale_mE496805FAB7D4C7273D7F06A1E9E074DA574AED4,
	ObiSolver_get_allocParticleCount_m54EE5AF265571EBA043DA09233C41D9F9E22910E,
	ObiSolver_get_pointCount_m2138390024C8A2E8F087D1411C198623BE88D84C,
	ObiSolver_get_edgeCount_mA206ADAEEF71C25CD106E2B45CD68A64E3BAA1D0,
	ObiSolver_get_triCount_mB99127D38C39BB12017009B3BFE5CE3F285A59C1,
	ObiSolver_get_contactCount_mA1423B73BAE2F730068A141340319262DB0F8D5A,
	ObiSolver_get_particleContactCount_m4D7418A6ADED5531781C2C97E3F9E93F1ED78751,
	ObiSolver_get_particleToActor_m8C24527E4AF167BF38FBAA41CBEF35C7E918002C,
	ObiSolver_get_rigidbodyLinearDeltas_m245881A5A99F88E1F6F0D3E7F470F8EF446EC078,
	ObiSolver_get_rigidbodyAngularDeltas_m44468761D636C511AEEB712F8074643A29D92060,
	ObiSolver_get_colors_mD936B7EDEBB6324E6EEEEA7B8BA34F9E4C99FCDF,
	ObiSolver_get_cellCoords_mEAB574A55A49F74BEE1BC9461EDAEFE971AD6456,
	ObiSolver_get_positions_m0C88959F7FD733D60EE1E08074ACA6B2DC6EC35D,
	ObiSolver_get_restPositions_mD6D8BC6C5ED0DC8B6DC1D8F7FEBAC5D2EFD9AE68,
	ObiSolver_get_prevPositions_mBBD0165222ED8AD2211A668F58CD0B1DFE984920,
	ObiSolver_get_startPositions_m9BF78FEAA535DA234D4785F2C7BFABD11BFE3279,
	ObiSolver_get_renderablePositions_m7807CBAD597B97FCF5FC1C7184D168F01C97E8D8,
	ObiSolver_get_orientations_mDD97AAB61F0DE2E6156B576946EE85482A36D495,
	ObiSolver_get_restOrientations_m9AC87DDE8ED8D54823805ACB1848782C09ED92C1,
	ObiSolver_get_prevOrientations_m41AF1E74113EB276342CCBBE27A067506F8AC0FF,
	ObiSolver_get_startOrientations_m14958738016263B46143EB1AB51385579C93504F,
	ObiSolver_get_renderableOrientations_m6315C54D34044153479A72F89B7F0E6AEB074E46,
	ObiSolver_get_velocities_m6EADFFB2BA3663150EA1AE4BAFEB3502914D4817,
	ObiSolver_get_angularVelocities_m2B5062FB47C4F47ED9D6DB85BFDAF12E06201568,
	ObiSolver_get_invMasses_mEAD2D3480FDDF75E32C5951A998CBCECBD24D3B3,
	ObiSolver_get_invRotationalMasses_m07B879855F0838D371BC5F64EE79C722E6EC75C4,
	ObiSolver_get_invInertiaTensors_m10FFFA6753076888A60BB29B828E6DD2AE39C858,
	ObiSolver_get_externalForces_m8C5E7D9722D7535C890003050858B2D4BA7634A7,
	ObiSolver_get_externalTorques_m4BE93F20AAD6068A04FED46FE15720B2B4668B70,
	ObiSolver_get_wind_m794C8FCFFF227D75C5E064EB592F995D99D668B4,
	ObiSolver_get_positionDeltas_mEDABA21B58E0ED5F314302BC505578EEFFDEAF65,
	ObiSolver_get_orientationDeltas_m7553005E502BE4300578DD6B42B9327DBB359D55,
	ObiSolver_get_positionConstraintCounts_mBEB32A98E26252D0AF555A30FC233EEA12D03746,
	ObiSolver_get_orientationConstraintCounts_mE778057F1D89CB8AFFD4F0E1B47D6C59CA51AE98,
	ObiSolver_get_collisionMaterials_m763C9617995F1AA97F44A98AC8C4E6451D79F437,
	ObiSolver_get_phases_m710DD563003F4AAA696450EC266F2CEF699DA71B,
	ObiSolver_get_filters_m802746B5BFEAF09C501A5DA28057F6698FBC745D,
	ObiSolver_get_anisotropies_m417C6BCD4180A9A33D325D8A56BC3982EB0524A9,
	ObiSolver_get_principalRadii_mFD708006AF8833856FC8A3388B3DAC453CD0CC92,
	ObiSolver_get_normals_mF5D5F729DD254D5C4589439BF80094E99A6BB073,
	ObiSolver_get_vorticities_mDBB35B67BDF862A0EBDAB5B03FBD44FB16B61A8E,
	ObiSolver_get_fluidData_m15AA21C3B2A390CD1EB4D1F9447A9A91E0ECAFA4,
	ObiSolver_get_userData_m1EE0A46AB278CD90EE43984496EB7C47ACF0F5D5,
	ObiSolver_get_smoothingRadii_mB786B31388B6800F59F07607B77CBF076599B422,
	ObiSolver_get_buoyancies_mFEA15451B676D494D1BDE5679D03C17124C1697A,
	ObiSolver_get_restDensities_m8D042F798C98339CE085260D8F81269CAD122172,
	ObiSolver_get_viscosities_m11E76D9C7E1BCCD395F024C7F0D9C0A2F3965E23,
	ObiSolver_get_surfaceTension_m96FBC03646C165F1CF91A0D80E6A506B0C571358,
	ObiSolver_get_vortConfinement_m28757BBCC63B9D797D78F9394D9FB6F36073087D,
	ObiSolver_get_atmosphericDrag_m46B1B3645BB3F3AC6920E53F49032EF9D7A2930F,
	ObiSolver_get_atmosphericPressure_m97A89CCC57CD237DDE740ABA272874AA1F9D5AA4,
	ObiSolver_get_diffusion_mBB44EC5D7C1ECC9FD2994FC121F9161750F240F4,
	ObiSolver_Update_m9ED3E3A02C9973F36132781D6AEAEB20CE5061DE,
	ObiSolver_OnDestroy_mA8F7B6138D59E738D0883567701A1664FD187DF0,
	ObiSolver_CreateBackend_mD8BE03B36506903B1F388B0DC214DD817FD1B798,
	ObiSolver_Initialize_m89B29A40A3418F087F30611AFAD542DD1AFC16FA,
	ObiSolver_Teardown_m0FF521346E884F8AE239BAC4A1835722A2550A7B,
	ObiSolver_UpdateBackend_m027C131E805EC2A9A36FBFE54B3D95E5A8F76BB0,
	ObiSolver_FreeRigidbodyArrays_m7D6EEFD9184E2E8767D47FF4882AA01FB6DBAC71,
	ObiSolver_EnsureRigidbodyArraysCapacity_m821AF4A6183257D4C939236A5D4EF8938259BB59,
	ObiSolver_FreeParticleArrays_m761E1045358FB28F88DD93AF49A9755C49E1CEE1,
	ObiSolver_EnsureParticleArraysCapacity_mB711BB3673CA3C04BBD363EFC78EE5FB29AA0278,
	ObiSolver_AllocateParticles_m2E904400BE8CD3551AD991B7BE20F49B3D8A99B9,
	ObiSolver_FreeParticles_m58E563D22C726EB41700F77AB8D0DD429111AD62,
	ObiSolver_AddActor_m659F33D07DFB3765DC89C6BE5618CDC8B3F3CE62,
	ObiSolver_RemoveActor_mF70200B7D46C8BB2F6488A15A429F99CD25B669E,
	ObiSolver_PushSolverParameters_m9B8F26085CB523DACA1DBE26C1BE132E92291AE5,
	ObiSolver_GetConstraintParameters_mFC75278F5EB861E2A14855A7FCB6B31B1C68BDD0,
	ObiSolver_GetConstraintsByType_mAF65FA4A57FF5231A7F958468B08919364BBF14B,
	ObiSolver_PushActiveParticles_m711160E900FAB1C9612ABE8E64DF3580D298B18F,
	ObiSolver_PushSimplices_m26F6066BBC71182C229F61991BEEB12D7C455A8A,
	ObiSolver_PushConstraints_mD5C51F2BE7021B9BA8C71BDF142D485693228C56,
	ObiSolver_UpdateVisibility_mE67BDBAD691D6C9E4163D5239B7B203E1F78FC46,
	ObiSolver_InitializeTransformFrame_mB5F807A577821103ED00EC2F04D06EC2F130665D,
	ObiSolver_UpdateTransformFrame_m149D2F3207C6955651F175B9FF035EA5B6CACB99,
	ObiSolver_BeginStep_m0209B4FE6F55F359D380B81628BFB93DD7171E4B,
	ObiSolver_Substep_mC87665C9B26AE616BE2846C00294D9656FBE4AB9,
	ObiSolver_EndStep_mB69D4B5498573F533F766596BDA938AAB4121B51,
	ObiSolver_Interpolate_m76EE8F8FF90774103389F56796B5158394B9FCBA,
	ObiSolver_SpatialQuery_mAC8DE6B38410845DA38B8415E8A9A7591F45938F,
	ObiSolver_SpatialQuery_m97851526FF0B3A4451EAE3C6B65F79A40B6DABA3,
	ObiSolver_Raycast_m295E64295FFE106BCE8A4C8AF731175FFBD53F4E,
	ObiSolver_Raycast_m1BF0ECF62E59E8E53B478E8E28E332DDDB6B7B07,
	ObiSolver__ctor_m62424792417D25BB315A0F76088BA278489E19D6,
	ObiSolver__cctor_mAB381E213872C557C12FC3F05519FF556F1B5E28,
	ObiCollisionEventArgs__ctor_m7C09249A2F3E995C9BEB7D796FCFDFDF5CFDE12E,
	ParticleInActor__ctor_mB6476E7B7DCC76BF8E7067F3C93981363C01F038,
	ParticleInActor__ctor_mFB6EC1CA7434441ECBE52C550353458005AF52C7,
	SolverCallback__ctor_m251483D210E63FBB0C36F821A4629020D9E749C5,
	SolverCallback_Invoke_m60FA315704A9C77F3EC00614322BFB0D0BBE59A1,
	SolverCallback_BeginInvoke_m9D4170E99C634E41DCB9A39E5EC8FFA97EC265FF,
	SolverCallback_EndInvoke_mE89E09F4CCF079537C3C0CB34CA11F3A654383D2,
	SolverStepCallback__ctor_mE4A7F99972297FEF75B4B25D998F4496E5890E18,
	SolverStepCallback_Invoke_m040E6103366E4451B8161A2E24A53FFCB2AF36C5,
	SolverStepCallback_BeginInvoke_mFD6E4177C6426D5562D15B58F3C6C0A71774F775,
	SolverStepCallback_EndInvoke_mF2ECB6CED6D79AD61904B933E40EEFFA74332BBE,
	CollisionCallback__ctor_m89C24EA62FB9D7815E190FB50B37031520F79A07,
	CollisionCallback_Invoke_m00A77BDBCF16A248EBAEC8B3D30FCC86AA91BF48,
	CollisionCallback_BeginInvoke_mC23F3ADD17827DB14FB0436FCC49CFFD8B58E9D6,
	CollisionCallback_EndInvoke_m592B0D186416285FBABBB542CB3D51DFFAE5331E,
	U3CU3Ec__cctor_m52C5CB1B3097B0072B0905815FA7FCB4E4CBC437,
	U3CU3Ec__ctor_m20A3AE22CB86D091916952C584CB8FE816F13064,
	U3CU3Ec_U3Cget_allocParticleCountU3Eb__143_0_m1CBFBBEBF59C2BB9DBAA1254BB36CB4DA2BA086A,
	ObiFixedUpdater_OnValidate_m76229031B40F92A4301EEC77378FE22C1585EF75,
	ObiFixedUpdater_Awake_m05F83D529770320298C9C4C6915A8F91719F1302,
	ObiFixedUpdater_OnDisable_m9F686B2F0BC11763672725B37057F421B46DC477,
	ObiFixedUpdater_FixedUpdate_m6960C109F19FEBC5A547AF798DEABAB519EBBE06,
	ObiFixedUpdater_Update_m91AC799CBA5CC912B6029EB6329A7B5E5E6E9498,
	ObiFixedUpdater__ctor_m6990831EFBDD958CA0F347B5EA7F3DEA852589C8,
	ObiLateFixedUpdater_OnValidate_m213475905C35111F196843770C5E36BF654EA143,
	ObiLateFixedUpdater_Awake_mE1C1C00A4FC7D3BCDFFCA0F70E84E51F5ADC8438,
	ObiLateFixedUpdater_OnEnable_m908C01CE84D9447C31A99BAE4A09D7FFB0D88FDB,
	ObiLateFixedUpdater_OnDisable_mA3804203725D3B8E9112D94FF6F877432FA31AFF,
	ObiLateFixedUpdater_RunLateFixedUpdate_m0CDA0F9B4B1972B0C449BC2FB04DE440C02AF7CD,
	ObiLateFixedUpdater_LateFixedUpdate_mA4CD86E0316FF07E3290E5C9684FF6167CC4761C,
	ObiLateFixedUpdater_Update_m18A4EDFF29C841B340689E1C7D9DAC5454F4B0D2,
	ObiLateFixedUpdater__ctor_m5198FFBED9AAE55028D7D8BD3C86961A54CFEE30,
	U3CRunLateFixedUpdateU3Ed__6__ctor_m1C39A2F60B53405954DCD5ED9FE325876FE5CDC7,
	U3CRunLateFixedUpdateU3Ed__6_System_IDisposable_Dispose_m5EF7D405A0731CF20480B0E79335798E70CCAAC4,
	U3CRunLateFixedUpdateU3Ed__6_MoveNext_mF569121D9A2070DDB78D5F28B7A198528F949DF2,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m6B8AA764EE69F1B4CD885E26C2D7331DEEA888A3,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_Reset_mEFE1C118BDB12FAF1140DDFE8C67855C34061EF2,
	U3CRunLateFixedUpdateU3Ed__6_System_Collections_IEnumerator_get_Current_mC15BD77B70D9ADFFC5B97FA09E8DC226360E37C4,
	ObiLateUpdater_OnValidate_m6A70608502C5AC388A21B4A40E60A1F726539846,
	ObiLateUpdater_LateUpdate_mFCFCD6F9F1BA8745F36429DDE808D093F581903A,
	ObiLateUpdater__ctor_mCBEB8B90CE4D6853A1FCD9606E7D1B40C5C78F53,
	ObiUpdater_BeginStep_m3AA829EBD617621E4C0470786FB8A83B5B170B98,
	ObiUpdater_Substep_mE0E36E8748E6F8C4FFD03D53AF5AB8D8E176D006,
	ObiUpdater_EndStep_mD6541C67A8513F342A8E91D557C6D19ADE67ED2E,
	ObiUpdater_Interpolate_m6EB1759D2C8325F5DE48A578DF71A5A1DD548382,
	ObiUpdater__ctor_mF81A4C5CD5B546F86F6AB868A63AFB947CFD59EC,
	ObiUpdater__cctor_m107D423EE0493C47465FCC5662C2D9BC850F9946,
	ChildrenOnly__ctor_m048197E7104356A10634C5B8B583E1010E74538A,
	Indent__ctor_mECAFC7D3C134B49D1C1A0DF0F973A9454722793A,
	InspectorButtonAttribute_get_ButtonWidth_m8D91F1A836B21EA9065F75CD372433EAB304C3BA,
	InspectorButtonAttribute_set_ButtonWidth_m43CB0912FC4563956094937D35F1A71C0C07A6C0,
	InspectorButtonAttribute__ctor_m1055891570899590DA6F5A5A4134C4156D341934,
	InspectorButtonAttribute__cctor_m94D32958C7FF15E09C795B7727387C628A286A58,
	MinMaxAttribute__ctor_mC312C7DFAB309656D26818D5AF3BD99E77195C51,
	MultiDelayed__ctor_mA3652B4326B3EC475333F955D325DA6991030CFC,
	MultiPropertyAttribute__ctor_mB2FEE6D7DD030ACDDDBE66B7D81C7A207AFEC921,
	MultiRange__ctor_mD5F08C882D531555721C836FCDFA4803B84CBABA,
	SerializeProperty_get_PropertyName_m8DB9FE2FCDAB970A4738FB21D3EBEDEC6A6C583F,
	SerializeProperty_set_PropertyName_m5875C7F872DCB76DB77EE11805CCA13341D20EA2,
	SerializeProperty__ctor_m63D37746946CC59110BBAFDD2ABB6E9B6A7D14D9,
	VisibleIf_get_MethodName_m1A4B9E57BD386B6B40C18154B22C972502F236C1,
	VisibleIf_set_MethodName_mD526342BE1632E2ED1BEC686C11C8B38957ECE12,
	VisibleIf_get_Negate_m7A90AD5D89223445CB93EB298618495AAA7FD991,
	VisibleIf_set_Negate_mA288AECADE78CF5998F29A41D1ADBEAB830677B6,
	VisibleIf__ctor_m93166DBD8925F3F77763F63933E2F7645312953C,
	CoroutineJob_get_Result_m005D2CAD9ED068AB5AF3EA3A23C24111EC4975E2,
	CoroutineJob_get_IsDone_mFF5515EC07F513B5BF06254C73DDEC42567EE798,
	CoroutineJob_get_RaisedException_m1B59EFB8ED5C17EFA9F1AF027BC10A1ED263D4AB,
	CoroutineJob_Init_m39C95915EEEFA70D389ADBBF37D655C876186E52,
	CoroutineJob_RunSynchronously_mD27E02C233BE1EF2F248BEBE9ED29B3488AC39A2,
	CoroutineJob_Start_m375FD45CCADE19D80E522854D0FB39CDDBEE1FE3,
	CoroutineJob_Stop_m9CABBC6A46ABBBE31C12D0C89D8F8EC430B475DC,
	CoroutineJob__ctor_m54279053114D120A1A69B77CD846D98BB00E2538,
	ProgressInfo__ctor_mE34603E06F6E11AA1D4902057779C81537F1BEC0,
	U3CStartU3Ed__15__ctor_m8940EBFDCAB29E979DEFB2E00C0C0EE3B8B327F2,
	U3CStartU3Ed__15_System_IDisposable_Dispose_mD1B1A2865792F4F722584801277F43A9A9AD7135,
	U3CStartU3Ed__15_MoveNext_mA51F84B63ACC0A785E9F7BA3345BBE4EFB0488FB,
	U3CStartU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m05E05A85EE72A4632DBE0E3418F0EE07485FFC0E,
	U3CStartU3Ed__15_System_Collections_IEnumerator_Reset_m68D0EF2950D90BADEFB1CA3A1A2BC46D5E581FB3,
	U3CStartU3Ed__15_System_Collections_IEnumerator_get_Current_mDB081BB96A0648750720D9D315A832F618E04ED2,
	EditorCoroutine_ShowCoroutineProgressBar_m6ADC4C30916CF6733EB5660CC91BD233C4E1B611,
	EditorCoroutine__ctor_mF7ADB1EF57F9E6E6F047E840CB54D4D269427BC0,
	ObiAmbientForceZone_ApplyForcesToActor_mD7F1FF1CA51BC4D30064221DCA818E5BBEE39F90,
	ObiAmbientForceZone_OnDrawGizmosSelected_m6247B9D0C476691F2087960CB116F2F291734833,
	ObiAmbientForceZone__ctor_mD5A328F41FE94DB134B362A688C33EFB40B0336D,
	ObiExternalForce_OnEnable_m9C8027C03DB5299C5CEFD71970D715838FA20F40,
	ObiExternalForce_OnDisable_m99DD20E326728D253D38776120151C4F78B29B90,
	ObiExternalForce_Solver_OnStepBegin_m83BA11CBBDEB98596786149E6B19B9AD07672D06,
	ObiExternalForce_GetTurbulence_m52C1C609B68FEBBEA5CC6A02CCCABFF0F168397B,
	NULL,
	ObiExternalForce__ctor_m1A216AE34D90460D03E46EFE5BE490C7BCA0647E,
	ObiSphericalForceZone_ApplyForcesToActor_m0C3D245E0CABB0E33F06DF470BF31EC361DCDC11,
	ObiSphericalForceZone_OnDrawGizmosSelected_mD53969A56A126FEF59332B15ECAFECCE039BD318,
	ObiSphericalForceZone__ctor_mE4B89FE34EA98D83198B1AC6BD3E0B659E42F82E,
	ObiContactEventDispatcher_CompareByRef_mDC7ED257C6F1AE367534B057503BDD3FF0A079C5,
	ObiContactEventDispatcher_Awake_m07A3EDEAA04DF5FE5C5E681957AD224CBB54E554,
	ObiContactEventDispatcher_OnEnable_m245E24D968CB80FDEA0E644B8666CC7D10AECF83,
	ObiContactEventDispatcher_OnDisable_mF486551C68967776E0F8F27588BC417F47F171A7,
	ObiContactEventDispatcher_FilterOutDistantContacts_m5D131E57296726442BEF2B4E59396989C10BF642,
	ObiContactEventDispatcher_RemoveDuplicates_m6A50787B4E23FD447ACCD8A8DE0995CE74E085FD,
	ObiContactEventDispatcher_InvokeCallbacks_m692BC361454FE9230E82B07824B0D0D3A9E0EB1B,
	ObiContactEventDispatcher_Solver_OnCollision_mBE446B9F7E91796B2A0CAE41FB666011D342B865,
	ObiContactEventDispatcher__ctor_m1A61C75E933604EDC536AAFB53F4F126C96D1475,
	ContactComparer__ctor_m4F8B5DB8A25B890F0E456DB3B8C932EA1869C2EB,
	ContactComparer_Compare_m430A55340AD018C0DC9AEA1CD28B35517B246AA7,
	ContactCallback__ctor_m380D27F1E006FE623593F2C3D10A1263C2E12C13,
	ObiParticleAttachment_get_actor_m27851E500622AFD123A35F0A983417D87C4C6F15,
	ObiParticleAttachment_get_target_m3792C1936854391A4B809902BEDAB269F30BA98A,
	ObiParticleAttachment_set_target_m7C3933233706C549785454D2E774BAC0411F15B1,
	ObiParticleAttachment_get_particleGroup_m8AAF20A83123429F9009F71AA70DC7FE94403D53,
	ObiParticleAttachment_set_particleGroup_m0F43704FD2C3A792860CE707C8A7CCFADEA15695,
	ObiParticleAttachment_get_isBound_m1876F5B16BCD7528475C74D74A79768CC4641B77,
	ObiParticleAttachment_get_attachmentType_m3C6DC87422FA425FBF438A8A7CA8FB5D443EB168,
	ObiParticleAttachment_set_attachmentType_mBCA200E1E3C33103DDC3A3B15F12CFABA97BF09B,
	ObiParticleAttachment_get_constrainOrientation_m08C5A79FCD2BFF79078F88EB6FA2BA72FFC4E28F,
	ObiParticleAttachment_set_constrainOrientation_m247F0066BF99CBC983FE9387278D1B4C7F56DA5D,
	ObiParticleAttachment_get_compliance_m4558C4878270146EF8D49DF5D34FB8AC0D4CCA06,
	ObiParticleAttachment_set_compliance_m8198F8613B2AED92CB67C4F2625F2C0C9AA64D7A,
	ObiParticleAttachment_get_breakThreshold_mCC7D45869EB44AB95B7180D2C15E5ED944541458,
	ObiParticleAttachment_set_breakThreshold_m31EB650F5F8C8054AC5753C115B1E479541BC3B0,
	ObiParticleAttachment_Awake_m23446C5C74A5E7B08FFAA9747F56CE720F87F336,
	ObiParticleAttachment_OnDestroy_m52134FFBF2E5CF631EA7F1292FBAC1A254C7BDE3,
	ObiParticleAttachment_OnEnable_mF34A9D513CF9D52B9A9E0759CC6A83C5176FD22F,
	ObiParticleAttachment_OnDisable_mF8C641CB0E3C1822D1484B333C1FA2AB240AA843,
	ObiParticleAttachment_OnValidate_m74E16BB78FFA79728F39C1946AE3DF5810C28EB4,
	ObiParticleAttachment_Actor_OnBlueprintLoaded_m9BDF9F756C2220716C7B7FF09A2638EC2622ED6C,
	ObiParticleAttachment_Actor_OnPrepareStep_m4ADFFF1468F95B8D80DFD601CDC5281CEEABB476,
	ObiParticleAttachment_Actor_OnEndStep_m17EEBDC31AE3E698DB7EDFDC55A91A46E8CE3B65,
	ObiParticleAttachment_Bind_m0F6831D2367C630B8A813F432781CEB86C372711,
	ObiParticleAttachment_EnableAttachment_mCD920F3627A2454FD1024B3F9B55ADFD3496A830,
	ObiParticleAttachment_DisableAttachment_mFAF630DD4461A1E43AF953FC4C3FC104B98E7AF5,
	ObiParticleAttachment_UpdateAttachment_m7F47F2E7D6E3CA43436CDCE92AE6C07907EA38AD,
	ObiParticleAttachment_BreakDynamicAttachment_mA604FCC48F74F62022DDF42E48A721F28631456C,
	ObiParticleAttachment__ctor_m3FED1D679DE4AE1FE898CA20D26ED611DAD16557,
	ObiParticleDragger_OnEnable_m3A773530120C1DFA785C937A4CF70CE4E1769A34,
	ObiParticleDragger_OnDisable_m98EB30439859B2308D9E1CB5F391B42ED3534582,
	ObiParticleDragger_FixedUpdate_mCD0D51639FBBD4CE43E7AF99B63D2C8ACAA99A2D,
	ObiParticleDragger_Picker_OnParticleDragged_m5AA44A3876EAF6D23656F2906CA70DC45EF0449B,
	ObiParticleDragger_Picker_OnParticleReleased_mCE191F876EB8019B642F2FB993EB80AEE4EE8412,
	ObiParticleDragger__ctor_mFE08058F6A328DAE0BBFA840DF0EE70A4A59750E,
	ObiParticleGridDebugger_OnEnable_m22D8D7E3A0ADF2C746157244B50A0865933C4EC3,
	ObiParticleGridDebugger_OnDisable_m99AFE2A426EEF0783EB1BD8EBEEF4E8F7BBF586B,
	ObiParticleGridDebugger_LateUpdate_m0688BD60CAC637CE3B6378C4A213F544ADAD5285,
	ObiParticleGridDebugger_OnDrawGizmos_m2C22C11A75725EC6F00CF9877B8EF38C17E4C397,
	ObiParticleGridDebugger__ctor_m4C3CDFAAE5C907D9C952386717E4A8FFD3F2EFAE,
	ObiParticlePicker_Awake_m7DA63AB0BE2A8142527070A871933B66E02D7A4D,
	ObiParticlePicker_LateUpdate_m361F701E0BBC294D1C8E01F3EDF0FEF602EB20B8,
	ObiParticlePicker__ctor_m2C886F2EBE8835F3AAD24DBFEFA5521847AE5215,
	ParticlePickEventArgs__ctor_m8E207C6EE600418DE884E613C5E3EBEDF9C09E7C,
	ParticlePickUnityEvent__ctor_m373BF8C05CFB614C704B30975C5D87E01CBFF86B,
	ObiProfiler_Awake_mBAFD910D188643E946BE591D620A0C08E68DA4DD,
	ObiProfiler_OnDestroy_mD81D7084FB7A54922C8839632A0DE626D5302AAC,
	ObiProfiler_OnEnable_m926D972796DF7CA64AEF295E2CB449E18C51CF13,
	ObiProfiler_OnDisable_mA82560267B0124673B8948B116500A7EDA5CF6FD,
	ObiProfiler_EnableProfiler_mB24613A79E73316CF4BF538282767044F78496D5,
	ObiProfiler_DisableProfiler_m81DCFE0E380D85DD402AFD692F8E2D97F79DF5E2,
	ObiProfiler_BeginSample_m1AE47D3A47A61B7AF518318647274933D6B3DBFF,
	ObiProfiler_EndSample_mD326A70EB8BE0A7D5BC9C693C361E5EE155F8A75,
	ObiProfiler_UpdateProfilerInfo_mD4D343B23B55CAF9B4CFF4ED94F0A4EF4C45D109,
	ObiProfiler_OnGUI_m3806190AA0605A63FFF39A275ED86B7114117BB0,
	ObiProfiler__ctor_m9F295F887DA4D4BE379F61D5409B3B4A2974F191,
	ObiStitcher_set_Actor1_mC6598CF544A62DB96DABC5CAEA7BD4779E17EEAD,
	ObiStitcher_get_Actor1_m356C884A3809D178ABBA1DEE765C9EDA6ECFDC60,
	ObiStitcher_set_Actor2_mCF698C1E37A95BBFAA70C2C42BBDEB1BEF628B55,
	ObiStitcher_get_Actor2_mBA875D475DB2DA7069888EBF0E24F2F51ADD15A4,
	ObiStitcher_get_StitchCount_m68C67B275943FE86E6A1809AA71A7F29828C5D08,
	ObiStitcher_get_Stitches_m8516F8D87128E8BBF4172365FEEB4758F5450A83,
	ObiStitcher_RegisterActor_m724A0B01EA95508502CD44192272FDC35C2DD21B,
	ObiStitcher_UnregisterActor_mF6B1662A4D928E4E65792DBD0D23198EA3C2562E,
	ObiStitcher_OnEnable_m36A5797014051C2083B66D6A5576446A3344CF11,
	ObiStitcher_OnDisable_m1023484912F786E1F9741D48EC1DF3B64E76EBD6,
	ObiStitcher_AddStitch_mAD511CFD835FFC50F4ACC3326E071703E50495BB,
	ObiStitcher_RemoveStitch_m18CD6F1050DACAAE2390B3F4C4E7BE53207503A7,
	ObiStitcher_Clear_m69516351DDF1114F9E6A43448E9F3FF2D13F8A16,
	ObiStitcher_Actor_OnBlueprintUnloaded_m72783F365B1F53FB341AB89987A93C5FF02EB371,
	ObiStitcher_Actor_OnBlueprintLoaded_m8E291D7C45A91930606F35AD7DA0FEE3F1EBB13A,
	ObiStitcher_AddToSolver_m2D51C19A3C3A694337A6CA34C2AB14F1CB280633,
	ObiStitcher_RemoveFromSolver_mF0ED8CCA4D5FAEA60D348F8AF912E12339662081,
	ObiStitcher_PushDataToSolver_mB26ABC18EBC9D1EBEB808EA46345C1543C81370F,
	ObiStitcher__ctor_mE621FF9A178F97484204039FE8A0E3C685D3831D,
	Stitch__ctor_m77F4E2459E65A256C836292F31CA58DC586A7E9F,
	ObiUtils_DrawArrowGizmo_mF3CFC39F67B29D29480C7892940CA723BDDBAC17,
	ObiUtils_DebugDrawCross_m45C81519A41F225746ACC8A364D5D095643F40C7,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ObiUtils_AreValid_mAD1A13E1BF4949DD9F7602194B329532DF969179,
	ObiUtils_Transform_m878DAF7F94B524911097CB33F9CF3AFE61C8092D,
	ObiUtils_CountTrailingZeroes_mA673FEC05FCA8F37DFAD005DBD0636FE69A26192,
	ObiUtils_Add_mED6C728427474910D9E3A107F7C89578D51E9EA7,
	ObiUtils_Remap_m7928236905391D25424A3810053A001388670968,
	ObiUtils_Mod_mA7B12E7E20B64E93A98DE55FB942B330EAB868EA,
	ObiUtils_Add_m6B22531DB64E242CC083C4F1AC3E573D3FC8CDBA,
	ObiUtils_FrobeniusNorm_mCE8806A43515214E6EA2739EF5B1609D8410662B,
	ObiUtils_ScalarMultiply_m241041DF6C04AC91ED22CD7636DAF832C4CA5E5C,
	ObiUtils_ProjectPointLine_m684F6C03FE7958BC77E6301F602C60B4E0D2ABF7,
	ObiUtils_LinePlaneIntersection_mE1B2F9FBA229B00CC48AD42DB6A64A9557C074BA,
	ObiUtils_RaySphereIntersection_m7405A53E33BA95DEEFE619690364CB5602FB65EA,
	ObiUtils_InvMassToMass_mDEADF97DA93B534C172F5C8D827408F3810D9088,
	ObiUtils_MassToInvMass_m8F9E7029E98E977A3CBBD522B526147B17A03772,
	ObiUtils_PureSign_m1E9060FDD0112F738B37BD15C6F0DABF5C89E6FB,
	ObiUtils_NearestPointOnTri_m4E23152DD3A4BF065FB6B8CE6777BDB1E8FBF1BE,
	ObiUtils_TriangleArea_mE27F37009D9B943502B2957C73B2A1B46973F7C2,
	ObiUtils_EllipsoidVolume_mE344833B89A9825ACBC2472A6981B11BCE8A21F5,
	ObiUtils_RestDarboux_m33CE5F79C43DBB7E49E448044E502678377F4F53,
	ObiUtils_RestBendingConstraint_m7E1AAC42BED920DA069CCEAC27CE2672E9A8279A,
	ObiUtils_BilateralInterleaved_m0AD7642462AC6B8EB970124C25A5DBE379A042C3,
	ObiUtils_BarycentricCoordinates_m69ADA96835525D413DA970DB5F58D1A796B78C66,
	ObiUtils_BarycentricInterpolation_mD6C458ECFDAF54343A22A4B9AD0EED2D914A1F57,
	ObiUtils_BarycentricInterpolation_m2662CCFC3CC272FA40F515F441172976D695C481,
	ObiUtils_BarycentricExtrapolationScale_mD546BF291A085DFB8421BF498430A56BE55E3E70,
	ObiUtils_CalculateAngleWeightedNormals_mFDE12B5D04049A2A619A43079141E88463B76E85,
	ObiUtils_MakePhase_m7E62F413AD3E7A431DFC67BA06B4257021417A4E,
	ObiUtils_GetGroupFromPhase_m1CDFCCEE425C8D288D514B3450EC7BEC27C3B1AB,
	ObiUtils_GetFlagsFromPhase_m4B508FE5CB15493204397F4AE79EF83C2613B22D,
	ObiUtils_MakeFilter_m602A612ED46165F004BB5706BAA4EF62B2BAE4CB,
	ObiUtils_GetCategoryFromFilter_m17910EB5F69DE5F0D1BA9EE0EE2EC7AE81B7AB6B,
	ObiUtils_GetMaskFromFilter_m257B2F84FBFC454B5369C96DCBE59D79F090778F,
	ObiUtils_EigenSolve_m78B129A7153A92C83DC119D143580E211348C7A3,
	ObiUtils_unitOrthogonal_m32F7323AD50B11C90A771E97782078839D00FE10,
	ObiUtils_EigenVector_m8C746635E6407BC6951F13A9132ADA38918CCA0B,
	ObiUtils_EigenValues_mC5D4BA6FD1D9695FB3DD025B6CB28E534CDBE928,
	ObiUtils_GetPointCloudCentroid_m36E46B062321B39ABBCDCD8750D9D54367296230,
	ObiUtils_GetPointCloudAnisotropy_mAD946D1DB7C96EFC405617E8D7AAF27FBC686FB1,
	ObiUtils__cctor_m785D5918F523383039FF034ED4BEC80433E0FAB6,
	U3CBilateralInterleavedU3Ed__38__ctor_m636AFC81E56CD82E2A1AE8DAE6C190B020EE51AD,
	U3CBilateralInterleavedU3Ed__38_System_IDisposable_Dispose_m073BA4D7FD1C1FA46850B336E8A3ABA8C4165985,
	U3CBilateralInterleavedU3Ed__38_MoveNext_mFDF261F7B732B98CCD34D5C718D947908D024DBE,
	U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m209A3C2B372F3100AF57BA69AA6484694C6D861D,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_Reset_m75BA15CF4816AF7B31D0F2B7B2E2C901246F361A,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerator_get_Current_m2D2F9CEEEC53323CD3E18E948B3132B37316504C,
	U3CBilateralInterleavedU3Ed__38_System_Collections_Generic_IEnumerableU3CSystem_ObjectU3E_GetEnumerator_mC949AADB0472E40C59322D457C4D712A6550F7C7,
	U3CBilateralInterleavedU3Ed__38_System_Collections_IEnumerable_GetEnumerator_m72DAC0FFCCEAB4F52B0E4EEE587674A0E9D9212E,
	ObiVectorMath_Cross_mFFDD7EEEB73CB7550F46DCF22305E54C9B3B26D3,
	ObiVectorMath_Cross_mF02960A84CD632E5A4DAA4C9358B0048D3574193,
	ObiVectorMath_Cross_mE9D0BC9214974A5FEE80CE8F9D2C1E8559FC0FBE,
	ObiVectorMath_Subtract_m2FD84948274EB3DDAA212F4FE0C75B11595704DE,
	ObiVectorMath_BarycentricInterpolation_mC150F53002C484C2C818980620D8EFCE37AE8BCA,
	SetCategory_Awake_m935169A493ED4AB9747EC5F1FA1659E3CA1A13F7,
	SetCategory_OnDestroy_mF5C3FA224A0A3304F35E5B040D5BB99108A52307,
	SetCategory_OnValidate_mBCFAC4034E7C6DB4DA31ABED278E44B4209E709A,
	SetCategory_OnLoad_mD070DB78A32168F801F7C7E5D26FED0D0E608D6B,
	SetCategory__ctor_m41A0F8EEF71EA38B13F698D252E76FDED327848D,
};
extern void SolverParameters__ctor_mCE0C179B96FED563AC3E2E669026A579BE013660_AdjustorThunk (void);
extern void ConstraintParameters__ctor_m863469DE4E22DA6D6EA4B50AB79348BFEB13BEB1_AdjustorThunk (void);
extern void SkinTransform__ctor_mABDA5BC834E71D0637F68EC987DE11419C85364A_AdjustorThunk (void);
extern void SkinTransform__ctor_m04F80634057138EDFCD0E9AF5883073EEE5D2D90_AdjustorThunk (void);
extern void SkinTransform_Apply_m60D31FA6B0918902E7F39C888B680AA41022BAE2_AdjustorThunk (void);
extern void SkinTransform_GetMatrix4X4_mD5A708E729C95FE3D38DD26D16C7E7BE94421D1B_AdjustorThunk (void);
extern void SkinTransform_Reset_m3FD007CF70C5C4ED52952EF1CA7F95B333BD6C1D_AdjustorThunk (void);
extern void BarycentricPoint__ctor_m3EEE56C6E6CB000D8224ADF9173123AC1FF91836_AdjustorThunk (void);
extern void StructuralConstraint_get_restLength_m4DF4D85631CA2689452264F43D092A2FF9FAD9B7_AdjustorThunk (void);
extern void StructuralConstraint_set_restLength_m41F5560600620E38F9CB5122513336B879D36763_AdjustorThunk (void);
extern void StructuralConstraint__ctor_m02AB4F391F2914D0C63E12EA1ABB8A8AB98B00B4_AdjustorThunk (void);
extern void DistanceFieldHeader__ctor_m251B2C29894323C5CEED156849B443EA8067C679_AdjustorThunk (void);
extern void Edge__ctor_m77B0841455A2B88452B465EFBC8FFEEC0A67E648_AdjustorThunk (void);
extern void Edge_GetBounds_m644FB98AE4FEA8DC1BCEEFF06588BEEBF7F7261C_AdjustorThunk (void);
extern void EdgeMeshHeader__ctor_m877A47F9943627D383EFDCEE3A19AE256A9B55A2_AdjustorThunk (void);
extern void HeightFieldHeader__ctor_m262BFF2169E5E485BD00BFACB85132FFED62CDDA_AdjustorThunk (void);
extern void Triangle__ctor_m15AEE3A9EEE96FA1698DED44AB98949A1E11CF62_AdjustorThunk (void);
extern void Triangle_GetBounds_m56E0F7268C436A9036CF11094F585C3F1AF0F998_AdjustorThunk (void);
extern void TriangleMeshHeader__ctor_m8EF03D0DD1903087DE66A92EC3433EF5C7CF9B91_AdjustorThunk (void);
extern void DFNode__ctor_mB94060EAF88B6233E60AB935634F6716E502547E_AdjustorThunk (void);
extern void DFNode_Sample_m14246F91889EC3E920F108B2B406FBC4E44CF124_AdjustorThunk (void);
extern void DFNode_GetNormalizedPos_m335E725D7443558FD8C47CFA490D1BC29034917E_AdjustorThunk (void);
extern void DFNode_GetOctant_m5808A88E16C5E0EEFF5E1CF2E66040C92961E884_AdjustorThunk (void);
extern void Aabb_get_center_m34EBD23AF65E6E8B49FECCE4F4C285610D38F823_AdjustorThunk (void);
extern void Aabb_get_size_mC66C740C1B769CEA883531AC10D85CAD0D4E5657_AdjustorThunk (void);
extern void Aabb__ctor_m4465FBAA62A29E90A0BBEC56EF534EA9582A8AC4_AdjustorThunk (void);
extern void Aabb__ctor_m59BB63BC91E612A9C3ADDB5106EAE996A0BD4465_AdjustorThunk (void);
extern void Aabb_Encapsulate_mAEA769FBEDA585C1186F2D1CB4BD06A880EB8209_AdjustorThunk (void);
extern void Aabb_Encapsulate_m832BC3BAF98AD3CE9B8F36C191777681E6F8FD6A_AdjustorThunk (void);
extern void Aabb_FromBounds_m27239365E8E05E7F09B43FD655D220D115610763_AdjustorThunk (void);
extern void AffineTransform__ctor_mCA54E543BC65B018A5C94C2CE89E325ECA1BB6DC_AdjustorThunk (void);
extern void AffineTransform_FromTransform_m82766F8868CBFE2E7E74A50669374B8987C8BAD9_AdjustorThunk (void);
extern void BIHNode__ctor_m311091C6FC6855D66C9272ACBEC22AFEEF02745A_AdjustorThunk (void);
extern void CellSpan__ctor_mBAA579B4157A8ECC1A3AF1945366566B354D74E1_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m8950C5EDEC5F5FCB2450A939D11E1B7F8F51CA51_AdjustorThunk (void);
extern void ColliderRigidbody_FromRigidbody_m634E6B8C72367A4FE8781687FDA571001810F64B_AdjustorThunk (void);
extern void CollisionMaterial_FromObiCollisionMaterial_m57CF8FE723A8624F55070A837BB0ABEF6A196750_AdjustorThunk (void);
extern void ParticlePair__ctor_m1ABA5CF889DBC5A410CE57D2A9E6F1F7D7536257_AdjustorThunk (void);
extern void ParticlePair_get_Item_m9D26C2D704AA4EE04E5B20ED21AE193CD4A4530A_AdjustorThunk (void);
extern void ParticlePair_set_Item_m9AAF33F1EC57D519A5C6267044F2A569B0E2E156_AdjustorThunk (void);
extern void QueryShape__ctor_mAB2B92EF1A8A1E13D04424FC3483D6990E64C959_AdjustorThunk (void);
extern void SimplexCounts_get_simplexCount_m71F95AE072FC89EBD43364AD9F5D99B8A4AB4C3C_AdjustorThunk (void);
extern void SimplexCounts__ctor_m87B2910E8FA9E6B152BF231CF0B85B8B1BC144AD_AdjustorThunk (void);
extern void SimplexCounts_GetSimplexStartAndSize_m1C63A710053493E24CDBEF324C8A66B27F871641_AdjustorThunk (void);
extern void VInt4__ctor_mAB09F0C216D6336E655285359F99CAC21749BD51_AdjustorThunk (void);
extern void VInt4__ctor_mBDE8910E0F589EDC34EF21F9A4DDC0D3C1B4551E_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[46] = 
{
	{ 0x06000011, SolverParameters__ctor_mCE0C179B96FED563AC3E2E669026A579BE013660_AdjustorThunk },
	{ 0x06000012, ConstraintParameters__ctor_m863469DE4E22DA6D6EA4B50AB79348BFEB13BEB1_AdjustorThunk },
	{ 0x06000128, SkinTransform__ctor_mABDA5BC834E71D0637F68EC987DE11419C85364A_AdjustorThunk },
	{ 0x06000129, SkinTransform__ctor_m04F80634057138EDFCD0E9AF5883073EEE5D2D90_AdjustorThunk },
	{ 0x0600012A, SkinTransform_Apply_m60D31FA6B0918902E7F39C888B680AA41022BAE2_AdjustorThunk },
	{ 0x0600012B, SkinTransform_GetMatrix4X4_mD5A708E729C95FE3D38DD26D16C7E7BE94421D1B_AdjustorThunk },
	{ 0x0600012C, SkinTransform_Reset_m3FD007CF70C5C4ED52952EF1CA7F95B333BD6C1D_AdjustorThunk },
	{ 0x0600012E, BarycentricPoint__ctor_m3EEE56C6E6CB000D8224ADF9173123AC1FF91836_AdjustorThunk },
	{ 0x06000345, StructuralConstraint_get_restLength_m4DF4D85631CA2689452264F43D092A2FF9FAD9B7_AdjustorThunk },
	{ 0x06000346, StructuralConstraint_set_restLength_m41F5560600620E38F9CB5122513336B879D36763_AdjustorThunk },
	{ 0x06000347, StructuralConstraint__ctor_m02AB4F391F2914D0C63E12EA1ABB8A8AB98B00B4_AdjustorThunk },
	{ 0x06000403, DistanceFieldHeader__ctor_m251B2C29894323C5CEED156849B443EA8067C679_AdjustorThunk },
	{ 0x06000408, Edge__ctor_m77B0841455A2B88452B465EFBC8FFEEC0A67E648_AdjustorThunk },
	{ 0x06000409, Edge_GetBounds_m644FB98AE4FEA8DC1BCEEFF06588BEEBF7F7261C_AdjustorThunk },
	{ 0x0600040B, EdgeMeshHeader__ctor_m877A47F9943627D383EFDCEE3A19AE256A9B55A2_AdjustorThunk },
	{ 0x06000414, HeightFieldHeader__ctor_m262BFF2169E5E485BD00BFACB85132FFED62CDDA_AdjustorThunk },
	{ 0x06000428, Triangle__ctor_m15AEE3A9EEE96FA1698DED44AB98949A1E11CF62_AdjustorThunk },
	{ 0x06000429, Triangle_GetBounds_m56E0F7268C436A9036CF11094F585C3F1AF0F998_AdjustorThunk },
	{ 0x0600042B, TriangleMeshHeader__ctor_m8EF03D0DD1903087DE66A92EC3433EF5C7CF9B91_AdjustorThunk },
	{ 0x06000440, DFNode__ctor_mB94060EAF88B6233E60AB935634F6716E502547E_AdjustorThunk },
	{ 0x06000441, DFNode_Sample_m14246F91889EC3E920F108B2B406FBC4E44CF124_AdjustorThunk },
	{ 0x06000442, DFNode_GetNormalizedPos_m335E725D7443558FD8C47CFA490D1BC29034917E_AdjustorThunk },
	{ 0x06000443, DFNode_GetOctant_m5808A88E16C5E0EEFF5E1CF2E66040C92961E884_AdjustorThunk },
	{ 0x06000444, Aabb_get_center_m34EBD23AF65E6E8B49FECCE4F4C285610D38F823_AdjustorThunk },
	{ 0x06000445, Aabb_get_size_mC66C740C1B769CEA883531AC10D85CAD0D4E5657_AdjustorThunk },
	{ 0x06000446, Aabb__ctor_m4465FBAA62A29E90A0BBEC56EF534EA9582A8AC4_AdjustorThunk },
	{ 0x06000447, Aabb__ctor_m59BB63BC91E612A9C3ADDB5106EAE996A0BD4465_AdjustorThunk },
	{ 0x06000448, Aabb_Encapsulate_mAEA769FBEDA585C1186F2D1CB4BD06A880EB8209_AdjustorThunk },
	{ 0x06000449, Aabb_Encapsulate_m832BC3BAF98AD3CE9B8F36C191777681E6F8FD6A_AdjustorThunk },
	{ 0x0600044A, Aabb_FromBounds_m27239365E8E05E7F09B43FD655D220D115610763_AdjustorThunk },
	{ 0x0600044B, AffineTransform__ctor_mCA54E543BC65B018A5C94C2CE89E325ECA1BB6DC_AdjustorThunk },
	{ 0x0600044C, AffineTransform_FromTransform_m82766F8868CBFE2E7E74A50669374B8987C8BAD9_AdjustorThunk },
	{ 0x06000454, BIHNode__ctor_m311091C6FC6855D66C9272ACBEC22AFEEF02745A_AdjustorThunk },
	{ 0x06000456, CellSpan__ctor_mBAA579B4157A8ECC1A3AF1945366566B354D74E1_AdjustorThunk },
	{ 0x06000457, ColliderRigidbody_FromRigidbody_m8950C5EDEC5F5FCB2450A939D11E1B7F8F51CA51_AdjustorThunk },
	{ 0x06000458, ColliderRigidbody_FromRigidbody_m634E6B8C72367A4FE8781687FDA571001810F64B_AdjustorThunk },
	{ 0x06000459, CollisionMaterial_FromObiCollisionMaterial_m57CF8FE723A8624F55070A837BB0ABEF6A196750_AdjustorThunk },
	{ 0x060004BB, ParticlePair__ctor_m1ABA5CF889DBC5A410CE57D2A9E6F1F7D7536257_AdjustorThunk },
	{ 0x060004BC, ParticlePair_get_Item_m9D26C2D704AA4EE04E5B20ED21AE193CD4A4530A_AdjustorThunk },
	{ 0x060004BD, ParticlePair_set_Item_m9AAF33F1EC57D519A5C6267044F2A569B0E2E156_AdjustorThunk },
	{ 0x060004BE, QueryShape__ctor_mAB2B92EF1A8A1E13D04424FC3483D6990E64C959_AdjustorThunk },
	{ 0x060004BF, SimplexCounts_get_simplexCount_m71F95AE072FC89EBD43364AD9F5D99B8A4AB4C3C_AdjustorThunk },
	{ 0x060004C0, SimplexCounts__ctor_m87B2910E8FA9E6B152BF231CF0B85B8B1BC144AD_AdjustorThunk },
	{ 0x060004C1, SimplexCounts_GetSimplexStartAndSize_m1C63A710053493E24CDBEF324C8A66B27F871641_AdjustorThunk },
	{ 0x060004C2, VInt4__ctor_mAB09F0C216D6336E655285359F99CAC21749BD51_AdjustorThunk },
	{ 0x060004C3, VInt4__ctor_mBDE8910E0F589EDC34EF21F9A4DDC0D3C1B4551E_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1624] = 
{
	89,
	23,
	23,
	23,
	27,
	23,
	147,
	23,
	23,
	23,
	23,
	1957,
	90,
	121,
	1002,
	3102,
	1454,
	1278,
	14,
	14,
	89,
	31,
	726,
	337,
	726,
	337,
	89,
	31,
	726,
	337,
	726,
	337,
	14,
	26,
	23,
	23,
	23,
	14,
	89,
	31,
	89,
	31,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	89,
	31,
	726,
	337,
	726,
	337,
	89,
	26,
	26,
	23,
	23,
	14,
	14,
	89,
	31,
	726,
	337,
	726,
	337,
	14,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	337,
	23,
	3,
	14,
	14,
	14,
	26,
	26,
	26,
	26,
	26,
	23,
	23,
	337,
	337,
	3103,
	3104,
	32,
	3105,
	3106,
	3107,
	26,
	32,
	23,
	23,
	23,
	23,
	3,
	131,
	27,
	132,
	26,
	3108,
	3,
	23,
	3109,
	89,
	14,
	14,
	14,
	14,
	14,
	23,
	114,
	26,
	123,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	30,
	3,
	23,
	3110,
	14,
	32,
	14,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	89,
	14,
	14,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	10,
	89,
	14,
	14,
	28,
	28,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	89,
	89,
	726,
	726,
	23,
	26,
	23,
	23,
	23,
	136,
	3111,
	3112,
	3113,
	3113,
	3113,
	14,
	30,
	32,
	23,
	89,
	3114,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	3115,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	3116,
	23,
	14,
	14,
	14,
	26,
	14,
	26,
	14,
	23,
	23,
	31,
	31,
	35,
	137,
	137,
	3117,
	1390,
	3118,
	3119,
	14,
	23,
	23,
	3005,
	23,
	2380,
	26,
	26,
	1358,
	23,
	3120,
	1660,
	4,
	89,
	3121,
	32,
	23,
	89,
	14,
	23,
	14,
	26,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	26,
	26,
	14,
	1358,
	23,
	23,
	23,
	23,
	23,
	23,
	27,
	27,
	23,
	35,
	35,
	26,
	23,
	3,
	23,
	27,
	27,
	23,
	1358,
	23,
	27,
	23,
	35,
	26,
	23,
	14,
	23,
	23,
	23,
	23,
	27,
	142,
	27,
	23,
	10,
	10,
	89,
	37,
	2401,
	3072,
	3039,
	1451,
	1453,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	89,
	14,
	26,
	89,
	31,
	10,
	10,
	89,
	89,
	89,
	1358,
	1358,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	52,
	1450,
	1651,
	32,
	30,
	30,
	30,
	31,
	31,
	23,
	32,
	34,
	23,
	37,
	2401,
	3072,
	3039,
	1451,
	1453,
	32,
	32,
	337,
	3122,
	1656,
	1656,
	137,
	23,
	23,
	26,
	23,
	23,
	26,
	26,
	337,
	337,
	337,
	337,
	23,
	31,
	23,
	14,
	26,
	131,
	26,
	218,
	26,
	131,
	939,
	3123,
	26,
	131,
	27,
	132,
	26,
	10,
	337,
	123,
	26,
	26,
	442,
	442,
	27,
	27,
	110,
	3124,
	3124,
	3125,
	10,
	14,
	31,
	89,
	23,
	32,
	10,
	10,
	14,
	10,
	3125,
	3126,
	3127,
	23,
	3126,
	123,
	3126,
	3125,
	3126,
	58,
	26,
	23,
	23,
	3128,
	3129,
	1534,
	26,
	137,
	3125,
	26,
	34,
	26,
	37,
	137,
	137,
	824,
	2166,
	3130,
	3131,
	10,
	35,
	56,
	3132,
	3133,
	568,
	23,
	10,
	26,
	212,
	58,
	26,
	23,
	23,
	3128,
	3129,
	1534,
	10,
	35,
	56,
	3132,
	26,
	26,
	137,
	23,
	568,
	3133,
	37,
	137,
	137,
	824,
	34,
	26,
	2166,
	3130,
	3131,
	3125,
	10,
	26,
	212,
	23,
	1451,
	1023,
	3134,
	10,
	14,
	26,
	3135,
	62,
	23,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	27,
	3136,
	23,
	62,
	136,
	26,
	26,
	10,
	14,
	26,
	3137,
	23,
	62,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	1955,
	23,
	62,
	136,
	27,
	26,
	26,
	10,
	10,
	32,
	10,
	32,
	10,
	14,
	26,
	26,
	27,
	30,
	30,
	23,
	23,
	62,
	136,
	10,
	10,
	32,
	10,
	32,
	10,
	14,
	27,
	136,
	62,
	26,
	26,
	137,
	136,
	23,
	23,
	37,
	30,
	30,
	30,
	23,
	32,
	136,
	23,
	10,
	14,
	32,
	3138,
	23,
	1451,
	1023,
	3134,
	62,
	137,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	3139,
	23,
	62,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	102,
	23,
	62,
	136,
	27,
	26,
	26,
	23,
	10,
	14,
	26,
	3140,
	23,
	62,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	3141,
	23,
	1451,
	1023,
	3134,
	62,
	136,
	27,
	26,
	26,
	10,
	14,
	26,
	3142,
	23,
	62,
	136,
	27,
	26,
	26,
	1341,
	10,
	14,
	26,
	939,
	23,
	62,
	136,
	27,
	26,
	26,
	1341,
	89,
	31,
	726,
	337,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	28,
	23,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	726,
	337,
	28,
	23,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	28,
	23,
	89,
	31,
	726,
	337,
	726,
	337,
	28,
	23,
	3143,
	34,
	10,
	23,
	9,
	89,
	10,
	10,
	23,
	27,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	726,
	337,
	1957,
	1,
	26,
	26,
	10,
	10,
	89,
	89,
	30,
	32,
	30,
	30,
	89,
	23,
	1459,
	14,
	34,
	37,
	2401,
	3072,
	3039,
	1451,
	1453,
	23,
	14,
	58,
	28,
	30,
	2088,
	23,
	961,
	3144,
	27,
	136,
	1022,
	23,
	26,
	23,
	14,
	23,
	131,
	26,
	218,
	26,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	14,
	26,
	10,
	30,
	23,
	27,
	89,
	27,
	89,
	27,
	89,
	27,
	23,
	89,
	23,
	27,
	89,
	27,
	89,
	27,
	89,
	212,
	23,
	89,
	23,
	27,
	23,
	89,
	23,
	23,
	89,
	23,
	27,
	89,
	27,
	23,
	89,
	23,
	26,
	14,
	26,
	14,
	23,
	495,
	23,
	23,
	26,
	14,
	23,
	495,
	23,
	23,
	26,
	14,
	32,
	10,
	337,
	726,
	14,
	14,
	15,
	14,
	23,
	495,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	-1,
	-1,
	-1,
	-1,
	-1,
	32,
	32,
	32,
	4,
	23,
	23,
	23,
	26,
	26,
	14,
	14,
	14,
	28,
	26,
	28,
	26,
	28,
	26,
	28,
	26,
	26,
	26,
	26,
	23,
	939,
	337,
	26,
	23,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	89,
	1459,
	726,
	26,
	14,
	23,
	14,
	34,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	137,
	136,
	23,
	28,
	26,
	23,
	3145,
	3146,
	137,
	1510,
	23,
	28,
	26,
	23,
	3,
	23,
	3147,
	137,
	136,
	23,
	28,
	26,
	23,
	23,
	337,
	337,
	1377,
	23,
	23,
	337,
	337,
	1377,
	23,
	23,
	23,
	337,
	1377,
	23,
	3148,
	3146,
	137,
	1510,
	23,
	28,
	26,
	23,
	3,
	23,
	3149,
	3150,
	3151,
	23,
	3,
	3,
	23,
	3149,
	32,
	23,
	89,
	14,
	23,
	14,
	1820,
	1390,
	1361,
	3029,
	1460,
	1460,
	3152,
	1820,
	1820,
	3153,
	3154,
	3155,
	102,
	3156,
	3157,
	3158,
	3159,
	3160,
	23,
	441,
	136,
	3146,
	3161,
	102,
	102,
	26,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	3162,
	136,
	136,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	136,
	136,
	3163,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	136,
	2401,
	1450,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	-1,
	136,
	37,
	136,
	3164,
	10,
	38,
	1352,
	341,
	32,
	3165,
	939,
	3166,
	3167,
	1613,
	3168,
	1380,
	23,
	3169,
	3170,
	26,
	1613,
	23,
	368,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	1470,
	23,
	23,
	23,
	23,
	26,
	23,
	3,
	14,
	14,
	23,
	23,
	23,
	26,
	23,
	23,
	3,
	14,
	26,
	23,
	26,
	23,
	3,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	26,
	14,
	89,
	14,
	32,
	10,
	3171,
	1459,
	89,
	726,
	10,
	10,
	10,
	10,
	10,
	10,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	14,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	32,
	23,
	32,
	26,
	26,
	9,
	9,
	23,
	3172,
	34,
	23,
	23,
	23,
	23,
	23,
	337,
	2166,
	3130,
	337,
	1341,
	212,
	3173,
	3174,
	3175,
	23,
	3,
	23,
	23,
	137,
	131,
	26,
	218,
	26,
	131,
	939,
	3123,
	26,
	131,
	27,
	132,
	26,
	3,
	23,
	9,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	14,
	23,
	23,
	23,
	32,
	23,
	89,
	14,
	23,
	14,
	23,
	23,
	23,
	337,
	2706,
	337,
	1341,
	23,
	3,
	23,
	23,
	726,
	337,
	26,
	3,
	1341,
	23,
	23,
	1341,
	14,
	26,
	26,
	14,
	26,
	89,
	31,
	102,
	14,
	89,
	89,
	23,
	0,
	28,
	23,
	23,
	939,
	32,
	23,
	89,
	14,
	23,
	14,
	393,
	23,
	26,
	23,
	23,
	23,
	23,
	939,
	1345,
	26,
	23,
	26,
	23,
	23,
	3176,
	23,
	23,
	23,
	503,
	503,
	137,
	27,
	23,
	26,
	3177,
	23,
	14,
	14,
	26,
	14,
	26,
	89,
	10,
	32,
	89,
	31,
	726,
	337,
	726,
	337,
	23,
	23,
	23,
	23,
	23,
	27,
	939,
	939,
	23,
	32,
	32,
	23,
	337,
	23,
	23,
	23,
	23,
	26,
	26,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	23,
	1450,
	23,
	23,
	23,
	23,
	23,
	3,
	3,
	610,
	3,
	23,
	23,
	23,
	26,
	14,
	26,
	14,
	10,
	14,
	26,
	26,
	23,
	23,
	56,
	32,
	23,
	27,
	27,
	26,
	26,
	23,
	23,
	136,
	1444,
	3178,
	-1,
	-1,
	-1,
	-1,
	-1,
	2305,
	2314,
	21,
	3179,
	3180,
	441,
	1560,
	3181,
	3182,
	3183,
	3184,
	3185,
	280,
	280,
	265,
	3186,
	1570,
	1571,
	1585,
	1570,
	43,
	3186,
	3186,
	3187,
	1571,
	1,
	182,
	21,
	21,
	182,
	21,
	21,
	2421,
	1415,
	3188,
	3189,
	2358,
	3190,
	3,
	32,
	23,
	89,
	14,
	23,
	14,
	14,
	14,
	3191,
	3179,
	3192,
	3179,
	3193,
	23,
	23,
	23,
	27,
	23,
};
static const Il2CppTokenRangePair s_rgctxIndices[11] = 
{
	{ 0x02000081, { 0, 17 } },
	{ 0x020000D2, { 17, 24 } },
	{ 0x020000D3, { 45, 3 } },
	{ 0x020000DE, { 48, 8 } },
	{ 0x020000DF, { 56, 1 } },
	{ 0x06000478, { 41, 1 } },
	{ 0x06000479, { 42, 1 } },
	{ 0x0600047A, { 43, 2 } },
	{ 0x0600061D, { 57, 2 } },
	{ 0x0600061E, { 59, 1 } },
	{ 0x0600061F, { 60, 1 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[61] = 
{
	{ (Il2CppRGCTXDataType)2, 63780 },
	{ (Il2CppRGCTXDataType)3, 62914 },
	{ (Il2CppRGCTXDataType)3, 62915 },
	{ (Il2CppRGCTXDataType)3, 62916 },
	{ (Il2CppRGCTXDataType)3, 62917 },
	{ (Il2CppRGCTXDataType)2, 59918 },
	{ (Il2CppRGCTXDataType)3, 62918 },
	{ (Il2CppRGCTXDataType)3, 62919 },
	{ (Il2CppRGCTXDataType)3, 62920 },
	{ (Il2CppRGCTXDataType)3, 62921 },
	{ (Il2CppRGCTXDataType)2, 63781 },
	{ (Il2CppRGCTXDataType)3, 62922 },
	{ (Il2CppRGCTXDataType)3, 62923 },
	{ (Il2CppRGCTXDataType)3, 62924 },
	{ (Il2CppRGCTXDataType)3, 62925 },
	{ (Il2CppRGCTXDataType)2, 63782 },
	{ (Il2CppRGCTXDataType)3, 62926 },
	{ (Il2CppRGCTXDataType)3, 62927 },
	{ (Il2CppRGCTXDataType)3, 62928 },
	{ (Il2CppRGCTXDataType)3, 62929 },
	{ (Il2CppRGCTXDataType)3, 62930 },
	{ (Il2CppRGCTXDataType)3, 62931 },
	{ (Il2CppRGCTXDataType)3, 62932 },
	{ (Il2CppRGCTXDataType)3, 62933 },
	{ (Il2CppRGCTXDataType)3, 62934 },
	{ (Il2CppRGCTXDataType)2, 60116 },
	{ (Il2CppRGCTXDataType)3, 62935 },
	{ (Il2CppRGCTXDataType)3, 62936 },
	{ (Il2CppRGCTXDataType)3, 62937 },
	{ (Il2CppRGCTXDataType)3, 62938 },
	{ (Il2CppRGCTXDataType)3, 62939 },
	{ (Il2CppRGCTXDataType)3, 62940 },
	{ (Il2CppRGCTXDataType)2, 63783 },
	{ (Il2CppRGCTXDataType)2, 60117 },
	{ (Il2CppRGCTXDataType)2, 60119 },
	{ (Il2CppRGCTXDataType)3, 62941 },
	{ (Il2CppRGCTXDataType)3, 62942 },
	{ (Il2CppRGCTXDataType)2, 60110 },
	{ (Il2CppRGCTXDataType)2, 63784 },
	{ (Il2CppRGCTXDataType)3, 62943 },
	{ (Il2CppRGCTXDataType)3, 62944 },
	{ (Il2CppRGCTXDataType)3, 62945 },
	{ (Il2CppRGCTXDataType)3, 62946 },
	{ (Il2CppRGCTXDataType)3, 62947 },
	{ (Il2CppRGCTXDataType)3, 62948 },
	{ (Il2CppRGCTXDataType)3, 62949 },
	{ (Il2CppRGCTXDataType)3, 62950 },
	{ (Il2CppRGCTXDataType)2, 60125 },
	{ (Il2CppRGCTXDataType)2, 63786 },
	{ (Il2CppRGCTXDataType)3, 62951 },
	{ (Il2CppRGCTXDataType)3, 62952 },
	{ (Il2CppRGCTXDataType)3, 62953 },
	{ (Il2CppRGCTXDataType)2, 60143 },
	{ (Il2CppRGCTXDataType)3, 62954 },
	{ (Il2CppRGCTXDataType)3, 62955 },
	{ (Il2CppRGCTXDataType)2, 60144 },
	{ (Il2CppRGCTXDataType)2, 60148 },
	{ (Il2CppRGCTXDataType)2, 63787 },
	{ (Il2CppRGCTXDataType)2, 60333 },
	{ (Il2CppRGCTXDataType)3, 62956 },
	{ (Il2CppRGCTXDataType)3, 62957 },
};
extern const Il2CppCodeGenModule g_ObiCodeGenModule;
const Il2CppCodeGenModule g_ObiCodeGenModule = 
{
	"Obi.dll",
	1624,
	s_methodPointers,
	46,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	11,
	s_rgctxIndices,
	61,
	s_rgctxValues,
	NULL,
};
